<!doctype html>
<html>
	<?php
	// this file inluded for js,css and for notes section and add colletion this is required to include
    require_once APPPATH.'modules/core/views/header_include.php';
	?>
	
<!-- TODO: Single PHP block to echo body? -->
<body>

<div id="sidedrawer" class="mui--no-user-select">
  <!--
  <div id="sidedrawer-brand" class="mui--appbar-line-height mui--text-title">
    <a href="" target="_self">
      <img src="<?php echo base_url(); ?>assets/core/images/<?php echo $appIcon?>" class="logo-nav-menu"/>
    </a>
  <?php echo($appTitle)?>
  </div>
  <div class="mui-divider"></div>-->
  <?php echo($menu_data); ?>
</div>



<header id="header">
	<!-- Load the charts library used in dashboards -->
	<script src="<?php echo(base_url())?>salesdashboard/assets/js/Chart.bundle.js"></script>
	<script src="<?php echo(base_url())?>salesdashboard/assets/js/ChartPieceLabel.min.js"></script>
	
	<!-- Load the CSS used in the dashboards -->
	<link href="<?php echo(base_url())?>salesdashboard/assets/css/style_dashboard.css" rel="stylesheet" type="text/css">
	<!-- <img src="<?php echo base_url();?>assets/core/images/chess.jpg" height="50px" width="50px" />-->
	
	<?php
    // If the user has permission, load the editbar
    if($this->session->userdata('Allowdesignmode') == 1) {
      echo('<script src="'.base_url().'core/assets/js/PageEdit.js"></script>');
      $this->load->view('editbar', true);
    }
	?>


	<div class="mui-appbar mui-container-fluid mui--appbar-line-height">
		<!-- Header and Edit section -->
		<table width="100%">
			<input type = "hidden" id="pageIdHeader" value="<?php echo $pageInfo->pageId; ?>">
			<tr style="vertical-align:middle;">
				<td class="mui--appbar-height">
					<a class="sidedrawer-toggle mui--visible-xs-inline-block js-show-sidedrawer">☰</a>
						<!-- Code start for Add page name in Header Added by HD -->
					<a class="sidedrawer-toggle mui--hidden-xs js-hide-sidedrawer" id="control_title">
						☰  <?php //echo($title)?>
					</a>
					<span class="mui--text-title mui--visible-xs-inline-block">
						<?php
						  echo strlen($pageInfo->Title) > 20 ?
							substr($pageInfo->Title, 0, 20)."..." :
							$pageInfo->Title;
						?>
					</span>
				<!-- Code end for Add page name in Header Added by HD -->
				</td>
				<td class="mui--appbar-height" align="right">

					<?php
					// If user has permissions to edit pages, show the button to open the edit bar
					if($this->session->userdata('Allowdesignmode') == 1) {
						$btn = '<button id="editBtn" class="mui-btn mui-btn--primary">';
						$btn .= '<img src="'.base_url().'assets/core/images/ic_build_black_24dp_1x.png" style="display:inline-block; -webkit-filter:invert(1); filter:invert(1);">';
						$btn .= '</button>';
						echo($btn);
					}
					?>
					<a href="<?php echo base_url();?>/core/Login/logout">
						<img src="<?php echo base_url();?>assets/core/images/logout-white.png" />
					</a>
				</td>
			</tr>
		</table>
	  
		<!-- Dashboard Paramerters section -->
		<div class="dashboard_heading">
			Opportunity Pipline Status Dashboard
			<form method="get" style="padding:0px; margin:0px">
			  <input type="hidden" id="user-id" value="<?php echo($this->session->userdata('user_id'))?>"/>
			</form>
		</div>
		<!--
		<div class="cs_search_container secondary_bg_color">
		  <div class="cs_search">
			<div class="cs_icon">
			  <img src="<?php echo base_url();?>/application/modules/salesdashboard/assets/images/search.png"/>
			</div>
			<form method="get">
			  <input type="hidden" id="user-id" value="<?php echo($this->session->userdata('user_id'))?>"/>
			  <input type="text" id="form-customer-search" class="cs_input" placeholder="To view a customer, search via customer name or ID">
			</form>
		  </div>
		</div>
		<div class="secondary_bg_color">
		  <form class="dashboard-params-main" id="dashboardParams">
			<div class="dashboard-param">

			  <label>Date From: </label>
			  <select id="dateSelector">
				<option id="param_date_ytd">FINANCIAL YTD</option>

				<optgroup id="param_date_group_curr" label="">
				</optgroup>

				<optgroup id="param_date_group_prev" label="">
				</optgroup>
			  </select>

			  <label>Account Manager: </label>
			  <select id="repSelector">
			  </select>

			  <input type="text" id="param-date-today" hidden/>
			</div>

			<button type="submit" id="param-btn-submit" class="dashboard-btn">SUBMIT</button>
		  </form>
		</div>
		-->
    </div>
</header>

<div id="content-wrapper">
  <div class="mui--appbar-height" style="height:150px;"></div>

<div class="mui-container-fluid">
  <div class="space-top" style="display: none;"></div>
  <div id="content_part" style="max-width:1200px; margin-left:auto; margin-right:auto;">
  


<?php
$curr_url = 'http://'.$_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI'];
$page_flg = 0;


foreach ($page_data_permission as $data) {
    if (($this->session->userdata('GroupID') == $data->GroupId) && (("page/".$this->uri->segment(2, 0) == $data->ModuleName || strpos(urldecode($_SERVER['REQUEST_URI']),$data->ModuleName) > 0))) {
        $page_flg = 1;
        break;
    } else {
        $page_flg = 0;
    }
}
//exit;
if (isset($page_flg_time)) {
    $page_flg = 1;
}
//echo $page_flg;exit;
// Pageheader Remove code added by HD
if ($page_flg == 1 || $curr_url == SITEURL.'home') {
    ?><h1><?php //echo($pageInfo->Title);?></h1><?php
}
?>
<?php
if ($page_flg == 1 || $curr_url == SITEURL.'home') {
    echo $page_content;
} else {
    include_once('Error_page.php');
}
?>

<script>

  const debug = false;

  // Setup URL's for procedure calls
  var controller_url = "/bsit_sch/salesdashboard/DashboardData/";
  var method_url = controller_url + "OpDashSP?method=";
  var param_url = controller_url + "OpDashSP?method=";

  // setup some global variables used in various reports
  var userID = 0;
  var selectedID = 0;
  var startDate;
  var endDate;
  var state = "VIC & TAS";
  var firstRowFrames = [];
  var secondRowFrames = [];
  var allCharts = [];

  const monthLabels = ['Jan', 'Feb', 'Mar','Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const historyColors = ['deepskyblue', 'lime', 'darkorange', 'red'];
  
  
  const cols = ['#2196F3','#FFC1CF','#DB3A34',	'#FFC857',	'#FF934F','#B7FFD8','#C4F5FC','#E2A0FF','#E8FFB7','#177E89','#084C61'];
  

  // Setup custom plugin for text inside doughnut charts
  const donuts = {
    beforeDraw: function(chart) {
      var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;

      ctx.restore();

      var fontSize = (height / 114).toFixed(2);
      ctx.font = fontSize + "em sans-serif";
      ctx.textBaseline = "middle";
      var text = chart.config.options.plugins.donuts.text + "%",
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = height / 2;

      ctx.fillText(text, textX, textY);
      ctx.save();
    }
  };

  // Run some functions when the page is ready
  $(document).ready(function() {
    // Hide the sidedrawer
    $("#sidedrawer").parent().toggleClass('hide-sidedrawer');

    userID = $("#user-data").data("userid");
    BuildFrameList();

    // for (let i = 78; i < 87; i++) {
    //   HideLoader(i);
    // }

    // Setup Dashboard Parameters
    SetupParameters()
	//SetupDateParameters();
    //SetupRepParameters();
	

    // Set default dashboard params
    //startDate = $("#dateSelector").val();
    //endDate = GetEndDate();


    // Setup the search autocomplete here
    /*$("#form-customer-search").autocomplete({
      // Customer Search Functions
      minLength: 0,
      delay: 300,
      autoFocus: true,
      source: (req, res) => {
        GetSearchData(req.term, (result) => res(result));
      }
    }).focus(function() {
      // Fire the recent customer search on an empty input focus
      if($("#form-customer-search").val().length < 1) {
        $("#form-customer-search").autocomplete("search");
      }
    }).data("ui-autocomplete")._renderItem = (ul, item) => {
      var param = ('"' + item.cid + "_" + item.aid + '"');
      $("ul.ui-autocomplete").css("border-radius", "5px");
      return $("<li onclick='DashboardRedirect(" + param + ")'>" + item.label + "</li>")
        .appendTo(ul);
    };
	*/

	/*
    // Submit parameters button
    $("#param-btn-submit").click(function(e) {
      e.preventDefault();
      // Set selected parameters
      selectedID = $('#repSelector').val();
      startDate = $("#dateSelector").val();
      endDate = GetEndDate();

      RefreshRepDashboard();
    });
	*/
	
	/*
    // Change CSS of search bar on focus
    $('.cs_input').focus(() => {
      $('.cs_search').css("background-color", "#FFF");
    }).blur(() => {
      $('.cs_search').css("background-color", "rgb(200, 238, 253)");
    });
	*/

  });

  //#region WINDOWEVENTS
  $(window).scroll(function() {
    // Show the header border as we start to scroll
    if ($(window).scrollTop() > 10) {
      $('.header-main').addClass("header-shadow");
      $('.header-main').removeClass("header-no-shadow");
    } else {
      $('.header-main').addClass("header-no-shadow");
      $('.header-main').removeClass("header-shadow");
    }
  });

  $(window).resize(function() {
    // Handle window resizing to update frame heights
    waitForFinalEvent(function(){
      AlignFrameHeights(firstRowFrames);
      //AlignFrameHeights(secondRowFrames);
    }, 300, "Window Resize Complete");
  });
  //#endregion

  // load any needed parameters and refresh the dashboard
  function SetupParameters() {
    var url = param_url + "RepDetails";
    RefreshRepDashboard();
  }
  
  //#region SETUP
  function SetupDateParameters() {

    displayOptions = {
      month: 'short',
      year: 'numeric',
      timeZone: 'Australia/Melbourne'
    };

    var serverDate = new Date($.ajax({async: false}).getResponseHeader('Date'));
    var yearMonth = {
      year: serverDate.getFullYear(),
      month: serverDate.getMonth()
    };

    // Set current date to provide to SP's.
    $("#param-date-today").attr('value', (yearMonth.year) + '-' + (yearMonth.month + 1) + '-' + serverDate.getDate());

    // Set the financial YTD value
    if(yearMonth.month < 7) {
      $("#param_date_ytd").val((yearMonth.year - 1) + '-07-01');
    } else {
      $("#param_date_ytd").val((yearMonth.year) + '-07-01');
    }

    // Setup current year options
    $("#param_date_group_curr").attr('label', yearMonth.year);
    for(var i = yearMonth.month; i >= 0; i--) {
      serverDate.setMonth(i);

      var val = yearMonth.year + '-' + (i + 1);
      var display = new Intl.DateTimeFormat('en-AU', displayOptions).format(serverDate);
      if(i == yearMonth.month) {
        $("#param_date_group_curr").append('<option value="' + val + '-01" selected>' + display + '</option>');
      } else {
        $("#param_date_group_curr").append('<option value="' + val + '-01">' + display + '</option>');
      }
    }

    // Setup previous year options
    $("#param_date_group_prev").attr('label', yearMonth.year - 1);
    serverDate.setFullYear(yearMonth.year - 1);
    for(var i = 11; i >= 0; i--) {
      serverDate.setMonth(i);

      var val = (yearMonth.year - 1) + '-' + (i + 1);
      var display = new Intl.DateTimeFormat('en-AU', displayOptions).format(serverDate);
      $("#param_date_group_prev").append('<option value="' + val + '-01">' + display + '</option>');
    }
  }

  function SetupRepParameters() {
    var url = param_url + "RepDetails";

    $.ajax({
      method: "POST",
      url:    url,
      data: { userID: userID }
    }).success(function(data) {
      // do we want to debug the responses?
      if(debug == true) { console.log(data); }

      // attempt to parse the JSON string
      var jsonData = JSON.parse(data);

      // do we have any errors in the call?
      if(jsonData.HasError == true) { return; }

      // loop each of the data elements and add to the list of reps
      var repCount = 0;
      var userGroup = 0;
      $.each(jsonData.Data, function(key, value) {
        if(repCount == 0) {
          userGroup = value.group_id;
          opt = '<option value="' + value.user_id + '" selected>' + value.name_first + ' ' + value.name_last + '</option>';
        } else {
          opt = '<option value="' + value.user_id + '">' + value.name_first + ' ' + value.name_last + '</option>';
        }
        $('#repSelector').append(opt);
        repCount++;
      });

      // if we have any reps, refresh the charts.
      selectedID = $("#repSelector").val();
      RefreshRepDashboard();
      // if(userGroup == 2) {
      // }
    });
  }

  function RefreshRepDashboard() {

    if(allCharts.length > 0) {
      allCharts.forEach((element, index) => {
        element.destroy();
        element = null;
        allCharts.splice(index, 1);
      });
    }

    // Row 1
    DashCallProcedure(97, 'PipelineOwner',  		UpdateOwner);
    DashCallProcedure(98, 'PipelineSource', 		UpdateSource);
	
	// Row 2
    DashCallProcedure(99, 'PipelineStage',      	UpdateStage);
	
    // Row 3
	DashCallProcedure(100, 'PipelineIncomeStream',  UpdateIncomeStream);

    // NOTE: The position of the below lines is legacy, as ASYNC calls above will all finish at different times....
    // this needs a different approach
    AlignFrameHeights(firstRowFrames);
    AlignFrameHeights(secondRowFrames);
  }
  //#endregion

  //#region REPORTS
  function UpdateOwner(data) {
    if(data.length > 0) {
		// Setup variables
		var datavals 	= [];
		var datalabels 	= [];
		var datacolors 	= [];
		
		// Build a dataset for each year (each line is a separate dataset)
		for(var i = 0; i < data.length; i++) {
			datavals.push(data[i].num_of_opps);
			datalabels.push(data[i].assigned_to);	
			datacolors.push(cols[i]);	
		}
		
		
		var chartData = {
			datasets: [{
			  data: datavals,
			  backgroundColor: datacolors
			}],
			labels:datalabels
		};

      // load the chart
      var ctx = document.getElementById("OwnerPieChart").getContext('2d');
      OwnerPieChart = new Chart(ctx, {
        type: 'pie',
        data: chartData,
        options: {
          maintainAspectRatio: false,
          responsive: true,
          legend: {
            display: true
          },
          tooltips: {
            enabled: true
          }
        }
      });

      allCharts.push(OwnerPieChart);
    }
  }
  
    function UpdateSource(data) {
    if(data.length > 0) {
		// Setup variables
		var datavals 	= [];
		var datalabels	= [];
		var datacolors 	= [];
		var dtsets		= [];
		
		// Build a dataset for each status and each opportunity source
		var loopstatus 	= '';
		var statusid	= -1;
		for(var i = 0; i < data.length; i++) {
			// Get the source listing, this is the labels
			var thissource = data[i].opportunity_source
			if(!datalabels.includes(thissource)){
				datalabels.push(thissource);	
			}		
			
			// get the status listing
			var thisstat = data[i].status;
			if(loopstatus !== thisstat){
				loopstatus = thisstat
				statusid = statusid+1
				dtsets[statusid] = { 	
										label:loopstatus,
										backgroundColor:cols[statusid],
										data:[]
									}
			}
			
			// Current # of Opportunities
			var num_of_opps = data[i].num_of_opps
			dtsets[statusid].data.push(num_of_opps)
		}
		
		var chartData = {
			datasets: dtsets,
			labels:datalabels
		};

      // load the chart
	  var ctx = document.getElementById('SourceBarChart').getContext('2d');
		window.myBar = new Chart(ctx, {
			type: 'horizontalBar',
			data: chartData,
			options: {
				tooltips: {
					mode: 'index',
					intersect: false
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true
					}]
				}
			}
		});

      allCharts.push(SourceBarChart);
    }
  }
  
    function UpdateStage(data) {
    if(data.length > 0) {
		// Setup variables
		var datavals 	= [];
		var datalabels	= [];
		var dtsets		= [];
		
		// Build a dataset for each status and each opportunity source
		var loopstream 	= '';
		var istid	= -1;
		for(var i = 0; i < data.length; i++) {
			// Get the source listing, this is the labels
			var thisstatus = data[i].status
			if(!datalabels.includes(thisstatus)){
				datalabels.push(thisstatus);	
			}		
			
			// get the status listing
			var income_stream = data[i].income_stream;
			if(loopstream !== income_stream){
				loopstream = income_stream
				istid = istid+1
				dtsets[istid] = { 	
										label:loopstream,
										backgroundColor:cols[istid],
										data:[]
									}
			}
			
			// Current # of Opportunities
			var val_of_opps = data[i].val_of_opps
			dtsets[istid].data.push(val_of_opps)
		}
		
		var chartData = {
			datasets: dtsets,
			labels:datalabels
		};

      // load the chart
	  var ctx = document.getElementById('StageBarChart').getContext('2d');
		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: chartData,
			options: {
				tooltips: {
					mode: 'index',
					intersect: false
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true
					}]
				}
			}
		});

      allCharts.push(StageBarChart);
    }
  }


  function UpdateIncomeStream(data) {
    if(data.length > 0) {
		// Setup variables
		var datavals 	= [];
		var datalabels	= [];
		var datacolors 	= [];
		var dtsetstarget		= [];
		var dtsetsactual		= [];
		
		/*
		IncomeStreamGroup
		Target
		OppValue
		IncomeStreamValue
		*/
		
		// Build a dataset for each status and each opportunity source
		for(var i = 0; i < data.length; i++) {
			// Get the source listing, this is the labels
			var thisincomestream = data[i].IncomeStreamGroup
			datalabels.push(thisincomestream);	
			
			// Get the Targets
			var thistarget = data[i].Target
			dtsetstarget.push(thistarget.toFixed(0));	
			
			// Get the Actualy
			var thisactual = data[i].IncomeStreamValue
			dtsetsactual.push(thisactual.toFixed(0));	
		}
		
		var chartData = {
			datasets: [{
				label: 'Target',
				data:	dtsetstarget,
				backgroundColor:cols[0]//,
				//yAxisID: 'y-axis-1'
			},{
				label: 'Actual',
				data:	dtsetsactual,
				backgroundColor:cols[1]//,
				//yAxisID: 'y-axis-2'
			}],
			labels:datalabels
		};
/*
		var barChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: [
					window.chartColors.red,
					window.chartColors.orange,
					window.chartColors.yellow,
					window.chartColors.green,
					window.chartColors.blue,
					window.chartColors.purple,
					window.chartColors.red
				],
				yAxisID: 'y-axis-1',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 2',
				backgroundColor: window.chartColors.grey,
				yAxisID: 'y-axis-2',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
*/
      // load the chart
	  var ctx = document.getElementById('IncomeStreamBarChart').getContext('2d');
		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: chartData,
			options: {
				tooltips: {
					mode: 'index',
					intersect: false
				},
				responsive: true
			}
		});

      allCharts.push(IncomeStreamBarChart);
    }
  }
  //#endregion

  //#region HELPERS TODO: Most of these could be shared between multiple dashboards
  function GetSearchData(strToSearch, handleData) {
    // get the user
    var curusrid = $('#user-id').val();
    // Setup data for procedure call
    var requestData = {
      parameters: [
        { key: "@repID", value: curusrid },
        { key: "@searchString", value: strToSearch }
      ],
      sp_name: [{ key: "AAD_CustomerSearch" }]
    }

    // Call the SP with ajax and return the customer list
    $.ajax({
      method: "POST",
      url: base_url + "Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      success: function(data) {
        var custList = data.map((cust) => {
          return {
            label: cust.delivery_name,
            cid: cust.customer_id,
            aid: cust.address_id
          }
        });
        handleData(custList);
      }
    });
  }

  function DashboardRedirect(custID) {
    // Clear search (doesn't currently work, due to jqueryUI autocomplete shenanigans)
    //$("#form-customer-search").val("");
    // Redirect to selected customers dashboard
    window.location.href = base_url + "page/customer_dashboard?cid=" + custID;
  }

  function DashCallProcedure(frameID, procedure, callback) {

    // setup the URL for the AJAX call
    var url = method_url + procedure;

    // show the overlay
    ShowLoader(frameID);

    // Call the controller and method to get the data we need
    $.ajax({
      method: "POST",
      url:    url,
      data:   {
        userID: selectedID,
        startDate: startDate,
        endDate: endDate
      }
    }).success(function(data) {
      // do we want to debug the responses?
      if(debug) { console.log(data); }

      // attempt to parse the JSON string
      var jsonData = JSON.parse(data);

      // TODO: Will this ever error in success?
      // do we have any errors in the call?
      if(jsonData.HasError == true){
        FailCallback(frameID, jsonData.ErrorMessage);
        return;
      }

      // Run the callback function with our data set.
      callback(jsonData.Data);

      // hide the loading banner
      HideLoader(frameID);

    }).error(function(jqXHR, status, err) {
      // What do we do if an error occurs
      FailCallback(frameID, 'Procedure Call Failed: ' + status);
    });
  }

  function BuildFrameList() {
    firstRowFrames.push($("#repPromoSalesMain"));
    firstRowFrames.push($("#repActiveCustsMain"));
  }

  function AlignFrameHeights(frameList) {
    // Get the tallest content of each frame
    var maxHeight = Math.max.apply(Math, frameList.map(function(frame) {
      return frame.height()
    }));

    // Apply some extra padding
    maxHeight += 5;

    // Set all frames to have the max height
    frameList.forEach(function(e) {
      e.parent().height(maxHeight);
    });
  }

  function ShowLoader(frameID) {

    // show the overlay
    var overlay = "#loading_overlay_frame_" + frameID;

    // show the overlay
    $(overlay).fadeIn(500);
  }

  function HideLoader(frameID) {

    // show the overlay
    var overlay = "#loading_overlay_frame_" + frameID;

    // show the overlay
    $(overlay).fadeOut(500);

    // Update the error overlay text
    var errorOverlay = "#loading_overlay_frame_err_" + frameID;

    $(errorOverlay).text('');
  }

  function ShowError(frameID, err) {
    if(debug) { console.log(err); }

    // Update the error overlay text
    var errorOverlay = "#loading_overlay_frame_err_" + frameID;
    $(errorOverlay).text(err);

    // show the overlay
    ShowLoader(frameID);
  }

  function FailCallback(frameID, err) {
    ShowError(frameID, err);
  }

  function GetEndDate() {
    if($('#dateSelector').children(":selected").attr("id") == "param_date_ytd") {
      return $("#param-date-today").val();
    } else {
      var endDateVar = new Date(startDate);
      endDateVar = new Date(endDateVar.getFullYear(), endDateVar.getMonth() + 1, 0);

      return endDateVar.getFullYear() + '-' + (endDateVar.getMonth() + 1) + '-' + endDateVar.getDate();
    }
  }

  var waitForFinalEvent = (function() {
    // Resize as the layout changes
    var timers = {};
    return function (callback, ms, uniqueId) {
      if (!uniqueId) {
        uniqueId = "Don't call this twice without a uniqueId";
      }
      if (timers[uniqueId]) {
        clearTimeout (timers[uniqueId]);
      }
      timers[uniqueId] = setTimeout(callback, ms);
    };
  })();
  //#endregion

</script>
