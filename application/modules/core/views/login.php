<!DOCTYPE html>
<html>

<head>
  <title><?php echo $appTitle; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">

  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,100,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

  <!-- addede by kevin 26-03-2020 -->
  <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assets/mui-0.2.10/css/mui.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>core/assets/css/style.css">

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/mui-0.2.10/js/mui.min.js"></script>

  <script src="<?php echo base_url(); ?>core/assets/js/aes.js"></script>

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url(); ?>assets/core/images/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/core/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/core/images/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/core/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url(); ?>assets/core/images/favicon/manifest.json">

  <style type="text/css">
    <?php echo $cssOverride; ?>
  </style>

  <script type="text/javascript">
    var action = '<?php echo $action ?>'

    function validateForm(formId) {

      //var timezone_offset_minutes = new Date().getTimezoneOffset();
      // var tzname = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
      var tzname = Intl.DateTimeFormat().resolvedOptions().timeZone;
      // $("#usertimezone").val(tzname);
      // var timezone = tzname;
      //    '<%Session["timezone"] = "' + timezone + '"; %>';
      // alert('<%=Session["timezone"] %>');


      $("#usertimezone").val(tzname);

      var form = $("#" + formId);
      var fail = false;
      var fail_log = '';
      form.find('select, textarea, input').each(function() {

        if (!$(this).prop('required')) {} else {

          if (!$(this).val()) {
            fail = true;
            name = $(this).attr('name');

            fail_log += name + " is required \n";
            $("[name=" + name + "]").addClass('required_border_class');
            $("[name=" + name + "]").next('label').addClass('required_label_class');
          }


        }
      });

      if (fail) {
        return false;
      } else {
        //We just need to do this for the signin
        if (formId == 'form-signin') {
          var password = document.getElementById('password').value;
          var key = CryptoJS.enc.Hex.parse("0123456789abcdef0123456789abcdef");
          var iv = CryptoJS.enc.Hex.parse("abcdef9876543210abcdef9876543210");
          var hash = CryptoJS.AES.encrypt(password, key, {
            iv: iv
          });

          $("#password").val(hash);
        }

        $("#" + formId).submit(); // Submit the form
      }

    }


    $(document).ready(function() {
      $(".inputForm").keydown(function(e) {
        if (e.which == 13) {
          var formId = $(this).parents('form').attr('id');
          validateForm(formId);
        }
      });
      $(document).on('click', '#btn-login-reset-password', function() {
        $('.alert-success,.mui-danger-color').html('');
        $('#form-signin').hide('slow');
        $('#form-reset').show('slow');
      });
      $(document).on('click', '#btn-cancel', function() {
        $('#form-signin').show('slow');
        $('#form-reset').hide('slow');
        $('.alert-success,.mui-danger-color').html('');
      });
      //If the page loading is the reset page, then we should show the right form.
      if (action == 'reset') {
        $('#form-reset').show();
        $('#form-signin').hide();
      }

      if (navigator.geolocation) {

        window.onload = function() {
          var startPos;
          var geoOptions = {
            enableHighAccuracy: true,
            timeout: 10 * 1000,
            maximumAge: 5 * 60 * 1000,
          }

          var geoSuccess = function(position) {
            startPos = position;
            $("input[name='latitude']").val(startPos.coords.latitude);
            $("input[name='longitude']").val(startPos.coords.longitude);
          };
          var geoError = function(error) {
            console.log('Error occurred. Error code: ' + error.code);
            // error.code can be:
            //   0: unknown error
            //   1: permission denied
            //   2: position unavailable (error response from location provider)
            //   3: timed out
          };

          navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
        };
      } else {
        console.log("Geolocation is not supported by this browser!");
      }
    });
  </script>


</head>

<body>
  <div class="bsit_login">
    <div class="basit_logon_image">
      <!-- <img src="<?php echo base_url(); ?>assets/core/images/HeaderAboutUs.jpg"/> -->
    </div>
    <div id="loginPageBG" class="bsit_login_form">
      <div class="mui-panel" id="loginForm">
        <a href="#" target="_blank">
          <img id="loginLogoImg" src="<?php echo base_url(); ?>assets/core/images/login_logo.png" />
        </a>
        <br />
        <?php if (!empty($resetSuccessMessage)) : ?>
          <div class="alert-success">
            <?php echo $resetSuccessMessage ?>
          </div>
        <?php endif ?>
        <form id="form-signin" role="form" name="form-signin" method="POST" action="<?php echo base_url(); ?>" style="display:<?php echo ($action == 'reset') ? 'none' : 'block' ?>;">
          <div class="bsit_login_titel">
            <input type="hidden" name="timezone" id="usertimezone" value="">
            <input type="hidden" name="latitude" id="latitude" value="">
            <input type="hidden" name="longitude" id="longitude" value="">
            <h2>Hello there!</h2>
            <p>Login below to get started</p>
          </div>
          <legend id="loginFormTitle"></legend>
          <div class="mui-textfield bsit_form">
            <input type="text" name="username" class="inputForm" required/ placeholder="Username">
            <label>Username</label>
          </div>
          <div class="mui-textfield bsit_form">
            <input type="password" name="password" id="password" class="inputForm" required placeholder="Enter password" />
            <label>Password</label>
          </div>
          <?php $err = validation_errors(); ?>

          <?php
          if (!empty($err)) :
          ?>
            <div class="mui-danger-color bsit_ionvalid_sms">
              <?php echo validation_errors(); ?>
            </div>

          <?php
          endif;
          ?>
          <div class="mui-row">
            <div class="mui-col-lg-12 mui-col-md-12 mui-col-sm-12 mui-col-xs-12 bsit_login_btn">
              <button class="mui-btn mui-btn--primary mui-float-right mui--text-right" type="button" onclick="validateForm('form-signin');">SIGN IN</button>
            </div>
            <div class="mui-col-lg-12 mui-col-md-12 mui-col-sm-12 mui-col-xs-12 reset-password">
              <a class="mui-float-left login-reset-password" id="btn-login-reset-password">Forgot your Password?</a>
            </div>

          </div>
          <input type="hidden" name="redirect" value="<?php echo $this->input->get('redirect'); ?>">
        </form>
        <form id="form-reset" role="form" name="form-reset" method="POST" autocomplete="off" action="<?php echo base_url() . 'reset-request'; ?>" style="display: none">
          <input type="hidden" name="latitude" id="latitude" value="">
          <input type="hidden" name="longitude" id="longitude" value="">
          <div class="bsit_login_titel">
            <h2>Forgot your password?</h2>
            <p>Enter your email address and we’ll send you a reset link.</p>
          </div>
          <input type="text" id="email" name="email" autocomplete="new-password" value="" class="customFieldHoneyPot">
          <div class="mui-textfield bsit_form">
            <input type="text" name="username" class="inputForm" required placeholder="Username" />
            <label>Username</label>
          </div>
          <div class="bsit_login_btn">
            <button class="mui-btn mui-btn--primary" type="button" onclick="validateForm('form-reset');">Send</button>
            <button class="mui-btn bsit_back_home" type="button" id="btn-cancel">
              <span>
                < </span> Back to login page
            </button>
          </div>
        </form>

      </div>
    </div>
  </div>
</body>

</html>