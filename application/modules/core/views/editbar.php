<div id="editBar" class="edit-appbar mui-appbar mui-container-fluid mui--appbar-line-height">
  <div class="mui-container-fluid">
    <div class="mui-row" style="max-height: 64px;">

      <div class="edit-dropdown-div" style="display:none">
        <button id="addframe" class="edit-btn mui-btn" title="Frame">
          <img src="<?php echo base_url();?>assets/core/images/plus.png"
            style="display:inline-block; vertical-align:top;">
        </button>
      </div>

      <!-- EXIT -->
      <div style="float:right; padding-right:10px;">
        <button id="exitEditBtn" class="edit-btn mui-btn" style="vertical-align:text-bottom;">
          <img src="<?php echo base_url();?>assets/core/images/ic_exit_to_app_black_24dp_1x.png"
            style="vertical-align:top;">
        </button>
      </div>


    </div>
  </div>
</div>
