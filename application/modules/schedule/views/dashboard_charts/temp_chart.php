<div class="chart_main">
  <div class="chart_head">
    <div class="chart_title">TEMPERATURE - Live 24 Hours</div>
  </div>
  <div class="chart_body" id="temp_chart_container" style="height:400px; max-height:400px">
    <canvas id="myTempChart"></canvas>
  </div>
</div>
