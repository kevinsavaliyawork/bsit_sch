<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobview extends brain_controller {

	function __construct() {
		
	}

	function index($data_pass){


		return $this->load->view($data_pass['module_name'].'/mytask',$data_pass,true);
		
	}

	function supporttasks($data_pass){


		return $this->load->view($data_pass['module_name'].'/supporttasks',$data_pass,true);
		
	}
	
	function customersupport($data_pass){


		return $this->load->view($data_pass['module_name'].'/supporttasks',$data_pass,true);
		
	}
	
	function developmenttasks($data_pass){


		return $this->load->view($data_pass['module_name'].'/developmenttasks',$data_pass,true);
		
	}
	
	function report($data_pass){


		return $this->load->view($data_pass['module_name'].'/report',$data_pass,true);
		
	}
	
}