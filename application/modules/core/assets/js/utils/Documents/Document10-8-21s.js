import {getPageFrameIDByName,getPageByName, getCollectionIDByName, advMultiCollectionSourceNote, getFrameData } from '../../frames/TableView/TableView.js';
import { getFrameFromElement, updatePageFrameArray } from '../../Page.js';

var page_id = await getPageByName('Documents');
var pageFrameId = await getPageFrameIDByName(page_id);

let UploadCollectionID = await getCollectionIDByName('attached_documents');

let collectionID;
let recordID;
let pfid;
 
async function showUploadModal(event,type) {
$("#imgUploadBox").hide();
  $('#uploadDocs').css("display","block");
  $('#page_frame_'+pageFrameId).css("display","block");
  type = (type && type == "edit") ? 'edit' : 'add';
  let frame = getFrameFromElement(event.target);

  collectionID = $(event.currentTarget).data('collid');
  recordID = $(event.currentTarget).data('recid');

  
  $("#UploadCollectionID").val(UploadCollectionID);

  let ID = '';
  let noteList = '';
  if (type == 'edit') {
    /*$("#collection_id").val(collectionID);
    $("#row_id").val(recordID);*/
    ID = $(event.currentTarget).data('id');
  }
  pfid =frame.PFID;
  if (frame && frame.PFID) {
    let frameTitle = $(`#page_frame_${frame.PFID}_header h3`).text();
    if (frameTitle != '') {
      $('#bsit-upload-modal-header').html(`Documents - ${frameTitle}`);
    } else {
      $('#bsit-upload-modal-header').html(`Documents - ${page_title}`);
    }
  }
  //reset field
  resetAllFields();
  
  // Assign variables to the modal
  $('#bsit-upload-modal-container').attr('data-collectionID', collectionID);
  $('#bsit-upload-modal-container').attr('data-rowID', recordID);

  pageFrameId


  var text = '<div id="uploadDocs main_content_part'+ pageFrameId +' " class="main_content_part_class mui-col-xs-12 mui-col-xs-offset- tab-pane fade in " style="position:relative; height:Fillpx; margin-bottom:20px;" data-frametitle="" data-framewidth="12" data-frameheight="Fill" data-frameoffset="" data-frameorder="2" data-frameid="'+ pageFrameId +'" pageid="97" collection_id="109" frameid="'+ pageFrameId +'" framename="documentsFrame" data-framehideadd="" data-framehidefilter="" data-framehidepagination="" data-framehidedownload="" data-framehidemultiedit="" data-framehidemultidelete="" style="display:none">';
  text += '<input type="hidden" id="fram_prop_filter_'+ pageFrameId +'" value="">';
  text += '<input type="hidden" id="fram_prop_url_'+ pageFrameId +'" value="">';
  text += '<input type="hidden" id="page_id_'+ pageFrameId +'" value="">';
  text += '<input type="hidden" id="default_page_size_'+ pageFrameId +'" value="10">';
  text += '<span class="bsit_form_titel"><i class="fa fa-tasks" aria-hidden="true" style="margin-right: 15px;"></i>Document List</span>';
  
  text += '<div class="bsit-page-frame edit-pointer-cancel table-responsive mui-panel" id="page_frame_'+ pageFrameId +'" style="min-height: 30vh;margin-top: 10px;border-top: 2px solid #bbb8b8; adding-top: 20px;height:Fillpx;padding: 0px!important;margin-bottom: 20px!important;box-shadow: none!important;" data-pfid="'+ pageFrameId +'"  style="display:none"></div></div>';
  
  // $('#bsit-upload-container').show();
  $('#bsit-document-modal').html(text);

  updatePageFrameArray();
 
  // Show the modal
  $('#bsit-upload-modal').modal();
   onclickClose();
   if (type == 'add') {
        $("#collection_id").val(collectionID);
        $("#row_id").val(recordID);
    $(".uploadbutton").show();
    $(".cola_table-responsive").show();
     getDocuments(collectionID, recordID);
  }
}

// Reset All fields
function resetAllFields() {

 
  $('#fram_prop_url_'+pageFrameId).val('');
  $('#page_id_'+pageFrameId).val('');
  $("#document_type_code").removeClass("required_border_class");
  $("#doc_url").removeClass("required_border_class");
  $("#fileuploadmultiple").closest("div").removeClass("required_border_class");
  //$('#uploadPreview').html(''); 
  $("#image_file").val('');
  $('#doc_keywords').val('');
  $('#doc_description').val('');
  $('#_id').val('');
  $('#old_file_name').val('');
  $('#old_file_location').val('');
  $('#old_file_size_kb').val('');
  $('#old_file_type').val('');
  $("#old_collection_id").val('');
  $('#old_row_id').val('');
  $("#doc_url").val('');
}


function getDocumentsData(collectionID, recordID, ID) {
 
 $("#document_type_code").attr("disabled", "disabled"); 
 $(".uploadbutton").hide();
 $(".cola_table-responsive").hide();
 $('.loader').show();
 var _id = ID;
 let imageData = $('#image_file').val();
 $("#image_file_outer").removeClass("required_border_class");
 

  resetAllFields();
  let requestData = {
    data: JSON.stringify([{
      collID: UploadCollectionID,
      filter: `bsd._id = ${ID} `,// AND bsd.row_id = ${recordID} AND bsd.collection_id = ${collectionID}
    }]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/loadfiltered',
    data: requestData,
    dataType: 'json',
    success: function (res) {
      $('.loader').hide();
      
      if (res && typeof res[0]['Results'][0] != 'undefined') {
          let data = res[0]['Results'][0];
          $("#_id").val(data._id);
          $("#doc_url").val(data.doc_url);
          let str = data.document_type_code;
          //$('#document_type_code').val(str.toLowerCase()).prop('selected', true);
          $('#document_type_code').val(str).prop('selected', true);
          $('#document_type_code').css('textTransform', 'capitalize');
         if(str.toLowerCase()  == "url") 
          {
            $("#edit").hide();
            $("#uploadFileCount").html('');
            $("#doc_url_div").show();
            $("#imgUploadBox").hide();
          } else {
            $("#doc_url_div").hide();
            $("#doc_url").val('');
            $("#imgUploadBox").show();
            $("#edit").show();
          }
          $("#collection_id").val(data.collection_id);
          $("#row_id").val(data.row_id);

          $("#doc_keywords").val(data.key_words);
          $("#doc_description").val(data.description);
          $("#old_file_location").val(data.file_location);
          $("#old_file_name").val(data.file_name);
          $("#old_file_size_kb").val(data.file_size_kb);
          $("#old_file_type").val(data.file_type);
          $("#old_collection_id").val(data.collection_id);
          $("#old_row_id").val(data.row_id);
          
        }
    }
  });
}


$( document ).ajaxComplete(function( event, xhr, settings ) {

  // after add   
  if ( settings.url === base_url+"core/Frame/load_content" )
  {
    $('#page_frame_'+pageFrameId).html(xhr.responseText);
  }

  // after add   
  if ( settings.url === base_url+"core/Apilocal/loadsingle" )
  {
    var data=xhr.responseText;
    var jsonResponse = JSON.parse(data);
    if(jsonResponse[0]["Results"]){
      $('#document_key_id').val(jsonResponse[0]["Results"][0]["key_id"]);
      $('#document_key_update_date').val(jsonResponse[0]["Results"][0]["key_update_date"]);
      $('#document_key_update_by').val(jsonResponse[0]["Results"][0]["key_update_by"]);
    }
  }
  
 
});


// get all documents
function getDocuments(collectionID, recordID) {
  resetAllFields();
  var url1="?job_id="+recordID+"&collid="+collectionID+"";
  var fram_prop_filter="bsd.row_id="+recordID+" AND bsd.collection_id="+collectionID+"";
    $("#fram_prop_filter_"+pageFrameId).val(fram_prop_filter);
    $("#fram_prop_url_"+pageFrameId).val(url1);
    $("#page_id_"+pageFrameId).val(page_id);
  // load frame data   
  var res = getFrameData(getFrameFromElement('', pageFrameId),0);
  return;
}

// Showing notes modal
$(document)
  .button()
  .on('click', '.bsit-upload-btn', function (e) {
    $("#edit").hide();
    $('#document_type_code').removeAttr("disabled")
    showUploadModal(e,"add");
});

 // Showing edit notes modal
$(document)
  .on('click', '.bsit-edit-upload-btn', function (e) {
    showUploadModal(e, "edit");
}); 

//edit Documents
$(document).on('click', 'a.edit-del-btn-'+pageFrameId+'.bsit-edit-btn', function (e) {  
  showUploadModal(e, "edit");
  var id = $(this).attr('data-recid');
  getDocumentsData(collectionID, recordID, id);
});



//Delete  Documents
$(document).on('click', 'a.edit-del-btn-'+pageFrameId+'.bsit-delete-btn', function (e) {  
  $('.loader').show();
  createModalDeleteDocuments();
  $('#ModalDeleteDocuments'+pageFrameId).modal();
  $('#bsit-upload-modal').modal('hide');
  $('.loader').hide();
});


// Deleting records using delete modal
$(document)
  .button()
  .on('click', '.bsit-commit-deletedoc', function(e) {
    commitDeleteDoc();
  });

var createModalDeleteDocuments = function () {
var divModal = '<div id="ModalDeleteDocuments" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">';
    divModal += '<div class="modal-dialog bsit_upload_popup" style="height: 100%; width:850px;">';
    divModal += '<div class="modal-content" style="float: left; width: 100%;">';
    divModal += '<div class="modal-header" style="padding-top: 0px; padding-bottom: 0px;">';
        divModal += '<button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">';
          divModal += '<svg class="" viewBox="0 0 24 24">';
          divModal += '<path fill="#000000"  d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z">';
          divModal += '</path>';
          divModal += '<path d="M0 0h24v24h-24z" fill="none"></path>';
          divModal += '</svg>';
        divModal += '</button>';
      divModal += '<h2 id="bsit-delete-modal-header" class="bsit_form_titel">Delete Documents</h2>';
    divModal += '</div>';
  divModal += '<div class="modal-body">';
  divModal += '<form method="post" class="mui-form bsit_form" autocomplete="off" id="deleteFormDocument'+pageFrameId+'" data-collid="'+UploadCollectionID+'">';
    divModal += '<input type="hidden" name="key_id" id="document_key_id" value=""/>';
    divModal += '<input type="hidden" name="key_update_date" id="document_key_update_date" value=""/>';
    divModal += '<input type="hidden" name="key_update_by" id="document_key_update_by" value=""/>';
    divModal += '<span class="bsit_side_nave_blank_title">Are you sure you would like to delete this?</span>';
  divModal += '</form>';
  divModal += '<div class="text-right" style="text-align:center;">';
  divModal += '<button id="commit-deleteDoc-'+pageFrameId+'"  class="bsit-commit-deletedoc mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Yes, Delete</button>';
  divModal += '<button class="mui-btn bsit-cancel-delete mui-btn--flat mui-btn--primary bsit_cancel_btn" data-dismiss="modal">No</button>';
  divModal += '</div>';
  divModal +='</div>';
  $('body').append(divModal);
                          
  $('#bsit-upload-modal').modal('hide');
  $('#ModalDeleteDocuments').modal();
}


// Delete records function
function commitDeleteDoc() {
  let form = $('#deleteFormDocument' + pageFrameId);
  let values = form.serializeObject();
  let collectionID = form.attr('data-collid');

  let data = Object.assign({
    colls: [{
      coll_id: collectionID,
      values: values
    }],
  });

  let requestData = {
    data: JSON.stringify(data),
  };

  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/delete',
    data: requestData,
    success: function (res) {
      $('#ModalDeleteDocuments').modal('hide');
      Materialize.toast('Delete document successfully!', 5000);
    },
    error: function (err) {

      if (err.status == 404) {
        err.responseText = 'The requested url was not found on this server.';
      } else if (err.status == 408) {
        err.responseText = 'API request timeout';
      } else if (err.status == 500 || err.status == 503) {
        err.responseText = 'Service Unavailable';
      }

      alert(err.responseText);
    },
  });
}


function onclickClose() {
  $(`.modal-backdrop`).on('click', function (e) {
     $('#uploadDocs').css("display","none");
  $('#page_frame_152').css("display","none");
    e.preventDefault();
    e.stopPropagation();
    return false;
  });
}


function uploadFiles() {
  $("#maxFileError").hide();
  $("#fileuploadmultiple").closest("div").removeClass("required_border_class");
  $("#startuploading").click();
}


var file_upload_url = base_url + "core/document/documentfileuploadDoc";



  $('#fileuploadoc').bind('fileuploadsubmit', function (e, data) {



    var document_type_code = $('#document_type_code');
    var keywords = $('#doc_keywords');
    var description = $('#doc_description');
    var doc_url = $("#doc_url");

    var collID = $('#bsit-upload-modal-container').attr('data-collectionID');
    var rowID =  $('#bsit-upload-modal-container').attr('data-rowID');

    var collection_id = $("#old_collection_id").val();
    var row_id = $("#old_row_id").val();

  // alert(collID);



    var _id = $('#_id').val();
    var colid=  '';
    var rowid=  '';   
      if(_id != ''){
        rowid=row_id;
        colid=collection_id;
       }else{
        rowid=rowID;
        colid=collID;
                   
      }

 
            data.formData = {
              collection_id: colid,
              row_id: rowid,
              UploadCollectionID:UploadCollectionID,
              document_type_code: document_type_code.val(),
              keywords: keywords.val(),
              description: description.val(),
              doc_url: doc_url.val(),
              //doc_folder: doc_folder.val(),
            };

            console.log( data.formData);
    //doc type validation
    if (!data.formData.document_type_code) {
      data.context.find('button').prop('disabled', false);
      document_type_code.focus();
       //alert("Please insert type.")
      $("#document_type_code").addClass("required_border_class");
      return false;
    }

    //url validation
    if (document_type_code.val() && document_type_code.val().toLowerCase() == 'url' && !data.formData.doc_url) {
      data.context.find('button').prop('disabled', false);
      doc_url.focus();
    //  alert("Please insert type.")
      $("#doc_url").addClass("required_border_class");
      return false;
    }

  });

$(document).on("click", "#checkValidation", function () {
  var fileCount = $('#uploadFileCount tr').length;
  var documentcheckType = $("#document_type_code").val();
  let doc_url = $("#doc_url").val();
  let collID = $('#bsit-upload-modal-container').attr('data-collectionID');
  let rowID = $('#bsit-upload-modal-container').attr('data-rowID');
  var count =0;

  var id = $("#_id").val();
 
 /* if (!$("#doc_job_id").val()) {
    alert("Job ID is missing.");
    return false;
  }*/

  //doc type validation
  if (!documentcheckType) {
    $("#document_type_code").focus();
    $("#document_type_code").addClass("required_border_class");
    return false;
  }

  if (documentcheckType && documentcheckType.toLowerCase() == 'url') {
    //url validation
    if (doc_url == '') {
      $("#doc_url").focus();
      $("#doc_url").addClass("required_border_class");
      return false;
    } else{

    if( id > 0 ) {
      uploadFileFromUrl();
    }else if(doc_url != '')
    {
      uploadFileFromUrl();
    }

    
  } 

}
else {
    if (fileCount == 0 && id == '' ) {
      $("#fileuploadmultiple").closest("div").addClass("required_border_class");
      return false;
    }
    else{  if (fileCount == 0 && id > 0 ) {
         uploadFileFromUrl();
      }
      
    }
  }
  count = $("#uploadFileCount").find("tr").length;

  //max file limit validation
  if (count > 20) {
    $("#maxFileError").show();
    return false;
  }
  
  uploadFiles();
});


function uploadFileFromUrl() {
  
  let document_type_code = $('#document_type_code');
  let keywords = $('#doc_keywords');
  let description = $('#doc_description');
  let doc_url = $("#doc_url");
  //let doc_folder = $("#doc_folder");
  
  var collID = $('#bsit-upload-modal-container').attr('data-collectionID');
  var rowID = $('#bsit-upload-modal-container').attr('data-rowID');
  var _id = $('#_id').val();
  var collection_id = $("#old_collection_id").val();
  var row_id = $("#old_row_id").val();
  var  formData;
      if(_id > 0 && _id != ''){
        var file_location = $("#old_file_location").val();
        var file_name = $("#old_file_name").val();
        var file_size_kb = $("#old_file_size_kb").val();
        var file_type = $("#old_file_type").val();
      //  var collection_id = $("#old_collection_id").val();
        //var row_id = $("#old_row_id").val();
           formData = {
              collection_id: collection_id,
              row_id: row_id,
              UploadCollectionID:UploadCollectionID,
              document_type_code: document_type_code.val(),
              keywords: keywords.val(),
              description: description.val(),
              doc_url: doc_url.val(),
              file_location:file_location,
              file_name : file_name,
              file_size_kb :file_size_kb,
              file_type : file_type,
              _id:_id
              //doc_folder: doc_folder.val(),
            };

       }else{
         formData = {
              collection_id : collID,
              UploadCollectionID: UploadCollectionID,
              row_id: rowID,
              document_type_code: document_type_code.val(),
              keywords: keywords.val(),
              description: description.val(),
              doc_url: doc_url.val(),
              _id:_id
             //doc_folder: doc_folder.val(),
            };
       } 
      

  // var files = $("#fileuploadmultipal").val();
  /*let formData = {
    collection_id : collId,
    UploadCollectionID: UploadCollectionID,
    row_id: rowId,
    document_type_code: document_type_code.val(),
    keywords: keywords.val(),
    description: description.val(),
    doc_url: doc_url.val(),
    _id:_id,
  };
  */
  $.ajax({
    type: 'POST',
    url: file_upload_url,
    data: formData,
    dataType: 'json',
    success: function (res) {
       if(_id != ''){
        $("#document_type_code").removeAttr("disabled", "disabled");
        $("#edit").hide();
        $('#uploadDocs').css("display","none");
        
        var collection_id = $("#old_collection_id").val();
        var row_id = $("#old_row_id").val();

        $('#page_frame_'+pageFrameId).css("display","block");
        //$('#page_frame_'+pageFrameId).html('');

        Materialize.toast('All file Data Updated successfully!', 5000);
        getDocuments(collection_id, row_id);
      }else{
         var collection_id1 = $("#collection_id").val();
         var row_id1 = $("#row_id").val();
        var collID = $('#bsit-upload-modal-container').attr('data-collectionID');
        var rowID = $('#bsit-upload-modal-container').attr('data-rowID');
     
        //$('#page_frame_'+pageFrameId).html('');
        Materialize.toast('All files uploaded successfully!', 5000);
        getDocuments(collection_id1, row_id1);
      }

      resetUploadFields();
     
    },
    error: (err) => {
      alert("Error while uploading file from URL.");
    },
  });
}


// show/ hide on change


$( "#document_type_code" ).change(function() {
  var val = $("#document_type_code option").filter(":selected").val();
  showUrlField(val);
});

function showUrlField(val) {
  if (val && val.toLowerCase() == "url") {
    let el = $("#uploadFileCount").find("tr");
    $(el).each(function() {
      $( this ).find( ".cancel" ).trigger("click");
    });
    $("#uploadFileCount").html('');
    $("#doc_url_div").show();
    $("#imgUploadBox").hide();
    
  } else {
    $("#doc_url_div").hide();
    $("#doc_url").val('');
    $("#imgUploadBox").show();
  }
}
      
$(function () {
  'use strict';
 var fileCount = $('#uploadFileCount tr').length;
 
 //if( fileCount > 0) {
  // Initialize the jQuery File Upload widget:
  $('#fileuploadDoc').fileupload({
    url: file_upload_url,
    limitMultiFileUploads: 3,
    limitConcurrentUploads: 3,
    // url: 'server/php/'
  }).on('fileuploadprogress', function (e, data) {
	
	$('.uploadmsg').hide();
    $('.progress').show();
    
	var progress = parseInt(data.loaded / data.total * 100, 10);
    if (progress == 100) {
      let td = data.context.find("td:last-child");
      td.find("button.cancel").hide();
      td.find("button.success").show();
    }

  }).on('fileuploadprogressall', function (e, data) {
    var progress_all = parseInt(data.loaded / data.total * 100, 10);
    //console.log(progress_all)


    if (progress_all == 100) {
      //Materialize.toast('All files uploaded successfully!', 5000);
    }
    
  }).on('fileuploaddone', function (e, data) {
    let _id = $('#_id').val();
    if ($('#uploadFileCount tr').length == 1){
	  if(_id != ''){
        var collection_id = $("#old_collection_id").val();
        var row_id = $("#old_row_id").val();

        $('#page_frame_'+pageFrameId).css("display","none");
       // $('#page_frame_'+pageFrameId).html('');

        Materialize.toast('All file Data Updated successfully!', 5000);
        getDocuments(collection_id, row_id);
      }else{
		var collection_id1 = $("#collection_id").val();
		var row_id1 = $("#row_id").val();
        
		var collID = $('#bsit-upload-modal-container').attr('data-collectionID');
        var rowID = $('#bsit-upload-modal-container').attr('data-rowID');
     
        //$('#page_frame_'+pageFrameId).html('');
        Materialize.toast('File uploaded successfully!', 5000);
        getDocuments(collection_id1, row_id1);
      }
	 }
    resetUploadFields();
    /*if ($('#uploadFileCount tr').length <= 1) {
      let collID = $('#bsit-upload-modal-container').attr('data-collectionID');
      let rowID = $('#bsit-upload-modal-container').attr('data-rowID');
      let _id = $('#_id').val();
      resetUploadFields();
      if(_id != ''){
        $('#uploadDocs').css("display","none");
        $('#page_frame_'+pageFrameId).css("display","none");
        $('#page_frame_'+pageFrameId).html('');
        $('.modal').modal('hide');
        Materialize.toast('All file Data Updated successfully!', 5000);
      }else{
        $('#page_frame_'+pageFrameId).html('');
        Materialize.toast('All files uploaded successfully!', 5000);
        getDocuments(collID, rowID );
      }
    }*/
  });


  if (window.location.hostname === 'blueimp.github.io') {
    // Demo settings:
    
  } else {
    // Load existing files:
    $('#fileuploadDoc').addClass('fileupload-processing');

    $.ajax({
      // Uncomment the following to send cross-domain cookies:
      //xhrFields: {withCredentials: true},
      url: $('#fileuploadDoc').fileupload('option', 'url'),
      dataType: 'json',
      context: $('#fileuploadDoc')[0]
    })
      .always(function () {
        $(this).removeClass('fileupload-processing');
      })
      .done(function (result) {
        $(this)
          .fileupload('option', 'done')
          // eslint-disable-next-line new-cap
          .call(this, $.Event('done'), {
            result: result
          });
      });
  }


 //}
  


  
});



