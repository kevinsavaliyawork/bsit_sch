<script src="<?php echo base_url();?>core/assets/js/Page.js" type="module"></script>
<style>
.table-responsive .first {

  display: none !important;

}</style>
 <?php
  // ------------------------------
  // -- CREATE TABLEVIEW CACHE FILE
  // ------------------------------
  // Its Inclue : Display the Content(Data) of Files

  $cacheDir = APPPATH."views/core/cache/";
  $linkopen = "";
  $html = "";
  $html_pagi = "";
  $html_sort = "";

/* echo "<pre>";
  print_r($frame);
  echo "</pre>";
  echo $frame['allow_view'];
  */
  // $abc = '<img src="'.base_url().'core/assets/image/aaedit.png" id="">';
  // echo $abc;
  
  $fullPath = $frame['fullPath_content'];
  $viewPath = $frame['viewPath_content'];
  $html .= '<div id="page_frame_'.$frame['PFID'].'_content" class="call_content_div" style="display: flex; flex-flow: column;">';
  $html .= '<div class="main_table_start">'; //bsit_table_scroll_fancy
  $html .= '<table id="table" class="table bsit_page_table_listing table-hover table-mc-light-blue mdl-data-table1 table_'.$frame['PFID'].'" style="white-space: normal;">';
  $html .= '<input type="hidden" name="key_id_frame_sidebar" id="frame_id_'.$frame['PFID'].'"  frame_val="'.$frame['PFID'].'" value="'.$frame['Properties'][0]->enable_pane_view.'"/>';

  $html .= '<thead>';
  $html .= '<tr>';// header row
  $html .= '<input type="hidden" id="frame_order" value='.$frame['order'].'>';
  // $allow_quick_filter_html .= '<li>displayProperties</li>';
  if ((((isset($frame['allow_multiple_edit'])) || (isset($frame['allow_multiple_delete']))) && 
    ($frame['allow_multiple_edit'] == '1' || $frame['allow_multiple_delete'] == '1')) ||
   ($frame['Properties'][0]->AllowEdit == '1' || $frame['Properties'][0]->AllowDelete == '1'|| $frame['Properties'][0]->AllowNotes == '1'))
  {
          $html .= '<th class="action_div first" ></th>';
  } 
  
  $total_property = '0';
  
  $frame_filter_data_value = $frame_filter_data;
  
    preg_match_all("/\[[^\]]*\]/", $frame_filter_data_value, $matches);
    foreach ($matches as $mat) 
    {
      $value_comapre = str_replace(array( '[', ']' ), '', $mat);
    }
     //print_r($value_comapre);
     
 $view = 0;
  foreach ($frame['Properties'] as $prop) 
  { 
    //echo "<pre/>";
        // print_r($prop); 
      /* code start for remove password from listing Added by HD*/
      if ($prop->PropEditType == 'PWD') 
      {
          continue;
      }
      /* code end for remove password from listing Added by HD*/

      $total_property++;
      $colWidth = '';
      //$colWidth = ' style="width:90px;"';
      $Clswidth = 'Clswdt';
      if ($prop->TableColWidth == 0) 
      {
          $colWidth = ' style="display:none;"';
          $Clswidth = '';
      }

      if ((isset($frame['allow_edit'])) || (isset($frame['allow_delete'])) || (isset($frame['allow_notes'])) || (isset($frame['allow_multiple_edit']))   || (isset($frame['allow_multiple_delete']))) 
      {
          if (($colWidth == '') && ($frame['allow_edit'] == '1' || $frame['allow_delete'] == '1' || $frame['allow_notes'] == '1' || $frame['allow_multiple_edit'] == '1' || $frame['allow_multiple_delete'] == '1')) 
          {
              //$html1 = '<style>.table tbody > tr > td:nth-child(3){padding-left: 125px  !important;} .table thead > tr > th:nth-child(4){padding-left: 125px  !important;}</style>';
          } else {
              $html1 = '';
          }
      } else {
          if ($colWidth == '' && ($prop->AllowEdit == '1' || $prop->AllowDelete == '1' || $prop->AllowNotes == '1')) 
          {
              //$html1 = '<style>.table tbody > tr > td:nth-child(3){padding-left: 125px  !important;} .table thead > tr > th:nth-child(4){padding-left: 125px  !important;}</style>';
          } else {
              $html1 = '';
          }
      }

      if (($prop->DefaultSortField == $prop->PropId)) 
      {
          if ($prop->DefaultSortType == 'asc') 
          {
              $default_sort_status = '&nbsp;↑' ;
          } else {
              $default_sort_status = '&nbsp;↓' ;
          }
      } else {
          $default_sort_status = '';
      }

     

      if (in_array($prop->PropColumn, $value_comapre )) 
        { 
            $add_filter_class = 'bsit_active_filters';
        } 
      else
        { 
            $add_filter_class = '';
        } 

      if ($prop->PropEditType != 'FILE') 
        {
        
          if ( $prop->allow_quick_filter == 1)
          {
            $quick_filter_text_box = "<span class='icon-search quick_filter_text_box_icon'></span><input type='text' id='bsit_quick_filter_text_".$prop->FramePropID."' name='quick_filter_text_box' class='quick_filter_text_box'>";
            $quick_filter = "bsit_allow_quick_filter_show";
            if($prop->AllowSearch == 0 || $prop->TableColWidth == 0)
            {
               $quick_filter = '';
               $quick_filter_text_box = '';
            }
        } else
          {
            $quick_filter = '';
            $quick_filter_text_box = '';
          }
        
          //echo "<pre/"; print_R($prop->PropName);
          if(($prop->PropName == 'scheduleHrsTotal' ) || ($prop->PropName == 'estimatedHours' ) || ($prop->PropName == 'actual_TotalHours')  )
          {
         
           if($view == 0){
            $html .= ' <th class="progwidth">
                <ul class="bsit_est_sch_act_section">
                <li><span class="est"> </span><label class="bsit_est_sch_act">EST Hrs </label></li>
                <li><span class="sch"> </span><label class="bsit_est_sch_act">SCH Hrs </label></li>
                <li><span class="act"> </span><label class="bsit_est_sch_act">ACTL Hrs </label></li>
                </ul>
            </th>'; 
            $view = 1;
           }
           
         
          }
          else{
           $html .= '<th'.$colWidth.' class=" pl_50 short-data-'.$frame['PFID'].' '.$Clswidth.' '.$add_filter_class.'" id="short-'.$prop->PropName.'" data-sort="asc"><span class ="bsit-sort-header short_data_'.$frame['PFID'].''.$prop->PropName.'" id="short-'.$prop->PropName.'" data-sort="asc">'.$prop->PropTitle.'</span><span class="sort_arrow">'.$default_sort_status.'</span><span class="bsit_allow_quick_filter '.$quick_filter.'" data-framepropertyid="'.$prop->FramePropID.'"><i class="fa fa-filter" aria-hidden="true"></i></span><ul id="quick_filter_ul_'.$prop->FramePropID.'" class="bsit_allow_quick_filter_ul" field="'.$prop->Alias.'.['.$prop->PropColumn.']">'.$quick_filter_text_box.' </ul></th>';
          }
         
           
           $html .= '<input type="hidden" id="short_data_'.$prop->PropName.'" value='.$prop->PropTitle.'>';

          
      }
  }
      

  if ((((isset($frame['allow_edit'])) || (isset($frame['allow_delete'])) || (isset($frame['allow_notes']))) && ($frame['allow_edit'] == '1' || $frame['allow_delete'] == '1' || $frame['allow_notes'] == '1')) || 
     ($frame['Properties'][0]->AllowEdit == '1' || $frame['Properties'][0]->AllowDelete == '1'|| $frame['Properties'][0]->AllowNotes == '1'))
  {
          $html .= '<th class="action_div second" ></th>';
  } else {
          $html .= '<th class="action_div second" ></th>';
  }
  $html .= '</tr></thead>';
  $html .= $html1;
  $html .= '<tbody>'; // Content rows

  
  if (!empty($load_data)) 
  { 
      foreach ($load_data as $frow) 
      {
          $html .= '<tr class="changeme">';
          // BUTTONS - Edit & Delete & Nots
          if ((((isset($frame['allow_edit'])) || (isset($frame['allow_delete'])) || (isset($frame['allow_notes'])) || (isset($frame['allow_multiple_edit'])) || (isset($frame['allow_multiple_delete']))) &&    ($frame['allow_edit'] == '1' || $frame['allow_delete'] == '1' || $frame['allow_notes'] == '1' || $frame['allow_multiple_edit'] == '1' || $frame['allow_multiple_delete'] == '1')) ||
            ($frame['Properties'][0]->AllowEdit == '1' || $frame['Properties'][0]->AllowDelete == '1' || $frame['Properties'][0]->AllowNotes == '1'))
          {
                  $html .= '<td class="first">';
          } else {
                  $html .= '<td class="first">';
          }
          
          $html .='<input type="hidden" id="testdemo" class="has_more_'.$frow->key_id.'" value="'.$frow->key_id.'">';
          if (isset($frame['allow_multiple_edit']) || isset($frame['allow_multiple_delete'])) 
          {
              if ($frame['allow_multiple_edit'] == '1' || $frame['allow_multiple_delete'] == '1') 
              {
                  $html .= '<a class="multi-edit-chk-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="multi_edit">
                      <div class="BSIT-TableActions edit_multi_checkbox_div" id="edit_multi_checkbox_div">
                      <label class="bsit_check">
                      <input id="edit_multi_id_'.$frow->key_id.'" value="'.$frow->key_id.'" type="checkbox" name="chkbox[]" class="edit_multi_checkbox edit_multi_checkbox_'.$frame['PFID'].'">
                      <span class="bsit_checkmark"></span>
                      </label>
                    </div>
                    </a>';
              }
          } else {
              if (($frame['Properties'][0]->allow_multiple_edit == '1' && $frame['Properties'][0]->hide_multiple_edit == '0') || ($frame['Properties'][0]->allow_multiple_delete == '1' && $frame['Properties'][0]->hide_multiple_delete == '0')) {
                  $html .= '<a class="multi-edit-chk-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'"data-recid="'.$frow->key_id.'" data-modal="multi_edit">
                    <div class="BSIT-TableActions edit_multi_checkbox_div">
                    <label class="bsit_check">
                    <input id="edit_multi_id_'.$frow->key_id.'" value="'.$frow->key_id.'" type="checkbox" name="chkbox[]" class="edit_multi_checkbox edit_multi_checkbox_'.$frame['PFID'].'">
                    <span class="bsit_checkmark"></span>
                    </label>
                  </div>
                  </a>';
              }
          }

          if ((((isset($frame['allow_edit'])) || (isset($frame['allow_delete'])) || (isset($frame['allow_notes']))) && 
            ($frame['allow_edit'] == '1' || $frame['allow_delete'] == '1' || $frame['allow_notes'] == '1')) || ($frame['Properties'][0]->AllowEdit == '1' || $frame['Properties'][0]->AllowDelete == '1' || $frame['Properties'][0]->AllowNotes == '1')) 
          {
                  $html .= '</td>';
          } else {
                  $html .= '</td>';
          }
          // LOOP FOR DISPLAYING PROPERTY CONTENT
          
          $dataview = $dataviewcl = 0;
          
          foreach ($frame['Properties'] as $prop) 
          {
              if ($prop->PropEditType == 'PWD') 
              {
                  continue;
              }
              $colWidth = ' ';
              $Clswidth = 'Clswdt';
              if ($prop->TableColWidth == 0) 
              {
                  $colWidth = ' style="display:none;"';
                  $Clswidth = '';
              }
              $prname = $prop->PropName;
              $default_value = $prop->default_value;
              $total_count = $prop->total_count;
//echo "<pre/>";print_R($frame);
                

              if ($prop->PropEditType != 'FILE') {
                  if(($prop->PropName == 'scheduleHrsTotal' ) || ($prop->PropName == 'estimatedHours' ) || ($prop->PropName == 'actual_TotalHours')  )
                  {

                   if($dataviewcl == 0){
                      $html .= '<td class="other_td pl_50 '.$Clswidth.' " '.$colWidth.'>';
                    }
                  }
                  else{
                    $html .= '<td class="other_td pl_50 '.$Clswidth.' " '.$colWidth.'>';
                  }
                  if ($frame['editURL'] <> "") {
                    
                  
                    if ($frame['editURL'] == 'collectionmanager') {
                        $linkopen = '<a class="next_page_' . $frame['PFID'] . ' next_page_all" data-current_frame_id="' . $frame['PFID'] . '" href="' . base_url() . $frame['editURL'] . '?';
                    } else {
                        $urlData = json_decode($frame['editURL']);
                        $frameurl = (!empty($urlData->url)) ? $urlData->url : $frame['editURL'];
                        $target = (!empty($urlData->target)) ? $urlData->target : '';
                        $linkopen = '<a class="next_page_' . $frame['PFID'] . ' next_page_all" data-current_frame_id="' . $frame['PFID'] . '" target="'.$target.'" href="' . $frameurl . '?';
                    }

                    foreach ($frame['editParms'] as $parm) {
                        $key = explode(':', $parm);
                        $parm = (isset($key[1]) && !empty($key[1])) ? $key[1] : $key[0];
                        $linkopen .= $parm . '=' . $frow->{$key[0]} . '&';
                    }

                      $linkopen .= '">';
                      if ($prop->PropEditType == 'COLL') {
                          $html .= '<input type="hidden" id="'.$prname.'" value="'.$frow->$prname.'">';
                      }
 // progress bar start from here

                      $largest =0;
      
      $largest=max($frow->actual_TotalHours,$frow->estimatedHours,$frow->scheduleHrsTotal);
      $smallest=min($frow->actual_TotalHours,$frow->estimatedHours,$frow->scheduleHrsTotal);
     
      
     if($largest == ''){
        $largest =0;
      }
      if($largest == $frow->scheduleHrsTotal)
      {
        //$max1 = max((int) $frow->actual_TotalHours*100/$largest,(int) $frow->estimatedHours*100/$largest);
        $act= round(($frow->actual_TotalHours * 100) / $largest, 2); 
        $est= round(($frow->estimatedHours*100) / $largest, 2);
        $sch = round(abs(100 - ($act+$est)), 2);// $frow->scheduleHrsTotal*100;
        $acttip= (!is_nan($act) && ($act > 0)) ? $act  : 0; 
        $esttip= (!is_nan($est) && ($est > 0)) ? $est : 0;
        $schtip = (($frow->actual_TotalHours > 0) || ($frow->estimatedHours  > 0) || ($frow->scheduleHrsTotal > 0)) ? 100 : 0;// $frow->scheduleHrsTotal*100;
      }
      else if($largest == $frow->actual_TotalHours)
      {
        $sch = round(($frow->scheduleHrsTotal*100) / $largest, 2);
        $est= round(($frow->estimatedHours*100) / $largest, 2);
        $act = round(abs(100 - ($sch+$est)), 2);// $frow->scheduleHrsTotal*100;
        $schtip= (!is_nan($frow->scheduleHrsTotal) && $frow->scheduleHrsTotal > 0) ?  ($frow->scheduleHrsTotal * 100) / $largest : 0;
        $esttip= (!is_nan($frow->estimatedHours) && $frow->estimatedHours > 0) ?  ($frow->estimatedHours*100 ) / $largest : 0;
        $acttip = (($frow->actual_TotalHours > 0) || ($frow->estimatedHours  > 0) || ($frow->scheduleHrsTotal > 0))  ?  100 : 0;// $frow->scheduleHrsTotal*100;
      }
      else if($largest == $frow->estimatedHours)
      {
        $act=  round(($frow->actual_TotalHours * 100) / $largest, 2); 
        $sch= round(($frow->scheduleHrsTotal * 100) / $largest, 2);//(int)$frow->scheduleHrsTotal*100/$largest;
        $est =  round(abs(100 - ($act+$sch)), 2);// $frow->scheduleHrsTotal*100;
        $acttip =   ($act > 0) ? $act : 0; 
        $schtip =   ($sch > 0) ?  ($sch) : 0; //(int)$frow->scheduleHrsTotal*100/$largest;
        $esttip = (($act > 0) ||( $sch > 0) || ($frow->estimatedHours > 0)) ?  100 : 0 ;// $frow->scheduleHrsTotal*100;
      }

    if($largest == $frow->actual_TotalHours && $smallest == $frow->scheduleHrsTotal){
        $total= abs(($sch + abs($est-$sch)) - 100);
         $third = (!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'</div>': '';
        $first= (!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'</div>': '';
        $second =  (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.abs($est).'%">
        '.$frow->estimatedHours.' </div>': '';         
    }
    elseif($largest == $frow->scheduleHrsTotal && $smallest == $frow->estimatedHours){
      $total= abs(($est + abs($act-$est)) - 100);
      $third= (!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'</div>': '';
        $second=(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.abs($act).'%">
        '.$frow->actual_TotalHours.'</div>' : '';
      $first = (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
        '.$frow->estimatedHours.' </div>': '';   
    }
    elseif($largest == $frow->estimatedHours && $smallest == $frow->actual_TotalHours){
      $total= abs(($act+ abs($sch-$act)) - 100);
      $third= (!is_nan($est) && $est > 0) ? ' <div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
      '.$frow->estimatedHours.' </div>': '';
        $second=(!is_nan($sch) && $sch > 0) ?'<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.abs($sch).'%">
        '.$frow->scheduleHrsTotal.'</div>': '';
        $first=(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'</div>' : '';
    }
    elseif($largest == $frow->estimatedHours && $smallest == $frow->scheduleHrsTotal){
        $total= abs(($sch+ abs($sch-$act)) - 100);
      $third= (!is_nan($est) && $est > 0) ? ' <div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
      '.$frow->estimatedHours.' </div>': '';
        $second =(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act" role="progressbar" style="width:'.abs($act).'%">
        '.$frow->actual_TotalHours.' </div>' : ''; 
        $first=(!is_nan($sch) && $sch > 0) ?'<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'</div>': '';
    }
    elseif($largest == $frow->scheduleHrsTotal && $smallest == $frow->actual_TotalHours){
        $total= abs(($act+ abs($est-$act)) - 100);
      $third=(!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'</div>': '';
      $second = (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.abs($est).'%">
        '.$frow->estimatedHours.'</div>': '';
      $first=(!is_nan($act) && $act > 0) ?  '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'</div>': ''; 
    }
    elseif($largest == $frow->actual_TotalHours && $smallest == $frow->estimatedHours){
        $total= abs(($est+ abs($sch-$est)) - 100);
    $third =(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'</div>' : '';
    $second=(!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.abs($sch).'%">
        '.$frow->scheduleHrsTotal.'</div>': '';
    $first = (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
        '.$frow->estimatedHours.' </div>': '';
    }
    
          if(($prop->PropName == 'scheduleHrsTotal' ) || ($prop->PropName == 'estimatedHours' ) || ($prop->PropName == 'actual_TotalHours')  )
              {
               if($dataview == 0){
                

   $tootltip="<div class='apg_progress_section'><span class='apg_progress_label'>Estimated</span> <span class='apg_progress_bar_tooltip'><label class='progress-bar progress-bar-est'  role='progressbar' style='width:".$esttip."%'> </label></span><span class='apg_progress_hr'>".$frow->estimatedHours." h</span></div>
   <div class='apg_progress_section'><span class='apg_progress_label'>Scheduled</span><span class='apg_progress_bar_tooltip'> <label class='progress-bar progress-bar-sch'  role='progressbar' style='width:".$schtip."%'></label> </span><span class='apg_progress_hr'>".$frow->scheduleHrsTotal." h</span></div>
   <div class='apg_progress_section'><span class='apg_progress_label'>Actual</span><span class='apg_progress_bar_tooltip'><label class='progress-bar progress-bar-act'  role='progressbar' style='width:".$acttip."%'></label> </span><span class='apg_progress_hr'>".$frow->actual_TotalHours." h</span></div> ";

    $html .= '<span class="progwidth"><div class="progressSide bit_progress_side">
    <div class="progress bsit_progress_cust" data-placement="bottom" data-toggle="tooltip" data-html="true" title="'.$tootltip.'">
    '.$first.'
    '.$second.'
    '.$third.' 
    </div>
  </div>
  <div class="maxhrs"> '.$largest.' Hrs</div></span>'; 
                $dataview = 1;
               }
          }elseif($prop->PropName == 'Status' )
          {
              if(strtolower($frow->statusColor) == 'yellow'){
                $color = '#ffba1a';
              }
              else{
                $color= $frow->statusColor;
              }
             $html .= '<span style="color:'.$color.';">'.$frow->Status.'</span>';
          }
          else{
                      if ($prop->PropEditType == 'SEL') {
                            $all_prop = json_decode($prop->EditValue);
                            
                            if(isset($all_prop->{$frow->$prname}))
                            {
                                $frow->$prname = $all_prop->{$frow->$prname};
                            }
                        }

                      if ($prop->PropEditType == 'YN') {
                          if ($frow->$prname==1) {
                              $frow->$prname='Yes';
                          } else {
                              $frow->$prname='No';
                          }
                      }

                      //* New code added by HD 
                      if ($prop->PropEditType =='DATE') {
                          if (strlen($frow->$prname)==0) {
                              $frow->$prname = '-';
                          } else {
                              $frow->$prname = date("Y-m-d", strtotime(str_replace("T", " ", $frow->$prname)));
                          }
                      }
            
                    if ($prop->PropEditType == 'DTTM') {
                          if (strlen($frow->$prname)==0) {
                              $frow->$prname = '-';
                          } else {
                              $frow->$prname = date("Y-m-d h:i a", strtotime(str_replace("T", " ", $frow->$prname)));
                          }
                       }

                      $linkopen .= (substr($frow->$prname, 0, 50));
                      $linkopen .= ((strlen($frow->$prname) > 50) ? "..." : "");
                      $linkopen .= ((strlen($frow->$prname) == 0) ? "-" : "");
                      $linkopen .= "</a>";
                      $html .= $linkopen;
              }
                  } else {

                              //  echo "<pre/>";print_R($frow);

          if ($prop->PropEditType == 'COLL') {
              $html .= '<input type="hidden" id="'.$prname.'" value="'.$frow->$prname.'">';
          }
          // progress bar start from here
          if(($prop->PropName == 'scheduleHrsTotal' ) || ($prop->PropName == 'estimatedHours' ) || ($prop->PropName == 'actual_TotalHours')  )
              {
               if($dataview == 0){
                /*$largest =0;
      
      $largest=max($frow->actual_TotalHours,$frow->estimatedHours,$frow->scheduleHrsTotal);
      $smallest=min($frow->actual_TotalHours,$frow->estimatedHours,$frow->scheduleHrsTotal);
     
      
     if($largest == ''){
        $largest =0;
      }
      if($largest == $frow->scheduleHrsTotal)
      {
        //$max1 = max((int) $frow->actual_TotalHours*100/$largest,(int) $frow->estimatedHours*100/$largest);
        $act= round(($frow->actual_TotalHours * 100) / $largest, 2); 
        $est= round(($frow->estimatedHours*100) / $largest, 2);
        $sch = round(abs(100 - ($act+$est)), 2);// $frow->scheduleHrsTotal*100;
        $acttip= (!is_nan($act) && ($act > 0)) ? $act  : 0; 
        $esttip= (!is_nan($est) && ($est > 0)) ? $est : 0;
        $schtip = (($frow->actual_TotalHours > 0) || ($frow->estimatedHours  > 0) || ($frow->scheduleHrsTotal > 0)) ? 100 : 0;// $frow->scheduleHrsTotal*100;
      }
      else if($largest == $frow->actual_TotalHours)
      {
        $sch = round(($frow->scheduleHrsTotal*100) / $largest, 2);
        $est= round(($frow->estimatedHours*100) / $largest, 2);
        $act = round(abs(100 - ($sch+$est)), 2);// $frow->scheduleHrsTotal*100;
        $schtip= (!is_nan($frow->scheduleHrsTotal) && $frow->scheduleHrsTotal > 0) ?  ($frow->scheduleHrsTotal * 100) / $largest : 0;
        $esttip= (!is_nan($frow->estimatedHours) && $frow->estimatedHours > 0) ?  ($frow->estimatedHours*100 ) / $largest : 0;
        $acttip = (($frow->actual_TotalHours > 0) || ($frow->estimatedHours  > 0) || ($frow->scheduleHrsTotal > 0))  ?  100 : 0;// $frow->scheduleHrsTotal*100;
      }
      else if($largest == $frow->estimatedHours)
      {
        $act=  round(($frow->actual_TotalHours * 100) / $largest, 2); 
        $sch= round(($frow->scheduleHrsTotal * 100) / $largest, 2);//(int)$frow->scheduleHrsTotal*100/$largest;
        $est =  round(abs(100 - ($act+$sch)), 2);// $frow->scheduleHrsTotal*100;
        $acttip =   ($act > 0) ? $act : 0; 
        $schtip =   ($sch > 0) ?  ($sch) : 0; //(int)$frow->scheduleHrsTotal*100/$largest;
        $esttip = (($act > 0) ||( $sch > 0) || ($frow->estimatedHours > 0)) ?  100 : 0 ;// $frow->scheduleHrsTotal*100;
      }

    if($largest == $frow->actual_TotalHours && $smallest == $frow->scheduleHrsTotal){
        $total= abs(($sch + abs($est-$sch)) - 100);
         $third = (!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'Hrs </div>': '';
        $first= (!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'Hrs </div>': '';
        $second =  (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.abs($est).'%">
        '.$frow->estimatedHours.'Hrs  </div>': '';         
    }
    elseif($largest == $frow->scheduleHrsTotal && $smallest == $frow->estimatedHours){
      $total= abs(($est + abs($act-$est)) - 100);
      $third= (!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'Hrs </div>': '';
        $second=(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.abs($act).'%">
        '.$frow->actual_TotalHours.'Hrs </div>' : '';
      $first = (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
        '.$frow->estimatedHours.'Hrs  </div>': '';   
    }
    elseif($largest == $frow->estimatedHours && $smallest == $frow->actual_TotalHours){
      $total= abs(($act+ abs($sch-$act)) - 100);
      $third= (!is_nan($est) && $est > 0) ? ' <div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
      '.$frow->estimatedHours.'Hrs  </div>': '';
        $second=(!is_nan($sch) && $sch > 0) ?'<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.abs($sch).'%">
        '.$frow->scheduleHrsTotal.'Hrs </div>': '';
        $first=(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'Hrs </div>' : '';
    }
    elseif($largest == $frow->estimatedHours && $smallest == $frow->scheduleHrsTotal){
        $total= abs(($sch+ abs($sch-$act)) - 100);
      $third= (!is_nan($est) && $est > 0) ? ' <div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
      '.$frow->estimatedHours.'Hrs  </div>': '';
        $second =(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act" role="progressbar" style="width:'.abs($act).'%">
        '.$frow->actual_TotalHours.'Hrs  </div>' : ''; 
        $first=(!is_nan($sch) && $sch > 0) ?'<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'Hrs </div>': '';
    }
    elseif($largest == $frow->scheduleHrsTotal && $smallest == $frow->actual_TotalHours){
        $total= abs(($act+ abs($est-$act)) - 100);
      $third=(!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.$sch.'%">
        '.$frow->scheduleHrsTotal.'Hrs </div>': '';
      $second = (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.abs($est).'%">
        '.$frow->estimatedHours.'Hrs  </div>': '';
      $first=(!is_nan($act) && $act > 0) ?  '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'Hrs </div>': ''; 
    }
    elseif($largest == $frow->actual_TotalHours && $smallest == $frow->estimatedHours){
        $total= abs(($est+ abs($sch-$est)) - 100);
    $third =(!is_nan($act) && $act > 0) ? '<div class="progress-bar progress-bar-act"  role="progressbar" style="width:'.$act.'%">
        '.$frow->actual_TotalHours.'Hrs </div>' : '';
    $second=(!is_nan($sch) && $sch > 0) ? '<div class="progress-bar progress-bar-sch"  role="progressbar" style="width:'.abs($sch).'%">
        '.$frow->scheduleHrsTotal.'Hrs </div>': '';
    $first = (!is_nan($est) && $est > 0) ? '<div class="progress-bar progress-bar-est" role="progressbar" style="width:'.$est.'%">
        '.$frow->estimatedHours.'Hrs  </div>': '';
    }*/

   $tootltip="<div class='apg_progress_section'><span class='apg_progress_label'>Estimated</span> <span class='apg_progress_bar_tooltip'><label class='progress-bar progress-bar-est'  role='progressbar' style='width:".$esttip."%'> </label></span><span class='apg_progress_hr'>".$frow->estimatedHours." h</span></div>
   <div class='apg_progress_section'><span class='apg_progress_label'>Scheduled</span><span class='apg_progress_bar_tooltip'> <label class='progress-bar progress-bar-sch'  role='progressbar' style='width:".$schtip."%'></label> </span><span class='apg_progress_hr'>".$frow->scheduleHrsTotal." h</span></div>
   <div class='apg_progress_section'><span class='apg_progress_label'>Actual</span><span class='apg_progress_bar_tooltip'><label class='progress-bar progress-bar-act'  role='progressbar' style='width:".$acttip."%'></label> </span><span class='apg_progress_hr'>".$frow->actual_TotalHours." h</span></div> ";

    $html .= '<span class="progwidth"><div class="progressSide bit_progress_side">
    <div class="progress bsit_progress_cust" data-placement="bottom" data-toggle="tooltip" data-html="true" title="'.$tootltip.'">
    '.$first.'
    '.$second.'
    '.$third.' 
    </div>
  </div>
  <div class="maxhrs"> '.$largest.' Hrs</div></span>'; 
                $dataview = 1;
               }
          }elseif($prop->PropName == 'Status' )
          {
              if(strtolower($frow->statusColor) == 'yellow'){
                $color = '#ffba1a';
              }
              else{
                $color= $frow->statusColor;
              }
             $html .= '<span style="color:'.$color.';">'.$frow->Status.'</span>';
          }
          else{
            
                      if ($prop->PropEditType == 'SEL') {
                          $all_prop = json_decode($prop->EditValue);
                          if(isset($all_prop->{$frow->$prname}))
                          {
                              $frow->$prname = $all_prop->{$frow->$prname};
                          }
                      }
                        // print_r($frow->$prname );
                      if ($prop->PropEditType == 'YN') {
                          if ($frow->$prname == 1) {
                              $frow->$prname = 'Yes';
                          } else {
                              $frow->$prname = 'No';
                          }
                      }

                      //* New code added by HD 
                      if ($prop->PropEditType == 'DATE') {
                          if (strlen($frow->$prname) == 0) {
                              $frow->$prname = '-';
                          } else {
                              $frow->$prname = date("Y-m-d", strtotime(str_replace("T", " ", $frow->$prname)));
                          }
                      }

                      ///* New code added by HD 
                      if ($prop->PropEditType == 'TIME') {
                          if (strlen($frow->$prname) == 0) {
                              $frow->$prname = '-';
                          } else {
                              $frow->$prname = date("h:i", strtotime(str_replace("T", " ", $frow->$prname)));
                          }
                      }

                      if(isset($frow->$prname)){
                          $html .= (substr($frow->$prname, 0, 50));
                          $html .= ((strlen($frow->$prname) > 50) ? "..." : "");
                          $html .= ((strlen($frow->$prname) == 0) ? "-" : "");
                      }
        }
                  }
                   if(($prop->PropName == 'scheduleHrsTotal' ) || ($prop->PropName == 'estimatedHours' ) || ($prop->PropName == 'actual_TotalHours')  )
                  {
                   if($dataviewcl == 0){
                      $html .= '</td>';
                      $dataviewcl = 1;
                    }
                  }
                  else{
                    $html .= '</td>';
                  }
                  

              }
          }
          $html .= '<td><ul id="toggle"><li class="bsit_link">'; 
          // $html .= '<a href="#" class="bsit_edite_img"><span class="icon-pen"></span></a>';


          if (((isset($frame['allow_edit'])) && ($frame['allow_edit'] == 1)) ||
            ($frame['Properties'][0]->AllowEdit == '1'))
            {
                $html .= '<a class="edit-del-btn-'.$frame['PFID'].' bsit-edit-btn" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit">
                <div class="BSIT-TableActions" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit">
                <span class="icon-pen" ></span>
                </div>
                </a>'  ;
          } 

          if ((isset($frame['allow_edit'])) || (isset($frame['allow_delete'])) || (isset($frame['allow_notes'])) ) {
              if ($frame['allow_edit'] == '1' || $frame['allow_delete'] == '1' || $frame['allow_notes'] == '1' ) {
                  $html .= '';
              }
          } else {
              if ($frame['Properties'][0]->AllowEdit == '1' || $frame['Properties'][0]->AllowDelete == '1' || $frame['Properties'][0]->AllowNotes == '1') {
                  $html .= '<a class="bsit_edite_menu">...</a>';
              }
          }

          $html .= '<ul class="bsit_edite_sub_menu " id="dropdown"  role="menu">';

          if (((isset($frame['allow_edit'])) && ($frame['allow_edit'] == 1)) ||
            ($frame['Properties'][0]->AllowEdit == '1'))
            {
                  $html .= '<li class="bsit_edit"><a class="edit-del-btn-'.$frame['PFID'].' bsit-edit-btn" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit">
                <div class="BSIT-TableActions" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit">
                <span class="icon-pen">Edit</span>
                </div>
                </a></li>';
          } 

          if (((isset($frame['allow_delete'])) && ($frame['allow_delete'] == 1)) || ($frame['Properties'][0]->AllowDelete == '1')) 
          {
                  $html .= '<li class="bsit_delete"><a class="edit-del-btn-'.$frame['PFID'].' bsit-delete-btn" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="delete">
                <div class="BSIT-TableActions" style="margin-right:3px;" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete">
                <span class="icon-trash">Delete</span>
                </div>
                </a></li>';
          }

          if (((isset($frame['allow_notes'])) && ($frame['allow_notes'] == 1)) || ($frame['Properties'][0]->AllowNotes == '1')){
                  $html .= '<li class="bsit_fav"><a class="notes-btn-'.$frame['PFID'].' bsit-notes-btn" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="notes">
                <div class="BSIT-TableActions" style="margin-right:10px;" data-toggle="tooltip" data-placement="bottom" data-original-title="Notes">
                  <span class="icon-copy" id="test">Notes</span>
                </div>
              </a></li>';
          } 
      
     
       if (((isset($frame['allow_links'])) && ($frame['allow_links'] == 1)) || ($frame['Properties'][0]->AllowLinks == '1')){
                  $html .= '<li class="bsit_documents"><a class="upload-btn-'.$frame['PFID'].' bsit-upload-btn" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="upload">
                <div class="BSIT-TableActions" style="margin-right:10px;" data-toggle="tooltip" data-placement="bottom" data-original-title="Documents">
                  <span class="icon-document" id="test">Documents</span>
                </div>
              </a></li>';
          } 
          // custom option in action Like Download
          if($prop->custom_action != ''){
            $customAction = json_decode($prop->custom_action);  
          
         
          foreach ($customAction->CustomActions as $optVal => $value) {
             $final_params = '';
            if(!empty($value->params)) {

                $explode_val = explode(',', $value->params);
                
                $all_params = array();
                foreach ($explode_val as $key => $expolde_value) {
                 
                 if($frow->$expolde_value){
                    $all_params[] = $frow->$expolde_value;
                 }
                }
         $final_params = implode("','",$all_params);
               // $final_params = "'".implode("','",$all_params)."'";
            }
              //onclick="'.$value->function.'('.$final_params.')"
                if($value->function == 'downloadDocument'){
                  $html .= '<li class="bsit_edit" id="download">
                     <a target="_blank" href="'.base_url().'page/downloaddocument?docid='.$final_params.'" class="edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit"><div class="BSIT-TableActions" >
                      <span class="'.$value->icon.' bsit_custom_auction_box">  '.$value->label.'</span>
                      </div></a>
                     </li>';
                }else{
                  $html .= '<li class="bsit_edit" id="download">
                   <a onclick="'.$value->function.'('.$final_params.')" class="edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit"><div class="BSIT-TableActions" >
                    <span class="'.$value->icon.' bsit_custom_auction_box">  '.$value->label.'</span>
                    </div></a>
                   </li>';
                }
            }
          }   

          $html .= '</ul></li></ul></td>'; 
          $html .= '</tr>';
          $html .= '<input type="hidden" class="tot_record_'.$frame['PFID'].'" value="'.$frow->total_count.'">';
      }
  } else {
      $html .= '<input type="hidden" class="tot_record_'.$frame['PFID'].'" value="1">';
  }

  /*New Code by HD*/
  $html .= '<input type="hidden" class="has_more_records_'.$frame['PFID'].'" value="'.$frow->has_more_records.'">';

  $html .= '</tbody>';
  $html .= '</table>';
  $html .= '</div>';

  $html_sort .= '<input type="hidden" id="sort_field_'.$frame['PFID'].'" class="" value="">'; // FOR SORTING DATA

  // ------------------------------
  // -- PAGINATION CODE
  // ------------------------------
  $html_pagi .= '<div class="pagination_tr">';
  if ($prop->hide_pagination == '0') {
      $html_pagi .= '<div class="pagination dataTables_paginate paging_simple_numbers" id="example_paginate">';
      $html_pagi .= '<div class="pagination">';
      $html_pagi .= '<div id="pagination_output">';
      $html_pagi .= '<div class="bsit_page_per_row">';
      $html_pagi .= '<input type="hidden" id="total_page_'.$frame['PFID'].'" value="'.$frame['tot_page_count'].'">';
      $html_pagi .= '<input type="hidden" id="page_num_'.$frame['PFID'].'" class="pagination_class_'.$frame['PFID'].'" value="1">';
      /*$html_pagi .= '<input type="hidden" id="default_page_size_'.$frame['PFID'].'" value="'.$frame['Properties'][0]->default_page_size.'">';*/
      $html_pagi .= '<select id="num_recs_'.$frame['PFID'].'" class="number_of_rec" >';

      //SET DEFAULT PAGE SIZE IN PAGINATION
      if ($frame['Properties'][0]->default_page_size == '') {
          $html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
      } else {
          if ($frame['Properties'][0]->default_page_size == '10') {
              $html_pagi .= '<option value="10" selected="selected">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
          } elseif ($frame['Properties'][0]->default_page_size == '20') {
              $html_pagi .= '<option value="10">10</option><option value="20" selected="selected">20</option><option value="50">50</option><option value="100">100</option>';
          } elseif ($frame['Properties'][0]->default_page_size == '50') {
              $html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50" selected="selected">50</option><option value="100">100</option>';
          } elseif ($frame['Properties'][0]->default_page_size == '100') {
              $html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100"  selected="selected">100</option>';
          } else {
              $html_pagi .= '<option value="'.$frame['Properties'][0]->default_page_size.'">'.$frame['Properties'][0]->default_page_size.'</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
          }
      }

      $html_pagi .= '</select>';
      $html_pagi .= '<span>Rows Per Page</span>';
      $html_pagi .= '</div>';
      $html_pagi .= '<div class="pagi_arrows">';
      $html_pagi .= '<input type="button" id="pagi_previous_'.$frame['PFID'].'" data-prevpage="prev" class="bsit-pagination bsit_prev_next mdl-button previous pagination_class_'.$frame['PFID'].'" value="Prev">';
      $html_pagi .= '<span id="pagination_number_div"></span>';
      $html_pagi .= '<input type="button" id="pagi_next_'.$frame['PFID'].'" data-nextpage="next" value="Next" class="bsit-pagination bsit_prev_next mdl-button pagination_class_'.$frame['PFID'].'">';
      $html_pagi .= '</div>';
      $html_pagi .= '</div>';
      $html_pagi .= '</div>';
      $html_pagi .= '</div>';
      $html_pagi .= '<input type="hidden" class="PFID" value="'.$frame['PFID'].'">';
  }

  $html_pagi .= '</div>';
  $html .= $html_pagi;
  $html .= $html_sort;
  $html .= '</div>';
//echo $fullPath;
  // ------------------------------
  // -- CREATE TABLEVIEW CACHE FILE
  // ------------------------------
  $myfile1 = fopen($fullPath, "w+") or die("test Unable to open file");
  fwrite($myfile1, $html);
  fclose($myfile1);
  echo $html;
?>

<script type="text/javascript">

$(document).ready(function() {

  $("table tr").each(function(index) {
      $(this).find("th.Clswdt:eq(0)").addClass("pl-125");
  });

  $("table tr").each(function(index){
      $(this).find("td.Clswdt:eq(0)").addClass("pl-125");
  });

  let numItems = $('.table.table-hover.table-mc-light-blue.mdl-data-table1').length

  if(numItems > 1) {
    $('.main_table_start').addClass('scrollTable');
  }

});

(function($){
      $(window).on("load",function(){

        $(".bsit_table_scroll_fancy").mCustomScrollbar({
          axis:"yx",
          scrollButtons:{enable:true},
          theme:"light-thick",
          scrollbarPosition:"outside"
        });
        });
    })(jQuery);

jQuery(document).ready(function () {
  jQuery('[data-toggle="tooltip"]').tooltip();
});


</script>
