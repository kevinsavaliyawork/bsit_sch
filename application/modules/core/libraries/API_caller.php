<?php

defined('BASEPATH') or exit('No direct script access allowed');

abstract class API_caller extends brain_controller
{
    private $user_id;

    public function __construct()
    {
        parent::__construct(0);
        $this->load->model('module');
        $this->load->model('Bsit_io', 'API');
        $this->load->library('session');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    // Adding a new record
    public function create_exec($data)
    {
        $CURLDATA = array('collections' => $data);

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_POST;

        $json_page_result = $this->API->CallAPI("POST", $BASEURLMETHOD, $CURLDATA);

        $page_result = json_decode($json_page_result);

        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : CREATE_FAIL;
            echo $error_message;
            return;
        }

        echo (json_encode($page_result->Data));
        return;
    }

    // Updating and existing record
    public function update_exec($data)
    {
        // debug($data);exit;
        // set the header so jquery know we are sending back json
        header('Content-type: application/json');

        // create an array to hold the correct structure for the data to send
        $CURLDATA = array('collections' => $data);

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_POST;
        $json_page_result = $this->API->CallAPI("POST", $BASEURLMETHOD, $CURLDATA);
        $page_result = json_decode($json_page_result);


        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : UPDATE_FAIL;
            echo $error_message;
            return;
        }


        echo (json_encode($page_result->Data));
        return;
    }

    // Deleteing a single record
    public function delete_exec($data)
    {
        // set the header so jquery know we are sending back json
        header('Content-type: application/json');

        // create an array to hold the correct structure for the data to send
        $CURLDATA = array('collections' => json_encode($data));

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_DELETE;

        $json_page_result = $this->API->CallAPI("DELETE", $BASEURLMETHOD, $CURLDATA);
        $page_result = json_decode($json_page_result);

        // code added for check error message of delete

        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : DELETE_FAIL;
            echo $error_message;
            return;
        }

        echo (json_encode($page_result->Data));
        return;
    }

    // loading a record
    public function loadsingle_exec($data)
    {
        // set the header so jquery knows we are sending back json
        header('Content-type: application/json');

        // get the variables from the user
        $postdata = json_decode($data);
        $coll_id = $postdata[0]->collID;
        $record_id = $postdata[0]->recID;

        // setup an array in the correct format to load the data
        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_id' => $coll_id,
                'rec_id' => $record_id
            )))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
        $page_result = json_decode($json_page_result);

        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : SELECT_FAIL;
            echo $error_message;
            return;
        }

        echo (json_encode($page_result->Data));
        return;
    }

    // loading many records
    public function loadfiltered_exec($data)
    {
        // set the header so jquery knows we are sending back json
        header('Content-type: application/json');

        // get the variables from the user
        $postdata = json_decode($data);

        $coll_id = $postdata[0]->collID;
        $filter = $postdata[0]->filter;
        $order = $postdata[0]->order;

        if (isset($postdata[0]->coll_name)) {

            $coll_name = $postdata[0]->coll_name;

            // setup an array in the correct format to load the data
            $CURLDATA = array('collections' => json_encode(
                array('colls' => array(array(
                    'coll_name' => $coll_name,
                    'filter' => $filter,
                    'order' => $order,
                )))
            ));
        } else {

            // setup an array in the correct format to load the data
            $CURLDATA = array('collections' => json_encode(
                array('colls' => array(array(
                    'coll_id' => $coll_id,
                    'filter' => $filter,
                    'order' => $order,
                )))
            ));
        }


        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
        $page_result = json_decode($json_page_result);

        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : SELECT_FAIL;
            echo $error_message;
            return;
        }

        echo (json_encode($page_result->Data));
        return;
    }

    // loading a record
    public function dropdownlist_exec($data)
    {
        // set the header so jquery knows we are sending back json
        header('Content-type: application/json');

        $filter = '';
        $filter_data = '';
        // get the variables from the user
        $postdata = json_decode($data);
        $coll_id = $postdata[0]->collID;
        $columns = $postdata[0]->cols;
        $order = $postdata[0]->order;

        if (isset($postdata[0]->filter)) {
            $filter = $postdata[0]->filter;
            $filter_data = $filter;
        }

        // setup an array in the correct format to load the data
        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_id' => $coll_id,
                'cols' => $columns,
                'filter' => $filter_data,
                'numrec' => '9999',
                'order' => $order
            )))
        ));

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
        $page_result = json_decode($json_page_result);

        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : SELECT_FAIL;
            echo $error_message;
            return;
        }

        echo (json_encode($page_result->Data));
        return;
    }

    // loading Generate Invoice Data
    public function call_sp_exec()
    {
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        if ($_POST['parameters'] == '') {
            $data_parameters = '';
            $this->BASEURL = $BASEURLMETHOD . '?key=' . $_POST['sp_name'][0]['key'] . '&filters=""';
        } else {
            $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
            $data_parameters = urlencode($data_parameters);
            $this->BASEURL = $BASEURLMETHOD . '?key=' . $_POST['sp_name'][0]['key'] . '&filters=' . $data_parameters;
        }

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);

        $page_result = json_decode($json_page_result);

        echo (json_encode($page_result->Data));
        return;
    }

    public function call_method_exec()
    {

        $_DATAPOST['parameters'] = array(
            'key' => 'bsit_addnewnote',
            'filters' => $_POST['parameters'],

        );
        $data_parameters = json_encode($_DATAPOST['parameters']);
        $this->BASEURLMETHOD = API_BASEURL . API_METHOD_GET;
        $get_task_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD, $data_parameters, false);
        // $task_result = json_decode($get_task_detail);
        $page_result = json_decode($get_task_detail);

        echo (json_encode($page_result->Data));
        // print_r($get_task_detail); exit;
        return;
    }

    public function Call_SP_exec_Login()
    {
        $array = $_POST['parameters'];
        $parameters = $_POST['parameters'];
        unset($parameters[1]);
        $parameters = array_values($parameters);
        $data_parameters = '{"parameters":' . json_encode($parameters) . '}';
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $this->BASEURL = $BASEURLMETHOD . '?key=' . $_POST['sp_name'][0]['key'] . '&filters=' . $data_parameters;

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);

        $page_result = json_decode($json_page_result);

        echo (json_encode($page_result));
        return;
    }
}
