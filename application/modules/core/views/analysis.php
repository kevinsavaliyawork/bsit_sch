
 <script src="<?php echo base_url();?>core/assets/js/analysis.js"></script>
  <script src="<?php echo base_url();?>core/assets/js/filters_search.js"></script>

 <link href="<?php echo base_url();?>core/assets/css/analysis.css" rel="stylesheet" type="text/css" />

<div class="mui-container-fluid bsit-primary-bgcol">
  <div id="f-hero" class="mui--text-center">
    <div id="f-hero-welcome" class="mui-container-fluid mui--text-left">
      <form method="get" id="analysisForm" action="core/analysis/run">

        <!-- Filters search -->
        <?php echo($filters_view); ?>

        <div>

<?php
$measureList = '';
$measDropList = '';
$measFormList = '';
$measSelectedList = '';
$selMeasuresHTML = array();

foreach ($measures as $measkey => $mea) {
    $measSelectedHTML = '';

    // check if the measure is in the list selected
    if (in_array($mea['measure_id'], $selectedMeasures)) {
        // setup the HTML which will be displayed
        $selMeasuresHTML[$mea['measure_id']] = '<li class="measItem" data-measureid="'.$mea['measure_id'].'">'.$mea['name'].'<a class="measureRemover" href="javascript:void(0)">x</a></li>';

        // add to set of selected HTML options in the form field
        $measFormList = $measFormList.'<option selected="selected" value="'.$mea['measure_id'].'">'.$mea['name'].'</option>';
    }

    // should this measure be displayed, or has it been excluded in the DB
    if ($mea['hide_from_select'] == 0) {
        // add option to list of measures
        $measDropList = $measDropList.'<li><a class="measureSelector" href="javascript:void(0)" data-measureid="'.$mea['measure_id'].'" data-measurename="'.$mea['name'].'">'.$mea['name'].'</a></li>';
    }
}

// diplay selected measures in correct order
foreach ($selectedMeasures as $meaId) {
    $measSelectedList .= $selMeasuresHTML[$meaId];
}

?>

          <div id="measuresList">
            <div id="measuresSelectedList">
              <ul id="measSelectedUL">
                <div class="mui-dropdown">
                  <button class="mui-btn mui-btn--raised" data-mui-toggle="dropdown">
                    MEASURES
                    <span class="mui-caret"></span>
                  </button>
                  <ul class="mui-dropdown__menu">
                    <?php echo($measDropList); ?>
                  </ul>
                </div>
                <?php echo($measSelectedList); ?>
              </ul>
            </div>
          </div>
        </div>
        <div>

<?php
$groupByFormList = '';
$groupByDropList = '';
$currSelGroupBy = '';
$groupByName = '';
$dimNameSpacer = 0 ;
$lastDimTable = '';
$groupByNamePadded = '';

// loop the dimensions for output in the drop down
foreach ($dimensions as $dimkey=>$dim) {
    // setup the name space to pad out the name
    if ($dim['dim_table'] == $lastDimTable) {
        $dimNameSpacer++;
    } else {
        $dimNameSpacer = 0;
    }

    $lastDimTable = $dim['dim_table'];

    // space out the name
    $groupByNamePadded = str_pad($dim['name'], strlen($dim['name']) + $dimNameSpacer, '-', STR_PAD_LEFT);

    // if selected group is null, use the first
    if ($selectedGroups == '') {
        $selectedGroups = $dim['dimension_id'];
    }

    // check if the measure is in the list selected
    if (in_array($dim['dimension_id'], $selectedGroups)) {
        // set the currently selected group by
        $currSelGroupBy = $currSelGroupBy.'<li class="groupBySelected" data-groupbyid="'.$dim['dimension_id'].'">'.$dim['name'].'<a class="groupRemover" href="javascript:void(0)" >x</a></li>';
    }

    // add option to list of available group bys
    if ($dim['show_in_groupby'] == '1') {
        $groupByDropList = $groupByDropList.'<li><a class="groupBySelector" href="javascript:void(0)" data-dimid="'.$dim['dimension_id'].'" data-dimname="'.$dim['name'].'">'.$groupByNamePadded.'</a></li>';
    }
}

?>
          <ul id="GroupBySelectedUL">
            <div class="mui-dropdown">
              <button class="mui-btn mui-btn--raised" data-mui-toggle="dropdown">
                GROUP BY
                <span class="mui-caret"></span>
              </button>
              <ul class="mui-dropdown__menu">
                <?php echo($groupByDropList); ?>
              </ul>
            </div>
            <?php echo($currSelGroupBy); ?>
          </ul>
        </div>

<?php
// if matrix is needed we need different parameters in the check box html
$matrix_check_params = " disabled";
if ($matrix == 1) {
    $matrix_check_params = " checked";
}

?>

        <!-- Filters, measures,group,sort,page,rows -->
        <input type="text" name="filters" id="filtersHiddenSelector">
        <select name="groups[]" multiple="multiple" id="groupBySelector">
          <?php echo($groupByFormList); ?>
        </select>
        <select name="measures[]" multiple="multiple" id="measureHiddenSelector">
          <?php echo($measFormList); ?>
        </select>
        <input type="hidden" name="sort" value="<?php echo($sort);?>"/>
        <input type="hidden" name="page" value="<?php echo($page);?>"/>
        <input type="hidden" name="rows" value="<?php echo($rows);?>"/>
        <input type="hidden" name="outputcsv" value="0" id="outputCsvFlag"/>
        <button type="submit" class="mui-btn mui-btn--raised">Submit</button>
        <input type="checkbox" value="1" id="matrixCheckbox" name="matrix" <?php echo($matrix_check_params);?> />
        <label for="matrixCheckbox" style="font-weight:normal">Matrix</label>
      </form>
    </div>
    <div class="mui--appbar-height"></div>
  </div>
</div>

<!-- This is closed in the footer -->
<div class="mui-container-fluid">
  <div class="space-top"></div>
  <div id="analysisDataMasterContainer">
    <div id="analysisDataContOverlay"></div>
    <div class="mui-panel" id="analysisDataContainer">
      <div class="main_menu_item">
        <div class="menu_item_icon">
          <img src="assets/core/images/methods.png" class="image">
        </div>
        <div class="menu_item_text">
          <p>The data you need is just one <span style="font-weight:bold; font-size:16px">search</span> away, try some of the examples below:</p>
          <p>
            Calendar 2016 - The full year 2016 <br/>
            March 2016 - All of March 2016 <br/>
            Holden - Find parts with a primary manufacturer of Holden<br/>
            Customer Name - Search for sales by a specific customer<br/>
            Part Name - Search for sales by a specific part<br/>
          </p>
        </div>
      </div>
    </div>
  </div>
