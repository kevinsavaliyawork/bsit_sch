<!-----------------------------------------
-- BSIT CHART
-- #KeyChartIDs
------------------------------------------>
<div class="chart_main" id="repTerrSalesMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Opps by Owner</div>
  </div>

  <!-- body -->
  <div class="chart_body">
      <!-- RIGHT -->
      <div class="chart_hero" id="OwnerPieChart_container" style="min-height:300px;">
        <canvas id="OwnerPieChart" style="width:100%; height:100%;"></canvas>
      </div>
  </div>

  <!-- Foooter -->
  <div class="chart_footer">
    <!-- LEFT -->
    <div class="chart_foot_1st"></div>

    <!-- RIGHT -->
    <div class="chart_foot_2nd"></div>
  </div>

</div>
