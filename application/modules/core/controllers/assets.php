    <?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');


    class assets extends brain_controller
    {



        function __construct()
        {
            parent::__construct(1);
            // echo '123';exit;
            //---get working directory and map it to your module
            $file = getcwd() . '/application/modules/' . implode('/', $this->uri->segments);
            // echo $file;exit;
            $file = urldecode($file);
            //  print "<pre>"; print_r($file); exit;
            //----get path parts form extension
            $path_parts = pathinfo($file);
            //---set the type for the headers
            $file_type =  strtolower($path_parts['extension']);

            //   echo "<pre>";print_r($this->uri->segment_array());exit;

            if (is_file($file)) {

                //----write propper headers
                switch ($file_type) {
                    case 'css':
                        header('Content-type: text/css');
                        break;

                    case 'js':
                        header('Content-type: text/javascript');
                        break;

                    case 'json':
                        header('Content-type: application/json');
                        break;

                    case 'xml':
                        header('Content-type: text/xml');
                        break;

                    case 'pdf':
                        header('Content-type: application/pdf');
                        break;

                    case 'jpg' || 'jpeg' || 'png' || 'gif':
                        // header('Content-type: image/'.$file_type);
                        // readfile($file);

                        $getInfo = getimagesize($file);
                        header('Content-type: ' . $getInfo['mime']);
                        ob_clean();
                        flush();
                        readfile($file);

                        exit;
                        break;
                }

                include $file;
            } else {
                //show_404();
            }
            exit;
        }

        function index()
        {
        }
        function js()
        {
        }
        function css()
        {
        }
    }
