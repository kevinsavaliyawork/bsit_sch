<?php 
	// ------------------------------
	// -- INVOICE REPORT VIEW - Shows the invoice report from the SSRS server 
	// ------------------------------
		
	$fullPath	= $frame['fullPath_content'];	
	$viewPath	= $frame['viewPath_content'];
	
	$html = '<div id="page_frame_'.$frame['PFID'].'_content" class="call_content_div">';
	
	/*
	echo "<pre>";
	print_r($load_data);
	echo "</pre>"; 
	*/
	
	// report URL
	$inv_num = $load_data[0]->InvNum;
	$cust_num = $load_data[0]->CustName;
	$report_url = "https://tt.brainstormit.com.au/bsit_api/DownloadReport.aspx?Reportname=Invoice_Single&Reportfolder=BSIT_TimeTracker&rpInvNum=".$inv_num."&fileName=BrainStormIT-".$cust_num."-".$inv_num."";
	//$report_url = "http://tt.brainstormit.com.au/ReportServer/Pages/ReportViewer.aspx?%2fBSIT_TimeTracker%2fInvoice_Single&rs:Command=Render&rc:Parameters=false&InvNum=".$report_url;
	//$report_url = "http://www.brainstormit.com.au";
	
	// create the iframe to show the report
	//$html .= '<div style="height:1000px">';
	$html .= '<a href="'.$report_url.'">Download Report</a>';
	
	//$html .= '<iframe style="width:100%; height:100%;" src="'.$report_url.'" />';
	//$html .= '</div>';
	$html .= '</div>';
	
	// ------------------------------
	// -- CREATE TABLEVIEW CACHE FILE
	// ------------------------------
	$myfile1 = fopen($fullPath, "w") or die("Unable to open file!");
	fwrite($myfile1, $html);
	fclose($myfile1);
	echo $html;
?>	