<?php
class Customermodel extends brain_model {
  function __construct(){
    parent::__construct();
    $this->load->model('core/Bsit_io','API');
    $this->load->library('session');
  }
 public function getActivityHistoryDetail($customerId){

          $_DATAPOST['parameters'] = array (
                'key' => 'get_customer_notes',
                'filters' => 
                  array(
                      array (
                        'key' => 'customerId',
                        'value' => $customerId,//'4079',
                      ),

                      
                      ),
                  );

            $data_parameters = json_encode($_DATAPOST['parameters']);

            $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
            $get_activity_history_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
         // echo "<pre/>";  print_r($get_activity_history_detail); exit;
            return $get_activity_history_detail;
    } 

 public function getCustomerDetail($customerId){
        
      $_DATAPOST['parameters'] = array (
          'key' => 'get_customer_overview',
          'filters' => 
            array(
                array (
                  'key' => 'customerId',
                  'value' => $customerId,
                ),
                ),
            );

      $data_parameters = json_encode($_DATAPOST['parameters']);
      $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
      $get_customer_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
      // echo "<pre/>";print_r($get_customer_detail);     exit;
      return $get_customer_detail;
    }

    public function getCustomerProjectsDetail($customerId){
        
      $_DATAPOST['parameters'] = array (
          'key' => 'getProjectsAndTasks',
          'filters' => 
            array(
                array (
                  'key' => '_id',
                  'value' => $customerId,
                ),
                ),
            );

      $data_parameters = json_encode($_DATAPOST['parameters']);
      $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
      $get_customer_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
      // echo "<pre/>";print_r($get_customer_detail);     exit;
      return $get_customer_detail;
    }
    public function getcustomerSupportTaskDetail($customerId){
        
      $_DATAPOST['parameters'] = array (
          'key' => 'getAllSupportTasks',
          'filters' => 
            array(
                array (
                  'key' => '_id',
                  'value' => $customerId,
                ),
                ),
            );

      $data_parameters = json_encode($_DATAPOST['parameters']);
      $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
      $get_customer_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
      // echo "<pre/>";print_r($get_customer_detail);     exit;
      return $get_customer_detail;
    }
}

