<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pageedit extends brain_controller
{
    const ERRORVIEW = "bsiterror";

    public function __construct()
    {
        parent::__construct(0);
        $this->load->model('module');
        // $this->load->model('Bsit_io','API');
        $this->load->model('collectionmodel');
        $this->load->library('session');

        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function frameproperties()
    {

        $page_result_data = $this->collectionmodel->getFrameCollectionProperties($_POST['frameId']);

        $data_all['default_property'] = $page_result_data;
        $data_all['frameID'] = $_POST['frameId'];
        $data_all['frameName'] = $_POST['frameName'];
        $data_all['pageId'] = $_POST['pageId'];
        $data_all['pageFrameId'] = $_POST['pageFrameId'];
        $data_all['collection_id'] = $_POST['collection_id'];
        $data_all['dataframeid'] = $_POST['dataframeid'];


        $page_result = $this->collectionmodel->getCollectionDataWithoutLimit($_POST['collection_id']);



        if ($page_result->HasError != 1) {
            $data_all['collection_name'] = $page_result[0]->Collection_Name;
        } else {
            $data_all['collection_name'] = '';
        }

        echo $this->load->view('framepropertybase', $data_all, true);
    }

    public function addEditProperty()
    {
        $json_page_result = $this->collectionmodel->addEditPropertyToFrame($_POST);
        echo $json_page_result;
    }

    public function showPropertyData()
    {
        if ($_POST['type'] == 'property') {

            $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(PROPERTY_OTHER_COLLECTION);


            $collectionId = $json_page_result_COLL[0]->Results[0]->CollID;

            $column_name = 'fp._id';

            $json_page_result_FRAME = $this->collectionmodel->getFrameDataByName(PROPERTY_OTHER_FRAMEID);

            $frameId = $json_page_result_FRAME[0]->Results[0]->frameID;

            $page_result_prop = $this->collectionmodel->getFrameProperties($frameId);

            $data_all['property_data'] = $page_result_prop;
            $data_all['class_name'] = 'mdl-cell mdl-cell--6-col collec-frame-properties';
            $data_all['static_coll_id'] = $collectionId;
            $data_all['id'] = $_POST['id'];
        } elseif ($_POST['type'] == 'table') {

            $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(TABLE_OTHER_COLLECTION);

            $collectionId = $json_page_result_COLL[0]->Results[0]->CollID;

            $column_name = 'bf._id';

            $json_page_result_FRAME = $this->collectionmodel->getFrameDataByName(TABLE_OTHER_FRAMEID);


            $frameId = $json_page_result_FRAME[0]->Results[0]->frameID;

            $page_result_prop = $this->collectionmodel->getFrameProperties($frameId);

            $data_all['property_data'] = $page_result_prop;

            $data_all['class_name'] = 'mdl-cell mdl-cell--6-col collec-frame-table';
            $data_all['static_coll_id'] = $collectionId;
            $data_all['id'] = $_POST['id'];
        } elseif ($_POST['type'] == 'collection') {

            $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(COLL_OTHER_COLLECTION);


            $collectionId = $json_page_result_COLL[0]->Results[0]->CollID;


            $column_name = 'pf._id';

            $json_page_result_FRAME = $this->collectionmodel->getFrameDataByName(COLL_OTHER_FRAMEID);

            $frameId = $json_page_result_FRAME[0]->Results[0]->frameID;

            $page_result_prop = $this->collectionmodel->getFrameProperties($frameId);

            $data_all['property_data'] = $page_result_prop;
            $data_all['class_name'] = 'mdl-cell mdl-cell--3-col  collec-coll collec-coll';
            $data_all['static_coll_id'] = $collectionId;
            $data_all['id'] = $_POST['id'];
        }

        $json_page_result = $this->collectionmodel->getCollectionFilterByColumn($collectionId, $column_name, $_POST['id']);

        $data_all['showpropertyData'] = $json_page_result[0]->Results[0];
        $data_all['dataframeid'] = $_POST['dataframeid'];

        echo $this->load->view('propertydata', $data_all, true);
    }

    public function addpageframe()
    {
        $pageId = $_POST['data']['pageId'];
        $collectionId = $_POST['data']['collection_id'];

        $json_page_resultAll = $this->collectionmodel->addPageFrame($pageId, $collectionId);

        echo json_encode($json_page_resultAll);
    }

    public function deleteFrame()
    {

        $json_page_result = $this->collectionmodel->deleteFrame($_POST['frameID'], $_POST['pageFrameId']);

        echo $json_page_result;
    }

    public function getCollection()
    {
        $json_page_result = $this->collectionmodel->getAllConnection();
        echo $json_page_result;
    }
}
