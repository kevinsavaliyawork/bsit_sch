<script src="<?php echo base_url();?>jobview/assets/js/job.js"></script>
<?php
	// ------------------------------
	// -- CREATE LISTVIEW CACHE FILE
	// ------------------------------
	// Its Inclue : Display the Content(Data) of Files

	$cacheDir	= APPPATH."views/cache/";
	$linkopen = "";
	$html = "";
	$html_pagi = "";
	$fullPath	= $frame['fullPath_content'];
	$viewPath	= $frame['viewPath_content'];

	$linkopen = "";
	$html .= '<div id="page_frame_'.$frame['PFID'].'_content" class="call_content_div">';
	$html .= '<input type="hidden" id="frame_order" value='.$frame['order'].'>';
	$html .= '<div id="job_view_status_container">';

	/* // CREATE ARAAYS OF JOB STATUS & BASED ON THAT DISPLAY JOBS
	foreach($frame['Properties'] as $prop)
	{
		if($prop->PropEditType == 'SEL' && $prop->PropTitle == 'Status')
		{
			$selectOptions_status = json_decode($prop->EditValue);
		}
	}  */
	$user_orgs_id = $this->session->userdata('orgs_id');


	$parent_workflow_id = '';
	$CURLDATA_PF 	= 	Array('collections' =>
		json_encode(
			Array(
				'colls'=> Array(
					Array(
						'coll_name'	=> 'bsit_jobs2'//,
					//	'filter'	=> "pj.workflow_id = 8 AND pj.Parent_id = 0 AND jb.status <> 'COMPLETE'"
						// 'filter'	=> "jb._id = ".$load_data[0]->ParentJobID.""
						)
	))));

	//print_r("query");
	//print_r(array_values($CURLDATA_PF));
	
	
	
	//$page_result_filter = $this->API->CallAPI("GET",BASEURL,$CURLDATA_PF);
	
	//print_r("results");
	//print_r($page_result_filter);
		
		
	$page_result_filter = json_decode($page_result_filter);

	if(isset($page_result_filter->Data)){
		$page_result_filter = $page_result_filter->Data;
		$parent_workflow_id = $page_result_filter[0]->Results[0]->ParentWorkflowId;

	}

		
	// call to get a list of all the status options.
	$CURLDATA 	= 	Array('collections' =>
				json_encode(
					Array(
						'colls'=> Array(
							Array(
								'coll_name'	=> 'job_workflow_steps',
								'cols'		=> Array("status_title","WorkflowOrder", "TotalValue", "ExpectedValue"),
								'filter'	=> "job.workflow_type_id = 7",
								'order'		=> "WorkflowOrder ASC",
								'numrec'	=> '9999')))));
	
	$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
	$json_page_result = $this->API->CallAPI("GET",$BASEURLMETHOD,$CURLDATA);
	$page_result = json_decode($json_page_result);

	// CREATE ARRAYS OF JOB STATUS & BASED ON THAT DISPLAY JOB STATS
	$status_results_data 	= $page_result->Data;
	$status_results_array 	= $status_results_data[0]->Results;
	// echo "<pre>";
	// print_r($status_results_array);
	// echo "</pre>";
	foreach($status_results_array as $key=>$data)
	{
		$selectOptions_status[] = $data->status_title;
		//$selectOptions_status[$data->status_title] = $data->_id;
	}

	// loop each job, if jobs has a stutus not in the array of stats, set to other!
	$other_needed = false;
	$other_status_text = 'InvalidStatus';

	foreach ($load_data as $key=>$frow)
	{
		// IS this status in the list of status options
		if(in_array($frow->Status,$selectOptions_status) != true){
			$load_data[$key]->Status = $other_status_text;
			$other_needed = true;
		}
	}
	// Add the option for other status
	if($other_needed){
		$selectOptions_status[] = $other_status_text;
		//$selectOptions_status[$other_status_text] = $other_status_text;
	}

	$html1 = '';
	foreach($selectOptions_status as $optVal=>$opt)
	{
		// Total Number of Job Count
		$count_job = 0;
		foreach ($load_data as $frow)
		{
			if($frow->Status == $opt)
			{
				$count_job++;
			}
		}

		if($count_job !=0)
		{
			$html .= '<div class="status_class">';
			$html .= '<div class="mui-panel">';
			$html .= '<div class="mui--text-subhead status_class_heading"><b>'.$opt.'</b> ('.$count_job.')</div>';
			// View value here
			$html .= '<div class="content mui-container-fluid status_class_content task-list task-container ui-droppable ui-sortable" id="'.$opt.'">';
			foreach ($load_data as $frow)
			{
				if($frow->Status == $opt)
				{
					// BUTTONS - Edit & Delete & Notes
					$html_action='';
					$html_action .= '<div style="float: right;">';

					// attachment option
					$html_action .= '<a class="upload-btn-'.$frame['PFID'].' bsit-upload-btn" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="upload" Title="Attachments">
						<div class="BSIT-TableActions hasTooltip" style="margin-right:3px;">
							<img src="'.BASEURL_IMAGE.'/attach.png"  id="tooltip2'.$frow->key_id.'">
							<span class="icon_tooltip">Delete</span>
						</div>
					</a>';
					if(isset($frame['allow_delete']))
					{
						if($frame['allow_delete'] == '1')
						{
							$html_action .= '<a class="edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="delete">
											<div class="BSIT-TableActions hasTooltip" style="margin-right:3px;">
												<img src="'.BASEURL_IMAGE.'/delete.png"  id="tooltip2'.$frow->key_id.'">
												<span class="icon_tooltip">Delete</span>
											</div>
										</a>';
						}
					}else{
						if($frame['Properties'][0]->AllowDelete == '1')
						{
							$html_action .= '<a class="bsit-delete-btn edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="delete">
											<div class="BSIT-TableActions hasTooltip" style="margin-right:3px;">
												<img src="'.BASEURL_IMAGE.'/delete.png"  id="tooltip2'.$frow->key_id.'">
												<span class="icon_tooltip">Delete</span>
											</div>
										</a>';
						}
					}

					if(isset($frame['allow_notes']))
					{
						if($frame['allow_notes'] == '1')
						{
							$html_action .= '<a class="bsit-notes-btn notes-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="notes">
										<div class="BSIT-TableActions hasTooltip" style="margin-right:10px;">
											<img src="'.BASEURL_IMAGE.'/note-multiple.png" id="tooltip_notes'.$frow->key_id.'">
											<span class="icon_tooltip">Notes</span>
										</div>
									</a>';
						}
					}else{
						if($frame['Properties'][0]->AllowNotes == '1')
						{
							$html_action .= '<a class="bsit-notes-btn notes-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="notes">
										<div class="BSIT-TableActions hasTooltip" style="margin-right:10px;">
											<img src="'.BASEURL_IMAGE.'/note-multiple.png" id="tooltip_notes'.$frow->key_id.'">
											<span class="icon_tooltip">Notes</span>
										</div>
									</a>';
						}
					}
					
					$stale = $frow->Stale;
					$stale_style = "";
					
					if($stale==1)
					{
						$stale_style = "background-color:rgba(255,87,51,.5);";
					}
					$html_action .= '   </div> ';
					
					$html .= '<div class="mui-row status_class_each_content todo-task" id="'.$frow->key_id.'"  style="'.$stale_style.';   "> ';
					
					// Title - Allow click to edit?
					if((isset($frame['allow_edit']) && $frame['allow_edit'] == '1') || $frame['Properties'][0]->AllowEdit == '1')	{
						$frow->title 	= '<span data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit"><span><a class="bsit-edit-btn opp_title_edit edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit" style="cursor:pointer; padding-top:5px;">'.$frow->key_id.' - '.$frow->title.'</a></span></span>';
					}
					// Title - Output
					//$html .= $frow->title;
					
					/*
					echo('<pre>');
					print_r($frow);
					echo('</pre>');
					*/
					
					$priority='color: #fff; background: #000000;';//not set
					$priority_value= $frow->priorty;
					$priority_border='#000000';
					
					switch($priority_value){
						case 5;
						$priority='color: #000; background: #00ff00;'; //green
						$priority_border='#00ff00;';
						break;
						case 4;
						$priority='color: #000; background: #ffff00;';//yellow
						$priority_border='#ffff00;';
						break;
						case 3;
						$priority='color: #fff; background: #ffa500;';//orange
						$priority_border='#ffa500;';
						break;
						case 2;
						$priority='color: #fff; background: #ff0000;';//red
						$priority_border='#ff0000;';
						break;
						case 1;
						$priority='color: #fff; background: #800000;';//maroon
						$priority_border='#800000;';
						break;
						
						default:
						$priority='color: #fff; background: #000000;';//not set
						$priority_border='#000000;';
						break;
					}
					
					
					// Assignee
					$span_assignee = '<span style="width:30px;font-weight:bold;float:right;z-index:10; height:30px;line-height:30px;border:thin solid black; border-radius:50%;text-align:center;margin-top:2px; font-size: 13px;font-weight: bold; background-color:black;color:white;letter-spacing:2px;">';
					$assingee = '';
					if(isset($frow->NameFirst)){
							$assingee = '<div style="letter-spacing:2px; padding-left: 4px; width: 30px; height: 30px;  border-radius: 50%;  font-size: 13px;font-weight: bold; line-height: 30px;  text-align: center; '.$priority.' float:right; padding-right: 14px;padding-top:2px;">';
							$assingee .= substr($frow->NameFirst,0,1).''.substr($frow->NameLast,0,1);
							$span_assignee .= substr($frow->NameFirst,0,1).''.substr($frow->NameLast,0,1);
							$assingee .= '  </div>';
							$span_assignee .= '     </span>';
					}
					
					
					// LOOP FOR DISPLAYING POPERTY CONTENT
					$cust_name = '';
					if($frow->CustColor == ''){
						$cust_colors[0] = '#000000';
						$cust_colors[1] = '#FFFFFF';
					}else{
						$cust_colors = explode(',',$frow->CustColor);
						$cust_colors = str_replace('[','',$cust_colors);
						$cust_colors = str_replace(']','',$cust_colors);
						$cust_colors = str_replace('"','',$cust_colors);
					}
					$cust_name .= '<div style="display:block; width: 100%;float: left;background-color:'.$cust_colors[0].';color:'.$cust_colors[1].';padding: 0 5px 0px;border-radius: 3px; border: thin solid '.$cust_colors[1].';font-weight:bold; height:40px;line-height:40px;border:none; border-radius:0px; border-bottom: 5px solid '.$priority_border.';">';
					$cust_name .= $frow->CustName;
						$cust_name .= $span_assignee;
					$cust_name .= '</div>';
					

					
					// Created Date
					$created = '';
					
					if(isset($frow->create_date)){
						$created .= '<span style="float: left; color:blue; padding: 5px 17px 0px; ; font-size:60%; float:left;bottom:0px;">';
						$created .= date("d/m/Y",strtotime($frow->create_date));
						$created .= '   </span>';
						
						
						
					}
					
					// Followup date
					$followup_date_html = '';
					if(isset($frow->followup_date)){
						$followup_sec 	= strtotime($frow->followup_date);
						$timenow_sec	= time();
						$diff_sec		= $followup_sec - $timenow_sec;
						$days			= floor($diff_sec / (60 * 60 * 24)); // 60 seconds per min, 60 mins per hour, 24 hours per day
						
						// if the followup date is overdue or expired show as red
						if($days <= 0){
							$opp_follup_div   = '<div class="opp_followup_date" style="color:#FF0000; font-weight:700; display:block; width:100%;" padding-top:5px;>';
						}elseif($days <= 3){
							$opp_follup_div   = '<div class="opp_followup_date" style="color:#FFA07A; font-weight:400; display:block; width:100%; padding-top:5px;">';
						}else{
							$opp_follup_div   = '<div class="opp_followup_date" style="display:block; width:100%; padding-top:5px;">';
						}
						
						// output the follup date
						$followup_date_html	.= $opp_follup_div;
						$followup_date_html	.= '<img src="'.BASEURL_IMAGE.'/clock.png" style="width:14px;"> ';
						$followup_date_html	.= date("d/m/Y" ,$followup_sec).' </div>';
					}
					
					// Planned date
					$planned_date_html = '';
					if(isset($frow->planned_date)){
						$planned_date_sec 	= strtotime($frow->planned_date);
						$timenow_sec	= time();
						$diff_sec		= $planned_date_sec - $timenow_sec;
						$days			= floor($diff_sec / (60 * 60 * 24)); // 60 seconds per min, 60 mins per hour, 24 hours per day
						
						// if the followup date is overdue or expired show as red
						if($days <= 0){
							$opp_follup_div   = '<div class="opp_followup_date" style="color:#FF0000; font-weight:700; display:block; width:100%;" padding-top:5px;>';
						}elseif($days <= 3){
							$opp_follup_div   = '<div class="opp_followup_date" style="color:#FFA07A; font-weight:400; display:block; width:100%; padding-top:5px;">';
						}else{
							$opp_follup_div   = '<div class="opp_followup_date" style="display:block; width:100%; padding-top:5px;">';
						}
						
						// output the follup date
						$planned_date_html	.= $opp_follup_div;
						$planned_date_html	.= '<img src="'.BASEURL_IMAGE.'/calendar.png" style="width:14px;"> ';
						$planned_date_html	.= date("d/m/Y" ,$planned_date_sec).' </div> ';
					}
					
					$html .= '<div class="each_job_footer_part" style="margin-top:0px;">';
						/*
						$html .= '<span class="icons_clock_doller">';
						$html .= $html_job_terms;
						$html .= '</span>';
						*/
							$html .= '<div style="clear:both; padding-top:2px; padding-bottom:2px;">';
							$html .= $cust_name;
							$html .= $frow->title;
							//$html .= $assingee;
							$html .= '</div>';
							$html .= '<div style="display:block; padding-top:5px;width:100%;">';
							$html .= $followup_date_html;
							$html .= $planned_date_html;
							$html .= $created;
						
							//$html .= '<div style="clear:both; padding-top:9px; padding-bottom:2px;">';
			
							$html .= '</div>';
							$html .= $html_action;
						
						$html .= '</div>';
					$html .= '  </div>';
					
				}
				$html .= '<input type="hidden" class="tot_record_'.$frame['PFID'].'" value="'.$frow->total_count.'">';
			}
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}
		if($count_job ==0){
			$html1 .= '<div class="status_class">';
			$html1 .= '<div class="mui-panel">';
			$html1 .= '<div class="mui--text-subhead status_class_heading"><b>'.$opt.'</b> ('.$count_job.')</div>';
			$html1 .= '<div class="content mui-container-fluid status_class_content task-list task-container ui-droppable ui-sortable" id="'.$opt.'">';
			$html1 .= '</div>';
			$html1 .= '</div>';
			$html1 .= '</div>';
		}
	}
		$html .= $html1;
		$html .= '</div>';
		//$html .= '<input type="hidden" class="tot_record_'.$frame['PFID'].'" value="'.$frow->total_count.'">';

	// ------------------------------
	// -- PAGINATION CODE
	// ------------------------------
	$html_pagi .= '<div class="pagination_tr" >';
	if(isset($frame['allow_download']))
	{
		if($frame['allow_download'] == 1){
			$html_pagi .= '<div class="download_button_div hasTooltip" id="download_data_'.$frame['PFID'].'">';
			$html_pagi .= '<img style="opacity:.4" src="'.BASEURL_IMAGE.'/ic_get_app_black_24dp_1x.png"/><span class="icon_tooltip_frame">Download</span>';
			$html_pagi .= '</div>';
		}
	}else{
		if($prop->hide_allow_download == '0'){
			$html_pagi .= '<div class="download_button_div hasTooltip" id="download_data_'.$frame['PFID'].'">';
			$html_pagi .= '<img style="opacity:.4" src="'.BASEURL_IMAGE.'/ic_get_app_black_24dp_1x.png"/><span class="icon_tooltip_frame">Download</span>';
			$html_pagi .= '</div>';
		}
	}
	if($prop->hide_pagination == '0')
	{
		$html_pagi .= '<div class="pagination dataTables_paginate paging_simple_numbers" id="example_paginate">';
		$html_pagi .= '<div class="pagination">';
		$html_pagi .= '<div id="pagination_output">';
		$html_pagi .= 'Rows Per Page : ';
		$html_pagi .= '<input type="hidden" id="total_page_'.$frame['PFID'].'" value="'.$frame['tot_page_count'].'">';
		$html_pagi .= '<input type="hidden" id="page_num_'.$frame['PFID'].'" class="pagination_class_'.$frame['PFID'].'" value="1">';
		$html_pagi .= '<input type="hidden" id="default_page_size_'.$frame['PFID'].'" value="'.$frame['Properties'][0]->default_page_size.'">';
		$html_pagi .= '<select id="num_recs_'.$frame['PFID'].'" class="number_of_rec" >';
		//$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
		//SET DEFAULT PAGE SIZE IN PAGINATION
		if($frame['Properties'][0]->default_page_size == ''){
			$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
		}else{
			if($frame['Properties'][0]->default_page_size == '10'){
				$html_pagi .= '<option value="10" selected="selected">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
			}else if($frame['Properties'][0]->default_page_size == '20'){
				$html_pagi .= '<option value="10">10</option><option value="20" selected="selected">20</option><option value="50">50</option><option value="100">100</option>';
			}else if($frame['Properties'][0]->default_page_size == '50'){
				$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50" selected="selected">50</option><option value="100">100</option>';
			}else if($frame['Properties'][0]->default_page_size == '100'){
				$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100"  selected="selected">100</option>';
			}else{
				$html_pagi .= '<option value="'.$frame['Properties'][0]->default_page_size.'">'.$frame['Properties'][0]->default_page_size.'</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
			}
		}
		$html_pagi .= '</select>';
		$html_pagi .= '<div class="pagi_arrows">';
		$html_pagi .= '<input type="button" id="pagi_previous_'.$frame['PFID'].'" data-prevpage="prev" class="mdl-button previous pagination_class_'.$frame['PFID'].'" value="" style="background: url('.base_url().'assets/images/chevron-left.png);background-repeat: no-repeat;">';
		$html_pagi .= '<span id="pagination_number_div"></span>';
		$html_pagi .= '<input type="button" id="pagi_next_'.$frame['PFID'].'" data-nextpage="next" value="" class="mdl-button pagination_class_'.$frame['PFID'].'" style="background: url('.base_url().'assets/images/chevron-right.png);background-repeat: no-repeat;">';
		$html_pagi .= '</div>';
		$html_pagi .= '</div>';
		$html_pagi .= '</div>';
		$html_pagi .= '</div>';
		$html_pagi .= '<input type="hidden" class="PFID" value="'.$frame['PFID'].'">';
	}
	$html_pagi .= '</div>';
	$html .= $html_pagi;

	$html .= $html_sort;
	$html .= '</div>';

	// ------------------------------
	// -- CREATE LISTVIEW CACHE FILE
	// ------------------------------
	$myfile1 = fopen($fullPath, "w") or die("Unable to open file!");
	fwrite($myfile1, $html);
	fclose($myfile1);
	echo $html;
?>
