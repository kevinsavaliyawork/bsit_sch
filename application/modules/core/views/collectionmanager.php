<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <link href="<?php echo base_url();?>core/assets/css/collection.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url();?>core/assets/js/collectionmanager.js"></script>
   <div style="padding: 8px;" class="bsit-page-frame" id="page_frame_<?php echo $dataframeid ?>" data-pfid="<?php echo $dataframeid ?>">
  <div class="tbl-controller">
    <!-- TODO: Remove hardcoded -->
    <div id="page_frame_14_header" class="call_header_div cut_collection">
      <h3></h3>
      <div id="filter_main_div_14" class="filter_main_div mdl-cell mdl-cell--12-col">
        <input type="hidden" name="collection_id" id="collection_id" value="<?php echo $_GET['CollID'];?>">
        <input type="hidden" name="pageId" id="pageId" value="<?php echo $_GET['pageId'];?>">
        <input type="text" name="collection_name" id="collection_name" value="<?php echo $collection_name;?>" style="width:225px;">
        <select name="tableall" class="cust-select-collect" id="tableall"></select>
        <button class="mui-btn mui-btn--primary" id="table_add">Add</button>
        <input class="history_page_number_14" value="" type="hidden">
      </div>
    </div>
    <div class="right">
      <button class="mui-btn mui-btn--primary" onclick="javascript:history.go(-1)">Back</button>
      <button class="mui-btn mui-btn--primary" id="view_collection">View</button>
      <a class="sidedrawer-toggle mui--hidden-xs" id="control_property" style="display: none;">☰</a>
    </div>
  </div>
  <div class="row tree_view_row mdl-grid cust-main-collect">
    <div class="mdl-cell mdl-cell--9-col">
        <div class="content_wrap">
          <div class="mdl-grid addsql">

<?php
if (!empty($default_property)) {
    foreach ($default_property as $key => $selected_property) {
        $table_name = explode('$$', $key);
?>

            <div class="table_cust mdl-cell--3-col s1" id="<?php echo $table_name[0]; ?>">
              <h5>&nbsp;</h5>
              <table class="mdl-data-table  mdl-data-table--selectable mdl-shadow--2dp">
                <thead>
                  <tr>
                    <th>
                      <a type="table" class="table_name col_name_cust" table_id="<?php echo $table_name[0]; ?>"><?php  echo $table_name[1]; ?></a>
                      <button type="button" class="mdl-chip__action">
                        <i table_id="<?php echo $table_name[0]; ?>" class="material-icons deleteAction">cancel</i>
                      </button>
                      </span>
                    </th>
                  </tr>
                </thead>
              <tbody>

<?php
foreach ($selected_property as $key => $allData) {
?>
              <tr>
                <td class="mdl-data-table__cell--non-numeric">
                  <label class="bsit_check">
                  <input title="Hide Filter" type="checkbox" class="columnCheck"
                    <?php echo($allData->property_id > 0 ? 'checked' : ''); ?>
                    column_name='<?php echo $allData->column_name; ?>'
                    tables_id='<?php echo $allData->table_id; ?>'
                    column_id='<?php echo $allData->property_id; ?>'
                  ><span class="bsit_checkmark"></span>
                </label>
                </td>
                <td type="property" column_id='<?php echo $allData->property_id; ?>'
                  class="mdl-data-table__cell--non-numeric
                    <?php echo($allData->property_id > 0 ? 'col_name_cust' : ''); ?>"
                >
                  <?php echo $allData->column_name; ?>
                </td>
              </tr>

<?php
}
?>

            </tbody>
          </table>
        </div>

<?php
  }
}
?>

      </div>
    </div>
  </div>
  <div class="propertyData"></div>
  <div id="allcollection" class="modal modal_add_edit fade"  data-modal="Add" style="width:auto; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:visible;z-index: 9999999999;left: 50%;transform: translateX(-50%);right: auto;max-width: 1100px;">
    <div class="mui-panel" style="margin-bottom:0px;">
      <button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent"
        style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">
        <svg class="" viewBox="0 0 24 24">
          <path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path>
          <path d="M0 0h24v24h-24z" fill="none"></path>
        </svg>
      </button>
      <h2>Add</h2>
      <hr/>
      <div id='collection_data'></div>
    </div>
  </div>
</div>

