<?php
defined('BASEPATH') or exit('No direct script access allowed');

class analysis extends brain_controller
{

	function __construct()
	{
		parent::__construct(0);
		//$this->load->database();
		$this->load->model('module');
		$this->load->model('analysismodel');
		$this->load->library('session');
	}

	function index()
	{

		//exit;
		// setup some needed vars
		$userID 					= $this->session->userdata('UserID');
		$userFullName 				= $this->session->userdata('FirstName') . ' ' . $this->session->userdata('LastName');
		$data['users_name_full']	= $userFullName;

		// load the menu infomation
		/* $tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules1();
		$data['menu_data'] = $this->load->view ('menu.php', $tmp_data, TRUE); */
		$tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules($userID);
		$data['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);

		$data['page_data_permission'] = $tmp_data['menu_data'];


		$data['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);


		$data['header_view'] = 1;

		$data['footer_view'] = 1;
		$data['pageMenu'] = 1;



		// get all the parameters posted to the page
		$parms 	= $this->get_options();
		// get all the dimensions and measures for use in the page


		$collection_id = null;
		if (isset($_GET['collection_id'])) {
			$collection_id = $_GET['collection_id'];
		}

		$data['dimensions'] = $this->analysismodel->getAllDimensions($collection_id);
		$data['measures'] = $this->analysismodel->getAllMeasures($userID, $collection_id);

		// add the permanent and default filters to the list of filters already selected

		$filters = $this->analysismodel->getCurrentUserFilters(intval($userID));

		$filters = $this->mergeAdHockDefAndPermFilters($parms[0], $filters, $data['dimensions']);


		// convert measures and filters into XML for execution in MSSQL
		/*$XMLfilters				= $this->getXMLFilters($filters);
		$XMLMeasures			= $this->getXMLmeasures($parms[1]);
		$XMLGroups				= $this->getXMLgroups($parms[3]);*/

		// setup the parms for display in the view
		$data['filters']			= $filters;
		$data['selectedMeasures']	= $parms[1];
		$data['sort']				= $parms[2];
		$data['selectedGroups']		= $parms[3];
		$data['rows']				= $parms[4];
		$data['page']				= $parms[5];
		$data['brkdwn']				= $parms[6];
		$data['matrix']				= $parms[7];

		// get the matrix parameter
		$matrix 					= $parms[7];

		// sort direction and column
		$data['sort_col']		= substr($parms[2], 1);
		$data['sort_order']		= substr($parms[2], 0, 1);

		// load the list of user links
		$data['user_links'] = $this->analysismodel->getAllSavedLinks(intval($userID));


		// load the filters search
		$data['filters_view']	= $this->load->view('filters_search', $data, TRUE);

		// load the page

		$data['Title'] = 'Analysis';
		$this->load->view('header', $data);
		$this->load->view('content', $data);

		$curr_url = 'http://' . $_SERVER['HTTP_HOST'] . "" . $_SERVER['REQUEST_URI'];


		$this->load->view('analysis');
		$this->load->view('footer');
	}

	// this actually fires the query into the database and converts the result into a table
	function run()
	{
		// setup some needed vars
		$userID = $this->session->userdata('UserID');
		// load the infomation
		/* $tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules1();
		$data['menu_data'] = $this->load->view ('menu.php', $tmp_data, TRUE); */
		$tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules($userID);
		$data['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);
		// get all the parameters posted to the page
		$parms	= $this->get_options();

		// get all the dimensions and measures for use in the page

		$collection_id = null;
		if (isset($_GET['collection_id'])) {
			$collection_id = $_GET['collection_id'];
		}


		$data['dimensions'] = $this->analysismodel->getAllDimensions($collection_id);
		$data['measures'] = $this->analysismodel->getAllMeasures($userID, $collection_id);

		// add the permanent and default filters to the list of filters already selected
		$filters = $this->mergeAdHockDefAndPermFilters($parms[0], array(), $data['dimensions']);

		// convert measures and filters into XML for execution in MSSQL
		$XMLfilters				= $this->getXMLFilters($filters);
		$XMLMeasures			= $this->getXMLmeasures($parms[1]);
		$XMLGroups				= $this->getXMLgroups($parms[3]);

		// check if the user has requested XLS format, if so, no limit on rows
		if (isset($_GET['outputcsv']) && $_GET['outputcsv'] == '1') {
			$parms[4] = 99999;
		}

		// get the matrix parameter
		$matrix = $parms[7];
		if ($matrix == 1) {
			$analysis_data 				= $this->analysismodel->runAnalysisV2($XMLfilters, $XMLMeasures, $parms[2], $XMLGroups, $parms[4], $parms[5], $parms[6], $userID, NULL, NULL, $matrix, NULL);
			$data['analysis_data'] 		= $analysis_data[0];
			$data['analysis_dim1'] 		= $analysis_data[1];
			$data['analysis_dim2'] 		= $analysis_data[2];
			$data['analysis_meas_cnt']	= $analysis_data[3];
			$data['analysis_matrix'] 	= $matrix;
		} else {
			// run the analysis on the database
			$analysis_data 			= $this->analysismodel->runAnalysis($XMLfilters, $XMLMeasures, $parms[2], $XMLGroups, $parms[4], $parms[5], $parms[6], $userID);
			$data['analysis_data'] 	= $analysis_data;
		}
		// setup the parms for display in the view
		$data['filters']			= $filters;
		$data['selectedMeasures']	= $parms[1];
		$data['sort']				= $parms[2];
		$data['selectedGroups']		= $parms[3];
		$data['rows']				= $parms[4];
		$data['page']				= $parms[5];
		$data['brkdwn']				= $parms[6];

		// sort direction and column
		$data['sort_col']		= substr($parms[2], 1);
		$data['sort_order']		= substr($parms[2], 0, 1);

		// can the user download
		$data['can_user_download'] = $this->session->userdata('can_download');

		// check if the user has requested XLS format
		if (isset($_GET['outputcsv']) && $_GET['outputcsv'] == '1') {
			if ($matrix == 1) {
				// load the XML version of the page for display
				$this->load->view('analysis_csv_matrix', $data);
			} else {
				// load the XML version of the page for display
				$this->load->view('analysis_csv', $data);
			}
		} else {
			if ($matrix == 1) {
				// load the page
				$this->load->view('analysis_run_matrix', $data);
			} else {
				// load the page
				$this->load->view('analysis_run', $data);
			}
		}
	}

	// merge the adhock and default and per filters into a single array
	private function mergeAdHockDefAndPermFilters($adhockFilters, $defPermFilters, $dimensions)
	{
		$filters = array();

		// loop each of the default and per filters and add to array
		foreach ($defPermFilters as $filter) {
			// do we want to draw the default filters
			// only when the user has not passed adhock filters
			if (count($adhockFilters) > 0 and $filter['perm_filter'] == 0) {
				// leave this for future options
			} else {
				// append this filter to the filters array
				$filters[$filter['dim_id']][] = array(
					'dim_id' 		=>	$filter['dim_id'],
					'dim_name' 		=>	$filter['dim_name'],
					'opperator' 	=>	$filter['opperator'],
					'value' 		=>	$filter['value'],
					'perm_filter' 	=>	$filter['perm_filter']
				);
			}
		}

		// loop each of the adhock filters and place into array we can use
		$dimkey = 0;
		foreach ($adhockFilters as $key => $filter) {
			// loop each dimension and get the name for this one
			$dimname = "";

			foreach ($dimensions as $dim) {
				if ($key == $dim['dimension_id']) {
					$dimname = $dim['name'];
				}
			}
			// each adhock filter can have many values, loop them
			foreach ($filter as $value) {
				// append each of the adhock filters to the array
				$filters[$key][] = array(
					'dim_id' 		=>	$key,
					'dim_name' 		=>	$dimname,
					'opperator' 	=> ($value[1] == "" ? "=" : $value[1]),
					'value' 		=>	$value[0],
					'perm_filter' 	=> ($value[2] == "" ? "0" : $value[2])
				);
			};
		}

		return $filters;
	}

	// generate XML filters from array
	private function getXMLFilters($inputfilters)
	{
		// if the array is empty, return empty string
		if (count($inputfilters) == 0) {
			return '<Filters><Dim></Dim></Filters>';
		}
		// setup an XML string to hold the XML data
		$filters 		= "<Filters><Dim>";
		$lastDim 		= 0;
		$loopNum		= 0;
		foreach ($inputfilters as $key => $dimensionfilters) {
			// add the opening tag for the dim
			if ($lastDim != $key) {
				// if first loop exclude closing dim tag
				if ($loopNum > 0) {
					$filters = $filters . '</Dim><Dim>';
				}
			}

			// loop each of the values
			foreach ($dimensionfilters as $filter) {
				// add this value for the dim to the XML
				$filters = $filters . '<Val Id="' . $key . '" ';
				$filters = $filters . 'Value="' . htmlspecialchars($filter['value']) . '" ';
				$filters = $filters . 'Opp="' . htmlspecialchars($filter['opperator']) . '" ';
				$filters = $filters . 'Perm="' . htmlspecialchars($filter['perm_filter']) . '" />';
			}
			$loopNum++;
		}

		// close the xml
		$filters = $filters . '</Dim></Filters>';

		//return the XML!!!
		return $filters;
	}

	// generate XML measures from array
	private function getXMLmeasures($inputmeasures)
	{
		// setup an XML string to hold the XML data
		$measures = "<Measures>";
		//print_r($inputmeasures);
		foreach ($inputmeasures as $measure) {

			// add this row to the XML
			$measures = $measures . '<Measure Id="' . $measure . '" />';
			//print_r($measures);
		}
		// close the xml
		$measures = $measures . "</Measures>";
		//print_r($measures);
		//return the XML!!!
		return $measures;
	}

	// generate XML groups from array
	private function getXMLgroups($inputgroups)
	{
		// setup an XML string to hold the XML data
		$groups = "<Groups>";
		foreach ($inputgroups as $group) {
			// add this row to the XML
			$groups = $groups . '<Group Id="' . $group . '" />';
		}
		// close the xml
		$groups = $groups . "</Groups>";

		//return the XML!!!
		return $groups;
	}

	// loads the get paramters passed to the page
	private function get_options()
	{
		// get the filters set, or set default
		if (isset($_GET['filters'])) {
			$filters = $_GET['filters'];
			$filters = json_decode($filters);
		} else {
			// no filters provided, setup empty XML string
			$filters = array();
		}

		// get the measures set, or set default
		if (isset($_GET['measures'])) {
			$measures = $_GET['measures'];
		} else {
			// no filters provided, setup empty XML string
			$measures =  array();
		}

		// get the group, or set default
		if (isset($_GET['groups'])) {
			$groups = $_GET['groups'];
			// if group 1 and 2 are the same, remove 2
			if (isset($groups[1]) && $groups[0] == $groups[1]) {
				unset($groups[1]);
			}
		} else {
			$groups = array();
		}

		// get the sort, or set default
		if (isset($_GET['sort'])) {
			$sort = $_GET['sort'];
		} else {
			$sort = "";
		}

		// get the rows, or set default
		if (isset($_GET['rows'])) {
			$rows =	$_GET['rows'];
		} else {
			$rows = 10;
		}

		// get the page, or set default
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 0;
		}

		// get the breakdown, or set default
		if (isset($_GET['breakdown'])) {
			$brkdwn = $_GET['breakdown'];
		} else {
			$brkdwn = 1;
		}

		// get the matrix, or set default
		if (isset($_GET['matrix'])) {
			$matrix = $_GET['matrix'];
		} else {
			$matrix = 0;
		}

		// setup the parms array
		$parms = array(
			$filters,
			$measures,
			$sort,
			$groups,
			$rows,
			$page,
			$brkdwn,
			$matrix
		);

		return $parms;
	}
}
