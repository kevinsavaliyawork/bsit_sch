<?php

class analysismodel extends brain_Model
{


	function runAnalysis($filters, $measures, $sort, $group, $rows, $page, $brkdwn, $userID)
	{


		$_POST['parameters'] = array(
			array(
				'key' => 'io_filters', 'value' => $filters
			),
			array(
				'key' => 'io_measures', 'value' => $measures
			),
			array(
				'key' => 'io_sort', 'value' => $sort
			),
			array(
				'key' => 'io_group', 'value' => $group
			),
			array(
				'key' => 'io_rows', 'value' => $rows
			),
			array(
				'key' => 'io_page', 'value' => $page
			),
			array(
				'key' => 'io_breakdwn', 'value' => $brkdwn
			),
			array(
				'key' => 'io_userid', 'value' => $userID
			)
		);
		$data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
		$data_parameters = urlencode($data_parameters);
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$this->BASEURL = $BASEURLMETHOD . '?key=BSIT_IO_ANALYSIS&filters=' . $data_parameters;
		$runData = $this->API->CallAPI("GET", $this->BASEURL);
		$data['runData'] = array();
		$data['runData'] = json_decode($runData);


		foreach ($data['runData']->Data as $key => $row) {
			$runDataAll[] = (array)$row;
		}

		return $runDataAll;
	}

	function runAnalysisV2($filters, $measures, $sort, $group, $rows, $page, $brkdwn, $userID, $formatvals, $rankDimId, $matrix, $debug)
	{


		$_POST['parameters'] = array(
			array(
				'key' => 'io_filters', 'value' => $filters
			),
			array(
				'key' => 'io_measures', 'value' => $measures
			),
			array(
				'key' => 'io_sort', 'value' => $sort
			),
			array(
				'key' => 'io_group', 'value' => $group
			),
			array(
				'key' => 'io_rows', 'value' => $rows
			),
			array(
				'key' => 'io_page', 'value' => $page
			),
			array(
				'key' => 'io_breakdwn', 'value' => $brkdwn
			),
			array(
				'key' => 'io_userid', 'value' => $userID
			),
			array(
				'key' => 'io_formatvals', 'value' => $formatvals
			),
			array(
				'key' => 'io_rankDimId', 'value' => $rankDimId
			),

			array(
				'key' => 'io_matrix', 'value' => $matrix
			),
			array(
				'key' => 'io_debug', 'value' => $debug
			),
			array(
				'key' => 'getdataset', 'value' => 1
			)
		);
		$data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
		$data_parameters = urlencode($data_parameters);
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$this->BASEURL = $BASEURLMETHOD . '?key=BSIT_IO_ANALYSIS&filters=' . $data_parameters;
		$runData = $this->API->CallAPI("GET", $this->BASEURL);


		// keep a row count
		$rowcnt = 0;
		$measure_count = 0;
		// Get the actual data for the rows


		$data['runData'] = array();
		$data['runData'] = json_decode($runData);



		foreach ($data['runData']->Data as $key => $row) {

			foreach ($row as $keyData => $rowAll) {
				$runDataAll[$key][$keyData] = (array)$rowAll;
			}
		}

		//echo "<pre>";print_r($runDataAll);echo "<pre>"; exit();		 

		foreach ($runDataAll['Table'] as $key => $row) {


			$row_keys = array_keys($row);
			$dim_1_key = (is_null($row_keys[0]) || $row_keys[0] == '' ? 'NA' : $row_keys[0]);
			$dim_2_key = (is_null($row_keys[1]) || $row_keys[1] == '' ? 'NA' : $row_keys[1]);



			// get the value of dimension 1 and dimension 2
			$dim1 = $row[$dim_1_key];
			$dim2 = $row[$dim_2_key];

			// remove the keys from the row array (they are in the title anyway)
			unset($row[$dim_1_key], $row[$dim_2_key]);

			// if first row get number of remaining columns in row (measures)

			$measure_count = count($row);
			$results[0]['BSIT']['BSIT'] = array($dim_1_key, $dim_2_key, array_keys($row));

			// put into a more user friendly format
			$results[0][$dim1][$dim2] = $row;


			$rowcnt++;
		}

		// if the row count is empty, exit this function
		if ($rowcnt == 0) {
			$results[0] = "";
			$results[1] = "";
			$results[2] = "";
			$results[3] = "";
			return $results;
		}

		if (array_key_exists('Table1', $runDataAll)) {
			// Get the meta data for the analysis 

			foreach ($runDataAll['Table1'] as $key => $row) {
				// put the row into the array we will return
				$row['dimVal'] = (is_null($row['dimVal']) ? 'NA' : $row['dimVal']);
				$row['dimVal'] = ($row['dimVal'] == '' ? 'NA' : $row['dimVal']);
				$results[1][] = $row;
			}
		}


		if (array_key_exists('Table2', $runDataAll)) {
			// Get the meta data for the analysis 

			foreach ($runDataAll['Table2'] as $key => $row) {

				// put the row into the array we will return
				$row['dimVal'] = (is_null($row['dimVal']) ? 'NA' : $row['dimVal']);
				$row['dimVal'] = ($row['dimVal'] == '' ? 'NA' : $row['dimVal']);
				$results[2][] = $row;
			}
		}

		// include the count of measures in the data to return
		$results[3] = $measure_count;

		return $results;
	}

	function getAllDimensions($collection_id)
	{

		$_POST['parameters'] = array(
			array(
				'key' => 'collection_id', 'value' => $collection_id
			)
		);
		$data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
		$data_parameters = urlencode($data_parameters);
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$this->BASEURL = $BASEURLMETHOD . '?key=BSIT_io_getdimensions&filters=' . $data_parameters;
		$dimensionData = $this->API->CallAPI("GET", $this->BASEURL);
		$data['dimensionData'] = array();
		$data['dimensionData'] = json_decode($dimensionData);


		foreach ($data['dimensionData']->Data as $key => $row) {
			$dimensionDataAll[] = (array)$row;
		}

		return $dimensionDataAll;
	}

	function getAllMeasures($userID, $collection_id)
	{


		$_POST['parameters'] = array(
			array(
				'key' => 'io_user_id', 'value' => $userID
			),
			array(
				'key' => 'collection_id', 'value' => $collection_id
			)
		);
		$data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
		$data_parameters = urlencode($data_parameters);
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$this->BASEURL = $BASEURLMETHOD . '?key=BSIT_IO_LOAD_USER_MEASURES&filters=' . $data_parameters;
		$measureData = $this->API->CallAPI("GET", $this->BASEURL);
		$data['measureData'] = array();
		$data['measureData'] = json_decode($measureData);


		foreach ($data['measureData']->Data as $key => $row) {
			$measureDataAll[] = (array)$row;
		}

		return $measureDataAll;
	}



	function lookupSearchTerm($searchTerm, $userID, $collection_id)
	{



		$_POST['parameters'] = array(
			array(
				'key' => 'io_search_term', 'value' => $searchTerm
			),
			array(
				'key' => 'io_user_id', 'value' => $userID
			),
			array(
				'key' => 'collection_id', 'value' => $collection_id
			)
		);

		$data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
		$data_parameters = urlencode($data_parameters);
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$this->BASEURL = $BASEURLMETHOD . '?key=BSIT_IO_FILTERSEARCH&filters=' . $data_parameters;
		$filterData = $this->API->CallAPI("GET", $this->BASEURL);
		$data['filterData'] = array();
		$data['filterData'] = json_decode($filterData);

		//print_r($data['filterData']);exit;
		foreach ($data['filterData']->Data as $key => $row) {
			$filterDataAll[] = (array)$row;
		}

		return $filterDataAll;
	}

	function getCurrentUserFilters($userID)
	{

		$_POST['parameters'] = array(
			array(
				'key' => 'io_user_id', 'value' => $userID
			)
		);
		$data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
		$data_parameters = urlencode($data_parameters);
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$this->BASEURL = $BASEURLMETHOD . '?key=BSIT_IO_LOAD_USER_FILTERS&filters=' . $data_parameters;
		$filterData = $this->API->CallAPI("GET", $this->BASEURL);
		$data['filterData'] = array();
		$data['filterData'] = json_decode($filterData);


		foreach ($data['filterData']->Data as $key => $row) {
			$filterDataAll[] = (array)$row;
		}

		return $filterDataAll;
	}

	function convDimTextToValue($searchTerm, $dimID)
	{
		$sql = "exec BSIT_IO_DRILL_THROUGH_LOOKUP @io_search_term = ?,@dimension_id = ?";
		$param = array(&$searchTerm, &$dimID);
		return $this->select_all($sql, $param);
	}

	function getAllSavedLinks($userID)
	{


		$_POST['parameters'] = array(
			array(
				'key' => 'io_user_id', 'value' => $userID
			)
		);
		$data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
		$data_parameters = urlencode($data_parameters);
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$this->BASEURL = $BASEURLMETHOD . '?key=BSIT_IO_LOAD_SAVED_LINKS&filters=' . $data_parameters;
		$userLink = $this->API->CallAPI("GET", $this->BASEURL);
		$data['user_links'] = array();
		$data['user_links'] = json_decode($userLink);
		foreach ($data['user_links']->Data as $key => $row) {
			$userData[] = (array)$row;
		}



		return $userData;
	}
}
