$(document).ready(() => {
  $('body').addClass('hide-sidedrawer');
  getAllTables();

  $('#control_property').on('click', () => {
    if ($('.collec-properties:visible').length == 1) {
      $('.collec-table').removeClass('slideIn');
      $('.collec-properties').addClass('slideIn');
    } else if ($('.collec-table:visible').length == 1) {
      $('.collec-properties').removeClass('slideIn');
      $('.collec-table').addClass('slideIn');
    } else if ($('.collec-coll:visible').length == 1) {
      $('.collec-table').removeClass('slideIn');
      $('.collec-properties').removeClass('slideIn');
      $('.collec-coll').addClass('slideIn');
    }
    $('.slideIn').toggle();
  });
});


// fetch all tables
function getAllTables() {
  let moduleUrl = base_url + 'core/collectionmanager/tableAll';

  jQuery.ajax({
    url: moduleUrl,
    type: 'POST',
    data: {},
    beforeSend: function () {
      collLoaderShow()
    },
    complete: function () {
      collHideLoader()
    },
    success: (data) => {
      let dataAll = JSON.parse(data);
      if (dataAll.HasError == true) {
        alert(dataAll.ErrorMessage);
        return false;
      } else {
        let newtradeContent = '';

        $('#tableall')
          .empty()
          .append('<option value="0">Select Table</option>');

        $.each(dataAll.Data[0].Results, (k, v) => {
          newtradeContent +=
            '<option value="' + v.name + '">' + v.name + '</option>';
        });

        $(newtradeContent).appendTo($('#tableall'));
      }
    },
  });
}

//add collection and display table with column
$(document)
  .button()
  .on('click', '#table_add', () => {
    let tablevalue = $('#tableall').val();
    let collection_id = $('#collection_id').val();
    let pageId = $('#pageId').val();
    let collection_name = $('#collection_name').val();

    if (tablevalue == 0 || collection_name == '') {
      $('#tableall').addClass('required_border_class');
      $('#collection_name').addClass('required_border_class');
      return false;
    } else {
      $('#tableall').removeClass('required_border_class');
      $('#collection_name').removeClass('required_border_class');
    }

    let moduleUrl = base_url + 'core/collectionmanager/sqlAll';
    let postData = {
      tablevalue: tablevalue,
      collection_id: collection_id,
      pageId: pageId,
      collection_name: collection_name,
    };

    jQuery.ajax({
      url: moduleUrl,
      type: 'POST',
      data: postData,
      beforeSend: function () {
        collLoaderShow()
      },
      complete: function () {
        collHideLoader()
      },
      success: (data) => {
        let dataAll = JSON.parse(data);
        if (dataAll.HasError == true) {
          alert(dataAll.ErrorMessage);
          return false;
        } else {
          if (collection_id == '' || collection_id == 0) {
            location.href =
              base_url +
              'core/collectionmanager?CollID=' +
              dataAll.Data[1]._id +
              '&Name=' +
              collection_name;
          }

          $('#collection_id').val(dataAll.Data[1]._id);

          let tables_id = dataAll.Data[2]._id;
          let newtradeContent = '';
          newtradeContent =
            '<div class="table_cust mdl-cell--3-col s1"  id=' +
            tables_id +
            '>' +
            '<h5>&nbsp;</h5><table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">' +
            '<thead>' +
            '<tr>' +
            '<th>' +
            '<a type="table" class="table_name col_name_cust" table_id=' +
            tables_id +
            '>' +
            tablevalue +
            '</a>' +
            '<button type="button" class="mdl-chip__action"><i table_id=' +
            tables_id +
            ' class="material-icons deleteAction">cancel</i></button></span></th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';

          $.each(dataAll.Data[0].Results, (k, v) => {
            newtradeContent +=
              '<tr>' +
              '<td class="mdl-data-table__cell--non-numeric"><input title="Hide Filter" type="checkbox"  class="columnCheck" tables_id = ' +
              tables_id +
              ' display_column_name=' +
              v.column_name +
              ' column_name=' +
              v.column_name +
              ' column_id = 0></input></td><td type="property" class="mdl-data-table__cell--non-numeric" column_id = 0>' +
              v.column_name +
              '</td>' +
              '</tr>';
          });

          $(newtradeContent).appendTo($('.addsql'));
        }
      },
    });
  });


//link/unlink column/property with collection
$(document).on('click', '.columnCheck', function (e) {
  let collection_id = $('#collection_id').val();

  let checked = 0;
  if ($(this).is(':checked')) {
    checked = 1;
  }

  let name = $(this).attr('column_name');
  let column_name = $(this).attr('column_name');
  let column_id = $(this).attr('column_id');
  let tables_id = $(this).attr('tables_id');

  let moduleUrl = base_url + 'core/collectionmanager/addEditProperty';
  let postData = {
    column_id: column_id,
    collection_id: collection_id,
    checked: checked,
    name: name,
    column_name: column_name,
    tables_id: tables_id,
  };

  jQuery.ajax({
    url: moduleUrl,
    type: 'POST',
    data: postData,
    context: this,
    beforeSend: function () {
      collLoaderShow()
    },
    complete: function () {
      collHideLoader()
    },
    success: (data) => {
      let dataCheck = JSON.parse(data);

      if (dataCheck.HasError == true) {
        alert(dataCheck.ErrorMessage);
        return false;
      } else {
        if (dataCheck.Data != null) {
          $(this).attr('column_id', dataCheck.Data[0]['_id']);
        }

        if ($(this).is(':checked')) {
          $(this)
            .closest('td')
            .next()
            .attr('column_id', dataCheck.Data[0]['_id']);
          $(this)
            .closest('td')
            .next()
            .addClass('col_name_cust');
          $(this)
            .closest('td')
            .next()
            .trigger('click');
          $(this)
            .closest('tr')
            .addClass('highliteRow');
        } else {
          $(this)
            .closest('td')
            .next()
            .attr('column_id', '');
          $(this)
            .closest('td')
            .next()
            .removeClass('col_name_cust');
          $(this)
            .closest('tr')
            .removeClass('highliteRow');
          $(this).attr('column_id', '');
          $('.collec-properties').hide();
          $('.collec-table').hide();
          $('#control_property').hide();
        }
      }
    },
  });
});

// display collection property
$(document)
  .button()
  .on('focus', '#collection_name', function (e) {
    let collection_id = $('#collection_id').val();

    if (collection_id == 0 || collection_id == '') {
      return false;
    }

    $('#key_id_coll').val(collection_id);

    $('tr').removeClass('highliteRow');

    let moduleUrl = base_url + 'core/collectionmanager/showPropertyData';
    let postData = {
      id: collection_id,
      type: 'collection',
    };

    jQuery.ajax({
      url: moduleUrl,
      type: 'POST',
      data: postData,
      context: this,
      beforeSend: function () {
        collLoaderShow()
      },
      complete: function () {
        collHideLoader()
      },
      success: (data) => {
        $('.propertyData').html(data);
        $(this)
          .closest('tr')
          .addClass('highliteRow');

        $('.table_name_prop').text(': ' + $(this).val());
        $('.property_name_prop').text('');
      },
    });
  });

// Fetch column property
$(document)
  .button()
  .on('click', '.col_name_cust', function (e) {
    let type = $(this).attr('type');
    let id
    $('tr').removeClass('highliteRow');

    if (type == 'property') {
      id = $(this).attr('column_id');
      $('#key_id').val(id);
    } else if (type == 'table') {
      id = $(this).attr('table_id');
      $('#key_id_table').val(id);
    }

    let moduleUrl = base_url + 'core/collectionmanager/showPropertyData';
    let postData = {
      id: id,
      type: type,
    };

    jQuery.ajax({
      url: moduleUrl,
      type: 'POST',
      data: postData,
      context: this,
      beforeSend: function () {
        collLoaderShow()
      },
      complete: function () {
        collHideLoader()
      },
      success: (data) => {
        $('.propertyData').html(data);

        $(this)
          .closest('tr')
          .addClass('highliteRow');
        if (type == 'property') {
          $('.table_name_prop').text(
            ': ' +
            $(this)
              .parent()
              .parent()
              .parent()
              .find('.table_name')
              .html()
          );
          $('.property_name_prop').text(': ' + $(this).html());
        } else if (type == 'table') {
          $('.table_name_prop').text(': ' + $(this).html());
          $('.property_name_prop').text('');
          $('.property_name_prop').text();
        }
      },
    });
  });


// delete table
$(document)
  .button()
  .on('click', '.deleteAction', function (e) {
    let confirm_delete = confirm('Are you sure you want to delete ?');

    if (confirm_delete == true) {
      let table_id = $(this).attr('table_id');
      let moduleUrl = base_url + 'core/collectionmanager/deleteTable';

      jQuery.ajax({
        url: moduleUrl,
        type: 'POST',
        data: {
          table_id: table_id,
        },
        context: this,
        beforeSend: function () {
          collLoaderShow()
        },
        complete: function () {
          collHideLoader()
        },
        success: (data) => {
          let dataCheck = JSON.parse(data);

          if (dataCheck.HasError == true) {
            alert(dataCheck.ErrorMessage);
            return false;
          } else {
            $(`#${table_id}`).remove();

            $('.collec-properties').hide();
            $('.collec-table').hide();
            $('#control_property').hide();

            return false;
          }
        },
      });
    }
  });
//fetch colletion data
$(document)
  .button()
  .on('click', '#view_collection', function (e) {
    let collection_id = $('#collection_id').val();
    let moduleUrl = base_url + 'core/collectionmanager/collectionData';

    jQuery.ajax({
      url: moduleUrl,
      type: 'POST',
      data: {
        collection_id: collection_id,
      },
      beforeSend: function () {
        collLoaderShow()
      },
      complete: function () {
        collHideLoader()
      },
      success: (data) => {
        $('#collection_data').html();

        let dataAll = JSON.parse(data);
        if (dataAll.HasError == true) {
          alert(dataAll.ErrorMessage);
          return false;
        } else {
          let col = [];
          for (let i = 0; i < dataAll.Data[0].Results.length; i++) {
            for (let key in dataAll.Data[0].Results[i]) {
              if (col.indexOf(key) === -1) {
                col.push(key);
              }
            }
          }

          // CREATE DYNAMIC TABLE.
          let table = document.createElement('table');
          table.className =
            'mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp';

          // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
          // TABLE ROW.
          let tr = table.insertRow(-1);

          for (let i = 0; i < col.length; i++) {
            let th = document.createElement('th');
            th.className = 'mdl-data-table__cell--non-numeric'; // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
          }

          // ADD JSON DATA TO THE TABLE AS ROWS.
          for (let i = 0; i < dataAll.Data[0].Results.length; i++) {
            tr = table.insertRow(-1);

            for (let j = 0; j < col.length; j++) {
              let tabCell = tr.insertCell(-1);
              tabCell.className = 'mdl-data-table__cell--non-numeric';
              tabCell.innerHTML = dataAll.Data[0].Results[i][col[j]];
            }
          }

          $($('#collection_data')).html(table);
          $('#allcollection').modal();
          $('.modal_add_edit h2').html('Collection Data');
        }
      },
    });
  });

function collLoaderShow() {
  $('.loader').show();

  //add new loader in count | if one ajax complete but second request is pending then it will help
  let loadcount = parseInt($(".loader").attr("loadcount"));
  if (loadcount) {
    loadcount += 1;
  } else {
    loadcount = 1;
  }

  $(".loader").attr("loadcount", loadcount);

}

function collHideLoader() {
  let loadcount = parseInt($(".loader").attr("loadcount"));
  if (loadcount) {
    loadcount -= 1;
  } else {
    loadcount = 0;
  }

  if (loadcount == 0) {
    $('.loader').hide();
  }

  $(".loader").attr("loadcount", loadcount);

}