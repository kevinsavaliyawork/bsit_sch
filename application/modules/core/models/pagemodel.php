<?php

class pagemodel extends brain_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('core/Bsit_io', 'API');
		$this->load->library('session');
	}

	// -- REQUEST THE PAGE HEADER INFO
	public function getPageHeaderInfo($page_name)
	{

		$CURLDATA = array('collections' => json_encode(
			array('colls' => array(array(
				'coll_name' => 'pages',
				'filter' => "module_name = 'page/" . $page_name . "'"
			)))
		));
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
		$page_result = json_decode($page_result);

		if (isset($page_result->Data)) {
			$page_result = $page_result->Data;
		}

		return $page_result;
	}

	// -- Fetch page frame properties
	public function getPageFrameProperties($pageId, $record_limit)
	{

		$CURLDATA = array('collections' => json_encode(
			array('colls' => array(array(
				'coll_name' => 'page_frame_properties',
				'filter' => "p._id = " . $pageId,
				'order' => 'pf.[order], fp.[order]',
				'page_num' => '0',
				'limit' => $record_limit
			)))
		));

		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$page_frame_properties = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
		$page_frame_properties = json_decode($page_frame_properties);

		if (isset($page_frame_properties->Data)) {
			$page_frame_properties = $page_frame_properties->Data;
		}

		return $page_frame_properties;
	}

	// -- Fetch  frame permission
	public function getFramePermission($frameID)
	{

		$CURLDATA_fper = array('collections' => json_encode(
			array('colls' => array(array(
				'coll_name' => 'frame_permissions',
				'filter' => "fper.group_id =" . $this->session->userdata('GroupID') . " AND fper.frame_id =" . $frameID,
				'order' => 'fper.[_id] desc',
				'page_num' => '0'
			)))
		));
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

		$page_result_group_permission_data = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_fper);


		$page_result_group_permission_data = json_decode($page_result_group_permission_data);

		if (isset($page_result_group_permission_data->Data)) {
			$page_result_group_permission_data = $page_result_group_permission_data->Data;
		}

		return $page_result_group_permission_data;
	}


	// -- Fetch page framePropertyId Quick Filter Values
	public function getQuickFilterValues($framePropertyId)
	{

		$CURLDATA = array('collections' => json_encode(
			array('colls' => array(array(
				'coll_name' => 'page_frame_properties',
				'filter' => "fp._id = " . $framePropertyId,
			)))
		));

		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$page_frame_properties = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
		$page_frame_properties = json_decode($page_frame_properties);

		if (isset($page_frame_properties->Data)) {
			$page_frame_properties = $page_frame_properties->Data;
			$getEditSource = $page_frame_properties['0']->Results['0'];

			if (!empty($getEditSource->EditSource)) {
				$coll_name = "";

				if (!empty(json_decode($getEditSource->EditValue)->DisplayProperties)) {
					$coll_name = json_decode($getEditSource->EditValue)->DisplayProperties[0];
				}

				if (!empty($coll_name)) {
					$CURLDATA = array('collections' => json_encode(
						array('colls' => array(array(
							'coll_id' => $getEditSource->EditSource
						)))
					));

					$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
					$collectionsData = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

					$collectionsData = json_decode($collectionsData)->Data[0]->Results;

					$displayProperties_data = [];
					foreach ($collectionsData as $key => $value) {
						array_push($displayProperties_data, $value->$coll_name);
					}
					$collectionsDataValues = array_unique($displayProperties_data);

					$displayProperties = [];
					foreach ($collectionsDataValues as $key => $value) {

						$roperties_values = [];
						array_push($roperties_values, $value);
						array_push($roperties_values, $value);

						array_push($displayProperties, $roperties_values);
					}
					return $displayProperties;
				}
			} else if (empty($getEditSource->EditSource) && !empty($getEditSource->EditValue)) {

				$editVal = $getEditSource->EditValue;

				$displayProperties = [];

				foreach (json_decode($editVal) as $key => $value) {
					$roperties_values = [];
					array_push($roperties_values, $key);
					array_push($roperties_values, $value);
					array_push($displayProperties, $roperties_values);
				}
				// print_r($displayProperties); exit;
				return $displayProperties;
			} else if (!empty($getEditSource->PropEditType) && $getEditSource->PropEditType == 'YN') {

				$displayProperties = [["1", "Yes"], ["0", "No"]];
				return $displayProperties;
			}
		}
		// print_r(array_unique($displayProperties)); exit;

	}

	public function getnotificationsData()
	{

		$CURLDATA = array('collections' => json_encode(
			array('colls' => array(array(
				'coll_name' => 'bsit_notifications',
				'filter' => "n.user_id = " . $this->session->userdata('UserID') . " AND (n.read_flag = 0 OR n.read_flag IS NULL)"
				//'filter' => "n.user_id = 2 AND (n.read_flag = 0 OR n.read_flag IS NULL)"

			)))
		));
		// print_r($CURLDATA); exit;
		$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
		$getnotificationsData = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
		return $getnotificationsData;
	}
}
