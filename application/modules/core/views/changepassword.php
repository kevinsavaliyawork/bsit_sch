<form class="bsit_form" id="form-change-password" role="form" data-has-error="0" name="form_change_password" method="POST" autocomplete="whatever" action="<?php echo base_url() . 'core/login/changepassword'; ?>">
  <div class="mui-textfield">
    <input type="password" name="old_password" id="old-password"   autocomplete="new-password" class="inputForm" required/>
    <label>Current Password</label>
  </div>
  <div class="mui-textfield">
    <input type="password" name="new_password"  autocomplete="new-password" id="newpassword" class="inputForm" required/>
    <label>New Password</label>
  </div>
  <div class="mui-textfield">
    <input type="password" name="confirm_password" id="confirm-password"   autocomplete="new-password" class="inputForm" required/>
    <label>Confirm new Password</label>
  </div>
  <div id="preloader-info" style="float: right"></div>
  <div id="change-password-error-report" ></div>
  <!-- <div class="row"> -->
    <div class="mui-col-md-12" style="text-align:center;">
      <button class="mui-btn mui-btn--primary mui-btn--flat bsit_save_btn" id="btn-change-password" type="submit">CHANGE PASSWORD</button>
      <button type="submit" class="mui-btn mui-btn--primary bsit_cancel_btn" data-dismiss="modal"> Cancel </button>
    </div>
  <!-- </div> -->
</form>
<script type="text/javascript">
  //Move this code block to a js file.
    var options = {
        minLength: 6,		  //Minimum Length of password
        minUpperCase: 0,	  //Minimum number of Upper Case Letters characters in password
        minLowerCase: 0,	  //Minimum number of Lower Case Letters characters in password
        minDigits: 1,		  //Minimum number of digits characters in password
        minSpecial: 0,		  //Minimum number of special characters in password
        maxRepeats: 5,		  //Maximum number of repeated alphanumeric characters in password dhgurAAAfjewd <- 3 A's
        maxConsecutive: 5,	  //Maximum number of alphanumeric characters from one set back to back
        noUpper: false,		  //Disallow Upper Case Lettera
        noLower: false,		  //Disallow Lower Case Letters
        noDigit: false,		  //Disallow Digits
        noSpecial: false,	  //Disallow Special Characters
        failRepeats: false,    //Disallow user to have x number of repeated alphanumeric characters ex.. ..A..a..A.. <- fails if maxRepeats <= 3 CASE INSENSITIVE
        failConsecutive: false,//Disallow user to have x number of consecutive alphanumeric characters from any set ex.. abc <- fails if maxConsecutive <= 3
        confirmField: '#confirm-password'
    };
    $(function () {
        $("#newpassword").passwordValidation(options,function(element, valid, match, failedCases) {
            $('#change-password-error-report').html(failedCases.join("\n"));
            if(!match && valid) {
                $('#change-password-error-report').html('Password and Confirm Password do not match');
            }
            if(valid && match) {
                $('#change-password-error-report').html('');
                $('#form-change-password').data('has-error',0);
            } else {
              $('#form-change-password').data('has-error',1);
            }
        });
        $(document).on('click', '#btn-change-password', function (e) {
            if (!$('#form-change-password')[0].checkValidity()) {
                $('#form-change-password')[0].reportValidity();
                return false;
            }
            //If is there any error in the form, we won't submit it.
            if(parseInt($('#form-change-password').data('has-error')) == 1) {
                e.preventDefault();
                return false;
            }
            $('#form-change-password').find('input').attr('readonly',true);
            setPasswordEncryption();
            var btnText = $(this).html();
            var btn = $(this);
            btn.hide();
            $('#preloader-info').html('<h6>Saving password...</h6>');
            $('#change-password-error-report').html('');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#form-change-password').attr('action'),
                data: $('#form-change-password').serializeArray(),
                success: function (data) {
                    if (data.success) {
                        Materialize.toast(data.msg,4000);
                        $('#change-password-modal').modal('hide');
                    } else {
                        $('#change-password-error-report').html(data.msg);
                        $('#form-change-password').find('input').val('').attr('readonly',false);
                    }
                    btn.show();
                    $('#preloader-info').html('');
                },
                error: function (jqXHR, exception) {
                    $('#change-password-error-report').html('Could not change password, please try again later.');
                    $('#form-change-password').find('input').val('').attr('readonly',false);
                    btn.show();
                    $('#preloader-info').html('');
                },
            });
        });

        $(document).on('submit', '#form-change-password', function (e) {
            if ($('#form-change-password')[0].checkValidity()) {
                e.preventDefault();
                return false;
            }
            $('#form-change-password')[0].reportValidity();
        });
    });
    var setPasswordEncryption = function () {
        var password = $('#newpassword').val();
        var key = CryptoJS.enc.Hex.parse("0123456789abcdef0123456789abcdef");
        var iv = CryptoJS.enc.Hex.parse("abcdef9876543210abcdef9876543210");
        var hash = CryptoJS.AES.encrypt(password, key, {iv: iv});
        $("#newpassword,#confirm-password").val(hash);
        var oldpassword = $('#old-password').val();
        var hash = CryptoJS.AES.encrypt(oldpassword, key, {iv: iv});
        $('#old-password').val(hash);
    }
</script>
