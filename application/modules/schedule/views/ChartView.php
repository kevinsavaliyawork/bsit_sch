

<script src='<?php echo base_url();?>Schedule/assets/js/Chart.bundle.js'></script>
 
 <script src='<?php echo base_url();?>Schedule/assets/js/ChartPieceLabel.js'></script>


    <link href="<?php echo base_url();?>Schedule/assets/css/style_dashboard.css" rel="stylesheet" type="text/css" />

<?php
$page_name = $this->uri->segment(2,0);

if (is_file(APPPATH.'modules/'.$frame['Properties'][0]->module_name.'/views/dashboard_params/'.$page_name.'.php')){
          $this->load->view('dashboard_params/'.$page_name);
        }

	$user = $_SESSION['username'];

	$html = '<div id="page_frame_'.$frame['PFID'].'_content" style="height:100%;" >';

    if (is_file(APPPATH.'modules/'.$frame['Properties'][0]->module_name.'/views/'.$frame['Properties'][0]->report_url.'.php')){

		$html .= $this->load->view($frame['Properties'][0]->report_url,'', true);
    }

	$html .= '</div>';
	echo $html;
?>
