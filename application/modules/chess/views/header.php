﻿<!doctype html>
<html>
 <!--this file inluded for js,css and for notes section and add colletion this is required to include -->
   <?php require_once APPPATH.'modules/core/views/header_include.php' ?>
<!-- TODO: Single PHP block to echo body? -->
<body>

<?php
if ($_GET['headerPage'] != 1) {
  
?>
<div id="sidedrawer" class="mui--no-user-select">
  <div id="sidedrawer-brand" class="mui--appbar-line-height mui--text-title">
    <a href="" target="_self">
      <img src="<?php echo base_url(); ?>assets/core/images/<?php echo $appIcon?>" class="logo-nav-menu"/>
    </a>
  <?php echo($appTitle)?>
  </div>
  <div class="mui-divider"></div>
  <?php echo($menu_data); ?>
</div>



<header id="header">

  <?php
    // If the user has permission, load the editbar
    if($this->session->userdata('Allowdesignmode') == 1) {
      echo('<script src="'.base_url().'core/assets/js/PageEdit.js"></script>');
      $this->load->view('editbar', true);
    }
?>


    <div class="mui-appbar mui-container-fluid mui--appbar-line-height">
      <table width="100%">
         <input type = "hidden" id="pageIdHeader" value="<?php echo $pageInfo->pageId; ?>">
        <tr style="vertical-align:middle;">
           <td class="mui--appbar-height">
          <a class="sidedrawer-toggle mui--visible-xs-inline-block js-show-sidedrawer">☰</a>
          <!-- Code start for Add page name in Header Added by HD -->
          <a class="sidedrawer-toggle mui--hidden-xs js-hide-sidedrawer"
            id="control_title">☰  <img src="<?php echo base_url();?>assets/core/images/chess.jpg" height="50px" width="50px" /></a>
          <span class="mui--text-title mui--visible-xs-inline-block">
            <?php
              echo strlen($pageInfo->Title) > 20 ?
                substr($pageInfo->Title, 0, 20)."..." :
                $pageInfo->Title;
            ?>
          </span>
          <!-- Code end for Add page name in Header Added by HD -->
        </td>

        
          <td class="mui--appbar-height" align="right">

            <?php
            // If user has permissions to edit pages, show the button to open the edit bar
            if($this->session->userdata('Allowdesignmode') == 1) {
                $btn = '<button id="editBtn" class="mui-btn mui-btn--primary">';
                $btn .= '<img src="'.base_url().'assets/core/images/ic_build_black_24dp_1x.png" style="display:inline-block; -webkit-filter:invert(1); filter:invert(1);">';
                $btn .= '</button>';
                echo($btn);
            }
          ?>

          
           <a href="<?php echo base_url();?>/core/Login/logout">
              <img src="<?php echo base_url();?>assets/core/images/logout-white.png" />
            </a>
          </td>
        </tr>
      </table>
    </div>
  </header>
  
<?php
}
?>

<div id="content-wrapper">
  <div class="mui--appbar-height"></div>

<div class="mui-container-fluid">
  <div class="space-top" style="display: none;"></div>
  <div id="content_part">
  


<?php
$curr_url = 'http://'.$_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI'];
$page_flg = 0;


foreach ($page_data_permission as $data) {
    if (($this->session->userdata('GroupID') == $data->GroupId) && (("page/".$this->uri->segment(2, 0) == $data->ModuleName || strpos(urldecode($_SERVER['REQUEST_URI']),$data->ModuleName) > 0))) {
        $page_flg = 1;
        break;
    } else {
        $page_flg = 0;
    }
}
//exit;
if (isset($page_flg_time)) {
    $page_flg = 1;
}
//echo $page_flg;exit;
// Pageheader Remove code added by HD
if ($page_flg == 1 || $curr_url == SITEURL.'home') {
    ?><h1><?php //echo($pageInfo->Title);?></h1><?php
}
?>
<?php
if ($page_flg == 1 || $curr_url == SITEURL.'home') {
    echo $page_content;
} else {
    include_once('Error_page.php');
}
?>
