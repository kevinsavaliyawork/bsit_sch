<div class="chart_main">
  <div class="chart_head">
    <div class="chart_title">Electricity Generation & Consumption</div>
  </div>
  <div class="chart_body" id="elec_chart_container" style="height:400px; max-height:400px">
    <canvas id="myElecChart"></canvas>
  </div>
  <div class="chart_footer">
    <button id="elec_btn_hour" class="elecChartPeriod dashboard-btn" value="Hour">Live 48 Hour</button>
    <button id="elec_btn_day" class="elecChartPeriod dashboard-btn" value="Day">Day</button>
    <button id="elec_btn_week" class="elecChartPeriod dashboard-btn" value="Week">Week</button>
    <input type="hidden" id="periodSelected" value="Hour" />
  </div>
</div>
