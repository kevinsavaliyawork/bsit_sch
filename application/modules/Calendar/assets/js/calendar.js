$(document).ready(function() {
	
	loadUserEvent();

function loadUserEvent(){

  let currentUser = $("#jobOwnerSelectDropdown").val();



    let requestData = {
        parameters: [
          { key: "@owner_id", value: currentUser },
        ],
        sp_name: [{ key: "BSIT_UserLoadEvents" }],
      };


    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      success: function(res) {
        
        $("#startStop").hide();
        $("#display").hide();
		$("#event-div").hide();

        if (res != "") {


          var hrs = new Date().getHours();
          var mins = new Date().getMinutes();
          var sec = new Date().getSeconds();


          $("#total_active_event").val(res[0].total_count);
          $("#startStop").attr('event_id',res[0]._id);

          if(res[0].total_count > 0){

            $("#startStop").show();
            $("#display").show();
			$("#event-div").show();

            $("#job_event_name").html(res[0].Name);
            $("#job_event_title").html(res[0].jobTitle);


            var dbdatetime = res[0].start_date_time;
            var finaltime = dbdatetime.split('T');





            $("#commit-event-startevent").hide();
            var allTime = hrs+':'+mins+':'+sec;


            var startTime=moment(finaltime[1], "HH:mm:ss");
            var endTime=moment(allTime, "HH:mm:ss");


            var duration = moment.duration(endTime.diff(startTime));
            var hours = parseInt(duration.asHours());
            var minutes = parseInt(duration.asMinutes())-hours*60;


            var finalTimeDiff = hours+':'+minutes+':'+'00';

            $("#display").html(finalTimeDiff);
            startStop();

          } else {
             $("#startStop").hide();
            $("#commit-event-startevent").show();
          }

          
        } 
      },
      error: function(e) {
        alert("Error: " + e);
      },
    });


}


  $(".js-example-basic-multiple").select2();

  // Initialise date/time pickers
  $(".datepicker").pickadate({
    format: "yyyy/mm/dd",
    formatSubmit: "yyyy/mm/dd",
  });

  $(".timepicker").pickatime({
    format: "HH:i",
    formatLabel: "<b>h</b>:i <!i>a</!i>",
    formatSubmit: "HH:i",
  });

  // hide the side drawer
  // $("#sidedrawer")
  //   .parent()
  //   .toggleClass("hide-sidedrawer");

  

  

        

  // Change event type
  $("#eventType").on("change", function() {
    eventTypeChange();
    eventTypeChangeSharedEvents();
  });

  function eventTypeChange() {
    let newType = $("#eventType").val();

    // if all day event, we need to change the hours of the job
    switch (newType) {
      case "allday":
        $("toDateSelector").prop("disabled", true);
        $("toTimeSelector").prop("disabled", true);

        // hide the material costs section
        $("#eventMaterialCosts").hide();
        $("#shared_event_div").show();
        break;
      case "material":
        $("toDateSelector").prop("disabled", true);
        $("toTimeSelector").prop("disabled", true);

        // show the material costs section
        $("#eventMaterialCosts").show();
        break;
      case "planned":
        $("#shared_event_div").show();
        $("#eventMaterialCosts").hide();
        break;
      default:
        $("toDateSelector").prop("disabled", false);
        $("toTimeSelector").prop("disabled", false);

        // hide the material costs section
        $("#eventMaterialCosts").hide();
        break;
    }
  }

  function eventTypeChangeSharedEvents() {
    let newType = $("#eventType").val();
    if (newType == "allday" || newType == "planned") {
      $("#shared_event_div").show();
    } else {
      $("#shared_event_div").hide();
    }
  }

  // ------------------------
  // -- SET MATERIAL PRICE FROM COST IF 0
  // ------------------------
  $("#MaterialCost").on("change", function() {
    let newCost = $(this).val();
    let charge = $("#MaterialChargePrice").val();

    // if cost is not zero and material charge price is zero
    if (newCost != 0 && charge == 0) {
      $("#MaterialChargePrice").val(newCost);
    }
  });

  // ------------------------
  // -- CHANGE OF FILTERS
  // ------------------------
  $("#jobOwnerSelectDropdown").on("change", function() {
    $("#calendar").fullCalendar("refetchEvents");
  });

  // ------------------------
  // -- SHOW HIDE JOBS IN LIST
  // ------------------------
  function hideShowAvailJobs() {
    // loop each of the options and either hide or show, depending on the selected customer
    $("#selectEventJobId option").each(function() {
      // does this customer number match the one selected?
      let $thisCustNum = $(this).data("cust");
      let $selCustNum = $("#selectEventCustomerId").val();
      if ($thisCustNum == $selCustNum) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });

    // unselect the selected option
    $("#selectEventJobId option:selected").removeAttr("selected");
    $("#selectEventJobId").val("");
  }

  /**
   * @param test test
   * @return other test
   */
  function ff() {}

  $("#selectEventCustomerId").on("change", function() {
    // PROJECT & JOBS FIELDS ARE BLANK - BASED ON CUSTOMER ONCHANGE EVENT
    $("#parent_job_title").val("");
    $("#sel_parent_job_id").val("");

    $("#job_title").val("");
    $("#sel_job_id").val("");

    hideShowAvailJobs();

    let parentJobID = $("#sel_parent_job_id").val();
    if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
      parentJobID = "";
    }

    // FUNCTION FOR LOAD JOB-PANEL BASED ON SELECT CUSTOMER
    $(".job_card_loader").show();
    getDataForJobCardPanel("", parentJobID, $("#selectEventCustomerId").val());
  });


  // made by add button event amit
  $("#mySelect").on('change', function () {
         var id = this.options[this.selectedIndex].id;
         
         if(id == "todaybutton"){
          $('#calendar').fullCalendar('changeView','agendaDay');
          } 
         if(id == "monthbutton"){
          $('#calendar').fullCalendar('changeView','month');
          } 
        if(id == "weekbutton"){
          $('#calendar').fullCalendar('changeView','agendaWeek');
          }

     });


    $('#prevbutton').click(function() {
         $('#calendar').fullCalendar('prev');
         $('#bsit_cal_week_day').html($(".fc-head").html());
         // $('#bsit_cal_schedules_event').html($(".fc-day-grid").html());
          $([document.documentElement, document.body]).animate({
        scrollTop: $("tr [data-time='05:00:00']").offset().top
    }, 1000);
    });

    $('#nextbutton').click(function() {
         $('#calendar').fullCalendar('next');
         $('#bsit_cal_week_day').html($(".fc-head").html());
         // $('#bsit_cal_schedules_event').html($(".fc-day-grid").html());
          $([document.documentElement, document.body]).animate({
        scrollTop: $("tr [data-time='05:00:00']").offset().top
    }, 1000);
    });

    // $('#monthbutton').click(function() {
    //      $('#calendar').fullCalendar('changeView','month');
    // });

    // $('#weekbutton').click(function() {
    //     $('#calendar').fullCalendar('changeView','agendaWeek');
    // });

    // $('#todaybutton').click(function() {
    //     $('#calendar').fullCalendar('changeView','agendaDay');
    // });

  // Initialise full calendar
  $("#calendar").fullCalendar({
    header: {
      left: "",
      right: "",
      // left: "prev,next today",
      // right: "month,agendaWeek,agendaDay",
    },
    scrollTime: "07:30",
    // height: "162px;",
    height: "auto",
    defaultView: "agendaWeek",
    selectable: true,
    selectHelper: true,
    select: function(start, end) {
      if (typeof event == "undefined") {
        showEventDialogue(undefined, start, end);
      } else {
        showEventDialogue(event.id, start, end);
      }

      $("#calendar").fullCalendar("unselect");
    },
    viewRender: function(view) {
      $("#cHeader").html(view.title);
    },
    editable: true,
    eventRender: function(event, element) {
      let disabledEvents = [];

      $.each(event.evt_disable_id, function(key, value) {
        disabledEvents.push(parseInt(value));
      });

      element.attr("href", "javascript:void(0);");

      // [START] Only show Popup the Invoiced Events-not changeable //
      if ($.inArray(parseInt(event.id), disabledEvents) > -1) {
        event.editable = false; // using for Stop the drag & drop event
        event.resizable = false;
        event.draggable = false;
      }

      element.click(function() {
        showEventDialogue(
          event.id,
          moment(event.start).format("YYYY/MM/DD HH:mm"),
          moment(event.end).format("YYYY/MM/DD HH:mm"),
        );
      });

      // [END] Only show Popup the Invoiced Events-not changeble //
    },

    // We reload events when the user changes the view date range
    events: function(start, end, timezone, callback) {
      // get the owner, and setup a filter for it
      let currentUser = $("#jobOwnerSelectDropdown").val();
      let userJson = JSON.stringify([
        {
          resourceId: currentUser,
        },
      ]);

      // event id was specified, load details from database using AJAX
      let filterStart = moment(start).format("YYYY/MM/DD HH:mm");
      let filterEnd = moment(end).format("YYYY/MM/DD HH:mm");
      let requestData = {
        parameters: [
          { key: "@start_date", value: filterStart },
          { key: "@end_date", value: filterEnd },
          { key: "@owner_id", value: currentUser },
          { key: "@owners_json", value: userJson },
        ],
        sp_name: [{ key: "BSIT_CalendarLoadEvents" }],
      };

      // Ajax call to the API to retrieve the list of events
      $.ajax({
        type: "POST",
        url: base_url + "core/Apilocal/Call_SP",
        data: requestData,
        dataType: "json",
        success: function(res) {
          if (typeof res[0] != "undefined") {
            let colorFg = "#000000";
            let colorBg = "#FFFFFF";
            let eventTitle = "";
            let events = [];
            let colorBorder = "";

            let disabledEventIds1 = "";
            let disabledEventIds = new Array();

            if (res[0]["disabled_events"] != null) {
              disabledEventIds1 = res[0]["disabled_events"].split(",");

              $.each(disabledEventIds1, function(i) {
                let index = $.inArray(disabledEventIds1[i], disabledEventIds);
                if (index == -1) {
                  disabledEventIds.push(parseInt(disabledEventIds1[i]));
                }
              });
            }

            $.each(res, function(key, value) {
              eventTitle = "";
              let colorJson = $.parseJSON(value["EventColor"]);
              if (typeof colorJson != "undefined") {
                colorBg = colorJson[0];
                colorFg = colorJson[1];
                colorBorder = "";
              }

              // set the css classes and specific settings for different job types
              let eventCSSClasses = [];
              let allDay = 0;
              switch (value["Type"]) {
                case "planned":
                  colorBg = colorJson[0] + "4D";
                  colorFg = colorJson[0];
                  colorBorder = colorJson[0] + "80";
                  eventCSSClasses.push("BSIT-CalPlannedEvent");
                  break;
                case "deleted":
                  eventCSSClasses.push("BSIT-CalDeletedEvent");
                  eventTitle = eventTitle + " ____ ";
                  break;
                case "material":
                  eventCSSClasses.push("BSIT-CalMaterialEvent");
                  eventTitle += " ";
                  break;
              }
              if(value["is_confirmed"]==1)
            {
                eventCSSClasses.push("BSIT-CalCompleteEvent");

            }

              if (value["allDay"] == 1) {
                allDay = 1;
              }

              // setup the event title
              eventTitle += value["Name"];

              // add the ref # to the event
              if (
                value["CustJobRefNum"] !== "" &&
                value["CustJobRefNum"] !== null
              ) {
                eventTitle =
                  eventTitle +
                  (value["CustJobRefNum"] == ""
                    ? ""
                    : ":" + value["CustJobRefNum"]);
              }

              // add the task title
              // If the event end time is empty, this is an all day event for a single day

              if (value["Type"] == "allday") {
                if (value["Title"] == null && value["jobTitle"] == null) {
                  eventTitle = eventTitle + " - " + value["Description"];
                }
              } else {
                eventTitle += ` -  ${
                  value["Title"] == null ? value["jobTitle"] : value["Title"]
                }`;
              }

              let startDateTime = value["StartDateTime"];
              let endDateTime = value["EndDateTime"];
              if (endDateTime == "0000-00-00 00:00:00") {
                endDateTime = startDateTime;
                allDay = 1;
              }

              // create the event if it hasn't been deleted
              if (value["Type"] != "deleted") {
                events.push({
                  id: value["key_id"],
                  title: eventTitle,
                  start: startDateTime,
                  end: endDateTime,
                  backgroundColor: colorBg,
                  borderColor:colorBorder,
                  textColor: colorFg,
                  allDay: allDay == 1 ? true : false,
                  className: eventCSSClasses,
                  type: value["Type"],
                  evt_disable_id: disabledEventIds,
                });
              }
            });
            callback(events);
          }
        },
        error: function(err) {
          console.log(err);
        },
      });
    },

    // when the event is repositioned, we must update the database
    eventDrop: function(event, delta, revertFunc) {
      let doUpdate = 1;

      // event id was specified, load details from database using AJAX
      let filterStart = moment(event.start).format("YYYY/MM/DD HH:mm");
      let filterEnd = moment(event.end).format("YYYY/MM/DD HH:mm");

      // if event type is deleted, don't allow moving
      if (event.type == "deleted") {
        revertFunc();
        doUpdate = 0;
      }

      // check if the end time is 00:00, this must be an allday event
      let updateEventTypeStr = "";

      // event type is currently all day, we need to change to something, planned for now
      if (event.allDay == 0 && event.type == "allday") {
        filterEnd = filterStart.add(moment.duration(2, "hours"));
        filterEnd = filterEnd.format("YYYY/MM/DD HH:mm");
        updateEventTypeStr = ',"Type":"planned"';
      }

      // if event type is currently not all day, but type is material,
      // user has tried to drop material to hours
      if (event.allDay == 0 && event.type == "material") {
        revertFunc();
        doUpdate = 0;
      }

      if (doUpdate == 1) {
        // setup the json string which will do the update
        let jsonStr =
          '{"colls": [{"coll_id": "15", "values": {"key_id":"' +
          event.id +
          '", "StartDateTime":"' +
          filterStart +
          '","EndDateTime":"' +
          filterEnd +
          '" ' +
          updateEventTypeStr +
          "}}]}";

        // update the single record details from the server
        $.ajax({
          type: "POST",
          url: base_url + "core/Apilocal/update",
          data: { data: jsonStr },
          dataType: "json",
          success: function(res) {},
          error: function(err) {
            revertFunc();
            alert("Error processing your request: " + err.responseText);
          },
        });
      }
    },

    // when an event is extended, we need to update the end date
    eventResize: function(event, delta, revertFunc) {
      let eventDisableId = [];

      $.each(event.evt_disable_id, function(key, value) {
        eventDisableId.push(parseInt(value));
      });

      if ($.inArray(parseInt(event.id), eventDisableId) > -1) {
        revertFunc();
        return false;
      }

      // event id was specified, load details from database using AJAX
      let filterStart = moment(event.start).format("YYYY/MM/DD HH:mm");
      let filterEnd = moment(event.end).format("YYYY/MM/DD HH:mm");
      let doUpdate = 1;

      if (doUpdate == 1) {
        let jsonStr = JSON.stringify({
          colls: [
            {
              coll_id: "15",
              values: {
                key_id: event.id.toString(),
                StartDateTime: filterStart,
                EndDateTime: filterEnd,
              },
            },
          ],
        });

        // load the single records details from the server
        $.ajax({
          type: "POST",
          url: base_url + "core/Apilocal/update",
          data: { data: jsonStr },
          dataType: "json",
          success: function(res) {},
          error: function(err) {
            revertFunc();
            alert("Error processing your request: " + err.responseText);
          },
        });
      }
    },
  });

  // ------------------------
  // -- EDIT/NEW EVENT DIALOG
  // ------------------------
  function showEventDialogue(eventId, startDateTime, endDateTime) {
    // get the event owner (current filter owner)
    let eventOwner = $("#jobOwnerSelectDropdown").val();
    let g = "f";
    let sendId = "0";
    // check if the event id is specified, if so we load details from DB
    if (eventId !== 0 && typeof eventId != "undefined") {
      // EDIT EVENT
      sendId = eventId;
    }

    let requestData = {
      parameters: [
        { key: "@event_id", value: sendId },
        { key: "@owner_id", value: eventOwner },
      ],
      sp_name: [{ key: "BSIT_CalendarLoadSingleEvent" }],
    };

    if (eventId === 0 || typeof eventId == "undefined") {
      $("#advancedSearch").hide();

      // FUNCTION FOR LOAD DEFAULT JOB-PANEL
      getDataForJobCardPanelDefault($("#jobOwnerSelectDropdown").val());
      getDataForNewJobCardPanel();
    }

    let jobRow = '<div class="mui-row" style="padding-bottom: 15px;">';

    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      beforeSend: function() {
        $(".loader").show();
      },
      success: function(res) {
        if (typeof res[0] != "undefined") {
          let results = res[0];

          $.each(results, function(key, value) {
            $("#addEditEventForm [name=" + key + "]").val(value);

            if (key == "jobId") {
              $("#addEditEventForm [name=jobId]").val("");
              $("#sel_job_id").val(results["jobId"]);
              $(".cust_back_color")
                .find("#job_box_" + results["jobId"])
                .show();

              $(".cust_back_color")
                .find("#job_box_" + results["jobId"])
                .children(".cust_color")
                .addClass("active");
            } else if (key == "ParentJobId") {
              $("#sel_parent_job_id").val(results["ParentJobId"]);
              $("#addEditEventForm [name=ParentJobID]").val(
                set_project_value_basedon_job(results["ParentJobId"], 1),
              );
            } else if (key == "WorkTypeId") {
              $("#selectWorkTypeId").val(results["WorkTypeId"]);
            } else if (key == "Type") {
              $("#eventType").val(results["Type"]);
            } else {
              $("#addEditEventForm [name=" + key + "]").val(value);
            }
          });

          if (eventId !== 0 && typeof eventId != "undefined") {
            jobRow +=
              '<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
              results["jobId"] +
              '">';
            jobRow += '<span class="cust_color active">';
            jobRow +=
              '<span class="job"><label>Job:</label><span class="job_card_title">' +
              results["jobTitle"] + ' (' + results["jobId"]+')' +
              "</span></span>";

            jobRow +=
              '<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
              results["ParentJobId"] +
              '">' +
              results["parent_job_name"] +
              "</span></span>";

            // Setup customer colours
            const colors = results["EventColor"].split(",", 10).map((col) => {
              let idx = col.indexOf("#");
              return col.substring(idx, idx + 7);
            });

            jobRow +=
              '<span class="icon"><span class="job_card_customer" data-cid="' +
              results["orgsId"] +
              '" style="float: left; background-color:' +
              colors[0] +
              ";color:" +
              colors[1] +
              ';padding: 0 5px 0px;border-radius: 3px; margin-right:5px;">' +
              results["Name"] +
              "</span></span>";

            jobRow +=
              '<span class="job_load_status" style="display: none;">' +
              results["Status"] +
              "</span>";
            jobRow += "</span>";
            jobRow += "</div>";

            $("#job_list").html(jobRow);
            // [START] Only show Popup the Invoiced Events-not changeble //
            // LOAD STATUS BASED ON PARENT JOB
            getJobStatus(results["ParentJobId"], results["Status"]);

            // [START] Only show Popup the Invoiced Events-not changeble //

            let otherUserEvents = [];
            if (results["disabled_events_from_shared_events"] != null) {
              let otherUserEventData = results[
                "disabled_events_from_shared_events"
              ].split(",");

              $.each(otherUserEventData, function(i) {
                let index = $.inArray(otherUserEventData[i], otherUserEvents);
                if (index == -1) {
                  otherUserEvents.push(parseInt(otherUserEventData[i]));
                }
              });
            }

            if (
              jQuery.inArray(eventId, otherUserEvents) > -1 ||
              results["disabled_events_from_invoiced"] == 1
            ) {
              $("#commit-event-addedit").hide();
              $("#add_job_box").hide();
              $("#submit_button").css("padding-bottom", "20px");
            } else {
              $("#commit-event-addedit").show();
              $("#add_job_box").show();
              $("#submit_button").css("padding-bottom", "0");
            }

            // [END] Only show Popup the Invoiced Events-not changeble //

            /* SharedEvent Selected value for each Event */
            let sharedEventsDataDropdown = results[
              "users_for_shared_events_dropdown"
            ].split(",");
            for (let i = 0; i < sharedEventsDataDropdown.length; i++) {
              sharedEventsDataDropdown[i] = parseInt(
                sharedEventsDataDropdown[i],
              );
            }
            $("#shared_user_id")
              .val(sharedEventsDataDropdown)
              .trigger("change");

            /* SharedEvent Selected value for each Event */

            if (results["jobTitle"].length > 125) {
              results["jobTitle"] =
                results["jobTitle"].substring(0, 125) + "...";
            }

            if (results["parent_job_name"].length > 75) {
              results["parent_job_name"] =
                results["parent_job_name"].substring(0, 75) + "...";
            }

            $("#MaterialCost").val(results["MaterialCost"]);
            $("#MaterialChargePrice").val(results["MaterialChargePrice"]);
            $("#parent_job_title").val(results["parent_job_name"]);
            $("#sel_parent_job_id").val(results["ParentJobId"]);
            $("#selectEventCustomerId").val(results["orgsId"]);

            $("#selectWorkTypeId").val(results["WorkTypeId"]);
            $("#internal_notes").val(results["InternalNotes"]);
            $("#description").val(results["Description"]);

            // show the dialogue
            $("#eventContent").modal();

             $(`.modal-backdrop`).on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();
              return false;
            });

          } else {
            // OPEN POPUP FOR ADD EVENT - ALL THE DATA(FIELDS) ARE RESET
            // set the Work-Type
            $("#addEditEventForm input")
              .first()
              .focus();
            $("#commit-event-addedit").show();
            $("#submit_button").css("padding-bottom", "0");

            $("#MaterialCost").val("");
            $("#MaterialChargePrice").val("");
            $("#parent_job_title").val("");
            $("#sel_parent_job_id").val("");
            $("#selectEventCustomerId").val("");
            $("#internal_notes").val("");
            $("#description").val("");
            $("#status").val("");
            $("#status").html('<option value=""></option>');

            // show the dialogue
            $("#eventContent").modal();

             $(`.modal-backdrop`).on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();
              return false;
            });
             
          }
        }
      },
      error: function(err) {},
      complete: function() {
        // if the ajax call is successful or not, set the owner to current selection
        $("#eventOwner").val(eventOwner);

        // run the event type update processes
        eventTypeChange();

        $(".loader").hide();

        if ($(window).width() > 1366) {
          setTimeout(function() {
            let jobCardHeight =
              $("#eventContent").outerHeight() -
              ($(".event_content_header").outerHeight() +
                $(".event_content_footer").outerHeight() +
                $(".event_content_footer2").outerHeight());
            jobCardHeight = jobCardHeight - 150;

            $(".cust_back_color").css("min-height", jobCardHeight);
            $(".cust_back_color").css("max-height", jobCardHeight);
          }, 200);
        }
      },
    });

    $("#addEditEventForm input")
      .first()
      .focus();
    if (eventId !== 0 && typeof eventId != "undefined") {
    } else {
      $("#addEditEventForm :input").each(function() {
        this.value = "";
      });

      // set the job type to actual as default
      $("#eventType").val("actual");

      // set the owner
      $("#eventOwner").val(eventOwner);

      // set the list of jobs to the selected customer.
      hideShowAvailJobs();

      // run the event type update processes
      eventTypeChange();
    }

    /* [START] Shows Shared_events_users in ADD Form */
    if (
      $("#addEditEventForm [name=Type]").val() == "planned" ||
      $("#addEditEventForm [name=Type]").val() == "allday"
    ) {
      $("#shared_event_div").show();
    } else {
      $("#shared_event_div").hide();
    }

    /* [END] Shows Shared_events_users in ADD Form */

    // set the values of each of the form components for edit and save
    let startTime = moment(startDateTime).format("HH:mm");
    let endTime = moment(endDateTime).format("HH:mm");
    let startDate = moment(startDateTime).format("YYYY/MM/DD");
    let endDate = moment(endDateTime).format("YYYY/MM/DD");

    // set the selectable date and time fields
    $("#fromDateSelector").val(startDate);
    $("#fromTimeSelector").val(startTime);
    $("#toDateSelector").val(endDate);
    $("#toTimeSelector").val(endTime);

    // set the hidden inputs
    $("#startTime").val(startDate + " " + startTime);
    $("#endTime").val(endDate + " " + endTime);

    let eventDuration = getEventDuration(
      startDate + " " + startTime,
      endDate + " " + endTime,
    );
    $("#event_duration").html(eventDuration);
  }
  
  
  $("#cust_job_title")
    .autocomplete({
      source: function(request, response) {

        // get data variables from the button
        let originalColumns = this.element.attr("data-columns");
        let collection_id = this.element.attr('data-coll');
        let columns = originalColumns.replace(/'/g, '"');
        let columnArray = jQuery.parseJSON(columns);
        var filter_val    = request['term']; 


        var filter_fields   = originalColumns.split(',')
        var filter_field1   = filter_fields[1].replace(/[\[\]']+/g,'');
        var filter_field2   = filter_fields[0].replace(/[\[\]']+/g,'');
        var alias_dropdown  = get_alias_for_filter_dropdown(collection_id); // getting alias for particular dropdown of Pages
        var filt_1      = alias_dropdown+'.'+filter_field1+' like \'%' + filter_val + '%\''; //GET FILTER NAME
        var filt_2      = alias_dropdown+'.'+filter_field2+' like \'%' + filter_val + '%\''; //GET FILTER ID
        var filter_final  = filt_1+' OR '+filt_2; //FILTER BY NAME OR ID
        
        //var json_str    = '[{"collID": '+collection_id+', "cols": '+columns + ', "filter":"'+filt_1+'"}]';
        var json_str    = '[{"collID": '+collection_id+', "cols": '+columns + ', "filter":"'+filter_final+'"}]';


        $.ajax({
          type: "POST",
          url: base_url+"core/Apilocal/dropdownlist",
          data:  {data: json_str} ,
          dataType: "json",
          success: function(res) {


            var responseData =  res[0]['Results'];

            
      let tempArray = [];
       let returnArray = [];

            $.each(responseData, function(index, value) {
              

              var i = 0;
        
              tempArray = {'label':"",'value':""};
              $.each(columnArray, function(col_index, col_val) {



                // put the first column as the value, concat all others for label
                if (i == 0) {
                  
                  tempArray["value"] = value[col_val];

                } else if (i == 3) {
                } else {
                  if (value[col_val] != "" && value[col_val] != null) {
                    if (tempArray["label"] != "") {
                      tempArray["label"] =
                        value[col_val] + " - " + tempArray["label"];
                    } else {
                      tempArray["label"] = value[col_val];
                    }
                  }
                }

                // increment I
                i = i + 1;
              });




              // if label is empty simple convert label to value column
              if (tempArray["label"] == "") {
                tempArray["label"] = tempArray["value"];
              }



              // append this row to the array we return
              returnArray.push(tempArray);
            });
            response(returnArray);
          },
        });
      },
      select: function(event, ui) {
        $(".autoCompColl_cust_job").val(ui.item.label);
        $("#selectEventCustomerId").val(ui.item.value); // Set Parent Job

        
        $("#parent_job_title").val("");
    $("#sel_parent_job_id").val("");

    $("#job_title").val("");
    $("#sel_job_id").val("");

    hideShowAvailJobs();

    let parentJobID = $("#sel_parent_job_id").val();
    if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
      parentJobID = "";
    }

    let completeStatus = 2;
      let jobCheckboxStatus = 2;
let opportunityStatus = 0;

      if ($("#checkbox-checked").is(":checked")) {
        completeStatus = 1;
      }
	  
	  if ($("#checkbox-opportunities").is(":checked")) {
        opportunityStatus = 1;
      }

      if ($("#job-checkbox-checked").is(":checked")) {
        jobCheckboxStatus = 1;
      }



    // FUNCTION FOR LOAD JOB-PANEL BASED ON SELECT CUSTOMER
    $(".job_card_loader").show();
    getDataForJobCardPanel("", parentJobID, $("#selectEventCustomerId").val(),completeStatus,jobCheckboxStatus,opportunityStatus);


        // Prevent the widget from inserting the value.
        return false;
      },
      focus: function(event, ui) {},
      minLength: 0,
    })


  

  // -----------------------------------------------
  // -- CODE FOR PROJECT(PARENT-JOB) FIELD DROP-DOWN
  // -----------------------------------------------
  $("#parent_job_title")
    .autocomplete({
      source: function(request, response) {
        // get data variables from the button
        let originalColumns = this.element.attr("data-columns");
        let columns = originalColumns.replace(/'/g, '"');
        let columnArray = jQuery.parseJSON(columns);

        let parentJobID = $("#parent_job_title").val();
        let customerId = $("#selectEventCustomerId").val();

        let requestData = {
          parameters: [
            { key: "@job_name", value: parentJobID },
            { key: "@cust_id", value: customerId },
          ],
          sp_name: [{ key: "BSIT_CalendarParentJobDropdownList" }],
        };

        $.ajax({
          type: "POST",
          url: base_url + "core/Apilocal/Call_SP",
          data: requestData,
          dataType: "json",
          success: function(res) {
            let curr_date = new Date();
            let returnArray = [];
            let jobsInEvent = "";
			let tempArray = [];

            $.each(res, function(index, value) {
              if (
                value["jobs_in_event"] != "" &&
                value["jobs_in_event"] != null
              ) {
                jobsInEvent = value["jobs_in_event"].split(",");
                for (var i = 0; i < jobsInEvent.length; i++) {
                  jobsInEvent[i] = parseInt(jobsInEvent[i]);
                }
              }

              if ($.inArray(value["key_id"], jobsInEvent) < 0) {
                if (
                  moment(curr_date).format("YYYY-MM-DD") ==
                  moment(value["key_create_date"]).format("YYYY-MM-DD")
                ) {
                  // RESENT JOB USE (+) ICON
                  tempArray = {
                    label: "",
                    value: "",
                    custId: "",
                    JobStatus: "new",
                  };
                } else {
                  tempArray = {
                    label: "",
                    value: "",
                    custId: "",
                    JobStatus: "",
                  };
                }
              } else {
                tempArray = {
                  label: "",
                  value: "",
                  custId: "",
                  JobStatus: "used",
                }; // JOB USED BY OTHER USERS THEN USE (clock) ICON
              }
              i = 0;
			  

              $.each(columnArray, function(col_index, col_val) {
                // put the first column as the value, concat all others for label
                if (i == 0) {
                  tempArray["value"] = value[col_val];
                } else if (i == 3) {
                } else {
                  if (value[col_val] != "" && value[col_val] != null) {
                    if (tempArray["label"] != "") {
                      tempArray["label"] =
                        value[col_val] + " - " + tempArray["label"];
                    } else {
                      tempArray["label"] = value[col_val];
                    }
                  }
                }

                // increment I
                i = i + 1;
              });

              // if label is empty simple convert label to value column
              if (tempArray["label"] == "") {
                tempArray["label"] = tempArray["value"];
              }

              // append this row to the array we return
              returnArray.push(tempArray);
            });
            response(returnArray);
          },
        });
      },
      select: function(event, ui) {
        $(".autoCompColl_parent_job").val(ui.item.label);
        $("#sel_parent_job_id").val(ui.item.value); // Set Parent Job

        let jobId = $("#job_title").val();
        let customerId = $("#selectEventCustomerId").val();
        let parentJobID = $("#sel_parent_job_id").val();

        // FUNCTION FOR LOAD JOB-PANEL BASED ON SELECTED CUSTOMER & PROJECT
        $(".job_card_loader").show();
        getDataForJobCardPanel(jobId, parentJobID, customerId);

        // Prevent the widget from inserting the value.
        return false;
      },
      focus: function(event, ui) {},
      minLength: 0,
    })
    .bind("focus", function() {
      $(this).autocomplete("search");
    })
    .data("ui-autocomplete")._renderItem = function(ul, item) {
    let imgIcon = "";
    if (item.JobStatus == "new") {
      imgIcon = "<img src='" + base_url + "assets/core/images/plus.png'>";
    } else if (item.JobStatus == "used") {
      imgIcon = "<img src='" + base_url + "assets/core/images/history.png'>";
    }

    return $("<li></li>")
      .data("item.autocomplete", item)
      .append(imgIcon + " " + item.label)
      .appendTo(ul);
  };

  $("#job_parent_job_title")
    .autocomplete({
      source: function(request, response) {
        // get data variables from the button
        let originalColumns = this.element.attr("data-jobcolumns");
        let columns = originalColumns.replace(/'/g, '"');
        let columnArray = jQuery.parseJSON(columns);

        let parentJobID = $("#jobForm [name=job_ParentJobID]").val();
        let customerId = $("#job_selectEventCustomerId").val();

        let requestData = {
          parameters: [
            { key: "@job_name", value: parentJobID },
            { key: "@cust_id", value: customerId },
          ],
          sp_name: [{ key: "BSIT_CalendarParentJobDropdownList" }],
        };

        $.ajax({
          type: "POST",
          url: base_url + "core/Apilocal/Call_SP",
          data: requestData,
          dataType: "json",
          success: function(res) {
            let currentDate = new Date();
            let returnArray = [];
			let tempArray = {};
            var jobsInEvent = "";

            $.each(res, function(index, value) {
              if (
                value["jobs_in_event"] != "" &&
                value["jobs_in_event"] != null
              ) {
                jobsInEvent = value["jobs_in_event"].split(",");
                for (var i = 0; i < jobsInEvent.length; i++) {
                  jobsInEvent[i] = parseInt(jobsInEvent[i]);
                }
              }

              if ($.inArray(value["key_id"], jobsInEvent) < 0) {
                if (
                  moment(currentDate).format("YYYY-MM-DD") ==
                  moment(value["key_create_date"]).format("YYYY-MM-DD")
                ) {
                  // RESENT JOB USE (+) ICON
                  tempArray = {
                    label: "",
                    value: "",
                    custId: "",
                    JobStatus: "new",
                  };
                } else {
                  tempArray = {
                    label: "",
                    value: "",
                    custId: "",
                    JobStatus: "",
                  };
                }
              } else {
                tempArray = {
                  label: "",
                  value: "",
                  custId: "",
                  JobStatus: "used",
                }; // JOB USED BY OTHER USERS THEN USE (clock) ICON
              }
              i = 0;

              $.each(columnArray, function(col_index, col_val) {
                // put the first column as the value, concat all others for label
                if (i == 0) {
                  tempArray["value"] = value[col_val];
                } else if (i == 3) {
                  // tempArray['custId'] = value[col_val];
                } else {
                  if (value[col_val] != "" && value[col_val] != null) {
                    if (tempArray["label"] != "") {
                      tempArray["label"] =
                        value[col_val] + " - " + tempArray["label"];
                    } else {
                      tempArray["label"] = value[col_val];
                    }
                  }
                }

                i++;
              });

              // if label is empty simple convert label to value column
              if (tempArray["label"] == "") {
                tempArray["label"] = tempArray["value"];
              }

              // append this row to the array we return
              returnArray.push(tempArray);
            });

            response(returnArray);
          },
        });
      },
      select: function(event, ui) {
        $(".job_autoCompColl_parent_job").val(ui.item.label);

        // Set Parent Job
        $("#job_sel_parent_job_id").val(ui.item.value);
        let workTypeId = getUserWorkTypeId($("#jobOwnerSelectDropdown").val());

        if (workTypeId == "") {
          workTypeId = 1;
        }

        $("[name=job_workflow_id]").prop("disabled", false);
        $("#job_selectWorkTypeId").val(workTypeId);
        $("[name=job_workflow_id]").val(workTypeId);

        // LOAD STATUS BASED ON PARENT JOB
        load_status_based_on_project_for_addJobModel(
          $("#job_sel_parent_job_id").val(),
          0,
        );

        // Prevent the widget from inserting the value.
        return false;
      },
      focus: function(event, ui) {},
      minLength: 1,
    })
    .data("ui-autocomplete")._renderItem = function(ul, item) {
    let imgIcon = "";
    if (item.JobStatus == "new") {
      imgIcon = "<img src='" + base_url + "assets/core/images/plus.png'>";
    } else if (item.JobStatus == "used") {
      imgIcon = "<img src='" + base_url + "assets/core/images/history.png'>";
    }

    return $("<li></li>")
      .data("item.autocomplete", item)
      .append(imgIcon + " " + item.label)
      .appendTo(ul);
  };

  // --------------------------------
  // -- CODE FOR JOBS FIELD DROP-DOWN
  // --------------------------------
  // ---------- [END] Task id-19 [AJAX Job list when entering event] ---------- //

  $("#checkbox-opportunities").click(function () {
    var isChecked = $(this).is(":checked");
    if (isChecked) {
          let jobId = "Opportunities";
          let parentJobID = $("#sel_parent_job_id").val();
          let customerId = $("#selectEventCustomerId").val();
          let completeStatus = 2;
          let jobCheckboxStatus = 2;
          let opportunityStatus = 0;

          if ($("#checkbox-checked").is(":checked")) {
            completeStatus = 1;
          }
        
          if ($("#checkbox-opportunities").is(":checked")) {
            opportunityStatus = 1;
          }

          if ($("#job-checkbox-checked").is(":checked")) {
            jobCheckboxStatus = 1;
          }

          if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
            parentJobID = "";
          }

        // Load job card based on search query
        $(".job_card_loader1").show();
            
            if (jobId == "") {
              getDataForJobCardPanelDefault($("#jobOwnerSelectDropdown").val());
              getDataForNewJobCardPanel();
            } else {
              getDataForJobCardPanel(jobId, parentJobID, customerId, completeStatus,jobCheckboxStatus,opportunityStatus);
              getDataForNewJobCardPanel(
                jobId,
                parentJobID,
                customerId,
                completeStatus == 2 ? 0 : completeStatus,
            jobCheckboxStatus == 2 ? 0 : jobCheckboxStatus,
            opportunityStatus == 2 ? 0 : opportunityStatus,
              );
            }
      }
  });


  $("#job_title").autocomplete({
    minLength: 0,
    delay: 300,
    search: function(event, ui) {
      let jobId = $(this).val();
      let parentJobID = $("#sel_parent_job_id").val();
      let customerId = $("#selectEventCustomerId").val();
      let completeStatus = 2;
	  let jobCheckboxStatus = 2;
	  let opportunityStatus = 0;

      if ($("#checkbox-checked").is(":checked")) {
        completeStatus = 1;
      }
	  
	  if ($("#checkbox-opportunities").is(":checked")) {
        opportunityStatus = 1;
      }

	  
	  if ($("#job-checkbox-checked").is(":checked")) {
        jobCheckboxStatus = 1;
      }

      if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
        parentJobID = "";
      }

      // Load job card based on search query
      $(".job_card_loader1").show();
      if (jobId == "") {
        getDataForJobCardPanelDefault($("#jobOwnerSelectDropdown").val());
        getDataForNewJobCardPanel();
      } else {
        getDataForJobCardPanel(jobId, parentJobID, customerId, completeStatus,jobCheckboxStatus,opportunityStatus);
        getDataForNewJobCardPanel(
          jobId,
          parentJobID,
          customerId,
          completeStatus == 2 ? 0 : completeStatus,
		  jobCheckboxStatus == 2 ? 0 : jobCheckboxStatus,
		  opportunityStatus == 2 ? 0 : opportunityStatus,
        );
      }
    },
  });

  $(document).on("click", ".job_box", async function(e) {
    $("#add_job_div").css("display", "none");
    $("#commit-event-addedit").prop("disabled", true);

    let jobCardId = $(this)
      .attr("id")
      .split("_");
    jobCardId = jobCardId[jobCardId.length - 1];

    $(".cust_back_color")
      .find(".cust_color")
      .removeClass("active");
    $("#job_list").css("border", "none");

    $(this)
      .find(".cust_color")
      .addClass("active");

    $("#sel_job_id").val(jobCardId);

    $("#job_title").val(
      $(this)
        .find(".job_card_title")
        .text(),
    );

    $("#parent_job_title").val(
      $.trim(
        $(this)
          .find(".job_card_project")
          .text(),
      ),
    );

    $("#sel_parent_job_id").val(
      $(this)
        .find(".job_card_project")
        .attr("data-pid"),
    );

    let jobStatus = $(this)
      .find(".job_load_status")
      .text();

    let parentJobId = $("#sel_parent_job_id").val();
    if (parentJobId != "") {
      await getJobStatus(parentJobId, jobStatus); // LOAD STATUS BASED ON PARENT JOB
    }

    await getJobWorkType(jobCardId); // LOAD STATUS BASED ON PARENT JOB

    $("#selectEventCustomerId option")
      .removeAttr("selected")
      .filter(
        "[value=" +
          $(this)
            .find(".job_card_customer")
            .attr("data-cid") +
          "]",
      )
      .attr("selected", true);

    $("#selectEventCustomerId").val(
      $(this)
        .find(".job_card_customer")
        .attr("data-cid"),
    );

    $("#status").val(
      $(this)
        .find(".job_load_status")
        .text(),
    );

    $("#commit-event-addedit").prop("disabled", false);
  });

  // LOAD STATUS BASED ON PARENT JOB
  $("#parent_job_title").on("change", function() {
    getJobStatus($("#sel_parent_job_id").val(), 0);
  });

  // ------------------------
  // -- COMMIT EDIT OR ADD NEW FUNCTIONALITY
  // ------------------------
  $(".commit-event-addedit")
    .button()
    .on("click", function() {
      // copy the user edited date and time into the submitting form inputs
	  let is_event_started = $(this).attr("is_event_started");
      let startTime = $("#fromTimeSelector").val();
      let endTime = $("#toTimeSelector").val();
	  
	  if(is_event_started == 1){

        var date = new Date();
        var min = date.getMinutes();
        var hrs = date.getHours();
		
		var endmin = date.getMinutes();
        var endhrs = date.getHours();
    

            if(min >= '00' && min < "30")
            {

             min = "00";
            }
            else
            {
              min = "30";
            }

          startTime = hrs+":"+min;
		  
		  
            if(endmin >= '00' && endmin < "30")
            {

                endmin = "30";
            }
            else
           {
            endmin = "00";
            endhrs = parseInt(hrs) + parseInt(1);
           }

          endTime = endhrs+":"+endmin;

      }
	  
      let startDate = $("#fromDateSelector").val();
      let endDate = $("#toDateSelector").val();

      let activeSubMenu = $("#all_job_list .job_box .active").length;

      if (activeSubMenu <= 0) {
        $("#all_job_list").css("border", "1px solid red");
        return false;
      } else {
        $("#all_job_list").css("border", "none");
      }

      // set the hidden input
      $("#startTime").val(`${startDate} ${startTime}`);
      $("#endTime").val(`${endDate} ${endTime}`);

      // setup the variable to talk with the submitting form
      let form = $("#addEditEventForm");
      let values = form.serializeObject();
      let parentJobId = "";

      if (values["ParentJobID"] != "") {
        parentJobId = $("#sel_parent_job_id").val();
      }

      delete values["ParentJobID"];
      delete values["TermsCode"];
      delete values["CustRefNum"];
      delete values["Status"];
      delete values["name"];
      delete values["shared_user"];

      values["Type"] = $("#eventType").val();
      values["WorkTypeId"] = $("#selectWorkTypeId").val();

      let eventType = $("#eventType").val(); // TaskId-40
      let jobId = $("#sel_job_id").val(); // TaskId-40

      let jsonValues = JSON.stringify(values);

      jsonValues = JSON.parse(jsonValues);
      jsonValues["jobId"] = jobId;
      jsonValues["ParentJobId"] = parentJobId;
      jsonValues = JSON.stringify(jsonValues);

      let eventId = $("#addEditEventForm [name=key_id]").val();

      // [START] - TaskId-40(Job required) //

      if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
        // ParentJobID FIELD IS REQUIRED
        $("#parent_job_title").addClass("required_border_class");
        return false;
      } else {
        $("#parent_job_title").removeClass("required_border_class");
      }
	  
	  if(is_event_started == 0){

      if (
        Date.parse(startDate + " " + startTime) >=
          Date.parse(endDate + " " + endTime) ||
        (startDate == "" || startTime == "" || endDate == "" || endTime == "")
      ) {
        $("#fromDateSelector").css({ "border-color": "#f44336" });
        $("#fromTimeSelector").css({ "border-color": "#f44336" });
        $("#toDateSelector").css({ "border-color": "#f44336" });
        $("#toTimeSelector").css({ "border-color": "#f44336" });
        return false;
      } else {
        $("#fromDateSelector").css({ "border-color": "seashell" });
        $("#fromTimeSelector").css({ "border-color": "seashell" });
        $("#toDateSelector").css({ "border-color": "seashell" });
        $("#toTimeSelector").css({ "border-color": "seashell" });
      }
	 }
	  
	  
      if (
        eventType == "actual" ||
        eventType == "actual-nonbillable" ||
        eventType == "material"
      ) {
        if (
          $("#addEditEventForm [name=jobId]").val() == "" &&
          $("#sel_job_id").val() == ""
        ) {
          $("#addEditEventForm [name=Description]").css({
            "border-color": "rgba(0, 0, 0, 0.26)",
            "border-width": "medium medium 1px",
          });

          $("#job_title").css({
            "border-color": "#f44336",
            "border-width": "2px",
          });

          return false;
        } else {
          $("#job_title").css({
            "border-color": "rgba(0, 0, 0, 0.26)",
            "border-width": "medium medium 1px",
          });
        }
      } else if (eventType == "planned" || eventType == "allday") {
        if ($("#addEditEventForm [name=Description]").val() == "") {
          $("#job_title").css({
            "border-color": "rgba(0, 0, 0, 0.26)",
            "border-width": "medium medium 1px",
          });
          $("#addEditEventForm [name=Description]").css({
            "border-color": "#f44336",
            "border-width": "2px",
          });
          return false;
        } else {
          $("#addEditEventForm [name=Description]").css({
            "border-color": "rgba(0, 0, 0, 0.26)",
            "border-width": "medium medium 1px",
          });
        }
      }

      // [END] - TaskId-40(Job required) //
      let url = "";
      // check if the event id is specified, if so we create new or save edited
      if ((eventId !== 0 && typeof eventId != "undefined") || eventId == "") {
        url = base_url + "core/Apilocal/update";
      }

      // if the customer ID is null, don't save, alter the user
      let customerId = $("#selectEventCustomerId").val();

      if (customerId == "" || customerId == null || customerId == "undefined") {
        $("#selectEventCustomerId")
          .fadeIn(100)
          .fadeOut(100)
          .fadeIn(100)
          .fadeOut(100)
          .fadeIn(100);

        $("#selectEventCustomerId").css({
          "border-color": "#f44336",
          "border-width": "2px",
        });

        return false;
      } else {
        $("#selectEventCustomerId").css({
          "border-color": "rgba(0, 0, 0, 0.26)",
          "border-width": "medium medium 1px",
        });
      }

      let jsonValues1 = JSON.parse(jsonValues);
      let jobKeyId = "";

      if ((eventId !== 0 && typeof eventId != "undefined") || eventId == "") {
        jobKeyId = jsonValues1["key_id"];
      } else {
        jobKeyId = "0";
      }

      let sharedEvents = $("#shared_user_id").val();
      let shared_events_data = "";
      if (sharedEvents != null) {
        shared_events_data = sharedEvents.toString(); // Convert to String
      } else {
        shared_events_data = "0";
      }

      $(".select2-selection__choice").remove();

      if (jsonValues1["MaterialCost"] == "") {
        jsonValues1["MaterialCost"] = 0;
      }

      if (jsonValues1["MaterialChargePrice"] == "") {
        jsonValues1["MaterialChargePrice"] = 0;
      }

      if (jsonValues1["Type"] != "material") {
        jsonValues1["MaterialCost"] = 0;
        jsonValues1["MaterialChargePrice"] = 0;
      }

      if (jsonValues1["jobId"] == 0) {
        return false;
      }

      // If the event both starts and ends at 12:00 am its an all day event
      let allDay = 0;
      let eventStart = moment(jsonValues1["StartDateTime"]).format("hh:mm a");
      let eventEnd = moment(jsonValues1["EndDateTime"]).format("hh:mm a");

      if (eventStart == eventEnd && eventStart == "12:00 am") {
        allDay = 1;
      }

      let requestData = {
        parameters: [
          { key: "@key_id", value: jobKeyId },
          {
            key: "start_date_time",
            value: moment(jsonValues1["StartDateTime"]).format(
              "YYYY-MM-DD HH:mm",
            ),
          },
          {
            key: "end_date_time",
            value: moment(jsonValues1["EndDateTime"]).format(
              "YYYY-MM-DD HH:mm",
            ),
          },
          { key: "@owner_id", value: jsonValues1["ownerId"] },
          { key: "@type", value: jsonValues1["Type"] },
          { key: "@job_id", value: jsonValues1["jobId"] },
          { key: "@material_cost", value: jsonValues1["MaterialCost"] },
          {
            key: "@material_charge_price",
            value: jsonValues1["MaterialChargePrice"],
          },
          { key: "@orgs_id", value: $("#selectEventCustomerId").val() },
          { key: "@description", value: jsonValues1["Description"] },
          { key: "@internal_notes", value: jsonValues1["InternalNotes"] },
          { key: "@work_type_id", value: jsonValues1["WorkTypeId"] },
          { key: "@shared_events_data", value: shared_events_data },
          { key: "@job_TermsCode", value: "Bill" },
          { key: "@job_CustRefNum", value: "" },
          { key: "@job_Status", value: $("#status").val() },
          { key: "@job_CustomerID", value: $("#selectEventCustomerId").val() },
          { key: "@job_name", value: $("#job_name").val() },
          { key: "@workflow_id", value: "" },
          { key: "@job_type", value: "" },
          { key: "@job_parent_id", value: jsonValues1["ParentJobId"] },
          { key: "@all_day", value: allDay },
		  { key: "@is_event_started", value: is_event_started },
        ],
        sp_name: [{ key: "BSIT_CalendarSaveEvent" }],
      };

      $.ajax({
        type: "POST",
        url: base_url + "core/Apilocal/Call_SP",
        data: requestData,
        dataType: "json",
        success: function(res) {
          $(".loader").hide();
          $("#eventContent").modal("hide");
          $("#calendar").fullCalendar("refetchEvents");
          $("#add_job_div").css("display", "none");

          $("#advancedSearch").hide();

          // FUNCTION FOR LOAD DEFAULT JOB-PANEL
          getDataForJobCardPanelDefault($("#jobOwnerSelectDropdown").val());
		  loadUserEvent();
        },
        error: function(err) {
          alert(err.responseText);
        },
      });
    });

  // -------------------------------------------
  // -- COMMIT EDIT OR ADD NEW JOB FUNCTIONALITY
  // -------------------------------------------
  $("#save_job_data")
    .button()
    .on("click", function() {
      let form = $("#jobForm");
      let collectionId = $(form).data("collid");
      let customerColor = $("#job_selectEventCustomerId :selected").attr(
        "iscolor",
      );
	 

      let customerId = $("#job_selectEventCustomerId").val();
      let customerName = $("#job_selectEventCustomerId :selected").text();
      let jobStatus = $("#job_status").val();
      let jobTitle = $("#job_name").val();
      let workTypeId = $("#job_selectWorkTypeId").val();

      if ($("#job_selectEventCustomerId").val() == "") {
        // ParentJobID FIELD IS REQUIRED
        $("#job_selectEventCustomerId").addClass("required_border_class");
        return false;
      } else {
        $("#job_selectEventCustomerId").removeClass("required_border_class");
      }

      if ($("#job_parent_job_title").val() == "") {
        // ParentJobID FIELD IS REQUIRED
        $("#job_parent_job_title").addClass("required_border_class");
        return false;
      } else {
        $("#job_parent_job_title").removeClass("required_border_class");
      }

      if ($("#job_name").val() == "") {
        // ParentJobID FIELD IS REQUIRED
        $("#job_name").addClass("required_border_class");
        return false;
      } else {
        $("#job_name").removeClass("required_border_class");
      }

      if ($("#job_selectWorkTypeId").val() == "") {
        // ParentJobID FIELD IS REQUIRED
        $("#job_selectWorkTypeId").addClass("required_border_class");
        return false;
      } else {
        $("#job_selectWorkTypeId").removeClass("required_border_class");
      }

      if ($("#job_status").val() == "") {
        // ParentJobID FIELD IS REQUIRED
        $("#job_status").addClass("required_border_class");
        return false;
      } else {
        $("#job_status").removeClass("required_border_class");
      }

      $("#job_selectWorkTypeId").prop("disabled", false);

      $("#job_status option").each(function() {
        $("#status").append(
          '<option value="' +
            $(this).attr("value") +
            '">' +
            $(this).attr("value") +
            "</option>",
        );
      });

      let jsonData = JSON.stringify({
        CustomerID: $("#job_selectEventCustomerId").val(),
        ParentJobID: $("#job_sel_parent_job_id").val(),
        CustRefNum: $("#job_CustRefNum").val(),
        title: $("#job_name").val(),
        desc: $("#job_desc").val(),
        EstDurationHrs: $("#job_EstDurationHrs").val(),
        work_type_id: $("#job_selectWorkTypeId").val(),
        Status: $("#job_status").val(),
        Priority: $("#job_Priority").val(),
        InternalNotes: $("#job_InternalNotes").val(),
        TermsCode: $("#job_TermsCode").val(),
        QuotedPrice: $("#job_QuotedPrice").val(),
      });

      let requestData = $.fn.bsitJsonObject(collectionId, jsonData);

      let lastJobId = 0;

      $.ajax({
        type: "POST",
        url: base_url + "core/Apilocal/create",
        data: { data: requestData },
        async: false,
        success: function(returnData) {
          returnData = jQuery.parseJSON(returnData);
          lastJobId = returnData[0]["last_insert_id"];
        },
      });

      let parentJobId = $("#job_sel_parent_job_id").val();
      let parentJobName = set_project_value_basedon_job(
        $("#job_sel_parent_job_id").val(),
        1,
      );

      let imgIcon = "";
      let filterList = '<div class="mui-row" style="padding-bottom:15px;">';

      filterList +=
        '<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
        lastJobId +
        '">';
      filterList += '<span class="cust_color active">';
      filterList +=
        '<span class="job"><label>Job:</label><span class="job_card_title">' +
        jobTitle +
        "</span></span>";

      let colorJson = $.parseJSON(customerColor);
	  let colorBg;
	  let colorFg;
      if (typeof colorJson != "undefined") {
         colorBg = colorJson[0];
         colorFg = colorJson[1];
      }

      filterList +=
        '<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
        parentJobId +
        '">' +
        parentJobName +
        "</span></span>";

      filterList +=
        '<span class="icon"><span class="job_card_customer" data-cid="' +
        customerId +
        '" style="float: left;background-color:' +
        colorBg +
        ";color:" +
        colorFg +
        ';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
        customerName +
        "</span>" +
        imgIcon +
        "</span>";

      filterList +=
        '<span class="job_load_status" style="display:none;">' +
        jobStatus +
        "</span>";

      filterList += "</span></div></div>";

      $("#job_list").html(filterList);

      $("#sel_job_id").val(lastJobId);
      $("#job_title").val(jobTitle);
      $("#parent_job_title").val(parentJobName);
      $("#sel_parent_job_id").val(parentJobId);
      $("#selectEventCustomerId").val(customerId);

      $("#status").val(jobStatus);
      $("#selectWorkTypeId").val(workTypeId);

      $("#internal_notes").val($("#job_InternalNotes").val());
      $("#description").val($("#job_desc").val());

      $("#jobContent").modal("hide");
      $("#eventContent").css("opacity", "1");
      $(form).trigger("reset");
    });

  // ------------------------
  // -- CHANGE DURATION BASE ON CHANGE START & END DATE TIME
  // ------------------------
  $("#fromTimeSelector").on("change", function() {
    let startTime = $("#fromTimeSelector").val();
    let endTime = $("#toTimeSelector").val();
    let startDate = $("#fromDateSelector").val();
    let endDate = $("#toDateSelector").val();

    if (
      Date.parse(startDate + " " + startTime) >=
        Date.parse(endDate + " " + endTime) ||
      (startDate == "" || startTime == "" || endDate == "" || endTime == "")
    ) {
      $("#fromDateSelector").css({ "border-color": "#f44336" });
      $("#fromTimeSelector").css({ "border-color": "#f44336" });
      $("#toDateSelector").css({ "border-color": "#f44336" });
      $("#toTimeSelector").css({ "border-color": "#f44336" });
      return false;
    } else {
      let eventDuration = getEventDuration(
        startDate + " " + startTime,
        endDate + " " + endTime,
      );
      $("#event_duration").html(eventDuration);
      $("#fromDateSelector").css({ "border-color": "seashell" });
      $("#fromTimeSelector").css({ "border-color": "seashell" });
      $("#toDateSelector").css({ "border-color": "seashell" });
      $("#toTimeSelector").css({ "border-color": "seashell" });
    }
  });

  $("#toTimeSelector").on("change", function() {
    let startTime = $("#fromTimeSelector").val();
    let endTime = $("#toTimeSelector").val();
    let startDate = $("#fromDateSelector").val();
    let endDate = $("#toDateSelector").val();

    let startDuration = `${startDate} ${startTime}`;
    let endDuration = `${endDate} ${endTime}`;
    if (
      Date.parse(startDuration) >= Date.parse(endDuration) ||
      (startDate == "" || startTime == "" || endDate == "" || endTime == "")
    ) {
      $("#fromDateSelector").css({ "border-color": "#f44336" });
      $("#fromTimeSelector").css({ "border-color": "#f44336" });
      $("#toDateSelector").css({ "border-color": "#f44336" });
      $("#toTimeSelector").css({ "border-color": "#f44336" });
      return false;
    } else {
      let eventDuration = getEventDuration(startDuration, endDuration);

      $("#event_duration").html(eventDuration);
      $("#fromDateSelector").css({ "border-color": "seashell" });
      $("#fromTimeSelector").css({ "border-color": "seashell" });
      $("#toDateSelector").css({ "border-color": "seashell" });
      $("#toTimeSelector").css({ "border-color": "seashell" });
    }
  });

  $("#toDateSelector").on("change", function() {
    let startTime = $("#fromTimeSelector").val();
    let endTime = $("#toTimeSelector").val();
    let startDate = $("#fromDateSelector").val();
    let endDate = $("#toDateSelector").val();
    let startDuration = `${startDate} ${startTime}`;
    let endDuration = `${endDate} ${endTime}`;

    if (
      Date.parse(startDuration) > Date.parse(endDuration) ||
      (startDate == "" || startTime == "" || endDate == "" || endTime == "")
    ) {
      $("#fromDateSelector").css({ "border-color": "#f44336" });
      $("#fromTimeSelector").css({ "border-color": "#f44336" });
      $("#toDateSelector").css({ "border-color": "#f44336" });
      $("#toTimeSelector").css({ "border-color": "#f44336" });
      return false;
    } else {
      let eventDuration = getEventDuration(startDuration, endDuration);

      $("#event_duration").html(eventDuration);
      $("#fromDateSelector").css({ "border-color": "seashell" });
      $("#fromTimeSelector").css({ "border-color": "seashell" });
      $("#toDateSelector").css({ "border-color": "seashell" });
      $("#toTimeSelector").css({ "border-color": "seashell" });
    }
  });

  function getEventDuration(startTime, endTime) {
    startTime = new Date(startTime);
    endTime = new Date(endTime);

    let diff = (endTime - startTime) / 1000;
    let HH = Math.floor(diff / 3600);
    let MM = Math.floor(diff % 3600) / 60;
    let formatted = (HH < 10 ? "0" + HH : HH) + ":" + (MM < 10 ? "0" + MM : MM);

    return formatted + " Mins";
  }

  // FUNCTION FOR GETTING TITLE OF PARENT_JOB_ID
  function set_project_value_basedon_job(parentId, loadFromEvent = 0) {
    let result = "";
    let requestData = {
      data: JSON.stringify([
        {
          collID: 1,
          filter: `jb._id = ${parentId}`,
        },
      ]),
    };

    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/loadfiltered",
      data: requestData,
      dataType: "json",
      success: function(res) {
        let updateData = "";
        let returnData = "";
        if (res != null) {
          returnData = res[0]["Results"];
          if (returnData != "") {
            if (loadFromEvent == 0) {
              updateData = `${returnData[0]["CustName"]} - ${jobName}`;
            } else {
              updateData = returnData[0]["title"];
            }
          }
        }
        result = updateData;
      },
      error: function(err) {
        alert(err.responseText);
      },
    });
    return result;
  }

  // ------------------------------------------
  // -- [start] LOAD STATUS BASED ON PARENT JOB
  // ------------------------------------------
  async function getJobStatus(parentJobId, selectedValue = "") {
    // Clear old list ready for new job
    $("#status").html("");

    let workTypeId = await getJobWorkflowId(parentJobId);
    let workflowSteps = await getJobWorkflowSteps(workTypeId);

    let output = [];
    if (selectedValue == "") {
      output.push('<option value=""></option>');
      $.each(workflowSteps, function(key, value) {
        output.push(
          '<option value="' +
            value["status_title"] +
            '">' +
            value["status_title"] +
            "</option>",
        );
      });

      $("#status").html(output.join(""));
    } else {
      let selected = "";
      output.push('<option value=""></option>');
      $.each(workflowSteps, function(key, value) {
        if (selectedValue == value["status_title"]) {
          selected = "selected";
        }

        output.push(
          '<option value="' +
            value["status_title"] +
            '" ' +
            selected +
            ">" +
            value["status_title"] +
            "</option>",
        );

        if (selected != "") {
          selected = "";
        }
      });

      $("#status").html(output.join(""));
    }
  }

  // -------------------------------------------
  // -- SELECT 'status' BASED ON 'parent_job_id'
  // -------------------------------------------
  function getJobWorkflowId(parentJobId) {
    let jsonStr = {
      data: JSON.stringify([
        {
          collID: 1,
          filter: `jb._id = ${parentJobId}`,
        },
      ]),
    };

    return new Promise((resolve, reject) => {
      $.ajax({
        type: "POST",
        url: base_url + "core/Apilocal/loadfiltered",
        data: jsonStr,
        dataType: "json",
        beforeSend: function() {
          $(".loader").hide();
        },
        complete: function(response, status, err) {
          if (status == "success") {
            let workTypeId = response.responseJSON[0].Results[0].workflow_id;
            if (workTypeId == null) {
              workTypeId = 1;
            }
			
            resolve(workTypeId);
          } else {
            reject(err);
          }
        },
        error: function(err) {
          reject(err);
        },
      });
    });
  }

  // -----------------------------------------
  // -- SELECT 'status' BASED ON 'workflow_id'
  // -----------------------------------------
  function getJobWorkflowSteps(workflowTypeId) {
    let requestData = {
      data: JSON.stringify([
        {
          collID: 45,
          filter: `[job].workflow_type_id = ${workflowTypeId}`,
        },
      ]),
    };

    return new Promise((resolve, reject) => {
      $.ajax({
        type: "POST",
        url: base_url + "core/Apilocal/loadfiltered",
        data: requestData,
        dataType: "json",
        beforeSend: function() {
          $(".loader").hide();
        },
        complete: function(res, stat, err) {
          if (stat == "success") {
            resolve(res.responseJSON[0].Results);
          } else {
            reject(err);
          }
        },
        error: function(err) {
          alert(err.responseText);
        },
      });
    });
  }

  // ------------------------------------------
  // -- [start] LOAD STATUS BASED ON PARENT JOB
  // ------------------------------------------
  function getJobWorkType(jobId) {
    // FIXME: No hardcoded collections
    let requestData = {
      data: JSON.stringify([
        {
          collID: 1,
          filter: `jb._id = ${jobId}`,
        },
      ]),
    };
    let result = "";

    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/loadfiltered",
      data: requestData,
      dataType: "json",
      beforeSend: function() {
        $(".loader").hide();
      },
      success: function(res) {
        let workTypeId = res[0]["Results"][0]["work_type_id"];
        if (workTypeId == null) {
          workTypeId = 1;
        }

        $("#selectWorkTypeId").val(workTypeId);
      },
      error: function(err) {
        alert(err.responseText);
      },
    });
    return result;
  }

  // ------------------------------------------------------------
  // -- [start] LOAD STATUS BASED ON PARENT JOB FOR ADD-JOB MODEL
  // ------------------------------------------------------------
  async function load_status_based_on_project_for_addJobModel(
    parentJobId,
    selectedValue = 0,
  ) {
    let workTypeId = await getJobWorkflowId(parentJobId);
	let workflowSteps = await getJobWorkflowSteps(workTypeId);
    let output = [];
	
    if (selectedValue == 0) {
      output.push('<option value=""></option>');
      $.each(workflowSteps, function(key, value) {
        output.push(
          '<option value="' +
            value["status_title"] +
            '">' +
            value["status_title"] +
            "</option>",
        );
      });
      $("#job_status").html(output.join(""));
    } else {
      let selected = "";
      output.push('<option value=""></option>');
      $.each(workflowSteps, function(key, value) {
        if (selectedValue === value["status_title"]) {
          selected = "selected";
        } else {
          selected = "";
        }

        output.push(
          '<option value="' +
            value["status_title"] +
            '" ' +
            selected +
            ">" +
            value["status_title"] +
            "</option>",
        );
      });

      $("#job_status").html(output.join(""));
    }
  }

  $(document).on("click", "#add_job_box", function(e) {
    $(".cust_back_color")
      .find(".cust_color")
      .removeClass("active");
    $("#job_name").val($("#job_title").val());
    $("#job_title").val("Add Job");
    $("#sel_job_id").val("[Add Job]");

    // OPEN ADD JOB POPUP
    $("#jobContent").modal();

    $("#eventContent").css("opacity", "0.3");

    if ($("#selectEventCustomerId").val() != "") {
      $("#job_selectEventCustomerId").val($("#selectEventCustomerId").val());
    } else {
      $("#job_selectEventCustomerId").val("");
    }

    let workTypeId = getUserWorkTypeId($("#jobOwnerSelectDropdown").val());
    if (workTypeId == "") {
      workTypeId = 1;
    }

    if ($("#sel_parent_job_id").val() != "" && $("#parent_job_title").val()) {
      $("#job_sel_parent_job_id").val($("#sel_parent_job_id").val());
      $("#job_parent_job_title").val($("#parent_job_title").val());

      // LOAD STATUS BASED ON PARENT JOB
      load_status_based_on_project_for_addJobModel(
        $("#job_sel_parent_job_id").val(),
        0,
      );

      $("#job_status").val($("#status").val());

      $("[name=job_workflow_id]").prop("disabled", false);
      $("#job_selectWorkTypeId").val(workTypeId);
      $("[name=job_workflow_id]").val(workTypeId);
    } else {
      $("#job_sel_parent_job_id").val("");
      $("#job_parent_job_title").val("");
      $("#job_status option").remove();
      $("[name=job_workflow_id]").prop("disabled", false);
      $("#job_selectWorkTypeId").val(workTypeId);
      $("[name=job_workflow_id]").val(workTypeId);
    }
  });

  // --------------------------------
  // show advanced search drop down
  // --------------------------------

  $(document).on("click", "#advancedSearchDropArrow", function(e) {
    showHideAdvancedSearch();
  });

  function showHideAdvancedSearch() {
    // show advanced search arrow
    $("#advancedSearch").toggle();

    // change the down/up image
    $("#advancedSearchDownImg").toggle();
    $("#advancedSearchUpImg").toggle();
  }

  // ----------------------------
  // -- MULTI-SELECT DROPDOWN
  // ----------------------------
  $(".js-example-basic-multiple").select2();

  function getUserWorkTypeId(userID) {
    let userWorkType = 0;
    let jsonStr = '[{"collID": 16, "filter": "usr._id = ' + userID + '"}]';
    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/loadfiltered",
      async: false,
      data: { data: jsonStr },
      dataType: "json",
      beforeSend: function() {
        $(".loader").hide();
      },
      success: function(data) {
        userWorkType = data[0]["Results"][0]["WorkTypeId"];
      },
      error: function(err) {
        alert(err.responseText);
      },
    });
    return userWorkType;
  }

  // SET DEFAULT JOB-CARD PANEL WHEN CLICK ON CLOSE ADD/EDIT POPUP
  $(document).on("click", "#close_button", function(e) {
    $("#advancedSearch").hide();
    $("#job_list").css("border", "none");

    $(".select2-selection__choice").remove();
  });

  $(document).on("click", "#job_close_button", function(e) {
    $("#eventContent").css("opacity", "1");
  });

  function getDataForNewJobCardPanel(
    searchString = "",
    parentJobId = null,
    customerId = null,
    completeStatus = 0,
	jobCheckboxStatus = 1,
	opportunityStatus = 0,
  ) {
	 
    let requestData = {
      parameters: [
        { key: "@jobName", value: searchString },
        { key: "@parentJobId", value: parentJobId || null },
        { key: "@customerId", value: customerId || null },
        { key: "@completeStatus", value: completeStatus },
		{ key: "@hideMspJobs", value: jobCheckboxStatus },
		{ key: "@opportunityStatus", value: opportunityStatus },
      ].filter((param) => param.value != null),
      sp_name: [{ key: "BSIT_CalendarNewJobLists" }],
    };

    let filterList = '<div class="mui-row" style="padding-bottom: 15px;">';

    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      success: function(res) {
        let currentDate = new Date();
        let colorFg = "#000000";
        let colorBg = "#FFFFFF";
        let cnt = 0;

        if (res != "") {
          $.each(res, function(index, value) {
            let imgIcon = "";
            let jobsInEvent = "";
            if (
              value["jobs_in_event"] != "" &&
              value["jobs_in_event"] != null
            ) {
              jobsInEvent = value["jobs_in_event"].split(",");
              for (let i = 0; i < jobsInEvent.length; i++) {
                jobsInEvent[i] = parseInt(jobsInEvent[i]);
              }
            }

            if (value["title"].length > 125) {
              value["title"] = value["title"].substring(0, 125) + "...";
            }

            if (value["parent_title"].length > 75) {
              value["parent_title"] =
                value["parent_title"].substring(0, 75) + "...";
            }

            filterList +=
              '<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
              value["_id"] +
              '">';
            filterList += '<span class="cust_color">';
            filterList +=
              '<span class="job"><label>Job:</label><span class="job_card_title">' +
              value["title"] +
              "</span></span>";

            if ($.inArray(value["_id"], jobsInEvent) < 0) {
              if (
                moment(currentDate).format("YYYY-MM-DD") ==
                moment(value["key_create_date"]).format("YYYY-MM-DD")
              ) {
                // RESENT JOB USE (+) ICON
                imgIcon =
                  "<img src='" +
                  base_url +
                  "assets/core/images/plus.png' style='opacity: 0.3;'>";
              }
            } else {
              // JOB USED BY OTHER USERS THEN USE (clock) ICON
              imgIcon =
                "<img src='" +
                base_url +
                "assets/core/images/history.png' style='opacity: 0.3;'>";
            }

            let colorJson = $.parseJSON(value["def_event_color"]);
            if (typeof colorJson != "undefined") {
              colorBg = colorJson[0];
              colorFg = colorJson[1];
            }

            filterList +=
              '<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
              value["parent_id"] +
              '">' +
              value["parent_title"] +
              "</span></span>";

            filterList +=
              '<span class="icon"><span class="job_card_customer" data-cid="' +
              value["customer_id"] +
              '" style="float: left;background-color:' +
              colorBg +
              ";color:" +
              colorFg +
              ';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
              value["customerName"] +
              "</span>" +
              imgIcon +
              "</span>";
            filterList +=
              '<span class="job_load_status" style="display:none;">' +
              value["status"] +
              "</span>";
            filterList += "</span>";
            filterList += "</div>";

            cnt++;
            if (cnt % 4 == 0) {
              filterList +=
                '</div><div class="mui-row" style="padding-bottom:15px;">';
            }
          });

          filterList += "</div>";

          $("#recent_jobs").html(filterList);
          $(".job_card_loader").hide();
          $(".job_card_loader1").hide();
        } else {
          $("#recent_jobs").html(filterList);
          $(".job_card_loader").hide();
          $(".job_card_loader1").hide();
        }
      },
      error: function(e) {
        alert("Error: " + e);
      },
    });
  }

  // --------------------------------------------------------------------
  // -- FUNCTION FOR LOAD JOB-PANEL BASED ON SELECT JOBS/CUSTOMER/PROJECT
  // --------------------------------------------------------------------
  function getDataForJobCardPanel(
    jobId = "",
    parentJobID = "",
    customerId = "",
    completeStatus = 2,
	jobCheckboxStatus = 1,
	opportunityStatus = 0,
  ) {
    let requestData = {
      parameters: [
        { key: "@job_name", value: jobId },
        { key: "@parent_id", value: parentJobID },
        { key: "@customer_id", value: customerId },
        { key: "@complete_status", value: completeStatus },
		{ key: "@hideMspJobs", value: jobCheckboxStatus },
		{ key: "@opportunity_status", value: opportunityStatus },
      ],
      sp_name: [{ key: "BSIT_CalendarJobDropdownList" }],
    };

    let filterList = '<div class="mui-row" style="padding-bottom:15px;">';

    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      success: function(res) {
        let currentDate = new Date();
        let colorFg = "#000000";
        let colorBg = "#FFFFFF";
        let cnt = 0;

        if (res != "") {
          $.each(res, function(index, value) {
            let imgIcon = "";
            let jobsInEvent = "";
            if (
              value["jobs_in_event"] != "" &&
              value["jobs_in_event"] != null
            ) {
              jobsInEvent = value["jobs_in_event"].split(",");
              for (let i = 0; i < jobsInEvent.length; i++) {
                jobsInEvent[i] = parseInt(jobsInEvent[i]);
              }
            }

            if (value["name"].length > 125) {
              value["name"] = value["name"].substring(0, 125) + "...";
            }

            if (value["parent_job_name"].length > 75) {
              value["parent_job_name"] =
                value["parent_job_name"].substring(0, 75) + "...";
            }

            filterList +=
              '<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
              value["jobId"] +
              '">';
            filterList += '<span class="cust_color">';
            filterList +=
              '<span class="job"><label>Job:</label><span class="job_card_title">' +
              value["name"] +
              "</span></span>";

            if ($.inArray(value["key_id"], jobsInEvent) < 0) {
              if (
                moment(currentDate).format("YYYY-MM-DD") ==
                moment(value["key_create_date"]).format("YYYY-MM-DD")
              ) {
                // RESENT JOB USE (+) ICON
                imgIcon =
                  "<img src='" +
                  base_url +
                  "assets/core/images/plus.png' style='opacity: 0.3;'>";
              }
            } else {
              // JOB USED BY OTHER USERS THEN USE (clock) ICON
              imgIcon =
                "<img src='" +
                base_url +
                "assets/core/images/history.png' style='opacity: 0.3;'>";
            }

            let colorJson = $.parseJSON(value["CustColor"]);
            if (typeof colorJson != "undefined") {
              colorBg = colorJson[0];
              colorFg = colorJson[1];
            }

            filterList +=
              '<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
              value["ParentJobID"] +
              '">' +
              value["parent_job_name"] +
              "</span></span>";

            filterList +=
              '<span class="icon"><span class="job_card_customer" data-cid="' +
              value["CustomerID"] +
              '" style="float: left;background-color:' +
              colorBg +
              ";color:" +
              colorFg +
              ';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
              value["CustName"] +
              "</span>" +
              imgIcon +
              "</span>";
            filterList +=
              '<span class="job_load_status" style="display:none;">' +
              value["Status"] +
              "</span>";
            filterList += "</span>";
            filterList += "</div>";

            cnt++;
            if (cnt % 4 == 0) {
              filterList +=
                '</div><div class="mui-row" style="padding-bottom:15px;">';
            }
          });

          filterList += "</div>";

          $("#job_list").html(filterList);
          $(".job_card_loader").hide();
          $(".job_card_loader1").hide();
        } else {
          $("#job_list").html(filterList);
          $(".job_card_loader").hide();
          $(".job_card_loader1").hide();
        }
      },
      error: function(e) {
        alert("Error: " + e);
      },
    });
  }

  // ---------------------------------------
  // -- FUNCTION FOR LOAD DEFAULT JOB-PANEL
  // ---------------------------------------
  function getDataForJobCardPanelDefault(userID) {
	  
	  let opportunityStatus = 0;
	  
	  if ($("#checkbox-opportunities").is(":checked")) {
        opportunityStatus = 1;
      }
	  
	  let jobCheckboxStatus = 2;
 if ($("#job-checkbox-checked").is(":checked")) {
        jobCheckboxStatus = 1;
      }
	  
	  
    let requestData = {
      parameters: [{ key: "@userID", value: userID },{ key: "@opportunityStatus", value: opportunityStatus },{ key: "@hideMspJobs", value: jobCheckboxStatus }],
      sp_name: [{ key: "BSIT_CalendarJobLists" }],
    };

    let filterList = '<div class="mui-row" style="padding-bottom:15px;">';

    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      success: function(res) {
        let colorFg = "#000000";
        let colorBg = "#FFFFFF";
        let cnt = 0;

        if (res != "") {
          $.each(res, function(index, value) {
            let imgIcon = "";
            if (value["title"].length > 125) {
              value["title"] = value["title"].substring(0, 125) + "...";
            }

            if (value["parent_title"].length > 75) {
              value["parent_title"] =
                value["parent_title"].substring(0, 75) + "...";
            }

            if (value["terms"] == "Bill") {
              imgIcon = `<img src="${BASEURL_IMAGE}/clock.png">`;
            } else if (value["terms"] == "QD") {
              imgIcon = `<img src="${BASEURL_IMAGE}/currency-usd.png">`;
            }

            filterList +=
              '<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
              value["_id"] +
              '">';
            filterList += '<span class="cust_color">';
            filterList +=
              '<span class="job"><label>Job:</label><span class="job_card_title">' +
              value["title"] +
              "</span></span>";

            let colorJson = $.parseJSON(value["def_event_color"]);
            if (typeof colorJson != "undefined") {
              colorBg = colorJson[0];
              colorFg = colorJson[1];
            }

            filterList +=
              '<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
              value["parent_id"] +
              '">' +
              value["parent_title"] +
              "</span></span>";

            filterList +=
              '<span class="icon"><span class="job_card_customer" data-cid="' +
              value["customer_id"] +
              '" style="float: left;background-color:' +
              colorBg +
              ";color:" +
              colorFg +
              ';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
              value["customerName"] +
              "</span>" +
              imgIcon +
              "</span>";
            filterList +=
              '<span class="job_load_status" style="display:none;">' +
              value["status"] +
              "</span>";
            filterList += "</span>";
            filterList += "</div>";

            cnt++;
            if (cnt % 4 == 0) {
              filterList +=
                '</div><div class="mui-row" style="padding-bottom:15px;">';
            }
          });
          filterList += "</div>";
          $("#job_list").html(filterList);
          $(".job_card_loader").hide();
          $(".job_card_loader1").hide();
        } else {
          $(".job_card_loader").hide();
          $(".job_card_loader1").hide();
        }
      },
    });
  }
});


// ------------------------------------------------------
// -- GETTING ALIAS FOR PARTICULAR COLLECTION OF DROPDOWN
// ------------------------------------------------------
function get_alias_for_filter_dropdown(coll_id)
{ 

  var result="";
  var json_str = '[{"collID": 6, "filter": "tbl.collection_id = '+coll_id+'"}]';  
  $.ajax({
    type: "POST",
    url: base_url+"core/Apilocal/loadfiltered",
    async: false,
    data:  {data: json_str} ,
    dataType: 'json',
    beforeSend: function(){
          $(".loader").hide();
        },
    success: function(returndata1){ //alert(JSON.stringify(returndata1['Data']));
      //returndata1 = returndata1['Data'];
      var udata_wt = returndata1[0]['Results'];        
      result = udata_wt[0]['Alias']; 
    },
    error: function(xx){
      alert(xx.responseText); 
    }
  }); 
  return result;    
}




jQuery(document).ready(function($) {
  
  $([document.documentElement, document.body]).animate({
        scrollTop: $("tr [data-time='05:00:00']").offset().top
    }, 1000);
});
function scrolldata(){
  var scroll = $(window).scrollTop();
     //>=, not <=
    if (scroll >= 65) {
    
      $(".fc-row.fc-widget-header table ").addClass("borderclass");
      $(".fc-row:first-child table").css("border-width","1px !important");
      $(".bsit_cal_day > .fc-widget-header > table.borderclass").css("border-width","1px !important");
     
      //$(".fc-row:first-child table").css('border-width', '1px');
     }
    else{
    
      $(".fc-row:first-child table").css("border-width","0");
      $(".bsit_cal_day > .fc-widget-header > table.borderclass").css("border-width","0");
      $(".fc-row.fc-widget-header table ").removeClass("borderclass");
    //  $(".fc-row.fc-widget-header table").css('border-width', '0');

    }
}

$(window).scroll(function() {    
    scrolldata();
});
/*
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 65) {
        //clearHeader, not clearheader - caps H
        $('#bsit_cal_week_day').html($(".fc-head").html());
        // $('#bsit_cal_schedules_event').html($(".fc-day-grid").html());
        $(".bsit_cal_day_sticky").addClass("sticky");
        $(".bsit_cal_fix").addClass("bsit_cal_sch_event_block");
    } else {       
        $(".bsit_cal_day_sticky").removeClass("sticky");
        $(".bsit_cal_fix").removeClass("bsit_cal_sch_event_block");
      }
});

// change column border when resize and load
$(window).resize(function() {
 setTimeout(function(){
  $("#control_title").trigger("click");
},300);
});
$(document).ready(function() { 
 setTimeout(function(){
  $("#control_title").trigger("click");
},300);
});
$("#control_title").on('click', function(){
  setTimeout(function(){
    var width = $("#body_widtht").css("width");    
    $(".bsit_cal_date_day").css("width", width);
    $(".bsit_cal_fix").css("width",width);
  },300);
});*/
/*
$(document).ready(function() { 
 setTimeout(function(){
  $("#control_title").trigger("click");
},300);
$("#control_title").on('click', function(){
    setTimeout(function(){
       var width = $("#body_widtht").css("width");    
  $(".bsit_cal_date_day").css("width", width);
  $(".bsit_cal_fix").css("width", width);

  },300);
   
    });

});*/


// $(document).ready(function(){
//   $("#body_widtht").find(function(){
//     // alert( $("#calendar tbody.fc-body").width());  
//     $('#body_widtht').val();  
//     alert ($('#body_widtht').width());
//   });
// });



// $(document).ready(function() {
//   var width = $("#body_widtht").css("width");
//   $("#body_widtht").css(width);   
//   $(".bsit_cal_date_day").css("width", width);   
//   // $("#body_widtht").css("width", width);   
//   alert(width);
// });



// $(window).resize(function() {
//   var width = $("#body_widtht").width();
//   $(".bsit_cal_date_day").html("width", width);   
//   // $("#body_widtht").css("width", width);   
//   // alert(width);
// });

  // $("#body_widtht").css(width);   


// $(window).resize(function() {
//         $("#body_widtht").html("<b>Width</b> : " +
//         $("#body_widtht").width() );
//         alert($(window).height());
// });


// $(document).ready(function(){
//   $(window).resize(function(){
//     $('#body_widtht').html($(window).width());
//   });
// });