function edit_menu() {
    $(".bsit_edite_menu").off('click');
    //$(".bsit_edite_menu").click(function(){
}

$(document).on("click", ".bsit_edite_menu", function () {
    // Hide other dropdown menus in case of multiple dropdowns
    $(".bsit_edite_sub_menu").not($(this).next()).slideUp("fast");
    // Toggle the current dropdown table listing
    $(this).next(".bsit_edite_sub_menu ").slideToggle("fast");
});

// Hide dropdown table listing on click outside
$(document).on("click", function (event) {
    if (!$(event.target).closest(".bsit_link").length) {
        $(".bsit_edite_sub_menu ").slideUp("fast");
    }
});

// collaps filter
$(document).ready(function () {

    // Toggle dropdown table listing on click of trigger element

    $(".collapsible").click(function () {
        var fram_id = $(this).attr('data');
        if ($('#bsit_content_' + fram_id + ':visible').length) {

            $('#bsit_content_' + fram_id).hide("slideDown");
        } else {

            $('#bsit_content_' + fram_id).show("slideDown");
        }
    });
});

function activate_content_filter() {

    $(".collapsible").each(function (i) {
        var fram_id = $(this).attr('data');
        if ($('#filter_list_div_' + fram_id).children().length) {
            $('#myelement_' + fram_id).addClass('bsit_active_filters');
        } else {
            $('#myelement_' + fram_id).removeClass('bsit_active_filters');
        }
    });
}


$(document).ready(function () {
    // Show hide popover
    $(".bsit_header_menu_li").click(function () {
        $(this).find(".bsit_header_sub_menu").slideToggle("fast");
    });
});
$(document).on("click", function (event) {
    var $trigger = $(".bsit_header_menu_li");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".bsit_header_sub_menu").slideUp("fast");
    }
});


(function ($) {
    $(window).on("load", function () {

        $("a[rel='load-content']").click(function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            $.get(url, function (data) {
                $(".content .mCSB_container").append(data); //load new content inside .mCSB_container
                //scroll-to appended content 
                $(".content").mCustomScrollbar("scrollTo", "h2:last");
            });
        });

        $(".content").delegate("a[href='top']", "click", function (e) {
            e.preventDefault();
            $(".content").mCustomScrollbar("scrollTo", $(this).attr("href"));
        });

    });
})(jQuery);


$(document).ready(function () {
    edit_menu();
    activate_content_filter();
});




// right side bar for edite
$(document).ready(function () {
    $(".openNav").click(function () {
        $('#mySidenav').addClass('open');
        // alert('test');
        $('body').addClass('open-bg');
        // alert('test1');
    });
    $("#closeNav").click(function () {
        $('#mySidenav').removeClass('open');
        $('body').removeClass('open-bg');
    });
});


$(document).ready(function () {

    $("#btn_cls").click(function () {

        $("#class_demo").addClass("addcls");

    });

});

$(document).ready(function () {
    /*
Reference: http://jsfiddle.net/BB3JK/47/
*/

    $('.bsit_custom_dropdown').each(function () {
        var $this = $(this), numberOfOptions = $(this).children('option').length;

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'select-options content mCustomScrollbar'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function (e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function () {
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });

        $listItems.click(function (e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            //console.log($this.val());
        });

        $(document).click(function () {
            $styledSelect.removeClass('active');
            // $list.hide();
        });

    });
});


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})



$(document)
    .button()
    .on('click', "#editBtn", function (event) {
        $("body").addClass("hide-sidedrawer");
    })

$(document).ready(function ($) {
    $('.ui-autocomplete-input').autocomplete(
        {
            position: { my: "left0 bottom", at: "left0 top", collision: "flip", within: ".bsit_popup_scroll" },

        });
});
// mui-select__menu
// bsit_popup_scroll
// bsit_statusdiv
$(document).on("click", function () {
    setTimeout(function () {
        // debugger;
        if ($(document).find(".mui-select__menu").length > 0) {
            var height = $(document).find(".mui-select__menu").height() - 32;
            // alert("ji");
            $(".mui-select__menu")
                .position({
                    my: "left top",
                    at: "left bottom-" + height + "",
                    of: $(document).find(".mui-select__menu"),
                    collision: "flip",
                    within: ".bsit_popup_scroll"
                })
        }
        // $(document).find(".mui-select__menu").length;
    }, 10);
});
//$(document).ready (function($){
//  $(window).on("load",function(){

//        $(".bsit_popup_scroll").mCustomScrollbar({
//        axis:"y",
//      scrollButtons:{enable:true},
//    theme:"light-thick",
//  scrollbarPosition:"outside"
//});
//});
//});


$(document).ready(function () {


    $('.nav-tabs-dropdown')
        .on("click", "li.bsit-page-frame:not('.active') a", function (event) {
            $(this).closest('ul').removeClass("open");
			$(this).closest('.bsit_cola_tab').removeClass("bsit_open");
        })

        .on("click", "li.bsit-page-frame.active a", function (event) {
            $(this).closest('ul').toggleClass("open");
			$(this).closest('.bsit_cola_tab').toggleClass("bsit_open");
        });

});