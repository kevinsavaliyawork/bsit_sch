var latitude = '';
var longitude = '';
$(document).ready(() => {
  let bodyEl = $('body');
  let sidedrawerEl = $('#sidedrawer');

  // TODO: Export hide/show sidedrawer funcs
  function showSidedrawer() {
    // show overlay
    let options = {
      onclose: () => {
        sidedrawerEl.removeClass('active').appendTo(document.body);
      },
    };

    let overlayEl = $(mui.overlay('on', options));

    // show element
    sidedrawerEl.appendTo(overlayEl);

    setTimeout(() => {
      sidedrawerEl.addClass('active');
    }, 20);
  }

  function hideSidedrawer() {
    bodyEl.toggleClass('hide-sidedrawer');
  }

  $('.js-show-sidedrawer').on('click', showSidedrawer);
  $('.js-hide-sidedrawer').on('click', hideSidedrawer);

  let menuHeaders = $('.bsit-menu-header', sidedrawerEl);

  if (!menuHeaders.hasClass('header-data')) {
    menuHeaders.next().hide();
  }

  // Show submenu items when clicking parent menu item
  menuHeaders.on('click', function () {
    $(this)
      .next()
      .slideToggle(275);
  });

  // format into a BSIT data object
  $.fn.bsitJsonObject = function (coll_id, values) {
    return `{"colls": [{"coll_id": "${coll_id}", "values": ${values}}]}`;
  };

  // convert a form to an array ready to be converted into json values
  $.fn.serializeObject = function () {
    let o = {};
    let a = this.serializeArray();

    $.each(a, function () {

      // debugger;
      if (o[this.name] !== undefined) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }

        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });

    return o;
  };

  $('table tr th.Clswdt:eq(0)').addClass('pl-125');

  // Padding left to each table tr
  $('table tr').each(() => {
    $(this)
      .find('td.Clswdt:eq(0)')
      .addClass('pl-125');
  });
});

// dropdown with ul and li
$(document).on('click', '.drop-down .selected a', (e) => {
  // stops link from making page jump to the top
  e.preventDefault();
  e.stopPropagation();

  $('.drop-down .options li.submenu-ul i.minus')
    .toggleClass('minus')
    .toggleClass('plus')
    .siblings('.sub-menu')
    .toggleClass('open');

  $('.drop-down .options li i').removeClass('plus');
  $('.drop-down .options li i').removeClass('minus');
  $('.drop-down .options > ul').toggleClass('d-show');

  $('.drop-down .options li .sub-menu')
    .parent('li')
    .addClass('submenu-ul')
    .prepend("<i class='plus'></i>");
});

$(document).on('click', '.drop-down .options ul li i', (e) => {
  // stops link from making page jump to the top
  e.preventDefault();
  e.stopPropagation();
});

$(document).on('click', '.drop-down .options li.submenu-ul i.plus', (e) => {
  // stops link from making page jump to the top
  e.preventDefault();
  e.stopPropagation();

  $(this)
    .toggleClass('minus')
    .toggleClass('plus')
    .siblings('.sub-menu')
    .toggleClass('open');
});

$(document).on('click', '.drop-down .options li.submenu-ul i.minus', (e) => {
  // stops link from making page jump to the top
  e.preventDefault();
  e.stopPropagation();

  $(this)
    .toggleClass('plus')
    .toggleClass('minus')
    .siblings('.sub-menu')
    .toggleClass('open');
});

$(document).on('click', '.drop-down .options ul li a', (e) => {
  // stops link from making page jump to the top
  e.preventDefault();
  e.stopPropagation();

  let text = $(this).html();

  $('.drop-down .options li.submenu-ul i.minus')
    .toggleClass('minus')
    .toggleClass('plus')
    .siblings('.sub-menu')
    .toggleClass('open');

  $('.drop-down .selected a span').html(text);
  $('.drop-down .options ul').removeClass('d-show');
});

$('body').click(() => {
  $('.drop-down .options ul').removeClass('d-show');
});

$(document).on('click', '.drop-down .reset-val', (e) => {
  // stops link from making page jump to the top
  e.preventDefault();
  e.stopPropagation();
  $('#copyLocationid').val('');
  $('.drop-down .selected a span').html('Please select');
});

setTimeout(() => {
  $('.ui-tabs-anchor').keydown((e) => {
    e.preventDefault();
    e.stopPropagation();
    return false;
  });
}, 2000);

//Change password modal.
$(document).on('click', '#change-password', function () {
  $('#form-change-password').find('input').val('').removeAttr('readonly');
  $('#form-change-password').find('input').removeClass('mui--is-dirty').removeClass('mui--is-not-empty');
  $('#change-password-error-report').html('');
  $('#change-password-modal').modal();
  $('#form-change-password input:first-child').trigger('focus');
});
/**
 * Check if the user session is active.
 */
var checkSession = function () {
  $.post(base_url + 'core/login/isSessionExpired', 'json', function (data) {
    if (data.expired) {
      //Stop verifying the session.
      clearTimeout(verifySession);
      //Show block screen
      $.blockUI({ message: $('#block-user-session') });
    }
  });
}
var sessionInterval = 600000;
var verifySession = setInterval(checkSession, sessionInterval);

//When the user click to unlock the screen
$(document).on('click', '#unlock-user-screen', function () {
  $('.block-user-message').hide();
  $('#loginFormAjax').show();
});

//CKEDITOR - defined mulitple ckeditors based on class
$(document).ready(function () {
  // Initialize All CKEditors
  allCkEditors = [];;

  let editorOPtions = {
    toolbar: {
      items: [
        'heading',
        '|',
        'bold',
        'italic',
        // 'link',
        'bulletedList',
        'numberedList',
        '|',
        // 'indent',
        // 'outdent',
        // '|',
        'blockQuote',
        'insertTable',
        'alignment',
        'fontBackgroundColor',
        'fontColor',
        'fontSize',
        'fontFamily',
        'undo',
        'redo'
      ]
    },
    // language: 'en',
    table: {
      contentToolbar: [
        'tableColumn',
        'tableRow',
        'mergeTableCells'
      ]
    },
    licenseKey: '',
  };

  document.querySelectorAll('.ckeditor').forEach((node, index) => {
    ClassicEditor
      .create(node, editorOPtions)
      .then(editor => {
        allCkEditors.push(editor);
      })
      .catch(error => {
        console.error(error);
      });
  });
});

//get ckeditor
function getCkEditor(name, formId) {
  for (var i = 0; i < allCkEditors.length; i++) {
    let srcEl = allCkEditors[i].sourceElement;
    let sourceFormId = $(srcEl.closest("form")).attr("id");

    if (srcEl.name === name && sourceFormId === formId) {
      return allCkEditors[i];
    }
  }

  return null;
}

//get Geo locatin when logout
$(document).on("click", "#logoutBtn", function () {

  let url = base_url + 'core/Login/logout';

  if (navigator.geolocation) {

    var startPos;
    var geoOptions = {
      enableHighAccuracy: true,
      timeout: 10 * 1000,
      maximumAge: 5 * 60 * 1000,
    }

    var geoSuccess = function (position) {
      startPos = position;
      latitude = startPos.coords.latitude;
      longitude = startPos.coords.longitude;

      window.location.href = url + '?latitude=' + latitude + '&longitude=' + longitude;
    };
    var geoError = function (error) {
      console.log('Error occurred. Error code: ' + error.code);
      window.location.href = url + '?latitude=' + latitude + '&longitude=' + longitude;
      // error.code can be:
      //   0: unknown error
      //   1: permission denied
      //   2: position unavailable (error response from location provider)
      //   3: timed out
    };

    navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
  } else {
    console.log("Geolocation is not supported by this browser!");
    window.location.href = url + '?latitude=' + latitude + '&longitude=' + longitude;
  }

});