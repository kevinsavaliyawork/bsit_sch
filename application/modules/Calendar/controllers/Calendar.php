<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends brain_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('module');
		$this->load->model('Bsit_io','API');

		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		// check if the user is 'logged in', if not, redirect to login page.
		$user_id = $this->session->userdata('UserID');
		$logged_in = $this->session->userdata('logged_in');
		if($logged_in == FALSE OR $user_id == "" ){
			// redirect to the login page
			redirect('/login');
		};
	}

	function index(){

		// load the infomation
		$userID = $this->session->userdata('UserID');
		$name_first = $this->session->userdata('FirstName');
		$tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules($userID);

		$data['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);

		// get the users ID so its available in the page
		$data['userid'] = $this->session->userdata('UserID');

		// get the list of users in the system
		$USRCURLDATA = Array('collections' =>
								json_encode(
									Array(
										'colls'=> Array(
										    // user list
											Array(
												'coll_id'	=> '16',
												//'filter'	=> ""
												'filter'	=> "([usr].group_id <> 8 AND [sch]._id <> 3 AND [usr].is_active = 1)" // Ignore inactive users
											),
											// Customer list
											Array(
												'coll_id'	=> '12',
												'order'		=> 'o.name', // TaskId-38[Sort customer drop down]
												'filter'	=> ""
											),
											// job list
											Array(
												'coll_id'	=> '1',
												'cols'		=> Array('jobid', 'ParentJobID', 'name', 'desc', 'CustomerID'),
												'filter'	=> "jb.status <> 'Complete'"
											),
											// Work Type list
											Array(
												'coll_id'	=> '21',
												'cols'		=> Array('WorkTypeId', 'Name'),
												'filter'	=> ""
											),
											// SharedEvents list
											Array(
												'coll_id'	=> '31',
												'cols'		=> Array('SharedEventId', 'EventId', 'UserId'),
												'filter'	=> ""
											),
										)
									)
								)
							);
		// call the API
		$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
		$users_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $USRCURLDATA);
		// convert JSON to array/object

		$users_result = json_decode($users_result);
		if(isset($users_result->Data)) {
			$users_result = $users_result->Data;
		}

		// Users
		$data['curlDataUsr'] = $users_result[0]->Results;
		// Customers
		$data['curlDataCust'] = $users_result[1]->Results;

		// Jobs
		$curlDataJobs = $users_result[2]->Results;

		// build the array of jobs
		$jobList = $this->buildJobTree($curlDataJobs);
		$data['curlDataJobs1'] = $jobList; // Currently not use this Function. [Task_id - 19]
		$jobList = $this->buildJobOptionList($jobList); // Currently not use this Function. [Task_id - 19]
		// Jobs
		$data['curlDataJobs'] = $jobList;// Currently not use this Function. [Task_id - 19]
		// WorkTypes
		$data['curlDataWorkType'] = $users_result[3]->Results;
		// SharedEvents
		$data['curlDataSharedEvent'] = $users_result[4]->Results;

		$recentJobsURL = $BASEURLMETHOD.'?key=BSIT_CalendarNewJobLists';
		$userJobsURL = $BASEURLMETHOD.'?key=BSIT_CalendarJobLists&filters={"parameters":[{"key":"@userID","value":"'.$userID.'"}]}';

		$userJobsResult = json_decode($this->API->CallAPI("GET", $userJobsURL));
		$recentJobsResult = json_decode($this->API->CallAPI("GET", $recentJobsURL));

		if(isset($userJobsResult->Data)) {
			$userJobsResult = $userJobsResult->Data;
		} else {
			$userJobsResult = '';
		}

		if(isset($recentJobsResult->Data)) {
			$recentJobsResult = $recentJobsResult->Data;
		} else {
			$recentJobsResult = '';
		}

		$data['JobsLists'] = $userJobsResult;
		$data['RecentJobsLists'] = $recentJobsResult;

		// load the page
		return $this->load->view('Calendar/calendar.html', $data, true);
	}

	// recursive function to build array of jobs
	private function buildJobTree(array $elements, $parentId = 0) {
		// create a new array to hold the branch
		$branch = array();

		// loop each item and, if needed recursively
		foreach ($elements as $element) {
			if ($element->ParentJobID == $parentId) {
				$children = $this->buildJobTree($elements, $element->key_id);

				if ($children) {
					$element->children = $children;
				}

				$branch[] = $element;
			}
		}
		return $branch;
	}

	// recursive function to build html list of jobs
	private function buildJobOptionList(array $elements, $level=0) {
		$html = "";
		// loop each item add to string
		foreach ($elements as $element)
		{
			$html = $html . '<option value="'.$element->key_id.'" data-cust="'.$element->CustomerID.'">';
			$html = $html . str_repeat('-',$level).$element->name.'</option>';
			// does this element have children
			if (isset($element->children)) {
				$html = $html . $this->buildJobOptionList($element->children, $level+1);
			}
		}
		return $html;
	}
	function header($data_pass){ 
			
					// load the infomation
					$userID = $this->session->userdata('UserID');
					$name_first = $this->session->userdata('FirstName');
					$tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules($userID);

					$data_pass['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);

					// get the users ID so its available in the page
					$data_pass['userid'] = $this->session->userdata('UserID');

					// get the list of users in the system
					$USRCURLDATA = Array('collections' =>
											json_encode(
												Array(
													'colls'=> Array(
													    // user list
														Array(
															'coll_id'	=> '16',
															//'filter'	=> ""
															'filter'	=> "([usr].group_id <> 8 AND [sch]._id <> 3 AND [usr].is_active = 1)" // Ignore inactive users
														),
														// Customer list
														Array(
															'coll_id'	=> '12',
															'order'		=> 'o.name', // TaskId-38[Sort customer drop down]
															'filter'	=> ""
														),
														// job list
														Array(
															'coll_id'	=> '1',
															'cols'		=> Array('jobid', 'ParentJobID', 'name', 'desc', 'CustomerID'),
															'filter'	=> "jb.status <> 'Complete'"
														),
														// Work Type list
														Array(
															'coll_id'	=> '21',
															'cols'		=> Array('WorkTypeId', 'Name'),
															'filter'	=> ""
														),
														// SharedEvents list
														Array(
															'coll_id'	=> '31',
															'cols'		=> Array('SharedEventId', 'EventId', 'UserId'),
															'filter'	=> ""
														),
													)
												)
											)
										);
					// call the API
					$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
					$users_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $USRCURLDATA);
					// convert JSON to array/object

					$users_result = json_decode($users_result);
					if(isset($users_result->Data)) {
						$users_result = $users_result->Data;
					}

					// Users
					$data_pass['curlDataUsr'] = $users_result[0]->Results;
			$this->load->view('Calendar/header', $data_pass);

		}

}