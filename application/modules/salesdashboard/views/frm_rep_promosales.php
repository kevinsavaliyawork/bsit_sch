<!--
.io_numReps
#id_PS_img
#id_PS_Rank
#id_PS_Value
-->

<div class="chart_main" id="repPromoSalesMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Promotional Sales</div>
  </div>

  <!-- body -->
  <div class="chart_body" id="repPromoSales_container">
      <!-- LEFT -->
      <div class="chart_metric">
        <div class="chart_primary">
          <span class="chart_primary_title">Rank:</span>
          <span class="chart_primary_value" id="id_PS_Rank">#</span>/<span class="io_numReps">#</span>
        </div>
        <div class="chart_scnd">
          <span class="chart_scnd_title"></span>
          <span class="chart_scnd_value"><strong id="id_PS_Value">#</strong></span>
        </div>
      </div>

      <!-- RIGHT -->
      <div class="chart_hero">
        <img id="id_PS_Img" src="<?php echo base_url();?>assets/images/dashboard_gifs/AAD_thumbs_up.gif" />
      </div>
  </div>
</div>
