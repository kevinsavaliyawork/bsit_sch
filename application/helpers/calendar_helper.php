<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('calendar')) {
    function calendar()
    {
        $ci = & get_instance();

        $html = '<div id="'.$id.'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        $html .= '<div class="modal-header">';
        $html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        $html .= '<h3 id="myModalLabel">'.$header.'</h3>';
        $html .= '</div>';

        $html .= '<div class="modal-body">';
        $html .= $body;
        $html .= '</div>';

        $html .= '<div class="modal-footer">';

        if (!$footer) {
            $html .= '<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>';
            $html .= '<button class="btn btn-primary" id="submit_plans">Save changes</button>';
        } else {
            $html .= $footer;
        }

        $html .= '</div></div>';

        return $html;
    }
}

if (!function_exists('monthlist')) {
    function monthlist($id, $sel='')
    {
        $ci = & get_instance();

        $html = '<select id="'.$id.'" name="'.$id.'">';

        $html .= $sel == '01' ? '<option value="01" selected="selected">January</option>' : '<option value="01">January</option>';
        $html .= $sel == '02' ? '<option value="02" selected="selected">February</option>' : '<option value="02">February</option>';
        $html .= $sel == '03' ? '<option value="03" selected="selected">March</option>' : '<option value="03">March</option>';
        $html .= $sel == '04' ? '<option value="04" selected="selected">April</option>' : '<option value="04">April</option>';
        $html .= $sel == '05' ? '<option value="05" selected="selected">May</option>' : '<option value="05">May</option>';
        $html .= $sel == '06' ? '<option value="06" selected="selected">June</option>' : '<option value="06">June</option>';
        $html .= $sel == '07' ? '<option value="07" selected="selected">July</option>' : '<option value="07">July</option>';
        $html .= $sel == '08' ? '<option value="08" selected="selected">August</option>' : '<option value="08">August</option>';
        $html .= $sel == '09' ? '<option value="09" selected="selected">September</option>' : '<option value="09">September</option>';
        $html .= $sel == '10' ? '<option value="10" selected="selected">October</option>' : '<option value="10">October</option>';
        $html .= $sel == '11' ? '<option value="11" selected="selected">November</option>' : '<option value="11">November</option>';
        $html .= $sel == '12' ? '<option value="12" selected="selected">December</option>' : '<option value="12">December</option>';
        $html .= '</select>';

        return $html;
    }
}


if (!function_exists('yearlist')) {
    function yearlist($id, $years = 1, $sel='')
    {
        $ci = & get_instance();

        $html = '<select id="'.$id.'" name="'.$id.'">';

        $i = date('Y') - 1;
        $k = $i + $years;

        for ($j = $i; $j <= $k; $j++) {
            if ($j == $sel) {
                $html .= '<option value="'.$j.'" selected="selected">'.$j.'</option>';
            } else {
                $html .= '<option value="'.$j.'">'.$j.'</option>';
            }
        }

        $html .= '</select>';

        return $html;
    }
}
