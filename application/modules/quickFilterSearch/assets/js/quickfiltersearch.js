$(document).ready(function() {

$(".searchData").keyup(function(event) {
    if (event.which === 13) {
      $(".bsit_common_search").click();
    }
  });

  $(document)
  .button()
  .on('click', '.bsit_common_search', function(e)
  {
    var searchData = $('#searchData').val();
     var numofrows = 500;
    // debugger  jb._id = V(job_id)
    $.ajax({
      type: 'POST',
      url: base_url + "QuickFilterSearch/quickfilterallsearch",
      data: { searchData:searchData,numofrows:numofrows},
      dataType: 'json',
      success: function(res)
        {
          
          var allSearchData = res.generic_all_search;
          var currentUrl = window.location.href;
          if(base_url+'page/search_all' != currentUrl)
          {
            var moduleUrl = base_url+'page/search_all';  
            window.open( moduleUrl, '_blank');
          }else{
            location.reload();
          }
        },
        error: function(err)
        {
            if(err.status == 404){
              err.responseText = 'The requested url was not found on this server.';
            } else if(err.status == 408) {
              err.responseText = 'API request timeout';
            } else if(err.status == 500 || err.status == 503) {
              err.responseText = 'Service Unavailable';
            } 
        },
    });

  });

});
var currentReq = null;
async function getSearchOptions(request, response, event) {
  var searchData = $('#searchData').val();
  var numofrows = 5;
    // debugger  jb._id = V(job_id)
    currentReq = $.ajax({
      type: 'GET',
      url: base_url+"QuickFilterSearch/quickfiltersearch_json",
      data: { search:searchData},
      dataType: 'json',
      beforeSend: function() {
        if (currentReq != null) {
            currentReq.abort();
        }
      },
      success: function(res)
        {
          let returnArray = [];

          if(!res.length){
      
              let result = {
                  label: "No record found",
                  search_value: "no-data",
                  url: "#"
                }

                returnArray.push(result);


          } else {
             let rtype='';
           

             res.forEach((result) => {
              
           // rtype = (result.additional_type) ? result.additional_type  : '' +' : ' + (result.additional_value) ? result.additional_value : '';
           
                let display = {
                  label: result.search_term,
                  search_term: result.search_term,
                  search_value: result.search_value,
                  type: result.type,
                  dim_id: result.dim_id,
                  url: base_url + result.url,
                  additional_type: result.additional_type,          
                  additional_value:result.additional_value
                 
                };
                returnArray.push(display);
              });

             let viewAllBtn = {
                  label: "View All",
                  search_term: "View All",
                  search_value: "view-all",
                  type: '',
                  dim_id: '',
                  url: "#",
                  additional_type: '',          
                  additional_value:''
                  }

                returnArray.push(viewAllBtn);
          }

          
          
          response(returnArray);
        },
        error: function(err)
        {
            if(err.status == 404){
              err.responseText = 'The requested url was not found on this server.';
            } else if(err.status == 408) {
              err.responseText = 'API request timeout';
            } else if(err.status == 500 || err.status == 503) {
              err.responseText = 'Service Unavailable';
            } 
        },
    });
}
$( function() {
    $( "#searchData" ).autocomplete({
      //source: availableTags,
      source: function(req, res) {
        getSearchOptions(req, res, $(this));
      },
      create: function() {
       
        $(this).data("ui-autocomplete")._renderItem = function (ul, item) {
          let atag = "<a href='"+ item.url +"' target='_blank'><span class='autotitle'>" + item.label+"</span>";
          let rtype = (item.additional_type) ? item.additional_type+' : ' : '';
          rtype += (item.additional_value) ? item.additional_value : '';
          let type = (item.type) ? item.type : '';
          if (item.search_value != "view-all" && item.search_value != "no-data") {
            atag += "<div class='autocontent'><span class='autoleft'>"+type+ "</span><span class='autoright'>"+rtype+"</span></div></a>";
            //atag += "<span>"+"Search Name:" +item.search_value + "<br/>dim_id:"+item.dim_id+"<br/>Type:"+item.dim_name+ "</span></a>";
          }
          
       
          return $("<li></li>")
           .data("item.autocomplete", item)
           .append(atag)
           .appendTo(ul);
        };

        $(this).data("ui-autocomplete")._renderMenu = function (ul, items) {
          var that = this;
          $.each( items, function( index, item ) {
            that._renderItemData( ul, item );
          });

          $(ul).find("li:last-child").addClass("viewBtn");
        };

        
      },
      select: function(event, ui) {
        event.preventDefault();
        //$(event.target).val(ui.item.label);
        
        if (ui.item.search_value == "view-all") {
          $(".bsit_common_search").trigger("click");
        } else if (ui.item.search_value == "no-data") {
          return false;
        } else {          
          window.open( ui.item.url, '_blank');
        }
        
      },

      focus: function(event, ui) {
        event.preventDefault();
        if (!ui.item.search_value) {
          return false;
        }
        
      },


    });
});