<?php
class Opportunitymodel extends brain_model {
  function __construct(){
    parent::__construct();
    $this->load->model('core/Bsit_io','API');
    $this->load->library('session');
  }

  /**
   * Gets a list of opportunities based on user search.
   * @param null $term
   * @return mixed
   */
  public function getOpportunities($term = null)
  {
    $parameters = array(
      array('key'=> '@term',	  'value' => $term),
    );

    $data_parameters = urlencode('{"parameters":'.json_encode($parameters).'}');
    $sp_name = 'Bsit_getOpportunities';
//    $data_parameters = json_encode($data_parameters);

    $BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
    $URL = $BASEURLMETHOD.'?key='.$sp_name.'&filters='.$data_parameters;
    $response = $this->API->CallAPI("GET",$URL);
    return $response;
  }

  /**
   * Saves emails from Gmail to opportunity.
   * @param $id
   * @param $data
   * @return mixed
   */
  public function saveGmailInfo($id,$data,$followUpDate,$price,$percent,$stage,$email) {
	  
	   $parameters = array(
      array('key'=> '@id', 'value' => $id),
	  array('key'=> '@data', 'value' => $data),
	  array('key'=> '@followUpDate', 'value' => $followUpDate),
	  array('key'=> '@price', 'value' => $price),
	  array('key'=> '@percent', 'value' => $percent),
	  array('key'=> '@stage', 'value' => $stage),
	  array('key'=> '@email', 'value' => $email)
    );
    $data_parameters = urlencode('{"parameters":'.json_encode($parameters).'}');
    $sp_name = 'Bsit_saveGmailInfo';
//  $data_parameters = json_encode($data_parameters);

    $BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
    $URL = $BASEURLMETHOD.'?key='.$sp_name.'&filters='.$data_parameters;
    $response = $this->API->CallAPI("GET",$URL);
	echo($followUpDate);
    return $response;

  }

  public function getActivityHistoryDetail($opportunity_id){

          $_DATAPOST['parameters'] = array (
                'key' => 'get_activity_history',
                'filters' => 
                  array(
                      array (
                        'key' => 'opportunity_id',
                        'value' => $opportunity_id,//'4079',
                      ),

                      
                      ),
                  );

            $data_parameters = json_encode($_DATAPOST['parameters']);

            $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
            $get_activity_history_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
         // echo "<pre/>";  print_r($get_activity_history_detail); exit;
            return $get_activity_history_detail;
    } 

 public function getCustomerDetail($Opportunity_id){
        
      $_DATAPOST['parameters'] = array (
          'key' => 'get_customer_by_opportunity_id',
          'filters' => 
            array(
                array (
                  'key' => 'Opportunity_id',
                  'value' => $Opportunity_id,
                ),
                ),
            );

      $data_parameters = json_encode($_DATAPOST['parameters']);
      $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
      $get_customer_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
      // echo "<pre/>";print_r($get_customer_detail);     exit;
      return $get_customer_detail;
    }

}
