<?php
	$user = $_SESSION['username'];
	$show_params = "true";

	$html = '<div id="page_frame_'.$frame['PFID'].'_content" style="height:100%;" >';
	$report = trim(str_replace('\\/', '/', $load_data[0]->report_url), " ");
	//$report = 'http://13.75.195.153/bsit_api/ViewReport.aspx?ReportFolder=Perigon%20Reports&ReportName=WeeklyTimesheet';
	
	$rpt_url = 	$report;
	$html .= '<iframe class="reportElement" id="iframe-id-'.$frame['PFID'].'" scrolling="auto" style="border:none; overflow:none;width:100%;" src="'.$rpt_url.'"></iframe>';

	$html .= '</div>';
	echo $html;
?>

<script type="text/javascript">
$(function(){

	function getDocHeight(doc) {
		//alert("Test");
		doc = doc || document;
		var body = doc.body, html = doc.documentElement;
		var height = Math.max( body.scrollHeight, body.offsetHeight,
			html.clientHeight, html.scrollHeight, html.offsetHeight );
		return height;
	}
	function getDocWidth(doc) {
		return $(doc).parent().width();
	}

	$(".reportElement").on('load', function() {
		//alert("Test");
		var ifrm = this;
		ifrm.style.height = "12px";
		checkIframeLoaded(ifrm,0);
	});

	function checkIframeLoaded(ifrm,runTimes) {
		runTimes++;
		// Get a handle to the iframe element
		var iframeDoc = ifrm.contentDocument ? ifrm.contentDocument : ifrm.contentWindow.document;
		var height = getDocHeight(iframeDoc);
		// Check if loading is complete
		if (iframeDoc.readyState == "complete" && height > 12) {
			// The loading is complete, call the function we want executed once the iframe is loaded
			afterLoading(ifrm);
			return;
		}

		// failsafe
		if(runTimes > 1000){
			return;
		}

		// If we are here, it is not loaded. Set things up so we check the status again in 1 second
		window.setTimeout(function(){
			checkIframeLoaded(ifrm,runTimes)
		}, 1000);
	}

	function afterLoading(ifrm){
		// Get a handle to the iframe element
		var iframeDoc = ifrm.contentDocument ? ifrm.contentDocument : ifrm.contentWindow.document;

		var width = $(iframeDoc).find("#rptWidth").val();
		var height = $(iframeDoc).find("#rptHeight").val();
		var ratio = (height / width);
		var newWidth = getDocWidth( ifrm );

		//alert("W: " + width + " H: " + height + "R: " + ratio);

		ifrm.style.visibility = 'hidden';
		//ifrm.style.height = (newWidth * ratio) + "px";
		ifrm.style.height = "650px";
		ifrm.style.visibility = 'visible';
		
	}

});


</script>