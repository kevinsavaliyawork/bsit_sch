$(document).ready(function() {
  var typingTimer;                //timer identifier
  var doneTypingInterval = 500;

 var $searchInput = $('#searchAllData');
  
  //on keyup, start the countdown
  $searchInput.on('keyup', function () {
    // show the user we are loading
    var loadingHTML = '<li class="noResult"><p>Loading...</p></li>';
    showResultsArea(loadingHTML);
        
    // reset the timer
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTypingsearch, doneTypingInterval);
  });
  


  //user is "finished typing," do something
  function doneTypingsearch () {
    // firstly get the value of the input field
    var $searchTerm = $searchInput.val();
          
    // Fire the AJAX call to return the list of matching terms
    var $listHTML = '';
    var $searchName = '';
    
    // we only do the search if the use has provided at least 1 character
    if($searchTerm.length > 0){
      $.ajax({
         //url: base_url + "core/filters",
        url: base_url+"QuickFilterSearch/quickfilterView",
        type: 'GET',
        data: {'searchTerm': $searchTerm},
        cache: false,
        dataType : 'json',
        success: function(data) {
          console.log(data);
        $listHTML = '';
        $.each(data, function() {
          $searchName = this.search_value;


          if(this.search_value !== this.search_term){
            $searchName = this.search_value + ': '+this.search_term;
          }
          $listHTML = $listHTML +'<li class="filterSearchResultLi" data-dimid="'+this.dim_id+'" data-dimvalue="'+this.search_value+'" data-dimname="'+this.dim_name+'">';
          $listHTML = $listHTML + $searchName+'<br/><span class="secondaryTxt">'+this.dim_name+'</span></li>';
        });
        




        // do we have any results?
        if($listHTML === ""){
          $listHTML = '<li class="noResult"><p>Your search <b>'+$searchTerm+'</b> did not match any known dimensions. <br/></p>';
          $listHTML = $listHTML + '<p> Suggestions: <br/> - Make sure the words are spelt correctly <br/> - Try and different keyword <br/> - Find the search term using Analysis; Group results by the dimension you need</p></li>';     
        }
        
        // show the results under the search bar
        showResultsArea($listHTML);

        },
        error: function(xhr, desc, err) {
        console.log(xhr + "\n" + err);
        }
      });
    }
    
    // was the search term populated
    if($searchTerm.length === 0){
      $listHTML = '<li class="noResult"><p>Search for any <b>Dimension</b></p> <p>A list can be found in the Analysis page group by list.</p>';
      $listHTML = $listHTML + '</li>';
      
      // show the box with instructions
      showResultsArea($listHTML);
    }
  }

// --------------------------------
// Show the results or loading or message about no results
// -------------------------------- 
function showResultsArea($inputHTML){
  // show the cross allowing clear of search field
  $('#filtersClearSearch').show();
  
  // hide the advanced search and arrow
  $('#advancedSearchDropArrow').hide();
  $('#advancedSearch').hide();
  
  // set the html to the newly loaded html and show the results
  $('#filterSearchResultsListData').html($inputHTML);
  $('#filterSearchResultsData').show();
}


// --------------------------------
// Close search box - clear results
// -------------------------------- 
function clearFiltersSearchAll(){
  // show the search box
  $('#filterSearchResultsData').hide();
  
  // hide the clear button
  $('#filtersClearSearch').hide();
  
  // show advanced search arrow
  $('#advancedSearchDropArrow').show();
  $('#advancedSearchDownImg').show();
  $('#advancedSearchUpImg').hide();
  
  // clear the input field
  $('#searchAllData').val('');  
}

$('#filterSearchResultsListData').on('click', 'li.filterSearchResultLi', function() {
    // close the results area of search
    clearFiltersSearchAll(); 
    
    // get the data needed, both ID and value
    var $dimID    = $(this).data('dimid');
    var $dimValue = $(this).data('dimvalue');
    var $dimName  = $(this).data('dimname');
    
    // add this option to the list
    addFilterAll($dimID,$dimValue,$dimName,'=');
    
    /*var $liHTML   = '<li  data-dimid="'+$dimID+'" data-dimvalue="'+$dimValue+'" data-dimname="'+$dimName+'" data-dimopp="=" data-dimperm="0">';
    $liHTML     = $liHTML +$dimValue+'<br/><span class="secondaryTxt">'+$dimName+' =</span>';
    $liHTML     = $liHTML +'<a class="dimRemover" href="javascript:void(0)">x</a></li>';
    $('#dimSelectedUL').append($liHTML);*/
  }); 



// --------------------------------
// add a filter 
// --------------------------------
// return 0 = all good, created
// return 1 = error, unknown
// return 2 = already created
function addFilterAll(dimID,dimValue,dimName,dimOpper,uniqueAdhock = 0,display = ''){
  
  var curFilterDimID    = '';
  var curFilterDimValue = '';
  var curFilterDimOpp   = '';
  var curFilterDimPerm  = '';
  var createNewFilter   = true;
  // does the filter already exist?
  $("#dimSelectedULAll li").each(function() {
    // get the data needed; ID, value and opp
    curFilterDimID    = $(this).data('dimid');
    curFilterDimValue = $(this).data('dimvalue');
    curFilterDimOpp   = $(this).data('dimopp');
    curFilterDimPerm  = $(this).data('dimperm');
    
    // if unique is specified, any matching adhock filters should be removed
    if(uniqueAdhock == 1 && curFilterDimPerm == 0 && curFilterDimID == dimID){
      $(this).remove();
    }else{    
      // check if this (in loop) is the same filter we are about to add
      if(curFilterDimID == dimID && curFilterDimValue == dimValue && curFilterDimOpp == dimOpper){
        createNewFilter = false;
        return 2;
      }
    }
  });
  
  // should we?
  if(createNewFilter === true){
    // if the display is empty, use value
    if(display === ''){
      display = dimValue;
    }
    // add the filter
    var $liHTML   = '<li  data-dimid="'+dimID+'" data-dimvalue="'+dimValue+'" data-dimname="'+dimName+'" data-dimopp="'+dimOpper+'" data-dimperm="0">';
    $liHTML     = $liHTML +display+'<br/><span class="secondaryTxt">'+dimName+' '+dimOpper+'</span>';
    $liHTML     = $liHTML +'<a class="dimRemover" href="javascript:void(0)">x</a></li>';
    $('#dimSelectedULAll').append($liHTML);
    
    return 0;
  }
  return 1;
}

 $('#dimSelectedULAll').on('click', 'li a.dimRemover', function() {
    // get parent values (the one clicked on)
    $(this).parent().remove();
    
  }); 


  }); 