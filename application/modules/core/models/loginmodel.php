<?php

class loginmodel extends brain_Model
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('core/Bsit_io', 'API');
    $this->load->library('session');
  }

  // -- REQUEST THE system data
  public function getSystemData()
  {

    $CURLDATA_sys_api = array('collections' => json_encode(
      array('colls' => array(array(
        'coll_name' => 'sys_params'

      )))
    ));
    $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
    $page_result_sys_api = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_sys_api);
    $page_result_sys_api = json_decode($page_result_sys_api);




    if (isset($page_result_sys_api->Data)) {
      $page_result_sys_api = $page_result_sys_api->Data;
    }

    return $page_result_sys_api;
  }


  /**
   * Get user data with a given username
   * @param $username
   * @return mixed
   */
  public function getUserByUsername($username)
  {

    if (!empty($username)) {

      $CURLDATA = array('collections' => json_encode(
        array('colls' => array(array(
          'coll_name' => 'users',
          'filter' => "usr.username = '" . $username . "'"
        )))
      ));
      $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
      $user_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

      $user_result = json_decode($user_result);

      return $user_result->Data[0]->Results[0];
    }

    return false;
  }

  /**
   * Sets the reset_password_key to the value passed in the param array
   * @param $params
   * @return bool
   */
  public function setUserResetPasswordKey($params)
  {
    $filters = [];
    foreach ($params as $key => $value) {
      $filters[] = ['key' => $key, 'value' => $value];
    }
    $parameters = [
      'key' => 'setresetpasswordkey',
      'filters' => $filters
    ];
    $result = $this->API->CallAPI("GET", API_BASEURL . API_METHOD_GET, json_encode($parameters), false);
    $result = json_decode($result);
    if ($result->HasError) {
      return false;
    }
    return true;
  }


  /**
   * Send a request to send the email.
   * @param $params
   * @return bool
   */
  public function createRequestToSendEmailResetPassword($params)
  {
    $filters = [];
    $filters[] = ['key' => 'siteRef', 'value' => SITE_REF];
    foreach ($params as $key => $value) {
      $filters[] = ['key' => $key, 'value' => $value];
    }
    $parameters = [
      'key' => 'createemailrequest',
      'filters' => $filters
    ];
    $result = $this->API->CallAPI("GET", API_BASEURL . API_METHOD_GET, json_encode($parameters), false);
    $result = json_decode($result);
    if ($result->HasError) {
      return false;
    }
    return true;
  }


  /**
   * Updates the user number of failed login attempts and if necessary, locks the user.
   * @param $params
   * @return bool|msg
   */
  public function setUserFailedLoginAttempts($params)
  {
    $filters = [];
    foreach ($params as $key => $value) {
      $filters[] = ['key' => $key, 'value' => $value];
    }
    $parameters = [
      'key' => 'setuserfailedloginattempts',
      'filters' => $filters
    ];
    $result = $this->API->CallAPI("GET", API_BASEURL . API_METHOD_GET, json_encode($parameters), false);
    $result = json_decode($result);
    if ($result->HasError) {
      return false;
    }
    return $result;
  }



  // -- get user data
  public function getUserDetail($username)
  {


    $CURLDATA = array('collections' => json_encode(
      array('colls' => array(array(
        'coll_name' => 'users',
        'filter' => "usr.username = '" . $username . "'"
      )))
    ));
    $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
    $user_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);


    $user_result = json_decode($user_result);

    if (isset($user_result->Data)) {
      $user_result = $user_result->Data;
    }
    return $user_result;
  }


  // -- get user collection data
  public function getUserCollectionData($user_id)
  {


    $CURLDATA = array('collections' => json_encode(
      array('colls' => array(array(
        'coll_name' => 'users',
        'filter' => "usr._id = '" . $user_id . "'"
      )))
    ));
    $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
    $user_collection_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
    $user_collection_result = json_decode($user_collection_result);
    if (isset($user_collection_result->Data)) {
      $user_collection_result = $user_collection_result->Data;
    }
    return $user_collection_result;
  }
}
