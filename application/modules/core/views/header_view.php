<?php
    // -------------------------------------------------
    // -- CREATE HEADER_VIEW CACHE FILE & CALL FROM HERE
    // -------------------------------------------------
    // Its Inclue : modal code (Add/Edit/Delete/Nots)

    $addHTML = '';
    $editHTML = '';
    $multi_editHTML = '';
    $multi_deleteHTML = '';
    $updateFields = '';
    $updateFields = '<input type="hidden" name="key_id" value="'.$frow->key_id.'"/>';
    $updateFields .= '<input type="hidden" name="key_update_date" value="'.$frow->key_update_date.'"/>';
    $updateFields .= '<input type="hidden" name="key_update_by" value="'.$frow->key_update_by.'"/>';
    $editHTML =  $updateFields;
    $multi_editHTML .=  $updateFields;
    $multi_deleteHTML .=  $updateFields;

    $CURLDATA_load_data = array('collections' => json_encode(
        array('colls' => array(array(
            'coll_name' => 'data_tables',
			'numrec' => '9999',
            //'filter' => "tbl.collection_id =".$prop->EditSource
        )))
    ));

    $BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;

    $frame_result_load_data = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_load_data);

    $frame_result_load_data = json_decode($frame_result_load_data);

    $collData = [];
    foreach ($frame_result_load_data->Data[0]->Results as $single) {
        if (!array_key_exists($single->CollectionId,$collData)) {
            $collData[$single->CollectionId] = $single->Alias;
        }
    }
	
    foreach ($frame['Properties'] as $prop) {

        // build a HTML string for display
        $propTitle  = $prop->PropTitle;
        $disabledMsg = ($prop->PropReadOnly == 1 ? "disabled" : "");
        $requiredMsg = ($prop->PropReq == 1 ? "required" : "");
        $hideAdd = ($prop->hide_on_add == 1 ? "display: none" : "");
        $hideEdit = ($prop->hide_on_edit == 1 ? "display: none" : "");
       

         

        // $aliasvalue = $frame_result_load_data->Data[0]->Results[0]->Alias;
        $aliasvalue = (!empty($prop->EditSource)) ? $collData[$prop->EditSource] : '';


        switch ($prop->PropEditType) {
             case 'TXT':

            $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }
                 $inputMask = $prop->input_mask;
                if ($prop->default_value != '') { // SET DEFAULT FOREIGH KEY VALUE
                    $addHTML .= '<div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                                        <input type="text" autocomplete="new-'.$prop->PropName.'" name="'.$prop->PropName.'" value="'.$defaultValue.'" data-inputmask="'. $inputMask.'" regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="" readonly />
                                        <label>'.$propTitle.'</label>
                                      </div>';
                } else {
                    if ($prop->PropReadOnly<>1) {
                        $addHTML .= '  <div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                                        <input type="text" autocomplete="new-'.$prop->PropName.'" name="'.$prop->PropName.'" value="'.$defaultValue.'" '.$disabledMsg.' '.$requiredMsg.' data-inputmask="'. $inputMask.'" regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="">
                                        <span></span>
                                        <label>'.$propTitle.'</label>
                                      </div>';
                    };
                }

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <input type="text" autocomplete="new-'.$prop->PropName.'" name="'.$prop->PropName.'" value="" data-inputmask="'. $inputMask.'" regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="">
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }

                $editHTML .= '  <div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <input type="text" autocomplete="new-'.$prop->PropName.'" name="'.$prop->PropName.'" value="" '.$disabledMsg.'  '.$requiredMsg.' data-inputmask="'. $inputMask.'"  regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="">

                                    <label>'.$propTitle.'</label>
                                  </div>';
            break;


            //  case 'MTXT':

            // $defaultValue = $prop->default_value;
            
            // if (substr($prop->default_value, 0, 1) == 'S') {

            //     $session_variable = substr($prop->default_value, 2, -1);

            //     $defaultValue = $this->session->userdata($session_variable);
            // } elseif (substr($prop->default_value, 0, 1) == 'Q') {
            //     $query_variable = substr($prop->default_value, 2, -1);
            //     $defaultValue = $_GET[$query_variable];
            // }
            //     // $inputMask = "'mask':'(999) 9999 999'";
            //     $inputMask = $prop->input_mask;
                
            //     if ($prop->default_value != '') { // SET DEFAULT FOREIGH KEY VALUE
            //         $addHTML .= '<div style="'.$hideAdd.'" id="inputMaskNumber" class="mui-textfield '.$prop->style_class.'">
                                       

            //                             <input id="cc" type="text" data-inputmask="'. $inputMask.'" name="'.$prop->PropName.'" value="'.$defaultValue.'" regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="" readonly />
            //                             <label>'.$propTitle.'</label>
            //                           </div>';
            //     } else {
            //         if ($prop->PropReadOnly<>1) {
            //             $addHTML .= '  <div style="'.$hideAdd.'" id="inputMaskNumber" class="mui-textfield '.$prop->style_class.'">
                                        
            //                             <input id="cc" type="text" data-inputmask="'. $inputMask.'" name="'.$prop->PropName.'" value="'.$defaultValue.'" '.$disabledMsg.' '.$requiredMsg.' regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="" />
            //                             <span></span>
            //                             <label>'.$propTitle.'</label>
            //                           </div>';
            //         };
            //     }

            //     if ($prop->allow_multi_field_edit == 1) {
            //         $multi_editHTML .= '<div style="'.$hideEdit.'" id="inputMaskNumber" class="mui-textfield '.$prop->style_class.'">
            //                         <input type="text" data-inputmask="'. $inputMask.'" name="'.$prop->PropName.'" value=""  regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="">
            //                         <label>'.$propTitle.'</label>
            //                       </div>';
            //     }

            //     $editHTML .= '  <div style="'.$hideEdit.'" id="inputMaskNumber" class="mui-textfield '.$prop->style_class.'">
            //                         <input type="text" data-inputmask="'. $inputMask.'" name="'.$prop->PropName.'" value="" '.$disabledMsg.'  regex= "'.htmlentities($prop->regular_expression).'" data-toggle="tooltip" title="">
                                    

            //                         <label>'.$propTitle.'</label>
            //                       </div>';
            // break;


            case 'HID':


            $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }



                if ($prop->default_value != '') { // SET DEFAULT FOREIGH KEY VALUE
                    $addHTML .= '<input type="hidden" name="'.$prop->PropName.'" value="'.$defaultValue.'" readonly />';
                } else {
                    if ($prop->PropReadOnly<>1) {
                        $addHTML .= '<input type="hidden" name="'.$prop->PropName.'" value="'.$defaultValue.'" '.$disabledMsg.' '.$requiredMsg.'/>';
                    };
                }

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .= '<input type="hidden" name="'.$prop->PropName.'" value="'.$defaultValue.'"  />';
                }

                $editHTML   .= '<input type="hidden" name="'.$prop->PropName.'" value="'.$defaultValue.'" '.$disabledMsg.'  '.$requiredMsg.'/>';
            break;

                //code start for password type Added by HD
               case 'PWD':


               $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }

                if ($prop->default_value != '') { // SET DEFAULT FOREIGH KEY VALUE
                    $addHTML .= '  <div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                      <input type="password" name="'.$prop->PropName.'" value="'.$defaultValue.'" readonly />
                      <label>'.$propTitle.'</label>
                       </div>';
                } else {
                    if ($prop->PropReadOnly<>1) {
                        $addHTML .= '  <div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                      <input type="password" name="'.$prop->PropName.'" value="'.$defaultValue.'" '.$disabledMsg.' '.$requiredMsg.'/>
                      <label>'.$propTitle.'</label>
                       </div>';
                    };
                }

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                     <input type="password" name="'.$prop->PropName.'" value=""  />
                     <label>'.$propTitle.'</label>
                      </div>';
                }

                $editHTML .= '  <div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                     <input type="password" name="'.$prop->PropName.'" value="" '.$disabledMsg.'  '.$requiredMsg.'/>
                     <label>'.$propTitle.'</label>
                      </div>';
               break;
            //code end for password type Added by HD
            case 'MLE':
                if ($prop->PropReadOnly<>1) {
                    $addHTML .=  '  <div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                                    <textarea name="'.$prop->PropName.'" '.$disabledMsg.' '.$requiredMsg.'></textarea>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }
                $editHTML   .=  '  <div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <textarea name="'.$prop->PropName.'" '.$disabledMsg.' '.$requiredMsg.'></textarea>
                                    <label>'.$propTitle.'</label>
                                  </div>';

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .=  '  <div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <textarea name="'.$prop->PropName.'" ></textarea>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }
            break;

            case 'CKE':
                if ($prop->PropReadOnly<>1) {
                    $addHTML .=  '  <div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                                    <textarea name="'.$prop->PropName.'" class="ckeditor" '.$disabledMsg.' '.$requiredMsg.'></textarea>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }
                $editHTML   .=  '  <div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <textarea name="'.$prop->PropName.'" class="ckeditor" '.$disabledMsg.' '.$requiredMsg.'></textarea>
                                    <label>'.$propTitle.'</label>
                                  </div>';

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .=  '  <div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <textarea name="'.$prop->PropName.'" class="ckeditor"></textarea>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }
            break;

            case 'YN':

            if ($prop->PropReadOnly <> 1) {

                if ($prop->default_value == 1) {
                    $checked_val = 'checked';
                }


                $addHTML .= '<div dds="'.$prop->default_value.'" style="'.$hideAdd.'" class="mui-checkbox '.$prop->style_class.'">
                <label class="bsit_check">
                  <input type="checkbox" name="'.$prop->PropName.'"  '.$requiredMsg.' '.$checked_val.'>
                 '.$propTitle.'
                <span class="bsit_checkmark bsit_checkmark_design_mode_popup"></span>
                </label>
              </div>';
            }
            $editHTML   .=  '<div class="mui-checkbox '.$prop->style_class.'">
                <label class="bsit_check">
                  <input style="'.$hideEdit.'" type="checkbox" name="'.$prop->PropName.'" '.$disabledMsg.'  '.$requiredMsg.'>
                 '.$propTitle.'
                <span class="bsit_checkmark bsit_checkmark_design_mode_popup"></span>
                </label>
              </div>';

            if ($prop->allow_multi_field_edit == 1) {
                $multi_editHTML .=  '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'" style="margin-bottom: 0px;"><label>'.$propTitle.'</label></div>
                                <div class="mui-select">
                                    <select name="'.$prop->PropName.'" >
                                        <option value=""></option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>';
            }
            break;

            case 'SEL':


            $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }


                // get the json string of values - if an available
                $selectOptions = json_decode($prop->EditValue);
                // if this isn't read only
                if ($prop->PropReadOnly<>1) {
                    $addHTML .=  '<div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'" style="margin-bottom: 0px;"><label>'.$propTitle.'</label>
                                <div class="mui-select bsit_statusdiv" style="padding-top: 0px !important;"><select name="'.$prop->PropName.'"  '.$requiredMsg.'>';
                    // if none in array, simply show the NA option only
                    if (count($selectOptions)<1) {
                        $addHTML .=  '<option value="NA">No values</option>';
                    } else {
                        foreach ($selectOptions as $optVal=>$opt) {
                            if ($optVal == '_empty_') {
                                $optVal = '';
                            }
                            $selected_val = '';
                            if ($optVal == $defaultValue) {
                                $selected_val = 'selected';
                            }
                            $addHTML .=  '<option value="'.htmlspecialchars($optVal).'" '.$selected_val.'>'.$opt.'</option>';
                        }
                    };
                    $addHTML .=  '</select></div></div>';
                }
                $editHTML .=  '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'" style="margin-bottom: 0px;"><label>'.$propTitle.'</label>
                            <div class="mui-select bsit_statusdiv" style="padding-top: 0px !important;"><select name="'.$prop->PropName.'" '.$disabledMsg.'  '.$requiredMsg.'>';
                                // if none in array, simply show the NA option only
                                if (count($selectOptions) < 1) {
                                    $editHTML .=  '<option value="NA">No values</option>';
                                } else {
                                    foreach ($selectOptions as $optVal=>$opt) {
                                        if ($optVal == '_empty_') {
                                            $optVal = '';
                                        }
                                        $editHTML .=  '<option value="'.htmlspecialchars($optVal).'">'.$opt.'</option>';
                                    };
                                };
                $editHTML .=  '</select></div></div>';

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'" style="margin-bottom: 0px;"><label>'.$propTitle.'</label>
                                <div class="mui-select bsit_statusdiv" style="padding-top: 0px !important;"><select name="'.$prop->PropName.'" >';
                    // if none in array, simply show the NA option only
                    $multi_editHTML .= '<option value=""></option>';
                    if (count($selectOptions) < 1) {
                        $multi_editHTML .= '<option value="NA">No values</option>';
                    } else {
                        foreach ($selectOptions as $optVal=>$opt) {
                            if ($optVal == '_empty_') {
                                $optVal = '';
                            }
                            $multi_editHTML .=  '<option value="'.htmlspecialchars($optVal).'">'.$opt.'</option>';
                        };
                    };
                    $multi_editHTML .=  '</select></div></div>';
                }
            break;
            case 'COLL':


            $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }
                // get the json string of values - if an available
                $editValue = str_replace('"', "'", $prop->EditValue);
                $editSource = $prop->EditSource;
                // read only field shouldn't be available during an add
                if ($prop->PropReadOnly <> 1) {
                    $addHTML .=  '  <div style="'.$hideAdd.'" class="mui-textfield ui-front '.$prop->style_class.'">
                                        <input name="'.$prop->PropName.'" class="autoCompColl" type="text" value="'.$defaultValue.'" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" data-modalid="addModel'.$frame['PFID'].'" '.$requiredMsg.' data-dirty="0" data-selectedid="'.$defaultValue.'"/>
                                        <label>'.$propTitle.'</label>
                                    </div>';
                }
                $editHTML .=  ' <div style="'.$hideEdit.'" class="mui-textfield ui-front '.$prop->style_class.'">
                                    <input name="'.$prop->PropName.'" class="autoCompColl" type="text" value="" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" data-modalid="editModel'.$frame['PFID'].'" '.$disabledMsg.' '.$requiredMsg.' data-dirty="0" data-selectedid="'.$defaultValue.'"/>
                                    <label>'.$propTitle.'</label>
                                </div>';
               // $editHTML .=  ' <div style="'.$hideEdit.'" class="mui-textfield ui-front '.$prop->style_class.'">
               //                      <input name="'.$prop->PropName.'" class="autoCompColl" type="text" value="" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" data-modalid="editsideModel'.$frame['PFID'].'" '.$disabledMsg.' '.$requiredMsg.'/>
               //                      <label>'.$propTitle.'</label>
               //                  </div>';                 

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .=  '<div class="mui-textfield ui-front '.$prop->style_class.'">
                                    <input name="'.$prop->PropName.'" class="autoCompColl" type="text" value="" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" data-modalid="editModel'.$frame['PFID'].'" />
                                    <label>'.$propTitle.'</label>
                                </div>';
                }
            break;
            case 'ADVC':


            $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }

            // get the json string of values - if an available
            $editValue = str_replace('"', "'", $prop->EditValue);
            $editSource = $prop->EditSource;
            // read only field shouldn't be available during an add

            if ($prop->PropReadOnly <> 1) {
                $addHTML .=  '  <div style="'.$hideAdd.'" class="mui-textfield ui-front '.$prop->style_class.'">
                                    <input name="'.$prop->PropName.'" class="autoCompCollAdv" type="text"  data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" value="'.$defaultValue.'" data-modalid="addModel'.$frame['PFID'].'" '.$requiredMsg.' data-dirty="0" data-selectedid="'.$defaultValue.'"/>
                                    <label>'.$propTitle.'</label>
                                </div>';
            }
            $editHTML .=  ' <div style="'.$hideEdit.'" class="mui-textfield ui-front '.$prop->style_class.'">
                                <input name="'.$prop->PropName.'" class="autoCompCollAdv" type="text" value="" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" data-modalid="editModel'.$frame['PFID'].'" '.$disabledMsg.' '.$requiredMsg.' data-dirty="0" data-selectedid="'.$defaultValue.'"/>
                                <label>'.$propTitle.'</label>
                            </div>';
            
			
			if ($prop->allow_multi_field_edit == 1) {
                $multi_editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield ui-front '.$prop->style_class.'">
                                <input name="'.$prop->PropName.'" class="autoCompCollAdv" type="text" value="" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" data-modalid="editModel'.$frame['PFID'].'" '.$disabledMsg.' '.$requiredMsg.'/>
                                <label>'.$propTitle.'</label>
                            </div>';
            }  
            break;
            // ADD Advance Multiselect  start
            case 'ADVCM':


            $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }

            // get the json string of values - if an available
            $editValue = str_replace('"', "'", $prop->EditValue);
            $editSource = $prop->EditSource;
            // read only field shouldn't be available during an add

            if ($prop->PropReadOnly <> 1) {
                $addHTML .=  '  <div style="'.$hideAdd.'" class="mui-textfield ui-front '.$prop->style_class.'"> 
                <ul class="multiSelect-ul bsit_multi_select">
                    <li class="li-input">
                        <input name="" type="text" class="advanceMultiSelect bsit_select_textbox" tablename="ProductInfo" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" value="'.$defaultValue.'" data-modalid="addModel'.$frame['PFID'].'" '.$requiredMsg.' />
                    </li>

                    <input type="hidden" name="" class="hidden form-control hf_advanceMultiSelect" id="hf_advanceMultiSelect" value="" labelMultiSelect="'.$prop->PropName.'_label"/>
                    <input type="hidden" name="" class=" hidden form-control hf_advanceMultiSelect_value" id="hf_advanceMultiSelect_value" value="" valueMultiSelect="'.$prop->PropName.'_value">
                    <input type="hidden" name="'.$prop->PropName.'" class="advanceMultiSelect advanceMultiSelectRemoveVal hidden form-control" value="" >
                </ul>
            
                    <label>'.$propTitle.'</label>
                </div>';
            }
            $editHTML .=  ' <div style="'.$hideEdit.'" class="mui-textfield ui-front '.$prop->style_class.'">
                                 <ul class="multiSelect-ul bsit_multi_select">
                                 <input type="hidden" name="" class="hidden form-control hf_advanceMultiSelect" id="hf_advanceMultiSelect" value="" labelMultiSelect="'.$prop->PropName.'_label"/>
                                     <li class="li-input">
                                         <input name="" type="text" class="advanceMultiSelect bsit_select_textbox" tablename="ProductInfo" data-returnpropname="'.$prop->PropName.'" data-coll="'.$editSource.'" data-alias="'.$aliasvalue.'" data-columns="'.$editValue.'" value="'.$defaultValue.'" data-modalid="addModel'.$frame['PFID'].'" '.$requiredMsg.' />
                                     </li>
                             
                             <input type="hidden" name="" class=" hidden form-control hf_advanceMultiSelect_value" id="hf_advanceMultiSelect_value" value="" valueMultiSelect="'.$prop->PropName.'_value">
                             <input type="hidden" name="'.$prop->PropName.'" class="advanceMultiSelect advanceMultiSelectRemoveVal hidden form-control" value="" >
                                 </ul>   
                                <label>'.$propTitle.'</label>
                            </div>';
           
            break;
            // ADD Advance Multiselect  Stop
            case 'DATE':
                //if ($prop->PropReadOnly<>1) {
                $defaultDate = "";

                $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }


                if(is_numeric($prop->default_value))
                {
                    $defaultDate = date('Y/m/d', strtotime($defaultValue." days"));
                }
                if ($prop->PropReadOnly <> 1) {
 
                $addHTML .= '  <div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                                <input type="text" name="'.$prop->PropName.'" class="datepicker" value="'.$defaultDate.'" '.$disabledMsg.' '.$requiredMsg.' '.($prop->PropReadOnly = 1 ? "readonly" : "").'/>
                                <label>'.$propTitle.'</label>
                              </div>';
                };
                $editHTML   .= '  <div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <input type="text" name="'.$prop->PropName.'" class="datepicker" value="" '.$disabledMsg.' '.$requiredMsg.'/>
                                    <label>'.$propTitle.'</label>
                                  </div>';

                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <input type="text" name="'.$prop->PropName.'" class="datepicker" value="" />
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }

            break;
            case 'TIME':
                if ($prop->PropReadOnly <> 1) {
                    $addHTML .= '<div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                                    <input type="text" name="'.$prop->PropName.'" class="timeperigon" value="" '.$disabledMsg.' '.$requiredMsg.'/>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                };
                $editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <input type="text" name="'.$prop->PropName.'" class="timeperigon" value="" '.$disabledMsg.' '.$requiredMsg.'/>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <input type="text" name="'.$prop->PropName.'" class="timeperigon" value="" />
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }
            break;
            case 'FILE':
                if ($prop->PropReadOnly <> 1) {
                    $addHTML .= '<div style="'.$hideAdd.'" class="mui-textfield '.$prop->style_class.'">
                                    <img name="'.$prop->PropName.'"  src="'.$disabledMsg.'" id="image_file"  '.$requiredMsg.'/>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                };
                $editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <img name="'.$prop->PropName.'"  src="'.$disabledMsg.'" id="image_file" '.$requiredMsg.'/>
                                    <label>'.$propTitle.'</label>
                                  </div>';
                if ($prop->allow_multi_field_edit == 1) {
                    $multi_editHTML .= '<div style="'.$hideEdit.'" class="mui-textfield '.$prop->style_class.'">
                                    <img name="'.$prop->PropName.'"  src="'.$disabledMsg.'" id="image_file" />
                                    <label>'.$propTitle.'</label>
                                  </div>';
                }
            break;

             case 'GHDR':

// print_r($prop); exit;
            $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }

               
                $addHTML .= '  <div class="bsit_form_titel_popup" style="'.$hideEdit.' background:; padding: 0 0 0 15px; float: left;" class="mui-textfield '.$prop->style_class.'">
                                    <label style="line-height: 42px; color: #5bada6; font-size: 18px; font-weight: bold;">'.strtoupper($propTitle).'</label>
                                  </div>';
                $editHTML .= '  <div class="bsit_form_titel_popup" style="'.$hideEdit.' background:; padding: 0 0 0 15px; float: left;" class="mui-textfield '.$prop->style_class.'">
                                    <label style="line-height: 42px; color: #5bada6; font-size: 18px; font-weight: bold;">'.strtoupper($propTitle).'</label>
                                  </div>';
            break;

        }
    }



    // --------------------------------------------------
    // -- ADD THE ADD & EDIT & DELETE & NOTES MODAL FORMS
    // --------------------------------------------------
    if($frame['Properties'][0]->enable_pane_view == 1){

    // <!-- -------------- ADD sidebar -------------- -->
    $html_model .= '<div id="addModel'.$frame['PFID'].'" class="sidenav model modal_'.$frame['PFID'].'" data-modal="Edit">
    <a href="javascript:void(0)" class="closebtn icon-Close" id="closeNavbar'.$frame['PFID'].'"></a>';
    $html_model .= '<h2 class="bsit_form_titel">Edit </h2>';
    $html_model .= '<hr/>';
    $html_model .= '<form class="bsit_form" autocomplete="off" id="addFormside'.$frame['PFID'].'" class="bsit-add-new-form" data-collid="'.$frame['collection'].'">';
    $html_model .= $addHTML;
    $html_model .= '</form>';
    $html_model .= '<div style="text-align:center;" class="mui-col-md-12">';
    $html_model .= '<button class="bsit-add-new-sidebar-save mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Add</button>';
    $html_model .= '<img id="correctImg" src="'.base_url().'assets/core/images/correct.png"
      style="display:none; vertical-align:middle">';

    $html_model .= '<img id="errorImg" src="'.base_url().'assets/core/images/error.png"
      style="display:none; vertical-align:middle">';
    $html_model .= '</div>';
    $html_model .= '</div>';    

    }else{

    $html_model = '<!-- -------------- ADD MODAL -------------- -->';
    $html_model .= '<div id="addModel'.$frame['PFID'].'" class="modal modal_'.$frame['PFID'].' fade in bsit_main_popup" data-modal="Add" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 99999;">';
    $html_model .= '<div class="mui-panel" style="margin-bottom:0px;">';
    // $html_model .='<div class="bsit_popup_title_fix">';
    $html_model .= '<button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">
                    <svg class="" viewBox="0 0 24 24"><path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                </button>';
    $html_model .= '<h2 class="bsit_form_titel"> Add </h2>';
    $html_model .= '<hr/>';
    // $html_model .='</div>';
    $html_model .= '<form class="bsit_form bsit_popup_scroll" autocomplete="off" id="addForm'.$frame['PFID'].'" class="bsit-add-new-form" data-collid="'.$frame['collection'].'">';
    $html_model .= $addHTML;
    $html_model .= '</form>';
    $html_model .= '<div style="text-align:center;" class="mui-col-md-12">';
    $html_model .= '<button class="bsit-add-new-save mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Add</button> <button type="submit" class="mui-btn mui-btn--primary bsit_cancel_btn" data-dismiss="modal"> Cancel </button>';
    $html_model .= '<img id="correctImg" src="'.base_url() .'assets/core/images/correct.png"
      style="display:none; vertical-align:middle">';

    $html_model .= '<img id="errorImg" src="'.base_url() .'assets/core/images/error.png"
      style="display:none; vertical-align:middle">';
    $html_model .= '</div>';
    $html_model .= '</div>';
    $html_model .= '</div>';
    
    }

    if($frame['Properties'][0]->enable_pane_view == 1){

        // sidebar start edit
        $html_model .= '<div id="editModel'.$frame['PFID'].'" class="sidenav model modal_'.$frame['PFID'].'" data-modal="Edit">
        <a href="javascript:void(0)" class="closebtn icon-Close" id="closeNav'.$frame['PFID'].'"></a>';
        $html_model .= '<h2 class="bsit_form_titel">Edit </h2>';
        $html_model .= '<hr/>';
        $html_model .= '<form class="bsit_form" autocomplete="off" id="editsideModel'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
        $html_model .= $editHTML;
        $html_model .= '</form>';
        $html_model .= '<div style="text-align:center;" class="mui-col-md-12">';
        $html_model .= '<button id="commit-edit-update-'.$frame['PFID'].'" class="bsit-commit-edit-update mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Save</button>';
        $html_model .= '<img id="correctImg" src="'.base_url() .'assets/core/images/correct.png"
      style="display:none; vertical-align:middle">';

        $html_model .= '<img id="errorImg" src="'.base_url() .'assets/core/images/error.png"
      style="display:none; vertical-align:middle">';
        $html_model .= '</div>';
        $html_model .= '</div>';

    }else{
    
    $html_model .= '<!-- -------------- EDIT MODAL -------------- -->';
    $html_model .= '<div id="editModel'.$frame['PFID'].'" class="modal modal_'.$frame['PFID'].' fade in bsit_main_popup" data-modal="Edit" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 99999;">';
    $html_model .= '<div class="mui-panel" style="margin-bottom:0px;">';
    $html_model .= '    <button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">
                    <svg class="" viewBox="0 0 24 24"><path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                </button>';
    $html_model .= '<h2 class="bsit_form_titel">Edit </h2>';
    $html_model .= '<hr/>';
    $html_model .= '<form class="bsit_form bsit_popup_scroll" autocomplete="off" id="editForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
    $html_model .= $editHTML;
    $html_model .= '</form>';
    $html_model .= '<div style="text-align:center;" class="mui-col-md-12">';
    $html_model .= '<button id="commit-edit-update-'.$frame['PFID'].'" class="bsit-commit-edit mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Save</button> <button type="submit" class="mui-btn mui-btn--primary bsit_cancel_btn" data-dismiss="modal"> Cancel </button>';
    $html_model .= '</div>';
    $html_model .= '</div>';
    $html_model .= '</div>';
    
    }

    if($frame['Properties'][0]->enable_pane_view == 1){
       // sidebar start MULTI EDIT

       $html_model .= '<div id="sidemultiEditModel'.$frame['PFID'].'" class="sidenav model modal_'.$frame['PFID'].'" data-modal="MultiEdit">
       <a href="javascript:void(0)" class="closebtn icon-Close" id="closeNavmulti'.$frame['PFID'].'"></a>';
       $html_model .= '<div class="multi_edit_content_'.$frame['PFID'].' multi_edit_content bsit_form_titel">';
       $html_model .= 'Please select at least one record to edit.';
       $html_model .= '</div>'; 
       $html_model .= '<div class="multi_edit_from_'.$frame['PFID'].'" style="display:none;">'; 
       $html_model .= '<h2 class="bsit_form_titel">Multi Edit </h2>';
       $html_model .= '<hr/>';
       $html_model .= '<form class="bsit_form"autocomplete="off" id="multiEditForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
       $html_model .= $multi_editHTML;
       $html_model .= '</form>';

       $html_model .= '<div style="text-align:center;" class="mui-col-md-12">';
       $html_model .= '<button class="bsit-commit-multi mui-btn mui-btn--flat mui-btn--primary bsit_save_btn" data-commit-multi="Edit">Save</button>';
       $html_model .= '</div>';
       $html_model .= '</div>';
       $html_model .= '</div>';   


    }else{

        $html_model .= '<!-- -------------- MULTI EDIT MODAL -------------- -->';
        $html_model .= '<div id="multiEditModel'.$frame['PFID'].'" class="modal modal_'.$frame['PFID'].' fade in bsit_main_popup" data-modal="MultiEdit" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 99999;">';
        $html_model .= '<div class="mui-panel" style="margin-bottom:0px;">';
        $html_model .= '<button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">
                        <svg class="" viewBox="0 0 24 24"><path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                    </button>';

        $html_model .= '<div class="multi_edit_content_'.$frame['PFID'].' multi_edit_content bsit_form_titel">';
        $html_model .= 'Please select at least one record to edit.';
        $html_model .= '</div>';

        $html_model .= '<div class="multi_edit_from_'.$frame['PFID'].'" style="display:none;">';
        $html_model .= '<h2 class="bsit_form_titel">Multi Edit </h2>';
        $html_model .= '<hr/>';
        $html_model .= '<form class="bsit_form bsit_popup_scroll" autocomplete="off" id="multiEditForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
        $html_model .= $multi_editHTML;
        $html_model .= '</form>';
        $html_model .= '<div style="text-align:center;" class="mui-col-md-12">';
        $html_model .= '<button class="bsit-commit-multi mui-btn mui-btn--flat mui-btn--primary bsit_save_btn" data-commit-multi="Edit">Save</button> <button type="submit" class="mui-btn mui-btn--primary bsit_cancel_btn" data-dismiss="modal"> Cancel </button>';
        $html_model .= '</div>';
        $html_model .= '</div>';

        $html_model .= '</div>';
        $html_model .= '</div>';
    
    }

    if($frame['Properties'][0]->enable_pane_view == 1){

        // sidebar start DELETE
        $html_model .= '<div id="deleteModel'.$frame['PFID'].'" class="sidenav model modal_'.$frame['PFID'].'" data-modal="Delete">
        <a href="javascript:void(0)" class="closebtn icon-Close" id="deletecloseNav'.$frame['PFID'].'"></a>';
        $html_model .= '<h2 class="bsit_form_titel">Delete </h2>';
        $html_model .= '<hr/>';
        $html_model .= '<form class="bsit_form" autocomplete="off" id="deleteForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
        $html_model .= $updateFields;
        $html_model .= '<span class="bsit_side_nave_blank_title">Are you sure you would like to delete this?</span>';

        $html_model .= '</form>';
        $html_model .= '<div style="text-align:center;">';
        $html_model .= '<button id="commit-delete-'.$frame['PFID'].'" class="bsit-commit-delete mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Yes, Delete</button>';
        $html_model .= '<button class="mui-btn bsit-cancel-delete mui-btn--flat mui-btn--primary bsit_cancel_btn" data-dismiss="modal">No</button>';
        $html_model .= '</div>';
        $html_model .= '</div>';

    }else{

        $html_model .= '<!---------------- DELETE MODAL ---------------->';
        $html_model .= '<div id="deleteModel'.$frame['PFID'].'" class="modal modal_'.$frame['PFID'].' fade in bsit_main_popup" data-modal="Delete" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 99999;">';
        $html_model .= '<div class="mui-panel" style="margin-bottom:0px;">';
        $html_model .= '<h2 class="bsit_form_titel">Delete </h2>';
        $html_model .= '<hr/>';
        $html_model .= '<form class="bsit_form bsit_popup_scroll" autocomplete="off" id="deleteForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
        $html_model .= $updateFields;
        $html_model .= 'Are you sure you would like to delete this?';

        $html_model .= '</form>';
        $html_model .= '<div style="text-align:right;">';
        $html_model .= '<button id="commit-delete-'.$frame['PFID'].'" class="bsit-commit-delete mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Yes, Delete</button>';
        $html_model .= '<button class="mui-btn mui-btn--flat mui-btn--primary bsit_cancel_btn" data-dismiss="modal">No</button>';
        $html_model .= '</div>';
        $html_model .= '</div>';
        $html_model .= '</div>';
    }    
    
    if($frame['Properties'][0]->enable_pane_view == 1){

        $html_model .= '<div id="sidemultiDeleteModel'.$frame['PFID'].'" class="sidenav model modal_'.$frame['PFID'].'" data-modal="MultiEdit">
        <a href="javascript:void(0)" class="closebtn icon-Close" id="closeNavmultidelete'.$frame['PFID'].'"></a>';
        $html_model .= '<div class="multi_delete_content_'.$frame['PFID'].' multi_edit_content bsit_form_titel">';
        $html_model .= 'Please select at least one record to delete.';
        $html_model .= '</div>';
        $html_model .= '<div class="multi_delete_from_'.$frame['PFID'].'" style="display:none;">'; 
        $html_model .= '<h2 class="bsit_form_titel"> Delete </h2>';
        $html_model .= '<hr/>';
        $html_model .= '<form class="bsit_form" autocomplete="off" id="multiDeleteForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
        $html_model .= $multi_deleteHTML;
        $html_model .= '<span class="bsit_side_nave_blank_title">Are you sure you would like to delete this?</span>';
        $html_model .= '</form>';

        $html_model .= '<div style="text-align:center;">';
        $html_model .= '<button class="bsit-commit-multi mui-btn mui-btn--flat mui-btn--primary bsit_save_btn" data-commit-multi="Delete">Yes, Delete</button>';
        $html_model .= '<button class="mui-btn bsit-cancel-delete mui-btn--flat mui-btn--primary" data-dismiss="modal">No</button>';
        $html_model .= '</div>';
        $html_model .= '</div>';
        $html_model .= '</div>'; 

    }else{

        $html_model .= '<!-- -------------- MULTI DELETE MODAL -------------- -->';
        $html_model .= '<div id="multiDeleteModel'.$frame['PFID'].'" class="modal modal_'.$frame['PFID'].' fade in bsit_main_popup" data-modal="MultiDelete" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 99999;">';
        $html_model .= '<div class="mui-panel" style="margin-bottom:0px;">';
        $html_model .= '    <button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">
                        <svg class="" viewBox="0 0 24 24"><path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                    </button>';

        $html_model .= '<div class="multi_delete_content_'.$frame['PFID'].' multi_edit_content bsit_form_titel">';
        $html_model .= 'Please select at least one record to delete.';
        $html_model .= '</div>';

        $html_model .= '<div class="multi_delete_from_'.$frame['PFID'].'" style="display:none;">';
        $html_model .= '<h2 class="bsit_form_titel">Delete </h2>';
        $html_model .= '<hr/>';
        $html_model .= '<form class="bsit_form" autocomplete="off" id="multiDeleteForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
        $html_model .= $multi_deleteHTML;
        $html_model .= 'Are you sure you would like to delete this?';
        $html_model .= '</form>';
        $html_model .= '<div style="text-align:center;">';
        $html_model .= '<button class="bsit-commit-multi mui-btn mui-btn--flat mui-btn--primary bsit_save_btn" data-commit-multi="Delete">Yes, Delete</button>';
        $html_model .= '<button class="mui-btn mui-btn--flat mui-btn--primary" data-dismiss="modal">No</button>';
        $html_model .= '</div>';
        $html_model .= '</div>';

        $html_model .= '</div>';
        $html_model .= '</div>';

     }    

    $html_model .= '<!-- -------------- NOTES MODAL -------------- -->';
    $html_model .= '<div id="notesModel'.$frame['PFID'].'" class="modal modal_'.$frame['PFID'].' fade in bsit_main_popup" data-modal="Add Notes" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 99999;">';
    $html_model .= '<div class="mui-panel" style="margin-bottom:0px;">';
    $html_model .= '    <button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">
                    <svg class="" viewBox="0 0 24 24"><path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                </button>';
    $html_model .= '<h2 class="bsit_form_titel"> Notes </h2>';
    $html_model .= '<hr/>';
    $html_model .= '<form class="bsit_form" autocomplete="off" id="notesForm'.$frame['PFID'].'" data-collid="'.$frame['collection'].'">';
    $html_model .= '<input type="hidden" id="rec_id" value=""/>';
    $html_model .= '<div class="mui-textfield">
                <input type="text" name="NoteName" value="" id="note_name"/>
                <label>Name</label>
                </div>';
    $html_model .= '<div class="mui-textfield" style="margin-bottom: 0px;"><label>Types</label></div>
                <div class="mui-select">
                    <select name="NoteType" id="note_type" >
                        <option value="1">General</option>
                        <option value="2">Internal</option>
                    </select>
                </div>';
    $html_model .= '<div class="mui-textfield" style="margin-bottom: 0px;"><label>Note Types</label></div>
                <div class="mui-select">
                    <select name="NType" id="select_note_link_'.$frame['PFID'].'" >
                        <option value="1">Note</option>
                        <option value="2">Link</option>
                    </select>
                </div>';
    $html_model .= '<div class="mui-textfield" id="disp_by_type_note_'.$frame['PFID'].'">
                <textarea id="note_note_'.$frame['PFID'].'"></textarea>
                <label>Notes</label>
              </div>';
    $html_model .= '<div class="mui-textfield" id="disp_by_type_link_'.$frame['PFID'].'">
                <input type="text" id="note_link_'.$frame['PFID'].'"/>
                <label>Link</label>
              </div>';
    $html_model .= '</form>';
    $html_model .= '<div style="text-align:center;">';
    $html_model .= '<button id="notes-add-'.$frame['PFID'].'" class="mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Save</button>';
    $html_model .= '</div>';
    $html_model .= '</div>';

    $html_model .= '<div class="mui-panel note_listing" id="notes_list_'.$frame['PFID'].'" data-recid="" style="margin-bottom:0px;padding-top:0">';
    $html_model .= '<h3 style="margin-top:0">Notes</h3>';
    $html_model .= '<div class="replace_note_div_'.$frame['PFID'].'" >';
    $html_model .= '</div>';
    $html_model .= '</div>';
    $html_model .= '</div>';

    echo $html_model;
