<!--
.io_numReps
#id_HB_Rank
#id_HB_Actuals
#id_HB_Img
-->

<div class="chart_main" id="repHomeBrandsMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Home Brands</div>
  </div>

  <!-- body -->
  <div class="chart_body" id="repHomeBrands_container">
      <!-- LEFT -->
      <div class="chart_metric">
        <div class="chart_primary">
          <span class="chart_primary_title">Rank:</span>
          <span class="chart_primary_value" id="id_HB_Rank">#</span>/<span class="io_numReps">#</span>
        </div>
        <div class="chart_scnd">
          <span class="chart_scnd_title"></span>
          <span class="chart_scnd_value"><strong id="id_HB_Actuals">#% - $#</strong></span>
        </div>
      </div>

      <!-- RIGHT -->
      <div class="chart_hero">
        <img id="id_HB_Img" src="<?php echo base_url();?>assets/images/dashboard_gifs/AAD_head_scratch.gif" />
      </div>
  </div>
</div>
