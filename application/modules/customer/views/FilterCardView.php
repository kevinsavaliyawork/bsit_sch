<div class="bsit_filter_card disp_filter_pt disp_filter  custom_filter" style="display: block;"
  data-filter_order="" data-filter="default_filter" data-filter_type="with_history">

  <div class="cancel_filter_custom">
    <svg viewBox="0 0 24 24" class="bsit-filter-svg">
      <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path>
    </svg>
  </div>




  <div class="s_value">
    <?php echo($filterText); ?>
  </div>

  <div class="s_name">
    <?php echo($filterField); ?>
  </div>

  <div class="s_operator">
    <?php echo($filterOperator); ?>
  </div>


<!--     <input type="hidden" class="filter_vals_<?php echo $pageFrameId?>"  data-hist_filter_val_<?php echo $pageFrameId?>="<?php echo $history_api_back_val;?>"   value=<?php echo('"'.$filterString.'"')?> >
 -->

</div>