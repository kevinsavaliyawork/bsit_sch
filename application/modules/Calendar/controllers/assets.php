    <?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    
    class assets extends brain_controller {

        
       
        function __construct() {
            parent::__construct(1);


           
            //---get working directory and map it to your module
            $file = getcwd() . '/application/modules/' . implode('/', $this->uri->segments);
            
            $file = urldecode($file);
          //  print "<pre>"; print_r($file); exit;
            //----get path parts form extension
            $path_parts = pathinfo( $file);
            //---set the type for the headers
            $file_type=  strtolower($path_parts['extension']);
          
          //  echo $file_type;exit; 
            
            if (is_file($file)) {

                //----write propper headers
                switch ($file_type) {
                    case 'css':
                        header('Content-type: text/css');
                        break;

                    case 'js':
                        header('Content-type: text/javascript');
                        break;
                    
                    case 'json':
                        header('Content-type: application/json');
                        break;
                    
                    case 'xml':
                       header('Content-type: text/xml');
                        break;
                    
                    case 'pdf':
                      header('Content-type: application/pdf');
                        break;
                    
                   case 'jpg' || 'jpeg' || 'png' || 'gif':
                    header('Content-type: image/'.$file_type);
                    readfile($file);
                    exit;
                    break;
                }
     
                include $file;
            } else {
                //show_404();
            }
            exit;
        } 
        function css() {
            
        }

        function images() {
            
        }

        function js() {
            
        }

        function fullcalendar() {
            
        }

    }