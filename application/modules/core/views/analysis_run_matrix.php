<?php
if(!empty($analysis_data)){
?>
	<div class="mui-row">
	  <div class="mui-col-md-4">
			Columns: <select id="rowCntSelector">
				<?php
				// create options for number of columns
				for($r=10; $r<=50; $r=$r+10){
					// if the row is the same as selected, highlight it.
					if($r == $rows){
						echo('<option selected=selected value="'.$r.'">'.$r.' Columns</option>');
					}else{
						echo('<option value="'.$r.'">'.$r.' Columns</option>');
					}
				}
				?>
			</select>
	  </div>
	</div>
	<div id="analysisTableCont">
    	<table class="mui-table">
			<thead>
				<tr>
					<?php
					// if the measures count is 1, only one measure is in use
					// so show variances
					$show_variance = false;
					$matrix_col_cnt = count($analysis_dim2);
					if($analysis_meas_cnt==1){
						$show_variance = true;
					}
					$show_variance=false;
					// here we output the matrix header, which includes first two rows.
					// loop dim 2, the matrix dimension running horizontally.
					echo('<th nowrap style="width:180px;"></th>');
					$col_headers_output = 1;
					foreach($analysis_dim2 as $matrixDim){
						// output the dim 1 empty cell
						$matrixDim_name 	= $matrixDim['dimName'];
						$matrixDim_value 	= $matrixDim['dimVal'];
						$matrixDim_id 		= $matrixDim['dimId'];

						// get dimensions and child dimension details
						$grpByDimID		=	$matrixDim_id;
						$grpByChildId	=	$dimensions[$grpByDimID]['child_id'];
						$grpByChildName	=	$dimensions[$grpByDimID]['child_name'];	
						$valLookup		=	$dimensions[$grpByDimID]['lookup_value'];

						// setup the "a" tag for the dimension
						$colLink		= '<a href="#" class="drillInSel" ';
						$colLink		.='data-value="'.$matrixDim_value.'"'; 
						$colLink		.='data-dimid="'.$matrixDim_id.'" '; 
						$colLink		.='data-dimname="'.$matrixDim_name.'" '; 
						$colLink		.='data-grpcolnum="2" '; 
						$colLink		.='data-grpchildid="'.$grpByChildId.'" ';
						$colLink		.='data-grpchildname="'.$grpByChildName.'" '; 
						$colLink		.='data-valLookup="'.$valLookup.'" '; 
						$colLink		.='>'; 
						$colLinkEnd		= '</a>';

						// setup number of columns for header to span
						if($show_variance==true && $col_headers_output<$matrix_col_cnt){
							$colespan_cnt = $analysis_meas_cnt + 1;
						}else{
							$colespan_cnt = $analysis_meas_cnt;
						}

						echo("<th nowrap colspan='".$colespan_cnt."' class='matrixTableLeftBorder'>");
							echo($colLink);
							echo('<div style="height:100%; width:100%;">');
							echo($matrixDim_value);
							echo('</div>');
							echo($colLinkEnd);
						echo("</th>");	

						$col_headers_output++;
					}
					?>
				</tr>
				<tr>
					<?php
					// here we loop the first row only, 
					// we output headers -- build array of keys for column names
					if(!empty($analysis_data['BSIT']['BSIT'])){
						$kcnt = 0;

						// get a list of measures
						$dim_1_name = $analysis_data['BSIT']['BSIT'][0];
						$dim_2_name = $analysis_data['BSIT']['BSIT'][1];
						$measure_list = $analysis_data['BSIT']['BSIT'][2];

						// output the name of the first dimension, running vertically
						echo('<th nowrap class="mui--text-left">'.$dim_1_name.'</th>');

						// loop each of the horizontal matrix dimensions
						$col_headers_output = 1;
						foreach($analysis_dim2 as $matrixDim){
							// loop each of the measures
							$mea_cnt = 0;
							foreach($measure_list as $row){
								if($mea_cnt == 0){
									// setup the CSS for right align and left border
									$columnClasses = 'class="mui--text-right matrixTableLeftBorder"';
								}else{
									$columnClasses = 'class="mui--text-right"';
								}
								// output the row header columns start
								echo('<th nowrap '.$columnClasses.'>'.$row.'</th>');
								$mea_cnt++;
							}
							// show variance header for previous matrix column
							if($show_variance==true && $col_headers_output<$matrix_col_cnt){
								echo('<th nowrap>VAR %</th>');
							}
							$col_headers_output++;
						}
					}

					?>
				</tr>
			</thead>
		<tbody>
			<?php 

			// Loop all data rows
			if(!empty($analysis_data)){
				// setup rowcount variable
				$rowCounter = 1;

				// loop each vertical dimension
				foreach($analysis_dim1 as $dim1){
					$dim1_name 	= $dim1['dimName'];
					$dim1_value = $dim1['dimVal'];
					$dim1_id 	= $dim1['dimId'];
					echo('<tr>');
					$columnClasses 	= 'class="mui--text-left"';

					// get dimensions and child dimension details
					$grpByDimID		=	$selectedGroups[0];
					$grpByChildId	=	$dimensions[$grpByDimID]['child_id'];
					$grpByChildName	=	$dimensions[$grpByDimID]['child_name'];	
					$valLookup		=	$dimensions[$grpByDimID]['lookup_value'];

					// setup the "a" tag for the dimension
					$colLink		= '<a href="#" class="drillInSel" ';
					$colLink		.='data-value="'.$dim1_value.'"'; 
					$colLink		.='data-dimid="'.$dim1_id.'" '; 
					$colLink		.='data-dimname="'.$dim1_name.'" '; 
					$colLink		.='data-grpcolnum="1" '; 
					$colLink		.='data-grpchildid="'.$grpByChildId.'" ';
					$colLink		.='data-grpchildname="'.$grpByChildName.'" '; 
					$colLink		.='data-valLookup="'.$valLookup.'" '; 
					$colLink		.='>'; 
					$colLinkEnd		= '</a>';

					// setup this line with link
					$rowValue = $colLink.'<div style="height:100%; width:100%;">'.$dim1_value.'</div>'.$colLinkEnd;

					?>
					<td <?php echo($columnClasses);?> >
						<?php echo($rowValue); ?>
					</td>
					<?php

					// loop each horizontal dimension
					$dim2Cnt = 0;
					$var_prev_val = 0;
					foreach($analysis_dim2 as $dim2){
						$dim2_name 	= $dim2['dimName'];
						$dim2_value = $dim2['dimVal'];
						$dim2_id 	= $dim2['dimId'];
						// loop each measure, output the line
						$mea_cnt = 0;
						foreach($measure_list as $measure){
							if($mea_cnt == 0){
								// setup the CSS for right align and left border
								$columnClasses = 'class="mui--text-right matrixTableLeftBorder"';
							}else{
								// setup the CSS for right align
								$columnClasses = 'class="mui--text-right"';
							}
							// setup this line with link
							if(!empty($analysis_data[$dim1_value][$dim2_value][$measure])){
								$rowValue = $analysis_data[$dim1_value][$dim2_value][$measure];
							}else{
								$rowValue = '-';
							}

							// show the variance column
							if($show_variance == true){
								if($rowValue != '-'){
									$var_curr_val = floatval(str_replace(array('$',','),'',$rowValue));
								}else{
									$var_curr_val = 0;
								}

								// if we are on the second column or more
								if($dim2Cnt > 0){
									// check if the value has increased or decreased since last itteration
									// --------------------------------------------------------------
									// VARIANCE IS FOR PREVIOUS COLUMN - SO OUTPUT BEFORE CURRENT
									// --------------------------------------------------------------
									if($var_curr_val > 0){
										$var_diff		= $var_prev_val-$var_curr_val;
										$var_percent 	= (($var_diff/$var_curr_val)*100);
										$var_percent 	= number_format($var_percent, 1, '.', '');
									}else{
										$var_percent 	= 	100;
									}
									if($var_prev_val > $var_curr_val){
										// gone up
										echo('<td class="bsit_var_up">'.$var_percent.'%</td>');
									}elseif($var_prev_val < $var_curr_val){
										// gone down
										echo('<td class="bsit_var_down">'.$var_percent.'%</td>');
									}else{
										// no change
										echo('<td class="bsit_var_same">'.$var_percent.'%</td>');
									}
								}
								$var_prev_val = $var_curr_val;
							}

							?>
							<td <?php echo($columnClasses);?> >
								<?php echo($rowValue); ?>
							</td>
							<?php
							$mea_cnt++;
						}
						$dim2Cnt++;
					}
					echo('</tr>');
					$rowCounter++;

					// exit if loop has run for too long
					if($rowCounter>1000){
						?>
						<tr><td colspan="51"> Results have been trimmed at 1000!</td></tr>
						<?php
						break;
					}
				}
			}
			?>
		</tbody>
	</table>
	<?php
	// should we show the download link?
	if($can_user_download==1){
	?>

		<div><a id="downloadCSV" href="#"><img style="opacity:.4" src="assets/core/images/ic_get_app_black_24dp_1x.png"/></a></div>
	<?php
	}
}else{
	?>
	<div class="main_menu_item">
		<div class="menu_item_icon">
			<img src="assets/core/images/methods.png" class="image">
		</div>
		<div class="menu_item_text">
			<p>No results have been found, <span style="font-weight:bold; font-size:16px">sorry!</span> </p>
			<p>Try using a different filter selection to expand your search.</p>
			<!--<p>Remember, filters of the same dimension work like an 'OR' statement, however, different dimensions are combined with an 'AND' clause.<br/> If two filters are entered, 1 for day and one for year, they will be combined with an 'AND'</p>-->
		</div>
	</div>
	<?php
}
?>