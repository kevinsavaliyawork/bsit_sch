<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OpportunityView extends brain_controller {

	function __construct() {
    parent::__construct(1);
    // Load the needed librarys
    $this->load->model('core/Bsit_io','API');
    $this->load->library('session');
    $this->load->model('opportunities/Opportunitymodel');
    
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	}


	
		
 

	/**
    * Get activity history
    * It will use when user submit any note then we will fetch 
    * all activity using ajax and update activity notes
    * Added by Niru [21-6-2021]
    */
    public function fetchLatestHistoryList()
    {
        $this->load->model('opportunities/Opportunitymodel');
        $get_activity_history_detail = $this->Opportunitymodel->getActivityHistoryDetail($_POST["job_id"]);
        $data_pass['activity_history'] = json_decode($get_activity_history_detail);
        echo $this->load->view('opportunities/jobs_notes_list', $data_pass, true);
        exit;
    }

    /**
    * Get opprtinity over data
    * It will use when user edit then we will fetch 
    * fetch all Opportunity Customer All Data
    * Added by Niru [21-6-2021]
    */

    public function fetchOpportunityCustomerAllData()
    {
		$get_customer_detail = $this->Opportunitymodel->getCustomerDetail($_POST["job_id"]);
		$data_pass['customer'] = json_decode($get_customer_detail)->Data;
        $data_pass['job_id'] = $_POST["job_id"];
        echo $this->load->view('opportunities/Overview', $data_pass, true);
        exit;
    }
	
}