<!DOCTYPE html>
<html>

<head>
  <title><?php echo $appTitle; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">

  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,100,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <!-- addede by kevin 26-03-2020 -->
  <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/mui-0.2.10/css/mui.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>core/assets/css/style.css">

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/mui-0.2.10/js/mui.min.js"></script>

  <script src="<?php echo base_url(); ?>core/assets/js/aes.js"></script>
  <script src="<?php echo base_url(); ?>core/assets/js/jquery-password-validator.js"></script>

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/core/images/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url(); ?>assets/core/images/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/core/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/core/images/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/core/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url(); ?>assets/core/images/favicon/manifest.json">

  <style type="text/css">
    <?php echo $cssOverride; ?>
  </style>

  <script type="text/javascript">
    function validateForm(formId) {
      var tzname = Intl.DateTimeFormat().resolvedOptions().timeZone;
      $("#usertimezone").val(tzname);

      var form = $("#" + formId);
      var fail = false;
      var fail_log = '';
      form.find('select, textarea, input').each(function() {

        if (!$(this).prop('required')) {} else {

          if (!$(this).val()) {
            fail = true;
            name = $(this).attr('name');

            fail_log += name + " is required \n";
            $("[name=" + name + "]").addClass('required_border_class');
            $("[name=" + name + "]").next('label').addClass('required_label_class');
          }


        }
      });

      //check pasword validation error
      if (parseInt(form.data('has-error')) == 1) {
        fail = true;
      }

      if (fail) {
        return false;
      } else {
        if ($('#password').val() != $('#confirmpassword').val()) {
          $("#password,#confirmpassword").addClass('required_border_class');
          $("#password,#confirmpassword").next('label').addClass('required_label_class');
          $('#error-container').html('Password and Confirm Password do not match');
          return false;
        }
        var password = document.getElementById('password').value;
        var key = CryptoJS.enc.Hex.parse("0123456789abcdef0123456789abcdef");
        var iv = CryptoJS.enc.Hex.parse("abcdef9876543210abcdef9876543210");
        var hash = CryptoJS.AES.encrypt(password, key, {
          iv: iv
        });
        $("#password,#confirmpassword").val(hash);
        $("#" + formId).submit(); // Submit the form
      }

    }
    var options = {
      minLength: 6, //Minimum Length of password
      minUpperCase: 0, //Minimum number of Upper Case Letters characters in password
      minLowerCase: 0, //Minimum number of Lower Case Letters characters in password
      minDigits: 1, //Minimum number of digits characters in password
      minSpecial: 0, //Minimum number of special characters in password
      maxRepeats: 5, //Maximum number of repeated alphanumeric characters in password dhgurAAAfjewd <- 3 A's
      maxConsecutive: 5, //Maximum number of alphanumeric characters from one set back to back
      noUpper: false, //Disallow Upper Case Lettera
      noLower: false, //Disallow Lower Case Letters
      noDigit: false, //Disallow Digits
      noSpecial: false, //Disallow Special Characters
      failRepeats: false, //Disallow user to have x number of repeated alphanumeric characters ex.. ..A..a..A.. <- fails if maxRepeats <= 3 CASE INSENSITIVE
      failConsecutive: false, //Disallow user to have x number of consecutive alphanumeric characters from any set ex.. abc <- fails if maxConsecutive <= 3
      confirmField: '#confirmpassword'
    };
    $(document).ready(function() {
      //Password validation
      $("#password").passwordValidation(options, function(element, valid, match, failedCases) {
        $('#error-container').html(failedCases.join("\n"));
        if (!match && valid) {
          $('#error-container').html('Password and Confirm Password do not match');
        }
        if (valid && match) {
          $('#error-container').html('');
          $("#form-reset").data('has-error', 0);
        } else {
          $("#form-reset").data('has-error', 1);
        }

      });
      $(".inputForm").keydown(function(e) {
        if (e.which == 13) {
          var formId = $(this).parents('form').attr('id');
          validateForm(formId);
        }
      });

      if (navigator.geolocation) {

        window.onload = function() {
          var startPos;
          var geoOptions = {
            enableHighAccuracy: true,
            timeout: 10 * 1000,
            maximumAge: 5 * 60 * 1000,
          }

          var geoSuccess = function(position) {
            startPos = position;
            $("input[name='latitude']").val(startPos.coords.latitude);
            $("input[name='longitude']").val(startPos.coords.longitude);
          };
          var geoError = function(error) {
            console.log('Error occurred. Error code: ' + error.code);
            // error.code can be:
            //   0: unknown error
            //   1: permission denied
            //   2: position unavailable (error response from location provider)
            //   3: timed out
          };

          navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
        };
      } else {
        console.log("Geolocation is not supported by this browser!");
      }
    });
  </script>
</head>

<body>
  <div class="bsit_login">
    <div class="basit_logon_image">
      <!-- <img src="<?php echo base_url(); ?>assets/core/images/HeaderAboutUs.jpg"/> -->
    </div>
    <div id="loginPageBG" class="bsit_login_form">
      <div class="mui-panel" id="loginForm">
        <a href="#" target="_blank">
          <img id="loginLogoImg" src="<?php echo base_url(); ?>assets/core/images/login_logo.png" />
        </a>

        <br />

        <?php if (!empty($message)) : ?>
          <?php if (!$error) : ?>
            <div class="alert-success">
              <?php echo $message ?>
            </div>
          <?php else : ?>
            <div class="alert-danger">
              <?php echo $message ?>
            </div>
          <?php endif; ?>

        <?php endif ?>
        <?php
        $showTryAgainBtn = (isset($showTryAgainBtn) && $showTryAgainBtn == 1) ? 1 : 0;
        if ($showTryAgainBtn == 1) {
        ?>
          <div class="bsit_login_btn">
            <a href="<?php echo base_url() . 'reset?key=' . urlencode($resetKey); ?>" title="Please try again.">Please try again.</a>
          </div>
        <?php } ?>

        <?php if (!$error) : ?>
          <form id="form-reset" role="form" autocomplete="off" name="form-signin" method="POST" data-has-error="0" action="<?php echo base_url() . 'reset?key=' . urlencode($resetKey); ?>">
            <input type="hidden" name="latitude" id="latitude" value="">
            <input type="hidden" name="longitude" id="longitude" value="">
            <div class="bsit_login_titel">
              <h2>Reset Password</h2>
              <p>Hello <?php echo $user->FirstName ?> <?php echo $user->LastName ?></p>
            </div>
            <legend id="loginFormTitle"></legend>
            <input type="text" id="email" name="email" value="" autocomplete="new-password" class="customFieldHoneyPot">
            <input type="hidden" name="timezone" id="usertimezone" value="">
            <div class="mui-textfield bsit_form">
              <input type="password" name="password" autocomplete="new-password" id="password" class="inputForm" required />
              <label>Password</label>
            </div>
            <div class="mui-textfield bsit_form">
              <input type="password" name="confirmpassword" autocomplete="new-password" id="confirmpassword" class="inputForm" required />
              <label>Confirm Password</label>
            </div>

            <div class="mui-danger-color bsit_ionvalid_sms" id="error-container">
              <?php $err = validation_errors();
              if (!empty($err)) :
                echo validation_errors();
              endif;
              ?>
            </div>

            <div class="mui-row">
              <div class="mui-col-lg-12 mui-col-md-12 mui-col-sm-12 mui-col-xs-12 bsit_login_btn">
                <button class="mui-btn mui-btn--primary mui-float-right mui--text-right" type="button" onclick="validateForm('form-reset');">RESET PASSWORD</button>
              </div>
            </div>
            <input type="hidden" name="redirect" value="<?php echo $this->input->get('redirect'); ?>">
          </form>
        <?php endif; ?>

      </div>
    </div>
  </div>
</body>

</html>