import './TableViewEvents.js';

// Access globals (header.php).
// TODO: Find better solution
const pageID = parseInt(page_id);


function showHideQuickFilter() {
  // debugger

  $(".bsit_allow_quick_filter").off('click');
  $(".bsit_allow_quick_filter").click(function () {
    // Hide other dropdown menus in case of multiple dropdowns
    $(".bsit_allow_quick_filter_ul ").not($(this).next()).slideUp("fast");
    // Toggle the current dropdown table listing
    $(this).next(".bsit_allow_quick_filter_ul ").slideToggle("fast");
  });
  // Hide dropdown table listing on click outside
  $(document).on("click", function (event) {
    // debugger     
    if (event.target.name == "chkbox[]") {
      return;
    }
    if ($(event.target.name).closest(".bsit_allow_quick_filter_ul").length) {
      $(".bsit_allow_quick_filter_ul ").slideToggle("fast");

      if (!$(event.target).closest(".bsit_allow_quick_filter_show").length) {
        $(".bsit_allow_quick_filter_ul ").slideUp("fast");
      }
    }
  });
}

function getQuickFilterValues(framepropertyid) {
  $.ajax({
    type: 'POST',
    url: base_url + 'core/frame/getQuickFilterValues',
    data: {
      framepropertyid: framepropertyid
    },
    context: framepropertyid,
    dataType: 'json',
    success: function (res) {

      var filterresults = res;
      var quickFilterFramepropertyId = this;

      $.each(filterresults, function (index, value) {
        $(`#quick_filter_ul_` + quickFilterFramepropertyId).append('<li class="bsit_filter_vailues" value="' + value[0] + '">' + value[1] + '</li>');
      });

    },
    error: (err) => {

    },
  });
}

function performFilter(frame, filterTextRaw, filterField, filterOperator, filterFieldText) {
  // This is the filterField display text, to be used on filter cards
  // alert(filterOperator);
  //const selectedField = $('.bsit-filter-name').get(0).selectedOptions[0];

  let filterString = '';
  let filterText = '';

  // Check filter text was supplied
  if (filterTextRaw != '') {
    // Pad filter text with %% on 'LIKE' filters.
    if (filterOperator == 'like') {
      filterText = `'%${filterTextRaw}%'`;
    } else {
      filterText = `'${filterTextRaw}'`;
    }
  } else {
    // No filter text supplied, so no filter for you
    return;
  }

  // Check all required filter fields have been set
  if (filterField != '' && filterOperator != '') {
    // Change filter string if user specified NULL.
    if (filterText == "'NULL'") {
      if (filterOperator == '<>') {
        filterString =
          `${filterField} ${filterOperator} ''` +
          ` OR NULLIF(${filterField}, '') IS NOT NULL`;
      } else {
        filterString =
          `${filterField} ${filterOperator} ` + `'' OR ${filterField} IS NULL`;
      }
    } else {
      filterString = `${filterField} ${filterOperator} ${filterText}`;
    }
  } else {
    // Not all fields provided, cannot create filter
    return;
  }


  // If we already have this filter, don't bother adding it
  if (frame.filterList.some((elem) => elem.fullFilterString == filterString)) {
    return;
  }

  let filterData = {
    filterField: filterField,
    filterOperator: filterOperator,
    filterText: filterText,
    filterFieldCard: filterFieldText, //$(selectedField).html(),
    filterTextCard: filterTextRaw,
    fullFilterString: filterString,
    filterType: "default_filter"
  };

  // Prepend the new filter to our array.
  frame.filterList.unshift(filterData);

  // Reset filter string and frame page ready for new data
  frame.filterString = '';
  frame.currPage = 1;

  // Create and display filter cards
  loadFilterCard(frame, filterData);

  // build the filter string
  applyFilterStrings(frame);

  // Get new data and refresh the frame
  updateFramePagination(frame);

  $("#filter_name_" + frame.PFID).val('');
  $("#filter_text_" + frame.PFID).val('');


  // save filter data in the history table
  saveHistoryData(frame);
}


// Applying / Appending filters to a frame
function applyFilter(frame) {
  const filterTextRaw = $(`#filter_text_${frame.PFID}`).val();
  const filterField = $(`#filter_name_${frame.PFID}`).val();
  const filterOperator = $(`#filter_oprator_${frame.PFID}`).val();
  const filterFieldText = $(`#filter_name_${frame.PFID}`).find(":selected").text();

  performFilter(frame, filterTextRaw, filterField, filterOperator, filterFieldText);
}


function saveHistoryData(frame) {

  var history_pass_data = '';
  var history_page_id = '';
  var history_flag = 'no';
  var history_state = '';
  var history_frame_id = '';
  $(".main_content_part_class").each(function () {
    //var multi_frame_id  = $(this).attr('data-frame');
    var multi_frame_id = frame.PFID;
    var form1 = $("#addForm" + multi_frame_id);
    var coll_id = form1.attr("data-collid");
    var history_pass_data = $('#history_api_data_' + multi_frame_id).val();
    //alert('GETTING HISTORY DATA:'+history_pass_data);        

    if (typeof coll_id != 'undefined' && history_pass_data != '') {
      history_pass_data = jQuery.parseJSON(history_pass_data);

      if (window.history.state == null || typeof window.history.state.state == 'undefined') {
        //alert(1); 
        if (history_page_id != history_pass_data['page_id']) {
          history_page_id = history_pass_data['page_id'];
        }

        var history_full_url = document.location.href;
        var history_split_url = history_full_url.split('?');
        history_split_url = history_split_url[1];

        $('#filter_history_api_state_' + multi_frame_id).val(1);
        history_state = $('#filter_history_api_state_' + multi_frame_id).val();
        history_pass_data['state'] = history_state;

        if (typeof history_split_url != 'undefined') {
          history_full_url = history_full_url.substring(0, history_full_url.indexOf('state='));
          var history_url = '?' + history_split_url + '&state=' + history_state + "&";
        } else {
          var history_url = "?state=" + history_state;
        }
        var sObj = '';
        var sObj = history_pass_data;
        var title = "title";


        history.pushState(sObj, title, history_url);
        var insert_history_api_tbl = HistoryApiInsertDataUsedForBackButton(coll_id, history_pass_data['filter'], history_pass_data['order'], history_pass_data['page_num'], history_pass_data['num_recs'], history_pass_data['frame_id'], history_full_url, sObj, history_state, userID);
      } else {


        var previous_frame_history = window.history.state;
        history_state = window.history.state.state;
        history_frame_id = window.history.state.frame_id;
        //alert(history_page_id+'*****'+history_pass_data['page_id']);

        if (history_page_id != history_pass_data['page_id']) {
          history_page_id = history_pass_data['page_id'];
          history_state++;
          history_flag = 'yes';

        } else {
          history_flag = 'no';
        }

        $('#filter_history_api_state_' + multi_frame_id).val(history_state);

        if (history_state != '') {
          var history_full_url = document.location.href;
          var history_split_url = history_full_url.split('?');
          history_split_url = history_split_url[1];

          history_pass_data['state'] = history_state;
          var sObj = history_pass_data;
          var title = "title";
          //var history_url = "?state=" + history_state;

          if (typeof history_split_url != 'undefined') {
            history_full_url = history_full_url.substring(0, history_full_url.indexOf('state='));
            history_split_url = history_split_url.substring(0, history_split_url.indexOf('state='));
            var history_url = '?' + history_split_url + 'state=' + history_state + "&";
            history_full_url = history_full_url + "state=" + history_state + "&";
          } else {
            var history_url = "?state=" + history_state;
          }

          if (history_flag == 'yes') {

            history.pushState(sObj, title, history_url);
          } else {
            if (history_frame_id != multi_frame_id) {
              previous_frame_history['frame_id_' + history_pass_data['frame_id']] = multi_frame_id;
              previous_frame_history['history_full_state_' + history_pass_data['frame_id']] = sObj;
              previous_frame_history[multi_frame_id] = sObj;
              previous_frame_history['history_api_back_val_' + history_pass_data['frame_id']] = history_pass_data['history_api_back_val'];


              history.replaceState(previous_frame_history, title, history_url);
            }
          }


          var insert_history_api_tbl = HistoryApiInsertDataUsedForBackButton(coll_id, history_pass_data['filter'], history_pass_data['order'], history_pass_data['page_num'], history_pass_data['num_recs'], history_pass_data['frame_id'], history_full_url, sObj, history_state, userID);
        }
      }
    }

    //}
  });
}


async function HistoryApiInsertDataUsedForBackButton(coll_id, filter, orders, page_num, num_rec, frame_id, history_url, history_full_state, history_state, create_by) {
  history_url = document.location.href;
  filter = encodeURI(encodeURI(filter));

  history_full_state = JSON.stringify(history_full_state);
  history_full_state = encodeURI(encodeURI(history_full_state));
  var navigation_collection_id = await getCollectionIDByName('navigation_history');

  var d = new Date();
  var currDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
  //currDate = "2017-11-25";
  var values = {
    coll_id: coll_id,
    filter: filter,
    orders: orders,
    page_num: page_num,
    num_rec: num_rec,
    frame_id: frame_id,
    history_url: history_url,
    history_full_state: history_full_state,
    history_state: history_state,
    user_id: create_by,
    key_create_by: create_by,
    key_update_by: create_by,
    apply_date: currDate
  };
  // console.log(values);
  var json_values = JSON.stringify(values);

  var full_json = $.fn.bsitJsonObject(navigation_collection_id, json_values);

  $.ajax({
    type: "POST",
    url: base_url + "core/Apilocal/create",
    //timeout: 70000,
    data: {
      data: full_json
    },
    dataType: 'json',
    success: function (returnData) { //alert(JSON.stringify(returnData));

      //session_expire(returnData);
      console.log('Success!');

    },
    error: function (xhr, ajaxOptions, thrownError) {

    }
  });
}


async function advCollectionSource(request, response, event) {


  let targetElement = event[0].element;
  let parentForm = $(targetElement).parents('form:first');
  let collectionID = $(targetElement).attr('data-coll');
  let collectionAlias = $(targetElement).attr('data-alias');

  let advCollectionJSON = jQuery.parseJSON(
    $(targetElement)
      .attr('data-columns')
      .replace(/'/g, '"')
  );

  let ValueProperty = advCollectionJSON.ValueProperty;

  let DisplayProperties = advCollectionJSON.DisplayProperties;

  // 'OR' filters
  let FilterDBCols = advCollectionJSON.FilterDBCols;

  // Permanent Filters to query the DB. 'AND' filters.
  let PermDBFilters = advCollectionJSON.PermDBFilters;

  let Separator = (advCollectionJSON.Separator) ? advCollectionJSON.Separator : "";

  // Permanent Filters sorte DB.
  let SortOrder = advCollectionJSON.SortOrder;

  // get the details of the field

  let searchString = request['term'];

  let filterString = '';

  if (typeof FilterDBCols != 'undefined') {
    filterString = '(';

    // loop each filter column and build a string
    FilterDBCols.forEach((column, index) => {
      // append the OR clause for loop 2,3,4...
      if (index > 0) {
        filterString = `${filterString} OR `;
      }

      // append the filter for each column
      let filterColumn = column.replace(/[\[\]']+/g, '');

      filterString = `${filterString} ${collectionAlias}.${filterColumn} LIKE '%${searchString}%'`;
    });

    // Close the 'OR' clauses
    filterString = filterString + ')';
  }

  let orderString = '';

  if (typeof SortOrder != 'undefined') {
    orderString = SortOrder;
  }

  let permFilterString = '';

  // loop each perm filter column and build a string (AND CLAUSE)
  if (typeof PermDBFilters != 'undefined') {
    PermDBFilters.forEach((filter, index) => {

      let permFilterValue = '';

      // Hardcoded val to search
      if (PermDBFilters[index].type == 'STR') {
        permFilterValue = filter.value;
      }

      // Find variable on form to filter by
      if (PermDBFilters[index].type == 'VAR') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .val();
      }

      //find dependancy on advance selectpicker | ADVC - advanced collection dependancy
      if (PermDBFilters[index].type == 'ADVC') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .attr('data-selectedid');
      }
      if (permFilterValue) {

        if (index == 0 && filterString != '') {
          // If theres no filter, dont start the perm filter with AND
          permFilterString = ' AND ';
        }

        // append the OR clause for loop 2,3,4...
        if (index > 0) {
          permFilterString = `${permFilterString} AND `;
        }

        permFilterString += `${collectionAlias}.${filter.col} ` + `${filter.op}` + `'${permFilterValue}'`;
      }
    });
  }

  // loop each display property and build a string
  let propList = '';
  DisplayProperties.forEach((prop, index) => {
    // append the OR clause for loop 2,3,4...
    if (index > 0) {
      propList += ', ';
    }

    // append the filter for each column
    propList += `"${DisplayProperties[index]}"`;
  });

  filterString += permFilterString;

  let requestData = {
    data: JSON.stringify([
      {
        collID: collectionID,
        cols: [propList],
        filter: filterString,
        order: orderString,
      },
    ]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/dropdownlist',
    data: requestData,
    dataType: 'json',
    beforeSend: () => {
      $('.loader').hide();
      $(parentForm).closest(".modal").find(".bsit-commit-edit").prop("disabled", true);
      $(parentForm).closest(".modal").find(".bsit_save_btn").prop("disabled", true);

    },
    complete: function () {
      $(parentForm).closest(".modal").find(".bsit-commit-edit").prop("disabled", false);
      $(parentForm).closest(".modal").find(".bsit_save_btn").prop("disabled", false);
    },
    success: function (res) {
      let responseData = res[0]['Results'];
      let returnArray = [];

      responseData.forEach((result) => {
        // Initialize the display object to be returned
        let display = {
          label: '',
          value: result[ValueProperty.toLowerCase() == 'id' ? '_id' : ValueProperty],
        };

        DisplayProperties.forEach((prop, index) => {

          display.label +=
            prop.toLowerCase() == 'id' ?
              `${result['_id']}`
              //: `${result[prop.toLowerCase()]}`;
              :
              `${result[prop]}`;

          // Don't append '-' for last display property
          if (index < DisplayProperties.length - 1) {
            if (Separator != "") {
              display.label += Separator;
            } else {
              display.label += ' - ';
            }
          }
        });

        returnArray.push(display);
      });

      if (returnArray.length == 0 || $(targetElement).val() == "") {
        //$(targetElement).val();
        $(targetElement).attr("data-selectedid", "");
      }

      // Return our list of label/value objects
      response(returnArray);
    },
    error: (err) => {
      response(err);
    },
  });
}

function advCollectionFocus(event) {
  let dropListDetails = $(event.target).attr('data-columns');

  // Escape single quotes! Otherwise Mr. Json isn't happy.
  dropListDetails = dropListDetails.replace(/'/g, '"');
  let dropListDetailsAdvanced = JSON.parse(dropListDetails);

  let RunOnFocus = dropListDetailsAdvanced.RunOnFocus;
  let func = dropListDetailsAdvanced.Functions.Focus;

  if (RunOnFocus) {
    $(event.target).autocomplete('enable');
    $(event.target).autocomplete('search', '');
  }

  if (typeof window[func] == 'function') {
    // safe to use the function
    window[func]();
  }
}


async function advMultiCollectionSource(request, response, event) {
  // debugger

  let targetElement = event[0].element;
  let parentForm = $(targetElement).parents('form:first');
  let collectionID = $(targetElement).attr('data-coll');
  let collectionAlias = $(targetElement).attr('data-alias');

  let advCollectionJSON = jQuery.parseJSON(
    $(targetElement)
      .attr('data-columns')
      .replace(/'/g, '"')
  );

  let ValueProperty = advCollectionJSON.ValueProperty;

  let DisplayProperties = advCollectionJSON.DisplayProperties;

  // 'OR' filters
  let FilterDBCols = advCollectionJSON.FilterDBCols;

  // Permanent Filters to query the DB. 'AND' filters.
  let PermDBFilters = advCollectionJSON.PermDBFilters;

  let Separator = (advCollectionJSON.Separator) ? advCollectionJSON.Separator : "";

  // get the details of the field

  let searchString = request['term'];

  if (searchString != '') {
    var res = searchString.split(",")[searchString.split(",").length - 1];
    // alert(res);
    searchString = res;

  }
  // alert(searchString);

  let filterString = '';

  if (typeof FilterDBCols != 'undefined') {
    filterString = '(';

    // loop each filter column and build a string
    FilterDBCols.forEach((column, index) => {
      // append the OR clause for loop 2,3,4...
      if (index > 0) {
        filterString = `${filterString} OR `;
      }

      // append the filter for each column
      let filterColumn = column.replace(/[\[\]']+/g, '');

      filterString = `${filterString} ${collectionAlias}.${filterColumn} LIKE '%${searchString}%'`;
    });

    // Close the 'OR' clauses
    filterString = filterString + ')';
  }

  let permFilterString = '';

  // loop each perm filter column and build a string (AND CLAUSE)
  if (typeof PermDBFilters != 'undefined') {
    PermDBFilters.forEach((filter, index) => {

      let permFilterValue = '';

      // Hardcoded val to search
      if (PermDBFilters[index].type == 'STR') {
        permFilterValue = filter.value;
      }

      // Find variable on form to filter by
      if (PermDBFilters[index].type == 'VAR') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .val();
      }

      //find dependancy on advance selectpicker | ADVC - advanced collection dependancy
      if (PermDBFilters[index].type == 'ADVC') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .attr('data-selectedid');
      }
      if (permFilterValue) {

        if (index == 0 && filterString != '') {
          // If theres no filter, dont start the perm filter with AND
          permFilterString = ' AND ';
        }

        // append the OR clause for loop 2,3,4...
        if (index > 0) {
          permFilterString = `${permFilterString} AND `;
        }

        permFilterString += `${collectionAlias}.${filter.col} ` + `${filter.op}` + `'${permFilterValue}'`;
      }
    });
  }

  // loop each display property and build a string
  let propList = '';
  DisplayProperties.forEach((prop, index) => {
    // append the OR clause for loop 2,3,4...
    if (index > 0) {
      propList += ', ';
    }

    // append the filter for each column
    propList += `"${DisplayProperties[index]}"`;
  });

  filterString += permFilterString;

  let requestData = {
    data: JSON.stringify([{
      collID: collectionID,
      cols: [propList],
      filter: filterString,
    },]),
  };
  // console.log(JSON.stringify(requestData));
  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/dropdownlist',
    data: requestData,
    dataType: 'json',
    beforeSend: () => {
      $('.loader').hide();
    },
    success: function (res) {
      let responseData = res[0]['Results'];
      let returnArray = [];
      // alert('done');

      responseData.forEach((result) => {
        // Initialize the display object to be returned
        let display = {
          label: '',
          value: result[ValueProperty.toLowerCase() == 'id' ? '_id' : ValueProperty],
        };


        DisplayProperties.forEach((prop, index) => {

          display.label +=
            prop.toLowerCase() == 'id' ?
              `${result['_id']}`
              //: `${result[prop.toLowerCase()]}`;
              :
              `${result[prop]}`;

          // Don't append '-' for last display property
          if (index < DisplayProperties.length - 1) {
            if (Separator != "") {
              display.label += Separator;
            } else {
              display.label += ' - ';
            }
          }
          // console.log(display.label);
        });

        returnArray.push(display);
      });
      // debugger
      // Return our list of label/value objects
      response(returnArray);

    },
    error: (err) => {
      response(err);
    },
  });
}


function advMultiCollectionFocus(event) {

  let dropListDetails = $(event.target).attr('data-columns');

  // Escape single quotes! Otherwise Mr. Json isn't happy.
  dropListDetails = dropListDetails.replace(/'/g, '"');
  let dropListDetailsAdvanced = JSON.parse(dropListDetails);
  let RunOnFocus = dropListDetailsAdvanced.RunOnFocus;
  let func = dropListDetailsAdvanced.Functions.Focus;

  if (RunOnFocus) {
    $(event.target).autocomplete('enable');
    $(event.target).autocomplete('search', '');
  }

  if (typeof window[func] == 'function') {
    // safe to use the function
    window[func]();
  }
}


async function advMultiCollectionSourceNote(request, response, event) {
  // debugger

  let targetElement = event[0].element;
  let parentForm = $(targetElement).parents('form:first');
  let collectionID = $(targetElement).attr('data-coll');
  let collectionAlias = $(targetElement).attr('data-alias');

  let advCollectionJSON = jQuery.parseJSON(
    $(targetElement)
      .attr('data-columns')
      .replace(/'/g, '"')
  );

  let ValueProperty = advCollectionJSON.ValueProperty;

  let DisplayProperties = advCollectionJSON.DisplayProperties;

  // 'OR' filters
  let FilterDBCols = advCollectionJSON.FilterDBCols;

  // Permanent Filters to query the DB. 'AND' filters.
  let PermDBFilters = advCollectionJSON.PermDBFilters;

  let Separator = (advCollectionJSON.Separator) ? advCollectionJSON.Separator : "";

  // get the details of the field

  let searchString = request['term'];

  if (searchString != '') {
    var res = searchString.split(",")[searchString.split(",").length - 1];
    // alert(res);
    searchString = res;

  }
  // alert(searchString);

  let filterString = '';

  if (typeof FilterDBCols != 'undefined') {
    filterString = '(';

    // loop each filter column and build a string
    FilterDBCols.forEach((column, index) => {
      // append the OR clause for loop 2,3,4...
      if (index > 0) {
        filterString = `${filterString} OR `;
      }

      // append the filter for each column
      let filterColumn = column.replace(/[\[\]']+/g, '');

      filterString = `${filterString} ${collectionAlias}.${filterColumn} LIKE '%${searchString}%'`;
    });

    // Close the 'OR' clauses
    filterString = filterString + ')';
  }

  let permFilterString = '';

  // loop each perm filter column and build a string (AND CLAUSE)
  if (typeof PermDBFilters != 'undefined') {
    PermDBFilters.forEach((filter, index) => {

      let permFilterValue = '';

      // Hardcoded val to search
      if (PermDBFilters[index].type == 'STR') {
        permFilterValue = filter.value;
      }

      // Find variable on form to filter by
      if (PermDBFilters[index].type == 'VAR') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .val();
      }

      //find dependancy on advance selectpicker | ADVC - advanced collection dependancy
      if (PermDBFilters[index].type == 'ADVC') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .attr('data-selectedid');
      }
      if (permFilterValue) {

        if (index == 0 && filterString != '') {
          // If theres no filter, dont start the perm filter with AND
          permFilterString = ' AND ';
        }

        // append the OR clause for loop 2,3,4...
        if (index > 0) {
          permFilterString = `${permFilterString} AND `;
        }

        permFilterString += `${collectionAlias}.${filter.col} ` + `${filter.op}` + `'${permFilterValue}'`;
      }
    });
  }

  // loop each display property and build a string
  let propList = '';
  DisplayProperties.forEach((prop, index) => {
    // append the OR clause for loop 2,3,4...
    if (index > 0) {
      propList += ', ';
    }

    // append the filter for each column
    propList += `"${DisplayProperties[index]}"`;
  });

  filterString += permFilterString;

  let requestData = {
    data: JSON.stringify([{
      collID: collectionID,
      cols: [propList],
      filter: filterString,
    },]),
  };
  // console.log(JSON.stringify(requestData));
  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/dropdownlist',
    data: requestData,
    dataType: 'json',
    beforeSend: () => {
      $('.loader').hide();
    },
    success: function (res) {
      let responseData = res[0]['Results'];
      let returnArray = [];
      // alert('done');
      // console.log(responseData);
      responseData.forEach((result) => {
        // Initialize the display object to be returned
        let display = {
          label: '',
          value: result[ValueProperty.toLowerCase() == 'id' ? '_id' : ValueProperty],
        };


        DisplayProperties.forEach((prop, index) => {

          display.label +=
            prop.toLowerCase() == 'id' ?
              `${result['_id']}`
              //: `${result[prop.toLowerCase()]}`;
              :
              `${result[prop]}`;

          // Don't append '-' for last display property
          if (index < DisplayProperties.length - 1) {
            if (Separator != "") {
              display.label += Separator;
            } else {
              display.label += ' - ';
            }
          }
          // console.log(display.label);
        });

        returnArray.push(display);
      });
      // debugger
      // Return our list of label/value objects
      response(returnArray);

    },
    error: (err) => {
      response(err);
    },
  });
}


async function autoCompCollSource(request, response, event) {


  let targetElement = event[0].element;

  let parentForm = $(targetElement).parents('form:first');
  let collectionID = $(targetElement).attr('data-coll');
  let collectionAlias = $(targetElement).attr('data-alias');

  let advCollectionJSON = jQuery.parseJSON(
    $(targetElement)
      .attr('data-columns')
      .replace(/'/g, '"')
  );


  let ValueProperty = advCollectionJSON[0];
  let DisplayProperties = advCollectionJSON;
  DisplayProperties.shift()
  // 'OR' filters
  let FilterDBCols = advCollectionJSON.FilterDBCols;

  if (typeof FilterDBCols == 'undefined') {
    FilterDBCols = advCollectionJSON;
  }

  // Permanent Filters to query the DB. 'AND' filters.
  let PermDBFilters = advCollectionJSON.PermDBFilters;

  let Separator = (advCollectionJSON.Separator) ? advCollectionJSON.Separator : "";

  // get the details of the field

  let searchString = request['term'];

  let filterString = '';

  if (typeof FilterDBCols != 'undefined') {
    filterString = '(';

    // loop each filter column and build a string
    FilterDBCols.forEach((column, index) => {
      // append the OR clause for loop 2,3,4...
      if (index > 0) {
        filterString = `${filterString} OR `;
      }

      // append the filter for each column
      let filterColumn = column.replace(/[\[\]']+/g, '');

      filterString = `${filterString} ${collectionAlias}.${filterColumn} LIKE '%${searchString}%'`;
    });

    // Close the 'OR' clauses
    filterString = filterString + ')';
  }

  let permFilterString = '';

  // loop each perm filter column and build a string (AND CLAUSE)
  if (typeof PermDBFilters != 'undefined') {
    PermDBFilters.forEach((filter, index) => {

      let permFilterValue = '';

      // Hardcoded val to search
      if (PermDBFilters[index].type == 'STR') {
        permFilterValue = filter.value;
      }

      // Find variable on form to filter by
      if (PermDBFilters[index].type == 'VAR') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .val();
      }

      //find dependancy on advance selectpicker | ADVC - advanced collection dependancy
      if (PermDBFilters[index].type == 'ADVC') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .attr('data-selectedid');
      }
      if (permFilterValue) {

        if (index == 0 && filterString != '') {
          // If theres no filter, dont start the perm filter with AND
          permFilterString = ' AND ';
        }

        // append the OR clause for loop 2,3,4...
        if (index > 0) {
          permFilterString = `${permFilterString} AND `;
        }

        permFilterString += `${collectionAlias}.${filter.col} ` + `${filter.op}` + `'${permFilterValue}'`;
      }
    });
  }

  //  console.log(DisplayProperties);return false;
  // loop each display property and build a string
  let propList = '';
  DisplayProperties.forEach((prop, index) => {
    // append the OR clause for loop 2,3,4...
    if (index > 0) {
      propList += ', ';
    }

    // append the filter for each column
    propList += `"${DisplayProperties[index]}"`;
  });

  filterString += permFilterString;
  let requestData = {
    data: JSON.stringify([{
      collID: collectionID,
      cols: [propList],
      filter: filterString,
    },]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/dropdownlist',
    data: requestData,
    dataType: 'json',
    beforeSend: () => {
      $('.loader').hide();
    },
    success: function (res) {
      let responseData = res[0]['Results'];
      let returnArray = [];

      responseData.forEach((result) => {
        // Initialize the display object to be returned
        let display = {
          label: '',
          value: result[ValueProperty.toLowerCase() == 'id' ? '_id' : ValueProperty],
        };


        DisplayProperties.forEach((prop, index) => {

          display.label +=
            prop.toLowerCase() == 'id' ?
              `${result['_id']}`
              //: `${result[prop.toLowerCase()]}`;
              :
              `${result[prop]}`;

          // Don't append '-' for last display property
          if (index < DisplayProperties.length - 1) {
            if (Separator != "") {
              display.label += Separator;
            } else {
              display.label += ' - ';
            }
          }
        });

        returnArray.push(display);
      });

      // Return our list of label/value objects
      response(returnArray);
    },
    error: (err) => {
      response(err);
    },
  });
}


function autoCompCollFocus(event) {


  $(event.target).autocomplete('enable');
  $(event.target).autocomplete('search');


}


// For adding / removing filters
function applyFilterStrings(frame) {


  // Build the complete filter string

  var filter_val_pass = '';
  var filter_val_pass_both_filter = '';
  var filter_val_pass_parm_fltr = '';

  frame.filterList.forEach((filter, index) => {


    if (filter.filterType == 'parmanent_filter') {
      filter_val_pass_parm_fltr += filter.fullFilterString + '++';
    } else if (filter.filterType == 'default_filter') {

      filter_val_pass += filter.fullFilterString + '++';
    }

  });


  // Need to check vikas bhai that this is useful or not

  /*
    $('.disp_filter_'+frame.PFID).each(function()
      {   
 
        if($(this).attr('data-filter')=='parmanent_filter')
        {       
          if(typeof $(this).children('input').val() !='undefined' && $(this).children('input').val() != '')
          {         
            filter_val_pass_parm_fltr += $(this).children('input').val()+'++';                  
          }                   
        }
        else if($(this).attr('data-filter')=='default_filter')
        { 
          
 
          if(typeof $(this).children('input').val() !='undefined' && $(this).children('input').val() != '')
          {         
            filter_val_pass += $(this).children('input').val()+'++';                  
          }     
        }
      });*/

  var filter_val_pass_final = get_filter_strings(filter_val_pass); // GET FILTER STRINGS for default
  var filter_val_pass_parm_fltr_final = get_filter_strings(filter_val_pass_parm_fltr); // GET FILTER STRINGS for permenant


  if (filter_val_pass_parm_fltr_final != '') {
    filter_val_pass_both_filter = filter_val_pass_final + ' AND ' + filter_val_pass_parm_fltr_final;
  } else {
    filter_val_pass_both_filter = filter_val_pass_final;
  }

  frame.filterString = filter_val_pass_both_filter;

}


function get_filter_strings(str) {

  var result = str.split('++');
  var myarray = new Array();
  var finalArr = new Array();
  var finalArr1 = new Array();

  var newres = new Array();
  var checkstring1 = "like";
  var checkstring2 = "=";
  var checkstring3 = ">";
  var checkstring4 = "<";
  var checkstring5 = "<=";
  var checkstring6 = ">=";
  var checkstring7 = "<>";

  $.each(result, function (index) {
    var string = result[index];
    if (string.indexOf(checkstring1) !== -1) {
      var newres = string.split('like');
      newres[2] = result[index];
    } else if (string.indexOf(checkstring6) !== -1) {
      var newres = string.split('>=');
      newres[2] = result[index]
    } else if (string.indexOf(checkstring5) !== -1) {
      var newres = string.split('<=');
      newres[2] = result[index];
    } else if (string.indexOf(checkstring2) !== -1) {
      var newres = string.split('=');
      newres[2] = result[index];
    } else if (string.indexOf(checkstring3) !== -1) {
      var newres = string.split('>');
      newres[2] = result[index];
    } else if (string.indexOf(checkstring4) !== -1) {
      var newres = string.split('<');
      newres[2] = result[index];
    } else if (string.indexOf(checkstring6) !== -1) {
      var newres = string.split('<>');
      newres[2] = result[index]
    }

    if ((typeof newres != 'undefined')) {
      if (myarray[newres[0]]) {
        finalArr.push(newres[0]);
        myarray[newres[0]].push(string);

      } else {
        myarray[newres[0]] = new Array(string);
        finalArr.push(newres[0]);
      }
    }
  });
  finalArr1 = $.grep(finalArr, function (v, k) {
    return $.inArray(v, finalArr) === k;
  });
  var str = '';
  var tempc = 1;
  for (var i = 0; i < finalArr1.length; i++) {
    var temA = finalArr1[i]
    var temCountAr = finalArr1.length

    str += "(";
    var tempCount = 1;
    for (var j = 0; j < myarray[temA].length; j++) {
      var te = myarray[temA].length;
      if (te == tempCount) {
        str += myarray[temA][j];
      } else {
        str += myarray[temA][j] + " OR ";
      }
      tempCount = tempCount + 1
    }
    if (tempc == temCountAr) {
      str += ")"
    } else {
      str += ") AND "
    }
    tempc = tempc + 1
  }
  return str;
}

function changePassword(newPass, userID = 0) {
  /* let requestData = JSON.stringify({
     password: newPass,
     user_id: userID,
   });*/


  let requestData = {
    data: JSON.stringify([{
      password: newPass,
      user_id: userID,
    },]),
  };


  return $.ajax({
    type: 'POST',
    url: base_url + 'core/Login/encrypt_password',
    async: false,
    data: requestData,
    success: function (res) {

      return res;
    },
  });
}

// Delete records (modal)
function commitDelete(frame) {

  // let form = $(`#deleteForm${frame.PFID}`);

  let form = $('#deleteForm' + frame.PFID);
  let values = form.serializeObject();
  let collectionID = form.attr('data-collid');

  let data = Object.assign({
    colls: [{
      coll_id: collectionID,
      values: values
    }],
  });

  let requestData = {
    data: JSON.stringify(data),
  };

  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/delete',
    data: requestData,
    complete: function () {
      hideLoader();
    },
    success: function () {
      $(`#deleteModel${frame.PFID}`).modal('hide');

      $(`#deleteModel${frame.PFID}`).removeClass('open');
      $('body').removeClass('open-bg');
      updateFramePagination(frame);
    },
    error: function (err) {

      if (err.status == 404) {
        err.responseText = 'The requested url was not found on this server.';
      } else if (err.status == 408) {
        err.responseText = 'API request timeout';
      } else if (err.status == 500 || err.status == 503) {
        err.responseText = 'Service Unavailable';
      }

      alert(err.responseText);
    },
  });
}

// update Notification
async function notificationRedirectUpdate(notificationId, notificationLinkUrl) {
  var notificationId = notificationId;
  var notificationLinkUrl = notificationLinkUrl;
  let collectionID = await getCollectionIDByName('bsit_notifications');
  var dataNotification = '{"colls": [{"coll_id": "' + collectionID + '", "values": {"key_id":"' + notificationId + '","key_update_by":"' + userID + '","read_flag":"1"}}]}';
  $.ajax({
    type: 'POST',
    url: base_url + "core/Apilocal/update",
    data: {
      data: dataNotification,
      notificationId: notificationId
    },
    notificationLinkUrl: notificationLinkUrl,
    dataType: 'json',
    success: function (res) {
      if (this.notificationLinkUrl != " ") {
        var notificationfilter = base_url + this.notificationLinkUrl;
        window.location = notificationfilter;
      }
    },
    error: function (err) {
      if (err.status == 404) {
        err.responseText = 'The requested url was not found on this server.';
      } else if (err.status == 408) {
        err.responseText = 'API request timeout';
      } else if (err.status == 500 || err.status == 503) {
        err.responseText = 'Service Unavailable';
      }
    },
  });
}
async function notificationUpdate(notificationId) {
  var notificationId = notificationId;
  let collectionID = await getCollectionIDByName('bsit_notifications');
  var dataNotification = '{"colls": [{"coll_id": "' + collectionID + '", "values": {"key_id":"' + notificationId + '","key_update_by":"' + userID + '","read_flag":"1"}}]}';
  $.ajax({
    type: 'POST',
    url: base_url + "core/Apilocal/update",
    data: {
      data: dataNotification,
      notificationId: notificationId
    },
    notificationId: notificationId,
    dataType: 'json',
    success: function (res) {
      $("#notification" + this.notificationId).remove();
      notificationCount();
    },
    error: function (err) {
      if (err.status == 404) {
        err.responseText = 'The requested url was not found on this server.';
      } else if (err.status == 408) {
        err.responseText = 'API request timeout';
      } else if (err.status == 500 || err.status == 503) {
        err.responseText = 'Service Unavailable';
      }
    },
  });
}
async function notificationCount() {
  var notificationCollection = $(document).find('.notificationDismissAllrecordData');
  if (notificationCollection.length == 0) {
    $('.notificationsDropdown').hide();
    $('#bsit_notifications_footer').hide();
  }
}

// Update records (modal)
async function commitEdit(frame, type, event) {
  if (!type) return;

  let formPrefix = "";
  if (type == "modal") {
    formPrefix = "editForm";
  } else if (type == "sidebar") {
    formPrefix = "editsideModel";
  }

  let form = $(`#${formPrefix}${frame.PFID}`);
  let values = form.serializeObject();
  let collectionID = form.attr('data-collid');
  let dynamicData = form.attr('dynamic_data');

  let fail = false;

  // PASS CHECKBOX VALUE FOR EDIT DATA
  let checkboxes = form.find('input[type="checkbox"]');
  $.each(checkboxes, function (key, value) {
    if (value.checked === false) {
      // TODO: Cleanup obj
      let obj = {};
      obj[value.name] = '0';
      $.extend(values, obj);
    } else {
      let obj = {};
      obj[value.name] = '1';
      $.extend(values, obj);
    }
  });

  let usersCollection = await getCollectionIDByName('Users');

  if (collectionID == usersCollection) {
    values['Password'] = await changePassword(values['Password'], values['key_id']);
  }

  /*form.find('select, textarea, input').each(function () {
    let prop = $(this).attr('name');
    if (
      prop != 'key_id' &&
      prop != 'key_update_date' &&
      prop != 'key_update_by' &&
      $(this).val() == ''
    ) {
      delete values[prop];
    }
  });*/

  let dataPass = {
    key_update_by: userID,
  };

  $.extend(values, dataPass);

  //filer form values
  values = filterFormValues(form, values);

  // TODO: build proper requestData object
  let jsonData = $.fn.bsitJsonObject(collectionID, JSON.stringify(values));

  // Validate fields
  fail = validateFormData(form, formPrefix);

  // Submit if fail never got set to true
  if (fail) {
    hideLoader(event);
  } else {
    $.ajax({
      type: 'POST',
      url: base_url + 'core/Apilocal/update',
      data: {
        data: jsonData,
      },
      dataType: 'json',
      success: function (res) {
        // $('.multiSelect-ul li:not(:last)').remove();

        if (dynamicData == 1) {

          $('#errorImg').hide();
          $('#correctImg').show();
          return false;
        } else {
          $(`#editModel${frame.PFID}`).modal('hide');
          $(`#mySidenav${frame.PFID}`).removeClass('open');
          $(`body`).removeClass('open-bg');
          updateFramePagination(frame);
          $(form).trigger('reset');
        }
      },
      complete: function () {
        hideLoader(event)
      },
      error: function (err) {

        if (err.status == 404) {
          err.responseText = 'The requested url was not found on this server.';
        } else if (err.status == 408) {
          err.responseText = 'API request timeout';
        } else if (err.status == 500 || err.status == 503) {
          err.responseText = 'Service Unavailable';
        }

        if (dynamicData == 1) {
          alert(err.responseText);
          $('#correctImg').hide();
          $('#errorImg').show();
        } else {
          alert(err.responseText);
        }
      },
    });

    //reset form fields
    resetFormFields(form, formPrefix, frame);
  }
}

// Update multiple records
function commitMultiEditDelete(event, frame) {
  let commitType = $(event.target).data('commit-multi');

  let form = $(`#multi${commitType}Form${frame.PFID}`);
  let formProps = form.serializeObject();
  const collectionID = form.data('collid');

  if (commitType == 'Edit') {
    form.find('select, textarea, input').each(function () {
      let prop = $(this).attr('name');
      if (
        prop != 'key_id' &&
        prop != 'key_update_date' &&
        prop != 'key_update_by' &&
        $(this).val() == ''
      ) {
        delete formProps[prop];
      }
    });
  }

  let checks = $('.edit_multi_checkbox:checked').toArray();

  //filer form values
  formProps = filterFormValues(form, formProps);

  let requestData = {
    colls: [],
  };

  let dataPass = {
    key_update_by: userID,
  };

  $.extend(formProps, dataPass);

  checks.forEach((checkbox, index) => {
    // Setup and push the new data object
    requestData.colls.push({
      coll_id: collectionID,
      values: {},
    });

    // Get rowID and assign it to the formProps obj
    formProps['key_id'] = $(checkbox).val();

    // Assign (copy) the formProps instead of referencing them directly
    Object.assign(requestData.colls[index].values, formProps);
  });

  const action = commitType == 'Edit' ? 'update' : 'delete';

  // submit if fail never got set to true
  $.ajax({
    type: 'POST',
    url: `${base_url}core/Apilocal/${action}`,

    data: {
      data: JSON.stringify(requestData),
    },
    dataType: 'json',
    success: function (res) {
      $(`#multi${commitType}Model${frame.PFID}`).modal('hide');
      $(form).trigger('reset');
      updateFramePagination(frame);
    },
    error: function (err) {

      if (err.status == 404) {
        err.responseText = 'The requested url was not found on this server.';
      } else if (err.status == 408) {
        err.responseText = 'API request timeout';
      } else if (err.status == 500 || err.status == 503) {
        err.responseText = 'Service Unavailable';
      }

      alert(err.responseText);
    },
  });

  $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
  $('body').removeClass('open-bg');
  $('.modal-backdrop').addClass('bsit_out');


}

// Saving new records from the 'ADD' modal
async function commitSave(frame, type, event) {
  if (!type) return;

  let formPrefix = "";
  if (type == "modal") {
    formPrefix = "addForm";
  } else if (type == "sidebar") {
    formPrefix = "addFormside";
  }
  let fail = false;
  var fail_log = '';

  let form = $(`#${formPrefix}${frame.PFID}`);
  let formValues = form.serializeObject();
  let collectionID = form.attr('data-collid');

  let saveData = {
    key_create_by: userID,
    key_update_by: userID,
  };

  let usersCollection = await getCollectionIDByName('users');
  if (collectionID == usersCollection) {
    formValues['Password'] = await changePassword(formValues['Password'], 0);
  }

  formValues = filterFormValues(form, formValues);

  // TODO: move from jq extend
  $.extend(formValues, saveData);

  // PASS CHECKBOX VALUE FOR INSERT DATA
  let checkboxes = form.find('input[type="checkbox"]');

  $.each(checkboxes, function (key, value) {
    if (value.checked === false) {
      // TODO: Cleanup obj
      let obj = {};
      obj[value.name] = '0';
      $.extend(formValues, obj);
    } else {
      let obj = {};
      obj[value.name] = '1';
      $.extend(formValues, obj);
    }
  });

  // TODO: Build requestData object correctly
  let jsonData = $.fn.bsitJsonObject(collectionID, JSON.stringify(formValues));

  // Validate fields
  fail = validateFormData(form);

  if (fail) {
    hideLoader(event);
  } else {
    $.ajax({
      type: 'POST',
      url: base_url + 'core/Apilocal/create',
      data: {
        data: jsonData,
      },
      dataType: 'json',
      success: function (res) {
        $(`#addModel${frame.PFID}`).modal('hide');
        updateFramePagination(frame);
        $(form).trigger('reset');

      },
      complete: function () {
        hideLoader(event)
      },
      error: function (err) {

        if (err.status == 404) {
          err.responseText = 'The requested url was not found on this server.';
        } else if (err.status == 408) {
          err.responseText = 'API request timeout';
        } else if (err.status == 500 || err.status == 503) {
          err.responseText = 'Service Unavailable';
        }

        alert(err.responseText);
        $(form).trigger('reset');
      },
    });

    //reset form fields
    resetFormFields(form, formPrefix, frame);

  }
}


function checkregex(regex, value) {

  var re = new RegExp(regex);
  if (!re.test(value)) {
    return 0
  } else {
    return 1;
  }

}
// Downloading frame data as csv
function downloadCSV(csvData, fileName) {
  // Create file & link
  let csvFile = new Blob([csvData], {
    type: 'text/csv'
  });
  let downloadLink = document.createElement('a');

  // File name
  downloadLink.download = fileName;

  // Create a link to the file
  downloadLink.href = window.URL.createObjectURL(csvFile);

  // Hide download link
  downloadLink.style.display = 'none';

  // Add the link to DOM
  document.body.appendChild(downloadLink);

  // Click download link
  downloadLink.click();
}

function filterFieldChange(event, frame) {
  frame.filterField = $(event.target.selectedOptions[0]).html();
  // alert(frame.filterField);
}

// Finding collections by name, so no hardcoded ID's should be used.
function getCollectionIDByName(name) {
  let requestData = {
    data: JSON.stringify([{
      // TODO: No hardcoded
      // collID: 3,

      coll_name: "data_collections",
      filter: `coll.name = '${name}'`,
    },]),
  };

  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      url: base_url + 'core/Apilocal/loadfiltered',
      async: true,
      data: requestData,
      dataType: 'json',
      success: function (res) {

        if (res.session_expire == 1) {

          window.location = base_url;
          return false;
        }


        let collectionID = res[0]['Results'][0].CollID;
        resolve(collectionID);
      },
      error: function (err) {
        reject(err);
      },
    });
  });
}


function pagerefrace(event, frame) {
  updateFramePagination(frame);


}

// Used to retrieve new data for a frame and reload it.
function getFrameData(frame, getCSV = 0) {
  // Loading data, show the spinner.
  showLoader(false, true, false);

  const requestURL = base_url + 'core/Frame/load_content';
  let sort = '';
  if (frame.sortProperty == undefined) {
    frame.sortProperty = '';
  }

  if (frame.sortDirection == undefined) {
    frame.sortDirection = '';
  }

  if (frame.sortProperty != '' && frame.sortDirection != '') {
    sort = `${frame.sortProperty} ${frame.sortDirection}`;
  }

  frame.filterField = '';

  // code added by HD for permanent filter
  var fram_filter_data = $('#fram_prop_filter_' + frame.PFID).val();

 // load data on popup 
  var fram_prop_url = $('#fram_prop_url_' + frame.PFID).val();
  var page_id = $('#page_id_' + frame.PFID).val();
  
 //var url = window.location.href;
   var url; 
  
  if((fram_prop_url && page_id )){
    url = fram_prop_url;
  }
  else{
    url = window.location.href;
  }
  
  var strGetParams = url.split('?');
  var parameter = '';
  if (typeof strGetParams[1] != "undefined") {
    parameter += strGetParams[1];
  }

  var prop_frame_filter_parameter = swapout_variables_AJAX(parameter, fram_filter_data);


  var filter_field = $('#filter_name_' + frame.PFID).val();
  var filter_oprator = $('#filter_oprator_' + frame.PFID).val();;
  var filter_text = $('#filter_text_' + frame.PFID).val();;


  var history_api_back_val = '';
  if (filter_field != '') {
    history_api_back_val = filter_field + '**' + filter_oprator + '**' + filter_text + '**3';
  }

  // code end by HD make a new function swapout_variables_AJAX


  if (typeof $('#filter_history_api_' + frame.PFID).val() == 'undefined' || $('#filter_history_api_' + frame.PFID).val() == '') {

    $('#filter_history_api_' + frame.PFID).val(history_api_back_val);
  } else {

    var fhv = $('#filter_history_api_' + frame.PFID).val();

    var final_history_filter_val = fhv + '&&' + history_api_back_val;
    $('#filter_history_api_' + frame.PFID).val(final_history_filter_val);

  }


  let requestData = {
    filter: frame.filterString,
    order: sort,
    page_num: frame.currPage,
    num_recs: frame.rowsPerPage,
   page_id: (page_id) ? page_id : pageID,
    frame_id: frame.PFID,
    pf: 1,
    prop_frame_filter_parameter: prop_frame_filter_parameter,
    history_api_back_val: history_api_back_val,

    // downloadCSV
    download_data: getCSV,
  };


  var history_filter_search_pass = $('#filter_history_api_' + frame.PFID).val();
  var field_id = $('#page_frame_' + frame.PFID + ' #sort_field_' + frame.PFID).val();

  if (frame.filterString != '' || sort != '' || frame.currPage >= '1') {
    var history_api_data_field = requestData
    history_api_data_field['history_api_back_val'] = history_filter_search_pass;
    history_api_data_field['sort_field_id'] = field_id;
    history_api_data_field['frame_id'] = frame.PFID;
    $('#history_api_data_' + frame.PFID).val(JSON.stringify(history_api_data_field));
  } else {
    $('#history_api_data_' + frame.PFID).val('');
  }


  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      data: requestData,
      url: requestURL,
      complete: function () {
        hideLoader();
      },
      success: function (res) {
        if (getCSV === 1) {
          downloadCSV(res, `bsit_io_${frame.PFID}_${Date.now()}.csv`);
        } else {
          // Update frame with new data
          $(`#page_frame_${frame.PFID}_content`).replaceWith(res);
        }
        resolve(1);


      },
      error: function (err) {
        reject(err);
      },
    });
  });
}

/*console.log(window.history.state);


if(window.history.state != null){
  console.log(frame.PFID);
}
*/

// ---------------------------------------------------
// -- GET PROPERTY FILTER VALUE FOR MAKING FRAME ARRAY
// ---------------------------------------------------
function swapout_variables_AJAX(parameter, fram_filter_data) {
  var result = "";
  $.ajax({
    type: 'GET',
    data: {
      'parameter': parameter,
      'fram_filter_data': fram_filter_data
    },
    async: false,
    url: base_url + "core/Page/swapout_variables_ajax",
    success: function (data) {
      result = data;
      //result = result.replace(new RegExp("\r\n", "g"), "");
    },
    error: function (xx) {
      alert(xx.responseText);
    }
  });
  return result;
}

function loadDefaultFilters(frame) {
  let filterCards = $('.bsit-filter-area')
    .children()
    .toArray();

  filterCards.forEach((filter) => {

    const filterText = $(filter)
      .find('.s_value')
      .text();

    const filterField = $(filter)
      .find('.s_name')
      .text();

    const filterOp = $(filter)
      .find('.s_operator')
      .text();

    const fullFilterString = $(filter)
      .find(`.filter_vals_${frame.PFID}`)
      .val();

    let filterData = {
      filterField: filterField,
      filterOperator: filterOp,
      filterText: filterText,
      filterFieldCard: $(filter).html(),
      filterTextCard: filterText,
      fullFilterString: fullFilterString,
      filterType: $(filter).attr('data-filter')
    };

    // Prepend the new filter to our array.
    frame.filterList.unshift(filterData);
  });

  // Reset filter string and frame page ready for new data
  frame.filterString = '';
  frame.currPage = 1;

  // build the filter string
  applyFilterStrings(frame);
}

function loadFilterCard(frame, filterData) {
  // Build a card for each filter


  var filter_field = filterData.filterField;
  var filter_oprator = filterData.filterOperator;
  var filter_text = filterData.filterTextCard;
  var history_api_back_val = '';

  $('.select .select-styled').html('');

  if (filter_field != '') {
    history_api_back_val = filter_field + '**' + filter_oprator + '**' + filter_text + '**3';
    // alert(history_api_back_val);
  }

  // amit changes
  var field_select_value = $(`#filter_name_${frame.PFID}`).find(":selected").text();

  let requestData = {
    filterField: filterData.filterFieldCard, //field_select_value,
    filterOperator: filterData.filterOperator,
    filterText: filterData.filterTextCard,
    filterString: filterData.fullFilterString,
    pageFrameId: frame.PFID,
    history_api_back_val: history_api_back_val,
  };


  // Load and build frame filter card for each filter
  $.ajax({
    type: 'POST',
    url: '../core/Frame/loadFilterCard',
    data: requestData,
    success: function (res) {

      $(`#filter_list_div_${frame.PFID}`).append(res);
      // alert(res);
    },
  });
}


// Navigating through table pages.
function paginationClick(event, frame) {
  $(`#addModel${frame.PFID}`).removeClass('open');
  $(`#editModel${frame.PFID}`).removeClass('open');
  $(`#deleteModel${frame.PFID}`).removeClass('open');
  $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
  $(`#sidemultiDeleteModel${frame.PFID}`).removeClass('open');
  $('.modal-backdrop').addClass('bsit_out');
  $('body').removeClass('open-bg');

  if ($(event.target).attr('data-nextpage')) {
    frame.currPage++;
    updateFramePagination(frame);
  } else {
    frame.currPage--;
    updateFramePagination(frame);
  }
}

function removeFilter(event, frame) {


  // TODO: Find better solution than .parent.find
  let cancelParent = $(event.currentTarget).parent();
  let filterValue = $(cancelParent)
    .find('.bsit_filter_values')
    .val();

  if (typeof filterValue == 'undefined') {
    filterValue = $(cancelParent)
      .find(`.filter_vals_${frame.PFID}`)
      .val();
  }

  var cancel_hist_val = $(event.currentTarget).parent().find("input[type=hidden]").attr('data-hist_filter_val_' + frame.PFID);

  //var filter_history_api_val = $('.filter_history_api_class').val();
  var filter_history_api_val = $('#filter_history_api_' + frame.PFID).val();
  var filter_history_api_single_val = filter_history_api_val.split('&&');
  // console.log(filter_history_api_single_val);
  // console.log(cancel_hist_val);
  if (filter_history_api_single_val != '') {
    $.each(filter_history_api_single_val, function (key, value) {

      if (value.toLowerCase() == cancel_hist_val.toLowerCase()) {

        filter_history_api_val = filter_history_api_val.replace(value, '');

      }
    });


    $('#filter_history_api_' + frame.PFID).val(filter_history_api_val);
    //$('.filter_history_api_class').val(filter_history_api_val);
  }

  //return false;

  // Remove the filter card
  $(event.currentTarget)
    .parent()
    .remove();

  if (frame.filterString == filterValue) {
    // Removing the only filter
    frame.filterList.splice(0, 1);
    frame.filterString = '';
  } else {
    // Find the index of the filter so we can remove it
    let index = frame.filterList.findIndex(
      (filter) => filter.fullFilterString == filterValue
    );


    // Remove the filter from the array.

    frame.filterList.splice(index, 1);


    // Rebuild the filter string
    applyFilterStrings(frame);
  }


  // Reload using new filter
  updateFramePagination(frame);
  saveHistoryData(frame);
}

// Updating the amount of returned rows for a frame
function rowCountChange(event, frame) {
  frame.rowsPerPage = parseInt(event.target.value);
  frame.currPage = 1;
  $(`#addModel${frame.PFID}`).removeClass('open');
  $(`#editModel${frame.PFID}`).removeClass('open');
  $(`#deleteModel${frame.PFID}`).removeClass('open');
  $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
  $(`#sidemultiDeleteModel${frame.PFID}`).removeClass('open');
  $('.modal-backdrop').addClass('bsit_out');
  $('body').removeClass('open-bg');
  updateFramePagination(frame);
}

// Fired on page load. Gets the frame's default row
// count and initializes the pagination buttons.
// Also creates the frame object that stores the frame state.
function setupFramePagination(frame) {
  // Get default record count and check for more recs
  let hasMoreRows = parseInt($(`.has_more_records_${frame.PFID}`).val());
  let rowsPerPage = $(`#default_page_size_${frame.PFID}`).val();
  rowsPerPage = rowsPerPage === '' ? 10 : rowsPerPage;

  // Setup pagination buttons
  if (!hasMoreRows) {
    $(`#pagi_next_${frame.PFID}`)
      .attr('disabled', 'disabled')
      .css('opacity', 0.3);
  } else {
    $(`#pagi_next_${frame.PFID}`)
      .removeAttr('disabled', 'disabled')
      .css('opacity', 1);
  }

  $(`#pagi_previous_${frame.PFID}`)
    .attr('disabled', 'disabled')
    .css('opacity', 0.3);

  // Apply specific TableView properties to the frame object
  Object.defineProperties(frame, {
    currPage: {
      value: 1,
      writable: true,
    },
    sortProperty: {
      value: '',
      writable: true,
    },
    sortDirection: {
      value: '',
      writable: true,
    },
    filterList: {
      value: [],
      writable: true,
    },
    filterString: {
      value: '',
      writable: true,
    },
    filterField: {
      value: '',
      writable: true,
    },
    rowsPerPage: {
      value: rowsPerPage,
      writable: true,
    },
  });


  loadDefaultFilters(frame);


  loadHistoryData(frame);


}


function loadHistoryData(frame) {


  if (window.history.state != null) {

    var back_history_var = window.history.state;
    var PFID = frame.PFID
    // back_history.frame_i

    var result = "back_history_var.frame_id_" + frame.PFID;

    var convertString = eval(result);


    if (typeof convertString != 'undefined') {

      back_history_var = window.history.state[frame.PFID];
    } else {


      back_history_var = window.history.state;

    }
    //  console.log(back_history_var);return false;

    if (back_history_var.frame_id == frame.PFID) {

      if (back_history_var.frame_id == frame.PFID) {
        $('#filter_history_api_' + frame.PFID).val(back_history_var.history_api_back_val);
        $('#history_api_data_' + frame.PFID).val(JSON.stringify(back_history_var));
      }

    }

  }
}

function onclickClose() {
  $(`.modal-backdrop`).on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
  });
}

// Show delete model 
function showEditDeleteModal(event, frame, isPaneView) {
  let modalID = (isPaneView == 1) ? "editForm" : "editsideModel";

  // TODO: Come up with a better solution than .parent.parent? .closest?
  // let parentElem = $(event.target)
  //   .parent()
  //   .parent();

  let parentElem = event.currentTarget;

  // console.log($(parentElem).data('collid'));
  // get data variables from the button
  let collectionID = parentElem.dataset['collid'];
  let recordID = parentElem.dataset['recid'];
  let modal = parentElem.dataset['modal'];
  let modalName = modal == 'edit' ? 'Edit' : 'Delete';

  $(`.modal_${frame.PFID} h2`).html(`${modalName} ${page_title}`);



  let requestData = {
    data: JSON.stringify([{
      collID: collectionID,
      recID: recordID,
    },]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/loadsingle',
    data: requestData,
    dataType: 'json',
    success: function (res) {

      if (res.session_expire == 1) {

        window.location = base_url;
        return false;
      }
      let results = res[0]['Results'][0];

      let advCollElements = [];
      let autoComElements = [];

      // TODO: forEach
      // loop each column and try and populate the corresponding form field
      var req = new Promise((resolve, reject) => {
        let l = 0;
        let totalLen = Object.keys(results).length;
        $.each(results, function (key, value) {

          // code for deleting Checkbox Value and assigning it in Checkbox
          if (value == '1') {
            $(`.mui-checkbox [name='${key}']`).prop('checked', true);
          } else {
            $(`.mui-checkbox [name='${key}']`).prop('checked', false);
          }

          if (key === 'signature_file') {
            $('[name=signature_file]').attr('src', value);
          } else {
            //if (value && (value != "NULL" || value != "null")) {
            $(`#${modalID}${frame.PFID} [name='${key}']`).val(value);
            $(`#deleteForm${frame.PFID} [name='${key}']`).val(value);
            //}

            //check to see if the field is a date field
            var el = $(`#${modalID}${frame.PFID} [name='${key}']`);
            var elementClass = el.attr('class');

            if (elementClass != undefined) {
              if (elementClass.indexOf("datepicker") >= 0 && value && (value != "NULL" || value != "null")) {
                var dateValue = new Date(value);
                $(`#${modalID}${frame.PFID} [name='${key}']`).val($.datepicker.formatDate("yy/mm/dd", dateValue)).change();
              }

              if (isPaneView != 2 && modalName == 'Edit') {
                //ckeditor
                if (elementClass.indexOf("ckeditor") >= 0) {
                  //Set CKeditor value
                  setValueOfCkeditor(el, value);
                }

                //autoCompColl
                if (elementClass.indexOf("autoCompCollAdv") >= 0) {
                  let advCollDependency = checkAdvanceCollDependency(el);

                  if (advCollDependency) {
                    advCollElements.push(el);
                  } else {
                    setValueOfAdvanceAutoComplete(el);
                  }
                } else if (elementClass.indexOf("autoCompColl") >= 0) {
                  let advCollDependency = checkAdvanceCollDependency(el);
                  if (advCollDependency) {
                    autoComElements.push(el);
                  } else {
                    setValueOfAutoComplete(el);
                  }
                }

                if (elementClass.indexOf("advanceMultiSelect") >= 0) {

                  $(`#${modalID}${frame.PFID} [name='${key}']`).val(value);
                  var userGetLebalValue = $(`#${modalID}${frame.PFID} [name='${key}']`).val().split(',');
                  $.each(userGetLebalValue, function (key, value) {

                    // $("#hf_advanceMultiSelect").attr("labelMultiSelect",""${key}+"_label").val(value);
                  });


                  var obj = {};
                  for (var i = 0; i < userGetLebalValue.length; i++) {
                    var keyValue = userGetLebalValue[i].split(":");
                    obj[keyValue[0]] = keyValue[1];
                  }

                  var suserValue = [];
                  var suserLabel = [];
                  $.each(obj, function (idx2, val2) {
                    var value = idx2;
                    var label = val2;
                    suserValue.push(value);
                    suserLabel.push(label);
                  });

                  var lavelUserAppend = suserLabel.join();
                  var valueUserAppend = suserValue.join();


                  $(`#${modalID}${frame.PFID} [labelMultiSelect='${key}_label']`).val(lavelUserAppend);
                  $(`#${modalID}${frame.PFID} [valueMultiSelect='${key}_value']`).val(valueUserAppend);

                  // $(`#${modalID}${frame.PFID} [name='label_${key}']`).val(lavelUserAppend);
                  // $(`#${modalID}${frame.PFID} [name='value_${key}']`).val(valueUserAppend);

                  $.each($(`#${modalID}${frame.PFID} [labelMultiSelect='${key}_label']`).val().split(","), function (i, v) {
                    if (v != "") {
                      var propname = `'${key}'`;
                      $(`#${modalID}${frame.PFID} [labelMultiSelect='${key}_label']`).after('<li><span class="del-multi-select-val" propname="' + propname + '" data-val="' + v + '">X</span>' + v + '</li>');
                    }
                  });

                }
              }
            }
          }

          l++;
          if (l === totalLen) {
            resolve();
          }
        });

      });

      req.then(() => {
        $.each(advCollElements, function (key, value) {
          setTimeout(function () {
            setValueOfAdvanceAutoComplete(value)
          }, 500);
        });

        $.each(autoComElements, function (key, value) {
          setTimeout(function () {
            setValueOfAutoComplete(value)
          }, 500);
        });
      });

    },
    error: function (err) {

      if (err.status == 404) {
        err.responseText = 'The requested url was not found on this server.';
      } else if (err.status == 408) {
        err.responseText = 'API request timeout';
      } else if (err.status == 500 || err.status == 503) {
        err.responseText = 'Service Unavailable';
      }

      alert(err.responseText);
    },
  });



  if (isPaneView == 1) {
    // Show the modal depending on which button was clicked
    if (modalName == 'Edit') {
      $(`#editModel${frame.PFID}`).modal();
      onclickClose();
    } else {
      $(`#deleteModel${frame.PFID}`).modal();
      onclickClose();
    }
  } else if (isPaneView == 2) {
    // Show the modal depending on which button was clicked
    if (modalName == 'Edit') {
      $(`#editModel${frame.PFID}`).modal();

    } else {
      // $(`#deleteModel${frame.PFID}`).modal();
      $(`#deleteModel${frame.PFID}`).addClass('open');
      $('body').addClass('open-bg');
      $(`#deletecloseNav${frame.PFID}`).click(function () {
        $(`#deleteModel${frame.PFID}`).removeClass('open');
        $('body').removeClass('open-bg');
      });

      $(`#editModel${frame.PFID}`).removeClass('open');
      $(`#addModel${frame.PFID}`).removeClass('open');
      $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
      $('.modal-backdrop').addClass('bsit_out');
      // $(`#deleteModel${frame.PFID}`).removeClass('open');
      // $(`#mySidenavbar${frame.PFID}`).removeClass('open');
      // $('body').removeClass('open-bg');
    }
  } else {
    $(`#editModel${frame.PFID}`).addClass('open');
    $('body').addClass('open-bg');
    $(`#closeNav${frame.PFID}`).click(function () {
      $(`#editModel${frame.PFID}`).removeClass('open');
      $('body').removeClass('open-bg');
    });
    $(`#mySidenavbar${frame.PFID}`).removeClass('open');
    $(`#addModel${frame.PFID}`).removeClass('open');
    $(`#deleteModel${frame.PFID}`).removeClass('open');
    $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
    $('.modal-backdrop').addClass('bsit_out');
    // $('body').removeClass('open-bg');
  }
}


function checkAdvanceCollDependency(el) {
  let advCollDependency = false;

  let advCollectionJSON = jQuery.parseJSON(
    el.attr('data-columns').replace(/'/g, '"')
  );

  // Permanent Filters to query the DB. 'AND' filters.
  let PermDBFilters = advCollectionJSON.PermDBFilters;

  if (typeof PermDBFilters != 'undefined') {
    PermDBFilters.forEach((filter, index) => {
      //find dependancy on advance selectpicker | ADVC - advanced collection dependancy
      if (PermDBFilters[index].type == 'ADVC') {
        advCollDependency = true;
      }
    });
  }

  return advCollDependency;
}

function showMultiEditDeleteModal(event, frame) {
  let modalType = $(event.target).attr('edit');

  $(`#multi${modalType}Model${frame.PFID}`).modal();

  commonShowMultiEditDeleteModal(frame, modalType);
}



function showMultiEditDeleteModalSidebar(event, frame) {

  let modalType = $(event.target).attr('edit');

  $(`#sidemulti${modalType}Model${frame.PFID}`).modal();

  commonShowMultiEditDeleteModal(frame, modalType);

  $(`#sidemultiEditModel${frame.PFID}`).addClass('open');
  $('body').addClass('open-bg');

  $(`#closeNavmulti${frame.PFID}`).click(function () {
    $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
    $('body').removeClass('open-bg');
    $('.modal-backdrop').addClass('bsit_out');
  });

  $(`#editModel${frame.PFID}`).removeClass('open');
  $(`#addModel${frame.PFID}`).removeClass('open');
  $(`#deleteModel${frame.PFID}`).removeClass('open');
  $(`#sidemultiDeleteModel${frame.PFID}`).removeClass('open');
  // $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
  // $('body').removeClass('open-bg');
  $('.modal-backdrop').addClass('bsit_out');

}

function commonShowMultiEditDeleteModal(frame, modalType) {
  let frameTitle = $(`#page_frame_${frame.PFID}_header h3`).text();
  let checkedCount = 0;
  let modalName = '';

  checkedCount = $('.edit_multi_checkbox:checked').length;

  if (frameTitle != '') {
    modalName = `${modalType} ${frameTitle} ${checkedCount} records`;
  } else {
    modalName = `${modalType} ${page_title} ${checkedCount} records`;
  }

  $(`.modal_${frame.PFID} h2`).html(modalName);

  if (checkedCount > 0) {
    $(`.multi_${modalType}_from_${frame.PFID}`).show();
    $(`.multi_${modalType}_content_${frame.PFID}`).hide();
  } else {
    $(`.multi_${modalType}_from_${frame.PFID}`).hide();
    $(`.multi_${modalType}_content_${frame.PFID}`).show();
  }
}

function showMultiDeleteModalSidebar(event, frame) {

  let modalType = $(event.target).attr('edit');

  $(`#sidemulti${modalType}Model${frame.PFID}`).modal();

  commonShowMultiEditDeleteModal(frame, modalType)

  $(`#sidemultiDeleteModel${frame.PFID}`).addClass('open');
  $('body').addClass('open-bg');

  $(`#closeNavmultidelete${frame.PFID}`).click(function () {
    $(`#sidemultiDeleteModel${frame.PFID}`).removeClass('open');
    $('body').removeClass('open-bg');
    $('.modal-backdrop').addClass('bsit_out');
  });

  $(`#editModel${frame.PFID}`).removeClass('open');
  $(`#addModel${frame.PFID}`).removeClass('open');
  $(`#deleteModel${frame.PFID}`).removeClass('open');
  $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
  // $('body').removeClass('open-bg');
  $('.modal-backdrop').addClass('bsit_out');

}

// Sorting frame records in ascending or descending order.
async function sortHeaderClick(event, frame) {
  let headerPropID = $(event.target).attr('id');

  if (typeof headerPropID != 'undefined' && headerPropID !== '') {
    let sortProperty = headerPropID.split('-')[1];
    let sortDirection = $(`#${headerPropID}`).data('sort');

    if (frame.sortProperty == `[${sortProperty}]`) {
      frame.sortDirection = frame.sortDirection == 'asc' ? 'desc' : 'asc';
    } else {
      frame.sortProperty = `[${sortProperty}]`;
      frame.sortDirection = sortDirection;
    }

    let arrow = frame.sortDirection == 'asc' ? ' ↑' : ' ↓';

    await updateFramePagination(frame);

    // TODO: Apply sort arrow after pagination change

    $(`#sort_field_${frame.PFID}`).val(headerPropID);
    $('.PFID').val(frame.PFID);

    $(`#page_frame_${frame.PFID} .sort_arrow`).html(' ');
    $(`#page_frame_${frame.PFID}  #${headerPropID}`).data(
      'sort',
      frame.sortDirection
    );
    $(`#page_frame_${frame.PFID} #${headerPropID} .sort_arrow`).html(arrow);
  }
}

async function setValueOfAutoComplete(element) {
  let targetElement = element;

  let parentForm = targetElement.parents('form:first');
  let collectionID = targetElement.attr('data-coll');
  let collectionAlias = targetElement.attr('data-alias');

  let advCollectionJSON = jQuery.parseJSON(
    targetElement
      .attr('data-columns')
      .replace(/'/g, '"')
  );


  let ValueProperty = advCollectionJSON[0];
  let DisplayProperties = advCollectionJSON;
  DisplayProperties.shift()
  // 'OR' filters
  let FilterDBCols = advCollectionJSON.FilterDBCols;

  if (typeof FilterDBCols == 'undefined') {
    FilterDBCols = advCollectionJSON;
  }

  // Permanent Filters to query the DB. 'AND' filters.
  let PermDBFilters = advCollectionJSON.PermDBFilters;

  let Separator = (advCollectionJSON.Separator) ? advCollectionJSON.Separator : "";

  // get the details of the field

  let searchString = $(targetElement).val();

  let filterString = '';
  let filterColumn = '';

  if (searchString && searchString != 'NULL') {

    if (typeof FilterDBCols != 'undefined') {

      // filterColumn = FilterDBCols[0].replace(/[\[\]']+/g, '');
      // filterString = '( ' + `${collectionAlias}.${filterColumn} = '${searchString}'` + ' )';
      filterString = '(';

      // loop each filter column and build a string
      FilterDBCols.forEach((column, index) => {
        // append the OR clause for loop 2,3,4...
        if (index > 0) {
          filterString = `${filterString} OR `;
        }

        // append the filter for each column
        let filterColumn = column.replace(/[\[\]']+/g, '');

        filterString = `${filterString} ${collectionAlias}.${filterColumn} = '${searchString}'`;
      });

      // Close the 'OR' clauses
      filterString = filterString + ')';
    }
  } else {
    element.attr("data-selectedid", searchString);
    return;
  }

  let permFilterString = '';

  // loop each perm filter column and build a string (AND CLAUSE)
  if (typeof PermDBFilters != 'undefined') {
    PermDBFilters.forEach((filter, index) => {

      let permFilterValue = '';

      // Hardcoded val to search
      if (PermDBFilters[index].type == 'STR') {
        permFilterValue = filter.value;
      }

      // Find variable on form to filter by
      if (PermDBFilters[index].type == 'VAR') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .val();
      }

      //find dependancy on advance selectpicker | ADVC - advanced collection dependancy
      if (PermDBFilters[index].type == 'ADVC') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .attr('data-selectedid');
      }
      if (permFilterValue) {

        if (index == 0 && filterString != '') {
          // If theres no filter, dont start the perm filter with AND
          permFilterString = ' AND ';
        }

        // append the OR clause for loop 2,3,4...
        if (index > 0) {
          permFilterString = `${permFilterString} AND `;
        }

        permFilterString += `${collectionAlias}.${filter.col} ` + `${filter.op}` + `'${permFilterValue}'`;
      }
    });
  }

  //  console.log(DisplayProperties);return false;
  // loop each display property and build a string
  let propList = '';
  DisplayProperties.forEach((prop, index) => {
    // append the OR clause for loop 2,3,4...
    if (index > 0) {
      propList += ', ';
    }

    // append the filter for each column
    propList += `"${DisplayProperties[index]}"`;
  });

  filterString += permFilterString;
  let requestData = {
    data: JSON.stringify([{
      collID: collectionID,
      cols: [propList],
      filter: filterString,
    },]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/dropdownlist',
    data: requestData,
    dataType: 'json',
    beforeSend: () => {
      $('.loader').hide();
    },
    success: function (res) {
      let responseData = res[0]['Results'];
      let returnArray = [];

      responseData.forEach((result) => {
        // Initialize the display object to be returned
        let display = {
          label: '',
          value: result[ValueProperty.toLowerCase() == 'id' ? '_id' : ValueProperty],
        };


        DisplayProperties.forEach((prop, index) => {
          //console.log(result);return false;
          display.label +=
            prop.toLowerCase() == 'id' ?
              `${result['_id']}`
              //: `${result[prop.toLowerCase()]}`;
              :
              `${result[prop]}`;

          // Don't append '-' for last display property
          if (index < DisplayProperties.length - 1) {
            if (Separator != "") {
              display.label += Separator;
            } else {
              display.label += ' - ';
            }
          }
        });

        element.val(display.label);
        element.attr("data-selectedid", display.value);
      });

    },
    error: (err) => {
      alert(err.responseText);
    },
  });
}



async function setValueOfAdvanceAutoComplete(element) {
  let targetElement = element;
  let parentForm = targetElement.parents('form:first');
  let collectionID = targetElement.attr('data-coll');
  let collectionAlias = targetElement.attr('data-alias');

  element.attr("data-dirty", "0");

  let advCollectionJSON = jQuery.parseJSON(
    targetElement
      .attr('data-columns')
      .replace(/'/g, '"')
  );

  let ValueProperty = advCollectionJSON.ValueProperty;

  let DisplayProperties = advCollectionJSON.DisplayProperties;

  // 'OR' filters
  let FilterDBCols = advCollectionJSON.FilterDBCols;

  // Permanent Filters to query the DB. 'AND' filters.
  let PermDBFilters = advCollectionJSON.PermDBFilters;

  let Separator = (advCollectionJSON.Separator) ? advCollectionJSON.Separator : "";

  // get the details of the field

  let searchString = $(targetElement).val();

  let filterColumn = '';
  let filterString = '';


  if (searchString && searchString != 'NULL') {
    //let filterColumn = ValueProperty.replace(/[\[\]']+/g, '');
    filterColumn = FilterDBCols[0].replace(/[\[\]']+/g, '');
    filterString = '( ' + `${collectionAlias}.${filterColumn} = '${searchString}'` + ' )';
  } else {
    element.attr("data-selectedid", searchString);
    return;
  }


  // if (typeof FilterDBCols != 'undefined') {
  //   filterString = '(';

  //   // loop each filter column and build a string
  //   FilterDBCols.forEach((column, index) => {
  //     // append the OR clause for loop 2,3,4...
  //     if (index > 0) {
  //       filterString = `${filterString} OR `;
  //     }

  //     // append the filter for each column
  //     let filterColumn = column.replace(/[\[\]']+/g, '');

  //     filterString = `${filterString} ${collectionAlias}.${filterColumn} = '${searchString}'`;

  //   });

  //   // Close the 'OR' clauses
  //   filterString = filterString + ')';
  // }

  let permFilterString = '';

  // loop each perm filter column and build a string (AND CLAUSE)
  if (typeof PermDBFilters != 'undefined') {
    PermDBFilters.forEach((filter, index) => {

      let permFilterValue = '';

      // Hardcoded val to search
      if (PermDBFilters[index].type == 'STR') {
        permFilterValue = filter.value;
      }

      // Find variable on form to filter by
      if (PermDBFilters[index].type == 'VAR') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .val();
      }

      //find dependancy on advance selectpicker | ADVC - advanced collection dependancy
      if (PermDBFilters[index].type == 'ADVC') {
        permFilterValue = $(parentForm)
          .find(`input[name='${filter.value}']:first`)
          .attr('data-selectedid');
      }
      if (permFilterValue) {

        if (index == 0 && filterString != '') {
          // If theres no filter, dont start the perm filter with AND
          permFilterString = ' AND ';
        }

        // append the OR clause for loop 2,3,4...
        if (index > 0) {
          permFilterString = `${permFilterString} AND `;
        }

        permFilterString += `${collectionAlias}.${filter.col} ` + `${filter.op}` + `'${permFilterValue}'`;
      }
    });
  }

  // loop each display property and build a string
  let propList = '';
  DisplayProperties.forEach((prop, index) => {
    // append the OR clause for loop 2,3,4...
    if (index > 0) {
      propList += ', ';
    }

    // append the filter for each column
    propList += `"${DisplayProperties[index]}"`;
  });

  filterString += permFilterString;

  let requestData = {
    data: JSON.stringify([{
      collID: collectionID,
      cols: [propList],
      filter: filterString,
    },]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/dropdownlist',
    data: requestData,
    dataType: 'json',
    beforeSend: () => {
      $('.loader').hide();
    },
    success: function (res) {
      let responseData = res[0]['Results'];
      let returnArray = [];


      responseData.forEach((result) => {
        // Initialize the display object to be returned
        let display = {
          label: '',
          value: result[ValueProperty.toLowerCase() == 'id' ? '_id' : ValueProperty],
        };


        DisplayProperties.forEach((prop, index) => {

          display.label +=
            prop.toLowerCase() == 'id' ?
              `${result['_id']}`
              //: `${result[prop.toLowerCase()]}`;
              :
              `${result[prop]}`;

          // Don't append '-' for last display property
          if (index < DisplayProperties.length - 1) {
            if (Separator != "") {
              display.label += Separator;
            } else {
              display.label += ' - ';
            }
          }
        });

        element.val(display.label);
        element.attr("data-selectedid", display.value);
      });
    },
    error: (err) => {
      alert(err.responseText);
    },
  });
}

async function setValueOfCkeditor(element, value) {
  let parentForm = element.parents('form:first');
  let elementName = element.attr("name");
  let formId = $(parentForm).attr("id");

  let cke = getCkEditor(elementName, formId);
  if (cke) {
    cke.setData(value);
  }
}

// Loads new data based on the frame object changes and
// updates the pagination buttons accordingly.
async function updateFramePagination(frame) {
  // Wait for new data.

  await getFrameData(frame, 0);
  // TODO: Fix this when filters load no data
  let hasMoreRows = parseInt($(`.has_more_records_${frame.PFID}`).val());

  let pagiNext = $(`#pagi_next_${frame.PFID}`);
  let pagiPrev = $(`#pagi_previous_${frame.PFID}`);

  // Disable pagination if there are less recs than default rec count
  if (!hasMoreRows) {
    // frame.currPage = 1;
    $(pagiNext)
      .attr('disabled', 'disabled')
      .css('opacity', 0.3);
  } else {
    $(pagiNext)
      .removeAttr('disabled', 'disabled')
      .css('opacity', 1);
  }

  if (frame.currPage > 1) {
    $(pagiPrev)
      .removeAttr('disabled', 'disabled')
      .css('opacity', 1);
  } else {
    $(pagiPrev)
      .attr('disabled', 'disabled')
      .css('opacity', 0.3);
  }
  $(`#num_recs_${frame.PFID}`).val(frame.rowsPerPage);
  // rendar_context_menu();

  activate_content_filter();
  //edit_menu();
  // seeMoreRecords_seeLessRecords();
}

/*
function seeMoreRecords_seeLessRecords() {
  $(".bsit_page_table_listing .bsit_visible_mobile").hide();
  if (window.matchMedia('(max-width: 768px)').matches) {
    $(".bsit_page_table_listing .bsit_visible_mobile").show();
    $(".bsit_page_table_listing tr .bsit_show_hide").hide();
    var visibleRecord = 4;
    $(".seeMoreRecords").each(function () {
      var k = 0;
      // var count = visibleRecord;
      for (k = 0; k < visibleRecord; k++) {
        $(this).parents('tr').find(".bsit_show_hide").slice(0, visibleRecord).show();
      }
      var totaltdcount = $(this).parents('tr').find(".bsit_show_hide").length;
      if (totaltdcount > visibleRecord) {
        $(this).next().hide();
      } else {
        $(this).next().hide();
        $(this).hide();
      }
    });
    $(document).on('click', '.seeMoreRecords', function (e) {
      var totaltd = 0
      totaltd = $(this).parents('tr').find(".bsit_show_hide").length;
      $(this).parents('tr').find(".bsit_show_hide").slice(0, totaltd).show();
      $(this).next().show();
      $(this).hide();
    });
    $(document).on('click', '.seeLessRecords', function (e) {
      var totaltd = 0
      totaltd = $(this).parents('tr').find(".bsit_show_hide").length;
      $(this).parents('tr').find(".bsit_show_hide").slice(0, totaltd).hide();
      $(this).prev().show();
      $(this).hide();
      var k = 0;
      // var count = visibleRecord;
      for (k = 0; k < visibleRecord; k++) {
        $(this).parents('tr').find(".bsit_show_hide").slice(0, visibleRecord).show();
      }
    });
  }
};*/

function filterFormValues(form, values) {
  //Get & Set CKeditor value
  let ckeditorFields = $(form).find("textarea.ckeditor");
  $.each(ckeditorFields, function (key, element) {
    let elementName = $(element).attr("name");
    let cke = getCkEditor(elementName, $(form).attr("id"));
    if (cke) {
      let ckdata = cke.getData();
      values[elementName] = (ckdata) ? ckdata : "";
    } else {
      values[elementName] = "";
    }
  });

  //date is not set for the date fields then unset thet fields from form data
  let datepickerField = $(form).find("input.datepicker");
  $.each(datepickerField, function (key, element) {
    let elementName = $(element).attr("name");
    if (!$(element).val()) {
      delete values[elementName];
    }
  });

  //Replace autocomplete advance select box value with its data attribute
  let autocompleteSelectBox = $(form).find("input.autoCompCollAdv");
  $.each(autocompleteSelectBox, function (key, element) {
    let elementName = $(element).attr("name");
    if ($(element).val()) {

      let elementDataId = $(element).attr("data-selectedid");
      values[elementName] = elementDataId;
    } else {
      values[elementName] = "";
    }
  });

  let autoCompCollBox = $(form).find("input.autoCompColl");
  $.each(autoCompCollBox, function (key, element) {
    let elementName = $(element).attr("name");
    if ($(element).val()) {

      let elementDataId = $(element).attr("data-selectedid");
      values[elementName] = elementDataId;
    } else {
      values[elementName] = "";
    }
  });

  return values;
}

function validateFormData(form, formPrefix) {
  let fail = false;
  let name = '';
  form.find('select, textarea, input').each(function () {
    let elementClass = $(this).attr('class');
    if (elementClass && (elementClass.indexOf("autoCompCollAdv") >= 0 || elementClass.indexOf("autoCompColl") >= 0)) {
      //data dirty validation
      if ($(this).attr('data-dirty') == 1 && $(this).val()) {
        fail = true;
        name = $(this).attr('name');
        $(`[name=${name}]`).addClass('required_border_class');
        $(`[name=${name}]`)
          .next('label')
          .addClass('required_label_class');
      }
    }

    if ($(this).prop('required')) {
      if (elementClass && elementClass.indexOf("ckeditor") >= 0) {
        //ckeditor validation
        let name = $(this).attr("name");
        let cke = getCkEditor(name, $(form).attr("id"));
        if (cke) {
          let ckdata = cke.getData();
          if (!ckdata) {
            fail = true;
            $(this).closest("div").find(".ck-editor__editable_inline").addClass("required_border_class")
          }
        }
      } else if (elementClass && (elementClass.indexOf("autoCompCollAdv") >= 0 || elementClass.indexOf("autoCompColl") >= 0)) {
        //collection box validation
        if (!$(this).attr('data-selectedid')) {
          fail = true;
          name = $(this).attr('name');
          $(`[name=${name}]`).addClass('required_border_class');
          $(`[name=${name}]`)
            .next('label')
            .addClass('required_label_class');
        }
      } else if (!$(this).val()) {
        fail = true;
        name = $(this).attr('name');
        $(`[name=${name}]`).addClass('required_border_class');
        $(`[name=${name}]`)
          .next('label')
          .addClass('required_label_class');
      }
    }

    if (formPrefix && formPrefix == "addForm") {
      // [start] CODE FOR Regular expression change

      if ($(this).attr('regex') != '' && $(this).attr('regex') != undefined) {
        var regex = $(this).attr('regex').split('",');
        var regularexp = regex[0].replace('["', '');
        var expmessage = regex[1].replace(/"]|\"/g, '');
        var check_reg = checkregex(regularexp, $(this).val());

        if (check_reg == 0) {
          fail = true;
          name = $(this).attr('name');

          fail_log += name + " is not valid \n";
          $("[name=" + name + "]").addClass('required_border_class');
          $("[name=" + name + "]").next('label').addClass('required_label_class');
          $("[name=" + name + "]").attr('title', expmessage);
        }

      }
      // [END] CODE FOR Regular expression change
    }
  });

  return fail;
}

function resetFormFields(form, formPrefix, frame) {
  form.find('select, textarea, input').each(function () {
    let name = $(this).attr('name');
    $(`[name=${name}]`).removeClass('required_border_class');
    $(`[name=${name}]`)
      .next('label')
      .removeClass('required_label_class');

    //reset dirty flag
    let elementClass = $(this).attr('class');
    if (elementClass && (elementClass.indexOf("autoCompCollAdv") >= 0 || elementClass.indexOf("autoCompColl") >= 0)) {
      $(this).attr('data-dirty', '0');
    }

    //reset ckeditor
    if (elementClass && elementClass.indexOf("ckeditor") >= 0) {
      //ckeditor validation
      let name = $(this).attr("name");
      let cke = getCkEditor(name, $(form).attr("id"));
      if (cke) {
        $(this).closest("div").find(".ck-editor__editable_inline").removeClass("required_border_class");
        cke.setData('');
      }
    }

    if (formPrefix == "addForm" || formPrefix == "editForm") {
      $('.modal').modal('hide');
    } else if (formPrefix == "addFormside") {
      $(`#addModel${frame.PFID}`).removeClass('open');
      $('body').removeClass('open-bg');
    } else if (formPrefix == "editsideModel") {
      $(`#editModel${frame.PFID}`).removeClass('open');
      $('body').removeClass('open-bg');
    }
  });
}

function showLoader(event, loader, disabledBtn) {
  //show loader
  if (loader) {
    $('.loader').show();

    //add new loader in count | if one ajax complete but second request is pending then it will help
    let loadcount = parseInt($(".loader").attr("loadcount"));
    if (loadcount) {
      loadcount += 1;
    } else {
      loadcount = 1;
    }

    $(".loader").attr("loadcount", loadcount);
    
  }

  //Disabled Save/Edit Button
  if (event && disabledBtn) {
    event.target.disabled = true;
  }
}

function hideLoader(event) {
  let loadcount = parseInt($(".loader").attr("loadcount"));
  if (loadcount) {
    loadcount -= 1;
  } else {
    loadcount = 0;
  }

  if (loadcount == 0) {
    $('.loader').hide();
  }

  $(".loader").attr("loadcount", loadcount);
  
  if (event) {
    event.target.disabled = false;
  }
}




/**
    * getPageByName
    * It will use when open popup for getting documents data
    * get page Id from Page Name
    * Added by Niru [28-06-2021]
*/

function getPageByName(name) {
  
   // bsit_pages collection collID= 3
let requestData = {
      data: JSON.stringify([
        {
          collID: 3,
          filter: `pg.title = '${name}'`,
        },
      ]),
    };

  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      url: base_url + 'core/Apilocal/loadfiltered',
      async: true,
      data: requestData,
      dataType: 'json',
      success: function (res) {
        if (res.session_expire == 1) {

          window.location = base_url;
          return false;
        }


        let pageId = res[0]['Results'][0].pageId;
        resolve(pageId);
      },
      error: function (err) {
        reject(err);
      },
    });
  });
}



/**
    * getPageFrameIDByName
    * It will use when open popup for getting documents data
    * get page Frame Id from Page ID
    * Added by Niru [28-06-2021]
*/

function getPageFrameIDByName(pageId) {
  
   // bsit_page_frame collection collID= 11
let requestData = {
      data: JSON.stringify([
        {
          collID: 11,
          filter: `pf.page_id = ${pageId}`,
        },
      ]),
    };

  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      url: base_url + 'core/Apilocal/loadfiltered',
      async: true,
      data: requestData,
      dataType: 'json',
      success: function (res) {
        if (res.session_expire == 1) {
          window.location = base_url;
          return false;
        }


        let PageFrameID = res[0]['Results'][0].PageFrameID;
        resolve(PageFrameID);
      },
      error: function (err) {
        reject(err);
      },
    });
  });
}

export {
  applyFilter,
  advCollectionFocus,
  advCollectionSource,
  advMultiCollectionSource,
  advMultiCollectionFocus,
  advMultiCollectionSourceNote,
  autoCompCollFocus,
  autoCompCollSource,
  commitDelete,
  commitEdit,
  //commitEditSidebar,
  commitSave,
  //commitSidebarSave,
  commitMultiEditDelete,
  filterFieldChange,
  getCollectionIDByName,
  getFrameData,
  paginationClick,
  removeFilter,
  rowCountChange,
  setupFramePagination,
  showEditDeleteModal,
  //showDeleteModalSidebar,
  //showEditSideBar,
  showMultiEditDeleteModal,
  //showMultiDeleteModal,
  showMultiEditDeleteModalSidebar,
  showMultiDeleteModalSidebar,
  sortHeaderClick,
  pagerefrace,
  showHideQuickFilter,
  getQuickFilterValues,
  performFilter,
  notificationUpdate,
  notificationRedirectUpdate,
  // seeMoreRecords_seeLessRecords,
  setValueOfAdvanceAutoComplete,
  setValueOfAutoComplete,
  resetFormFields,
  showLoader,
  hideLoader,
  getPageByName,
  getPageFrameIDByName
};