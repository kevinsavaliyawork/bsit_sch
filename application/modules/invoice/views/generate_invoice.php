<div id="InvoiceMainArea" ><!-- class="mui-panel">-->
  <h2> Generate Invoice</h2>

  <?php
	if($curlDataInvoice[0]->InvoiceId == '') {
		$curlDataInvoice[0]->InvoiceId==0;
	}
	?>
  <div class="alert alert-success invoice_msg_success" role="alert" style="display:none;" >
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<i class="mdi-navigation-check"></i>
	<strong>Done!</strong> Invoice Generated Successfully.
  </div>

  <div id="InvoiceBody" class="mui-panel">
	<div id="addForm_invoice" >
      <div class="mui-textfield" tabindex="-1">
        <label>From Date</label>
        <input name="StartDate" id="startDateSelector" class="datepicker" tabindex="0">
      </div>
	  <div class="mui-textfield" tabindex="-1">
        <label>To Date</label>
        <input name="EndDate" id="endDateSelector" class="datepicker" tabindex="0">
      </div>
	  <div class="mui-select1" tabindex="-1">
		<label>Customer</label><br/>
		<select name="orgsId" id="customerId" tabindex="0" class="js-example-basic-multiple" multiple="multiple">
		  <?php foreach ($curlDataCust as $cust): ?>
		  <option value="<?php echo($cust->_id)?>"> <?php echo($cust->name)?> </option>
		  <?php endforeach; ?>
		</select>
	  </div>
	  <input type="hidden" id="last_inv_id" value="<?php echo $curlDataInvoice[0]->InvoiceId; ?>">

	  <div style="text-align:right;">
		<button id="generate_invoice_button" class="mui-btn mui-btn--flat mui-btn--primary">Generate</button>
	  </div>
    </div>
  </div>
</div>
<!-- container -->
</div>
    </div>
  </body>
</html>
<style>
.select2 {width:50%}
</style>
<script type="text/javascript">

jQuery(function($) {
	$('.invoice_msg_error').css('display','none');
	$('.invoice_msg_success').css('display','none');
	$("#generate_invoice_button").click(function() {
		var start_date 	= $("#startDateSelector").val();
		var end_date 	= $("#endDateSelector").val();
		/*var last_inv_id = $("#last_inv_id").val();*/

		if(start_date > end_date)
		{
			$("#endDateSelector").css('border-color',"red");
			return false;
		}

		/*
    var get_due_date = new Date();
		get_due_date.setDate(get_due_date.getDate() + 30);
		var get_due_date_Formated = get_due_date.toISOString().substr(0,10);
    */

		var data_array1;
		var $el=$("#customerId");
    var NumElements = $el.find('option:selected').length;
    var NumCompleteSuccess = 0;
    var NumCompleteFailure = 0;
		$el.find('option:selected').each(function(){
$('.loader').show();
			var generate_invoice_data = {"parameters":[{"key":"@start_date","value":start_date},{"key":"@end_date","value":end_date},{"key":"@cust_id","value":$(this).val()}],"sp_name":[{"key":"BSIT_GenerateInvoice"}]};
			$.ajax({
				type: "POST",
				url: base_url+"core/Apilocal/Call_SP",
				data:generate_invoice_data,
				dataType: 'json',
				success: function(returnData){
					$('.loader').hide();
					if(returnData == 1)
					{
            NumCompleteSuccess = NumCompleteSuccess+1;
					}else{
            NumCompleteFailure = NumCompleteFailure+1;
						//$('.invoice_msg_error').css('display','block');
					}
          if((NumCompleteSuccess+NumCompleteFailure)>=NumElements){
            $('.invoice_msg_success').show();
          }
				}
			});
		});
	});

	// ------------------------
	// -- DATE TIME PICKERS
	// ------------------------
	$('.datepicker').pickadate({
		format:			'yyyy/mm/dd',
		formatSubmit: 	'yyyy/mm/dd',
		startDate: '-3d'
	});


	// ----------------------------
	// -- MULTI-SELECT DROPDOWN
	// ----------------------------
	//$(".js-example-basic-multiple").select2();

	// ----------------------------
	// -- LOADER
	// ----------------------------
	/*var loading = $(".loader");
	loading.hide();
	$(document).ajaxStart(function () {
		loading.show();
	});

	$(document).ajaxStop(function () {
		loading.hide();
	});*/

});
</script>
