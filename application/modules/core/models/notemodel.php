<?php

class notemodel extends brain_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('core/Bsit_io', 'API');
        $this->load->library('session');
    }

    public function updateNotes($param)
    {
        $_DATAPOST['parameters'] = [
            'key' => 'update_notes',
            'filters' => [
                [
                    'key' => 'note_id',
                    'value' => $param['note_id'],
                ],
                [
                    'key' => 'note',
                    'value' => $param['note'],
                ],
                [
                    'key' => 'note_type',
                    'value' => $param['note_type'],
                ],
                [
                    'key' => 'note_header',
                    'value' => $param['note_header'],
                ],
                [
                    'key' => 'notify_users',
                    'value' => $param['notify_users'],
                ]
            ]
        ];

        $data_parameters = json_encode($_DATAPOST['parameters']);
        $this->BASEURLMETHOD = API_BASEURL . API_METHOD_GET;
        $json_page_result = $this->API->CallAPI("GET", $this->BASEURLMETHOD, $data_parameters, false);
        $page_result = json_decode($json_page_result);


        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : UPDATE_FAIL;
            echo $error_message;
            return;
        }

        return json_encode($page_result->Data);
    }
}
