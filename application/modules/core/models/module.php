<?php

class module extends brain_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('core/Bsit_io', 'API');
	}

	public function getClientActiveModules($userID = 56)
	{
		try {

			$CURLDATA 	= 	array('collections' =>
			json_encode(
				array(
					'colls' => array(
						array(
							'coll_name'	=> 'group_modules',
							'filter'	=> "m.status = '1' AND m.display_in_menu = '1' AND u._id = '" . $userID . "'",
							'order'		=> 'm.ordering ASC',
							'page_num'	=> '0'
						)
					)
				)
			));
			$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
			$page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
			$page_result = json_decode($page_result);
			if (isset($page_result->Data)) {
				$page_result = $page_result->Data;
			}
			/* echo "<pre>";
			print_r($CURLDATA);
			print_r($page_result);
			echo "</pre>"; */

			if (isset($page_result[0]->Results) && !empty($page_result[0]->Results)) {
				$result = (array)$page_result[0]->Results;
			}

			if ($result) {
				return $result;
			} else {
				return 0;
			}
		} catch (Exception $e) {
			return 0;
		}
	}
	public function getClientActiveModulesPermission($userID = 56)
	{
		try {

			$CURLDATA 	= 	array('collections' =>
			json_encode(
				array(
					'colls' => array(
						array(
							'coll_name'	=> 'group_modules',
							'filter'	=> "m.status = '1' AND u._id = '" . $userID . "'",
							'order'		=> 'm.ordering ASC',
							'page_num'	=> '0'
						)
					)
				)
			));
			$BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

			$page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
			$page_result = json_decode($page_result);
			if (isset($page_result->Data)) {
				$page_result = $page_result->Data;
			}

			if (isset($page_result[0]->Results) && !empty($page_result[0]->Results)) {
				$result = (array)$page_result[0]->Results;
			}



			if ($result) {
				return $result;
			} else {
				return 0;
			}
		} catch (Exception $e) {
			return 0;
		}
	}
}
