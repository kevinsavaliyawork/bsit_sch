<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ScheduleModel extends brain_Model
{
	function __construct()
    {
        parent::__construct();
        $this->load->model('core/Bsit_io', 'API');
        $this->load->library('session');
        // $this->_BaseUrlMethod = API_BASEURL . API_METHOD_GET;
    }

	public function getHoursToSchedule($date) {
        $param = $this->input->post();
        if (empty($param)) {
            echo json_encode(["message"=>"Invalid Request."]);
            exit;
        }//exit for empty data

        $_DATAPOST['parameters'] = [
            'key' => 'getHoursToSchedule',
            'filters' => [
                [
                    'key' => 'date',
                    'value' => $date
                ]
            ]
        ];
        
        $data_parameters = json_encode($_DATAPOST['parameters']);
        $this->BASEURLMETHOD = API_BASEURL . API_METHOD_GET;
        $json_page_result = $this->API->CallAPI("GET", $this->BASEURLMETHOD, $data_parameters, false);
		
        return $json_page_result;

        // if (isset($page_result->HasError) && $page_result->HasError == 1) {
        //     $load_error = true;
        //     $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : UPDATE_FAIL;
        //     echo $error_message;
        //     return;
        // }
        // echo  json_encode($page_result->Data);
    }
}