<?PHP

// clean a string
 function cleanData(&$str)
 {
		if($str == 't') $str = 'TRUE';
		if($str == 'f') $str = 'FALSE';
		if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
		  $str = "'$str";
		}
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
 }	

// filename for download
$filename = "bsit_io_" . date('Ymd') . ".csv";

header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-Type: text/csv");

$out = fopen("php://output", 'w');

// first we loop headers row only, 
if(!empty($analysis_data[0])){
	$kcnt = 0;
	 
	// display field/column names as first row
	fputcsv($out, array_keys($analysis_data[0]), ',', '"');
		
	foreach($analysis_data[0] as $key=>$row){
		// add to the list of keys
		$keys[] = $key;
		$kcnt++;
	}

	// setup rowcount variable
	$rowCounter = 1;
	// loop each row and output table
	foreach($analysis_data as $row){
		fputcsv($out, array_values($row), ',', '"');
	}
}

fclose($out);
exit;
?>