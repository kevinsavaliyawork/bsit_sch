<?php
defined('BASEPATH') or exit('No direct script access allowed');

class login extends brain_controller
{
    public function __construct()
    {
        parent::__construct(1);


        $this->load->helper(array('form', 'url', 'date', 'mystring', 'cookie'));
        $this->load->library('form_validation');
        $this->form_validation->CI = &$this;


        $this->load->library('session');
        $this->load->model('loginmodel');

        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function index()
    {

        $page_result_sys_api = $this->loginmodel->getSystemData();



        foreach ($page_result_sys_api[0]->Results as $key => $sysValue) {

            if ($sysValue->paramname == SYS_APPLICATION_TITLE) {
                $data['appTitle'] = $sysValue->paramvalue;
                $this->session->set_userdata('appTitle', $sysValue->paramvalue);
            } elseif ($sysValue->paramname == SYS_APPLICATION_ICON) {
                $data['appIcon'] = $sysValue->paramvalue;
                $this->session->set_userdata('appIcon', $sysValue->paramvalue);
            } elseif ($sysValue->paramname == SYS_LOGIN_ICON) {
                $data['loginIcon'] = $sysValue->paramvalue;
                $this->session->set_userdata('loginIcon', $sysValue->paramvalue);
            } elseif ($sysValue->paramname == SYS_CSS_OVERRIDE) {
                $data['cssOverride'] = $sysValue->paramvalue;
                $this->session->set_userdata('cssOverride', $sysValue->paramvalue);
            }
        }

        // if the users is already logged in, redirect the user to their home page
        if ($this->session->userdata('logged_in')) {
            $default_home = $this->session->userdata('DefaultHome');
            redirect($default_home, 'refresh');
        }

        // Validate the username and pass entered are valid
        // NOTE: the last part of this validation rule 'callback_check_database' will run the
        // custom function 'check_database' to verify the username and password are correct.
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

        if ($this->form_validation->run($this) === false) {
            $this->load->view('login', $data);
        } else {
            $uri = $this->input->post('redirect');
            if (isset($_POST['rememberme'])) {
                $cookie = array(
                    'name'   => 'firstuser',
                    'value'  => $_POST['username'],
                    'expire' => 86500000,
                    'secure' => false
                );

                $this->input->set_cookie($cookie);
            }

            $default_home = $this->session->userdata('DefaultHome');
            $userID = $this->session->userdata('UserID');
            $loggedIn = $this->session->userdata('logged_in');

            if ($uri) {
                redirect('/' . $uri);
            } else {
                redirect($default_home);
            }
        }
    }

    public function check_database($password)
    {

        $key = pack("H*", "0123456789abcdef0123456789abcdef");
        $iv = pack("H*", "abcdef9876543210abcdef9876543210");
        $encrypted = base64_decode(trim($this->input->post('password')));
        $password = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv), "\0..\32");


        $username = $this->input->post('username');


        $page_result = $this->loginmodel->getUserDetail($username);



        if (isset($page_result[0]->Results) && !empty($page_result[0]->Results)) {
            if ($page_result[0]->Results[0]->is_active == 0) {
                $reason = 'Invalid Authentication';
                $this->form_validation->set_message('check_database', $reason);
                return false;
            }

            $user = (array)$page_result[0]->Results[0];

            $password_correct_db = $user['Password'];
            $params = array(8, false);

            $check =  $this->CheckPassword($password, $password_correct_db);

            if ($check) {
                if ($user) {

                    // Create a new instance of the user class and setup login etc
                    // set all the user specific values with the 'logged in' status and users group info
                    foreach ($user as $key => $value) {
                        $this->session->set_userdata($key, $value);
                    }

                    $this->session->set_userdata('logged_in', true);
                    $this->session->set_userdata('today_date', date('Y-m-d'));
                    return true;
                } else {
                    $reason = 'Invalid Access';
                    $this->form_validation->set_message('check_database', $reason);
                    return false;
                }
                return true;
            } else {
                $reason = 'Invalid Username/Password';
                $this->form_validation->set_message('check_database', $reason);
                return false;
            }
        } else {
            $reason = 'Invalid Authentication';
            $this->form_validation->set_message('check_database', $reason);
            return false;
        }
    }

    public function logout()
    {
        // It's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }

        // unset the session variables
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('group_id');
        unset($_SESSION['cart_items']);
        $this->session->sess_destroy();
        unset($_SESSION);
        redirect('login', 'refresh');
    }

    public function CheckPassword($password_input, $password_db)
    {

        if (crypt($password_input, $password_db) == $password_db) {
            return true;
        } else {
            return false;
        }
    }

    public function encrypt_password()
    {

        $postdata = json_decode($_POST['data']);

        $new_password = $postdata[0]->password;
        $user_id = $postdata[0]->user_id;


        $page_result = $this->loginmodel->getUserCollectionData($user_id);
        // TODO: Remove hardcoded collection


        $old_pass = $page_result[0]->Results[0]->Password;
        if ($old_pass == $new_password) {
            echo $new_password;
        } else {
            $blowfish_salt = "$2y$15$" . bin2hex(openssl_random_pseudo_bytes(22));
            echo $newpass = crypt($new_password, $blowfish_salt);
        }
    }
}
