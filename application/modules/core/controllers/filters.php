<?php
defined('BASEPATH') or exit('No direct script access allowed');

class filters extends brain_controller
{
    public function __construct()
    {
        parent::__construct(0);
        $this->load->model('module');
        $this->load->model('analysismodel');
    }

    public function index()
    {
        // check if the user is 'logged in', if not, redirect to login page.
        $userID = $this->session->userdata('UserID');
        $loggedIn = $this->session->userdata('logged_in');
        if ($loggedIn == false or $userID == "") {
            return;
        }

        // get the search term requested
        $searchTerm = '';
        if (isset($_GET['searchTerm'])) {
            $searchTerm = $_GET['searchTerm'];
        }

        $collection_id = null;
        if (isset($_GET['collection_id'])) {
            $collection_id = $_GET['collection_id'];
        }

        // perform the search against the database
        $results = $this->analysismodel->lookupSearchTerm($searchTerm, $userID, $collection_id);

        // output results as json type
        header('Content-Type: application/json');
        echo json_encode($results);
    }

    // This function will search the DB and convert a DIM display name back in a DIM Value
    public function getDimValue()
    {
        // check if the user is 'logged in', if not, redirect to login page.
        $userID = $this->session->userdata('UserID');
        $loggedIn = $this->session->userdata('logged_in');
        if ($loggedIn == false or $userID == "") {
            return;
        }

        // get the search term and dim ID
        $searchTerm = '';
        $dimID = '';
        if (isset($_GET['searchTerm'])) {
            $searchTerm = htmlspecialchars($_GET['searchTerm']);
        }
        if (isset($_GET['dimID'])) {
            $dimID = $_GET['dimID'];
        }

        // perform the search against the database
        $results = $this->analysismodel->convDimTextToValue($searchTerm, $dimID);

        // output results as json type
        header('Content-Type: application/json');
        echo json_encode($results[0]);
    }
}
