<!--
#id_WS_Projected
#repWrittenSales_container
#repWrittenSalesPercChart
#id_WS_FooterVal1
#id_WS_FooterVal2
-->

<div class="chart_main" id="repWrittenSalesMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Written Sales</div>
  </div>

  <!-- body -->
  <div class="chart_body" id="repWrittenSalesBody_container">
      <!-- LEFT -->
      <div class="chart_metric">
        <div class="chart_primary">
          <span class="chart_primary_title">Projected:</span>
          <span class="chart_primary_value" id="id_WS_Projected">$#</span>
        </div>
      </div>

      <!-- RIGHT -->
      <div class="chart_hero" id="repWrittenSales_container" style="height:400px; max-height:150px">
        <canvas id="repWrittenSalesPercChart" style="width:100%; height:100%;"></canvas>
      </div>
  </div>

  <!-- Foooter -->
  <div class="chart_footer">
    <!-- LEFT -->
    <div class="chart_foot_1st">
      <span class="chart_foot_1st_title" >LY:</span>
      <span class="chart_foot_1st_value" id="id_WS_FooterVal1">#%</span>
    </div>

    <!-- RIGHT -->
    <div class="chart_foot_2nd">
      <span class="chart_foot_2nd_title">LY:</span>
      <span class="chart_foot_2nd_value" id="id_WS_FooterVal2">$#</span>
    </div>
  </div>

</div>
