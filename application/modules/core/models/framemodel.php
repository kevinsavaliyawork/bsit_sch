<?php

class framemodel extends brain_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('core/Bsit_io', 'API');
        $this->load->library('session');
    }


    //load frame data
    public function loadFrameData($collection, $sorting, $filtering_all, $page_number, $number_of_records)
    {

        $CURLDATA_load_data = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_id' => $collection,
                'order' => $sorting,
                'filter' => $filtering_all,
                'pagenum' => $page_number,
                'numrec' => $number_of_records
            )))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $frame_result_load_data = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_load_data);

        $frame_result_load_data = json_decode($frame_result_load_data);

        return $frame_result_load_data;
    }

    // fetch page frame properties
    public function getPageFrameProperties($pageId, $frameId)
    {

        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_name' => 'page_frame_properties',
                'filter' => "p._id = " . $pageId . " AND pf._id = " . $frameId,
                'order' => 'pf.[order], fp.[order]',
                'page_num' => '0',
                'limit' => ''
            )))
        ));

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $page_frame_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);


        $page_frame_result = json_decode($page_frame_result);
        if (isset($page_frame_result->Data)) {
            $page_frame_result = $page_frame_result->Data;
        }

        return $page_frame_result;
    }


    //fetch permement filter data.
    public function fetchPermanentFilter($PFID)
    {


        // fetch the filter data from the table.
        $CURLDATA_PF = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_name' => 'permanent_filters',
                'filter' => "fltr.page_frame_id = " . $PFID .
                    " AND (group_id = " . $this->session->userdata('GroupID') .
                    " OR group_id='' OR group_id IS NULL OR group_id = 0)"
            )))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $result_permanent_filter = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_PF);
        $result_permanent_filter = json_decode($result_permanent_filter);
        if (isset($result_permanent_filter->Data)) {
            $result_permanent_filter = $result_permanent_filter->Data;
        }

        return $result_permanent_filter;
    }

    //fetch history data.
    public function fetchHistory($collection, $PFID, $actual_curr_url)
    {

        $CURLDATA_history_api = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_name' => 'navigation_history',
                'filter' => "habb.coll_id = " . $collection . " AND habb.frame_id = " . $PFID . " AND habb.user_id = " . $this->session->userdata('UserID') . " AND habb.history_url = '" . $actual_curr_url . "' AND habb.apply_date = '" . date('Y-m-d') . "'",
                'order' => 'habb.[_id] desc',
            )))
        ));

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $page_result_history_api = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_history_api);
        $page_result_history_api = json_decode($page_result_history_api);

        if (isset($page_result_history_api->Data)) {
            $page_result_history_api = $page_result_history_api->Data;
        }

        return $page_result_history_api;
    }

    //fetch frame permission  data.
    public function fetchFramePermission($frameID)
    {

        // Getting data for Group Permission for Particular Frame
        $CURLDATA_fper = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_name' => 'frame_permissions',
                'filter' => "fper.group_id =" . $this->session->userdata('GroupID') . " AND fper.frame_id =" . $frameID,
                'order' => 'fper.[_id] desc',
                'page_num' => '0'
            )))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $page_result_group_permission_data = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_fper);
        $page_result_group_permission_data = json_decode($page_result_group_permission_data);
        if (isset($page_result_group_permission_data->Data)) {
            $page_result_group_permission_data = $page_result_group_permission_data->Data;
        }

        return $page_result_group_permission_data;
    }
}
