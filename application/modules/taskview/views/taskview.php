<?php


    $job_data = $json_result->Data[0];
    $note_data = $json_result->Data[1];
    $user_data = $json_result->Data[2];

    // echo('<pre>');
    //     print_r($user_data->Results);
    // echo('</pre>');

    // Setup the HTML structure and plugin data where needed
    $html = '<div id="main_content_part" class="main_content_part_class mui-col-md-12 mui-col-md-offset-0">';
    $html .= '<!-- test >> --><div class="edit-pointer-cancel table-responsive mui-panel">';
	$html .= '<div id="page_frame_'.$frame['PFID'].'_content" style="height:100%;" >';

    $html .= '<h2>'.$job_data->Results[0]->title.'</h2>';
    // If the job has nobody assigned to it,
    // show the select list to assign someone to it.
    if(is_null($job_data->Results[0]->assigned_to))
    {
        // ADD DIVS TO ALL HEADERS AND DISPLAY BLOCK CLEAR BOTH
        // $html .= '<div> <div class="task-header"><h6>Assign To: </h6>';
        $html .= '<div><div class="task-header">Assign To: </div>';
        $html .= '<div class="mui-select mui-col-md-12"><select id="assigneeSelect">';
        foreach ($user_data->Results as $user):
            $selected = "";
            if ($userid==$user->key_id) {
                $selected = " selected ";
            }
            $html .= '<option value="'.$user->key_id.'"'.$selected.'> '.$user->FirstName.' '.$user->LastName.' - '.$user->Nickname.' </option>';
        endforeach;
        $html .= '</select></div>';
        $html .= '<div class="task-header"><button id="assignBtn" class="mui-btn mui-btn--primary" style="float: right; margin-top: 0px;">ASSIGN</button></div>';
        $html .= '</div>';
    } else { // Otherwise show who is assigned to the job.

        // need to filter the users list to whoever is assigned to the
        // job to return the username/name etc.
        $assignedId = $job_data->Results[0]->assigned_to;
        $assigned = array_filter(
            $user_data->Results,
            function ($e) use (&$assignedId) {
                return $e->key_id == $assignedId;
            }
        );
        // reset($assigned) is used to get the one and only username.
        $html .= '<div class="task-header">Assigned To: '.reset($assigned)->FirstName.' '.reset($assigned)->LastName.'</div>';
        $html .= '<button id="assignBtn" class="mui-btn mui-btn--primary" style="float: right; margin-top: 0px;">UNASSIGN</button>';
        $html .= '</div>';
    }
    $html .= '<div class="task-header">Timeline:</div>';
    $html .= '<div class="task-header">Priority: '.$job_data->Results[0]->Priority.'</div>';
    $html .= '<div class="task-header">Status: '.$job_data->Results[0]->Status.'</div>';

    $html .= '<h4 style="margin-bottom: 0px;">Notes</h4>';
    $html .= '<div class="mui-divider"></div>';
    $html .= '<form class="mui-form">';
        $html .= '<div class="mui-textfield"><input id="newNote" type="text" placeholder="New note..." data-JobId="'.$jobID.'" data-UserId="'.$userid.'"></div>';
    $html .= '</form>';
    $html .= '<div id="notesContainer" class="mui-panel mui-col-md-12" style="background-color: #ceedfb">';
    for($i=0;$i<count($note_data->Results);$i++){
        $noteData = $note_data->Results[$i]->Note;
        if(preg_match($reg_exUrl, $noteData, $url)) {
            // make the urls hyper links
            $noteData = preg_replace($reg_exUrl, '<a href="'.$url[0].'" rel="nofollow">'.$url[0].'</a>', $noteData);
        }
        $html .= '<div class="mui-panel mui-col-md-12"> <span style="color: #C5C5C5;"
         class="mui--divider-right">&nbsp;'.$note_data->Results[$i]->username.' | '.$note_data->Results[$i]->create_date.'&nbsp;</span> '.$noteData.'</div>';
    }
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div><!-- << subhead -->';
    $html .= '</div>';

    // echo('<pre>');
    // print_r($user_data);
    // echo('</pre>');
    echo $html;
?>
