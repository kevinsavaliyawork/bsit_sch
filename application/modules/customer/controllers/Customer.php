<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Customer extends brain_controller {

	function __construct() {
    parent::__construct(1);
    // Load the needed librarys
    $this->load->model('core/Bsit_io','API');
    $this->load->library('session');
    $this->load->model('customer/Customermodel');
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	}

	

  public function customerOverview($data_pass)
  {
    $data_pass=[];
    $get_customer_detail = $this->Customermodel->getCustomerDetail($_GET["_id"]);
    $data_pass['customer'] = json_decode($get_customer_detail)->Data;
    $opportunityDetails = $data_pass['load_data'][0];
    
    $data_pass['opportunityDetails'] = $opportunityDetails;
    
    // activity data
    $get_activity_history_detail = $this->Customermodel->getActivityHistoryDetail($_GET["_id"]);
    $data_pass['activity_history'] = json_decode($get_activity_history_detail);
    // print_r($data_pass['activity_history']); exit;
    return $this->load->view('customer/customer_overview',$data_pass,true);   
  }
  
    /**
    * Get activity history
    * It will use when user submit any note then we will fetch 
    * all activity using ajax and update activity notes
    * Added by Niru [21-6-2021]
    */
    public function fetchLatestNotesHistoryList()
    {
        $get_activity_history_detail = $this->Customermodel->getActivityHistoryDetail($_POST["_id"]);
        $data_pass['activity_history'] = json_decode($get_activity_history_detail);
        echo $this->load->view('customer/customer_notes_list', $data_pass, true);
        exit;
    }

    /**
    * Get opprtinity over data
    * It will use when user edit then we will fetch 
    * fetch all Opportunity Customer All Data
    * Added by Niru [21-6-2021]
    */

    public function fetchCustomerAllData()
    {
        $get_customer_detail = $this->Customermodel->getCustomerDetail($_POST["_id"]);
        $data_pass['customer'] = json_decode($get_customer_detail)->Data;
        $data_pass['_id'] = $_POST["_id"];
        echo $this->load->view('customer/Overview', $data_pass, true);
        exit;
    }

   public function customerProjectsTask($data_pass)
      {
        $data_pass=[];
        $get_projects_detail = $this->Customermodel->getCustomerProjectsDetail($_GET["_id"]);
        $data_pass['projects'] = json_decode($get_projects_detail)->Data;
       
        // print_r($data_pass['activity_history']); exit;
        return $this->load->view('customer/customer_projectList',$data_pass,true);   
      }

          

  public function customerSupportTask($data_pass)
      {
        $data_pass=[];
        $get_projects_detail = $this->Customermodel->getcustomerSupportTaskDetail($_GET["_id"]);
        $data_pass['projects'] = json_decode($get_projects_detail)->Data;
        
        // print_r($data_pass['activity_history']); exit;
        return $this->load->view('customer/customer_supportList',$data_pass,true);   
      }

}
