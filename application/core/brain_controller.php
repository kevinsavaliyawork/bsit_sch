<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";
 
/**
 * Description of brain_controller
 *
 * @author https://roytuts.com
 */
class brain_controller extends MX_Controller {
 
 function __construct($login) {

        parent::__construct();

        if($login == 0) {

	        	$this->load->library('session');

				error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

				// check if the user is 'logged in', if not, redirect to login page.
				$user_id = $this->session->userdata('UserID');
				$logged_in = $this->session->userdata('logged_in');
				$is_ajax = $this->input->is_ajax_request();


				if($logged_in == FALSE OR $user_id == "" )
				{
					// redirect to the login page
					if($is_ajax == 1){
					$return  = array('session_expire'=>1); 
					echo json_encode($return);
						exit;
					} else{
						redirect('/login');
					}	
				}

        }

        


      
        if (version_compare(CI_VERSION, '2.1.0', '<')) {
            $this->load->library('security');
        }
    }
 
}
 
/* End of file brain_controller.php */
/* Location: ./application/core/brain_controller.php */