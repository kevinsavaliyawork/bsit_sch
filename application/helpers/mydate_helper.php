<?php

function mysql_date($date, $delimiter = '/')
{
    $date = explode($delimiter, $date);
    $day = $date[0];
    $month = $date[1];
    $yr = $date[2];

    return $yr.'-'.$month.'-'.$day;
}
