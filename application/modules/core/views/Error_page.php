<div class="brain_error">
	<div class="container error-block table-responsive mui-panel">
    	<div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
            	<div class="right-content col-lg-12">	
					<div class="col-lg-3">
						<div class="img-block-icon1"><img src="<?php echo base_url();?>assets/core/images/green.png" /> </div>
					</div>
					<div class="col-lg-9 ">
						<p class="error_content"><b>Error message :</b> You are not Authorized to Perform this Action</p>
						<a href="<?php echo SITEURL.$this->session->userdata('DefaultHome') ?>" class="mui-btn mui-btn--primary">Back To Home</a>
					</div>
                </div>
             </div>
        </div>
	</div>
</div>
