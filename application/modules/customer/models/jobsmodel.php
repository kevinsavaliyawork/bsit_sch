<?php

class jobsmodel extends brain_Model
{
    /**
     * @var string $_BaseUrlMethod it holds the url to the API
     */
    private $_BaseUrlMethod;

    function __construct()
    {
        parent::__construct();
        $this->load->model('core/Bsit_io', 'API');
        $this->load->library('session');
        // $this->_BaseUrlMethod = API_BASEURL . API_METHOD_GET;
    }



    public function getCustomerDetail($Opportunity_id){
        
      $_DATAPOST['parameters'] = array (
          'key' => 'get_customer_by_opportunity_id',
          'filters' => 
            array(
                array (
                  'key' => 'Opportunity_id',
                  'value' => $Opportunity_id,
                ),
                ),
            );

      $data_parameters = json_encode($_DATAPOST['parameters']);
      $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
      $get_customer_detail = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
      // echo "<pre/>";print_r($get_customer_detail);     exit;
      return $get_customer_detail;
    }

  
}
