<?php

class collectionmodel extends brain_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('core/Bsit_io', 'API');
        $this->load->library('session');
    }

    //Get Collection Data
    public function getCollectionDataOfId($collId)
    {

        $CURLDATA_COLL = array('collections' => json_encode(
            array('colls' => array(
                array(
                    'coll_name' => 'data_collections',
                    'filter' => "coll._id = '" . $collId . "'"
                ),
            ))
        ));

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $json_page_result_COLL = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_COLL);

        $json_page_result_COLL = json_decode($json_page_result_COLL);

        if (isset($json_page_result_COLL->Data)) {
            $json_page_result_COLL = $json_page_result_COLL->Data;
        }

        return $json_page_result_COLL;
    }


    //Get Collection Tables and Properties
    public function getCollectiontablesAndProperties($collId)
    {

        $_POST['parameters'] = array(array(
            'key' => 'collectionId',
            'value' => $collId
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_getcollectiontablesandproperties&filters=' . $data_parameters;

        $all_default_result = $this->API->CallAPI("GET", $this->BASEURL);

        $page_result_data = json_decode($all_default_result);

        if (isset($page_result_data->Data)) {
            $page_result_data = $page_result_data->Data;
        }

        return $page_result_data;
    }


    //Get Collection Data by name.
    public function getCollectionDataByName($collName)
    {

        $CURLDATA_COLL = array('collections' => json_encode(
            array('colls' => array(
                array(
                    'coll_name' => 'data_collections',
                    'filter' => "coll.name = '" . $collName . "'"
                ),
            ))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $json_page_result_COLL = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_COLL);

        $json_page_result_COLL = json_decode($json_page_result_COLL);

        if (isset($json_page_result_COLL->Data)) {
            $json_page_result_COLL = $json_page_result_COLL->Data;
        }

        return $json_page_result_COLL;
    }

    //Get Collection tables
    public function getCollectionTables()
    {

        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(
                array(

                    'coll_name' => 'get_sql_tables',
                    'order' => 'gst.name'
                )
            ))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

        return $json_page_result;
    }


    //Add Collections
    public function addCollection($collectionName)
    {

        $_POST['parameters'] = array(array(
            'key' => 'name',
            'value' => $collectionName
        ), array(
            'key' => 'userId',
            'value' => $this->session->userdata('UserID')
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_addcollection&filters=' . $data_parameters;

        $json_page_resultCollection = $this->API->CallAPI("GET", $this->BASEURL);

        $page_result_data = json_decode($json_page_resultCollection);
        if (isset($page_result_data->Data)) {
            $page_result = $page_result_data->Data;
        }

        return $page_result;
    }


    //Add Page Frame
    public function addPageFrame($pageId, $collectionId)
    {

        $_POST['parameters'] = array(array(
            'key' => 'pageId',
            'value' => $pageId
        ), array(
            'key' => 'collection_id',
            'value' => $collectionId
        ), array(
            'key' => 'userId',
            'value' => $this->session->userdata('UserID')
        ), array(
            'key' => 'name',
            'value' => 'frame' . date("dmYHi")
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_addpageframe&filters=' . $data_parameters;

        $json_page_resultAll = $this->API->CallAPI("GET", $this->BASEURL);

        return $json_page_resultAll;
    }


    //Add Table Collection
    public function addTableCollection($collectionId, $tableName)
    {

        $_POST['parameters'] = array(array(
            'key' => 'collection_id',
            'value' => $collectionId
        ), array(
            'key' => 'name',
            'value' => $tableName
        ), array(
            'key' => 'userId',
            'value' => $this->session->userdata('UserID')
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_addtabletocollection&filters=' . $data_parameters;

        $json_page_resultAll = $this->API->CallAPI("GET", $this->BASEURL);

        return $json_page_resultAll;
    }


    //Get table columns from collection
    public function getColumnsFromTablesColection()
    {

        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(
                array(
                    'coll_name' => 'get_sql_columns',
                    'filter' => "gsc.table_name = '" . $_POST['tablevalue'] . "'"
                )
            ))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

        return $json_page_result;
    }


    //Add Edit property to collection
    public function addEditPropertyToCollection($post)
    {

        $_POST['parameters'] = array(array(
            'key' => 'collection_id',
            'value' => $post['collection_id']
        ), array(
            'key' => 'checked',
            'value' => $post['checked']
        ), array(
            'key' => 'name',
            'value' => $post['name']
        ), array(
            'key' => 'column_name',
            'value' => $post['column_name']
        ), array(
            'key' => 'tables_id',
            'value' => $post['tables_id']
        ), array(
            'key' => 'userId',
            'value' => $this->session->userdata('UserID')
        ), array(
            'key' => 'id',
            'value' => $post['column_id']
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_addeditpropertytocollection&filters=' . $data_parameters;

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);
        return $json_page_result;
    }


    //Get Frame Data by name.
    public function getFrameDataByName($frameName)
    {

        $CURLDATA_FRAME = array('collections' => json_encode(
            array('colls' => array(
                array(
                    'coll_name' => 'frames',
                    'filter' => "bf.name = '" . $frameName . "'"
                ),
            ))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $json_page_result_frame = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA_FRAME);

        $json_page_result_frame = json_decode($json_page_result_frame);

        if (isset($json_page_result_frame->Data)) {
            $json_page_result_frame = $json_page_result_frame->Data;
        }

        return $json_page_result_frame;
    }

    //Get Frame properties.
    public function getFrameProperties($frameId)
    {

        $_POST['parameters'] = array(array(
            'key' => 'frame_id',
            'value' => $frameId
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_showframeproperties&filters=' . $data_parameters;

        $page_result_prop = $this->API->CallAPI("GET", $this->BASEURL);
        $page_result_prop = json_decode($page_result_prop);

        if (isset($page_result_prop->Data)) {
            $page_result_prop = $page_result_prop->Data;
        }

        return $page_result_prop;
    }

    //Get collection filter by column
    public function getCollectionFilterByColumn($collectionId, $column_name, $id)
    {

        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(
                array(
                    'coll_id' => $collectionId,
                    'filter' => "" . $column_name . " = '" . $id . "'"
                ),
            ))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

        $json_page_result = json_decode($json_page_result);

        if (isset($json_page_result->Data)) {
            $json_page_result = $json_page_result->Data;
        }

        return $json_page_result;
    }

    //delete table
    public function deleteTable($tableId)
    {

        $_POST['parameters'] = array(array(
            'key' => 'tables_id',
            'value' => $tableId
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_deletetablefromcollection&filters=' . $data_parameters;

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);

        return $json_page_result;
    }

    //get collection data with limit
    public function getCollectionData($collectionId)
    {

        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(
                array(
                    'coll_id' => $collectionId,
                    'pagenum' => '0',
                    'numrec'  => '20'
                )
            ))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

        return $json_page_result;
    }


    //// Now all functions called from pageedit file

    //Get Frame Collection properties. 
    public function getFrameCollectionProperties($frameId)
    {

        $_POST['parameters'] = array(array(
            'key' => 'frameId',
            'value' => $frameId
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_getframecollectionproperties&filters=' . $data_parameters;

        $all_default_result = $this->API->CallAPI("GET", $this->BASEURL);

        $page_result_data = json_decode($all_default_result);

        if (isset($page_result_data->Data)) {
            $page_result_data = $page_result_data->Data;
        }
        return $page_result_data;
    }


    //get collection data with limit 
    public function getCollectionDataWithoutLimit($collectionId)
    {

        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_id' => $collectionId
            )))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

        $page_result = json_decode($json_page_result);

        if (isset($page_result->Data)) {
            $page_result = $page_result->Data;
        }

        return $page_result;
    }


    //Add Edit property to collection
    public function addEditPropertyToFrame($post)
    {

        $_POST['parameters'] = array(array(
            'key' => 'property_id',
            'value' => $post['property_id']
        ), array(
            'key' => 'checked',
            'value' => $post['checked']
        ), array(
            'key' => 'name',
            'value' => $post['name']
        ), array(
            'key' => 'frameID',
            'value' => $post['frameID']
        ), array(
            'key' => 'userId',
            'value' => $this->session->userdata('UserID')
        ), array(
            'key' => 'id',
            'value' => $post['column_id']
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_addeditpropertytoframe&filters=' . $data_parameters;

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);
        return $json_page_result;
    }

    //delete frame
    public function deleteFrame($frameId, $pageFrameId)
    {

        $_POST['parameters'] = array(array(
            'key' => 'frameID',
            'value' => $frameId
        ), array(
            'key' => 'pageFrameId',
            'value' => $pageFrameId
        ));

        $data_parameters = '{"parameters":' . json_encode($_POST['parameters']) . '}';
        $data_parameters = urlencode($data_parameters);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $this->BASEURL = $BASEURLMETHOD . '?key=bsit_deleteframefrompage&filters=' . $data_parameters;

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);

        return $json_page_result;
    }


    //get collection data with limit 
    public function getAllConnection()
    {

        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_name' => 'data_collections'
            )))
        ));
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;
        $json_page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);

        return $json_page_result;
    }
}
