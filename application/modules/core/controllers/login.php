<?php
defined('BASEPATH') or exit('No direct script access allowed');

class login extends brain_controller
{
  /**
   * Define some constants for the log types.
   */
  const LOG_LOGIN_TYPE = 'login';
  const LOG_LOGOUT_TYPE = 'logout';
  const LOG_RESET_PASSWORD_TYPE = 'reset password';
  const LOG_REQUEST_RESET_PASSWORD_TYPE = 'reset password request';
  const LOG_CHANGE_PASSWORD = 'change password';
  /**
   * Array of log types options for this controller.
   * @var array
   */
  public $logAllowedTypes = [self::LOG_LOGIN_TYPE, self::LOG_LOGOUT_TYPE, self::LOG_RESET_PASSWORD_TYPE, self::LOG_REQUEST_RESET_PASSWORD_TYPE, self::LOG_CHANGE_PASSWORD];
  /*
   * Default log type
   */
  public $logType;
  /**
   * Variable for the log content.
   * @var
   */
  public $logContent;

  public function __construct()
  {
    parent::__construct(1);

    $this->load->helper(array('form', 'url', 'date', 'mystring', 'cookie'));
    $this->load->library('form_validation');
    $this->form_validation->CI = &$this;
    $this->load->library('session');
    $this->load->model('Bsit_io', 'API');
    $this->load->model('loginmodel');
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
  }

  public function index()
  {
    $isAjax = $this->input->is_ajax_request();
    $data = $this->loadSysParams();
    // if the users is already logged in, redirect the user to their home page
    if ($this->session->userdata('logged_in')) {
      $default_home = $this->session->userdata('DefaultHome');
      redirect($default_home, 'refresh');
    }

    // Validate the username and pass entered are valid
    // NOTE: the last part of this validation rule 'callback_check_database' will run the
    // custom function 'check_database' to verify the username and password are correct.
    $this->form_validation->set_rules('username', 'Username', 'trim|required');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

    if ($this->form_validation->run($this) === false) {
      if (!$isAjax) {
        $this->load->view('login', $data);
      } else {
        //If it is the login ajax, read the validation errors and return a json object.
        $errors = $this->form_validation->error_array();
        foreach ($errors as $key => $error) {
          $response['msg'][] = ['field' => $key, 'message' => $error];
        }
        $response['success'] = false;
        echo json_encode($response);
        die();
      }
    } else {
      $uri = $this->input->post('redirect');
      if (isset($_POST['rememberme'])) {
        $cookie = array(
          'name' => 'firstuser',
          'value' => $_POST['username'],
          'expire' => 86500000,
          'secure' => false
        );

        $this->input->set_cookie($cookie);
      }

      $default_home = $this->session->userdata('DefaultHome');
      $userID = $this->session->userdata('UserID');
      $loggedIn = $this->session->userdata('logged_in');
      if (!$isAjax) {
        if ($uri) {
          redirect('/' . $uri);
        } else {
          redirect($default_home);
        }
      } else {
        //If it is the login ajax, return a JSON response.
        $response['success'] = true;
        echo json_encode($response);
        die();
      }
    }
  }

  public function check_database($password)
  {

    $key = pack("H*", "0123456789abcdef0123456789abcdef");
    $iv = pack("H*", "abcdef9876543210abcdef9876543210");
    $encrypted = base64_decode(trim($this->input->post('password')));
    $password = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv), "\0..\32");


    $username = $this->input->post('username');

    $page_result = $this->loginmodel->getUserDetail($username);

    if (isset($page_result[0]->Results) && !empty($page_result[0]->Results)) {
      if ($page_result[0]->Results[0]->is_active == 0) {
        $reason = 'Invalid Authentication';
        $this->logType = self::LOG_LOGIN_TYPE;
        $this->saveLog($page_result[0]->Results[0]->Username, 'User is not active');
        $this->form_validation->set_message('check_database', $reason);
        return false;
      }

      $user = (array)$page_result[0]->Results[0];

      $password_correct_db = $user['Password'];
      $params = array(8, false);
      $check = $this->CheckPassword($password, $password_correct_db);

      if ($check) {
        if ($user) {
          //Check if the user is locked.
          if ($user['is_locked'] == 1) {
            $reason = 'User is locked. Please contact Admin.';
            //Save log
            $this->logType = self::LOG_LOGIN_TYPE;
            $this->saveLog($username, $reason);
            $this->form_validation->set_message('check_database', $reason);
            return false;
          }
          //Save log
          $this->logType = self::LOG_LOGIN_TYPE;
          $this->saveLog($username, 'Successful login.', true);
          // Create a new instance of the user class and setup login etc
          // set all the user specific values with the 'logged in' status and users group info
          foreach ($user as $key => $value) {
            $this->session->set_userdata($key, $value);
          }

          $this->session->set_userdata('logged_in', true);
          $this->session->set_userdata('today_date', date('Y-m-d'));
          $this->session->set_userdata('timezone', $this->input->post('timezone'));
          return true;
        } else {
          $reason = 'Invalid Access';
          //Save log
          $this->logType = self::LOG_LOGIN_TYPE;
          $this->saveLog($username, 'Invalid username/password');
          $this->form_validation->set_message('check_database', $reason);
          return false;
        }
        return true;
      } else {
        $reason = 'Invalid Username/Password.';
        //Save log
        $this->logType = self::LOG_LOGIN_TYPE;
        $this->saveLog($username, $reason);
        //Save failed login attempts.
        $params = ['userId' => $page_result[0]->Results[0]->UserID];
        $response = $this->loginmodel->setUserFailedLoginAttempts($params);
        if ($response) {
          if (!empty($response->Data)) {
            $reason .= 'User is locked. Please contact the Admin.';
            //Just verifying if the account was already locked. So we don't send multiple emails
            if ($page_result[0]->Results[0]->is_locked != 1) {
              //Send email for user locked.
              $this->loginmodel->createRequestToSendEmailResetPassword(['userId' => $page_result[0]->Results[0]->UserID, 'type' => 'UserAccountLocked']);
            }
          }
        }
        $this->form_validation->set_message('check_database', $reason);
        return false;
      }
    } else {
      $reason = 'Invalid Authentication.';
      //Save log
      $this->logType = self::LOG_LOGIN_TYPE;
      $this->saveLog($username, 'Invalid username/password');
      //Save failed login attempts.
      $params = ['userId' => $page_result[0]->Results[0]->UserID];
      $response = $this->loginmodel->setUserFailedLoginAttempts($params);
      if ($response) {
        if (!empty($response->Data)) {
          $reason .= 'User is locked. Please contact the Admin.';
          //Just verifying if the account was already locked. So we don't send multiple emails.
          if ($page_result[0]->Results[0]->is_locked != 1) {
            //Send email for user locked.
            $this->loginmodel->createRequestToSendEmailResetPassword(['userId' => $page_result[0]->Results[0]->UserID, 'type' => 'UserAccountLocked']);
          }
        }
      }
      $this->form_validation->set_message('check_database', $reason);
      return false;
    }
  }

  public function logout()
  {
    //Save log
    $this->logType = self::LOG_LOGOUT_TYPE;
    $this->saveLog($this->session->userdata('Username'), 'Logout', true);
    // It's desired to kill the session, also delete the session cookie.
    // Note: This will destroy the session, and not just the session data!
    if (ini_get("session.use_cookies")) {
      $params = session_get_cookie_params();
      setcookie(
        session_name(),
        '',
        time() - 42000,
        $params["path"],
        $params["domain"],
        $params["secure"],
        $params["httponly"]
      );
    }

    // unset the session variables
    $this->session->unset_userdata('logged_in');
    $this->session->unset_userdata('user_id');
    $this->session->unset_userdata('group_id');
    unset($_SESSION['cart_items']);
    $this->session->sess_destroy();
    unset($_SESSION);
    redirect('login', 'refresh');
  }

  public function CheckPassword($password_input, $password_db)
  {

    if (crypt($password_input, $password_db) == $password_db) {
      return true;
    } else {
      return false;
    }
  }

  public function encrypt_password()
  {

    $postdata = json_decode($_POST['data']);

    $new_password = $postdata[0]->password;
    $user_id = $postdata[0]->user_id;

    $page_result = $this->loginmodel->getUserCollectionData($user_id);

    $old_pass = $page_result[0]->Results[0]->Password;
    if ($old_pass == $new_password) {
      echo $new_password;
    } else {
      $blowfish_salt = "$2y$15$" . bin2hex(openssl_random_pseudo_bytes(22));
      echo $newpass = crypt($new_password, $blowfish_salt);
    }
  }

  /**
   * Process a request to reset a password
   * If all validations are ok, then set a password_reset_key to the user and send an email.
   * If not, display error messages in the login page.
   * @return void
   */
  public function processRequestResetPassword()
  {
    $data = $this->loadSysParams();
    $data['action'] = 'reset';
    $username = $this->input->post('username');

    //HoneyPot validation.
    //If this field is !empty, is probably a robot or someone with bad intentions.
    if (!empty($this->input->post('email'))) {
      $this->load->view('login', $data);
      return;
    }
    //Set validation
    $this->form_validation->set_rules('username', 'Username', 'trim|required|callback_check_validusername');
    if ($this->form_validation->run($this) === false) {
      $data['action'] = '';
      //$data['resetSuccessMessage'] = validation_errors();
      $this->load->view('login', $data);
    } else {
      $data['action'] = '';
      $data['resetSuccessMessage'] = 'An email with instructions to reset your password has been sent. Please, verify your email to continue.';
      $this->load->view('login', $data);
    }
  }

  /**
   * Resets user password
   * Expects a urlencode($key)
   */
  public function resetPassword()
  {
    $resetKey = $this->input->get('key');
    $data = $this->loadSysParams();
    $data['resetKey'] = $resetKey;
    $data['message'] = '';
    $data['error'] = 0;
    //Key is expired.
    if (!$this->IsValidPasswordKey($resetKey)) {
      $data['message'] = 'This key is no longer valid. Please, create a new request to reset your password.';
      $this->logType = self::LOG_RESET_PASSWORD_TYPE;
      $data['error'] = 1;
      $this->saveLog('', 'Expired key.');
    }
    if (!$data['error']) {
      $resetKeyDecrypt = $this->decryptResetPasswordKey($resetKey);
      $userNameAndData = explode('|', $resetKeyDecrypt);
      $keyUsername = $userNameAndData[0];
      $keyTimestamp = substr($userNameAndData[1], 0, 25);

      //Validate the user
      $user = $this->loginmodel->getUserByUsername($keyUsername);
      //Username doesn't exist.
      if (!$user) {
        $data['message'] = 'Invalid user.';
        $this->logType = self::LOG_RESET_PASSWORD_TYPE;
        $data['error'] = 1;
        $this->saveLog('', $data['message']);
      } //Key passed is different of user key.
      else if (urldecode($user->reset_password_key) != $resetKey) {
        $data['message'] = 'Invalid key.';
        $this->logType = self::LOG_RESET_PASSWORD_TYPE;
        $data['error'] = 1;
        $this->saveLog($user->Username, $data['message']);
      } //The user hasn't request a password reset.
      else if (empty($user->reset_password_key)) {
        $data['message'] = 'Invalid key.';
        $data['error'] = 1;
        $this->logType = self::LOG_RESET_PASSWORD_TYPE;
        $this->saveLog($user->Username, $data['message']);
      } //User is locked or inactive
      else if ($user->is_locked == 1 || $user->is_active == 0) {
        $data['message'] = 'User is locked or inactive. Please, contact admin.';
        $data['error'] = 1;
        $this->logType = self::LOG_RESET_PASSWORD_TYPE;
        $this->saveLog($user->Username, 'User is locked or inactive.');
      }
      $data['user'] = $user;
    }
    //All good, we can reset the password.
    if (!empty($this->input->post())) {
      $password = $this->input->post('password');
      $confirmpassword = $this->input->post('confirmpassword');

      //Honeypot validation.
      if (!empty($this->input->post('email'))) {
        return false;
      }
      //Someone trying to be smart.
      if (empty($password)) {
        $data['message'] = 'Invalid Password.';
        $data['error'] = 1;
        $this->logType = self::LOG_RESET_PASSWORD_TYPE;
        $this->saveLog($user->Username, $data['message']);
      } else {
        //All good again!
        //decrypt the password sent via JS
        $key = pack("H*", "0123456789abcdef0123456789abcdef");
        $iv = pack("H*", "abcdef9876543210abcdef9876543210");
        $encrypted = base64_decode(trim($this->input->post('password')));
        //plain text password.
        $password = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv), "\0..\32");

        $newPasswordDecrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv), "\0..\32");

        //Check if new password = old password
        if ($this->CheckPassword($newPasswordDecrypted, $user->Password)) {
          //$response = ['success' => false, 'msg' => 'New Password is equal current password. Please, use a different password.']; //Improve this message.
          $data['message'] = 'New Password is equal current password. Please, use a different password.';
          $data['error'] = 1;
          $data['showTryAgainBtn'] = 1;
          $this->logType = self::LOG_RESET_PASSWORD_TYPE;
          $this->saveLog($user->Username, 'Could not set new password to user.');
        } else {
          //Encrypt password for saving in  DB
          $salt = "$2y$15$" . bin2hex(openssl_random_pseudo_bytes(22));
          $userpass = crypt($password, $salt);
          //Save password and clear the reset_password_key.
          $params = ['userId' => $user->_Id, 'password' => utf8_encode($userpass)];
          $result = $this->loginmodel->setUserResetPasswordKey($params);
          //We couldn't save the password in the db.
          if (!$result) {
            $data['message'] = 'Could not save password, please contact I.T.';
            $data['error'] = 1;
            $this->logType = self::LOG_RESET_PASSWORD_TYPE;
            $this->saveLog($user->Username, 'Could not set new password to user.');
          } else {
            //All good, Do the user login.
            $this->logType = self::LOG_RESET_PASSWORD_TYPE;
            $this->saveLog($user->Username, 'Password reset successfully', true);
            foreach ($user as $key => $value) {
              $this->session->set_userdata($key, $value);
            }
            $this->session->set_userdata('logged_in', true);
            $this->session->set_userdata('today_date', date('Y-m-d'));
            $default_home = $this->session->userdata('DefaultHome');
            $this->session->set_userdata('timezone', $this->input->post('timezone'));

            redirect($default_home, 'refresh');
          }
        }
      }
    }
    return $this->load->view('resetpassword', $data);
  }


  /**
   * Load the sysparams for the login page.
   * @return mixed
   */
  public function loadSysParams()
  {

    $page_result_sys_api = $this->loginmodel->getSystemData();

    foreach ($page_result_sys_api[0]->Results as $key => $sysValue) {

      if ($sysValue->paramname == SYS_APPLICATION_TITLE) {
        $data['appTitle'] = $sysValue->paramvalue;
        $this->session->set_userdata('appTitle', $sysValue->paramvalue);
      } elseif ($sysValue->paramname == SYS_APPLICATION_ICON) {
        $data['appIcon'] = $sysValue->paramvalue;
        $this->session->set_userdata('appIcon', $sysValue->paramvalue);
      } elseif ($sysValue->paramname == SYS_LOGIN_ICON) {
        $data['loginIcon'] = $sysValue->paramvalue;
        $this->session->set_userdata('loginIcon', $sysValue->paramvalue);
      } elseif ($sysValue->paramname == SYS_CSS_OVERRIDE) {
        $data['cssOverride'] = $sysValue->paramvalue;
        $this->session->set_userdata('cssOverride', $sysValue->paramvalue);
      } elseif ($sysValue->paramname == SYS_MAX_LOGIN_ATTEMPTS) {
        $data['maxLoginAttempts'] = $sysValue->paramvalue;
        $this->session->set_userdata('maxLoginAttempts', $sysValue->paramvalue);
      } elseif ($sysValue->paramname == SYS_TIME_EXPIRE_RESET_PASSWORD_LINK) {
        $data['timeExpireResetPasswordLink'] = $sysValue->paramvalue;
        $this->session->set_userdata('timeExpireResetPasswordLink', $sysValue->paramvalue);
      }
    }
    return $data;
  }

  /**
   * Check if the password key is still valid.
   * Conditions: Username of the key = username submitted in the form
   * than key time + time to expire < current datetime
   * @param $key
   * @param $username
   * @return bool
   * @throws Exception
   */
  public function IsValidPasswordKey($key, $username = null)
  {
    $now = new Datetime();
    $timeExpireResetPasswordLink = $this->session->userdata('timeExpireResetPasswordLink');
    $oldKey = $this->decryptResetPasswordKey($key);
    $userNameAndData = explode('|', $oldKey);
    $keyUsername = $userNameAndData[0];
    $keyTimestamp = new Datetime(substr($userNameAndData[1], 0, 25));
    if (!is_null($username)) {
      //Key username is different of user trying to recover the password.
      if ($username != $keyUsername) {
        return false;
      }
    }
    //Check if the key is still valid.
    if ($keyTimestamp->modify('+' . $timeExpireResetPasswordLink . ' hour') < $now) {
      return false;
    }
    return true;
  }

  /**
   * Validates the username
   * @param $username
   * @return boolean
   */
  public function check_validusername($username)
  {
    $resetTime = new Datetime();
    //get user details.
    $user = $this->loginmodel->getUserByUsername($username);
    //User name does not exists
    if (empty($user)) {
      $reason = 'Invalid Username.';
      $this->logType = self::LOG_REQUEST_RESET_PASSWORD_TYPE;
      $this->saveLog($username, $reason);
      $this->form_validation->set_message('check_validusername', $reason);
      return false;
    }
    //User exists and it is locked
    $isLocked = !empty($user->is_locked) ? $user->is_locked : 0;
    if ($isLocked) {
      $reason = 'User is locked. Please contact System admin';
      $this->logType = self::LOG_REQUEST_RESET_PASSWORD_TYPE;
      $this->saveLog($username, 'User is locked');
      $this->form_validation->set_message('check_validusername', $reason);
      return false;
    }
    $email = $user->Email;
    $userId = $user->_Id;
    $userExistingResetPasswordKey = $user->reset_password_key;
    if (!empty($userExistingResetPasswordKey)) {
      //Check if the existing key is valid.
      if (!$this->IsValidPasswordKey(utf8_decode($userExistingResetPasswordKey), $username)) {
        $reason = 'Instructions to reset password were already sent by e-mail. Please, verify your email to continue.';
        $this->logType = self::LOG_REQUEST_RESET_PASSWORD_TYPE;
        $this->saveLog($username, 'User already has a valid reset password key.');
        $this->form_validation->set_message('check_validusername', $reason);
        return false;
      }
    }
    //set the password key
    $reset_password_key = $this->setResetPasswordKey($username, $resetTime);
    $params = ['userId' => $userId, 'reset_password_key' => utf8_encode(urlencode($reset_password_key))];
    $result = $this->loginmodel->setUserResetPasswordKey($params);
    if ($result) {
      //Send email
      if ($this->loginmodel->createRequestToSendEmailResetPassword(['userId' => $userId, 'type' => 'UserForgotPassword'])) {
        $this->logType = self::LOG_REQUEST_RESET_PASSWORD_TYPE;
        $reason = 'E-mail sent successfully';
        $this->saveLog($username, $reason, true);
        return true;
      }
    }
    $reason = 'Could not send reset password. Please contact I.T.';
    $this->logType = self::LOG_REQUEST_RESET_PASSWORD_TYPE;
    $this->saveLog($username, 'Could not create email request/could not set reset password key.');
    $this->form_validation->set_message('check_validusername', $reason);
    return false;
  }



  /**
   * Create ResetKey
   * @param $username
   * @param $resetTime
   * @return string|null
   */
  public function setResetPasswordKey($username, $resetTime)
  {
    //reset_password_key => username + datetime reset.
    $resetKey = $username . '|' . $resetTime->format('c');
    $key = pack("H*", "0123456789abcdef0123456789abcdef");
    $iv = pack("H*", "abcdef9876543210abcdef9876543210");
    return openssl_encrypt(trim($resetKey), 'AES-128-CBC', $key, 0, $iv);
  }

  /**
   * Decrypt the ResetKey
   * @param $key
   * @return string|null
   */
  public function decryptResetPasswordKey($resetKey)
  {
    $key = pack("H*", "0123456789abcdef0123456789abcdef");
    $iv = pack("H*", "abcdef9876543210abcdef9876543210");
    return openssl_decrypt($resetKey, 'AES-128-CBC', $key, OPENSSL_ZERO_PADDING, $iv);
  }

  /**
   * Save the log.
   * @param $username
   * @param $reason
   * @param bool $success
   * @return mixed
   * @throws Exception
   */
  public function saveLog($username, $reason, $success = false)
  {
    //Check to see if the someone is trying to save a log with a type that was not defined.
    if (!in_array($this->logType, $this->logAllowedTypes)) {
      throw new Exception('Log type not allowed');
    }
    //Set log additional info
    $logContent = [];
    $logContent['date_time'] = new Datetime();
    $logContent['session_id'] = session_id();
    $logContent['ip'] = $_SERVER['REMOTE_ADDR'];
    $logContent['server_name'] = $_SERVER['SERVER_NAME'];
    $logContent['username'] = $username;
    $logContent['reason'] = $reason;

    $latitude = ($this->input->post('latitude') != '') ? $this->input->post('latitude') : $this->input->get('latitude');
    $longitude = ($this->input->post('longitude') != '') ? $this->input->post('longitude') : $this->input->get('longitude');
    $logContent['latitude'] = $latitude;
    $logContent['longitude'] = $longitude;

    $this->logContent = json_encode($logContent);
    //Set sp params
    $filters[] = ['key' => 'action', 'value' => $this->logType];
    $filters[] = ['key' => 'parameters', 'value' => $username];
    $filters[] = ['key' => 'additional_info', 'value' => $this->logContent];
    $filters[] = ['key' => 'username', 'value' => 'autogen'];
    $filters[] = ['key' => 'success', 'value' => (int)$success];
    $parameters = [
      'key' => 'saveLog',
      'filters' => $filters
    ];
    //Save log.
    $result = $this->API->CallAPI("GET", API_BASEURL . API_METHOD_GET, json_encode($parameters), false);
    $result = json_decode($result);
    return $result;
  }


  public function changepassword()
  {
    if ($this->input->is_ajax_request()) {
      header('Content-Type: application/json');
      if ($this->session->userdata('logged_in')) {
        $post = $this->input->post();
        $oldPassword = $post['old_password'];
        $newPassword = $post['new_password'];
        $confirmPassword = $post['confirm_password'];
        $response = ['success' => true, 'msg' => 'Password Changed Successfully'];
        if (empty($oldPassword) || empty($newPassword) || empty($confirmPassword)) {
          $response = ['success' => false, 'msg' => 'Invalid data.'];
          echo json_encode($response);
          exit();
        }
        //Check if new password = confirm password
        if ($newPassword != $confirmPassword) {
          $response = ['success' => false, 'msg' => 'New Password and Confirm Password do not match.'];
          echo json_encode($response);
          exit();
        }
        $user = $this->loginmodel->getUserByUsername($this->session->userdata('Username'));
        $key = pack("H*", "0123456789abcdef0123456789abcdef");
        $iv = pack("H*", "abcdef9876543210abcdef9876543210");
        $encrypted = base64_decode(trim($oldPassword));
        $oldpassword = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv), "\0..\32");

        $password_correct_db = $user->Password;
        $params = array(8, false);
        //Check if old password informed is users password
        if (!$this->CheckPassword($oldpassword, $password_correct_db)) {
          $response = ['success' => false, 'msg' => 'Current Password and user password do not match.'];
          echo json_encode($response);
          exit();
        }
        $encrypted = base64_decode(trim($newPassword));
        $newPasswordDecrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv), "\0..\32");

        //Check if new password = old password
        if ($this->CheckPassword($newPasswordDecrypted, $password_correct_db)) {
          $response = ['success' => false, 'msg' => 'New Password is equal current password. Please, use a different password.']; //Improve this message.
          echo json_encode($response);
          exit();
        }
        //All good, save password in the db.
        $encrypted = base64_decode(trim($newPassword));
        //plain text password.
        $password = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv), "\0..\32");
        //Encrypt password for saving in  DB
        $salt = "$2y$15$" . bin2hex(openssl_random_pseudo_bytes(22));
        $userpass = crypt($password, $salt);
        //Save password and clear the reset_password_key.
        $params = ['userId' => $user->_Id, 'password' => utf8_encode($userpass), 'change_password' => 1];
        $result = $this->loginmodel->setUserResetPasswordKey($params);
        if (!$result) {
          $response = ['success' => false, 'msg' => 'Could not change password. Plese, try again later.'];
          echo json_encode($response);
          die();
        }
        $reason = 'User Changed Password';
        $this->logType = self::LOG_CHANGE_PASSWORD;
        $this->saveLog($user->Username, $reason, 1);
        echo json_encode($response);
        die();
      }
    }
  }

  /**
   * Check if the session is expired.
   */
  public function isSessionExpired()
  {

    if ($this->input->is_ajax_request()) {
      header('Content-Type: application/json');
      echo json_encode(['expired' => (empty($this->session->userdata('UserID')) ? 1 : 0)]);
    }
  }
}
