import './utils/Notes/Notes.js';
import {
  setupFramePagination,
  advCollectionFocus,
  advCollectionSource,
  autoCompCollFocus,
  autoCompCollSource,
  advMultiCollectionSource,
  advMultiCollectionFocus,


} from './frames/TableView/TableView.js';

// Page frame object array
let pageFrameArray = [{}];

// Run functions on page load
$(document).ready(() => {
  // Gather all frames and store their data in an object
  updatePageFrameArray();
  //design home page task as per their priority
  home_job_task_priority();
});

function updatePageFrameArray(){
  pageFrameArray = $('.bsit-page-frame')
  .toArray()
  .map((frame) => {
    return {
      pfElement: frame,
      PFID: $(frame).data('pfid'),
      frameView: $(frame).data('frameview'),
    };
  });

// Setup frames, TODO: Load modules
pageFrameArray.forEach((frame) => {
  switch (frame.frameView) {
    case 'TableView':
      setupFramePagination(frame);
      break;
    default:
      setupFramePagination(frame);
      break;
  }
});
}

// Close modals on escape
$(document).on('keypress', (e) => {
  if (e.keyCode === 27) {
    $('.modal').modal('hide');
  }
});

// Close modals when clicking outside of them
//$('body').on('click', (e) => {
//if (!$(e.target).closest('.modal').length) {
//$('.modal').modal('hide');
//}
//});

$(document).on("focusin", ".autoCompCollAdv", function () {
  $(this).attr('data-oldval', $(this).val());
});
$(document).on("keyup", ".autoCompCollAdv", function () {
  if ($(this).val() != $(this).attr('data-oldval')) {
    $(this).attr("data-dirty", "1");
  }
});

$(document).on("focusin", ".autoCompColl", function () {
  $(this).attr('data-oldval', $(this).val());
});
$(document).on("keyup", ".autoCompColl", function () {
  if ($(this).val() != $(this).attr('data-oldval')) {
    $(this).attr("data-dirty", "1");
  }
});

$('.autoCompCollAdv')
  .autocomplete({
    minLength: 0,
    autoFocus: true,
    source: function (req, res) {
      advCollectionSource(req, res, $(this));
    },
    //Jigar [23 Mar 2021]
    select: function (event, ui) {
      event.preventDefault();
      event.stopPropagation();
      $(event.target).val(ui.item.label);
      $(event.target).attr("data-selectedid", ui.item.value);
      setTimeout(function () {
        $(event.target).attr("data-dirty", "0");
      }, 200)
      resetField(event);
    },
  })
  .focus((e) => {
    advCollectionFocus(e);
  });

function resetField(event) {
  let parentForm = $(event.target).parents('form:first');
  let advCollectionJSON = jQuery.parseJSON(
    $(event.target)
      .attr('data-columns')
      .replace(/'/g, '"')
  );

  //reset other fields on change
  let ResetFields = advCollectionJSON.ResetFields;

  //reset other field value on change
  if (ResetFields) {
    ResetFields.forEach((field, index) => {
      let pe = $(parentForm)
        .find(`input[name='${field}']:first`);

      pe.attr('data-selectedid', '');
      pe.val('');
    });
  }
}

// input mask
$(":input").inputmask({'autoUnmask' : true});

//MULTI SELECT ADD Code Start
var id_advanceMultiSelectObj = new Object();

$(function () {
  $(".advanceMultiSelect")
    .bind("keydown", function (event) {
      id_advanceMultiSelectObj = $(this);
      if (event.keyCode === $.ui.keyCode.TAB &&
        $(this).data("ui-autocomplete").menu.active) {
        event.preventDefault();
      }
    })
    .autocomplete({
      minLength: 1,
      source: function (request, response, event) {
        advMultiCollectionSource(request, response, $(this));
      },
      focus: function () {
        // prevent value inserted on focus
        return false;
      },
      //Jigar [23 Mar 2021]
      select: function (event, ui) {

        var selectlabel = ui.item.label;
        var selectvalue = ui.item.value;
        var selectvalue_str = ui.item.value.toString();
        var getPropName = event.target.attributes["data-returnpropname"].value;

        // let select_advanceMultiValue = $("#hf_advanceMultiSelect_value").attr("labelMultiSelect",""+getPropName+"_value").val().split(",");
        let select_advanceMultiValue = $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val().split(',');

        if (jQuery.inArray(selectvalue_str, select_advanceMultiValue) != -1) {
          $('.advanceMultiSelect').val();
          return false;
        }

        // if($('#hf_advanceMultiSelect').attr("labelMultiSelect",""+getPropName+"_label").val().length == 0){
        //   selectlabel=$('#hf_advanceMultiSelect').attr("labelMultiSelect",""+getPropName+"_label").val()+$.trim(selectlabel);
        //   $("#hf_advanceMultiSelect").attr("labelMultiSelect",""+getPropName+"_label").val($.trim(selectlabel));
        //   selectvalue=$('#hf_advanceMultiSelect_value').attr("valueMultiSelect",""+getPropName+"_value").val()+$.trim(selectvalue);
        //   $("#hf_advanceMultiSelect_value").attr("valueMultiSelect",""+getPropName+"_value").val($.trim(selectvalue));
        // }else{
        //   selectlabel=$('#hf_advanceMultiSelect').attr("labelMultiSelect",""+getPropName+"_label").val()+','+$.trim(selectlabel);
        //   $("#hf_advanceMultiSelect").attr("labelMultiSelect",""+getPropName+"_label").val($.trim(selectlabel));
        //   selectvalue=$('#hf_advanceMultiSelect_value').attr("valueMultiSelect",""+getPropName+"_value").val()+','+$.trim(selectvalue);
        //   $("#hf_advanceMultiSelect_value").attr("valueMultiSelect",""+getPropName+"_value").val($.trim(selectvalue));
        // }

        var obj_data = jQuery(".multiSelect-ul").find("li");

        $.each(obj_data, function (i, e) {
          var classname = $(this).attr("class");
          if (classname != "li-input") {
            $(this).remove();
          } else {
            $(this).find("input").val("");
          }
        });

        // alert($(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val().length);
        if ($(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val().length == 0) {

          var userlabelGet = $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val();
          $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val(selectlabel);

          var userValueGet = $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val();
          $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val(selectvalue);

        } else {

          var userlabelGet = $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val();
          $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val(userlabelGet + ',' + selectlabel);

          var userValueGet = $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val();
          $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val(userValueGet + ',' + selectvalue);

        }
        // var userlabelGet= $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val();
        // $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val(userlabelGet+','+selectlabel);

        //  var userValueGet= $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val();
        // $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val(userValueGet+','+selectvalue);
        // alert($(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val());
        $.each($(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val().split(","), function (i, v) {
          if (v != "") {

            jQuery(id_advanceMultiSelectObj).parent().parent().parent().find(".multiSelect-ul li:eq(0)").before('<li><span class="del-multi-select-val" propname="' + getPropName + '" data-val="' + v + '">X</span>' + v + '</li>');
          }
        });


        let getUserValue = $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect_value").eq(0).val().split(',');

        let getUserLabel = $(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val().split(',');

        let combainUserLabelValue = "";

        $.each($(id_advanceMultiSelectObj).parent().parent().parent().find(".hf_advanceMultiSelect").eq(0).val().split(','), function (i, v) {
          if (combainUserLabelValue.length == 0) {

            combainUserLabelValue = getUserValue[i] + ":" + getUserLabel[i];

          } else {

            combainUserLabelValue = combainUserLabelValue + "," + getUserValue[i] + ":" + getUserLabel[i];
          }

        });
        let get_combainUserLabelValue = $.trim(combainUserLabelValue);


        $("input[name=" + getPropName + "]").val(get_combainUserLabelValue);


        return false;
      }
    }).autocomplete("instance")._renderItem = function (ul, item) {
      return $("<li>")
        .append("<div>" + item.label + ", " + item.value + "</div>")
        .appendTo(ul);
    };

  function split(val) {
    return val.split(/,\s*/);
  }

  function extractLast(term) {
    return split(term).pop();
  }
});
//MULTI SELECT ADD REMOVE CODE

jQuery(document).on("click", ".del-multi-select-val", function (event) {
  // debugger
  id_advanceMultiSelectObj = $(this).parent().parent().parent();
  // var getPropName = $(this).attr("data-returnpropname");
  var getPropName = event.target.attributes['propname'].value;

  // alert($("input[name="+getPropName+"]").val());

  var val = $(this).attr("data-val");
  var hf = "";
  var hf_removeUserValue = "";
  var hf_removegetUserLabelValue = "";
  var removeUserValue = $(id_advanceMultiSelectObj).find('.hf_advanceMultiSelect_value').eq(0).val().split(',');
  var removegetUserLabelValue = $(id_advanceMultiSelectObj).find('.advanceMultiSelectRemoveVal').eq(0).val().split(',');
  var obj_data = jQuery(id_advanceMultiSelectObj).find(".multiSelect-ul").find("li");

  $.each(obj_data, function (i, e) {

    var classname = $(this).attr("class");
    if (classname != "li-input") {
      $(this).remove();
    } else {
      $(this).find("input").val("");
    }
  });
  hf_removegetUserLabelValue = "";
  $.each($(id_advanceMultiSelectObj).find('.hf_advanceMultiSelect').eq(0).val().split(','), function (i, v) {
    if (v != "") {
      if (v != val) {

        hf = hf + "," + v;
        var uservl = removeUserValue[i]
        hf_removeUserValue = hf_removeUserValue + "," + removeUserValue[i];
        hf_removegetUserLabelValue = hf_removegetUserLabelValue + "," + uservl + ":" + v;
        jQuery(id_advanceMultiSelectObj).find(".multiSelect-ul li:eq(0)").before('<li><span class="del-multi-select-val" propname="' + getPropName + '" data-val="' + v + '">X</span>' + v + '</li>');
      }

    }
  });

  $(id_advanceMultiSelectObj).find('.hf_advanceMultiSelect').eq(0).val(hf);
  $(id_advanceMultiSelectObj).find('.hf_advanceMultiSelect_value').eq(0).val(hf_removeUserValue);
  $(id_advanceMultiSelectObj).find('.advanceMultiSelectRemoveVal').eq(0).val(hf_removegetUserLabelValue);
});
//MULTI SELECT ADD Code Stop
$('.autoCompColl')
  .autocomplete({
    minLength: 0,
    autoFocus: true,
    source: function (req, res) {
      autoCompCollSource(req, res, $(this));
    },
    //Jigar [23 Mar 2021]
    select: function (event, ui) {
      event.preventDefault();
      event.stopPropagation();
      $(event.target).val(ui.item.label);
      $(event.target).attr("data-selectedid", ui.item.value);
      setTimeout(function () {
        $(event.target).attr("data-dirty", "0");
      }, 200)
      resetField(event);
    },
  })
  .focus((e) => {

    autoCompCollFocus(e);
  });


// Finding parent page frame from any element
function getFrameFromElement(element, pageframeid) {
  let pfid = '';

  if (pageframeid) {
    pfid = pageframeid
  } else {
    pfid = $(element)
      .closest('.bsit-page-frame')
      .data('pfid');
  }

  let frameIndex = pageFrameArray.findIndex((pf) => pf.PFID == pfid);

  // Ensure frame was found
  if (typeof frameIndex !== undefined) {
    return pageFrameArray[frameIndex];
  } else {
    return undefined;
  }
}

$(document).ajaxComplete(function (event, xhr, settings) {
  home_job_task_priority();
});

//design home page task as per their priority
function home_job_task_priority() {
  let pf = (ENVIRONMENT == "production") ? "page_frame_82_content" : "page_frame_89_content";
  var tr = $("#" + pf).find("table > tbody > tr");
  tr.each(function() {
    let td = $(this).find("td:nth-child(9)");
    let priority = td.text();
    if (priority) {
      if (priority.toLowerCase() == 'high') {
        $(this).addClass("jt_priority_high");
      } else if (priority.toLowerCase() == 'urgent') {
        $(this).addClass("jt_priority_urgent");
      }
    }
  });
}

// hide title
 $('#editBtn').click(function() {
   $(".FrameMainTitle").css("display","none");
});
export {
  getFrameFromElement,
  updatePageFrameArray
};