<!-----------------------------------------
-- BSIT CHART
-- #KeyChartIDs
------------------------------------------>
<div class="chart_main" id="repTerrSalesMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Stage</div>
  </div>

  <!-- body -->
  <div class="chart_body">
      <!-- RIGHT -->
      <div class="chart_hero" id="StageBarChart_container" style="height:400px; max-height:500px">
        <canvas id="StageBarChart" style="width:100%; height:100%;"></canvas>
      </div>
  </div>
</div>
