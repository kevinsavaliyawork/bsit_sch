</div>
</div>
</div>
</div>
<!-- Loader / Spinner -->
<div class="loader" style="display: none;">
  <div class="mdl-spinner mdl-js-spinner is-active material_loder"></div>
</div>

<!-- Modal change password -->
<div id="change-password-modal" class="modal fade bsit_main_popup" data-modal="Change Password" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 9999999999;">
  <button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">
                    <svg class="" viewBox="0 0 24 24"><path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                </button>
  <div class="modal-content" style="float: left; width: 100%; padding: 15px 0;">
    <div class="modal-header" style="padding-top: 0px; padding-bottom: 0px; border-bottom:0;">
      <h2 class="bsit_form_titel">Change Password</h2>
      <hr>
    </div>
    <div class="modal-body">
      <?php echo $this->load->view('changepassword');?>
    </div>
  </div>
</div>


<?php if ($_GET['headerPage'] != 1) { ?>
	 <?php if($footer_view == 1) {?>

<!-- <footer id="footer">
  <div class="mui-container-fluid">
    Copyright © BrainStorm IT <b>BRAIN</b> Application <?php echo(date("Y"));?>
  </div>
</footer> -->
	<?php } ?>
<?php } ?>
<div id="bsit-upload-modal-container" style="color: black;">
  <?php require_once APPPATH.'modules/core/views/Documents.html' ?>
</div>
</body>
</html>
