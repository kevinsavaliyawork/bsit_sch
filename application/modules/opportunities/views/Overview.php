	<script src="<?php echo base_url();?>opportunities/assets/js/opportunity_details.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>opportunities/assets/css/opportunity_style.css">

		<div class="bsit_opp_overview_info">

			<div class="bsit_opp_overview_value_block">
				<div class="bsit_opp_overview_value bsit_opp_overview_desc ">
						<h3 class="left"></h3>
					<!-- 	<h3 class="left"><?php echo $customer[0]->job_title; ?>
						</h3>
						<br/>
						<span class="custmerDetails">
						<b>
						<?php if($customer[0]->customerName == '' || $customer[0]->customerName == null){ ?>
						<?php }else{ ?>
						 	<?php echo $customer[0]->customerName ?>
						<?php } ?>
						 - 
						<?php if($customer[0]->org_type == '' || $customer[0]->org_type == null){ ?>
						<?php }else{ ?>
							<?php echo $customer[0]->org_type ?>
						<?php } ?></b></span> -->


						<?php	
						$job_id= isset($_GET['job_id']) ? $_GET['job_id'] :$_POST['job_id'];
						$html_action='';
				
			
				//if((isset($frame['allow_edit']) && $frame['allow_edit'] == '1') || $frame['Properties'][0]->AllowEdit == '1')	{
					echo $html_action .= '<a  style="display:none;" id="customerEditView"  data-pfid="144" class="bsit-delete-btn  bsit-page-frame" data-collid="117" data-recid="'.$job_id.'" data-modal="edit">
										<div class="BSIT-TableActions hasTooltip bsit_opp_delete_edit" style="margin-right:10px;opacity:1;">
											<i class="fa fa-pencil-square-o" aria-hidden="true" style="font-weight:bold;font-size:22px;color: #2196f3;"></i>
											<span class="icon_tooltip">Edit </span>
										</div>
									</a>';  
					
									//} 
									?>
						<!-- //customerName -->
					<div class="right">
						<a target="_blank" href="OrgDetails?_id=<?php echo $customer[0]->orgs_id ?>&name=<?php echo $customer[0]->customerName ?>&">
							<h3 class="but">Customer</h3>
						</a>
					</div>
				</div>
				<?php 
				//echo "<pre/>";print_R($customer); echo count($customer);
				?>
				<div style="clear:both;"></div>
					<!-- <div class="bsit_opp_overview_value">
						<?php if($customer[0]->deal_value == '' || $customer[0]->deal_value == null){ ?>
							<h3>$0</h3>
						 <?php }else{ ?>
						 	<h3 id="estimated_value">$<?php echo number_format($customer[0]->deal_value, 2, '.', ',') ?></h3>
						 <?php } ?>
						<label>Deal Value</label>
					</div>
					<div class="bsit_opp_overview_value">
						
						<?php if($customer[0]->chance_of_winning == '' || $customer[0]->chance_of_winning == null){ ?>
							<h3>0%</h3>
						 <?php }else{ ?>
						 	<h3><?php echo $customer[0]->chance_of_winning ?>%</h3>
						 <?php } ?>
						
						<label>Chance of winning</label>
					</div> -->
					<div class="bsit_opp_overview_value">
						
						<?php if(($customer[0]->est_12mth_sales_val == '' || $customer[0]->est_12mth_sales_val == null) && ($customer[0]->chance_of_winning == '' || $customer[0]->chance_of_winning == null)){ ?>
							<h3>$0</h3>
						 <?php }else{ ?>
						 	<h3>$<?php echo (($customer[0]->est_12mth_sales_val * $customer[0]->chance_of_winning) / 100) ?></h3>
						 <?php } ?>
						
						<label>Opportunity Value</label>
					</div> 
					<div class="bsit_opp_overview_value">
						<?php if(($customer[0]->scheduleallHrsTotal == '' || $customer[0]->scheduleallHrsTotal == null)){ ?>
							<h3>0%</h3>
						 <?php }else{ ?>
						 	
						 	<h3><?php echo round((($customer[0]->scheduleallHrsTotal) * 100) /$customer[0]->est_duration_hours )?>%</h3>
						 <?php } ?>
						<?php /*if($customer[0]->scheduleHrsTotal == '' || $customer[0]->scheduleHrsTotal == null){ ?>
							<h3>0%</h3>
						 <?php }else{ ?>
						 	<h3><?php echo round($customer[0]->scheduleHrsTotal) ?>%</h3>
						 <?php } */?>
						
						
						<label>Scheduled vs Estimated Hours</label>
					</div>
					<div class="bsit_opp_overview_value">
						<?php if($customer[0]->est_duration_hours == '' || $customer[0]->est_duration_hours == null){ ?>
							<h3>0</h3>
						 <?php }else{ ?>
						 	<h3><?php echo $customer[0]->est_duration_hours ?></h3>
						 <?php } ?>
						
						<label>Sales Hour Total</label>
					</div>
					<div class="bsit_opp_overview_value">
						
						<?php if($customer[0]->dev_hours_logged == '' || $customer[0]->dev_hours_logged == null){ ?>
							<h3>0</h3>
						 <?php }else{ ?>
						 	<h3><?php echo $customer[0]->dev_hours_logged ?></h3>
						 <?php } ?>
						
						<label>Dev Hours Logged</label>
					</div>
					<a class="bsit_opp_overview_value" target="_blank" href="projects?CustomerID=<?php echo $customer[0]->orgs_id ?>&OpportunityID=<?php echo $job_id ?>">
					
						
						<?php if($customer[0]->TotalTask == '' || $customer[0]->TotalTask == null){ ?>
							<h3>0</h3>
						 <?php }else{ ?>
						 	<h3><?php echo $customer[0]->TotalTask ?></h3>
						 <?php } ?>
						<label style="cursor: pointer;">Number of Open Projects</label>
						
					
					</a>
					<a  class="bsit_opp_overview_value" target="_blank" href="developmenttasks?customer_id=<?php echo $customer[0]->orgs_id ?>&job_id=<?php echo $job_id ?>&">
					
						
						<?php if($customer[0]->TotalDevTask == '' || $customer[0]->TotalDevTask == null){ ?>
							<h3>0</h3>
						 <?php }else{ ?>
						 	<h3><?php echo $customer[0]->TotalDevTask ?></h3>
						 <?php } ?>
						<label style="cursor: pointer;">Number of Open Development Task</label>
						
					
					</a>
					<div class="bsit_opp_overview_value" style="background: #2196f345;">
						
						<?php if($customer[0]->assigned_to_username == '' || $customer[0]->assigned_to_username == null){ ?>
							<h3>-</h3>
						 <?php }else{ ?>
						 	<h3><?php echo ucfirst($customer[0]->assigned_to_username) ?></h3>
						 <?php } ?>
						
						<label>Owner</label>
					</div>
					


					
			</div>
			<?php /* ?><div class="bsit_opp_overview_client_details">
				<div class="bsit_client_details_info">
					<h3>CUSTOMER</h3>
					<div class="bsit_client_block bsit_client_name">
						<label>Customer</label>
						<?php if($customer[0]->customerName == '' || $customer[0]->customerName == null){ ?>
							<p>--</p>
						<?php }else{ ?>
						 	<p><?php echo $customer[0]->customerName ?></p>
						<?php } ?>
					</div>
					<div class="bsit_client_block bsit_client_address">
						<label>Loc</label>
						<div id="bait_map"></div>
						<p>
							<?php echo $customer[0]->custadd; ?>
							<!-- <?php echo $customer[0]->address_line1 ?> <?php echo $customer[0]->address_line2 ?>, <?php echo $customer[0]->town ?> <?php echo $customer[0]->state ?> <?php echo $customer[0]->postal_code ?> -->
							
						</p>
						
						
						
						<?php	
						$address = $customer[0]->address_line1.','.$customer[0]->address_line2.','.$customer[0]->town.','.$customer[0]->state.','.$customer[0]->postal_code ;   
    						?>
						
						
					<!-- <div style="width: 100%"><iframe width="100%" height="100" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBFw-9gXHseXjWwddtvNNncvFgkhBYJu7g&q=<?php echo $address ?>&zoom=20&maptype=satellite"></iframe></div> -->

					</div>
						<div class="bsit_client_block bsit_client_opp_owner">
							<label>Type</label>
							<?php if($customer[0]->org_type == '' || $customer[0]->org_type == null){ ?>
								<p>--</p>
							<?php }else{ ?>
							 	<p><?php echo $customer[0]->org_type ?></p>
							<?php } ?>
						</div>
					
				</div>
				<?php if(count($customer) >= 1){
					foreach ($customer as $key => $value) { ?>
						<?php //if(($customer[$key]->contact_name != '' || $customer[$key]->contact_name != null) && ($customer[$key]->contact_phone != '' || $customer[$key]->contact_phone != null) && ($customer[$key]->contact_email != '' || $customer[$key]->contact_email != null)){ ?>
					<div class="bsit_client_details_info">
						<h3>CUSTOMER CONTACTS</h3>
						<div class="bsit_client_block bsit_client_name">
							<label>Customer</label>
							<?php if($customer[$key]->contact_name == '' || $customer[$key]->contact_name == null){ ?>
								<p>--</p>
							<?php }else{ ?>
							 	<p><?php echo $customer[$key]->contact_name ?></p>
							<?php } ?>
						</div>
						<div class="bsit_client_block bsit_client_phone">
							<label>Phone</label>
							<?php if($customer[$key]->contact_phone == '' || $customer[$key]->contact_phone == null){ ?>
								<p>--</p>
							<?php }else{ ?>
							 	<p><?php echo $customer[$key]->contact_phone ?></p>
							<?php } ?>
						</div>
						<div class="bsit_client_block bsit_client_email">
							<label>Email</label>
							<?php if($customer[$key]->contact_email == '' || $customer[$key]->contact_email == null){ ?>
								<p>--</p>
							<?php }else{ ?>
							 	
								<a href="mailto:<?php echo $customer[0]->email ?>" onclick="window.location=another.html"><p><?php echo $customer[$key]->contact_email ?></p></a>
							<?php } ?>					
							
						</div>
						
					</div>
						<?php //} ?>
				<?php }
				}  ?>
				
				
			</div> <?php */ ?>
		</div>
	

	