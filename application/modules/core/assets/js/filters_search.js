// jQuery document
$( document ).ready(function() {
 
    
  // --------------------------------
  // Search Box
  // --------------------------------
  //setup before functions
  var typingTimer;                //timer identifier
  var doneTypingInterval = 500;  //time in ms, 1/2 a second while set to 500
  var $searchInput = $('#filterSearchBox');
  
  //on keyup, start the countdown
  $searchInput.on('keyup', function () {
    // show the user we are loading
    var loadingHTML = '<li class="noResult"><p>Loading...</p></li>';
    showResultsArea(loadingHTML);
        
    // reset the timer
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
  });
  
  //on keydown, clear the countdown 
  $searchInput.on('keydown', function () {
    clearTimeout(typingTimer);
  });

  //user is "finished typing," do something
  function doneTyping () {
    // firstly get the value of the input field
    var $searchTerm = $searchInput.val();
          
    // Fire the AJAX call to return the list of matching terms
    var $listHTML = '';
    var $searchName = '';
    
    // we only do the search if the use has provided at least 1 character
    if($searchTerm.length > 0){
      $.ajax({
         url: base_url + "core/filters",
        
        type: 'GET',
        data: {'searchTerm': $searchTerm},
        cache: false,
        dataType : 'json',
        success: function(data) {
        $listHTML = '';
        $.each(data, function() {
          $searchName = this.search_value;
          if(this.search_value !== this.search_term){
            $searchName = this.search_value + ': '+this.search_term;
          }
          $listHTML = $listHTML +'<li class="filterSearchResult" data-dimid="'+this.dim_id+'" data-dimvalue="'+this.search_value+'" data-dimname="'+this.dim_name+'">';
          $listHTML = $listHTML + $searchName+'<br/><span class="secondaryTxt">'+this.dim_name+'</span></li>';
        });
        
        // do we have any results?
        if($listHTML === ""){
          $listHTML = '<li class="noResult"><p>Your search <b>'+$searchTerm+'</b> did not match any known dimensions. <br/></p>';
          $listHTML = $listHTML + '<p> Suggestions: <br/> - Make sure the words are spelt correctly <br/> - Try and different keyword <br/> - Find the search term using Analysis; Group results by the dimension you need</p></li>';     
        }
        
        // show the results under the search bar
        showResultsArea($listHTML);

        },
        error: function(xhr, desc, err) {
        console.log(xhr + "\n" + err);
        }
      });
    }
    
    // was the search term populated
    if($searchTerm.length === 0){
      $listHTML = '<li class="noResult"><p>Search for any <b>Dimension</b></p> <p>A list can be found in the Analysis page group by list.</p>';
      $listHTML = $listHTML + '</li>';
      
      // show the box with instructions
      showResultsArea($listHTML);
    }
  }

  // --------------------------------
  // clear filter
  // --------------------------------   
  $('#filtersClearSearch').on('click', function() {
    clearFiltersSearch();   
  });
  
  // --------------------------------
  // show advanced search drop down
  // --------------------------------   
  $('#advancedSearchDropArrow').on('click', function() {
    showHideAdvancedSearch();   
  });

  // --------------------------------
  // Select a filters results 
  // --------------------------------
  $('#filterSearchResultsList').on('click', 'li.filterSearchResult', function() {
    // close the results area of search
    clearFiltersSearch(); 
    
    // get the data needed, both ID and value
    var $dimID    = $(this).data('dimid');
    var $dimValue = $(this).data('dimvalue');
    var $dimName  = $(this).data('dimname');
    
    // add this option to the list
    addFilter($dimID,$dimValue,$dimName,'=');
    
    /*var $liHTML   = '<li  data-dimid="'+$dimID+'" data-dimvalue="'+$dimValue+'" data-dimname="'+$dimName+'" data-dimopp="=" data-dimperm="0">';
    $liHTML     = $liHTML +$dimValue+'<br/><span class="secondaryTxt">'+$dimName+' =</span>';
    $liHTML     = $liHTML +'<a class="dimRemover" href="javascript:void(0)">x</a></li>';
    $('#dimSelectedUL').append($liHTML);*/
  }); 
  
  // --------------------------------
  // Remove a filter 
  // --------------------------------
  $('#dimSelectedUL').on('click', 'li a.dimRemover', function() {
    // get parent values (the one clicked on)
    $(this).parent().remove();
    
  }); 

  // --------------------------------
  // advanced search date pickers
  // --------------------------------
  var dateFormat = 'yy-mm-dd';
  var from = $( "#fromDate" ).datepicker({
        //maxDate: "-1D",
        dateFormat: dateFormat,
        showOn: "button",
        buttonImage: base_url+"assets/core/images/calendar.png",
        buttonImageOnly: true,
        buttonText: "Select After Date"
      }).on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
      });
  var to = $( "#toDate" ).datepicker({
        //maxDate: "1D" 
        dateFormat: dateFormat,
        showOn: "button",
        buttonImage: base_url+"assets/core/images/calendar.png",
        buttonImageOnly: true,
        buttonText: "Select Before Date"
      }).on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
  function getDate( element ) { 
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  
  // --------------------------------
  // advanced search filter on date
  // --------------------------------
  $('#advancedSearchFilterDate').on('click', function() {
    // get the from and to dates
    var afterDate   = $("#fromDate").val();
    var beforeDate  = $("#toDate").val();
    var afterSet  = false;
    var beforeSet = false;
    
    // if afterDate is populated, setup filter
    if(afterDate !== '' && typeof afterDate != 'undefined'){
      afterSet = true;
    }
    
    // if afterDate is populated, setup filter
    if(beforeDate !== '' && typeof beforeDate != 'undefined'){
      beforeSet = true;
    }
    
    // depending on the option set, we now create the new filter
    if(beforeSet === true && afterSet === true){
      // date between given dates
      addFilter('23',afterDate+"' and '"+beforeDate,'date','between',1,afterDate+" and "+beforeDate);
    }else if(afterSet === true){
      // date after given date
      addFilter('23',afterDate,'date','>=',1);
    }else if(beforeSet === true){
      // date before given date
      addFilter('23',beforeDate,'date','<=',1);
    }else{
      // return without closing the popup -- basically do nothin  
      return false; 
    }
    
    // hide advanced search
    showHideAdvancedSearch();
    return false; 
  });
});

// --------------------------------
// Show the results or loading or message about no results
// -------------------------------- 
function showResultsArea($inputHTML){
  // show the cross allowing clear of search field
  $('#filtersClearSearch').show();
  
  // hide the advanced search and arrow
  $('#advancedSearchDropArrow').hide();
  $('#advancedSearch').hide();
  
  // set the html to the newly loaded html and show the results
  $('#filterSearchResultsList').html($inputHTML);
  $('#filterSearchResults').show();
}


// --------------------------------
// Close search box - clear results
// -------------------------------- 
function clearFiltersSearch(){
  // show the search box
  $('#filterSearchResults').hide();
  
  // hide the clear button
  $('#filtersClearSearch').hide();
  
  // show advanced search arrow
  $('#advancedSearchDropArrow').show();
  $('#advancedSearchDownImg').show();
  $('#advancedSearchUpImg').hide();
  
  // clear the input field
  $('#filterSearchBox').val('');  
}

// --------------------------------
// show advanced search
// -------------------------------- 
function showHideAdvancedSearch(){
  // show advanced search arrow
  $('#advancedSearch').toggle();
  
  // change the down/up image
  $('#advancedSearchDownImg').toggle();
  $('#advancedSearchUpImg').toggle();
}

// --------------------------------
// add a filter 
// --------------------------------
// return 0 = all good, created
// return 1 = error, unknown
// return 2 = already created
function addFilter(dimID,dimValue,dimName,dimOpper,uniqueAdhock = 0,display = ''){
	
  var curFilterDimID    = '';
  var curFilterDimValue = '';
  var curFilterDimOpp   = '';
  var curFilterDimPerm  = '';
  var createNewFilter   = true;
  // does the filter already exist?
  $("#dimSelectedUL li").each(function() {
    // get the data needed; ID, value and opp
    curFilterDimID    = $(this).data('dimid');
    curFilterDimValue = $(this).data('dimvalue');
    curFilterDimOpp   = $(this).data('dimopp');
    curFilterDimPerm  = $(this).data('dimperm');
    
    // if unique is specified, any matching adhock filters should be removed
    if(uniqueAdhock == 1 && curFilterDimPerm == 0 && curFilterDimID == dimID){
      $(this).remove();
    }else{    
      // check if this (in loop) is the same filter we are about to add
      if(curFilterDimID == dimID && curFilterDimValue == dimValue && curFilterDimOpp == dimOpper){
        createNewFilter = false;
        return 2;
      }
    }
  });
  
  // should we?
  if(createNewFilter === true){
    // if the display is empty, use value
    if(display === ''){
      display = dimValue;
    }
    // add the filter
    var $liHTML   = '<li  data-dimid="'+dimID+'" data-dimvalue="'+dimValue+'" data-dimname="'+dimName+'" data-dimopp="'+dimOpper+'" data-dimperm="0">';
    $liHTML     = $liHTML +display+'<br/><span class="secondaryTxt">'+dimName+' '+dimOpper+'</span>';
    $liHTML     = $liHTML +'<a class="dimRemover" href="javascript:void(0)">x</a></li>';
    $('#dimSelectedUL').append($liHTML);
    
    return 0;
  }
  return 1;
}
