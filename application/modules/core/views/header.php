﻿
<!doctype html>
<html>

<!--this file inluded for js,css and for notes section and add colletion this is required to include -->
   <?php require_once APPPATH.'modules/core/views/header_include.php' ?>

<!-- headerPage is 1  when we open page in the iframe -->
<?php

 if($pageMenu == 0){
     $className='hide-sidedrawer';
   } else {
     $className='showsidedrawer';
   }
   
   
if ($_GET['headerPage'] == 1) {
?>
<body style="background-color: white!important;" class="<?php echo $className;?>">
<?php
} else {
?>
<body class="<?php echo $className;?>">
<?php
}
?>

<?php
if ($_GET['headerPage'] != 1) {
   if($pageMenu ==1){
  
?>
<div id="sidedrawer" class="mui--no-user-select">
  
  <?php echo($menu_data); ?>
</div>
<?php }?>
<?php if($header_view == 1){?>

<header id="header">

<?php
    // If the user has permission, load the editbar
    if($this->session->userdata('Allowdesignmode') == 1) {
      echo('<script src="'.base_url().'core/assets/js/PageEdit.js"></script>');
      $this->load->view('editbar', true);
    }
?>

  <div class="mui-appbar mui-container-fluid mui--appbar-line-height">
    <nav class="bsit_main_nave">
      <input type = "hidden" id="pageIdHeader" value="<?php echo $pageInfo->pageId; ?>">
      <ul class="bsit_header_section">
        <li class="bsit_header_menu_icon">
          <a class="sidedrawer-toggle mui--visible-xs-inline-block js-show-sidedrawer">☰</a>
          <!-- Code start for Add page name in Header Added by HD -->
          <a class="sidedrawer-toggle mui--hidden-xs js-hide-sidedrawer"
            id="control_title"><?php if($pageMenu == 1){?>☰<?php }?>&nbsp;&nbsp;<?php echo($pageInfo->Title); ?></a>
          <span class="mui--text-title mui--visible-xs-inline-block">
            <?php
              echo strlen($pageInfo->Title) > 20 ?
                substr($pageInfo->Title, 0, 20)."..." :
                $pageInfo->Title;
            ?>
          </span>
          <!-- Code end for Add page name in Header Added by HD -->
        </li>
       <!--  <td class="mui--appbar-height search_bar" align="center">
                  <div class="input-group header_search">
          <input type="text" class="form-control searchData" id="searchAllData" placeholder="Search All" value="">
          <div class="input-group-append">
            <button class="btn btn-secondary cola_common_search" type="button" onclick="">
              <i class="fa fa-search "></i>
            </button>
          </div>
        </div>
              </td> -->
              <!--Add for Search -->
    <?php 
     $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if(($pageInfo->enable_quickfilter_search == 1) || (strpos($actual_link, "home") != false) ){ ?>
      <li class="search_bar bsit_header_search_bar">
      <div class="input-group header_search ui-front bsit_header_search">
         <input type="text" class="form-control searchData searchData" id="searchData" placeholder="Search All" value="<?php echo $this->session->userdata('searchData'); ?>">
          <div class="input-group-append">
            <button class="btn btn-secondary bsit_common_search" type="button" onclick="">
              <i class="fa fa-search "></i>
            </button>
          </div>
     </div>
    </li>
    <?php } ?>
    <!--end of Search -->
        <li class="bsit_menu_user_icon">
          <ul class="bsit_header_menu">
            <li class="bsit_header_menu_li">
              <a class="bsit_header_profile">
        
        
        <?php
        
        $user = $this->session->userdata('FirstName');
        $username = trim($user,' ');
       
        $img_path = base_url()."core/assets/userimage/".strtolower($username).".jpg";
       
        
        if (! file_exists(APPPATH."modules/core/assets/userimage/".strtolower($username).".jpg")) {
              $img_path = base_url()."assets/core/images/top_header/user.svg";
            } 
        
        ?>
        <img src="<?php echo $img_path?>"  title="profile"/>




        <img src="<?php echo base_url();?>assets/core/images/arrow-down.svg" style="width: 11px; height: auto; filter: brightness(0) invert(1); -webkit-filter:invert(-1);" title="profile"/></a>
              <ul class="bsit_header_sub_menu">
                
                  <?php

                    // If user has permissions to edit pages, show the button to open the edit bar
                    if($this->session->userdata('Allowdesignmode') == 1) {
                        $btn = '<li id="bsit_header_hide">';
                        $btn .= '<a id="editBtn">';
                        $btn .= '<img src="'.base_url().'assets/core/images/top_header/edit_page.png" title="Edit Mode">';
                        $btn .= '<span>Design Mode</span></a>';
                        $btn .= '</li>';
                        echo($btn);
                    }
                  ?>
                
                <li>
                  <a id="change-password">
                    <img src="<?php echo base_url();?>assets/core/images/top_header/change_password.png" title="Change Password" class="change-password-icon"/><span>Change Password</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0);" id="logoutBtn">
                    <img src="<?php echo base_url();?>assets/core/images/top_header/logout.png" title="Logout"/>
                    <span>Logout</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
     
      <div class="btn-group bsit_notifications_block">

       <?php if (!empty($notifications)) { ?>
               <button type="button" class="dropdown-toggle bsit_notifications_icon notificationsDropdown" id="notificationsDropdown" data-toggle="dropdown"><span id="notificationCount"><?php echo count($notifications); ?></span><img src="<?php echo base_url();?>assets/core/images/notification.png">
            </button>

       <?php } ?>
           
            <ul class="dropdown-menu" role="menu" id="bsit_notifications_footer">
              <li>
                <ul class="mCustomScrollbar">
                <?php 

                foreach($notifications as $key=>$data)
                { ?>
                  <li class="notificationDismissAllrecordData" notificationId="<?php echo $data->_id; ?>" id="notification<?php echo $data->_id; ?>">    
                  <div class="notificationFilter" notificationId="<?php echo $data->_id; ?>" notificationLinkUrl="<?php echo $data->link_url; ?>">             
                    <div class="bsit_notifications_description" ><span><?php echo $data->description;  ?></span></div>
                    <!-- <p class="bsit_notifications_description"><?php echo  date("d/m/Y h:m:s" ,strtotime($data->create_date));?></p> -->
					<?php 
                      $timezone_offset_minutes = $this->session->userdata('timezone'); 
                      $dt = new DateTime($data->create_date, new DateTimeZone('UTC'));
                      $dt->setTimezone(new DateTimeZone($timezone_offset_minutes));
                      $dateString = $dt->format('d/m/Y h:i:s');
                    ?>
                    <p class="bsit_notifications_description"><?php echo $dateString  ?></p>
					
                    <?php  if($data->user_id == 1){
                      $usearName = "Auto Created";
                    } else{
                      $usearName = $data->username;
                    } 

                      if($usearName == ''){
                        $usearName = '--';
                      }

                    ?>

                    <p class="bsit_notifications_description"><?php echo $usearName ?></p>
                   </div> 
                    <span class="bsit_notifications_read close_notification" id="close_notification" notificatioId="<?php echo $data->_id;  ?>">&#x2715</span>
                    <!-- <div id="bsit_close_notification" class="lds-dual-ring"></div> -->
                    <div class="lds-dual-ring bsit_close_notification1"></div>
                    <div id="bsit_close_notification_<?php echo $data->_id; ?>" class="lds-dual-ring"></div>
                  </li>
               <?php }

                   ?>
                 
                </ul>
              </li> 
              <li class="bsit_notifications_footer">
                <a class="bsit_notifications_view" id="redirectNotificationPage">View All</a>
                <a class="bsit_notifications_dismiss" id="dismissAllrecords">Dismiss All</a>
              </li>                            
            </ul>

           
          </div>
        </li>
      </ul>
    </nav>
  </div>

</header>
<!-- Search box add css start-->
<style type="text/css">
  /*.input-group.header_search {
    display: flex;
    width: 60%;
    justify-content: center;
    align-items: center;
  }
*/
  /*.input-group-append {
    display: flex;
    margin-left: -3px;
  }

  .input-group.header_search .searchData {
    box-shadow: none;
    border: 0;
    border-radius: 6px 0 0 6px;
  }
  .input-group-append .btn-secondary {
    border-right: 0 6px 6px 0;
    background-color: #ffffff;
    color: #000000;
  }*/
  /*<!-- Search box add css End-->*/
</style>

<!-- <script type="text/javascript">
$(document).ready(function() {

  $(document)
  .button()
  .on('click', '.cola_common_search', function(e)
  {
    var searchData = $('#searchAllData').val();
    // debugger  jb._id = V(job_id)
    $.ajax({
      type: 'POST',
      url: base_url + "jobs/searchall/getgenericallsearch",
      data: { searchData:searchData},
      dataType: 'json',
      success: function(res)
        {
          debugger
          var allSearchData = res.generic_all_search;
          var currentUrl = window.location.href;
          if(base_url+'page/search_all' != currentUrl)
          {
            var moduleUrl = base_url+'page/search_all';  
            window.open( moduleUrl, '_blank');
          }else{
            location.reload();
          }
        },
        error: function(err)
        {
            if(err.status == 404){
              err.responseText = 'The requested url was not found on this server.';
            } else if(err.status == 408) {
              err.responseText = 'API request timeout';
            } else if(err.status == 500 || err.status == 503) {
              err.responseText = 'Service Unavailable';
            } 
        },
    });

  });

});
</script> -->

<?php
} }
?>



