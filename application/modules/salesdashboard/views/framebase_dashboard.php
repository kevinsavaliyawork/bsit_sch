<?php
/*
style="position:relative; height:'.$frame_height.'px; margin-bottom: 20px;"  data-frameTitle="'.$FrameTitle.'" data-frameWidth="'.$frame_width.'"
data-frameHeight="'.$frame_height.'" data-frameOffset="'.$frame_offset.'" data-frameOrder="'.$order.'" data-frameID="'.$frameID.'"
data-frameHideAdd="'.$hide_add.'" data-frameHideFilter="'.$hide_filter.'" data-frameHidePagination="'.$hide_pagination.'"
data-frameHideDownload="'.$hide_allow_download.'" data-frameHideMultiEdit="'.$hide_multiple_edit.'" data-frameHideMultiDelete="'.$hide_multiple_delete.'" ">

<div class="edit-pointer-cancel mui-panel-dashchart" id="page_frame_'.$frameID.'" style="height:'.$frame_height.'px" ">
<!--<div class="edit-pointer-cancel table-responsive mui-panel" id="page_frame_'.$frameID.'" style="height:'.$frame_height.'px" ">-->
    <input type="hidden" class="filter_history_api_class" id="filter_history_api_'.$frameID.'" value="" >
    <input type="hidden" class="page_type" id="page_type_'.$frameID.'" value="'.$page_type.'" >
    <input type="hidden" class="page_category_used_for_url" value="'.$page_category_used_for_url.'" >
    <input type="hidden" class="editParms" value="'.$editParms.'" >
    <input type="hidden" class="filter_history_api_class_state" id="filter_history_api_state_'.$frameID.'" value="" >
    <input type="hidden" class="history_api_data" id="history_api_data_'.$frameID.'" value="" >
    <input type="hidden" id="fram_prop_filter_'.$frameID.'" value="'.$filter.'">
    '.$frame_content.'

</div>

<div id="'.$frameID.'" class="edit-div-overlay">
</div>
*/
?>

<div id="main_content_part<?php echo($frameID);?>" style="padding-bottom: 10px; padding-left: 8px; padding-right: 8px;" class="main_content_part_class <?php echo($frame_width);?>">
    <div class="mui-panel-dashchart" id="page_frame_<?php echo($frameID);?>" style="height:<?php echo($frame_height);?>px; position:relative;">
        <?php echo($frame_content);?>

        <div id="loading_overlay_frame_<?php echo($frameID);?>" class="loading-div-overlay">
          <img src="/bsit_aad/assets/images/AAD_Logo_Only_transparent.png" style="width:30px; height:30px;"/>
          <div id="loading_overlay_frame_err_<?php echo($frameID);?>" class="loading-div-overlay-error-msg"></div>
        </div>
    </div>
</div>
