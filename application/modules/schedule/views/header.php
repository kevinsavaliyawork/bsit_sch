
<!doctype html>
<html>

<!--this file inluded for js,css and for notes section and add colletion this is required to include -->
   <?php require_once APPPATH.'modules/core/views/header_include.php' ?>

<!-- headerPage is 1  when we open page in the iframe -->
<?php

 if($pageMenu == 0){
     $className='hide-sidedrawer';
   } else {
     $className='showsidedrawer';
   }
   
   
if ($_GET['headerPage'] == 1) {
?>
<body style="background-color: white!important;" class="<?php echo $className;?>">
<?php
} else {
?>
<body class="<?php echo $className;?>">
<?php
}
?>

<?php
if ($_GET['headerPage'] != 1) {
   if($pageMenu ==1){
  
?>
<div id="sidedrawer" class="mui--no-user-select">
  
  <?php echo($menu_data); ?>
</div>
<?php }?>
<?php if($header_view == 1){?>

<header id="header">

<?php
    // If the user has permission, load the editbar
    if($this->session->userdata('Allowdesignmode') == 1) {
      echo('<script src="'.base_url().'core/assets/js/PageEdit.js"></script>');
      $this->load->view('editbar', true);
    }
?>

  <div class="mui-appbar mui-container-fluid mui--appbar-line-height">
    <nav class="bsit_main_nave">
      <input type = "hidden" id="pageIdHeader" value="<?php echo $pageInfo->pageId; ?>">
      <ul class="bsit_header_section">
        <li class="bsit_header_menu_icon">
          <a class="sidedrawer-toggle mui--visible-xs-inline-block js-show-sidedrawer">☰</a>
          <!-- Code start for Add page name in Header Added by HD -->
          <a class="sidedrawer-toggle mui--hidden-xs js-hide-sidedrawer"
            id="control_title"><?php if($pageMenu == 1){?>☰<?php }?><!-- &nbsp;&nbsp;<?php echo($pageInfo->Title); ?> --></a>
          <span class="mui--text-title mui--visible-xs-inline-block bsit_display_none">
            <?php
              echo strlen($pageInfo->Title) > 20 ?
                substr($pageInfo->Title, 0, 20)."..." :
                $pageInfo->Title;
            ?>
          </span>
          <!-- Code end for Add page name in Header Added by HD -->
        </li>
        <li class="bsit_header_week_month">
          <div class="mui-select bsit_statusdiv bsit_form bsit_calendar_header" style="color: #000000;">
                      <form>
                        <select id="mySelect">
                          <!-- <option value="month" id="monthbutton">Month</option> -->
                          <option value="week" id="timelineWeek">Week</option>
                          <option value="to day" id="timelineDay">Day</option>
                          <option value="to day" id="timelineNext3Months" selected="week">3 Months</option>
                        </select>
                      </form>
                  </div>
                  <div class="fc-button-group bsit_schedule_calendar" style="float: left;">
                          <button type="button" id="prevbutton" class="fc-prev-button fc-button fc-state-default fc-corner-left" aria-label="prev"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
                          <button type="button" id="nextbutton" class="fc-next-button fc-button fc-state-default fc-corner-right" aria-label="next"><span class="fc-icon fc-icon-right-single-arrow"></span></button>
                        </div>
                </li>
                <li class="bsit_sch_month_title_name">
                  <div class="fc-center">
                            <p id="title" class="fc-title bsit_month_title_name"></p>
                          </div>
                </li>
                <!-- <td class="mui--appbar-height" align="right"> -->
                  <!-- <div class="fc-toolbar fc-header-toolbar bsit_schedule_calendar"> -->
                      
                          <!-- <div class="fc-left">
                            <div class="fc-button-group">
                              <button id="monthbutton" type="button" class="fc-month-button fc-button fc-state-default fc-corner-left">month</button>
                              <button type="button" id="weekbutton" class="fc-agendaWeek-button fc-button fc-state-default">week</button>
                              <button id="todaybutton" type="button" class="fc-agendaDay-button fc-button fc-state-default fc-corner-right">To day</button>
                            </div>
                          </div> -->
                          <!-- <div class="fc-right"> -->
                        <!-- <div class="fc-button-group">
                          <button type="button" id="prevbutton" class="fc-prev-button fc-button fc-state-default fc-corner-left" aria-label="prev"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
                          <button type="button" id="nextbutton" class="fc-next-button fc-button fc-state-default fc-corner-right" aria-label="next"><span class="fc-icon fc-icon-right-single-arrow"></span></button>
                        </div> -->
                        <!-- <button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled" disabled="">today</button> -->
                      <!-- </div> -->
                          
                          <!-- <div class="fc-clear"></div>
                  </div> -->
                <!-- </td> -->
        <li class="bsit_menu_user_icon">
          <ul class="bsit_header_menu">
            <li class="bsit_header_menu_li">
              <a class="bsit_header_profile">

                
                <?php
                
                $user = $this->session->userdata('FirstName');

                $username = trim($user,' ');
               
                $img_path = base_url()."assets/core/userimage/".strtolower($username).".jpg";
                               
                if (! file_exists(APPPATH."assets/core/userimage/".strtolower($username).".jpg")) {
                      $img_path = base_url()."assets/core/userimage/usericon.jpg";
                    } 
                
                ?>
                <img src="<?php echo $img_path ?>"  title="profile"/>
                <!-- <img src="<?php echo base_url();?>assets/core/images/top_header/user.svg" title="profile"/>  -->


                <img src="<?php echo base_url();?>assets/core/images/arrow-down.svg" style="width: 11px; height: auto; filter: brightness(0) invert(1); -webkit-filter:invert(-1);" title="profile"/></a>
              <ul class="bsit_header_sub_menu">
                
                  <?php

                    // If user has permissions to edit pages, show the button to open the edit bar
                    if($this->session->userdata('Allowdesignmode') == 1) {
                        $btn = '<li id="bsit_header_hide">';
                        $btn .= '<a id="editBtn">';
                        $btn .= '<img src="'.base_url().'assets/core/images/top_header/edit_page.png" title="Edit Mode">';
                        $btn .= '<span>Design Mode</span></a>';
                        $btn .= '</li>';
                        echo($btn);
                    }
                  ?>
                
                <li>
                  <a id="change-password">
                    <img src="<?php echo base_url();?>assets/core/images/top_header/change_password.png" title="Change Password" class="change-password-icon"/><span>Change Password</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url();?>core/Login/logout">
                      <img src="<?php echo base_url();?>assets/core/images/top_header/logout.png" title="Logout"/>
                      <span>Logout</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>

</header>

<?php
} }
?>







