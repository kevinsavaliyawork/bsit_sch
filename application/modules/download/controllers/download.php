<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class download extends brain_controller {

	function __construct() {
		parent::__construct(0);
		
		$this->load->helper('download');


		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		
	}

	function index(){
		
		$id = $this->uri->segment(3);
		$file_name = $this->uri->segment(4);

		if (($file_name) && ($id > 0)) {
		    $file = APPPATH."modules/download/data/".$id."/".$file_name;
		    
		    // check file exists    
		    if (file_exists ( $file )) {

            //download file from directory
            force_download($file, NULL);
            exit;

		    } else {
		     // Redirect to base url
		     redirect ( base_url () );
		    }
		}
	}
}
