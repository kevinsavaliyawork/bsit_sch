<!--
1 - chart_title
2 - chart_subhead
3 - chart_measure_descriptor
4 - chart_primary_metric
5 - chart_info_textblock
6 - chart_secondary_metric
-->
<div class="chart_main">
  <div class="chart_head">
    <div class="chart_title">Electricity Use Stats</div>
    <div class="chart_chart_subhead"></div>
  </div>
  <div class="chart_body">
    <div class="chart_metric_50">
      <div class="chart_primary_metric">
        <span id="elecKwPerLt">...</span><span class="chart_primary_symbol">kWh</span>
      </div>
      <div class="chart_secondary_metric">
        $<span id="elecDollarPerLt">...</span>
      </div>
      <div class="chart_image_cont">
        <img src="<?php echo base_url();?>assets/images/dashboard_images/milk-can.jpg"/>
      </div>
      <div class="chart_measure_descriptor">
        PER 1000 LITRES
      </div>
    </div>
    <div class="chart_metric_50">
      <div class="chart_primary_metric">
        <span id="elecKwPerDay">...</span><span class="chart_primary_symbol">kWh</span>
      </div>
      <div class="chart_secondary_metric">
        $<span id="elecDollarPerDay">...</span>
      </div>
      <div class="chart_image_cont">
        <img src="<?php echo base_url();?>assets/images/dashboard_images/day.png"/>
      </div>
      <div class="chart_measure_descriptor">
        Per Day
      </div>
    </div>
  </div>
</div>
