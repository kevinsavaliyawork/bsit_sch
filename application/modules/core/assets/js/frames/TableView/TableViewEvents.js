import {
  applyFilter,
  commitDelete,
  commitEdit,
  //commitEditSidebar,
  commitSave,
  //commitSidebarSave,
  commitMultiEditDelete,
  filterFieldChange,
  getFrameData,
  paginationClick,
  removeFilter,
  rowCountChange,
  showEditDeleteModal,
  //showDeleteModalSidebar,
  //showEditSideBar,
  showMultiEditDeleteModal,
  //showMultiDeleteModal,
  showMultiEditDeleteModalSidebar,
  showMultiDeleteModalSidebar,
  sortHeaderClick,
  pagerefrace,
  showHideQuickFilter,
  getQuickFilterValues,
  performFilter,
  notificationUpdate,
  notificationRedirectUpdate,
  // seeMoreRecords_seeLessRecords,
  setValueOfAdvanceAutoComplete,
  setValueOfAutoComplete,
  resetFormFields,
  setupFramePagination,
  showLoader,
  hideLoader,

} from './TableView.js';

import {
  getFrameFromElement
} from '../../Page.js';


$(document).ready(() => {
  window.addEventListener("popstate", function (e) {
    location.reload();
  });
});


$(document).on('mousedown', '.modal-backdrop', function (e) {
  window.clickStartedInModal = $(e.target).is('.modal-dialog *');
});

$(document).on('mouseup', '.modal-backdrop', function (e) {
  if (!$(e.target).is('.modal-dialog *') && window.clickStartedInModal) {
    window.preventModalClose = true;
  }
});

$(".modal-backdrop").on("hide.bs.modal", function (e) {
  if (window.preventModalClose) {
    window.preventModalClose = false;
    return false;
  }
});


function onclickClose() {
  $(`.modal-backdrop`).on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
  });
}

// Frame filter field changed
$(document).on('change', '.bsit-filter-name', function (e) {
  filterFieldChange(e, getFrameFromElement(e.target));
});

//lazy load code
$('#menu-nav li a').click(function (e) {
  if ($(".bsit_checkmark_design_mode").length == 0) {

    var has_data_loaded = $(this).attr("has_data_loaded");
    if (has_data_loaded > 0) {
      $(this).attr("has_data_loaded", 0);
      var fm = getFrameFromElement(e.target);
      getFrameData(fm, 0)
        .then(() => {
          setupFramePagination(fm);
        });
    }
  }
});

// Applying filters
$(document)
  .button()
  .on('click', '.bsit-filter-btn', function (e) {
    applyFilter(getFrameFromElement(e.target));
  });

// Enter on filter search.
$(document)
  .button()
  .on('keyup', '.bsit-filter-text', function (e) {
    // alert("123");


    if (e.which === 13) {
      applyFilter(getFrameFromElement(e.target));
    }
  });

// Removing filters
$(document).on('click', '.cancel_filter', function (e) {
  removeFilter(e, getFrameFromElement(e.target));
});

// remove sidebar on click
$(document).on('click', '#bsit_header_hide', function (e) {

  $('body').removeClass('open-bg');
  $('.modal-backdrop').addClass('bsit_out');
  // $(`#addModel${frame.PFID}`).removeClass('open');
  // $(`#editModel${frame.PFID}`).removeClass('open');  

});

// Deleting records using delete modal
$(document)
  .button()
  .on('click', '.bsit-commit-delete', function (e) {
    showLoader(e, true, true);
    commitDelete(getFrameFromElement(e.target));
  });

// Updating records using edit modal
$(document)
  .button()
  .on('click', '.bsit-commit-edit', function (e) {
    // var obj_data=jQuery(this).parent().find('.mui-col-md-12').find(".multiSelect-ul").find("li");
    showLoader(e, true, true);

    var obj_data = $(".multiSelect-ul").find("li");
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect_value").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".advanceMultiSelectRemoveVal").val('');
    // alert(JSON.stringify(obj_data));
    $.each(obj_data, function (i, e) {
      var classname = $(this).attr("class");
      if (classname != "li-input") {
        $(this).remove();
      } else {
        $(this).find("input").val("");
      }
    });
    commitEdit(getFrameFromElement(e.target), "modal", e);
  });

// Updating records using edit Sidebar
$(document)
  .button()
  .on('click', '.bsit-commit-edit-update', function (e) {
    showLoader(e, true, true);

    var obj_data = jQuery(this).parent().find('.mui-col-md-12').find(".multiSelect-ul").find("li");
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect_value").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".advanceMultiSelectRemoveVal").val('');
    // alert(JSON.stringify(obj_data));
    $.each(obj_data, function (i, e) {
      var classname = $(this).attr("class");
      if (classname != "li-input") {
        $(this).remove();
      } else {
        $(this).find("input").val("");
      }
    });
    commitEdit(getFrameFromElement(e.target), "sidebar", e);
    //commitEditSidebar(getFrameFromElement(e.target), "editsideModel");
  });

$(document)
  .button()
  .on('click', '.bsit_form_close_btn', function () {

    // $('.multiSelect-ul li:not(:last)').remove();
    var obj_data = jQuery(this).parent().find(".multiSelect-ul").find("li");
    // alert(obj_data);
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect_value").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".advanceMultiSelectRemoveVal").val('');

    $.each(obj_data, function (i, e) {
      var classname = $(this).attr("class");
      if (classname != "li-input") {
        $(this).remove();
      } else {
        $(this).find("input").val("");
      }
    });

  });

// Add button modal
$(document)
  .button()
  .on('click', '.bsit-btn-add', function (e) {

    var obj_data = $(".multiSelect-ul").find("li");
    $('#hf_advanceMultiSelect').val('');
    $('#hf_advanceMultiSelect_value').val('');
    $('.advanceMultiSelectRemoveVal').val('');
    $.each(obj_data, function (i, e) {
      var classname = $(this).attr("class");
      if (classname != "li-input") {
        $(this).remove();
      } else {
        $(this).find("input").val("");
      }
    });

    // return
    let pfid = $(e.target)
      .closest('.bsit-page-frame')
      .data('pfid');

    var enablePaneView = $(`#enable_pane_view_` + pfid).val();
    let frame = getFrameFromElement(e.target);
    let form = $(`#addForm${frame.PFID}`);

    if (!enablePaneView || enablePaneView == 0) {
      // Set modal title and show the modal
      $(`.modal_${frame.PFID} h2`).html(`Add ${page_title}`);
      $(`#addModel${frame.PFID}`).modal({
        backdrop: 'static'
      });
      //set default value to full name at add time
      let autocompleteSelectBox = $(form).find("input.autoCompCollAdv");
      $.each(autocompleteSelectBox, function (key, element) {
        setValueOfAdvanceAutoComplete($(element))
      });
      let autoCompCollBox = $(form).find("input.autoCompColl");
      $.each(autoCompCollBox, function (key, element) {
        //setValueOfAdvanceAutoComplete($(element))
        setValueOfAutoComplete($(element))
      });
      onclickClose();

    } else {

      // Set modal title and show the modal
      $(`.modal_${frame.PFID} h2`).html(`Add ${page_title}`);
      // $(`#mySidenav${frame.PFID}`).modal();
      $(`#addModel${frame.PFID}`).addClass('open');
      $('body').addClass('open-bg');

      $(`#mySidenavbar${frame.PFID}`).removeClass('open');
      $(`#editModel${frame.PFID}`).removeClass('open');
      $(`#deleteModel${frame.PFID}`).removeClass('open');
      $(`#sidemultiEditModel${frame.PFID}`).removeClass('open');
      $(`#sidemultiDeleteModel${frame.PFID}`).removeClass('open');
      $('.modal-backdrop').addClass('bsit_out');

      $(`#closeNavbar${frame.PFID}`).click(function () {
        $(`#addModel${frame.PFID}`).removeClass('open');
        $('body').removeClass('open-bg');

        // var obj_data=$(".multiSelect-ul").find("li");
        // $('#hf_advanceMultiSelect').val('');
        // $('#hf_advanceMultiSelect_value').val('');
        // $('.advanceMultiSelectRemoveVal').val('');

        // $.each(obj_data,function(i,e){
        //   var classname=$(this).attr("class");
        //     if(classname!="li-input")
        //     {
        //       $(this).remove();
        //     }
        //     else
        //     {
        //       $(this).find("input").val("");
        //     }
        // });

      });

    }

    //set adv. collection box value to its default id
    $(`#addModel${frame.PFID}`).on('hidden.bs.modal', function () {
      $(`#addForm${frame.PFID}`).trigger('reset');
      resetFormFields(form);
    })

  });

// Saving new records using add modal
$(document)
  .button()
  .on('click', '.bsit-add-new-save', function (e) {
    showLoader(e, true, true);
    commitSave(getFrameFromElement(e.target), "modal", e);
  });

// Saving new records from the 'ADD' sidebar
$(document)
  .button()
  .on('click', '.bsit-add-new-sidebar-save', function (e) {
    showLoader(e, true, true);
    commitSave(getFrameFromElement(e.target), "sidebar", e);
    // commitSidebarSave(getFrameFromElement(e.target));
  });

// Showing edit modals and side bar
$(document)
  .button()
  .on('click', '.bsit-edit-btn', function (e) {
    // alert('hi');
    var obj_data = jQuery(this).parent().find('.mui-col-md-12').find(".multiSelect-ul").find("li");
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".hf_advanceMultiSelect_value").val('');
    jQuery(this).parent().find(".multiSelect-ul").find(".advanceMultiSelectRemoveVal").val('');
    // alert(JSON.stringify(obj_data));
    $.each(obj_data, function (i, e) {
      var classname = $(this).attr("class");
      if (classname != "li-input") {
        $(this).remove();
      } else {
        $(this).find("input").val("");
      }
    });

    let pfid = $(e.target)
      .closest('.bsit-page-frame')
      .data('pfid');
    // $(`#addModel`+pfid).removeClass('open');
    var enablePaneView = $(`#enable_pane_view_` + pfid).val();
    // alert('enablePaneView'); 

    // if (!enablePaneView || enablePaneView == 0)
    //   showEditDeleteModal(e, getFrameFromElement(e.target));
    // else
    //   showEditSideBar(e, getFrameFromElement(e.target));

    var isPaneView = (!enablePaneView || enablePaneView == 0) ? 1 : 0;

    showEditDeleteModal(e, getFrameFromElement(e.target), isPaneView);
  });


// cancel delete sidebar
$(document)
  .button()
  .on('click', '.bsit-cancel-delete', function (e) {

    let pfid = $(e.target)
      .closest('.bsit-page-frame')
      .data('pfid');
    $(`#editModel` + pfid).removeClass('open');
    $(`#addModel` + pfid).removeClass('open');
    $(`#deleteModel` + pfid).removeClass('open');
    $(`#sidemultiEditModel` + pfid).removeClass('open');
    $(`#sidemultiDeleteModel` + pfid).removeClass('open');
    $('.modal-backdrop').addClass('bsit_out');
    $('body').removeClass('open-bg');
  });

// Showing delete modals

$(document)
  .button()
  .on('click', '.bsit-delete-btn ', function (e) {
    let pfid = $(e.target)
      .closest('.bsit-page-frame')
      .data('pfid');
    $(`#addModel` + pfid).removeClass('open');
    var enablePaneView = $(`#enable_pane_view_` + pfid).val();
    // if (!enablePaneView || enablePaneView == 0)
    //   showEditDeleteModal(e, getFrameFromElement(e.target));
    // else
    //   showDeleteModalSidebar(e, getFrameFromElement(e.target));

    var isPaneView = (!enablePaneView || enablePaneView == 0) ? 1 : 2;

    showEditDeleteModal(e, getFrameFromElement(e.target), isPaneView);
  });


// Clicking multi edit/delete checkboxes
$(document)
  .button()
  .on('click', '.edit_multi_checkbox', function (e) {

    let frame = getFrameFromElement(e.target);

    //$('.edit_multi_checkbox').each(function() {
    var id = $(this).attr('id');
    if ($('#' + id).is(':checked')) {
      $(this).closest('tr').addClass('bsit_active');
    } else {
      $(this).closest('tr').removeClass('bsit_active');
    }

    //});


    if ($('.edit_multi_checkbox:checked').length > 0) {
      $('changeme').addClass('bsit_active');
      $(`.multi_edit_from_${frame.PFID}`).show();
      $(`.multi_edit_content_${frame.PFID}`).hide();
      $(`.multi_delete_from_${frame.PFID}`).show();
      $(`.multi_delete_content_${frame.PFID}`).hide();
    } else {
      $(`.multi_edit_from_${frame.PFID}`).hide();
      $(`.multi_edit_content_${frame.PFID}`).show();
      $(`.multi_delete_from_${frame.PFID}`).hide();
      $(`.multi_delete_content_${frame.PFID}`).show();
    }
  });

// Showing MULTI edit and delete modals
$(document)
  .button()
  .on('click', '.bsit-multi-edit-btn', function (e) {
    let pfid = $(e.target)
      .closest('.bsit-page-frame')
      .data('pfid');

    var enablePaneView = $(`#enable_pane_view_` + pfid).val();
    if (!enablePaneView || enablePaneView == 0)
      showMultiEditDeleteModal(e, getFrameFromElement(e.target));

    else
      showMultiEditDeleteModalSidebar(e, getFrameFromElement(e.target));

  });

// Showing MULTI delete modals
$(document)
  .button()
  .on('click', '.bsit-multi-delete-btn', function (e) {

    let pfid = $(e.target)
      .closest('.bsit-page-frame')
      .data('pfid');

    var enablePaneView = $(`#enable_pane_view_` + pfid).val();
    if (!enablePaneView || enablePaneView == 0)
      showMultiEditDeleteModal(e, getFrameFromElement(e.target));

    else
      showMultiDeleteModalSidebar(e, getFrameFromElement(e.target));

  });

// Saving MULTI edit and delete records
$(document)
  .button()
  .on('click', '.bsit-commit-multi', function (e) {
    commitMultiEditDelete(e, getFrameFromElement(e.target));
  });

// Pagination buttons
$(document).on('click', '.bsit-pagination', function (e) {
  paginationClick(e, getFrameFromElement(e.target));
});

// CSV Download
$(document).on('click', '.download_button_div', function (e) {
  let pfid = $(e.target)
    .closest('.bsit-page-frame')
    .data('pfid');
  $(`#editModel` + pfid).removeClass('open');
  $(`#addModel` + pfid).removeClass('open');
  $(`#deleteModel` + pfid).removeClass('open');
  $(`#sidemultiEditModel` + pfid).removeClass('open');
  $(`#sidemultiDeleteModel` + pfid).removeClass('open');
  $('.modal-backdrop').addClass('bsit_out');
  $('body').removeClass('open-bg');
  getFrameData(getFrameFromElement(e.target), 1);
});

// PAGE Refresh

$(document).on('click', '.page_refresh', function (e) {

  let pfid = $(e.target)
    .closest('.bsit-page-frame')
    .data('pfid');
  $(`#editModel` + pfid).removeClass('open');
  $(`#addModel` + pfid).removeClass('open');
  $(`#deleteModel` + pfid).removeClass('open');
  $(`#sidemultiEditModel` + pfid).removeClass('open');
  $(`#sidemultiDeleteModel` + pfid).removeClass('open');
  $('.modal-backdrop').addClass('bsit_out');
  $('body').removeClass('open-bg');
  pagerefrace(e, getFrameFromElement(e.target));
});


// Column header sorting
$(document)
  .button()
  .on('click', '.bsit-sort-header', function (e) {
    sortHeaderClick(e, getFrameFromElement(e.target));
  });

// Frame rows to display changed
$(document).on('change', '.number_of_rec', function (e) {
  rowCountChange(e, getFrameFromElement(e.target));
});

// Date/Time pickers
$('.datepicker').pickadate({
  format: 'yyyy/mm/dd',
  startDate: '-3d',
});

$('.timepicker').pickatime({
  format: 'HH:i',
  formatLabel: '<b>h</b>:i <!i>a</!i>',
});

// Quick flilter active event

$(document)
  .button()
  .on('click', ".bsit_filter_vailues", function (event) {


    var ele_litext = $(this).attr('value');
    var span_text = $(this).parent().parent().find(".bsit-sort-header").eq(0).text();
    var ul_fieldtext = $(this).parent().attr("field");

    var filterField = ul_fieldtext.trim(); // event.currentTarget.offsetParent.attributes.field.value;    
    var filterFieldText = span_text.trim(); //event.currentTarget.offsetParent.offsetParent.firstElementChild.innerText;
    var filterOperator = '=';
    var filterTextRaw = ele_litext.trim(); // event.currentTarget.innerText;
    let frame = getFrameFromElement(event.target);

    $('#filter_name_' + frame.PFID).val(filterField);
    $('#filter_oprator_' + frame.PFID).val(filterOperator);
    $('#filter_text_' + frame.PFID).val(filterTextRaw);

    performFilter(frame, filterTextRaw, filterField, filterOperator, filterFieldText);

  });

$(document)
  .button()
  .on('keypress', 'input[name="quick_filter_text_box"]', function (e) {


    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {

      var ele_litext = e.currentTarget.value;
      var span_text = $(this).parent().parent().find(".bsit-sort-header").eq(0).text();
      var ul_fieldtext = $(this).parent().attr("field");

      var filterField = ul_fieldtext.trim(); // event.currentTarget.offsetParent.attributes.field.value;    
      var filterFieldText = span_text.trim(); //event.currentTarget.offsetParent.offsetParent.firstElementChild.innerText;
      var filterOperator = 'like';
      var filterTextRaw = ele_litext.trim(); // event.currentTarget.innerText;
      let frame = getFrameFromElement(event.target);

      $('#filter_name_' + frame.PFID).val(filterField);
      $('#filter_oprator_' + frame.PFID).val(filterOperator);
      $('#filter_text_' + frame.PFID).val(filterTextRaw);

      performFilter(frame, filterTextRaw, filterField, filterOperator, filterFieldText);

    }
  });

$(document)
  .button()
  .on('click', '.quick_filter_text_box_icon', function (e) {

    var ele_litext = e.currentTarget.nextElementSibling.value;
    var span_text = $(this).parent().parent().find(".bsit-sort-header").eq(0).text();
    var ul_fieldtext = $(this).parent().attr("field");

    var filterField = ul_fieldtext; // event.currentTarget.offsetParent.attributes.field.value;    
    var filterFieldText = span_text; //event.currentTarget.offsetParent.offsetParent.firstElementChild.innerText;
    var filterOperator = 'like';
    var filterTextRaw = ele_litext; // event.currentTarget.innerText;
    let frame = getFrameFromElement(event.target);

    $('#filter_name_' + frame.PFID).val(filterField);
    $('#filter_oprator_' + frame.PFID).val(filterOperator);
    $('#filter_text_' + frame.PFID).val(filterTextRaw);

    performFilter(frame, filterTextRaw, filterField, filterOperator, filterFieldText);

  });


// Quick flilter open event
$(document).ready(function (e) {
  setupQuickFilters();
});

function setupQuickFilters() {
  showHideQuickFilter();

  $.each($('.bsit_allow_quick_filter_show'), function (index, value) {

    getQuickFilterValues($(this).attr("data-framepropertyid"));
  });
}

$(document).ajaxComplete(function (event, xhr, settings) {
  if (settings.url.indexOf("load_content") > 0) {
    setupQuickFilters();
  }
});


$(document).ready(function () {

  $('#menu-nav a').click(function (e) {
    e.preventDefault();

  });

  window.onpopstate = function (e) {
    $("#menu-nav a").fadeTo('fast', 1.0);
    //setCurrentPage(e.state ? e.state.url : null);
  };

});


$(document).on("click", ".notificationFilter", function () {
  var notificationFilterID = $(this).attr('notificationId');
  var notificationLinkUrl = $(this).attr('notificationLinkUrl');
  notificationRedirectUpdate(notificationFilterID, notificationLinkUrl);

});

$(document).ready(function () {
  $(".dropdown-toggle").click(function () {
    $(".dropdown-menu").toggle();
  });

  $("#redirectNotificationPage").click(function () {
    var notifications = base_url + 'page/notifications';
    window.location = notifications;
  });

  $(".close_notification").click(function (e) {

    var notificationId = $(this).attr('notificatioId');
    notificationUpdate(notificationId);
    $('#bsit_close_notification_' + notificationId).show();

  });


  $("#dismissAllrecords").click(function (e) {
    var notificationCollection = $('.notificationDismissAllrecordData');
    $.each(notificationCollection, function (index, value) {
      var notificationId = $(this).attr('notificationId');
      notificationUpdate(notificationId);
      $('.bsit_close_notification1').show();
    });
  });
  $('.bsit_close_notification1').hide();
});


// Updating records using edit modal for opprtunity
$(document)
  .button()
  .on('click', '.bsit-commit-editcustom', function (e) {
    commitEdit(getFrameFromElement('',144), "modal");
});

// Updating records using edit modal for customer
$(document)
  .button()
  .on('click', '.bsit-commit-editcustomer', function (e) {
    commitEdit(getFrameFromElement('',30), "modal");
});

$(document).ready(function () {
  //$(document).find('.loader').hide();
  // seeMoreRecords_seeLessRecords();
});
