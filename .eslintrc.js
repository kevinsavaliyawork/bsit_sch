module.exports = {
  env: {
    browser: true,
    jquery: true,
  },
  extends: ['prettier'],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2017,
  },
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',
    'camelcase': ['warn', {
      'properties': 'always'
    }],
    'no-console': ['warn', {
      'allow': ['log', 'warn', 'error'],
    }],
    'max-len': ['warn', { code: 100, ignoreUrls: true }],
    'lines-between-class-members': ['warn', 'always'],
    'object-curly-spacing': ['warn', 'always'],
    'spaced-comment': ['warn', 'always'],
    'space-before-blocks': ['warn', 'always'],
    'lines-around-comment': ['warn', {
      'beforeBlockComment': true,
      'beforeLineComment': true,
      'allowBlockStart': true,
    }],
  },
  globals: {},
};
