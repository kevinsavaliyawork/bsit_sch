<?php

/* $data_pass['frame'] = $frame;
$data_pass['load_data'] = $load_data;
$data_pass['frame_filter_data'] = $frame_filter_data;*/



  // ------------------------------
  // -- CREATE LISTVIEW CACHE FILE
  // ------------------------------
  // Its Inclue : Display the Content(Data) of Files

  $cacheDir = APPPATH."views/cache/";
  $linkopen = "";
  $html = "";
  $html_pagi = "";
  $fullPath = $frame['fullPath_content'];
  $viewPath = $frame['viewPath_content'];

  $linkopen = "";
  $html .= '<div id="page_frame_'.$frame['PFID'].'_content" class="call_content_div">';
  $html .= '<input type="hidden" id="frame_order" value='.$frame['order'].'>';
 
  $html .= '<div class="listview" style="">';
  $html .= $this->load->view('core/TableViewCustom.php',$data_pass,true);
  $html .= '</div>';
   
    
  // ------------------------------
  // -- PAGINATION CODE
  // ------------------------------
  $html_pagi .= '<div class="pagination_tr" >';
  if(isset($frame['allow_download']))
  {
    if($frame['allow_download'] == 1){
      $html_pagi .= '<div class="download_button_div hasTooltip" id="download_data_'.$frame['PFID'].'">';
      $html_pagi .= '<img style="opacity:.4" src="'.BASEURL_IMAGE.'/ic_get_app_black_24dp_1x.png"/><span class="icon_tooltip_frame">Download</span>';
      $html_pagi .= '</div>';
    }
  }else{
    if($prop->hide_allow_download == '0'){
      $html_pagi .= '<div class="download_button_div hasTooltip" id="download_data_'.$frame['PFID'].'">';
      $html_pagi .= '<img style="opacity:.4" src="'.BASEURL_IMAGE.'/ic_get_app_black_24dp_1x.png"/><span class="icon_tooltip_frame">Download</span>';
      $html_pagi .= '</div>';
    }
  }

  if($prop->hide_pagination == '0')
  {
    $html_pagi .= '<div class="pagination dataTables_paginate paging_simple_numbers" id="example_paginate">';
    $html_pagi .= '<div class="pagination">';
    $html_pagi .= '<div id="pagination_output">';
    $html_pagi .= 'Rows Per Page : ';
    $html_pagi .= '<input type="hidden" id="total_page_'.$frame['PFID'].'" value="'.$frame['tot_page_count'].'">';
    $html_pagi .= '<input type="hidden" id="page_num_'.$frame['PFID'].'" class="pagination_class_'.$frame['PFID'].'" value="1">';
    $html_pagi .= '<input type="hidden" id="default_page_size_'.$frame['PFID'].'" value="'.$frame['Properties'][0]->default_page_size.'">';
    $html_pagi .= '<select id="num_recs_'.$frame['PFID'].'" class="number_of_rec" >';
    //$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
    //SET DEFAULT PAGE SIZE IN PAGINATION
    if($frame['Properties'][0]->default_page_size == ''){
      $html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
    }else{
      if($frame['Properties'][0]->default_page_size == '10'){
        $html_pagi .= '<option value="10" selected="selected">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
      }else if($frame['Properties'][0]->default_page_size == '20'){
        $html_pagi .= '<option value="10">10</option><option value="20" selected="selected">20</option><option value="50">50</option><option value="100">100</option>';
      }else if($frame['Properties'][0]->default_page_size == '50'){
        $html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50" selected="selected">50</option><option value="100">100</option>';
      }else if($frame['Properties'][0]->default_page_size == '100'){
        $html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100"  selected="selected">100</option>';
      }else{
        $html_pagi .= '<option value="'.$frame['Properties'][0]->default_page_size.'">'.$frame['Properties'][0]->default_page_size.'</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
      }
    }
    $html_pagi .= '</select>';
    $html_pagi .= '<div class="pagi_arrows">';
    $html_pagi .= '<input type="button" id="pagi_previous_'.$frame['PFID'].'" data-prevpage="prev" class="mdl-button previous pagination_class_'.$frame['PFID'].'" value="" style="background: url('.base_url().'assets/images/chevron-left.png);background-repeat: no-repeat;">';
    $html_pagi .= '<span id="pagination_number_div"></span>';
    $html_pagi .= '<input type="button" id="pagi_next_'.$frame['PFID'].'" data-nextpage="next" value="" class="mdl-button pagination_class_'.$frame['PFID'].'" style="background: url('.base_url().'assets/images/chevron-right.png);background-repeat: no-repeat;">';
    $html_pagi .= '</div>';
    $html_pagi .= '</div>';
    $html_pagi .= '</div>';
    $html_pagi .= '</div>';
    $html_pagi .= '<input type="hidden" class="PFID" value="'.$frame['PFID'].'">';
  }
  $html_pagi .= '</div>';
  $html .= $html_pagi;

  $html .= $html_sort;
  $html .= '</div>';

  // ------------------------------
  // -- CREATE LISTVIEW CACHE FILE
  // ------------------------------
  //echo $fullPath;


/*  if (file_exists($fullPath)) {
    $myfile1 = fopen($fullPath, "w") or die("Unable to open file!");
    fwrite($myfile1, $html);
    fclose($myfile1);
     }*/
  echo $html;
?>
