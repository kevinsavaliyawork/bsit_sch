<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taskview extends brain_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('module');
		$this->load->model('Bsit_io','API');

		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		// check if the user is 'logged in', if not, redirect to login page.
		$user_id = $this->session->userdata('UserID');
		$logged_in = $this->session->userdata('logged_in');
		if($logged_in == FALSE OR $user_id == "" ){
			// redirect to the login page
			redirect('/login');
		};
	}
	
	function index(){

		// load the infomation
		// load the infomation
		$userID = $this->session->userdata('UserID');
		$name_first = $this->session->userdata('FirstName');
		$tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules($userID);

		//echo "<pre>";print_r($tmp_data['menu_data']); exit;
		$data['menu_data'] = $this->load->view ('menu.php', $tmp_data, TRUE);

		// get the users ID so its available in the page
		$data['userid'] =$this->session->userdata('UserID');

		// get the users ID so its available in the page
		$data['userid'] = $userID;

		$jobID = $_GET['jobId'];
		$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/"; // used to format notes with links
		$data['reg_exUrl'] = $reg_exUrl;
		$data['jobID'] = $jobID;

		// Setup request
		$CURLDATA 	= 	Array('collections' =>
			json_encode(
				Array(
					'colls'=> Array(
							// Jobs collection
							Array(
								'coll_name'	=> 'bsit_jobs',
								'cols'		=> Array("title", "description", "priority", "assigned_to", "status"),
								'filter'	=> "[jb]._id=".$jobID,
								// 'order'		=> "[jb].create_date desc",
								'numrec'	=> '1'
							),
							// Notes collection
							Array(
								'coll_name'	=> 'bsit_notes',
								'cols'		=> Array("Note", "create_by", "create_date", "username"),
								'filter'	=> "[nt].row_id=".$jobID,
								'order'		=> "[nt].create_date desc",
								'numrec'	=> '10'
							),
							// Users collection
							Array(
								'coll_id'	=> '16',
								'filter'	=> ""
							)
		))));
		// Retrieve data using the request we setup earlier
		$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
		$json_page_result = $this->API->CallAPI("GET",$BASEURLMETHOD,$CURLDATA);
		$json_result = json_decode($json_page_result);

		$data['json_result'] = $json_result;

		// load the page
		//$data['page_content'] = $this->load->view('TaskView',$data,true);

		return $this->load->view('taskview/taskview',$data,true);
		

	}
}