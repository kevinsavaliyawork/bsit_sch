<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardData extends brain_controller {
	
	const ERRORVIEW = "bsiterror";
	
	// Setup some global variables
	private $method 		= '';
	private $userID 		= '';
	private $spName 		= '';

	private $spNameLookup = Array(
		'PipelineOwner'	    	=> 'BSIT_Dash_Opp_Owner',
		'PipelineSource'		=> 'BSIT_Dash_Opp_Source',
		'PipelineStage'    		=> 'BSIT_Dash_Opp_Stage',
		'PipelineIncomeStream'	=> 'BSIT_Dash_Opp_IncomeStream'
	);

	// setup
	function __construct(){
		parent::__construct(1);
		
		// Load the needed librarys
		$this->load->model('core/Bsit_io','API');
		$this->load->library('session');

		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		// check if the user is 'logged in', if not, redirect to login page.
		$user_id = $this->session->userdata('UserID');
		$logged_in = $this->session->userdata('logged_in');
		session_write_close();
		if($logged_in == FALSE OR $user_id == "" ){
			// report the unlogged in user
			$this->throwError('ERROR: NOT LOGGED IN!');
		}

		// Setup the User ID so it is accessible in other methods
		$this->userID     =   $user_id;
		
		// Get the method the user is requesting and lookup the DB SP name
		$this->method     =   (isset($_REQUEST['method'])     ? $_REQUEST['method'] : '');
		$this->spName = $this->spNameLookup[$this->method];
		if($this->spName == ''){
			$this->throwError('ERROR: SP Name is blank.');
		}
	}

	// a method to intercept the request from the client
	function OpDashSP(){
		$this->dashboardSPCall($this->spName);
	}

	// ------------------------------------
	// A std call to the DB for an SP with only user ID input.
	// ------------------------------------
	function dashboardSPCall($sp_name){
		// Ensure correct parameters are being supplied.
		if($this->userID == ''){
		  $this->throwError('ERROR: A user is required.');
		}

		// setup the parameters for the call
		$parameters = array(
		  array('key'=> '@userID',	  'value' => $this->userID),
		);
		
		$t=time();
		//echo(date("Y-m-d",$t));
		//echo($sp_name.'</br>');
	
		error_log($sp_name);
		$data_parameters = urlencode('{"parameters":'.json_encode($parameters).'}');

		// call the SP
		$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
		$URL = $BASEURLMETHOD.'?key='.$sp_name.'&filters='.$data_parameters;
		$json_page_result = $this->API->CallAPI("GET",$URL);

		// Show the result so as JSON so JQUERY setup the display
		$t=time();
		//echo(date("Y-m-d",$t));
		echo($json_page_result);
			
		exit;
	}

	// Throw an error back to the caller
	function throwError($msg){
		echo('{"HasError": true, "ErrorMessage":"'.$msg.'"}');
		exit;
	}
}
