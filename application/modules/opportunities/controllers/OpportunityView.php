<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class OpportunityView extends brain_controller {

	function __construct() {
    parent::__construct(1);
    // Load the needed librarys
    $this->load->model('core/Bsit_io','API');
    $this->load->library('session');
    $this->load->model('opportunities/Opportunitymodel');
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	}

	function index($data_pass){
		return $this->load->view($data_pass['module_name'].'/OpportunityView',$data_pass,true);
		
	}

  /**
   * Perform an opportunity search
   * @param null $searchTerm
   * @return false|string
   */
	public function getOpportunities() {
	  //If this is an ajax call, list the opportunities
   // if ($this->input->is_ajax_request()) {
      header('Content-Type: application/json');
      $searchTerm = $this->input->post('search_term'); //via post
      $search = $this->Opportunitymodel->getOpportunities($searchTerm);
      echo $search; die();
  //  }
    return false;
  }

  public function saveGmailAdditionalInfo($id, $data, $followUpDate, $price, $percent, $stage, $email) {
    //if ($this->input->is_ajax_request()) {
      $id = $this->input->post('id');
      $data = $this->input->post('data');
	  $followUpDate = $this->input->post('followUpDate');
	  $price = $this->input->post('price');
	  $percent = $this->input->post('percent');
	  $stage = $this->input->post('stage');
	  $email = $this->input->post('email');
      $response = $this->Opportunitymodel->saveGmailInfo($id, $data, $followUpDate, $price, $percent, $stage, $email);
      echo $response; 
	  die();
    //}
    return false;
  }
public function opportunityJobsOverview($data_pass)
  {
    $get_customer_detail = $this->Opportunitymodel->getCustomerDetail($_GET["job_id"]);
    $data_pass['customer'] = json_decode($get_customer_detail)->Data;
    $opportunityDetails = $data_pass['load_data'][0];
    
    $data_pass['opportunityDetails'] = $opportunityDetails;
    
    // activity data
    $get_activity_history_detail = $this->Opportunitymodel->getActivityHistoryDetail($_GET["job_id"]);
    $data_pass['activity_history'] = json_decode($get_activity_history_detail);
    // print_r($data_pass['activity_history']); exit;
    return $this->load->view('opportunities/jobs_overview',$data_pass,true);   
  }
  
}
