$(document).ready(function()
{ 
   if($('ul.nav-tabs li').hasClass('bsit-page-frame')){
      $("ul.nav-tabs li.bsit-page-frame:first").addClass("active");
      $("#commit-edit-update-30").removeClass("bsit-commit-edit").addClass("bsit-commit-editcustomer");
      var editPOPUP = $('#editModel30').get(0).outerHTML;
      $( "#editModel30").remove();
      $( "#opprtunityViewTab").append(editPOPUP);
    }

// upload documents add button
  var get_url = window.location.href; //window.location.href
  var url = new URL(get_url);
  var getParameter = url.searchParams.get("_id");
  $('#add-btn-143').addClass('bsit-upload-btn').removeClass('bsit-btn-add');
  $("#add-btn-143").attr("data-collid","12");
  $("#add-btn-143").attr("data-recid",getParameter);
  $("#add-btn-143").attr("data-modal",'upload');
});  

  $(document).on('click', '#customerEditButton', function () {  
    $("#orgEditbutton").trigger("click");
  });

$('#editBtn').click(function() {
  $("#editModel30").remove();
});

function refresh_customerdocuments(){
  $(".loader").show(); 
  var pageFrameId= 143;
  var pageId = 32;
  var pageParams;
    pageParams =  $('#fram_prop_filter_143').val();
    var pass_data_ajax = {'filter':"",'order':"",'page_num':1,'num_recs':10,'page_id':pageId,'frame_id':pageFrameId,'prop_frame_filter_parameter':pageParams}
    //jQuery.extend(pass_data_ajax,pageParams);
    var new_url1 = base_url+"core/Frame/load_content";
    $.ajax({
      type: "POST",
      data:  pass_data_ajax,
      url: new_url1,
      success: function(returnData){
        $(".loader").hide(); 
        $('#page_frame_'+pageFrameId+'_content').replaceWith(returnData);
      }
    });
}

$(document)
  .button()
  .on('click', '.bsit_form_close_btn', function() {
	  if(!$('li.bsit-page-frame:first').hasClass('active')){
        var get_url = window.location.href; //window.location.href
        var url = new URL(get_url);
        var getParameter = url.searchParams.get("_id");
		
        if (getParameter) {  refresh_customerdocuments();}
    }
	
  });
  
$( document ).ajaxComplete(function( event, xhr, settings )
{ 
  $('#page_frame_143_content table .bsit-edit-btn').addClass('bsit-edit-documents-btn-list').removeClass('bsit-edit-btn');
  showMoreAndLess();
});  

function showMoreAndLess(){
	
  $(".readMoreNOte").addClass("readlessactive")
  $(".readLessNOte").addClass("readlessinactive")
  $(".cola_more").removeClass("intro");

 $(document).on("click",".readMoreNOte",function(){
    $(this).prev().addClass("intro");
    $(this).removeClass("readlessactive");
    $(this).addClass("readlessinactive");
    $(this).next().removeClass("readlessinactive");
    $(this).next().addClass("readlessactive");
 });

  $(document).on("click",".readLessNOte",function(){
    $(this).prev().prev().removeClass("intro");
    $(this).removeClass("readlessactive");
    $(this).addClass("readlessinactive");
    $(this).prev().removeClass("readlessinactive");
    $(this).prev().addClass("readlessactive");
 });

  
}

$( document ).ajaxComplete(function( event, xhr, settings ) {
  // after add   
  if ( settings.url == base_url+"core/apilocal/call_method" )
  {
      var total = $('span#activityCount').text();
      total=parseInt(total)+1;
      $('span#activityCount').text(total);
      if($('li.bsit-page-frame:first').hasClass('active')){
        updateCustomerNoteList();
      }
  }
  // after Edit  
 if ( settings.url == base_url+"core/apilocal/updateNotes" )
  {
	   if(xhr.responseText){
		if($('li.bsit-page-frame:first').hasClass('active')){
			updateCustomerNoteList();
		}
      }
  }
 /*  if ( settings.url == base_url+"core/Apilocal/update" )
  {
    if(xhr.responseText){
     if($('li.bsit-page-frame:first').hasClass('active')){
        updateCustomerOverviewList();
      }
      
  }
  }*/
 
});
// notes details
function updateCustomerNoteList() {
  let url = base_url + 'Customer/fetchLatestNotesHistoryList';
  let _id = $("#_id").val();
  if (_id) {
    $.ajax({
      type: 'POST',
      url: url,
      data: { '_id': _id },
      success: function (res) {
        $("#customer_notes_list").html(res);
      }
    });
  }

}

// customer details 
function updateCustomerOverviewList() {
  let url = base_url + 'Customer/fetchCustomerAllData';
  let job_id = $("#_id").val();
  if (job_id) {
    $.ajax({
      type: 'POST',
      url: url,
      data: { '_id': job_id },
      success: function (res) {
        
        $("#bsit_opp_overview_data").html(res);
      }
    });
  }

}




$('#editBtn').click(function() {
 $(".FrameMainTitle").css("display","none");
});

function change_color(color,address,name,htm){


var htmldata  = '<div class="framecolorBox" style="background:'+color+'">'+name.charAt(0)+'</div><div class="frameNameDetails"><h5 id="custname">'+name+'<span class="editButtontop">'+htm+'</span></h5><p id="custaddress">'+address+'</p></div>';
//$(".framecolorBox").css("background",color);
//$("#custaddress").html(address);
$(".FrameMainTitle").html(htmldata);

}
