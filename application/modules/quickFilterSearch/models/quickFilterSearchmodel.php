<?php

defined('BASEPATH') or exit('No direct script access allowed');
class QuickFilterSearchmodel extends brain_model {
  function __construct(){
    parent::__construct();
    $this->load->model('core/Bsit_io','API');
    $this->load->library('session');
  }


    public function quickSearchTerm($searchData,$rows){
      $_DATAPOST['parameters'] = array (
          'key' => 'quickFilterSearch',
          'filters' => 
            array(
                array (
                  'key' => 'search',
                  'value' => $searchData
                ),
                array (
                  'key' => 'NUMROWS',
                  'value' => $rows
                ),
                ),
            );
      $data_parameters = json_encode($_DATAPOST['parameters']);
      $this->BASEURLMETHOD = API_BASEURL.API_METHOD_GET;
      $get_user_preferences = $this->API->CallAPI("GET", $this->BASEURLMETHOD,$data_parameters,false);
      /* $data['filterData'] = array();
    $data['filterData'] = json_decode($get_user_preferences);

    //print_r($data['filterData']);exit;
    foreach ($data['filterData']->Data as $key => $row) {
      $filterDataAll[] = (array)$row;
    }
    
    return $filterDataAll;*/
      return $get_user_preferences;
    }


}

