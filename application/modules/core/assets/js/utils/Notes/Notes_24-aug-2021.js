import { getCollectionIDByName, advMultiCollectionSourceNote } from '../../frames/TableView/TableView.js';
import { getFrameFromElement } from '../../Page.js';

async function showNotesModal(event) {
  let frame = getFrameFromElement(event.target);
  let collectionID = $(event.currentTarget).data('collid');
  let recordID = $(event.currentTarget).data('recid');

  // Assign variables to the modal
  $('#bsit-notes-modal-container').attr('data-collectionID', collectionID);
  $('#bsit-notes-modal-container').attr('data-rowID', recordID);

  //reset field
  resetNoteFields();
  
  // Clear notes that may have been in there previously.
  $('#bsit-notes-container').html('');
  $('#bsit-notes-container').css('display', 'none');

  // Show the modal
  $('#bsit-notes-modal').modal();

  let noteList = await getNotes(collectionID, recordID).catch(() => undefined);

  let frameTitle = $(`#page_frame_${frame.PFID}_header h3`).text();

  if (frameTitle != '') {
    $('#bsit-notes-modal-header').html(`Notes - ${frameTitle}`);
  } else {
    $('#bsit-notes-modal-header').html(`Notes - ${page_title}`);
  }

  // Check we got notes to display
  if (typeof noteList != 'undefined') {
    // Get HTML for, and display each note.
    noteList.notes.forEach((note) => {
      let noteHTML = buildNoteHtml(note.createBy, note.createDate, note.note,note.note_header,note.notify_users);

      $('#bsit-notes-container').css('display', 'block');

      // Add html to the note container
      $('#bsit-notes-container').prepend(noteHTML);
    });
  }
}

async function getNotes(collectionID, recordID) {
  let notesCollectionID = await getCollectionIDByName('notes');

  let requestData = {
    data: JSON.stringify([
      {
        collID: notesCollectionID,
        filter: `nt.row_id = ${recordID} AND nt.coll_id = ${collectionID}`,
      },
    ]),
  };

  let url = base_url + 'core/Apilocal/loadfiltered';

  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      url: url,
      async: false,
      data: requestData,
      dataType: 'json',
      success: function(res) {
        if (typeof res[0]['Results'][0] != 'undefined') {
          let results = res[0]['Results'];

          let notes = results.map((val) => {
            return {
              createBy: val.username,
              createDate: val.create_date,
              note: val.Note,
              note_header: val.note_header,
              notify_users: val.notify_users
            };
          });

          let notesToReturn = {
            collectionName: results[0].collectionName,
            notes: notes,
          };

          resolve(notesToReturn);
        } else {
          reject(undefined);
        }
      },
      error: function(err) {
        alert(err.responseText);
        reject(err);
      },
    });
  });
}

function saveNote() {
	let note_header = $("#note_header").val().trim();
  /*if (note_header == "") {
    $("#note_header").addClass("required_border_class");
    return false;
  } else {
    $("#note_header").removeClass("required_border_class");
  }*/
  
  let note = $('#editor').val();
  if (note.length == 0) {
     $("div#cke_editor").addClass("required_border_class");
     $(".ck-editor__editable_inline").addClass("required_border_class");
    return false;
  }
  else {
    $("div#cke_editor").removeClass("required_border_class");
     $(".ck-editor__editable_inline").removeClass("required_border_class");
  }
  // Get what was typed to save as the note
  //let note = $('#editor').val();
  let notify_user_val = $('#hf_advanceMultiSelectNote_value').val().split(",");
  let notify_user_label = $('#hf_advanceMultiSelectNote').val().split(",");
  let collID = $('#bsit-notes-modal-container').attr('data-collectionID');
  let rowID = $('#bsit-notes-modal-container').attr('data-rowID');
  let noteType = $('#bsit-notes-select').val();
  // Set up the SP call with the new values to update the db
  let userNotifyValue = "";

  if($("#hf_advanceMultiSelectNote").val().length != 0){
	  $.each($("#hf_advanceMultiSelectNote").val().split(","),function(i,v){
			if(userNotifyValue.length == 0 ){

			  userNotifyValue=notify_user_val[i]+":"+notify_user_label[i];
			
			} else{

			  userNotifyValue=userNotifyValue+","+notify_user_val[i]+":"+notify_user_label[i];
			}
		 
	  });
  }
  
  let notify_users = $.trim(userNotifyValue);
  // console.log(notify_users);
  
  /*let noteSaveRequest = {
    parameters: [
      {
        key: '@collectionID',
        value: collID,
      },
      {
        key: '@rowID',
        value: rowID,
      },
	  {
        key: 'note_header',
        value: note_header,
      },
      {
        key: '@note',
        value: note,
      },
      {
        key: '@noteType',
        value: noteType,
      },
      {
        key: '@notify_users',
        value: notify_users,
      },
      {
        key: '@user_id',
        value: userID,
      },
    ],
    sp_name: [
      {
        key: 'bsit_addnewnote',
      },
    ],
  };*/
  let noteSaveRequest = {
    parameters: [
      {
        key: 'collectionID',
        value: collID,
      },
      {
        key: 'rowID',
        value: rowID,
      },
      {
        key: 'note_header',
        value: note_header,
      },
      {
        key: 'note',
        value:note,
      },
      {
        key: 'noteType',
        value: noteType,
      },
      {
        key: 'notify_users',
        value: notify_users,
      },
      {
        key: 'user_id',
        value: userID,
      },
    ],
    sp_name: [
      {
        key: 'bsit_addnewnote',
      },
    ],
  };
      
      // console.log(noteSaveRequest);
	  let url = base_url + 'core/Apilocal/call_method';
	  
  //let url = base_url + 'core/Apilocal/Call_SP';
console.log(url);
  // Save the note
  $.ajax({
    type: 'POST',
    url: url,
    data: noteSaveRequest,
    dataType: 'json',
    success: function(res) {

      let data = res[0];
	  
	    //reset field
        resetNoteFields();
  
      // $('.notemultiSelect-ul li:not(:last)').remove();
      var obj_data=$(".notemultiSelect-ul").find("li");
       $('#hf_advanceMultiSelectNote').val('');
       $('#hf_advanceMultiSelectNote_value').val('');
       //$("#note_header").val('');
       $.each(obj_data,function(i,e){
         var classname=$(this).attr("class");
           if(classname!="li-inputnote")
           {
             $(this).remove();
           }
           else
           {
             $(this).find("input").val("");
           }
       });

      // Insert the note into the DOM
      let newNoteHTML = buildNoteHtml(
        data.username,
        data.create_date,
        data.note,
		data.note_header,
        data.notify_users
        
      );

      $('#bsit-notes-container').prepend(newNoteHTML);

      if ($('#bsit-notes-container').css('display') == 'none') {
        // Show notes container if it was hidden
        $('#bsit-notes-container').css('display', 'block');
      }

      // Clear and refocus the input box;
      //window.editor.setData('');
      // window.editor.setdata('');
      // $('#bsit-note-input')
      //   .val('')
      //   .focus();
    },
  });
}
function resetNoteFields() {
  //reset ckeditor
  $('iframe').contents().find('body').empty();
  
//on load destroy 
  if(CKEDITOR.instances.editor)
  CKEDITOR.instances.editor.destroy();

// reinistial ckediter
CKEDITOR.replace( 'editor', {
  height: 300,
  extraPlugins: 'emoji,justify,font,colorbutton',
   toolbar: [
        ['ajaxsave'],
        ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'EmojiPanel'],//'-', 'Link', 'Unlink',
        [ 'Copy', 'Paste', 'PasteText'],//'Cut',
        ['Undo', 'Redo', '-', 'RemoveFormat'],
        ['TextColor', 'BGColor'],
       // ['Maximize']//, 'Image'
    ],
  
/*  filebrowserUploadUrl: '<?php echo base_url(); ?>core/apilocal/notesUpload',
  filebrowserUploadMethod: "form"*/

 });
  $("#note_id").val('');

  //reset note header
  $("#note_header").val('');

  

  $("#bsit-notes-select").val('1');

  // $('.notemultiSelect-ul li:not(:last)').remove();
  var obj_data = $(".notemultiSelect-ul").find("li");
  $('#hf_advanceMultiSelectNote').val('');
  $('#hf_advanceMultiSelectNote_value').val('');

  $.each(obj_data, function (i, e) {
    var classname = $(this).attr("class");
    if (classname != "li-inputnote") {
      $(this).remove();
    } else {
      $(this).find("input").val("");
    }
  });
}

function buildNoteHtml(username, createDate, note,note_header,notify_users) {
  // Format urls so they can be highlighted and clicked
  note = linkifyHtml(note, {
    attributes: {
      rel: 'nofollow',
    },
    defaultProtocol: 'https',
  });
	let noteHeader = note_header;
	if(note_header == undefined){
	  noteHeader='';  
	  }
  if (noteHeader) {
    noteHeader = `<br/><strong>${noteHeader}</strong>`;
  }
  let newNote =
    `<div class="mui-panel mui-col-md-12">` +
    `<span style="color: #999999;" class="mui--divider-right">` +
    `<strong>${moment(createDate).format('LLL')}` +
    ` | ${username}:</strong></span>` +
    noteHeader +
    `<br/>
	${note}<span style="color: #999999;font-size: 14px;"><br/>Notify to - ${notify_users} </span>  </div>`;

  return newNote;
}





$(function() {
  $(".advanceMultiSelectNote")
    .bind("keydown", function(event) {
      if (event.keyCode === $.ui.keyCode.TAB &&
        $(this).data("ui-autocomplete").menu.active) {
        event.preventDefault();
      }
    })
    .autocomplete({
      minLength: 1,
      source: function(request, response, event) {
           advMultiCollectionSourceNote(request, response, $(this));
      },
      focus: function() {
        // prevent value inserted on focus
        return false;
      },
      select: function(event, ui) {
// debugger
    var selectlab= ui.item.label;
    var selectval= ui.item.value;
    var selectval_value= ui.item.value.toString();
    var select_advanceMultiValue = $("#hf_advanceMultiSelectNote_value").val().split(",");

          if(jQuery.inArray(selectval_value,select_advanceMultiValue) != -1) {
              $('.advanceMultiSelectNote').val();
            return false;
          }

  if($('#hf_advanceMultiSelectNote').val().length == 0){
    selectlab=$('#hf_advanceMultiSelectNote').val()+$.trim(selectlab);
    $("#hf_advanceMultiSelectNote").val($.trim(selectlab));
    selectval=$('#hf_advanceMultiSelectNote_value').val()+$.trim(selectval);
    $("#hf_advanceMultiSelectNote_value").val($.trim(selectval));
  }else{
    selectlab=$('#hf_advanceMultiSelectNote').val()+','+$.trim(selectlab);
    $("#hf_advanceMultiSelectNote").val($.trim(selectlab));
    selectval=$('#hf_advanceMultiSelectNote_value').val()+','+$.trim(selectval);
    $("#hf_advanceMultiSelectNote_value").val($.trim(selectval));
  }
        
        

        var obj_data=jQuery(".notemultiSelect-ul").find("li");
        // alert($("#hf_advanceMultiSelectNote_value").val());
        $.each(obj_data,function(i,e){
          var classname=$(this).attr("class");
            if(classname!="li-inputnote")
            {
              $(this).remove();
            }
            else
            {
              $(this).find("input").val("");
            }
        });

        $.each($("#hf_advanceMultiSelectNote").val().split(","),function(i,v){

            if(v!="")
            {

              jQuery(".notemultiSelect-ul li:eq(0)").before('<li><span class="del-user-multi-select-val" data-lebal="'+v+'">X</span>'+v+'</li>');
            }
        });

        
        return false;
      }
    }).autocomplete("instance")._renderItem = function(ul, item) {
      return $("<li>")
        .append("<div>" + item.label + ", "+item.value+"</div>")
        .appendTo(ul);
    };
    function split(val) {
      return val.split(/,\s*/);
    }
    function extractLast(term) {
      return split(term).pop();
    }
});
//MULTI SELECT ADD REMOVE CODE
  jQuery(document).on("click",".del-user-multi-select-val",function(){
    // alert($('#hf_advanceMultiSelectNote_value').val());
    var val=$(this).attr("data-lebal");
    
    var hf="";
    var hf_val="";
    var obj_list = $('#hf_advanceMultiSelectNote_value').val().split(',');
    var obj_data=jQuery(".notemultiSelect-ul").find("li");

        $.each(obj_data,function(i,e){
          
          var classname=$(this).attr("class");
            if(classname!="li-inputnote")
            {
              $(this).remove();
            }
            else
            {
              $(this).find("input").val("");
            }
        });

      $.each($("#hf_advanceMultiSelectNote").val().split(","),function(i,v){
          if(v!="")
          {
            if(v!=val)
            {
              hf=hf+","+v;
              hf_val=hf_val+","+obj_list[i];
              jQuery(".notemultiSelect-ul li:eq(0)").before('<li><span class="del-user-multi-select-val" data-lebal="'+v+'">X</span>'+v+'</li>');
            }

          }
      });

      $("#hf_advanceMultiSelectNote").val(hf);
      $("#hf_advanceMultiSelectNote_value").val(hf_val);
  });




// Notes save / enter
$(document).on('submit', '#bsit-notes-form', function(e) {
  e.preventDefault();
  saveNote();
});

// Change note type
$(document).on('change', '#bsit-notes-select', function(e) {
  // Update the selected option.
  $('#bsit-notes-select')
    .val(e.target.value)
    .prop('selected', true);
});

// Showing notes modal
$(document)
  .button()
  .on('click', '.bsit-notes-btn', function(e) {
    showNotesModal(e);
  });

  $(document)
  .button()
  .on('click', '.bsit_form_close_btn', function() {
      
      var obj_data=$(".notemultiSelect-ul").find("li");
       $('#hf_advanceMultiSelectNote').val('');
       $('#hf_advanceMultiSelectNote_value').val('');

       $.each(obj_data,function(i,e){
         var classname=$(this).attr("class");
           if(classname!="li-inputnote")
           {
             $(this).remove();
           }
           else
           {
             $(this).find("input").val("");
           }
       }); 
    
  });  

