import {commitDelete,getPageFrameIDByName,getPageByName, getCollectionIDByName, advMultiCollectionSourceNote, getFrameData } from '../../frames/TableView/TableView.js';

import { getFrameFromElement } from '../../Page.js';

var page_id = await getPageByName('Documents');

var pageFrameId = await getPageFrameIDByName(page_id);

let UploadCollectionID = await getCollectionIDByName('attached_documents');

let collectionID;
let recordID;
let pfid;
 
async function showUploadModal(event,type) {
  type = (type && type == "edit") ? 'edit' : 'add';
  let frame = getFrameFromElement(event.target);

  collectionID = $(event.currentTarget).data('collid');
  recordID = $(event.currentTarget).data('recid');

  let ID = '';
  let noteList = '';
  if (type == 'edit') {
    ID = $(event.currentTarget).data('id');
  }
  pfid =frame.PFID;
  if (frame && frame.PFID) {
    let frameTitle = $(`#page_frame_${frame.PFID}_header h3`).text();
    if (frameTitle != '') {
      $('#bsit-upload-modal-header').html(`Documents - ${frameTitle}`);
    } else {
      $('#bsit-upload-modal-header').html(`Documents - ${page_title}`);
    }
  }
  //reset field
  resetAllFields();
  
  // Assign variables to the modal
  $('#bsit-upload-modal-container').attr('data-collectionID', collectionID);
  $('#bsit-upload-modal-container').attr('data-rowID', recordID);

  /*  var text = '<div id="main_content_part'+pageFrameId+'" class="main_content_part_class mui-col-xs-12 mui-col-xs-offset- tab-pane fade in " style="position:relative; height:Fillpx; margin-bottom:20px;" data-frametitle="" data-framewidth="12" data-frameheight="Fill" data-frameoffset="" data-frameorder="2" data-frameid="'+pageFrameId+'" pageid="'+page_id+'" collection_id="'+UploadCollectionID+'" frameid="'+pageFrameId+'" framename="documentsFrame" data-framehideadd="" data-framehidefilter="" data-framehidepagination="" data-framehidedownload="" data-framehidemultiedit="" data-framehidemultidelete="">';
          text += '<input type="hidden" id="fram_prop_filter_'+pageFrameId+'" value="">';
          text += '<input type="hidden" id="fram_prop_url_'+pageFrameId+'" value="">';
          text += '<input type="hidden" id="page_id_'+pageFrameId+'" value="">';
          text += '<div class="bsit-page-frame edit-pointer-cancel table-responsive mui-panel" id="page_frame_'+pageFrameId+'" style="height:Fillpx;padding: 0px!important;margin-bottom: 20px!important;box-shadow: none!important;" data-pfid="'+pageFrameId+'">';
          text += '</div></div>';
          console.log(text);
  $('#bsit-upload-container').css('display', 'block');
  $('#bsit-upload-container').html(text);*/
 
  // Show the modal
  $('#bsit-upload-modal').modal();
   onclickClose();
   if (type == 'add') {
     getDocuments(collectionID, recordID);
  }
}

// Reset All fields
function resetAllFields() {
  $("#image_file_outer").removeClass("required_border_class");
  $('#uploadPreview').html(''); 
  $("#image_file").val('');
  $('#doc_keywords').val('');
  $('#doc_description').val('');
  $('#_id').val('');
  $('#old_file_name').val('');
  $('#old_file_location').val('');
  $('#old_file_size_kb').val('');
  $('#old_file_type').val('');
  $("#old_collection_id").val('');
  $('#old_row_id').val('');

}


function getDocumentsData(collectionID, recordID, ID) {
 $('.loader').show();
 var _id = ID;
 let imageData = $('#image_file').val();
 $("#image_file_outer").removeClass("required_border_class");
 

  resetAllFields();
  let requestData = {
    data: JSON.stringify([{
      collID: UploadCollectionID,
      filter: `bsd._id = ${ID} `,// AND bsd.row_id = ${recordID} AND bsd.collection_id = ${collectionID}
    }]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/loadfiltered',
    data: requestData,
    dataType: 'json',
    success: function (res) {
      $('.loader').hide();
      
      if (res && typeof res[0]['Results'][0] != 'undefined') {
          let data = res[0]['Results'][0];
          $("#_id").val(data._id);
          $("#doc_keywords").val(data.key_words);
          $("#doc_description").val(data.description);
          $("#old_file_location").val(data.file_location);
          $("#old_file_name").val(data.file_name);
          $("#old_file_size_kb").val(data.file_size_kb);
          $("#old_file_type").val(data.file_type);
          $("#old_collection_id").val(data.collection_id);
          $("#old_row_id").val(data.row_id);
          
        }
    }
  });
}


$( document ).ajaxComplete(function( event, xhr, settings ) {

  // after add   
  if ( settings.url === base_url+"core/Frame/load_content" )
  {
    $('#page_frame_'+pageFrameId).html(xhr.responseText);
  }

  // after add   
  if ( settings.url === base_url+"core/Apilocal/loadsingle" )
  {
    var data=xhr.responseText;
    var jsonResponse = JSON.parse(data);
    if(jsonResponse[0]["Results"]){
      $('#document_key_id').val(jsonResponse[0]["Results"][0]["key_id"]);
      $('#document_key_update_date').val(jsonResponse[0]["Results"][0]["key_update_date"]);
      $('#document_key_update_by').val(jsonResponse[0]["Results"][0]["key_update_by"]);
    }
  }
  
 
});


// get all documents
function getDocuments(collectionID, recordID) {
  resetAllFields();
  var url1="?job_id="+recordID+"&collid="+collectionID+"";
  var fram_prop_filter="bsd.row_id="+recordID+" AND bsd.collection_id="+collectionID+"";
    $("#fram_prop_filter_"+pageFrameId).val(fram_prop_filter);
    $("#fram_prop_url_"+pageFrameId).val(url1);
    $("#page_id_"+pageFrameId).val(page_id);
  // load frame data   
  var res = getFrameData(getFrameFromElement('', pageFrameId),0);
  return;
}

// Showing notes modal
$(document)
  .button()
  .on('click', '.bsit-upload-btn', function (e) {
    showUploadModal(e,"add");
});

 // Showing edit notes modal
$(document)
  .on('click', '.bsit-edit-upload-btn', function (e) {
    showUploadModal(e, "edit");
}); 

//edit Documents
$(document).on('click', 'a.edit-del-btn-'+pageFrameId+'.bsit-edit-btn', function (e) {  
  showUploadModal(e, "edit");
  var id = $(this).attr('data-recid');
  getDocumentsData(collectionID, recordID, id);
});



//Delete  Documents
$(document).on('click', 'a.edit-del-btn-'+pageFrameId+'.bsit-delete-btn', function (e) {  
  $('.loader').show();
  createModalDeleteDocuments();
  $('#ModalDeleteDocuments'+pageFrameId).modal();
  $('#bsit-upload-modal').modal('hide');
  $('.loader').hide();
});


// Deleting records using delete modal
$(document)
  .button()
  .on('click', '.bsit-commit-deletedoc', function(e) {
    commitDeleteDoc();
  });

var createModalDeleteDocuments = function () {
var divModal = '<div id="ModalDeleteDocuments" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">';
    divModal += '<div class="modal-dialog bsit_upload_popup" style="height: 100%; width:850px;">';
    divModal += '<div class="modal-content" style="float: left; width: 100%;">';
    divModal += '<div class="modal-header" style="padding-top: 0px; padding-bottom: 0px;">';
        divModal += '<button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">';
          divModal += '<svg class="" viewBox="0 0 24 24">';
          divModal += '<path fill="#000000"  d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z">';
          divModal += '</path>';
          divModal += '<path d="M0 0h24v24h-24z" fill="none"></path>';
          divModal += '</svg>';
        divModal += '</button>';
      divModal += '<h2 id="bsit-delete-modal-header" class="bsit_form_titel">Delete Documents</h2>';
    divModal += '</div>';
  divModal += '<div class="modal-body">';
  divModal += '<form method="post" class="mui-form bsit_form" autocomplete="off" id="deleteFormDocument'+pageFrameId+'" data-collid="'+UploadCollectionID+'">';
    divModal += '<input type="hidden" name="key_id" id="document_key_id" value=""/>';
    divModal += '<input type="hidden" name="key_update_date" id="document_key_update_date" value=""/>';
    divModal += '<input type="hidden" name="key_update_by" id="document_key_update_by" value=""/>';
    divModal += '<span class="bsit_side_nave_blank_title">Are you sure you would like to delete this?</span>';
  divModal += '</form>';
  divModal += '<div class="text-right" style="text-align:center;">';
  divModal += '<button id="commit-deleteDoc-'+pageFrameId+'"  class="bsit-commit-deletedoc mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Yes, Delete</button>';
  divModal += '<button class="mui-btn bsit-cancel-delete mui-btn--flat mui-btn--primary bsit_cancel_btn" data-dismiss="modal">No</button>';
  divModal += '</div>';
  divModal +='</div>';
  $('body').append(divModal);
                          
  $('#bsit-upload-modal').modal('hide');
  $('#ModalDeleteDocuments').modal();
}


// Delete records function
function commitDeleteDoc() {
  let form = $('#deleteFormDocument' + pageFrameId);
  let values = form.serializeObject();
  let collectionID = form.attr('data-collid');

  let data = Object.assign({
    colls: [{
      coll_id: collectionID,
      values: values
    }],
  });

  let requestData = {
    data: JSON.stringify(data),
  };

  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/delete',
    data: requestData,
    success: function (res) {
      $('#ModalDeleteDocuments').modal('hide');
      Materialize.toast('Delete document successfully!', 5000);
    },
    error: function (err) {

      if (err.status == 404) {
        err.responseText = 'The requested url was not found on this server.';
      } else if (err.status == 408) {
        err.responseText = 'API request timeout';
      } else if (err.status == 500 || err.status == 503) {
        err.responseText = 'Service Unavailable';
      }

      alert(err.responseText);
    },
  });
}



function onclickClose() {
  $(`.modal-backdrop`).on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
  });
}


// form submit 
var file_upload_url = base_url + "core/apilocal/documentfileuploadDoc";
$('form#upload_form').on('submit', function(e){  

 var _id = $('#_id').val();
 let imageData = $('#image_file').val();
 if(_id == '')
 {
    if (imageData == "") {
        $("#image_file_outer").addClass("required_border_class");
        return false;
      } else {
        $("#image_file_outer").removeClass("required_border_class");
      }
 } 

e.preventDefault();  
var form_data = new FormData();
var keywords = $('#doc_keywords').val();
var description = $('#doc_description').val();
let collID = $('#bsit-upload-modal-container').attr('data-collectionID');
let rowID = $('#bsit-upload-modal-container').attr('data-rowID');

var ins = document.getElementById('image_file').files.length;

    if(_id != '' && ins == 0 ){
        var file_location = $("#old_file_location").val();
        var file_name = $("#old_file_name").val();
        var file_size_kb = $("#old_file_size_kb").val();
        var file_type = $("#old_file_type").val();
        var collection_id = $("#old_collection_id").val();
        var row_id = $("#old_row_id").val();

        form_data.append("file_location", file_location);
        form_data.append("file_name", file_name);
        form_data.append("file_size_kb", file_size_kb);
        form_data.append("file_type", file_type);
        form_data.append("collection_id", collection_id);
        form_data.append("row_id ", row_id );
    }
    for (var x = 0; x < ins; x++) {
        form_data.append("files[]", document.getElementById('image_file').files[x]);
       
    }
    if(_id != ''){
        var collection_id = $("#old_collection_id").val();
        var row_id = $("#old_row_id").val();

        form_data.append("collection_id", collection_id);
        form_data.append("row_id", row_id );

    }else{
        form_data.append("collection_id", collID);
        form_data.append("row_id", rowID);
    }
    

    form_data.append("keywords", keywords);
    form_data.append("description", description);
    form_data.append("_id", _id);
    
    
    form_data.append("UploadCollectionID", UploadCollectionID);

$.ajax({  
    url: file_upload_url,   
    method:"POST",  
    data:form_data,  
    contentType: false,  
    cache: false,  
    processData:false,  
    dataType: "json",
    success:function(res)  
    {  
      if(_id != ''){
        $('#page_frame_'+pageFrameId).html('');
        $('.modal').modal('hide');
        Materialize.toast('All file Data Updated successfully!', 5000);
      }else{
        Materialize.toast('All files uploaded successfully!', 5000);
        getDocuments(collID, rowID);
      }
  }  

});  
//}  
});  

// var url = window.URL || window.webkitURL; // alternate use
function readImage(file) {
var reader = new FileReader();
var image  = new Image();
reader.readAsDataURL(file);  
reader.onload = function(_file) {
image.src = _file.target.result; // url.createObjectURL(file);
image.onload = function() {
var w = this.width,
h = this.height,
t = file.type, // ext only: // file.type.split('/')[1],
n = file.name,
s = ~~(file.size/1024) +'KB';
$('#uploadPreview').append('<img src="' + this.src + '" class="thumb">');  
};
/*image.onerror= function() {
alert('Invalid file type: '+ file.type);
}; */     
};
}
$("#image_file").change(function (e) {
if(this.disabled) {
return alert('File upload not supported!');
}
 $('#uploadPreview').html('');  
const fileList = e.target.files;
console.log(fileList);
$('#uploadPreview').html('<p>Total '+fileList.length+' files </p>');
var F = this.files;
if (F && F[0]) {
for (var i = 0; i < F.length; i++) {
readImage(F[i]);
}
}
});

