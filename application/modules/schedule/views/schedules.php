


<!-- full calendar styles -->
   
    <script src='<?php echo base_url();?>Schedule/assets/scheduler/lib/moment.min.js'></script>
	
    <script src='<?php echo base_url();?>Schedule/assets/scheduler/lib/fullcalendar.min.js'></script>
	<script src="<?php echo base_url();?>Schedule/assets/scheduler/scheduler.min.js"></script>


	<script src="<?php echo base_url();?>Schedule/assets/js/schedules.js"></script>

	  <!-- multiselect dropdoen bootstrap -->
 <script src="<?php echo base_url();?>Schedule/assets/js/select2.full.js"></script>
 <link rel="stylesheet" href="<?php echo base_url();?>Schedule/assets/css/select2.min.css">

	 <link href='<?php echo base_url();?>Schedule/assets/scheduler/lib/fullcalendar.min.css' rel='stylesheet' />
    <link href='<?php echo base_url();?>Schedule/assets/scheduler/lib/fullcalendar.print.min.css' rel='stylesheet' media='print' />
	<link href='<?php echo base_url();?>Schedule/assets/scheduler/scheduler.min.css' rel='stylesheet' />
	<link href='<?php echo base_url();?>Schedule/assets/css/bsit_schedule.css' rel='stylesheet' />
	<!-- full calendar JS -->



<?php
	$CURLDATA 	= 	Array('collections' =>
	json_encode(
		Array('colls'=> Array(
					// Users collection
					Array(
						'coll_name'	=> 'Users',
						'cols'		=> Array("_id", "name_first"),
						'coll_id'	=> '16',
						'filter'	=> "([usr].group_id <> 8 AND [sch]._id <> 3 AND [usr].is_active = 1)" // Ignore inactive users
					)
	))));
	// Retrieve data using the request we setup earlier
	$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
	$json_page_result = $this->API->CallAPI("GET",$BASEURLMETHOD,$CURLDATA);
	$json_result = json_decode($json_page_result);

	$user_data = $json_result->Data[0];
	// echo '<pre>';
 //print_r($user_data->Results);
	// echo '</pre>';
	echo('<script> var userArray = '.json_encode($user_data->Results).';</script>');
?>

<div id="calendarMainArea">
	<!-- class="mui-panel">-->
	<div class="bsit_left-right-scroll">
	<button id="slideBack" class="bsit_scroll_btn_sticky" type="button"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
    <button id="slide" class="bsit_scroll_btn_sticky" type="button"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
    </div>
    
	<div class="tableScrollTop" style="display: none;"><div style="width:1500px; height:20px;"></div></div>
	<div class="schedule_calendar bsit_schedule_calendar fc fc-unthemed fc-ltr bsit_cal_day_sticky bsit_schedules_date bsit_schedules_scroll"><div class="fc-view-container" style=""><div class="fc-view fc-timelineNext3Months-view fc-timeline fc-flat"><table id="bsit_cal_all_day_date">
	
</table>
</div>
</div>
</div>
	<input type="hidden" id="curr_user" value="<?php echo $userid; ?>">
	<div id="calendarBody">
		<div id='calendar' class="schedule_calendar bsit_schedule_calendar"></div>
	</div>
	<div id="calendarFooter"> </div>
</div>
<br/>
<div style="text-align: center;">SHIFT + Left mouse click on an existing event to duplicate it!</div>


<!-- MODALS -->
<div id="eventContent" class="modal fade in bsit_schedules_popup" style="width:100%; min-width:200px; margin:auto; overflow:auto; background-color: #ffffff">

	<div class="mui-panel " style="margin-bottom:0;min-height: 100%; box-shadow: none;">

		<div class="event_content_header">


			<div class="mui-row" style="border-bottom: 1px solid #e4e3e3;">
				<div class="mui-col-md-1" id="close_button">
					<button data-dismiss="modal" style="fill: white; padding: 0.6rem;" class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn">
						<svg viewBox="0 0 24 24" class="">
							<path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"
							/>
							<path fill="none" d="M0 0h24v24h-24z" />
						</svg>
					</button>
				</div>

				<div class="mui-col-md-5" style="padding-left: 0;">
					<form class="bsit_form">
						<div class="mui-textfield">
							<!--<label>Event Date Time</label>-->
							<div id="dateTimeSelector" style="display:flex; flex-wrap: wrap;">
								<div>
									<span>From:</span>
									<input name="fromDate" id="fromDateSelector" class="datepicker" style="width:120px;"> -
									<input name="fromTime" id="fromTimeSelector" class="timepicker bsit_visibility_hidden" style="width:70px;" />
								</div>
								<div>
									<span>To:</span>
									<input name="toDate" id="toDateSelector" class="datepicker" style="width:120px;">
									<input name="toTime" id="toTimeSelector" class="timepicker bsit_visibility_hidden" style="width:70px;" />
								</div>
								<div class="bsit_visibility_hidden">
									<div id="event_duration"></div>
								</div>
								<div style="padding-left: 15px;">									
									<span>Hours / Day:</span>								
									<input name="hours_per_day" type="number" min="0" id="hours_per_day" placeholder="Hours / Day" style="width:150px;">
								</div>
							</div>
						</div>
					</form>
				</div>
				
				<div class="mui-col-md-6 mui-col-xs-12">
				<div class="eventtypediv bsit_statusdiv bsit_form bsit_visibility_hidden" style="margin-bottom: 15px;">
					<div class="mui-select mui-textfield" tabindex="-1">
						<label>Event Type</label>
						<select name="Type" id="eventType" tabindex="0">
							<option id="ifDelete" value="planned">Planned</option>
							<option value="deleted">Deleted</option>
						</select>
					</div>
				</div>
				
				<div class="mui-col-md-3 worktypediv bsit_statusdiv bsit_form bsit_visibility_hidden">
					<div class="mui-select mui-textfield" tabindex="-1">
						<label>Work Type</label>
						<select name="WorkTypeId" id="selectWorkTypeId" tabindex="0">
							<?php foreach ($curlDataWorkType as $wt): ?>
							<?php
			   $selected1 = "";
				if ($userid==$user->UserID)
				{
					if($user->_id == $wt->_id)
					{
						$selected1 = " selected ";
					}
				}
				?>
								<option value="<?php echo($wt->_id)?>" <?php echo($selected1)?>>
									<?php echo($wt->name)?> </option>
								<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="bsit_save_schedules_btn">
					<div style="text-align:right;" id="submit_button">
						<button id="commit-event-addedit" class="mui-btn mui-btn--primary bsit_save_btn" style="min-width: 100px; height: 42px;">Save</button>
					</div>
				</div>
				<div id="completeCheckbox" class="1mui-checkbox bsit_check_schedules_btn">
					<label class="bsit_check1" data-toggle="tooltip" title data-original-title="Confirmed" data-placement="bottom">
						<input type="checkbox" id="checkbox-confirmed" name="is_active" style="float: left; margin-right: 5px;">
						<span class="bsit_schedules_checkmark"></span>
					<!-- Confirmed -->
					</label>

				</div>
				<!-- <input type="hidden" name="delete" value="planned" id="ifDelete"> -->
				<div class="bsit_schedules_deleted_btn">
						<button class="schedules_job_deleted_btn" id="schedules_calendar_delete_job" ><i class="fa fa-trash" aria-hidden="true"></i> <i class="fa fa-trash-o" aria-hidden="true"></i></button>
				</div>
				</div>
			</div>
			<!-- /.mui-row -->



			<form id="addEditEventForm" data-collid="15" class="bsit_form">
				<input type="hidden" name="key_id" id="key_id" value="" />
				<input type="hidden" name="StartDateTime" id="startTime" value="" />
				<input type="hidden" name="EndDateTime" id="endTime" value="" />
				<input type="hidden" name="ownerId" id="eventOwner" value="" />

				<div id="completeCheckbox" class="mui-checkbox bsit_sch_checkbox" style="margin-bottom: 20px; padding-left: 15px;">
									<!-- <label class="bsit_check">
										<input type="checkbox" id="checkbox-checked" name="is_active">
										Display Complete Jobs
										<span class="bsit_checkmark"></span>
									</label>
									
									<label class="bsit_check">
										<input type="checkbox" id="job-checkbox-checked" name="is_active" checked>
										Hide MSP Jobs
										<span class="bsit_checkmark"></span>
									</label> -->
									
									<label class="bsit_check_switch">
										<input type="checkbox" id="checkbox-opportunities" name="is_active">
										Display Opportunities
										<span class="bsit_checkmark_slider"></span>
									</label>
									
								</div>

				<div id="filtersSearch" class="mui-col-md-4 mui-col-xs-12" style="position: relative;">

					<div id="filtersClearSearch" style="display: none;">
						<img src="<?php echo BASEURL_IMAGE; ?>/ic_close_24px.svg" />
					</div>
					<!-- <div id="advancedSearchDropArrowCalendar" class="bsit_dropdoen_img">
						<img id="advancedSearchDownImg" src="<?php echo BASEURL_IMAGE; ?>/ic_arrow_down_24px.svg" />
						<img id="advancedSearchUpImg" style="display: none;" src="<?php echo BASEURL_IMAGE; ?>/ic_arrow_up_24px.svg" />
					</div> -->

					<div id="filtersDimList" class="bsit_dropdoen_img bsit_schedules_search_icon">
						<img src="<?php echo BASEURL_IMAGE; ?>/ic_search_black_24dp.png" />
					</div>


					<div class="mui-textfield ui-front job_sel bsit_form bsit_schedules_search_text_box" id="job_sel">
						<label>Search</label>
						<input type="hidden" id="sel_job_id" value="">
						<input name="jobId" class="" id="job_title" type="text" value="" data-coll="1" data-columns="['jobId','name','CustName','CustomerID','Status','ParentJobID']"
						    data-modalid="eventContent" placeholder="Search..." style="padding-left: 40px;" />
					</div>

				</div>
				<div id="advancedSearch" class="mui-col-md-8 mui-col-xs-12 bsit_schedules_advancedSearchDropArrow" style="padding: 0;">
					<div class="mui-row">
						<?php //echo "<pre>";print_r($curlDataCust);echo "</pre>"; ?>
						<div class="mui-col-md-6 bsit_statusdiv bsit_form mui-col-xs-12">
							<div class="mui-select mui-textfield" tabindex="-1">
								<label>Customer</label>
								<select name="orgsId" id="selectEventCustomerId" tabindex="0" style="margin-top: 0;min-height: 40px;">
									<option value=""></option>
									<?php foreach($curlDataCust as $cust){ ?>
									<option value="<?php echo($cust->_id)?>">
										<?php echo($cust->name)?>
									</option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="mui-col-md-6 mui-col-xs-12">
							<div class="mui-textfield ui-front bsit_form" id="parent_job_sel">
								<label>Project</label>
								<input type="hidden" id="sel_parent_job_id" value="">
								<input name="ParentJobID" class="autoCompColl1 autoCompColl_parent_job" id="parent_job_title" type="text" value="" data-returnpropname="ParentJobID"
								    data-coll="1" data-columns="['jobId','name','CustName','CustomerID']" data-modalid="eventContent" />
							</div>
						</div>
					</div>
				</div>

		</div>
		<!-- /.event_content_header -->

		

		<div class="event_content_body">

			<div class="job_list_main_div">
				<div class="cust_back_color bsit_schedules_job_list_block" id="job_list">
					<div class="mui-row" style="padding-bottom:15px;">
						<?php
					$cnt = 0;
					/*  echo "<pre>";
					print_r($JobsLists);
					exit;  */
					foreach($JobsLists as $JobsList)
					{
						$cust_name = '';
						if($JobsList->def_event_color == ''){
							$cust_colors[0] = '#000000';
							$cust_colors[1] = '#FFFFFF';
						}else{
							$cust_colors = explode(',',$JobsList->def_event_color);
							$cust_colors = str_replace('[','',$cust_colors);
							$cust_colors = str_replace(']','',$cust_colors);
							$cust_colors = str_replace('"','',$cust_colors);
						}
						?>

							<?php
							if(strlen($JobsList->title) > 125){$JobsList->title = substr($JobsList->title, 0, 125).'...';}
							if(strlen($JobsList->parent_title) > 75){$JobsList->parent_title = substr($JobsList->parent_title, 0, 75).'...';}
							?>

								<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_<?php echo $JobsList->_id; ?>">
									<span class="cust_color">
										<span class="job">
											<label>Job ID:<?php echo $JobsList->_id;?> </label>
											<span class="job_card_title">
												<?php echo $JobsList->title; ?>
											</span>
										</span>
										<span class="project">
											<label>Project:</label>
											<span class="job_card_project" data-pid="<?php echo $JobsList->parent_id; ?>">
												<?php echo $JobsList->parent_title; ?>
											</span>
										</span>
										<span class="icon">
											<span class="job_card_customer" data-cid="<?php echo $JobsList->customer_id; ?>" style="float: left;background-color:<?php echo $cust_colors[0]; ?>;color:<?php echo $cust_colors[1]; ?>;padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">
												<?php echo $JobsList->customerName; ?>
											</span>

											<?php
									$html_job_terms = '';
									if($JobsList->terms == 'Bill'){
										$html_job_terms = '<img src="'.BASEURL_IMAGE.'/clock.png">';
									}else if($JobsList->terms == 'QD'){
										$html_job_terms = '<img src="'.BASEURL_IMAGE.'/currency-usd.png">';
									}
									echo $html_job_terms;
									?>
										</span>
										<span class="job_load_status" style="display:none;">
											<?php echo $JobsList->status; ?>
										</span>
									</span>
								</div>
								<?php
							//echo '<input type="hidden" id="job_load_status" value="'.$JobsList->status.'" />';
						$cnt++;
						if($cnt % 4== 0) {echo '</div><div class="mui-row" style="padding-bottom:15px;">';}
					}
					?>
					</div>
				</div>
				<div id="add_job_box" class="bsit_visibility_hidden">
					<div class="mui-btn mui-btn--fab mui-btn--primary">+</div>
				</div>

				<div class="job_card_loader" style="display:none;">
					<div class="mdl-spinner mdl-js-spinner is-active material_loder"></div>
				</div>

			</div>

		</div>
		<!--/.event_content_body -->

		<div class="event_content_footer">

			<div class="mui-row">
				<div class="mui-col-md-5 statusdiv bsit_statusdiv bsit_visibility_hidden">
					<div class="mui-textfield bsit_form" style="margin-bottom: 0px;margin-top: 9px;">
						<label style="margin-top: 7px;">Status</label>
					</div>
					<div class="mui-select bsit_form" style="margin-top: 0;">
						<select name="Status" id="status" style="margin-top: 0;">
							<option value=""></option>
						</select>
					</div>
				</div>
				<div class="mui-col-md-7 bsit_visibility_hidden">
					<div class="mui-textfield bsit_form" style="margin-top: 15px;">
						<!--<textarea type="text" name="Description" id="eventInfo" value="" />
			</textarea>-->
						<input type="text" name="Description" id="description" placeholder="Invoice Notes..." style="margin-top: 9px;" />
						<!--<label>Event Description</label>-->
						<label>Invoice Notes</label>
					</div>
				</div>
			</div>

			<!-- ---------- [START] TaskId-2(Add new job from event screen)->17-Nov-2016 ---------- -->
			<div class="mui-row">
				<div id="eventMaterialCosts" style="display:none;">
					<div class="mui-textfield mui-col-md-6">
						<input type="text" name="MaterialCost" id="MaterialCost" />
						<label>Material Price Paid</label>
					</div>
					<div class="mui-textfield mui-col-md-6">
						<input type="text" name="MaterialChargePrice" id="MaterialChargePrice" />
						<label>Material Customer Charge</label>
					</div>
				</div>
			</div>


			<div class="mui-row mui-col-md-12 event_content_footer2" style="padding-top: 20px;">
				<div class="mui-textfield bsit_form">
					<input type="text" name="InternalNotes" id="internal_notes" placeholder="Internal Notes..." />
					<label>Internal Notes</label>
				</div>
			</div>
		</div>
		<!-- /.event_content_footer -->
		<?php //echo "<pre>";print_r($curlDataWorkType);echo "</pre>"; ?>

		<div class="mui-row mui-col-md-12 bsit_visibility_hidden">
			<div class="mui-select1" tabindex="-1" id="shared_event_div">
				<label>Shared Users</label>
				<select name="shared_user" id="shared_user_id" class="js-example-basic-multiple shared_user_selected" multiple="multiple">
					<?php foreach ($curlDataUsr as $user): ?>
					<?php if($userid!=$user->UserID){ ?>
					<option value="<?php echo($user->UserID)?>">
						<?php echo($user->FirstName." ".$user->LastName." - ".$user->Nickname)?>
					</option>
					<?php } ?>
					<?php endforeach; ?>
				</select>
			</div>
		</div>

		<div class="mui-row mui-col-md-12 job_card_loader1" style="display:none;">
			Loading Data....
		</div>

		</form>

	</div>
	<!-- / .mui-panel -->

	<div class="loader" style="display:none;">
		<div class="mdl-spinner mdl-js-spinner is-active material_loder"></div>
	</div>

</div>

<div id="jobContent" class="modal fade in bsit_schedules_popup" style="width:80%; min-width:200px; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;z-index: 99999; background-color: #ffffff">

<div class="mui-panel " style="margin-bottom:0; box-shadow: none;">
	<div class="mui-row">
		<div class="mui-col-md-12">
		
			<button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn" id="job_close_button" style="fill: white; float:right; padding: 0.6rem;"
				data-dismiss="modal">
				<svg class="" viewBox="0 0 24 24">
					<path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path>
					<path d="M0 0h24v24h-24z" fill="none"></path>
				</svg>
			</button>
			<h2 class="margin_0 bsit_form_titel"> Add </h2>
		</div>
	</div>
	<hr/>

	<form id="jobForm" data-collid="1" class="bsit_form">

		<div class="mui-row">
			<div class="mui-col-md-6 bsit_statusdiv bsit_form">
				<div class="mui-select mui-textfield" tabindex="-1">
					<label>Customer</label>
					<select name="job_orgsId" id="job_selectEventCustomerId" tabindex="0" required="required" class="bsit_custom_dropdown" >
						<option value=""></option>
						<?php foreach($curlDataCust as $cust){ ?>
						<option iscolor='<?php echo($cust->DefaultEventColor)?>' value="<?php echo($cust->_id)?>">
							<?php echo($cust->name)?>
						</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="mui-col-md-6">
				<div class="mui-textfield ui-front" id="job_parent_job_sel">
					<label>Project</label>
					<input type="hidden" id="job_sel_parent_job_id">
					<input name="job_ParentJobID" class="autoCompColl job_autoCompColl_parent_job" id="job_parent_job_title" type="text" required="required"
						value="" data-jobreturnpropname="ParentJobID" data-jobcoll="1" data-jobcolumns="['jobId','name','CustName','CustomerID']"
						data-modalid="jobContent" />
				</div>
			</div>
		</div>

		<div class="mui-row">
			<div class="mui-col-md-6">
				<div class="mui-textfield">
					<label>Cust Ref #</label>
					<input type="text" name="job_CustRefNum" id="job_CustRefNum" class="mui--is-empty" >
				</div>
			</div>
			<div class="mui-col-md-6 bsit_statusdiv bsit_form">
				<div class="mui-select mui-textfield">
					<label>Status</label>				
				
					<select name="job_Status" id="job_status" required="required">
						<option value=""></option>
					</select>
				</div>
			</div>
		</div>

		<div class="mui-row">
			<div class="mui-col-md-4">
				<div class="mui-textfield">
					<label>Estimate Hours</label>
					<input type="text" name="job_EstDurationHrs" id="job_EstDurationHrs" class="mui--is-empty margin_top_25">
				</div>
			</div>
			<div class="mui-col-md-4 bsit_statusdiv bsit_form">
				<div class="mui-select mui-textfield">
					<label>Work Type</label>
					<select name="job_workflow_id" id="job_selectWorkTypeId" tabindex="0" disabled>
						<option value=""></option>
						<?php foreach ($curlDataWorkType as $wt): ?>
						<?php
					   $selected1 = "";
						if ($userid==$user->UserID)
						{
							if($user->_id == $wt->_id)
							{
								$selected1 = " selected ";
							}
						}
						?>
							<option value="<?php echo($wt->_id)?>" <?php echo($selected1)?>>
								<?php echo($wt->name)?> </option>
							<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="mui-col-md-4 bsit_statusdiv bsit_form">
				<div class="mui-select mui-textfield" tabindex="-1">
					<label>Priority</label>
					<select name="job_Priority" id="job_Priority">
						<option value="0">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</div>
			</div>
		</div>

		<div class="mui-col-md-12">
			<div class="mui-textfield">
				<input type="text" name="job_title" id="job_name" class="mui--is-empty" required="required">
				<label>Job Title</label>
			</div>
		</div>

		<div class="mui-col-md-12">
			<div class="mui-textfield">
				<textarea name="job_desc" id="job_desc" class="mui--is-empty"></textarea>
				<label>Job Description</label>
			</div>
		</div>

		<div class="mui-col-md-12">
			<div class="mui-textfield">
				<input type="text" name="job_InternalNotes" id="job_InternalNotes" class="mui--is-empty">
				<label>Internal Notes</label>
			</div>
		</div>

		<div class="mui-row">
			<div class="mui-col-md-6 bsit_statusdiv bsit_form">
				<div class="mui-select mui-textfield" tabindex="-1">				
					<label>Job Terms</label>		

					<select name="job_TermsCode" id="job_TermsCode">
						<option value="Bill">Hourly Billable Work</option>
						<option value="NB">Not Billable</option>
						<option value="QD">Quoted Dollars - Must fill in quote below</option>
					</select>
				</div>
			</div>
			<div class="mui-col-md-6">
				<div class="mui-textfield">
					<input type="text" name="job_QuotedPrice" id="job_QuotedPrice" class="mui--is-empty margin_top_15">
					<label>Quoted Price</label>
				</div>
			</div>
		</div>
	</form>

	<div style="text-align:center;">
		<button id="save_job_data" class="mui-btn mui-btn--flat mui-btn--primary bsit_save_btn">Add</button>
	</div>
</div>

</div>

<!-- container -->
</div>
</div>
</body>

</html>
<script type="text/javascript">
	jQuery(function ($) {
		// ----------------------------
		// -- MULTI-SELECT DROPDOWN
		// ----------------------------
		//$(".js-example-basic-multiple").select2();
	});
</script>
