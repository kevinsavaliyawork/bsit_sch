
<?php

$dateOptions = "";
for ($i = 0; $i < 1; $i++) {
  $dateOptions .= '<option value="'.date('01 F Y', strtotime("-$i month")).'">'.date('F Y', strtotime("-$i month")).'</option>';
}

?>

<script>
// once the page has loaded
$().ready(function(){
  // setup some global variables used in various reports
  var userID;
  var dateStart;

  // A function to call the DB and load the data for the report
  function load_summary_data(userID,startDate){

  };

  // on submit of the dashboard parameters form
  $("#dashboardParams").submit(function(e){
    // don't submit the form
    e.preventDefault();

    //refresh each of the dashboard refresh_dashboard_elements
    refresh_dashboard_elements();

    return false;
  });

  // reload all frames after a page load or submit of parameters
  function refresh_dashboard_elements(){
    // Set the global variable used by various dashboard element
    userID = $('#userSelector').val();
    dateStart = $('#starDate').val();

    // refresh the elements of the dashboard
    summary_sch_chart_refresh();
    detail_sch_chart_refresh();

  }

  // The schedule summary chart at the top of the page
  var mySumChart=null;
  function summary_sch_chart_refresh(){
    // attempt to load the data straight away
    var load_event_data = {"parameters":[{"key":"@inputuserID","value":userID},{"key":"@startDate","value":dateStart}],"sp_name":[{"key":"BSIT_SchedSummary_rpt"}]};

    // Ajax call to the API to retrieve the list of events
    $.ajax({
      type: "POST",
      url: base_url+"core/Apilocal/Call_SP",
      data: load_event_data,
      dataType: 'json',
      success: function(results){
		  console.log(results);
        // Loop each of the results and format into the needed structure
        datasets = undefined;
        // planned, allday, actual-nonbillable, actual, deleted, material, Avail Mins
        var planned = [];
        var allday = [];
        var nonbillable = [];
        var actual = [];
        var avail = [];
        var dates_list = [];
        $.each(results, function(key, value) {
          // add this value to the array for display
          planned.push(value.planned_hrs);
          actual.push(value.actual_hrs);
          avail.push(value.avail_hrs);
          nonbillable.push(value.nonbill_hrs);

          // Add the date to the array if not already
          dates_list.push(value.date);
        });

        // setup a data set for use in the chart
        var sumChartData = {
            datasets: [
              {
                label: 'Planned',
                type: 'line',
                data: planned,
                //steppedLine: true,
                borderDash: [5,2],
                borderColor: 'rgba(0, 0, 150, 0.8)',
                backgroundColor: 'rgba(0, 0, 150, 0.4)',
                fill: false,
                lineTension: 0.125
              },
              {
                label: 'Actual',
                data: actual,
                stack: 'actual',
                borderColor: 'rgba(0, 150, 0, 0.8)',
                backgroundColor: 'rgba(0, 150, 0, 0.4)',
                fill: false
              },
              {
                label: 'Avail',
                data: avail,
                type: 'line',
                pointStyle: 'line',
                pointRadius: 20,
                pointBorderWidth: 4,
                pointHoverBorderWidth: 4,
                pointHoverRadius:20,
                borderColor: 'rgba(200, 0, 0, 0.8)',
                backgroundColor: 'rgba(200, 0, 0, 0.4)',
                showLine: false,
                fill: false
              },
              {
                label: 'Actual Non-Billable',
                data: nonbillable,
                stack: 'actual',
                //borderColor: window.chartColors.blue,
                //backgroundColor: window.chartColors.blue,
                fill: false
              },
            ],
            labels: dates_list
        };

        // clear the canvas of old charts and reload
        if(mySumChart != null){
          mySumChart.destroy();
        }
        mySumChart=null;
        // clear the HTML element and we will redraw to fix the height issue
        $("#mySumChart_container").html();
        $("#mySumChart_container").html('<canvas id="mySumChart" style="width:100%; height:100%;"></canvas>');

        // load the chart
        var ctx = document.getElementById("mySumChart").getContext('2d');
        mySumChart = new Chart(ctx, {
          type: 'bar',
          data:sumChartData,
          options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
              xAxes: [{
                type: 'time',
                display: true,
                ticks: {
                  major: {
                    fontStyle: 'bold',
                    fontColor: '#FF0000'
                  }
                }
              }]
            }
        }
      });

      },
      error: function(e, x, y) {
        console.log(e);
        console.log(x);
        console.log(y);
      }
    });
  }

  // The schedule details chart at the bottom of the page
  var myDetChart=null;
  function detail_sch_chart_refresh(){

    // get the parameters from the form
    /*
    var url = "<?php echo base_url();?>Farmdata/temperature?farmID="+farmID;
    $.getJSON(url,function(data){
        console.log(arguments);

        // Get the results for display
        SensorData = data.Data;

        // Get the results for display
        SensorData = data.Data;
        var vatDates =[];
        var Milk_Vat_Temperature =[];

        for(i=0; i<SensorData.length; i++){
          var rowData = SensorData[i];
          vatDates.push(rowData.vatDates);
          Milk_Vat_Temperature.push(rowData.Milk_Vat_Temperature);
        }

    		var tempChartData = {
            datasets: [
              {
                label: 'Milk Vat',
                data: Milk_Vat_Temperature,
                borderColor: window.chartColors.blue,
                backgroundColor: window.chartColors.blue,
                fill: false
              }
            ],
            labels: vatDates
        };

        // clear the canvas of old charts and reload
        if(myDetChart != null){
          myDetChart.destroy();
        }
        myDetChart=null;

        // clear the HTML element and we will redraw to fix the height issue
        $("#myDetChart_container").html();
        $("#myDetChart_container").html('<canvas id="myDetChart" style="width:100%; height:100%;"></canvas>');

        // load the chart
        var ctx = document.getElementById("myDetChart").getContext('2d');
        myDetChart = new Chart(ctx, {
          type: 'line',
          data:tempChartData,
          options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
              xAxes: [{
                type: 'time',
                display: true,
                ticks: {
                  major: {
                    fontStyle: 'bold',
                    fontColor: '#FF0000'
                  }
                }
              }]
            }
        }
      });
      myDetChart.update();
    });
    */
  };


  // refresh all chart data on initial load
  refresh_dashboard_elements();
  // hide the side redraw
  $("#sidedrawer").parent().toggleClass('hide-sidedrawer');

});
</script>

<div class="dashboard-title secondary_bg_color">
  See Actual and Scheduled Time
</div>
<div class="secondary_bg_color">
  <form class="dashboard-params-main" id="dashboardParams">
    <input type="hidden" id="starDate" value="<?php echo(date("Y-m-d")); ?>" />
    <div class="dashboard-param">
      <label>
        User</label>
      <select id="userSelector">
		<option value="all">All</option>
        <?php
		print_r($SchUsrList);
        // setup the drop list with all users
        foreach($SchUsrList as $user_info){
          echo('<option value="'.$user_info->UserID.'">');
          echo($user_info->FirstName.' '.$user_info->LastName);
          echo('</option>');
        }
        ?>
      </select>
    </div>
    <!--<div class="dashboard-param">
      <label>Date</label>
      <select id="dateStartSelector">
        <?php echo($dateOptions)?>
      </select>
    </div>-->
    <button type="submit" id="dashboardSubmit" class="dashboard-btn">Submit</button>
  </form>
</div>
