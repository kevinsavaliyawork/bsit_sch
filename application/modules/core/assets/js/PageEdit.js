/*
  Author:       Nick Dallafiore
  Date:         12/12/2017
  Description:  Provide functionality of an 'edit bar' that allows users with permissions
  specified in the 'header.php' view, to add, edit and delete frames.
*/

$(document).ready(function () {
  $(document).click(function (event) {
    event.stopPropagation();
  });

  //#region FUNCTION BUTTONS

  // ENTER EDIT MODE - SHOW EDIT BAR
  $('#editBtn').click(function () {
    // Hide the edit button and the sidedrawer
    $(this).css('display', 'none');
    $('#sidedrawer')
      .parent()
      .toggleClass('hide-sidedrawer');

    var content = $('#editBar');

    // If the editbar isn't visible, show it.
    if (!content.is(':visible')) {
      $(content).css('display', 'inline-block');
      content
        .animate(
          {
            minHeight: '64px',
          },
          400,
          function () { }
        )
        .promise()
        .done(function () {
          // Show all edit components after animating, and apply the overlay
          // for frames so they can be selected and edited.
          content.find('.edit-component').css('display', 'inline-block');
          $('.edit-dropdown-div').css('display', 'inline-block');
          $('.edit-div-overlay').css('visibility', 'hidden');
          $('.edit-btn').css('display', 'inline-block');
          $('.edit-div-overlay').css('pointer-events', 'all');
          $('#editBtn').css('display', 'none');
          $('.edit-pointer-cancel').css('pointer-events', 'none');

          $('.main_content_part_class').each(function (i, e) {
            var frameId = $(e).attr('frameid');
            var dataframeid = $(e).attr('data-frameid');
            var frameName = $(e).attr('frameName');
            var pageId = $(e).attr('pageId');
            var collection_id = $(e).attr('collection_id');
            var pageFrameId = $(e).attr('data-frameid');

            var moduleUrl = base_url + 'core/Pageedit/frameproperties';
            var postData = {
              frameId: frameId,
              frameName: frameName,
              pageId: pageId,
              pageFrameId: pageFrameId,
              collection_id: collection_id,
              dataframeid: dataframeid,
            };

            jQuery.ajax({
              url: moduleUrl,
              type: 'POST',
              data: postData,
              success: function (data) {
                $('#main_content_part' + dataframeid).html(data);
              },
            });
          });
        });
    }

  });



  // HIDE EDIT BAR
  $('#exitEditBtn').click(function () {
    //  $(".mdl-cell").remove();
    //  $('.main_content_part_class').show();
    // Show the sidedrawer and remove div overlay
    $('#sidedrawer')
      .parent()
      .toggleClass('hide-sidedrawer');
    $('.edit-div-overlay').removeClass('edit-div-outline');

    var content = $('#editBar');

    // Hide all edit bar components then animate the bar out.
    if (content.is(':visible')) {

      $('.edit-btn').css('display', 'none');
      content.find('.edit-component').css('display', 'none');
      $('.edit-dropdown-div').css('display', 'none');
      $('.edit-div-overlay').css('visibility', 'hidden');
      $('.edit-div-overlay').css('pointer-events', 'none');
      $('.edit-pointer-cancel').css('pointer-events', 'all');
      content
        .animate(
          {
            minHeight: '0px',
          },
          400,
          function () { }
        )
        .promise()
        .done(function () {
          content.css('display', 'none');
          $('#editBtn').css('display', 'inline-block');
        });

      location.reload();
    }
  });

  // Animate out the content of each edit bar btn component
  $('.edit-btn').click(function () {
    // Get starting height
    if (
      $(this).attr('id') != 'exitEditBtn' &&
      $(this).attr('id') != 'saveBtn'
    ) {
      var divContent = $(this).next();

      if (!divContent.hasClass('edit-show')) {
        //hideDropdownContent(this);
        divContent.css('height', '0px');
        divContent.toggleClass('edit-show');
        divContent.animate(
          {
            height: divContent.get(0).scrollHeight,
          },
          300,
          function () {
            divContent.height('auto');
          }
        );
      } else {
        divContent.animate(
          {
            height: 0,
          },
          300,
          function () {
            divContent.height('auto');
            divContent.toggleClass('edit-show');
          }
        );
      }
    }
  });

});


$(document).on('click', '.columnCheckFrame', function () {
  if ($(this).is(':checked')) {
    var checked = 1;
  } else {
    var checked = 0;
  }
  var column_name = $(this).attr('column_name');
  var name = $(this).attr('name');
  var column_id = $(this).attr('column_id');
  var frameID = $(this).attr('frameID');
  var property_id = $(this).attr('property_id');

  var moduleUrl = base_url + 'core/PageEdit/addEditProperty';
  var postData = {
    column_id: column_id,
    property_id: property_id,
    checked: checked,
    name: name,
    column_name: column_name,
    frameID: frameID,
  };
  jQuery.ajax({
    url: moduleUrl,
    type: 'POST',
    data: postData,
    context: this,
    beforeSend: function () {
      pageLoaderShow();
    },
    complete: function () {
      pageHideLoader()
    },
    success: function (data) {
      var dataCheck = JSON.parse(data);

      if (dataCheck.HasError == true) {
        alert(dataCheck.ErrorMessage);
        return false;
      } else {
        if (dataCheck.Data != null) {
          $(this).attr('column_id', dataCheck.Data[0]['_id']);
        }

        if ($(this).is(':checked')) {
          $(this)
            .closest('td')
            .next()
            .attr('column_id', dataCheck.Data[0]['_id']);
          $(this)
            .closest('td')
            .next()
            .addClass('col_name_cust_frame');
          $(this)
            .closest('td')
            .next()
            .trigger('click');
          $(this)
            .closest('tr')
            .addClass('highliteRow');
        } else {
          $(this)
            .closest('td')
            .next()
            .attr('column_id', '');
          $(this)
            .closest('td')
            .next()
            .removeClass('col_name_cust_frame');
          $(this)
            .closest('tr')
            .removeClass('highliteRow');
          $(this).attr('column_id', '');
          $('.collec-frame-properties').hide();
          $('.collec-frame-table').hide();
          $('#control_property').hide();
        }
      }
    },
  });
});

$(document)
  .button()
  .on('click', '.col_name_cust_frame', function () {
    $('tr').removeClass('highliteRow');

    var type = $(this).attr('type');
    var frameID = $(this).attr('frameID');
    var dataframeid = $('#dataframeid').val();

    if (type == 'property') {
      var id = $(this).attr('column_id');
      $('#key_id').val(id);
    } else if (type == 'table') {
      var id = $(this).attr('frameID');
      $('#key_id_table').val(id);
    } else if (type == 'collection') {
      var id = $(this).attr('pageFrameId');
      $('#key_id_coll').val(id);
    }

    var moduleUrl = base_url + 'core/Pageedit/showPropertyData';
    var postData = { id: id, type: type, dataframeid: dataframeid };

    jQuery.ajax({
      url: moduleUrl,
      type: 'POST',
      data: postData,
      context: this,
      beforeSend: function () {
        pageLoaderShow();
      },
      complete: function () {
        pageHideLoader()
      },
      success: function (data) {
        $('.propertyData').html(data);

        $(this)
          .closest('tr')
          .addClass('highliteRow');
        if (type == 'property') {
          $('.table_name_prop').text(': ' + $('#frameName' + frameID).val());
          $('.property_name_prop').text(': ' + $(this).html());
        } else if (type == 'table') {
          $('.table_name_prop').text(': ' + $(this).html());
          $('.property_name_prop').text('');
          $('.property_name_prop').text();
        } else {
          //  $(".table_name_prop").text(": "+$(this).html());
          $('.table_name_prop').text('');
          $('.property_name_prop').text('');
        }
      },
    });
  });

$(document)
  .button()
  .on('click', '.deleteActionFrame', function () {
    var confirm_delete = confirm('Are you sure you want to delete ?');

    if (confirm_delete == true) {
      var frameID = $(this).attr('frameID');
      var pageFrameId = $(this).attr('pageframeid');

      var moduleUrl = base_url + 'core/Pageedit/deleteFrame';
      var postData = { frameID: frameID, pageFrameId: pageFrameId };
      jQuery.ajax({
        url: moduleUrl,
        type: 'POST',
        data: postData,
        context: this,
        beforeSend: function () {
          pageLoaderShow();
        },
        complete: function () {
          pageHideLoader()
        },
        success: function (data) {
          var dataCheck = JSON.parse(data);

          if (dataCheck.HasError == true) {
            alert(dataCheck.ErrorMessage);
            return false;
          } else {
            location.reload();

            return false;
          }
        },
      });
    }
  });

$(document).on('click', '.page_filter', function () {
  $('.loader').show();
  var pageFrameId = $(this).attr('pageFrameId');

  $('#frame' + pageFrameId).attr(
    'src', base_url + 'page/permanent_filters?PageFrameID=' + pageFrameId + '&headerPage=1'
  );

  $('#frame' + pageFrameId).load(function () {
    pageHideLoader();
    $('#pagePopup' + pageFrameId).modal();
  });
});

$(document)
  .button()
  .on('click', '.frame_permission', function () {
    $('.loader').show();

    var frameId = $(this).attr('frameId');

    $('#frame_permission').attr(
      'src',
      base_url + 'page/framepermissions?frameID=' + frameId + '&headerPage=1'
    );

    $('#frame_permission').load(function () {
      pageHideLoader();
      $('#framePopup' + frameId).modal();
    });
  });

$(document)
  .button()
  .on('click', '#saveframe', function () {
    var form = $('#allcoll');
    var values = form.serializeObject();

    values.pageId = $('#pageIdHeader').val();

    var fail = false;
    var fail_log = '';
    form.find('select, textarea, input').each(function () {
      if (!$(this).prop('required')) {
      } else {
        if (!$(this).val()) {
          fail = true;
          name = $(this).attr('name');
          fail_log += name + ' is required \n';
          $('[name=' + name + ']').addClass('required_border_class');
          $('[name=' + name + ']')
            .next('label')
            .addClass('required_label_class');
        }
      }
    });

    if (!fail) {
      $.ajax({
        type: 'POST',
        url: base_url + 'core/Pageedit/addpageframe',
        data: { data: values },
        dataType: 'json',
        beforeSend: function () {
          pageLoaderShow();
        },
        complete: function () {
          pageHideLoader()
        },
        success: function (returnData) {
          $('#allcollpopup').modal('hide');
          $(form).trigger('reset');
          location.reload();
        },
        error: function (xx) {
          $('#allcollpopup').modal('hide');
          $(form).trigger('reset');
        },
      });
    }
  });

$(document)
  .button()
  .on('click', '#addframe', function () {
    var pageId = $('#pageIdHeader').val();

    $('#allcollpopup').modal();

    var moduleUrl = base_url + 'core/Pageedit/getCollection';
    var postData = { userId: 0, device: 0 };
    jQuery.ajax({
      url: moduleUrl,
      type: 'POST',
      data: postData,
      beforeSend: function () {
        pageLoaderShow();
      },
      complete: function () {
        pageHideLoader()
      },
      success: function (data) {
        var dataAll = JSON.parse(data);
        if (dataAll.HasError == true) {
          alert(dataAll.ErrorMessage);
          return false;
        } else {
          var newtradeContent = '';

          $('#collId')
            .empty()
            .append('<option value="">SELECT COLLECTION</option>');

          $.each(dataAll.Data[0].Results, function (k, v) {
            newtradeContent +=
              '<option value="' + v.CollID + '">' + v.Name + '</option>';
          });

          $(newtradeContent).appendTo($('#collId'));
        }
      },
    });
  });

function pageLoaderShow() {
  $('.loader').show();

  //add new loader in count | if one ajax complete but second request is pending then it will help
  let loadcount = parseInt($(".loader").attr("loadcount"));
  if (loadcount) {
    loadcount += 1;
  } else {
    loadcount = 1;
  }

  $(".loader").attr("loadcount", loadcount);

}

function pageHideLoader() {
  let loadcount = parseInt($(".loader").attr("loadcount"));
  if (loadcount) {
    loadcount -= 1;
  } else {
    loadcount = 0;
  }

  if (loadcount == 0) {
    $('.loader').hide();
  }

  $(".loader").attr("loadcount", loadcount);

}