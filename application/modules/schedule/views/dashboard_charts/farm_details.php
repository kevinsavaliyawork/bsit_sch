<div class="chart_main">
  <div class="chart_head" style="border-bottom:thin solid #cccccc;">
    <div class="chart_title">FARM Details - Latest Info</div>
    <div class="chart_chart_subhead" id="farmDescription">...</div>
  </div>
  <div class="chart_body_icondetails">
    <div class="chart_metric_iconleft">
      <div class="chart_image_cont">
        <img src="<?php echo base_url();?>assets/images/dashboard_images/elect.jpg"/>
      </div>
      <div style="width:100%">
        <div class="chart_info_textblock" id="elecChargeDesc">...</div>
        <div class="chart_milk_aveminmax">
          <div class="chart_milk_aveminmax_metric">
            <div style="width:100%">
              <div class="chart_primary_metric" id="tariffRatePeak">...</div>
              <div class="chart_primary_symbol">c/kWh Peak tariff rate</div>
            </div>
          </div>
          <div class="chart_milk_aveminmax_metric">
            <div style="width:100%">
              <div class="chart_primary_metric" id="tariffRateOffPeak">...</div>
              <div class="chart_primary_symbol">c/kWh Off-peak</div>
            </div>
          </div>

          <div class="chart_milk_aveminmax_metric">
            <div style="width:100%">
              <div class="chart_primary_metric" id="dailySupplyCharge">...</div>
              <div class="chart_primary_symbol">Daily Supply Charge</div>
            </div>
          </div>
        </div>
    </div>
  </div>
    <div class="chart_metric_iconleft">
      <div class="chart_image_cont">
        <img src="<?php echo base_url();?>assets/images/dashboard_images/milk-can.jpg"/>
      </div>
      <div style="width:100%">
        <div class="chart_info_textblock"> 1000 Litres per day</div>
        <div class="chart_milk_aveminmax">
        <div class="chart_milk_aveminmax_metric">
          <div style="width:100%">
            <div class="chart_primary_metric" id="ltrPerDayMin">...</div>
            <div class="chart_primary_symbol">min</div>
          </div>
        </div>
        <div class="chart_milk_aveminmax_metric">
          <div style="width:100%">
            <div class="chart_primary_metric" id="ltrPerDayAve">...</div>
            <div class="chart_primary_symbol">ave</div>
          </div>
        </div>
        <div class="chart_milk_aveminmax_metric">
          <div style="width:100%">
            <div class="chart_primary_metric" id="ltrPerDayMax">...</div>
            <div class="chart_primary_symbol">max</div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="chart_metric_iconleft">
      <div class="chart_image_cont">
        <img src="<?php echo base_url();?>assets/images/dashboard_images/cow.png"/>
      </div>
      <div>
        <div class="chart_info_textblock" id="dairyDescription">...</div>
        <div class="chart_primary_metric" id="dairyNumberOfCows">...</div>
      </div>
    </div>
  </div>
</div>
