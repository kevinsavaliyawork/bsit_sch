<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Frame Controller
 *
 * @category Controller
 * @package  NA
 * @author   BrainStorm IT <admin@brainstormit.com.au>
 * @license  brainstormit.com.au N/A
 * @link     NA
 */

require_once(APPPATH.'/modules/core/libraries/Mobile_Detect.php');
$detect = new Mobile_Detect;

class Frame extends brain_controller
{
    public $detectDevice = ''; 
    /**
     * Load required libraries, models, set error reporting.
     */
    public function __construct()
    {
        parent::__construct(0);
        $this->load->model('framemodel');
        $this->load->model('pagemodel');
        // $this->load->model('Bsit_io');
        $this->load->library('session');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        
        $detect = new Mobile_Detect;
        $this->detectDevice = ($detect->isMobile()) ? 'mobile' : 'web';
    }

    /**
     * LOAD HEADER DATA FOR EACH FRAME
     * Get the associated collection and properties
     * Get the Page size, page #, filters, sort order
     * Load data for collection
     *
     * @param string $frame Frame to load
     * @param string $other_func_data Data to load
     *
     * @return $frame_result_data
     */
    public function load($frame, $other_func_data)
    {

        $pf_val = '';

        $sorting = $other_func_data['sorting'];
        $filtering = $other_func_data['filtering'];
        $page_number = $other_func_data['page_number'];
        $number_of_records = $other_func_data['number_of_records'];
        $pf = $other_func_data['pf'];
        $download_data = $other_func_data['download_data'];

        if ($download_data == 1) {
            $page_number = 1;
            $number_of_records = '9999';
        }

        // -- REQUEST THE FILTER FROM PERMANENT FILTER
        // setup the default Filter Values from Parmanant Filter
        $pf_val = $this->getPermanentFilterData($frame, 2, $sorting);

        $htmlData = array();
        $html = '';
        $html_filter = '';

        if ($pf == 1) {
            if ($filtering == '') {
                $filtering_all = $frame['filter'];
            } else {

                if (substr($filtering, 0, 4) != ' AND') {


                    $frame['filter'] = $frame['filter'] . ' AND ';
                }
            }

            $filtering_all = $frame['filter'] . '' . $filtering;
        } else {
            // SET FILTER VARIABLE WITHOUR AJAX LOAD(FIRST TIME PAGE LOAD)
            if ($pf_val != '') {
                $filtering_all = $frame['filter'] . ' AND (' . $pf_val . ')';
            } else {
                $filtering_all = $frame['filter'] . '' . $pf_val;
            }
        }


        // -- CODE FOR GETTING DATA FROM HISTORY TABLE AND APPLYING IT IN PAGE
        $page_result_history_api = $this->fetchHistory($frame);

        if (isset($page_result_history_api[0]->Results) && !empty($page_result_history_api[0]->Results)) {
            $page_number = $page_result_history_api[0]->Results[0]->page_num;
            $number_of_records = $page_result_history_api[0]->Results[0]->num_rec;

            $frame['Properties'][0]->default_page_size = $number_of_records;

            if ($page_result_history_api[0]->Results[0]->filter != '') {
                if ($frame['filter'] == '') {
                    $filtering_all = $page_result_history_api[0]->Results[0]->filter;
                } else {
                    $filtering_all = $frame['filter'] . ' AND ' . $page_result_history_api[0]->Results[0]->filter;
                }
            } else {
                $filtering_all = $filtering_all;
            }
            if ($page_result_history_api[0]->Results[0]->orders == '' && $sorting != '') {
                $sorting = $sorting;
            } else {
                $sorting = $page_result_history_api[0]->Results[0]->orders;
            }
        }

        // -- LOAD DATA FOR EACH FRAME
        // loop through each frame and call a data load from the server
        // THIS NEEDS TO BE REWRITTEN, ONE REQUEST CAN BE MADE TO THE SERVER
        // TODO: REWRITE
        // FOR ALL FRAMES.  THEN BASED ON THE REQUEST_SEQ, RESULTS CAN
        // ITTERATED AND ASSIGNED AS WE ARE DOING.

        $frame_result_load_data = $this->framemodel->loadFrameData($frame['collection'], $sorting, $filtering_all, $page_number, $number_of_records);

        return $data['FD' . $frame['PFID']] = $frame_result_load_data;
    }

    // [end] STATUS WISE PAGINATION FOR TRELLO STYLE INTERFACE...

    /**
     * LOAD HEADER DATA FOR EACH FRAME
     * Shows Title, ADD Button, Filters Code, Modal Code
     * @param string $frame     Frame to load
     * @param string $load_data Data to load
     *
     * @return html
     */
    public function frame_header($frame, $load_data, $frame_filter_data)
    {

        $cacheDir = APPPATH . "views/core/cache/";
        $html_filter = '';
        $html_filter1 = '';
        $filter_name = '';
        $filter_list1 = '';

        // -- REQUEST THE FILTER FROM PARMANANT FILTER

        // setup the default Filter Values from Parmanant Filter

        $filter_data_all = $this->getPermanentFilterData($frame, 1, $sorting);


        // ------ [Start] GETTING FILTER FORM HISTORY TABLE(USED IT WHEN CLICKED ON BACK BUTTON FROM BROWESR)
        // -- CODE FOR GETTING DATA FROM HISTORY TABLE AND APPLYING IT IN PAGE
        $history_page_num = '';

        // fetch history record
        $page_result_history_api = $this->fetchHistory($frame);


        $history_filters_vals = $page_result_history_api[0]->Results[0]->history_full_state;
        $history_page_num = $page_result_history_api[0]->Results[0]->page_num;


        if ($history_filters_vals != '') {
            $history_filters_vals = json_decode($history_filters_vals);
            $history_filters_vals = $history_filters_vals->history_api_back_val;
            $history_filters_vals = explode("&&", $history_filters_vals);

            foreach ($history_filters_vals as $history_filters_val) {
                $history_filter_array_datas[] = $history_filters_val;
            }
        }

        foreach ($history_filter_array_datas as $history_filter_array_data) {
            $get_history_filter_prop_ids = explode("**", $history_filter_array_data);

            foreach ($frame['Properties'] as $property) {
                if ($property->Alias . '.' . '[' . $property->PropColumn . ']' == $get_history_filter_prop_ids[0]) {
                    $get_history_filter_prop_ids[0] = $property->PropId;
                    $his_final_filter_values[] = $get_history_filter_prop_ids[0] . '_' . $get_history_filter_prop_ids[1] . '_' . $get_history_filter_prop_ids[2] . '_' . $get_history_filter_prop_ids[3];
                }
            }
        }

        if (isset($filter_data_all) && isset($his_final_filter_values)) {
            // $pf_filter_data_all = $his_final_filter_values;
            $filter_data_all = array_merge($filter_data_all, $his_final_filter_values);
        } elseif (isset($filter_data_all)) {
            $filter_data_all = $filter_data_all;
        } elseif (isset($his_final_filter_values)) {
            $filter_data_all = $his_final_filter_values;
        }




        // ------ [End] GETTING FILTER FORM HISTORY TABLE(USED IT WHEN CLICKED ON BACK BUTTON FROM BROWESR) ------ //
        // SET PARTICULOR PROPERTY WISE FILTER VARIABLE AND CREATE DIVs FOR THAT


        $fullPath   = $frame['fullPath_header'];
        $viewPath   = $frame['viewPath_header'];

        $filter_list = $this->propertyFilter($frame, $filter_data_all);

        // -- HEADER CODE START FROM HERE
        // Its include : Title, ADD Button & Filters Code

        $html = $this->loadHeader($frame, $filter_list);

        // CREATING HEADER CACHE FILE & INSERT DATA
        $cacheFile = fopen($fullPath, "w+") or die("Unable to open file!");
        fwrite($cacheFile, $html);
        fclose($cacheFile);

        // CREATE HEADER_VIEW CACHE FILE & CALL FROM HERE
        // Its Inclue : modal code (Add/Edit/Delete/Nots)
        $data_pass['frame'] = $frame;
        $data_pass['load_data'] = $load_data;
        $data_pass['frame_filter_data'] = $frame_filter_data;

        $html_model .= $this->load->view("header_view.php", $data_pass, true);
        $html .= $html_model;
        return $html;
    }

    // -- LOAD CONTENT DATA OF THE PAGE
    // Use the is_core_module for check it is core or custom module
    // if it is custom module then we will redirect to particular custom module of controller and function

    public function frame_content($frame, $load_data, $frame_filter_data)
    {


        $data_pass['frame'] = $frame;
        $data_pass['load_data'] = $load_data;
        $data_pass['frame_filter_data'] = $frame_filter_data;
        $data_pass['detect_device'] = $this->detectDevice;

        if ($frame['Properties'][0]->is_core_module == 1) {

            $page_content_part = $this->load->view('TableView', $data_pass, true);
        } else {

            if ($frame['Properties'][0]->controller_name != '') {

                $displayController = explode("/", $frame['Properties'][0]->controller_name);
                $controllerName = $displayController[0];
                $functionName = $displayController[1];
            }

            $this->load->model('Bsit_io', 'API');
            $data_pass['objectdata'] = $this;

            require_once(APPPATH . 'modules/' . $frame['Properties'][0]->module_name . '/controllers/' . $controllerName . '.php');

            $oHome =  new $controllerName();

            $data_pass['module_name'] = $frame['Properties'][0]->module_name;
			
			// custom view in 89 task progressbar 
            if((($functionName == 'customerSupportTask' ) || ($functionName == 'customerProjectsTask' ) ) && $controllerName == 'Customer' )
            {
               $page_content_part .= $this->load->view('TableViewCustom', $data_pass, true);
            }
            $page_content_part = $oHome->$functionName($data_pass);
        }

        return $page_content_part;
    }

    // -- LOAD WHOLE PAGE
    // Load Data for particulor Page.

    public function load_frame($frame, $other_func_data, $count_frame, $enableTabbedFrames)
    {
        //$load_data = $this->load($frame, $other_func_data);
        if ($enableTabbedFrames == 1) {
            if ($count_frame == 1) {
                $load_data = $this->load($frame, $other_func_data);
            }
        } else {
            $load_data = $this->load($frame, $other_func_data);
        }


        if ($load_data->HasError == 0) {
            if (isset($load_data->Data)) {
                $load_data = $load_data->Data;
            }

            $frame_filter_data = $load_data[0]->filter;

            $load_data  = $load_data[0]->Results;
            //$frame['allow_view'] = 0;

            if (isset($frame['allow_view']) && $frame['allow_view'] == 0) {
                $html .= '<div class="brain_error brain_error_query">';
                $html .= '<div class="container error-block table-responsive mui-panel">';
                $html .= '<div class="row">';
                $html .= '<div class="col-lg-12 col-sm-12 col-xs-12">';
                $html .= '<div class="right-content col-lg-12">';
                $html .= '<div class="col-lg-3">';
                $html .= '<div class="img-block-icon1"><img src="' . base_url() . 'assets/core/images/green.png" /> </div>';
                $html .= '</div>';
                $html .= '<div class="col-lg-9 ">';
                $html .= '<p class="error_content"><b>Error message : </b> You are not Authorized to Perform this Action</p>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
            } else {
                $html .= $this->frame_header($frame, $load_data, $frame_filter_data);
                $html .= $this->frame_content($frame, $load_data, $frame_filter_data);
            }


            return $html;
        } else {
            $html .= '<div class="brain_error brain_error_query">';
            $html .= '<div class="container error-block table-responsive mui-panel">';
            $html .= '<div class="row">';
            $html .= '<div class="col-lg-12 col-sm-12 col-xs-12">';
            $html .= '<div class="right-content col-lg-12">';
            $html .= '<div class="col-lg-3">';
            $html .= '<div class="img-block-icon1"><img src="' . base_url() . 'assets/core/images/green.png" /> </div>';
            $html .= '</div>';
            $html .= '<div class="col-lg-9 ">';
            $html .= '<p class="error_content"><b>Error message : </b>' . $load_data->ErrorMessage . '</p>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            return $html;
        }
    }

    /**
     * ONLY LOAD THE CONTENT OF THE PAGE USING AJAX CALL
     * Load the Collection Data for particulor Frame wise
     * After Add/Edit/Delete & Sorting/Searching/Paging Functionality call this Function
     *
     * @return none
     */
    public function load_content()
    {
        $frames = array();
        $staticDir = APPPATH . "views/static/";
        $cacheDir = APPPATH . "views/cache/";

        $pageId = $this->input->post('page_id');
        $frameId = $this->input->post('frame_id');
        $other_func_data['sorting'] = $this->input->post('order');
        $other_func_data['filtering'] = $this->input->post('filter');
        $other_func_data['page_number'] = $this->input->post('page_num');
        $other_func_data['number_of_records'] = $this->input->post('num_recs');
        $other_func_data['download_data'] = $this->input->post('download_data');

        $prop_frame_filter_parameter = $this->input->post('prop_frame_filter_parameter');

        if (empty($prop_frame_filter_parameter)) {
            $prop_frame_filter_parameter = '';
        }

        $other_func_data['pf'] = $this->input->post('pf');


        $page_result = $this->framemodel->getPageFrameProperties($pageId, $frameId);
        // print_r($page_result); exit;
	if($page_result){
		
        foreach ($page_result[0]->Results as $property) {
            // get the page frame id - Key for futher lookups
            $PFID = $property->PageFrameId;

            if (!array_key_exists($PFID, $frames)) {

                $frames = $this->getFramePermission($PFID, $frames, $property, $cacheDir);
            }
            // add the property to the properties array
            $frames[$PFID]['Properties'][] = $property;
        }
	}

        /* foreach ($frames as $frame) {
            $frame = $frame;
        }*/

        // this function is optimize of above each loop
        //this function is used to remove the first element from an array
        $frame = array_shift($frames);

        $load_data = $this->load($frame, $other_func_data);

        if ($load_data->HasError == 0) {
            if (isset($load_data->Data)) {
                $load_data = $load_data->Data;
            }

            $frame_filter_data = $load_data[0]->filter;
            $load_data = $load_data[0]->Results;

            if ($other_func_data['download_data'] == 1) {
                $csv_terminated = "\n";
                $csv_separator = ",";
                $csv_enclosed = '"';
                $csv_escaped = "\\";
                $out = "";

                foreach ($frame['Properties'] as $prop) {
                    if (($prop->TableColWidth) == 0) {
                        continue;
                    } else if ($this->detectDevice == 'mobile' && $prop->TableColWidth == 2) {
                        // web only
                        continue;
                    } else if ($this->detectDevice == 'web' && $prop->TableColWidth == 3) {
                        // mobile only
                        continue;
                    }

                    $out .= $csv_enclosed . $prop->PropTitle . $csv_enclosed . $csv_separator;
                }

                $out .= $csv_terminated;

                if (!empty($load_data)) {
                    $count = 1;

                    foreach ($load_data as $frow) {
                        foreach ($frame['Properties'] as $prop) {
                            $prname = $prop->PropName;

                            if (($prop->TableColWidth) == 0) {
                                continue;
                            } else if ($this->detectDevice == 'mobile' && $prop->TableColWidth == 2) {
                                // web only
                                continue;
                            } else if ($this->detectDevice == 'web' && $prop->TableColWidth == 3) {
                                // mobile only
                                continue;
                            }

                            // FIXME: 2 Ifs into 1
                            if ($prop->PropEditType == 'YN') {
                                if ($frow->$prname == '1') {
                                    $frow->$prname = 'Yes';
                                } else {
                                    $frow->$prname = 'No';
                                }
                            }

                            if ($prop->PropEditType == 'DATE') {
                                if (strlen($frow->$prname) == 0) {
                                    $frow->$prname = '-';
                                } else {
                                    $frow->$prname = date("Y-m-d h:i a", strtotime(str_replace("T", " ", $frow->$prname)));
                                }
                            }

                            $out .= $csv_enclosed . str_replace('"', '\'', $frow->$prname) . $csv_enclosed . $csv_separator;
                        }

                        $out .= $csv_terminated;
                    }
                }

                echo $out;
            } else {
                $html = $this->frame_content($frame, $load_data, $frame_filter_data);
                echo $html;
            }
        } else {
            $html = '<div class="brain_error brain_error_query">';
            $html .= '<div class="container error-block table-responsive mui-panel">';
            $html .= '<div class="row">';
            $html .= '<div class="col-lg-12 col-sm-12 col-xs-12">';
            $html .= '<div class="right-content col-lg-12">';
            $html .= '<div class="col-lg-3">';
            $html .= '<div class="img-block-icon1"><img src="' . base_url() . 'assets/core/images/green.png" /> </div>';
            $html .= '</div>';
            $html .= '<div class="col-lg-9 ">';
            $html .= '<p class="error_content"><b>Error message : </b>' . $load_data->ErrorMessage . '</p>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            echo $html;
        }
    }

    /**
     * FUNCTION FOR MANAGING FILTER STRINGS USING AND/OR CONDITION
     *
     * @param string $filterstring String to use
     *
     * @return string List of filter strings
     */
    public function getFilterStrings($filterstring)
    {

        $filterstring = rtrim($filterstring, " ++ ");
        $filterstring = explode('++', $filterstring);
        $arr = array();
        $filterarray = array();

        for ($i = 0; $i < count($filterstring); $i++) {


            if (strpos($filterstring[$i], 'like') != false) {
                $filterarray =  explode('like', $filterstring[$i]);
                $filterarray[2] = $filterstring[$i];
            } elseif (strpos($filterstring[$i], '>=') != false) {
                $filterarray = explode('>=', $filterstring[$i]);
                $filterarray[2] = $filterstring[$i];
            } elseif (strpos($filterstring[$i], '<=') != false) {
                $filterarray = explode('<=', $filterstring[$i]);
                $filterarray[2] = $filterstring[$i];
            } elseif (strpos($filterstring[$i], '<>') != false) {
                $filterarray = explode('<>', $filterstring[$i]);
                $filterarray[2] = $filterstring[$i];
            } elseif (strpos($filterstring[$i], '=') != false) {
                $filterarray = explode('=', $filterstring[$i]);
                $filterarray[2] = $filterstring[$i];
            } elseif (strpos($filterstring[$i], '>') != false) {
                $filterarray = explode('>', $filterstring[$i]);
                $filterarray[2] = $filterstring[$i];
            } elseif (strpos($filterstring[$i], '<') != false) {
                $filterarray = explode('<', $filterstring[$i]);
                $filterarray[2] = $filterstring[$i];
            }

            if (isset($arr[$filterarray[0]])) {
                array_push($arr[$filterarray[0]], $filterarray);
            } else {
                $arr[$filterarray[0]] = array($filterarray);
            }
        }



        $final_filter_str = '';

        foreach ($arr as $arrs) {
            $implode_array = array();
            for ($i = 0; $i < count($arrs); $i++) {
                array_push($implode_array, $arrs[$i][2]);
            }

            $final_filter_str .= "(" . implode(" OR ", $implode_array) . ")";
            $final_filter_str .= " AND ";
        }

        $final_filter_str = rtrim($final_filter_str, " AND ");
        return $final_filter_str;
    }

    // load filter card
    public function loadFilterCard()
    {
        $data['filterField'] = $this->input->post('filterField');
        $data['filterOperator'] = $this->input->post('filterOperator');
        $data['filterText'] = $this->input->post('filterText');
        $data['filterString'] = $this->input->post('filterString');
        $data['pageFrameId'] = $this->input->post('pageFrameId');
        $data['history_api_back_val'] = $this->input->post('history_api_back_val');


        $this->load->view('FilterCardView', $data);
    }


    // fetch the filter data and return filter string
    public function getPermanentFilterData($frame, $check, &$sorting)
    {

        $filter_data_string = array();
        $filterString = '';

        $filter_permanent_string = '';
        $filter_default_string = '';


        $result_filter = $this->framemodel->fetchPermanentFilter($frame['PFID']);

        foreach ($result_filter[0]->Results as $filter_data) {

            // if formula type set to variable
            if ($filter_data->formula_type == 'V') {
                // if formula type set to Query string
                if (substr($filter_data->FilterValue, 0, 1) == 'Q') {
                    $FilterValue = $this->swapout_variables_query_string_filter($filter_data->FilterValue);
                    // if formula type set to Session
                } elseif (substr($filter_data->FilterValue, 0, 1) == 'S') {
                    $session_variable = substr($filter_data->FilterValue, 2, -1);
                    $FilterValue = $this->session->userdata($session_variable);
                }
                if ($FilterValue != '') {
                    $filter_data_string[]  = $filter_data->PropertyId . '_' . $filter_data->FilterOperator . '_' . $FilterValue . '_' . $filter_data->FilterType;
                }
            } else {
                // FORMULA_TYPE = "C-CONSTANT" (DEFAULT)
                $filter_data_string[]  = $filter_data->PropertyId . '_' . $filter_data->FilterOperator . '_' . $filter_data->FilterValue . '_' . $filter_data->FilterType;
            }
        }

        if ($check == 1) {
            return $filter_data_string;
        } else {


            // PARTICULOR PROPERTY WISE SET FILTER VARIABLE
            foreach ($frame['Properties'] as $property) {
                if (!empty($filter_data_string)) {
                    foreach ($filter_data_string as $filter_data) {

                        $filter_string_explode = explode("_", $filter_data);

                        if ($filter_string_explode[0] == $property->PropId) {
                            $filter_string_explode[0] = $property->Alias . '.[' . $property->PropColumn . ']';

                            // if filter string contain "like" then we will include "%" initial and in the end.
                            if ($filter_string_explode[1] == 'like') {
                                $filter_string_explode[2] = "%" . $filter_string_explode[2] . "%";
                            }

                            // if filter string contain "string" then we will include "''" initial and in the end.
                            if (gettype($filter_string_explode[2]) == 'string') {
                                $filter_string_explode[2] = "'" . $filter_string_explode[2] . "'";
                            }

                            // if filter string contain "NULL" then we will include "blank string".
                            if ($filter_string_explode[2] == "'NULL'" || $filter_string_explode[2] == "'null'") {
                                $filter_string_explode[2] = "''";

                                if ($filter_string_explode[1] == '<>') {
                                    $filter_array[] = $filter_string_explode[0];
                                    $filter_array[] = $filter_string_explode[1];
                                    $filter_array[] = $filter_string_explode[2] . " OR ";
                                    $filter_array[] = "NULLIF(" . $filter_string_explode[0] . ", '') IS NOT NULL ++";
                                    // if filter type is Permanent
                                    if ($filter_string_explode[3] == 1) {
                                        $filter_permanent_string = implode("", $filter_array);
                                    }
                                    // if filter type is default
                                    if ($filter_string_explode[3] == 2) {
                                        $filter_default_string = implode("", $filter_array);
                                    }
                                } else {
                                    $filter_array[] = $filter_string_explode[0];
                                    $filter_array[] = $filter_string_explode[1];
                                    $filter_array[] = $filter_string_explode[2] . " OR ";
                                    $filter_array[] = $filter_string_explode[0];
                                    $filter_array[] = "IS NULL ++";

                                    // if filter type is Permanent
                                    if ($filter_string_explode[3] == 1) {
                                        $filter_permanent_string = implode("", $filter_array);
                                    }
                                    // if filter type is default
                                    if ($filter_string_explode[3] == 2) {
                                        $filter_default_string = implode("", $filter_array);
                                    }
                                }
                            } else {

                                $filter_array[] = $filter_string_explode[0];
                                $filter_array[] = $filter_string_explode[1];
                                $filter_array[] = $filter_string_explode[2] . "++";

                                // if filter type is Permanent
                                if ($filter_string_explode[3] == 1) {
                                    $filter_permanent_string = implode("", $filter_array);
                                }

                                // if filter type is default
                                if ($filter_string_explode[3] == 2) {
                                    $filter_default_string = implode("", $filter_array);
                                }
                            }
                        }
                    }
                }

                if (($property->DefaultSortField == $property->PropId) && $sorting == '') {
                    $sorting = '[' . $property->PropName . ']  ' . $property->DefaultSortType;
                }
            }



            $final_filter_permanent_string = '';
            $final_filter_default_string = '';

            if ($filter_permanent_string != '') {
                //fetch filter string of permanent string.
                $final_filter_permanent_string = $this->getFilterStrings($filter_permanent_string);
            }
            if ($filter_default_string != '') {
                //fetch filter string of default string.
                $final_filter_default_string = $this->getFilterStrings($filter_default_string);
            }

            if ($final_filter_permanent_string != '' && $final_filter_default_string != '') {
                $filterString = $final_filter_permanent_string . ' AND ' . $final_filter_default_string;
            } else {
                if ($final_filter_permanent_string != '') {
                    $filterString = $final_filter_permanent_string;
                } elseif ($final_filter_default_string != '') {
                    $filterString = $final_filter_default_string;
                }
            }

            return $filterString;
        }
    }

    //Get page history from the database.
    function fetchHistory($frame)
    {

        $actual_curr_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $actual_curr_url = str_replace("%20", " ", $actual_curr_url);


        $page_result_history_api = $this->framemodel->fetchHistory($frame['collection'], $frame['PFID'], $actual_curr_url);


        return $page_result_history_api;
    }

    // Getting data for Group Permission for Particular Frame
    public function getFramePermission($PFID, $frames, $property, $cacheDir)
    {

        $prop_frame_filter_parameter = $this->input->post('prop_frame_filter_parameter');

        if (empty($prop_frame_filter_parameter)) {
            $prop_frame_filter_parameter = '';
        }

        // if the frame doesn't already exist add it to the frames array
        $fullPath_header = $cacheDir . $property->FrameName . '_' . $PFID . '_header.php';
        $viewPath_header = 'cache/' . $property->FrameName . '_' . $PFID . '_header';
        $fullPath_content = $cacheDir . $property->FrameName . '_' . $PFID . '_content.php';
        $viewPath_content = 'cache/' . $property->FrameName . '_' . $PFID . '_content';

        if (file_exists($fullPath_header)) {
            $frameCacheExists_header = true;
        } else {
            $frameCacheExists_header = false;
        }

        if (file_exists($fullPath_content)) {
            $frameCacheExists_content = true;
        } else {
            $frameCacheExists_content = false;
        }


        $page_result_group_permission_data = $this->framemodel->fetchFramePermission($property->FrameID);



        if (!empty($page_result_group_permission_data[0]->Results)) {
            // add the frame details to frames array
            $frames[$PFID] = array(
                'PFID' => $PFID,
                //'FramePageView' => $FramePageView,
                'name' => $property->FrameName,
                'order' => $property->FrameOrder,
                'type' => $property->FrameType,
                'filter' => $prop_frame_filter_parameter,
                'editURL' => $property->EditURL,
                'editParms' => json_decode($property->EditParms),
                'FrameTitle' => $property->FrameTitle,
                'collection' => $property->CollId,
                'fullPath_header' => $fullPath_header,
                'viewPath_header' => $viewPath_header,
                'fullPath_content' => $fullPath_content,
                'viewPath_content' => $viewPath_content,
                'cacheExists_header' => $frameCacheExists_header,
                'cacheExists_content' => $frameCacheExists_content,
                'allow_view' => $page_result_group_permission_data[0]->Results[0]->allow_view,
                'allow_add' => $page_result_group_permission_data[0]->Results[0]->allow_add,
                'allow_edit' => $page_result_group_permission_data[0]->Results[0]->allow_edit,
                'allow_delete' => $page_result_group_permission_data[0]->Results[0]->allow_delete,
                'allow_notes' => $page_result_group_permission_data[0]->Results[0]->allow_notes,
                'allow_links' => $page_result_group_permission_data[0]->Results[0]->allow_links,
                'allow_download' => $page_result_group_permission_data[0]->Results[0]->allow_download,
                'allow_multiple_edit' => $page_result_group_permission_data[0]->Results[0]->allow_multiple_edit,
                'allow_multiple_delete' => $page_result_group_permission_data[0]->Results[0]->allow_multiple_delete,
                'tot_page_count' => $property->total_count
            );
        } else {
            // add the frame details to frames array
            $frames[$PFID] = array(
                'PFID' => $PFID,
                //'FramePageView' => $FramePageView,
                'name' => $property->FrameName,
                'order' => $property->FrameOrder,
                'type' => $property->FrameType,
                'filter' => $prop_frame_filter_parameter,
                'editURL' => $property->EditURL,
                'editParms' => json_decode($property->EditParms),
                'FrameTitle' => $property->FrameTitle,
                'collection' => $property->CollId,
                'fullPath_header' => $fullPath_header,
                'viewPath_header' => $viewPath_header,
                'fullPath_content' => $fullPath_content,
                'viewPath_content' => $viewPath_content,
                'cacheExists_header' => $frameCacheExists_header,
                'cacheExists_content' => $frameCacheExists_content,
                'tot_page_count' => $property->total_count
            );
        }
        return $frames;
    }

    // load filter html
    public function propertyFilter($frame, $filter_data_array)
    {

        foreach ($frame['Properties'] as $property) {

            foreach ($filter_data_array as $filter_data) {

                $filter_string_explode = explode("_", $filter_data);


                if ($filter_string_explode[0] == $property->PropId) {

                    $filter_string_display = explode("_", $filter_data);

                    if ($filter_string_explode[3] == 1) {
                        $filterType = "parmanent_filter";
                        $num = 1;
                    }

                    if ($filter_string_explode[3] == 2) {
                        $filterType = "default_filter";
                        $num = 2;
                    }

                    if ($filter_string_explode[3] == 3) {
                        $filterType = "default_filter";
                        $num = 3;
                    }


                    $pf_val_parm = '';
                    $filter_array = array();
                    $filter_string_explode[0] = $property->Alias . '.[' . $property->PropColumn . ']';
                    // if filter string contain "like" then we will include "%" initial and in the end.
                    if ($filter_string_explode[1] == 'like') {
                        $filter_string_explode[2] = "%" . $filter_string_explode[2] . "%";
                    }
                    // if filter string contain "string" then we will include "''" initial and in the end.
                    if (gettype($filter_string_explode[2]) == 'string') {
                        $filter_string_explode[2] = "'" . $filter_string_explode[2] . "'";
                    }

                    // if filter string contain "NULL" then we will include "blank string".
                    if ($filter_string_explode[2] == "'NULL'" || $filter_string_explode[2] == "'null'") {
                        $filter_string_explode[2] = "''";
                        if ($filter_string_explode[1] == '<>') {
                            $filter_array[] = $filter_string_explode[0];
                            $filter_array[] = $filter_string_explode[1];
                            $filter_array[] = $filter_string_explode[2] . " OR ";
                            $filter_array[] = "NULLIF(" . $filter_string_explode[0] . ", '') IS NOT NULL ";
                            $pf_val_parm = implode("", $filter_array);
                        } else {
                            $filter_array[] = $filter_string_explode[0];
                            $filter_array[] = $filter_string_explode[1];
                            $filter_array[] = $filter_string_explode[2] . " OR ";
                            $filter_array[] = $filter_string_explode[0];
                            $filter_array[] = "IS NULL";
                            $pf_val_parm = implode("", $filter_array);
                        }
                    } else {
                        $filter_array[] = $filter_string_explode[0];
                        $filter_array[] = $filter_string_explode[1];
                        $filter_array[] = $filter_string_explode[2];
                        $pf_val_parm = implode("", $filter_array);
                    }

                    $filter_string_display[0] = $property->PropTitle;

                    $filter_list .= '<div class="disp_filter_' . $frame['PFID'] . ' disp_filter" data-filter="' . $filterType . '" data-filter_type="without_history" data-filter_val="' . $property->PropId . '_' . $filter_string_explode[1] . '" id="filter_id_' . $property->PropId . '">';


                    if ($filter_string_explode[3] == 2 || $filter_string_explode[3] == 3) {

                        $filter_list .= '<div class="cancel_filter cancel_filter_' . $frame['PFID'] . '" id="cancel_filter_' . $property->PropId . '"><svg viewBox="0 0 24 24" style="-moz-user-select: none; display: inline-block; color: rgb(255, 255, 255); fill: #7f8fa4; cursor: pointer; height: 15px; width: 15px; transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg></div>';
                    }

                    $pf_val_parm_for_history = $filter_string_explode[0] . '**' . $filter_string_display[1] . '**' . $filter_string_display[2] . '**' . $num;
                    $filter_list .= '<input type="hidden" class="filter_vals_' . $frame['PFID'] . '" id="fid_' . $property->PropId . '" data-hist_filter_val_' . $frame['PFID'] . ' = "' . $pf_val_parm_for_history . '" value="' . $pf_val_parm . '">';


                    $filter_list .= '<div class="s_value">' . $filter_string_display[2] . '</div>';
                    $filter_list .= '<div class="s_name" id="' . $filter_string_display[0] . '">' . $filter_string_display[0] . '</div>';
                    $filter_list .= '<div class="s_operator">' . $filter_string_display[1] . '</div>';
                    $filter_list .= '</div>';
                }
            }
        }

        return $filter_list;
    }

    // Load the all header like search,add,edit,delete 
    public function loadHeader($frame, $filter_list)
    {

        $page_name = $this->uri->segment(2, 0);
        $page_result = $this->pagemodel->getPageHeaderInfo($page_name);
        $enableTabbedFrames = $page_result[0]->Results[0]->enable_tabbed_frames;

        if ($enableTabbedFrames == '') {
            $html_filter .= '<h3 class="bsit_fram_titel">' . $frame['FrameTitle'] . '</h3>';
        }

        // if ((isset($frame['allow_search']) && $frame['allow_search'] == '1') || ($frame['Properties'][0]->AllowSearch == '1')) {
        $html_filter_start .= '<div class="bsit_content bsit_filter_div" id="bsit_content_' . $frame['PFID'] . '" style="padding-left:0;">';
        $html_filter_start .= '<div id="page_frame_' . $frame['PFID'] . '_header" class="call_header_div">';
        $html_filter_start .= '<div id="filter_main_div_' . $frame['PFID'] . '" class="filter_main_div mdl-cell mdl-cell--12-col bsit_form bsit_filter_design">';
        $html_filter .= '<div class="mdl-cell mdl-cell--12-col bsit_top_icon_bar" style="margin: 0px 0" >';
        // }


        $html_filter .= '<input type="hidden" name="key_id_frame_sidebar" id="enable_pane_view_' . $frame['PFID'] . '"  frame_val="' . $frame['PFID'] . '" value="' . $frame['Properties'][0]->enable_pane_view . '"/>';
        // if ((isset($frame['allow_add']) && $frame['allow_add'] == '1') || ($frame['Properties'][0]->AllowAdd == '1' && $frame['Properties'][0]->hide_add_button == '0')){
        //     $html_filter .= '<a class="bsit-btn-add-side openNav" id="add-btn-'.$frame['PFID'].'" style="font-size:16px;cursor:pointer"><span class="icon-filters"></span></a>';
        // }

        if ((isset($frame['allow_add']) && $frame['allow_add'] == '1') || ($frame['Properties'][0]->AllowAdd == '1' && $frame['Properties'][0]->hide_add_button == '0')) {
            $html_filter .= '<button class="bsit_ahm_btn mui-btn mui-btn--primary bsit-btn-add" id="add-btn-' . $frame['PFID'] . '" data-toggle="tooltip" data-placement="bottom" data-original-title="Add"></button>';
        }

        if ((isset($frame['allow_multiple_edit']) && $frame['allow_multiple_edit'] == '1') || ($frame['Properties'][0]->allow_multiple_edit == '1' && $frame['Properties'][0]->hide_multiple_edit == '0')) {
            $html_filter .= '<button class="bsit_ahm_btn mui-btn mui-btn--primary bsit-multi-edit-btn" edit="Edit" id="multi-edit-btn-' . $frame['PFID'] . '" data-toggle="tooltip" data-placement="bottom" data-original-title="Multi Edit">e</button>';
        }

        // if (((isset($frame['allow_download'])) && ($frame['allow_download'] == 1)) ||
        //              ($frame['Properties'][0]->hide_allow_download == '0')){
        //     $html_filter .= '<button  class="download_button_div bsit_ahm_btn mui-btn mui-btn--primary" id="download_data_'.$frame['PFID'].'">&#xe903;</button>';
        //  }
        // if (((isset($frame['allow_download'])) && ($frame['allow_download'] == '1')) ||
        //              ($frame['Properties'][0]->hide_allow_download == '0')){
        //     $html_filter .= '<button  class="download_button_div bsit_ahm_btn mui-btn mui-btn--primary" id="download_data_'.$frame['PFID'].'">&#xe903;</button>';
        //  }

        if (isset($frame['allow_download'])) {
            if ($frame['allow_download'] == 1) {
                $html_filter .= '<button  class="download_button_div bsit_ahm_btn mui-btn mui-btn--primary" id="download_data_' . $frame['PFID'] . '" data-toggle="tooltip" data-placement="bottom" data-original-title="Download">&#xe903;</button>';
            }
        } else {
            if ($frame['Properties'][0]->hide_allow_download == '0') {
                $html_filter .= '<button  class="download_button_div bsit_ahm_btn mui-btn mui-btn--primary" id="download_data_' . $frame['PFID'] . '" data-toggle="tooltip" data-placement="bottom" data-original-title="Download">&#xe903;</button>';
            }
        }

        if ((isset($frame['allow_search']) && $frame['allow_search'] == '1') || ($frame['Properties'][0]->AllowSearch == '1')) {
            $html_filter .= '<button  class="page_refresh bsit_ahm_btn mui-btn mui-btn--primary" id="" data-toggle="tooltip" data-placement="bottom" data-original-title="Page Refresh">&#xe905;</button>';
        }

        if ((isset($frame['allow_multiple_delete']) && $frame['allow_multiple_delete'] == '1') || ($frame['Properties'][0]->allow_multiple_delete == '1' && $frame['Properties'][0]->hide_multiple_delete == '0')) {
            $html_filter .= '<button class="bsit_ahm_btn mui-btn mui-btn--primary bsit-multi-delete-btn" edit="Delete" id="multi-delete-btn-' . $frame['PFID'] . '" data-toggle="tooltip" data-placement="bottom" data-original-title="Multiple Delete"></button>';
        }


        if ((isset($frame['allow_search']) && $frame['allow_search'] == '1') || ($frame['Properties'][0]->AllowSearch == '1')) {
            $html_filter .= '<button type="button" class="collapsible bsit_filters" data="' . $frame['PFID'] . '" id="myelement_' . $frame['PFID'] . '" data-toggle="tooltip" data-placement="bottom" data-original-title="Search"></button>';
        }

        //$html_filter .= '<button type="button" class="collapsible bsit_filters myelement_jobs_kanbanView" id="myelement_jobs_kanbanView'.$frame['PFID'].'" pfid = "'.$frame['PFID'].'" style="margin-right: 5px; display: none;" data-toggle="tooltip" data-placement="bottom" data-original-title="Kanban View"><img src="'.base_url().'assets/core/images/logo_kanban.png" style="width:15px;"></button>';

        $html_filter .= '</div>';
        $html = '';
        $html .= $html_filter;
        if ((isset($frame['allow_search']) && $frame['allow_search'] == '1') || ($frame['Properties'][0]->AllowSearch == '1')) {
            $filter_name .= '<select class="bsit-filter-name bsit_custom_dropdown" name="filter_name" id="filter_name_' . $frame['PFID'] . '">';
            $filter_name .= '<option value="" id=""></option>';
            foreach ($frame['Properties'] as $prop) {
                if (($prop->TableColWidth) == 0) {
                    continue;
                } else if ($this->detectDevice == 'mobile' && $prop->TableColWidth == 2) {
                    // web only
                    continue;
                } else if ($this->detectDevice == 'web' && $prop->TableColWidth == 3) {
                    // mobile only
                    continue;
                }
                $filter_name .= '<option value="' . $prop->Alias . '.[' . $prop->PropColumn . ']" id="' . $prop->PropId . '">' . $prop->PropTitle . '</option>';
            }
            $filter_name .= '</select>';

            $filter_name .= '<select class="bsit_custom_dropdown" name="filter_oprator" id="filter_oprator_' . $frame['PFID'] . '">';
            $filter_name .= '<option value="like">Like</option><option value="=">Exact "="</option><option value="<>">Not Equals "<>"</option><option value=">">Greater then ">"</option><option value="<">Less then "<"</option><option value=">=">Greater than Equals ">="</option><option value="<=">Less than Equals "<="</option>';
            $filter_name .= '</select>';
            $html_filter1 .= $filter_name;
            $html_filter1 .= '<div class="input_with_search"><span class="filter_submit icon-search bsit-filter-btn" id="filter-btn-' . $frame['PFID'] . '"></span><input type="text" class="bsit-filter-text" name="filter_text" id="filter_text_' . $frame['PFID'] . '" placeholder="Search"></div>';
            $html_filter_end = '</div>';
            $html_filter_end .= '<div class="bsit-filter-area" id="filter_list_div_' . $frame['PFID'] . '" data_table="' . $frame['PFID'] . '">';
        }
        $html_filter_end .= $filter_list;
        $html_filter_end .= '</div>';
        $html_filter_end .= '</div>';
        $html_filter_end .= '</div>';
        $html .= $html_filter_start;
        if ($frame['Properties'][0]->hide_filter == '0') {
            $html .= $html_filter1;
        }
        $html .= '<input type="hidden" class="history_page_number_' . $frame['PFID'] . '" value="' . $history_page_num . '">';
        $html .= $html_filter_end;

        // elseif ($frame['Properties'][0]->AllowSearch == '1') {

        // $html_filter .= '</div>';
        // $html = '';
        // $html .= $html_filter;
        // $filter_name .= '<select class="bsit-filter-name" name="filter_name" id="filter_name_'.$frame['PFID'].'">';
        // $filter_name .= '<option value="" id=""></option>';

        // foreach ($frame['Properties'] as $prop) {
        //     $filter_name .= '<option value="'.$prop->Alias.'.['.$prop->PropColumn.']" id="'.$prop->PropId.'">'.$prop->PropTitle.'</option>';
        // }

        // $filter_name .= '</select>';
        // $filter_name .= '<select name="filter_oprator" id="filter_oprator_'.$frame['PFID'].'">';
        // $filter_name .= '<option value="like">Like</option><option value="=">Exact "="</option><option value="<>">Not Equals "<>"</option><option value=">">Greater then ">"</option><option value="<">Less then "<"</option><option value=">=">Greater than Equals ">="</option><option value="<=">Less than Equals "<="</option>';
        // $filter_name .= '</select>';

        // $html_filter1 .= $filter_name;
        // $html_filter1 .= '<div class="input_with_search"><input type="text" class="bsit-filter-text" name="filter_text" id="filter_text_'.$frame['PFID'].'" placeholder="Search"><span class="filter_submit bsit-filter-btn" id="filter-btn-'.$frame['PFID'].'"><img src="'.base_url().'assets/core/images/search.png" class="image" ></span></div>';

        // $html_filter_end = '</div>';
        // $html_filter_end .= '<div class="bsit-filter-area" id="filter_list_div_'.$frame['PFID'].'">';
        // $html_filter_end .= $filter_list;
        // $html_filter_end .= '</div>';
        // $html_filter_end .= '</div>';

        // $html .= $html_filter_start;

        // if ($frame['Properties'][0]->hide_filter == '0') {
        //     $html .= $html_filter1;
        // }

        // $html .= '<input type="hidden" class="history_page_number_'.$frame['PFID'].'" value="'.$history_page_num.'">';
        // $html .= $html_filter_end;


        // }
        return $html;
    }

    // [start] GET QUERY STRINGS VARIABLE's VALUE FOR PERMANANT FILTER
    public function swapout_variables_query_string_filter($origstr)
    {
        // find all the occurances of V(XXX)
        preg_match_all('#Q\((.*?)\)#', $origstr, $matches);

        $updatestr = $origstr;

        foreach ($matches[1] as $match) {
            $updatestr = $this->swapout_multiple_query_string_filter_fields($updatestr, $match);
        }

        return $updatestr;
    }

    // GET FILTER VALUES AS MULTIPLE FILTER
    public function swapout_multiple_query_string_filter_fields($origstr, $match)
    {
        $singleval = '';

        if ($_GET[$match] != '') {
            $singleval = str_replace("Q(" . $match . ")", $_GET[$match], $origstr);
        }
        return $singleval;
    }

    public function getQuickFilterValues()
    {

        $filter = $_POST['framepropertyid'];


        $getAllowQuickFilter = $this->pagemodel->getQuickFilterValues($filter);
        // debug($getAllowQuickFilter); exit;
        echo json_encode($getAllowQuickFilter);
    }
}
