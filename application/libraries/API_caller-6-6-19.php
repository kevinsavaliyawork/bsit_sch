<?php

defined('BASEPATH') or exit('No direct script access allowed');

abstract class API_caller extends brain_controller
{
    private $user_id;

    public function __construct()
    {
        parent::__construct(0);
        $this->load->model('module');
        $this->load->model('Bsit_io', 'API');
        $this->load->library('session');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    // Adding a new record
    public function create_exec($data)
    {
        $CURLDATA = array('collections' => $data);

        $json_page_result = $this->API->CallAPI("POST", BASEURL_POST, $CURLDATA);
        $page_result = json_decode($json_page_result);

        if (isset($page_result->ErrorMessage)) {
            $load_error = true;
            $page_result->message = CREATE_FAIL;
            $error_message = (isset($page_result->message)) ? $page_result->message : $page_result->ErrorMessage;
            $error_msg[] = $error_message;
            $page_result->ErrorMessage = $page_result->message." ".$page_result->ErrorMessage;
            echo(json_encode($page_result));
            return;
        }

        if (isset($page_result->status) && $page_result->status <> "sucess") {
            $load_error = true;
            $error_message = (isset($page_result->error)) ? $page_result->error : $page_result->message;
            $error_msg[] = "CURL Error:".$error_message;
            echo(json_encode($error_msg));
            return;
        }

        echo(json_encode($page_result->Data));
        return ;
    }

    // Updating and existing record
    public function update_exec($data)
    {
        // set the header so jquery know we are sending back json
        header('Content-type: application/json');

        // create an array to hold the correct structure for the data to send
        $CURLDATA = array('collections' => $data);

        $json_page_result = $this->API->CallAPI("POST", BASEURL_POST, $CURLDATA);
        $page_result = json_decode($json_page_result);

        if (isset($page_result->ErrorMessage)) {
            $load_error = true;
            $page_result->message = UPDATE_FAIL;
            $error_message = (isset($page_result->message)) ? $page_result->message : $page_result->ErrorMessage;
            $error_msg[] = $error_message;

            $page_result->ErrorMessage = $page_result->message." ".$page_result->ErrorMessage;
            echo(json_encode($page_result));
            return;
        }

        if (isset($page_result->status) && $page_result->status <> "sucess") {
            $load_error = true;
            $error_message = (isset($page_result->error)) ? $page_result->error : $page_result->message;
            $error_msg[] = "CURL Error:".$error_message;
            echo(json_encode($error_msg));
            return;
        }

        echo(json_encode($page_result->Data));
        return;
    }

    // Deleteing a single record
    public function delete_exec($data)
    {
        // set the header so jquery know we are sending back json
        header('Content-type: application/json');

        // create an array to hold the correct structure for the data to send
        $CURLDATA = array('collections' => json_encode($data));

        $json_page_result = $this->API->CallAPI("DELETE", BASEURL_DELETE, $CURLDATA);
        $page_result = json_decode($json_page_result);

        // code added for check error message of delete

        if (isset($page_result->ErrorMessage)) {
            $load_error = true;
            $page_result->message = DELETE_FAIL;
            $error_message = (isset($page_result->message)) ? $page_result->message : $page_result->ErrorMessage;
            $error_msg[] = $error_message;
            $page_result->ErrorMessage = $page_result->message." ".$page_result->ErrorMessage;
            echo(json_encode($page_result));
            return;
        }

        // FIXME: spelling error
        if (isset($page_result->status) && $page_result->status <> "sucess") {
            $load_error = true;
            $error_message = (isset($page_result->error)) ? $page_result->error : $page_result->message;
            $error_msg[] = "CURL Error:".$error_message;
            echo(json_encode($error_msg));
            return;
        }

        echo(json_encode($page_result->Data));
        return ;
    }

    // loading a record
    public function loadsingle_exec($data)
    {
        // set the header so jquery knows we are sending back json
        header('Content-type: application/json');

        // get the variables from the user
        $postdata = json_decode($data);
        $coll_id = $postdata[0]->collID;
        $record_id = $postdata[0]->recID;

        // setup an array in the correct format to load the data
        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_id' => $coll_id,
                'rec_id' => $record_id
            )))
        ));

        $json_page_result = $this->API->CallAPI("GET", BASEURL, $CURLDATA);
        $page_result = json_decode($json_page_result);

        // if errors are returned, we have the change to reword before displaying to the user
        if (isset($page_result->status) && $page_result->status <> "sucess") {
            $load_error = true;
            $error_message = (isset($page_result->error)) ? $page_result->error : $page_result->message;
            $error_msg[] = "ID=".$coll_id." CURL Error:".$error_message;
            echo(json_encode($error_msg));
            return;
        }

        echo(json_encode($page_result->Data));
        return;
    }

    // loading many records
    public function loadfiltered_exec($data)
    {
        // set the header so jquery knows we are sending back json
        header('Content-type: application/json');

        // get the variables from the user
        $postdata = json_decode($data);
        $coll_id = $postdata[0]->collID;
        $filter = $postdata[0]->filter;

        // setup an array in the correct format to load the data
        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_id' => $coll_id,
                'filter' => $filter
            )))
        ));

        $json_page_result = $this->API->CallAPI("GET", BASEURL, $CURLDATA);
        $page_result = json_decode($json_page_result);

        // FIXME: spelling error
        // if errors are returned, we have the change to reword before displaying to the user
        if (isset($page_result->status) && $page_result->status <> "sucess") {
            $load_error = true;
            $error_message = (isset($page_result->error)) ? $page_result->error : $page_result->message;
            $error_msg[] = "ID=".$coll_id." CURL Error:".$error_message."---".$data;
            echo(json_encode($error_msg));
            return;
        }

        echo(json_encode($page_result->Data));
        return;
    }

    // loading a record
    public function dropdownlist_exec($data)
    {
        // set the header so jquery knows we are sending back json
        header('Content-type: application/json');

        $filter = '';
        $filter_data = '';
        // get the variables from the user
        $postdata = json_decode($data);
        $coll_id = $postdata[0]->collID;
        $columns = $postdata[0]->cols;

        if (isset($postdata[0]->filter)) {
            $filter = $postdata[0]->filter;
            $filter_data = $filter;
        }

        // setup an array in the correct format to load the data
        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_id' => $coll_id,
                'cols' => $columns,
                'filter' => $filter_data,
                'numrec' => '9999'
            )))
        ));

        $json_page_result = $this->API->CallAPI("GET", BASEURL, $CURLDATA);
        $page_result = json_decode($json_page_result);

        // if errors are returned, we have the change to reword before displaying to the user
        if (isset($page_result->status) && $page_result->status <> "sucess") {
            $load_error = true;
            $error_message = (isset($page_result->error)) ? $page_result->error : $page_result->message;
            $error_msg[] = "Cols-".$columns." CURL Error:".$error_message;
            echo(json_encode($error_msg));
            return;
        }

        echo(json_encode($page_result->Data));
        return;
    }

    // loading Generate Invoice Data
    public function call_sp_exec()
    {
        if ($_POST['parameters'] == '') {
            $data_parameters='';
            $this->BASEURL= BASEURL.'?key='.$_POST['sp_name'][0]['key'].'&filters=""';
        } else {
            $data_parameters = '{"parameters":'.json_encode($_POST['parameters']).'}';
            $data_parameters = urlencode($data_parameters);
            $this->BASEURL= BASEURL.'?key='.$_POST['sp_name'][0]['key'].'&filters='.$data_parameters;
        }

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);

        $page_result = json_decode($json_page_result);

        echo(json_encode($page_result->Data));
        return;
    }

    public function Call_SP_exec_Login()
    {
        $array = $_POST['parameters'];
        $parameters = $_POST['parameters'];
        unset($parameters[1]);
        $parameters = array_values($parameters);
        $data_parameters = '{"parameters":'.json_encode($parameters).'}';

        $this->BASEURL= BASE_URL_API.'api/tablecollection/get?key='.$_POST['sp_name'][0]['key'].'&filters='.$data_parameters;

        $json_page_result = $this->API->CallAPI("GET", $this->BASEURL);

        $page_result = json_decode($json_page_result);

        echo(json_encode($page_result));
        return;
    }
}
