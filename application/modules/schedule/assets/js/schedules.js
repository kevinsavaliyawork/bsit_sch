var objjobdata=[];

//12-08-2021 
function setRestHours(){
	/*if(id == "timelineWeek"){
		var daysLeftPixcels = 180;
	}else if(id == "timelineDay"){
		var daysLeftPixcels = 1230;
	}else{
		var daysLeftPixcels = 100;
	}*/
	//check week, day and 3months
	if($(".fc-timelineCustomWeek-view")[0] != undefined){
	  // it exists
	  //alert("week found");
	  var daysLeftPixcels = 180;
	}else if($(".fc-timelineCustomDay-view")[0] != undefined){
	  //alert("day");
	  var daysLeftPixcels = 1230;
	}else if($(".fc-timelineNext3Months-view")[0] != undefined){
		//alert("3months");
		var daysLeftPixcels = 100;
	}else{
		alert("Error!");
		var daysLeftPixcels = 100;
	}

  	var date =$('#calendar').fullCalendar('getView').start;
  	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  	var t = new Date(date);
  	var dateForGetEvents = t.getDate()+' '+monthNames[t.getMonth()]+' '+t.getFullYear();
  	//alert(dateForGetEvents);
  	
	$.ajax({
		type: "POST",
		url: base_url + "schedule/schedule/getHoursToSchedule",
		 data: {
            date: dateForGetEvents,
        },
		dataType: "json",
		success: function(resultsData) {
			//clean previous data
			jQuery(".addplusspan").empty();
			var obj = resultsData;
			var resource_id="";
			var key_name="";
			var hours_planned="";
			var days_since="";
			var hours_to_schedule="";
			var total_hours = 8;

      		$.each (obj, function (bb) {
			    resource_id = obj[bb].owner_id;
			    hours_planned = obj[bb].hours_planned;
			    days_since = obj[bb].days_since;
			    hours_to_schedule = obj[bb].hours_to_schedule;

			    //process data for display
			    var day_count = daysLeftPixcels * days_since;
			    var hour_count = 20 * hours_planned;
			    if(hours_to_schedule != 0){
		        	if(hours_to_schedule > 0){
		        		var hour_display_span = "<span class='resthoursingreen'> +"+hours_to_schedule+"</span>";
		        	} else if(hours_to_schedule < 0){
		        		var hour_display_span = "<span class='resthoursinred'> "+hours_to_schedule+"</span>";
		        	}
		        	jQuery( ".bsit_sche_width.fc-time-area.fc-widget-content.fc-unselectable").find('*[data-resource-id="'+resource_id+'"] td').append("<div class='addplusspan' style='display:block; position:absolute; left:"+day_count+"px; bottom:4px; margin-left: 4px;' > "+hour_display_span+" </div> ");
		        }
			});
			
		},
		error: function(xx) {
			alert(xx.responseText);
		}
		});
}
//end
/* 16-08-2021*/
function setWeekendColor(){
	$(".fc-content.bsit_custom_width").find(" .fc-sat ").addClass(' holiday');
	$(".fc-content.bsit_custom_width").find(" .fc-sun ").addClass(' holiday');
	$(".fc-slats").find(" .fc-sat ").addClass(' holiday');
	$(".fc-slats").find(" .fc-sun ").addClass(' holiday');
}
function setHoursTimeStrip(){
	$( ".fc-resource-area").find("td.fc-widget-content" ).after( '<td class="hrtd"><div class="hrstrip"><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">2h</span></div><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">4h</span></div><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">6h</span></div><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">8h</span></div></div></td>');
}
//17-08-2021
function checkBrokenProfileImg(){  
		$('.fc-icon.bsit_user_img img').error(function(){
			var str = $(this).closest("div").find('span.fc-cell-text').html();
			var matches = str.match(/\b(\w)/g);
			var acronym = matches.join('');
			$(this).parent().html('<span class="bsit_sch_user_name">'+acronym+'</span>');
			//$(this).attr('src', 'http://localhost/bsit_sch_staging/assets/core/userimage/evelyn.carter.jpg');
		});
	}
/* end*/

// $(".download_button_div").hide();
$(document).ready(function() {

	 // hide the side drawer
  // $("#sidedrawer")
  //   .parent()
  //   .toggleClass("hide-sidedrawer");
$("#control_title").trigger("click");
     $('#prevbutton').click(function() {
         $('#calendar').fullCalendar('prev');
         $('#bsit_cal_all_day_date').html($(".fc-head").html());
		 var view = $('#calendar').fullCalendar('getView');
		 $('#title').html(view.title);
		 $(".addplusspan").empty();
		 setWeekendColor();
		 setRestHours();
    });

    $('#nextbutton').click(function() {
         $('#calendar').fullCalendar('next');
         $('#bsit_cal_all_day_date').html($(".fc-head").html());
		 var view = $('#calendar').fullCalendar('getView');
		 $('#title').html(view.title);
		 $(".addplusspan").empty();
		 setWeekendColor();
		 setRestHours();
    });

    // $('#bsit_cal_all_day_date').html(bsit_test);
    $(function(){
    'use strict';
    $(".tableScrollTop,.fc-scroller").scroll(function(){
        $(".fc-scroller,.tableScrollTop")
            .scrollLeft($(this).scrollLeft());
    });
});
    $(document).ready(function() {

  var button = document.getElementById('slide');
button.onclick = function () {	
    var container = $('.bsit_sche_width').find('.fc-scroller')[0];    
    sideScroll(container,'right',50,200,20);
};
var back = document.getElementById('slideBack');
back.onclick = function () {
    var container = $('.bsit_sche_width').find('.fc-scroller')[0];
    sideScroll(container,'left',50,200,20);
};

function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}


});
//***************New sticky header by ND****************
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 65) {
    	
    	//$('#bsit_cal_all_day_date').html($(".fc-head").html());
    	
        //clearHeader, not clearheader - caps H
        $("td.fc-time-area.fc-widget-header").addClass("sticky");
        $(".bsit_left-right-scroll button").addClass("sticky");
    } else {       
        $("td.fc-time-area.fc-widget-header").removeClass("sticky");
        $(".bsit_left-right-scroll button").removeClass("sticky");
      }
});
    /*$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 65) {
    	$('#bsit_cal_all_day_date').html($(".fc-head").html());
    	
        //clearHeader, not clearheader - caps H
        $(".bsit_cal_day_sticky").addClass("sticky");
        $(".bsit_scroll_btn_sticky").addClass("sticky");
    } else {       
        $(".bsit_cal_day_sticky").removeClass("sticky");
        $(".bsit_scroll_btn_sticky").removeClass("sticky");
      }
});
*/
$(".js-example-basic-multiple").select2();
	// Setup the user list
	var userOutput = userArray.map(function(user) {
		return {
			id: user.UserID,
			title: user.Username
		}
	});

	function getServerTime() {
		return $.ajax({async: false}).getResponseHeader('Date');
	}

	function getMonday(fromDate) {
		fromDate = new Date(fromDate);
		var day = fromDate.getDay(),
			diff = fromDate.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
		return new Date(fromDate.setDate(diff));
	}

	var shiftPressed = false;

	$(document).keydown(function(event) {
    if(event.which == "16")
			shiftPressed = true;
	});

	$(document).keyup(function() {
		shiftPressed = false;
	});

	// made by add button event amit
	$("#mySelect").on('change', function () {
	       var id = this.options[this.selectedIndex].id;
	       
	       if(id == "timelineDay"){ 
	        $('#calendar').fullCalendar('changeView','timelineCustomDay');
	         	var view = $('#calendar').fullCalendar('getView');
				$('#title').html(view.title);
	        // $(".fc-view-container .fc-timelineDay-view table thead tr td:nth-child(3) .fc-scroller-clip .fc-scroller .fc-scroller-canvas .fc-content table tbody tr:nth-child(1)").remove();
	        
			/*16-08-2021*/
			//setHoursTimeStrip();
			setWeekendColor();
			setRestHours();
			checkBrokenProfileImg();
			/*end*/
	        } 
	       if(id == "monthbutton"){
	        $('#calendar').fullCalendar('changeView','month');
	        } 
	      if(id == "timelineWeek"){
	        $('#calendar').fullCalendar('changeView','timelineCustomWeek');
	        var view = $('#calendar').fullCalendar('getView');
				$('#title').html(view.title);
	        // $(".fc-view-container .fc-timelineWeek-view table thead tr td:nth-child(3) .fc-scroller-clip .fc-scroller .fc-scroller-canvas .fc-content table tbody tr:nth-child(2)").remove();
			
			/*16-08-2021*/
			//setHoursTimeStrip();
			setWeekendColor();
			setRestHours();
			checkBrokenProfileImg();
			/*end*/
	        }
	      if(id == "timelineNext3Months"){
	        $('#calendar').fullCalendar('changeView','timelineNext3Months');
	        var view = $('#calendar').fullCalendar('getView');
				$('#title').html(view.title);
				 $(function(){
				    'use strict';
				    $(".tableScrollTop,.fc-scroller").scroll(function(){
				        $(".fc-scroller,.tableScrollTop")
				            .scrollLeft($(this).scrollLeft());
				    });
				});
	        }  
	        /*16-08-2021*/
	        //setHoursTimeStrip();
			setWeekendColor();
			setRestHours();
			checkBrokenProfileImg();
			/*end*/
	   });

	$(document).ready(function() { 
	   var view = $('#calendar').fullCalendar('getView');
		$('#title').html(view.title);
	});   

	
	$('#calendar').fullCalendar({
		//now: '2017-05-07',
		schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
		now: getMonday(new Date(getServerTime())),
		editable: true,
		aspectRatio: 1.8,
		scrollTime: '00:00',
		slotWidth: 2,
		height:'auto',
		header: {
			left: '',
			// left: 'today,timelineDay,agendaWeek,month,timelineNext3Months,timelineThreeDays',
			// center: 'title',
			
			// right: 'prev,next'
		},
		defaultView: 'timelineNext3Months',
		views: {
			timelineNext3Months: {
				type: 'timeline',
				duration: { days: 90 }
			},
			timelineCustomWeek: {
				type: 'timeline',
				duration: { days: 7 },
				slotDuration: {days:1},
			},
			timelineCustomDay: {
				type: 'timeline',
				duration: { days: 1 },
				slotDuration: {days:1},
			}
		},
		customButtons: {
			promptResource: {
				text: '+ room',
				click: function() {
					var title = prompt('Room name');
					if (title) {
						$('#calendar').fullCalendar(
							'addResource',
							{ title: title },
							true // scroll to the new resource?
						);
					}
				}
			}
		},
		selectable: true,
		selectHelper: true,
		// select function requires 5 params (start, end, event, view, resource) IN THAT ORDER.
		select: function(start, end, event, view, resource) {
		  /* $('.picker__box ul li').removeClass("picker__day--selected");
				$('.picker__box ul li').removeClass("picker__day--highlighted"); */
		  	ShowEventDialogue(event.id, start, end, resource.id);
		  	$("#calendar").fullCalendar("unselect");
		},
		resourceLabelText: 'All Users',
		resourceAreaWidth: "15%",
		resourceRender: function(resource, cellEls) {
			// When clicking on a users cell
			// cellEls.on('click', function() {

			// });
		},
		// userArray is setup and built in the schedules.php file
		resources: userOutput,
		// We reload events when the user changes the view date range
		events: function(start, end, timezone, callback) {
			// Get the list of all resources (users) to display events from.
			//debugger
			var resourceList = $('#calendar').fullCalendar('getResources');
			//var resource_json =	'[{"resourceId": 56 }, {"resourceId": 57}, {"resourceId": 66}]'; // Example JSON structure
			// Build a JSON string to pass the list of all users to the SP.
			var resource_json = '[';
			$.each(resourceList, function (indexInArray, valueOfElement) {
				if(indexInArray == (resourceList.length-1)) {
					 // If we're at the last element in the array, don't append ","
					 resource_json = resource_json + '{"resourceId":' + resourceList[indexInArray].id + '}';
				} else {
					resource_json = resource_json + '{"resourceId":' + resourceList[indexInArray].id + '},';
				}
			});
			resource_json = resource_json + ']';

			// event id was specified, load details from database using AJAX
			var filterStart = moment(start).format("YYYY/MM/DD HH:mm");
			var filterEnd = moment(end).format("YYYY/MM/DD HH:mm");
			var load_event_data = {
			parameters: [
				{ key: "@start_date", value: filterStart },
				{ key: "@end_date", value: filterEnd },
				{ key: "@owner_id", value: -1 },
				{ key: "@owners_json", value: resource_json }
			],
			sp_name: [{ key: "BSIT_CalendarLoadEvents" }]
			};

			// Setup the events array so it can be populated
			var events = [];
			// -------------------------------------------------
			// Ajax call to the API to retrieve the list of events
			// Std Events, most important!
			// -------------------------------------------------
			
			$.ajax({
				type: "POST",
				url: base_url + "core/Apilocal/Call_SP",
				data: load_event_data,
				dataType: "json",
				success: function(results) {
								
				var colorFg = "#000000";
				var colorBg = "#FFFFFF";
				var evntTitle = "";
				var colorBorder = "";

				var disabled_event_ids1 = "";
				var disabled_event_ids = new Array();

				// check we recieved some results back
				if (results[0] == null || results[0] === ''){
					// No results
				}
				else {
					if (results[0]["disabled_events"] != null) {
						disabled_event_ids1 = results[0]["disabled_events"].split(",");

						$.each(disabled_event_ids1, function(i) {
							var idx = $.inArray(disabled_event_ids1[i], disabled_event_ids);
							if (idx == -1) {
								disabled_event_ids.push(parseInt(disabled_event_ids1[i]));
							}
						});
					}
					//console.log(results);
					objjobdata=results;
					
					$.each(results, function(key, value) {
						evntTitle = "";
						var colorJson = $.parseJSON(value["EventColor"]);
						if (typeof colorJson != "undefined") {
							if(value["is_confirmed"]==1){
								colorBg = colorJson[0];
								colorFg = "#fff";
							}
							else{
								colorBg = colorJson[0] + "4D";
								colorFg = colorJson[0];
							}
							colorBorder = colorJson[0] + "80";
						}
						
						

						// set the css classes and specific settings for different job types
						var EventCSSClasses = [];

						var allDay = 0;

						var renderingOption = '';
						switch (value["Type"]) {
							case "avail":
									allDay = (value["allDay"] == 1 ? 1 : 0);
									renderingOption = 'background';
									colorBg = '#e6f246';
									EventCSSClasses.push("BSIT-CalAvailHoursEvent");
								break;
							case "planned":
									allDay = (value["allDay"] == 1 ? 1 : 0);
									EventCSSClasses.push("BSIT-CalPlannedEvent");
								break;
							case "deleted":
									EventCSSClasses.push("BSIT-CalDeletedEvent");
									evntTitle = evntTitle + " ____ ";
								break;
							// case "allday":
							// 	allDay = 1;
							// 	break;
							case "material":
									EventCSSClasses.push("BSIT-CalMaterialEvent");
									evntTitle = evntTitle + " ____ ";
									allDay = 1;
								break;
						}
						
						
						if(value["is_confirmed"]==1)
						{
								EventCSSClasses.push("BSIT-CalCompleteEvent");
								
						}else{
							EventCSSClasses.push("BSIT-stripsEvent");
						}
						
						
						
						// setup the event title
						evntTitle = evntTitle + value["Name"];

						// add the ref # to the event
						if (value["CustJobRefNum"] !== "" && value["CustJobRefNum"] !== null) {
							evntTitle = evntTitle + (value["CustJobRefNum"] == ""	? "" : ":" + value["CustJobRefNum"]);
						}

						// add the task title
						// If the event end time is empty, this is an all day event for a single day
						if (value["Type"] == "allday") {
							if (value["Title"] == null && value["jobTitle"] == null) {
								evntTitle = evntTitle + " - " + value["Description"];
							}
						}
						else {
							evntTitle = evntTitle + " - " + (value["Title"] == null ? value["jobTitle"] : value["Title"]);
						}

						var startDateTime = value["StartDateTime"];
						var endDateTime = value["EndDateTime"];

						// if (endDateTime == "0000-00-00 12:00:00 am") {
						// 	endDateTime = startDateTime;
						// 	allDay = 1;
						// }
						var hoursperday=value["hours_per_day"];

						// Disable the owner of 0 ownerId events.
						if(value["ownerId"] == 0) {
							value["ownerId"] = "";
						}
						// show planned and available events
						if((value["Type"] == "planned" || value["Type"] == "avail") && allDay == 1 && value["Type"] != "deleted"){
							events.push({
								id: value["key_id"],
								title: evntTitle,
								start: startDateTime,
								end: endDateTime,
								backgroundColor: colorBg,
								borderColor:colorBorder,
								
								textColor: colorFg,
								allDay: allDay,
								className: EventCSSClasses,
								type: value["Type"],
								evt_disable_id: disabled_event_ids,
								rendering: renderingOption,
								resourceId: value["ownerId"],
								hoursperday:hoursperday
							});
						}
						
						
					});
				};
				callback(events);
				},
				error: function(e, x, y) {
					console.log(e);
					console.log(x);
					console.log(y);
			}
			});
		},
		eventClick: function(event, jsEvent, view) {
			if(shiftPressed) {
				// Setup request parameters using the original events' data.
				var loadEventDetails = {
					parameters: [
						{ key: "@eventID", value: event.id }
					],
					sp_name: [{ key: "BSIT_GetEventDetails" }]
				};

				$.ajax({
					type: "POST",
					url: base_url + "core/Apilocal/Call_SP",
					data: loadEventDetails,
					dataType: "json",
					success: function(resultsData) {
						// Successfully retrieved event details.
						var duplicateEventData = {
							parameters: [
								{ key: "@key_id", value: 0 }, // new event id
								{ key: "@start_date_time",	value: event.start._i }, // event times to duplicate
								{ key: "@end_date_time",	value: event.end._i },
								{ key: "@owner_id", value: event.resourceId }, // owner
								{ key: "@type", value: "planned" }, // planned since only planned will be showing on schedules
								{ key: "@job_id", value: resultsData[0].job_id }, // job
								{ key: "@material_cost", value: 0 },
								{ key: "@material_charge_price", value: 0	},
								{ key: "@orgs_id", value: resultsData[0].orgs_id }, // customer
								{ key: "@description", value: "" },
								{ key: "@internal_notes", value: resultsData[0].internal_notes },
								{ key: "@work_type_id", value: resultsData[0].work_type_id },
								{ key: "@shared_events_data", value: 0 },
								{ key: "@job_TermsCode", value: "" },
								{ key: "@job_CustRefNum", value: "" },
								{ key: "@job_Status", value: "" },
								{ key: "@job_CustomerID", value: 0 },
								{ key: "@job_name", value: "" },
								{ key: "@workflow_id", value: 0 },
								{ key: "@job_type", value: "" },
								{ key: "@job_parent_id", value: 0 },
								{ key: "@all_day", value: 1 },
								{ key: "@isconfirmed", value: resultsData[0].is_confirmed},
								{ key: "@hours_per_day", value: resultsData[0].hours_per_day}
							],
							sp_name: [{ key: "BSIT_CalendarSaveEvent" }]
						};
						// Now create new event
						$.ajax({
							type: "POST",
							url: base_url + "core/Apilocal/Call_SP",
							data: duplicateEventData,
							dataType: "json",
							success: function(resultsData) {
								// Event created, rerender the calendar.
								$("#calendar").fullCalendar("refetchEvents");
							},
							error: function(xx) {
								alert(xx.responseText);
							}
						});
					},
					error: function(e) {
						alert(e.responseText);
					},
					complete: function() {
						setRestHours();
					}
				});
			} else {
				// Show normal event details
				ShowEventDialogue(
					event.id,
					moment(event.start).format("YYYY/MM/DD HH:mm"),
					moment(event.end).format("YYYY/MM/DD HH:mm"),
					event.resourceId
				);
			}
		},
		  // when the event is repositioned, we must update the database
		eventDrop: function(event, delta, revertFunc) {
			console.log(event);
			var doUpdate = 1;
			// event id was specified, load details from database using AJAX
			var filterStart = moment(event.start).format("YYYY/MM/DD HH:mm");
			var filterEnd = moment(event.end).format("YYYY/MM/DD HH:mm");

			// if event type is deleted, don't allow moving
			if (event.type == "deleted") {
			revertFunc();
			doUpdate = 0;
			}

			// check if the end time is 00:00, this must be an allday event
			// event type is currently all day, we need to change to something, planned for now
			if (event.allDay == 0 && event.type == "allday") {
				filterEnd = filterStart.add(moment.duration(2, "hours"));
				filterEnd = filterEnd.format("YYYY/MM/DD HH:mm");
			}
			// if event type is currently not all day, but type is material, user has tried to drop material to hours
			if (event.allDay == 0 && event.type == "material") {
				revertFunc();
				doUpdate = 0;
			}

			if (doUpdate == 1) {
			// setup the json string which will do the update
			var json_str =
				'{"colls": [{"coll_id": "15", "values": {"key_id":"' +
				event.id +
				'","ownerId":"' +
				parseInt(event.resourceId) +
				'", "StartDateTime":"' +
				filterStart +
				'","EndDateTime":"' +
				filterEnd +
				'" ' +
				',"Type":"planned"' +
				"}}]}";

			console.log("JSONSTR: "  + json_str);
			// update the single record details from the server
			$.ajax({
				type: "POST",
				url: base_url + "core/Apilocal/update",
				data: { data: json_str },
				dataType: "json",
				success: function(response) {
				//if(response.status != 'success')
				//revertFunc();
				},
				error: function(e) {
				revertFunc();
				alert("Error processing your request: " + e.responseText);
				},
			complete: function() {
				setRestHours();
				}
			});
			}
		},
		// when an event is extended, we need to update the end date
		eventResize: function(event, delta, revertFunc) {
			var event_disable_id1 = [];
			$.each(event.evt_disable_id, function(key, value) {
			event_disable_id1.push(parseInt(value));
			});
			if ($.inArray(parseInt(event.id), event_disable_id1) > -1) {
			revertFunc();
			return false;
			}
			// event id was specified, load details from database using AJAX
			var filterStart = moment(event.start).format("YYYY/MM/DD HH:mm");
			var filterEnd = moment(event.end).format("YYYY/MM/DD HH:mm");
			var doUpdate = 1;

			if (doUpdate == 1) {
				var json_str =
					'{"colls": [{"coll_id": "15", "values": {"key_id":"' +
					event.id +
					'", "StartDateTime":"' +
					filterStart +
					'","EndDateTime":"' +
					filterEnd +
					'"}}]}';

				// load the single records details from the server
				$.ajax({
					type: "POST",
					url: base_url + "core/Apilocal/update",
					data: { data: json_str },
					dataType: "json",
					success: function(response) {
					//if(response.status != 'success')
					//revertFunc();
					},
					error: function(e) {
					revertFunc();
					alert("Error processing your request: " + e.responseText);
					},
					complete: function() {
						setRestHours();
					}
				});
			}
		}
	});

	$(document).on("click", "#close_button", function(e) {
		$("#advancedSearch").hide();
		$("#job_list").css("border", "none");
		//$('#shared_event_div li').remoattr('selected',false);
		$(".select2-selection__choice").remove();
		var current_user = $("#curr_user").val();
		$('#ifDelete').val();
				$('#schedules_calendar_delete_job').removeClass('bsit_delete_file');				
				$('#ifDelete').val('planned');
		//FUNCTION FOR LOAD DEFAULT JOB-PANEL
		getDataForJobCardPanelDefault(current_user);
	});

	$(document).on("click", "#job_close_button", function(e) {
		$("#eventContent").css("opacity", "1");
	});

	$(document).on("click", ".job_box", function(e) {
		$("#add_job_div").css("display", "none");
		var curr_jc_id = $(this).attr("id");
		curr_jc_id = curr_jc_id.split("_");
		curr_jc_id = curr_jc_id[2];
		$(".cust_back_color").find(".cust_color").removeClass("active");
		$("#job_list").css("border", "none");
		$(this).find(".cust_color").addClass("active");
		$("#sel_job_id").val(curr_jc_id);
		$("#job_title").val($(this).find(".job_card_title").text());
		$("#parent_job_title").val($.trim($(this).find(".job_card_project").text()));
		$("#sel_parent_job_id").val($(this).find(".job_card_project").attr("data-pid"));

		var parent_job_id = $("#sel_parent_job_id").val();
		if (parent_job_id != "") {
		load_status_based_on_project(parent_job_id, 0); // LOAD STATUS BASED ON PARENT JOB
		} else {
		//output.push('<option value="">In Progress</option>');
		}
		//alert($(this).find('.job_load_status').text());
		$("#selectEventCustomerId option")
		.removeAttr("selected")
		.filter(
			"[value=" +
			$(this)
				.find(".job_card_customer")
				.attr("data-cid") +
			"]"
		)
		.attr("selected", true);
		$("#selectEventCustomerId").val(
		$(this)
			.find(".job_card_customer")
			.attr("data-cid")
		);
		$("#status").val(
		$(this)
			.find(".job_load_status")
			.text()
		);
	});

	$(document).on("click", "#advancedSearchDropArrowCalendar", function(e) {
		showHideAdvancedSearch();
	});

	$(document).on("click", "#add_job_box", function(e) {
		$(".cust_back_color")
			.find(".cust_color")
			.removeClass("active");
		//$("#add_job_div").css('display','block');
		$("#job_name").val($("#job_title").val());
		$("#job_title").val("Add Job");
		$("#sel_job_id").val("[Add Job]");

		$("#jobContent").modal(); // OPEN ADD JOB POPUP

		$("#eventContent").css("opacity", "0.3");

		/* alert($('#selectEventCustomerId').val());
		alert($('#sel_parent_job_id').val()+'***'+$('#parent_job_title').val());  */

		if ($("#selectEventCustomerId").val() != "") {
			$("#job_selectEventCustomerId").val($("#selectEventCustomerId").val());
		} else {
			$("#job_selectEventCustomerId").val("");
		}
		if ($("#sel_parent_job_id").val() != "" && $("#parent_job_title").val()) {
			$("#job_sel_parent_job_id").val($("#sel_parent_job_id").val());
			$("#job_parent_job_title").val($("#parent_job_title").val());
			load_status_based_on_project_for_addJobModel(
				$("#job_sel_parent_job_id").val(),
				0
			);
			$("[name=job_workflow_id]").prop("disabled", false);
			$("[name=job_workflow_id]").val(7);
			$("[name=job_workflow_id]").prop("disabled", true);
		} else {
			$("#job_sel_parent_job_id").val("");
			$("#job_parent_job_title").val("");
			$("#job_status option").remove();

			$("[name=job_workflow_id]").prop("disabled", false);
			$("[name=job_workflow_id]").val(7);
			$("[name=job_workflow_id]").prop("disabled", true);
		}
	});
	
  
  $("#checkbox-opportunities").click(function () {
      var isChecked = $(this).is(":checked");
      let jobId = "";
      if (isChecked) {
         jobId = "Opportunities";
    	}

        var ParentJobID = $("#sel_parent_job_id").val();
        var customer_id = $("#selectEventCustomerId").val();

        if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
          ParentJobID = "";
        }
        // Load job card based on search query
        $(".job_card_loader1").show();
              
        if(jobId == "") 
        {
        	if (customer_id = '') 
        	{          	
        		var current_user = $("#curr_user").val();
        		getDataForJobCardPanelDefault(current_user);
        	} else if ( ParentJobID = '') {
        		var current_user = $("#curr_user").val();
        		getDataForJobCardPanelDefault(current_user);
        	} else {
        		jobId = "Opportunities";
        		getDataForJobCardPanel(jobId, ParentJobID, customer_id);
        	}
        } else {
          getDataForJobCardPanel(jobId, ParentJobID, customer_id);
        }
        
    });	

  $("#job_title").autocomplete({
    minLength: 0,
    delay: 300,
    search: function(event, ui) {
      var jobId = $(this).val();
	  
      var ParentJobID = $("#sel_parent_job_id").val();
      var customer_id = $("#selectEventCustomerId").val();

      if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
        ParentJobID = "";
      }
      // Load job card based on search query
      $(".job_card_loader1").show();
      if(jobId == "") {
        var current_user = $("#curr_user").val();
        getDataForJobCardPanelDefault(current_user);
      } else {
        getDataForJobCardPanel(jobId, ParentJobID, customer_id);
      }
    }
  });

	$("#commit-event-addedit").button().on("click", function() {
		
		// copy the user edited date and time into the submitting form inputs
		var startTime = $("#fromTimeSelector").val();
		var endTime = $("#toTimeSelector").val();
		var startDate = $("#fromDateSelector").val();
		var endDate = $("#toDateSelector").val();
		var isconfirmed = 0;
		var hours_per_day = $ ("#hours_per_day").val();
		
		if (hours_per_day == "" || hours_per_day == null) {
			$("#hours_per_day").css({ "border-color": "#f44336" });
			return false;
		}
		else {

			$("#hours_per_day").css({ "border-color": "#e1e8ee" });

		}

		if ($("#checkbox-confirmed").is(":checked")) {
        isconfirmed = 1;
      }

		//alert($('#job_list .job_box').children().hasClass('.active'));
		var active_sub_menu = $("#job_list .job_box .active").length;
		

		if (active_sub_menu <= 0) {
			$("#job_list").css("border", "1px solid red");
			return false;
		} else {
			$("#job_list").css("border", "none");
		}

		// set the hidden input
		$("#startTime").val(startDate + " " + startTime);
		$("#endTime").val(endDate + " " + endTime);

		// setup the variable to talk with the submitting form
		var form = $("#addEditEventForm");
		var values = form.serializeObject();

		if (values["ParentJobID"] != "") {
			var ParentJobId = $("#sel_parent_job_id").val();
		} else {
			var ParentJobId = "";
		}

		delete values["ParentJobID"];
		delete values["TermsCode"];
		delete values["CustRefNum"];
		delete values["Status"];
		delete values["name"];
		delete values["shared_user"];

		values["Type"] = $("#eventType").val();
		values["WorkTypeId"] = $("#selectWorkTypeId").val();

		var eventType = $("#eventType").val();
		var jobId = $("#sel_job_id").val();

		if (eventType == "actual" || eventType == "actual-nonbillable") {
			var jobId = $("#sel_job_id").val();
		} else {
			var jobId = $("#sel_job_id").val();
			if (jobId != "") {
				jobId = $("#sel_job_id").val();
			}
		}
		var json_values = JSON.stringify(values);
		json_values = JSON.parse(json_values);
		json_values["jobId"] = jobId;
		json_values["ParentJobId"] = ParentJobId;
		json_values = JSON.stringify(json_values);

		var coll_id = form.attr("data-collid");
		var full_json = $.fn.bsitJsonObject(coll_id, json_values);
		var eventId = $("#addEditEventForm [name=key_id]").val();
		var url = base_url + "core/Apilocal/create";

		// [START] - TaskId-40(Job required) //

		if ($("#addEditEventForm [name=ParentJobID]").val() == "") {
			// ParentJobID FIELD IS REQUIRED
			$("#parent_job_title").addClass("required_border_class");
			return false;
		} else {
			$("#parent_job_title").removeClass("required_border_class");
		}
		

		if (Date.parse(startDate + " " + startTime) >= Date.parse(endDate + " " + endTime) ||
			(startDate == "" || startTime == "" || endDate == "" || endTime == "")) {
			$("#fromDateSelector").css({ "border-color": "#f44336" });
			$("#fromTimeSelector").css({ "border-color": "#f44336" });
			$("#toDateSelector").css({ "border-color": "#f44336" });
			$("#toTimeSelector").css({ "border-color": "#f44336" });

			return false;
		} else {
			$("#fromDateSelector").css({ "border-color": "#e1e8ee" });
			$("#fromTimeSelector").css({ "border-color": "#e1e8ee" });
			$("#toDateSelector").css({ "border-color": "#e1e8ee" });
			$("#toTimeSelector").css({ "border-color": "#e1e8ee" });
		}
		if (eventType == "actual" || eventType == "actual-nonbillable" || eventType == "material") {
			if ($("#addEditEventForm [name=jobId]").val() == "" && $("#sel_job_id").val() == "") {
				$("#addEditEventForm [name=Description]").css({
				"border-color": "rgba(0, 0, 0, 0.26)",
				"border-width": "medium medium 1px"
				});

				$("#job_title").css({
				"border-color": "#f44336",
				"border-width": "2px"
				});

				return false;
			} else {
				$("#job_title").css({
				"border-color": "rgba(0, 0, 0, 0.26)",
				"border-width": "medium medium 1px"
				});
			}
		} else if (eventType == "planned" || eventType == "allday") {
			if ($("#addEditEventForm [name=Description]").val() == "") {
				
				$("#job_title").css({
				"border-color": "rgba(0, 0, 0, 0.26)",
				"border-width": "medium medium 1px"
				});

				$("#addEditEventForm [name=Description]").css({
				"border-color": "#f44336",
				"border-width": "2px"
				});
				return false;
			} else {
				$("#addEditEventForm [name=Description]").css({
				"border-color": "rgba(0, 0, 0, 0.26)",
				"border-width": "medium medium 1px"
				});
			}
		}
		
		
		// [END] - TaskId-40(Job required) //

		// check if the event id is specified, if so we create new or save edited
		if ((eventId !== 0 && typeof eventId != "undefined") || eventId == "") {
			url = base_url + "core/Apilocal/update";
		}

		// if the customer ID is null, don't save, alter the user
		var CustID = $("#selectEventCustomerId").val();
		
		if (CustID == "" || CustID == null || CustID == "undefined") {
			$("#selectEventCustomerId")
				.fadeIn(100)
				.fadeOut(100)
				.fadeIn(100)
				.fadeOut(100)
				.fadeIn(100);
			$("#selectEventCustomerId").css({
				"border-color": "#f44336",
				"border-width": "2px"
			});
			return false;
		} else {
			$("#selectEventCustomerId").css({
				"border-color": "rgba(0, 0, 0, 0.26)",
				"border-width": "medium medium 1px"
			});
		}

		var json_values1 = JSON.parse(json_values);

		if ((eventId !== 0 && typeof eventId != "undefined") || eventId == "") {
			var job_key_id = json_values1["key_id"];
		} else {
			var job_key_id = "0";
		}

		var shared_events = $("#shared_user_id").val();
		if (shared_events != null) {
			shared_events_data = shared_events.toString(); // Convert to String
		} else {
			shared_events_data = "0";
		}
		$(".select2-selection__choice").remove();

		if (json_values1["MaterialCost"] == "") {
		json_values1["MaterialCost"] = 0;
		}

		if (json_values1["MaterialChargePrice"] == "") {
		json_values1["MaterialChargePrice"] = 0;
		}

		if (json_values1["Type"] != "material") {
		json_values1["MaterialCost"] = 0;
		json_values1["MaterialChargePrice"] = 0;
		}

		var orgsId = $("#selectEventCustomerId").val();
		var cust_ref_num = "";
		var status = $("#status").val();
		var job_name = $("#job_name").val();
		var hours_per_day = $("#hours_per_day").val();
		if (json_values1["jobId"] == 0) {
			return false;
		}

		var job_type = "";
		var workflow_id = "";
		var save_event_data = {
			parameters: [
				{ key: "@key_id", value: job_key_id },
				{
					key: "start_date_time",
					value: moment(json_values1["StartDateTime"]).format("YYYY-MM-DD HH:mm")
				},
				{
					key: "end_date_time",
					value: moment(json_values1["EndDateTime"]).format("YYYY-MM-DD HH:mm")
				},
				{ key: "@owner_id", value: json_values1["ownerId"] },
				{ key: "@type", value: ((json_values1["Type"] == "deleted") ? "deleted" : "planned") },
				{ key: "@job_id", value: json_values1["jobId"] },
				{ key: "@material_cost", value: json_values1["MaterialCost"] },
				{
					key: "@material_charge_price",
					value: json_values1["MaterialChargePrice"]
				},
				{ key: "@orgs_id", value: json_values1["orgsId"] },
				{ key: "@description", value: json_values1["Description"] },
				{ key: "@internal_notes", value: json_values1["InternalNotes"] },
				{ key: "@work_type_id", value: json_values1["WorkTypeId"] },
				{ key: "@shared_events_data", value: shared_events_data },
				{ key: "@job_TermsCode", value: "Bill" },
				{ key: "@job_CustRefNum", value: cust_ref_num },
				{ key: "@job_Status", value: status },
				{ key: "@job_CustomerID", value: orgsId },
				{ key: "@job_name", value: job_name },
				{ key: "@workflow_id", value: workflow_id },
				{ key: "@job_type", value: job_type },
				{ key: "@job_parent_id", value: json_values1["ParentJobId"] },
				{ key: "@all_day", value: 1 },
				{ key: "@isconfirmed", value: isconfirmed},
				{ key: "@hours_per_day", value: hours_per_day},
				
			],
			sp_name: [{ key: "BSIT_CalendarSaveEvent" }]
		};
		// console.log(save_event_data); return
		$.ajax({
			type: "POST",
			url: base_url + "core/Apilocal/Call_SP",
			data: save_event_data,
			dataType: "json",
			success: function(resultsData) {

				$('#ifDelete').val();
				$('#schedules_calendar_delete_job').removeClass('bsit_delete_file');
				$('#ifDelete').val('planned');

				$(".loader").hide();
				$("#eventContent").modal("hide");
				$("#calendar").fullCalendar("refetchEvents");
				$("#add_job_div").css("display", "none");
				$("#advancedSearch").hide();

				//FUNCTION FOR LOAD DEFAULT JOB-PANEL
				var current_user = $("#curr_user").val();
				
				getDataForJobCardPanelDefault(current_user);
			},
			error: function(xx) {
				alert(xx.responseText);
			},
			complete: function() {
				//console.log(save_event_data);
				//jQuery(".addplusspan").empty();
				setRestHours();
				// call function for set rest hours  ajax
			}
		});
	});

	$("#save_job_data").button().on("click", function() {
		var form = $("#jobForm");
		var values = form.serializeObject();
		var rec_id = $("#rec_id").val();
		var coll_id = $(form).data("collid");

		//alert($('#job_parent_job_title').text());
		//return false;
		var cust_color = $("#job_selectEventCustomerId :selected").attr(
			"iscolor"
		);
		var Customer_id = $("#job_selectEventCustomerId").val();
		var Customer_name = $("#job_selectEventCustomerId :selected").text();
		var job_status = $("#job_status").val();
		var jobTitle = $("#job_name").val();
		var InternalNotes = $("#job_InternalNotes").val();
		var job_desc = $("#job_desc").val();
		var workflow_id = $("#job_selectWorkTypeId").val();

		if ($("#job_selectEventCustomerId").val() == "") {
			// ParentJobID FIELD IS REQUIRED
			$("#job_selectEventCustomerId").addClass("required_border_class");
			return false;
		} else {
			$("#job_selectEventCustomerId").removeClass("required_border_class");
		}
		if ($("#job_parent_job_title").val() == "") {
			// ParentJobID FIELD IS REQUIRED
			$("#job_parent_job_title").addClass("required_border_class");
			return false;
		} else {
			$("#job_parent_job_title").removeClass("required_border_class");
		}
		if ($("#job_name").val() == "") {
			// ParentJobID FIELD IS REQUIRED
			$("#job_name").addClass("required_border_class");
			return false;
		} else {
			$("#job_name").removeClass("required_border_class");
		}
		if ($("#job_selectWorkTypeId").val() == "") {
			// ParentJobID FIELD IS REQUIRED
			$("#job_selectWorkTypeId").addClass("required_border_class");
			return false;
		} else {
			$("#job_selectWorkTypeId").removeClass("required_border_class");
		}
		if ($("#job_status").val() == "") {
			// ParentJobID FIELD IS REQUIRED
			$("#job_status").addClass("required_border_class");
			return false;
		} else {
			$("#job_status").removeClass("required_border_class");
		}
		$("#job_selectWorkTypeId").prop("disabled", false);

		$("#job_status option").each(function() {
			$("#status").append(
			'<option value="' +
				$(this).attr("value") +
				'">' +
				$(this).attr("value") +
				"</option>"
			);
		});

		var data_pass = {
			CustomerID: $("#job_selectEventCustomerId").val(),
			ParentJobID: $("#job_sel_parent_job_id").val(),
			CustRefNum: $("#job_CustRefNum").val(),
			title: $("#job_name").val(),
			desc: $("#job_desc").val(),
			EstDurationHrs: $("#job_EstDurationHrs").val(),
			workflow_id: $("#job_selectWorkTypeId").val(),
			Status: $("#job_status").val(),
			Priority: $("#job_Priority").val(),
			InternalNotes: $("#job_InternalNotes").val(),
			TermsCode: $("#job_TermsCode").val(),
			QuotedPrice: $("#job_QuotedPrice").val()
		};
		var json_values = JSON.stringify(data_pass);
		var full_json = $.fn.bsitJsonObject(coll_id, json_values);

		function add_new_job_event() {
			var result;
			//alert(base_url+"core/Apilocal/create");

			$.ajax({
			type: "POST",
			url: base_url + "core/Apilocal/create",
			data: { data: full_json },
			async: false,
			//dataType: 'json',
			success: function(returnData) {
				returnData = jQuery.parseJSON(returnData);
				result = returnData[0]["last_insert_id"];
			}
			});
			return result;
		}

		var last_job_id = add_new_job_event();
		var parent_job_id = $("#job_sel_parent_job_id").val();
		var parent_job_name = set_project_value_basedon_job(
			$("#job_sel_parent_job_id").val(),
			1
		);

		//alert(last_job_id+'**'+parent_job_name);
		var img_icon = "";
		var cust_name = "";
		var parent_job_title = "";
		var jobs_in_event = "";

		var filter_list = '<div class="mui-row" style="padding-bottom:15px;">';

		filter_list +=
			'<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
			last_job_id +
			'">';
		filter_list += '<span class="cust_color active">';
		filter_list +=
			'<span class="job"><label>Job:</label><span class="job_card_title">' +
			jobTitle +
			"</span></span>";

		var colorJson = $.parseJSON(cust_color);
		if (typeof colorJson != "undefined") {
			colorBg = colorJson[0];
			colorFg = colorJson[1];
		}
		filter_list +=
			'<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
			parent_job_id +
			'">' +
			parent_job_name +
			"</span></span>";

		filter_list +=
			'<span class="icon"><span class="job_card_customer" data-cid="' +
			Customer_id +
			'" style="float: left;background-color:' +
			colorBg +
			";color:" +
			colorFg +
			';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
			Customer_name +
			"</span>" +
			img_icon +
			"</span>";
		filter_list +=
			'<span class="job_load_status" style="display:none;">' +
			job_status +
			"</span>";
		filter_list += "</span>";
		filter_list += "</div>";
		filter_list += "</div>";
		$("#job_list").html(filter_list);

		$("#sel_job_id").val(last_job_id);
		$("#job_title").val(jobTitle);
		$("#parent_job_title").val(parent_job_name);
		$("#sel_parent_job_id").val(parent_job_id);
		$("#selectEventCustomerId").val(Customer_id);
		//$('#status').val(job_status);

		//$('#status').append('<option value="'+job_status+'" selected="selected">'+job_status+'</option>');
		//$("#state option:selected").val(job_status);
		$("#status").val(job_status);
		$("#selectWorkTypeId").val(workflow_id);

		//alert($("#job_InternalNotes").val()+'**'+$("#job_desc").val());
		//alert(InternalNotes+'***'+job_desc);

		/* $("#internal_notes").val(InternalNotes);
			$("#description").val(job_desc); */

		$("#internal_notes").val($("#job_InternalNotes").val());
		$("#description").val($("#job_desc").val());

		//alert($("#internal_notes").val()+'****'+$("#description").val());

		$("#jobContent").modal("hide");
		$("#eventContent").css("opacity", "1");
		$(form).trigger("reset");
	});

	$("#parent_job_title").autocomplete({
		source: function(request, response) {
			// get data variables from the button
			var returnfield = this.element.attr("data-returnpropname");
			var collection_id = this.element.attr("data-coll");
			var orig_columns = this.element.attr("data-columns");
			var field_name = this.element.attr("name");
			var columns = orig_columns.replace(/'/g, '"');
			var col_array = jQuery.parseJSON(columns);

			//var ParentJobID			 = $("#addEditEventForm [name=ParentJobID]").val();
			var ParentJobID = $("#parent_job_title").val();
			var selected_cust_id = $("#selectEventCustomerId").val();
			//alert(ParentJobID+'**'+selected_cust_id);
			/* if(selected_cust_id != '' || selected_cust_id != null)
				{ */

			var load_job_search_data = {
			parameters: [
				{ key: "@job_name", value: ParentJobID },
				{ key: "@cust_id", value: selected_cust_id }
			],
			sp_name: [{ key: "BSIT_CalendarParentJobDropdownList" }]
			};

			$.ajax({
			type: "POST",
			url: base_url + "core/Apilocal/Call_SP",
			data: load_job_search_data,
			dataType: "json",
			success: function(responseData) {
				//alert(JSON.stringify(responseData));
				var curr_date = new Date();
				var returnArray = [];

				$.each(responseData, function(index, value) {
				if (
					value["jobs_in_event"] != "" &&
					value["jobs_in_event"] != null
				) {
					var jobs_in_event = value["jobs_in_event"].split(",");
					for (var i = 0; i < jobs_in_event.length; i++) {
					jobs_in_event[i] = parseInt(jobs_in_event[i]);
					}
				} else {
					var jobs_in_event = "";
				}

				if ($.inArray(value["key_id"], jobs_in_event) < 0) {
					if (
					moment(curr_date).format("YYYY-MM-DD") ==
					moment(value["key_create_date"]).format("YYYY-MM-DD")
					) {
					// RESENT JOB USE (+) ICON
					tempArray = {
						label: "",
						value: "",
						custId: "",
						JobStatus: "new"
					};
					} else {
					tempArray = {
						label: "",
						value: "",
						custId: "",
						JobStatus: ""
					};
					}
				} else {
					tempArray = {
					label: "",
					value: "",
					custId: "",
					JobStatus: "used"
					}; // JOB USED BY OTHER USERS THEN USE (clock) ICON
				}
				i = 0;
				//alert(col_array);  //jobId,name,CustName,CustomerID

				$.each(col_array, function(col_index, col_val) {
					// put the first column as the value, concat all others for label
					if (i == 0) {
					tempArray["value"] = value[col_val];
					} else if (i == 3) {
					//tempArray['custId'] = value[col_val];
					} else {
					if (value[col_val] != "" && value[col_val] != null) {
						if (tempArray["label"] != "") {
						tempArray["label"] =
							value[col_val] + " - " + tempArray["label"];
						} else {
						tempArray["label"] = value[col_val];
						}
					}
					}
					// increment I
					i = i + 1;
				});
				// if label is empty simple convert label to value column
				if (tempArray["label"] == "") {
					tempArray["label"] = tempArray["value"];
				}
				// append this row to the array we return
				returnArray.push(tempArray);
				//alert(JSON.stringify(returnArray));
				});
				//alert(returnArray);
				response(returnArray);
			}
			});
			//}
		},
		select: function(event, ui) {
			$(".autoCompColl_parent_job").val(ui.item.label);
			$("#sel_parent_job_id").val(ui.item.value); // Set Parent Job
			//alert($("#sel_parent_job_id" ).val()+'***'+$("#selectEventCustomerId").val());

			/*
				load_status_based_on_project(ParentJobID,0); // LOAD STATUS BASED ON PARENT JOB */

			var jobId = $("#job_title").val();
			var customer_id = $("#selectEventCustomerId").val();
			var ParentJobID = $("#sel_parent_job_id").val();
			/* if(ParentJobID == ''){
					ParentJobID		= '';
				} */

			// FUNCTION FOR LOAD JOB-PANEL BASED ON SELECTED CUSTOMER & PROJECT
			$(".job_card_loader").show();
			getDataForJobCardPanel(jobId, ParentJobID, customer_id);

			return false; // Prevent the widget from inserting the value.
		},
		focus: function(event, ui) {
			//$("#job_name").val($( "#job_title" ).val());
		},
		minLength: 0
	}).bind("focus", function() {
		$(this).autocomplete("search");
	}).data("ui-autocomplete")._renderItem = function(ul, item) {
		//}).data("ui-autocomplete")._renderItem = function (ul, item) {
		if (item.JobStatus == "new") {
		var img_icon = "<img src='" + base_url + "assets/core/images/plus.png'>";
		} else if (item.JobStatus == "used") {
		var img_icon = "<img src='" + base_url + "assets/core/images/history.png'>";
		} else {
		var img_icon = "";
		}
		return $("<li></li>")
		.data("item.autocomplete", item)
		.append(img_icon + " " + item.label)
		.appendTo(ul);
	};

	$("#job_parent_job_title").autocomplete({
	source: function(request, response) {
		// get data variables from the button
		var returnfield = this.element.attr("data-jobreturnpropname");
		var collection_id = this.element.attr("data-jobcoll");
		var orig_columns = this.element.attr("data-jobcolumns");
		var field_name = this.element.attr("name");
		var columns = orig_columns.replace(/'/g, '"');
		var col_array = jQuery.parseJSON(columns);

		var ParentJobID = $("#jobForm [name=job_ParentJobID]").val();
		var selected_cust_id = $("#job_selectEventCustomerId").val();

		var load_job_search_data = {
		parameters: [
			{ key: "@job_name", value: ParentJobID },
			{ key: "@cust_id", value: selected_cust_id }
		],
		sp_name: [{ key: "BSIT_CalendarParentJobDropdownList" }]
		};
		$.ajax({
		type: "POST",
		url: base_url + "core/Apilocal/Call_SP",
		data: load_job_search_data,
		dataType: "json",
		success: function(responseData) {
			//alert(JSON.stringify(responseData));
			var curr_date = new Date();
			var returnArray = [];

			$.each(responseData, function(index, value) {
			if (
				value["jobs_in_event"] != "" &&
				value["jobs_in_event"] != null
			) {
				var jobs_in_event = value["jobs_in_event"].split(",");
				for (var i = 0; i < jobs_in_event.length; i++) {
				jobs_in_event[i] = parseInt(jobs_in_event[i]);
				}
			} else {
				var jobs_in_event = "";
			}

			if ($.inArray(value["key_id"], jobs_in_event) < 0) {
				if (
				moment(curr_date).format("YYYY-MM-DD") ==
				moment(value["key_create_date"]).format("YYYY-MM-DD")
				) {
				// RESENT JOB USE (+) ICON
				tempArray = {
					label: "",
					value: "",
					custId: "",
					JobStatus: "new"
				};
				} else {
				tempArray = {
					label: "",
					value: "",
					custId: "",
					JobStatus: ""
				};
				}
			} else {
				tempArray = {
				label: "",
				value: "",
				custId: "",
				JobStatus: "used"
				}; // JOB USED BY OTHER USERS THEN USE (clock) ICON
			}
			i = 0;
			//alert(col_array);  //jobId,name,CustName,CustomerID

			$.each(col_array, function(col_index, col_val) {
				// put the first column as the value, concat all others for label
				if (i == 0) {
				tempArray["value"] = value[col_val];
				} else if (i == 3) {
				//tempArray['custId'] = value[col_val];
				} else {
				if (value[col_val] != "" && value[col_val] != null) {
					if (tempArray["label"] != "") {
					tempArray["label"] =
						value[col_val] + " - " + tempArray["label"];
					} else {
					tempArray["label"] = value[col_val];
					}
				}
				}
				// increment I
				i = i + 1;
			});
			// if label is empty simple convert label to value column
			if (tempArray["label"] == "") {
				tempArray["label"] = tempArray["value"];
			}
			// append this row to the array we return
			returnArray.push(tempArray);
			//alert(JSON.stringify(returnArray));
			});
			//alert(returnArray);
			response(returnArray);
		}
		});
	},
	select: function(event, ui) {
		$(".job_autoCompColl_parent_job").val(ui.item.label);
		$("#job_sel_parent_job_id").val(ui.item.value); // Set Parent Job

		$("[name=job_workflow_id]").prop("disabled", false);
		$("[name=job_workflow_id]").val(7); // WorkType 7 = PLANNING - SCHEDULING
		$("[name=job_workflow_id]").prop("disabled", true);

		load_status_based_on_project_for_addJobModel(
		$("#job_sel_parent_job_id").val(),
		0
		); // LOAD STATUS BASED ON PARENT JOB
		return false; // Prevent the widget from inserting the value.
	},
	focus: function(event, ui) {
		//$("#job_name").val($( "#job_title" ).val());
	},
	minLength: 1
	}).data("ui-autocomplete")._renderItem = function(ul, item) {
		if (item.JobStatus == "new") {
			var img_icon = "<img src='" + base_url + "assets/core/images/plus.png'>";
		} else if (item.JobStatus == "used") {
			var img_icon = "<img src='" + base_url + "assets/core/images/history.png'>";
		} else {
			var img_icon = "";
		}
		return $("<li></li>").data("item.autocomplete", item).append(img_icon + " " + item.label).appendTo(ul);
	};

	function ShowEventDialogue(eventId, startDateTime, endDateTime, resourceId) {
		//alert(eventId+'--'+startDateTime+'--'+endDateTime+'--'+resourceId);

		var name = "";
		var customer = "";
		var project = "";
		var task = "";
		//alert(base_url);
		// get the event owner (current filter owner)
		//var eventOwner = $("#jobOwnerSelectDropdown").val();
		var eventOwner = resourceId;

		// check if the event id is specified, if so we load details from DB
		if (eventId !== 0 && typeof eventId != "undefined") {
		//EDIT EVENT
		//$('#eventContent h2').html('Edit Event');
		var load_single_event_data = {
			parameters: [
			{ key: "@event_id", value: eventId },
			{ key: "@owner_id", value: eventOwner }
			],
			sp_name: [{ key: "BSIT_CalendarLoadSingleEvent" }]
		};
		} else {
		//ADD NEW EVENT
		//$('#eventContent h2').html('Add Event');
		var load_single_event_data = {
			parameters: [
			{ key: "@event_id", value: "0" },
			{ key: "@owner_id", value: eventOwner }
			],
			sp_name: [{ key: "BSIT_CalendarLoadSingleEvent" }]
		};
		}
		//console.log(load_single_event_data);
		if (eventId !== 0 && typeof eventId != "undefined") {
			//???
		} else {
			//alert(eventId);
			$("#advancedSearch").hide();
			var current_user = $("#curr_user").val();
			//FUNCTION FOR LOAD DEFAULT JOB-PANEL
			getDataForJobCardPanelDefault(current_user);
		}

		$.ajax({
		type: "POST",
		url: base_url + "core/Apilocal/Call_SP",
		data: load_single_event_data,
		dataType: "json",
		beforeSend: function() {
			$(".loader").show();
		},
		success: function(resultsData) {
			var results = resultsData[0];
			// For each results key
			$.each(results, function(key, value) {
				// Set the values on the form from the results.
				$("#addEditEventForm [name=" + key + "]").val(value);
				if (key == "jobId") {
					// Really
					$("#addEditEventForm [name=jobId]").val("");
					$("#sel_job_id").val(results["jobId"]);
					$(".cust_back_color").find("#job_box_" + results["jobId"]).show();
					$(".cust_back_color").find("#job_box_" + results["jobId"]).children(".cust_color").addClass("active");
				} else if (key == "ParentJobId") {
					// Need
					$("#sel_parent_job_id").val(results["ParentJobId"]);
					$("#addEditEventForm [name=ParentJobID]").val(set_project_value_basedon_job(results["ParentJobId"], 1));
				} else if (key == "WorkTypeId") {
					// Comments
					$("#selectWorkTypeId").val(1);
				} else if (key == "Type") {
					// Here
					$("#eventType").val(results["Type"]);
				} else {
					// Please
					$("#addEditEventForm [name=" + key + "]").val(value);
				}
			});

			// If there is a proper event to display
			if (eventId !== 0 && typeof eventId != "undefined") {
				// [START] Only show Popup the Invoiced Events-not changeble //
				var parent_job_id = results["ParentJobId"];
				load_status_based_on_project(parent_job_id, results["Status"]); // LOAD STATUS BASED ON PARENT JOB
				var other_users_events = new Array();
				if (results["disabled_events_from_shared_events"] != null) {
					var other_users_events_data = results["disabled_events_from_shared_events"].split(",");
					$.each(other_users_events_data, function(i) {
						var idx = $.inArray(
							other_users_events_data[i],
							other_users_events
						);
						if (idx == -1) {
							other_users_events.push(parseInt(other_users_events_data[i]));
						}
					});
				}
				if (
					jQuery.inArray(eventId, other_users_events) > -1 ||
					results["disabled_events_from_invoiced"] == 1
				) {
					$("#commit-event-addedit").hide();
					$("#add_job_box").hide();
					$("#submit_button").css("padding-bottom", "20px");
				} else {
					$("#commit-event-addedit").show();
					$("#add_job_box").show();
					$("#submit_button").css("padding-bottom", "0");
				}
				// [END] Only show Popup the Invoiced Events-not changeble //

				/* SharedEvent Selected value for each Event */
				var shared_events_data_dropdown = results["users_for_shared_events_dropdown"].split(",");
				for (var i = 0; i < shared_events_data_dropdown.length; i++) {
					shared_events_data_dropdown[i] = parseInt(shared_events_data_dropdown[i]);
				}
				$("#shared_user_id").val(shared_events_data_dropdown).trigger("change");
				/* SharedEvent Selected value for each Event */
				console.log(results);
				if (results["jobTitle"].length > 125) {
					results["jobTitle"] = results["jobTitle"].substring(0, 125) + "...";
				}
				// WHAT IS GOING ON HERE
				if (results["parent_job_name"].length > 75) {
					results["parent_job_name"] = results["parent_job_name"].substring(0, 75) + "...";
				}
				var img_icon = "";
				var cust_name = "";
				var parent_job_title = "";
				var jobs_in_event = "";

				var filter_list = '<div class="mui-row" style="padding-bottom:15px;">';

				filter_list +=
					'<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
					results["jobId"] +
					'">';
				filter_list += '<span class="cust_color active">';
				filter_list +=
					'<span class="job"><label>Job:</label><span class="job_card_title">' +
					results["jobTitle"] +
					"</span></span>";

				var colorJson = $.parseJSON(results["EventColor"]);
				if (typeof colorJson != "undefined") {
					colorBg = colorJson[0];
					colorFg = colorJson[1];
				}

				filter_list +=
					'<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
					results["ParentJobId"] +
					'">' +
					results["parent_job_name"] +
					"</span></span>";

				filter_list +=
					'<span class="icon"><span class="job_card_customer" data-cid="' +
					results["orgsId"] +
					'" style="float: left;background-color:' +
					colorBg +
					";color:" +
					colorFg +
					';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
					results["Name"] +
					"</span>" +
					img_icon +
					"</span>";
				filter_list +=
					'<span class="job_load_status" style="display:none;">' +
					results["Status"] +
					"</span>";
				filter_list += "</span>";
				filter_list += "</div>";
				filter_list += "</div>";
				$("#job_list").html(filter_list);

				//alert(results['MaterialChargePrice']);
				$('.bsit_schedules_deleted_btn').removeClass('bsit_add_job_delete_btn_remove');
				$("#MaterialCost").val(results["MaterialCost"]);
				$("#MaterialChargePrice").val(results["MaterialChargePrice"]);
				$("#parent_job_title").val(results["parent_job_name"]);
				$("#sel_parent_job_id").val(results["ParentJobId"]);
				$("#selectEventCustomerId").val(results["orgsId"]);
				//$('#status').val(job_status);
				//$('#status').append('<option value="'+job_status+'" selected="selected">'+job_status+'</option>');
				//alert(results['InternalNotes']);
				$("#selectWorkTypeId").val("planned");
				$("#internal_notes").val(results["InternalNotes"]);
				$("#description").val(results["Description"]);
				$("#hours_per_day").val(results["hours_per_day"]);
				// console.log(results.is_confirmed);
 				// alert(results["is_confirmed"]);
				if(results["is_confirmed"] == 1){
					
					document.getElementById("checkbox-confirmed").checked = true;
				}else {
					
					document.getElementById("checkbox-confirmed").checked = false;
				}
				
				// show the dialogue
				$("#eventContent").modal();

				 $(`.modal-backdrop`).on('click', function (e) {
				  e.preventDefault();
				  e.stopPropagation();
				  return false;
				});

			} else {
			// OPEN POPUP FOR ADD EVENT - ALL THE DATA(FIELDS) ARE RESET
			// set the Work-Type
			$('.bsit_schedules_deleted_btn').addClass('bsit_add_job_delete_btn_remove');
			$("#addEditEventForm input").first().focus();
			//$("#selectWorkTypeId").val('');
			$("#commit-event-addedit").show();
			$("#submit_button").css("padding-bottom", "0");
			$("#MaterialCost").val("");
			$("#MaterialChargePrice").val("");
			$("#parent_job_title").val("");
			$("#sel_parent_job_id").val("");
			$("#selectEventCustomerId").val("");
			$("#internal_notes").val("");
			$("#hours_per_day").val("8");
			$("#description").val("");
			$("#status").val("");
			$("#status").html('<option value=""></option>');
			// show the dialogue
			$("#eventContent").modal();

			 $(`.modal-backdrop`).on('click', function (e) {
			  e.preventDefault();
			  e.stopPropagation();
			  return false;
			});
			 
			}
		},
		error: function(xx) {
			//alert(xx.responseText);
		},
		complete: function() {
			// if the ajax call is successful or not, set the owner to current selection
			$("#eventOwner").val(eventOwner);

			// run the event type update processes
			eventTypeChange();

			$(".loader").hide();
			/* // show the dialogue
					$("#eventContent").modal(); */

			if ($(window).width() > 1366) {
				setTimeout(function() {
					var job_card_panel_height =
					$("#eventContent").outerHeight() -
					($(".event_content_header").outerHeight() +
						$(".event_content_footer").outerHeight() +
						$(".event_content_footer2").outerHeight());
					job_card_panel_height = job_card_panel_height - 150;
					//alert(job_card_panel_height);

					$(".cust_back_color").css("min-height", job_card_panel_height);
					$(".cust_back_color").css("max-height", job_card_panel_height);
				}, 200);
			}

		}
		});

		$("#addEditEventForm input").first().focus();
		if (eventId !== 0 && typeof eventId != "undefined") {
		} else {
		$("#addEditEventForm :input").each(function() {
			this.value = "";
		});
		// set the job type to actual as default
		$("#eventType").val("planned");

		// set the owner
		$("#eventOwner").val(eventOwner);

		// set the list of jobs to the selected customer.
		hideShowAvailJobs();

		// run the event type update processes
		//eventTypeChange();
		}
		/* [START] Shows Shared_events_users in ADD Form */
		if (
		$("#addEditEventForm [name=Type]").val() == "planned" ||
		$("#addEditEventForm [name=Type]").val() == "allday"
		) {
		$("#shared_event_div").show();
		} else {
		$("#shared_event_div").hide();
		}
		/* [END] Shows Shared_events_users in ADD Form */

		// set the values of each of the form components for edit and save
		var startTime = moment(startDateTime).format("HH:mm");
		var endTime = moment(endDateTime).format("HH:mm");
		var startDate = moment(startDateTime).format("YYYY/MM/DD");
		var endDate = moment(endDateTime).format("YYYY/MM/DD");

		// set the selectable date and time fields
		$("#fromDateSelector").val(startDate);
		$("#fromTimeSelector").val(startTime);
		$("#toDateSelector").val(endDate);
		$("#toTimeSelector").val(endTime);

		// set the hidden inputs
		$("#startTime").val(startDate + " " + startTime);
		$("#endTime").val(endDate + " " + endTime);

		var event_duration = get_event_duration(
		startDate + " " + startTime,
		endDate + " " + endTime
		);
		$("#event_duration").html(event_duration);
	}

	function getDataForJobCardPanel(jobId = "", ParentJobID = "", customer_id = "") {
		
		/* $(".job_card_loader").show();
			$(".job_card_loader1").show(); */
		var load_job_search_data = {
		parameters: [
			{ key: "@job_name", value: jobId },
			{ key: "@parent_id", value: ParentJobID },
			{ key: "@customer_id", value: customer_id }
		],
		sp_name: [{ key: "BSIT_ScheduleCalendarJobDropdownList" }]
		};

		var filter_list = "";
		filter_list += '<div class="mui-row" style="padding-bottom:15px;">';
		$.ajax({
		type: "POST",
		url: base_url + "core/Apilocal/Call_SP",
		data: load_job_search_data,
		dataType: "json",
		success: function(responseData) {
			
			//alert(JSON.stringify(responseData));
			var curr_date = new Date();
			var colorFg = "#000000";
			var colorBg = "#FFFFFF";
			var cnt = 0;

			if (responseData != "") {
			$.each(responseData, function(index, value) {
				var img_icon = "";
				var cust_name = "";
				var parent_job_title = "";

				if (
				value["jobs_in_event"] != "" &&
				value["jobs_in_event"] != null
				) {
				var jobs_in_event = value["jobs_in_event"].split(",");
				for (var i = 0; i < jobs_in_event.length; i++) {
					jobs_in_event[i] = parseInt(jobs_in_event[i]);
				}
				} else {
				var jobs_in_event = "";
				}

				if (value["name"].length > 125) {
				value["name"] = value["name"].substring(0, 125) + "...";
				}
				if (value["parent_job_name"].length > 75) {
				value["parent_job_name"] =
					value["parent_job_name"].substring(0, 75) + "...";
				}

				filter_list +=
				'<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
				value["jobId"] +
				'">';
				filter_list += '<span class="cust_color">';
				filter_list +=
				'<span class="job"><label>Job:</label><span class="job_card_title">' +
				value["name"] +
				"</span></span>";

				if ($.inArray(value["key_id"], jobs_in_event) < 0) {
				if (
					moment(curr_date).format("YYYY-MM-DD") ==
					moment(value["key_create_date"]).format("YYYY-MM-DD")
				) {
					// RESENT JOB USE (+) ICON
					img_icon =
					"<img src='" +
					base_url +
					"assets/core/images/plus.png' style='opacity: 0.3;'>";
				}
				} else {
				// JOB USED BY OTHER USERS THEN USE (clock) ICON
				img_icon =
					"<img src='" +
					base_url +
					"assets/core/images/history.png' style='opacity: 0.3;'>";
				}

				var colorJson = $.parseJSON(value["CustColor"]);
				if (typeof colorJson != "undefined") {
				colorBg = colorJson[0];
				colorFg = colorJson[1];
				}
				filter_list +=
				'<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
				value["ParentJobID"] +
				'">' +
				value["parent_job_name"] +
				"</span></span>";

				filter_list +=
				'<span class="icon"><span class="job_card_customer" data-cid="' +
				value["CustomerID"] +
				'" style="float: left;background-color:' +
				colorBg +
				";color:" +
				colorFg +
				';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
				value["CustName"] +
				"</span>" +
				img_icon +
				"</span>";
				filter_list +=
				'<span class="job_load_status" style="display:none;">' +
				value["Status"] +
				"</span>";
				filter_list += "</span>";
				filter_list += "</div>";

				cnt++;
				if (cnt % 4 == 0) {
				filter_list +=
					'</div><div class="mui-row" style="padding-bottom:15px;">';
				}
			});
			filter_list += "</div>";
			$("#job_list").html(filter_list);
			$(".job_card_loader").hide();
			$(".job_card_loader1").hide();
			} else {
			$("#job_list").html(filter_list);
			$(".job_card_loader").hide();
			$(".job_card_loader1").hide();
			}
		},
		error: function(e) {
			alert("Error: " + e);
		}
		});
	}
$("#selectEventCustomerId").change(function() {
	    	// alert(("#selectEventCustomerId").val());
	    	var jobId = $("#job_title").val();
	    	var customer_id = $("#selectEventCustomerId").val();
	    	var ParentJobID = $("#sel_parent_job_id").val();
	    	$(".job_card_loader").show();
	    	getDataForJobCardPanel(jobId, ParentJobID, customer_id);
	    });
	function getDataForJobCardPanelDefault(userID) {		
		console.log(userID);
		var save_event_data = {
		parameters: [{ key: "@userID", value: userID },{ key: "@opportunityStatus", value: 1 }],
		sp_name: [{ key: "BSIT_CalendarJobLists" }]
		};

		/* $(".job_card_loader").show();
			$(".job_card_loader1").show(); */
		//<div class="mui-row" style="padding-bottom:15px;">
		var filter_list = "";
		filter_list += '<div class="mui-row" style="padding-bottom:15px;">';

		$.ajax({
		type: "POST",
		url: base_url + "core/Apilocal/Call_SP",
		data: save_event_data,
		dataType: "json",
		success: function(resultsData) {
			var curr_date = new Date();
			var colorFg = "#000000";
			var colorBg = "#FFFFFF";
			var cnt = 0;

			//$(".loader").hide();

			if (resultsData != "") {
			//alert(JSON.stringify(resultsData));

			$.each(resultsData, function(index, value) {
				var img_icon = "";
				var cust_name = "";
				var parent_job_title = "";
				var jobs_in_event = "";

				/* if(value['jobs_in_event'] !='' && value['jobs_in_event'] != null)
								{
									var jobs_in_event = value['jobs_in_event'].split(',');
									for(var i=0;i<jobs_in_event.length;i++){
										jobs_in_event[i]=parseInt(jobs_in_event[i]);
									}
								} */

				if (value["title"].length > 125) {
				value["title"] = value["title"].substring(0, 125) + "...";
				}
				if (value["parent_title"].length > 75) {
				value["parent_title"] =
					value["parent_title"].substring(0, 75) + "...";
				}

				filter_list +=
				'<div class="mui-col-md-3 job_box cust1 mui-col-xs-12 mui-col-sm-6" id="job_box_' +
				value["_id"] +
				'">';
				filter_list += '<span class="cust_color">';
				filter_list +=
				'<span class="job"><label>Job:</label><span class="job_card_title">' +
				value["title"] +
				"</span></span>";

				/* if($.inArray(value['_id'], jobs_in_event) < 0){
									if(moment(curr_date).format('YYYY-MM-DD') == moment(value['key_create_date']).format('YYYY-MM-DD')){ // RESENT JOB USE (+) ICON
										img_icon = "<img src='"+base_url+"assets/images/plus.png' style='opacity: 0.3;'>";
									}
								} else{ // JOB USED BY OTHER USERS THEN USE (clock) ICON
										img_icon = "<img src='"+base_url+"assets/images/history.png' style='opacity: 0.3;'>";
								}	 */

				var colorJson = $.parseJSON(value["def_event_color"]);
				if (typeof colorJson != "undefined") {
				colorBg = colorJson[0];
				colorFg = colorJson[1];
				}

				filter_list +=
				'<span class="project"><label>Project:</label><span class="job_card_project" data-pid="' +
				value["parent_id"] +
				'">' +
				value["parent_title"] +
				"</span></span>";

				filter_list +=
				'<span class="icon"><span class="job_card_customer" data-cid="' +
				value["customer_id"] +
				'" style="float: left;background-color:' +
				colorBg +
				";color:" +
				colorFg +
				';padding: 0 5px 0px;border-radius: 3px;margin-right:5px;">' +
				value["customerName"] +
				"</span>" +
				img_icon +
				"</span>";
				filter_list +=
				'<span class="job_load_status" style="display:none;">' +
				value["status"] +
				"</span>";
				filter_list += "</span>";
				filter_list += "</div>";

				cnt++;
				if (cnt % 4 == 0) {
				filter_list +=
					'</div><div class="mui-row" style="padding-bottom:15px;">';
				}
			});
			filter_list += "</div>";
			$("#job_list").html(filter_list);
			$(".job_card_loader").hide();
			$(".job_card_loader1").hide();
			} else {
			$(".job_card_loader").hide();
			$(".job_card_loader1").hide();
			//$('#job_list').html(filter_list);
			}
		}
		});
	}

	function load_status_based_on_project_for_addJobModel(parent_job_id, selected_val = 0) {
		var status_data = get_status_from_parent_job(parent_job_id);
		//alert(selected_val);
		if (selected_val == 0) {
		var output = [];
		output.push('<option value=""></option>');
		$.each(status_data, function(key, value) {
			//output.push('<option value="'+ value['_id'] +'">'+ value['status_title'] +'</option>');
			output.push(
			'<option value="' +
				value["status_title"] +
				'">' +
				value["status_title"] +
				"</option>"
			);
		});
		$("#job_status").html(output.join(""));
		} else {
		var output = [];
		var selected = "";
		output.push('<option value=""></option>');
		$.each(status_data, function(key, value) {
			//alert(selected_val+'**'+value['status_title']);
			if (selected_val === value["status_title"]) {
			selected = "selected";
			} else {
			selected = "";
			}
			//output.push('<option value="'+ value['_id'] +'" '+selected+'>'+ value['status_title'] +'</option>');
			output.push(
			'<option value="' +
				value["status_title"] +
				'" ' +
				selected +
				">" +
				value["status_title"] +
				"</option>"
			);
		});
		$("#job_status").html(output.join(""));
		}
	}

	function set_project_value_basedon_job(parent_id, load_from_event = 0) {
		//alert(load_from_event);
		var result = "";
		var json_str = '[{"collID": 1, "filter": "jb._id = ' + parent_id + '"}]';
		$.ajax({
			type: "POST",
			url: base_url + "core/Apilocal/loadfiltered",
			async: false,
			data: { data: json_str },
			dataType: "json",
			success: function(returndata) {
				//alert(returndata);
				var udata_wt = "";
				if (returndata != null) {
					returndata1 = returndata[0]["Results"];
					//alert(JSON.stringify(returndata1));
					if (returndata1 != "") {
						var job_name = returndata1[0]["title"];
						if (load_from_event == 0) {
							//udata_wt = job_name;
							var cust_name = returndata1[0]["CustName"];
							udata_wt = cust_name + " - " + job_name;
						} else {
							udata_wt = job_name;
							/* var cust_name = returndata1[0]['CustName'];
											udata_wt = cust_name+' - '+job_name;	 */
						}
					}
				}
				result = udata_wt;
			},
			error: function(xx) {
				alert(xx.responseText);
			}
		});
		return result;
	}

	function load_status_based_on_project(parent_job_id, selected_val = 0) {
		var status_data = get_status_from_parent_job(parent_job_id);
		console.log(status_data);
		//alert(selected_val);
		if (selected_val == 0) {
			var output = [];
			output.push('<option value=""></option>');
			$.each(status_data, function(key, value) {
				//output.push('<option value="'+ value['_id'] +'">'+ value['status_title'] +'</option>');
				output.push(
				'<option value="' +
					value["status_title"] +
					'">' +
					value["status_title"] +
					"</option>"
				);
			});
			$("#status").html(output.join(""));
		} else {
			var output = [];
			var selected = "";
			output.push('<option value=""></option>');
			$.each(status_data, function(key, value) {
				//alert(selected_val+'**'+value['status_title']);
				if (selected_val === value["status_title"]) {
					selected = "selected";
				} else {
					selected = "";
				}
				//output.push('<option value="'+ value['_id'] +'" '+selected+'>'+ value['status_title'] +'</option>');
				output.push(
				'<option value="' +
					value["status_title"] +
					'" ' +
					selected +
					">" +
					value["status_title"] +
					"</option>"
				);
			});
			$("#status").html(output.join(""));
		}
	}

	function hideShowAvailJobs() {
		// loop each of the options and either hide or show, depending on the selected customer
		$("#selectEventJobId option").each(function() {
			// does this customer number match the one selected?
			var $thisCustNum = $(this).data("cust");
			var $selCustNum = $("#selectEventCustomerId").val();
			if ($thisCustNum == $selCustNum) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});

		// unselect the selected option
		$("#selectEventJobId option:selected").removeAttr("selected");
		$("#selectEventJobId").val("");
	}

	function eventTypeChange() {
		var newType = $("#eventType").val();
		//alert(newType);
		// if all day event, we need to change the hours of the job
		switch (newType) {
			case "allday":
				$("toDateSelector").prop("disabled", true);
				$("toTimeSelector").prop("disabled", true);
				// hide the material costs section
				$("#eventMaterialCosts").hide();
				$("#shared_event_div").show();
			break;
			case "material":
				$("toDateSelector").prop("disabled", true);
				$("toTimeSelector").prop("disabled", true);
				// show the material costs section
				$("#eventMaterialCosts").show();
			break;
			case "planned":
				$("#shared_event_div").show();
				$("#eventMaterialCosts").hide();
			break;
			default:
				$("toDateSelector").prop("disabled", false);
				$("toTimeSelector").prop("disabled", false);
				// hide the material costs section
				$("#eventMaterialCosts").hide();
			break;
		}
	}

	function get_event_duration(start_actual_time, end_actual_time) {
		start_actual_time = new Date(start_actual_time);
		end_actual_time = new Date(end_actual_time);

		var diff = end_actual_time - start_actual_time;

		var diffSeconds = diff / 1000;
		var HH = Math.floor(diffSeconds / 3600);
		var MM = Math.floor(diffSeconds % 3600) / 60;

		var formatted = (HH < 10 ? "0" + HH : HH) + ":" + (MM < 10 ? "0" + MM : MM);
		return formatted + " Mins";
	}

	function showHideAdvancedSearch() {
		// show advanced search arrow
		$("#advancedSearch").toggle();

		// change the down/up image
		$("#advancedSearchDownImg").toggle();
		$("#advancedSearchUpImg").toggle();
	}

	function get_status_from_parent_job(parent_job_id) {
		var result = "";
		var json_str =
		'[{"collID": 1, "filter": "jb._id = ' + parent_job_id + '"}]';
		$.ajax({
		type: "POST",
		url: base_url + "core/Apilocal/loadfiltered",
		async: false,
		data: { data: json_str },
		dataType: "json",
		beforeSend: function() {
			$(".loader").hide();
		},
		success: function(returndata1) {
			var workflow_id = returndata1[0]["Results"][0]["workflow_id"];
			if (workflow_id == null) {
			workflow_id = 1;
			}
			var parent_ststus = get_status_from_workflow_id(workflow_id);
			result = parent_ststus;
			//result = workflow_id;
			//alert(JSON.stringify(result));
		},
		error: function(xx) {
			alert(xx.responseText);
		}
		});
		return result;
	}

	function get_status_from_workflow_id(workflow_id) {
		var result = "";
		var json_str =
		'[{"collID": 45, "filter": "[job].workflow_type_id = ' +
		workflow_id +
		' "}]';
		$.ajax({
		type: "POST",
		url: base_url + "core/Apilocal/loadfiltered",
		async: false,
		data: { data: json_str },
		dataType: "json",
		beforeSend: function() {
			$(".loader").hide();
		},
		success: function(returndata1) {
			var udata_wt = returndata1[0]["Results"];
			//alert(JSON.stringify(udata_wt));
			result = udata_wt;
		},
		error: function(xx) {
			alert(xx.responseText);
		}
		});
		return result;
	}

});


$(document).ready(function(){
	$('#schedules_calendar_delete_job').click(function(){
		var txt;
		  var schedules_deleted_job = confirm("Are you sure you want to delete the selected events");
		  if (schedules_deleted_job == true) {
		  	$('#ifDelete').val();
		  	$('#schedules_calendar_delete_job').addClass('bsit_delete_file');		  	
		    txt = $('#ifDelete').val('deleted');
		  	$('#commit-event-addedit').click();
		  } else {
		  	$('#ifDelete').val();
		  	$('#schedules_calendar_delete_job').removeClass('bsit_delete_file');
		    txt = $('#ifDelete').val('planned');
		  }    
	});

	//06-08-2021 code
	//append td hourstrip to table
	/*jQuery ( ".fc-resource-area").find("td.fc-widget-content" ).after( '<td class="hrtd"><div class="hrstrip"><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">2h</span></div><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">4h</span></div><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">6h</span></div><div class="box firstt"><span class="htext"> </span></div><div class="box secondt"><span class="htext">8h</span></div></div></td>');*/
	
	//16-06-2021 set weekend color change
	setWeekendColor();
	//end

	// code for display rest hours with ajax 13-08-2021
	setRestHours();
	//end
	//check broken links for profile image
	checkBrokenProfileImg();


})
