<div class="chart_main">
  <div class="chart_head">
    <div class="chart_title">Details</div>
  </div>
  <div class="chart_body" id="myDetChart_container" style="height:400px; max-height:400px">
    <canvas id="myDetChart" style="width:100%; height:100%;"></canvas>
  </div>
</div>
