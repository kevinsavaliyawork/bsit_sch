<!--
.io_numReps
#id_TM_Rank
#id_TM_Budget
#repTerritoryMargin_container
#repTerritoryMarginPercChart
#id_TM_FooterVal1
-->

<div class="chart_main" id="repTerritoryMarginMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Territory Margin</div>
  </div>

  <!-- body -->
  <div class="chart_body" id="repTerritoryMarginBody_container">
      <!-- LEFT -->
      <div class="chart_metric">
        <div class="chart_primary">
          <span class="chart_primary_title">Rank:</span>
          <span class="chart_primary_value" id="id_TM_Rank">#</span>/<span class="io_numReps">#</span>
        </div>
        <div class="chart_scnd">
          <span class="chart_scnd_title">Margin:</span>
          <span class="chart_scnd_value"><strong id="id_TM_Budget">#%</strong>
        </div>
      </div>

      <!-- RIGHT -->
      <div class="chart_hero" id="repTerritoryMargin_container" style="height:400px; max-height:150px">
          <canvas id="repTerritoryMarginPercChart" style="width:100%; height:100%;"></canvas>
      </div>
  </div>

  <!-- Foooter -->
  <div class="chart_footer">
    <!-- LEFT -->
    <div class="chart_foot_1st">
      <span class="chart_foot_1st_title" >LY:</span>
      <span class="chart_foot_1st_value" id="id_TM_FooterVal1">#%</span>
    </div>
  </div>

</div>
