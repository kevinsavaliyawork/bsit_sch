
<?php

$dateOptions = "";
for ($i = 0; $i < 1; $i++) {
  $dateOptions .= '<option value="'.date('01 F Y', strtotime("-$i month")).'">'.date('F Y', strtotime("-$i month")).'</option>';
}

?>

<script>
$(function() {
  // don't call the resize until the user is finished.
  var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();
  // when the use resizes the window, fire the resize event
  $(window).resize(function () {
      waitForFinalEvent(function(){
        //alert('Resize...');
        align_row_charts();
      }, 500, "Window Resize Complete");
  });

  // refresh the drop down list of farms
  function refresh_farms_list(){
    var farmurl = "<?php echo base_url();?>Farmdata/loadFarmList";
    $.getJSON(farmurl, function(farm_list){
      // if we have results enable the submit and repopulate dropdown
      if(farm_list.length > 0){
        // clear options
        $('#farmSelector').html("");
        // Loop the list of farms and add options
        for(var i = 0; i < farm_list.length; i++) {
          $('#farmSelector').append($('<option>', {
              value: farm_list[i].id,
              text: farm_list[i].name
          }));
        }
        // enable submit
        $('#farmSelector').prop('disabled', false);;
      }
    });
  };

  // on submit of the dashboard parameters form
  $("#dashboardParams").submit(function(e){
    e.preventDefault();
    // get the values from the form
    var farmID = $('#farmSelector').val();
    var dateStart = $('#dateStartSelector').val();
    var pageParams = {farmID:farmID,dateStart:dateStart}
    // loop each of the frames and refresh
    var page_id 		= '<?php echo($pageId); ?>';

    //refresh each of the dashboard refresh_dashboard_elements
    refresh_dashboard_elements();
    /*refresh_dashbaord_frame_simple(page_id,54,pageParams);
    refresh_dashbaord_frame_simple(page_id,55,pageParams);
    refresh_dashbaord_frame_simple(page_id,56,pageParams);
    refresh_dashbaord_frame_simple(page_id,57,pageParams);
    refresh_dashbaord_frame_simple(page_id,58,pageParams);*/

    return false;
  });

  // when the page images etc are loaded, resize some frames.
  $(window).on("load", function() {
      align_row_charts();
  });

  // reload all frames after a page load or submit of parameters
  function refresh_dashboard_elements(){
    elec_chart_refresh();
    farm_details_refresh();
    farm_elecstat_refresh();
    farm_greenhouse_refresh();
    elec_location_pie_refresh();
    temp_chart_refresh();
  }

  function align_row_charts(){
    $frame1height = $('#page_frame_57_content > .chart_main').height();
    $frame2height = $('#page_frame_58_content > .chart_main').height();

    $frame1height += 10;
    $frame2height += 10;

    if($frame1height>$frame2height){
      $('#page_frame_58').height($frame1height);
      $('#page_frame_57').height($frame1height);
    }else{
      $('#page_frame_58').height($frame2height);
      $('#page_frame_57').height($frame2height);
    }
  }

  // refresh dashboard page frame simple
  function refresh_dashbaord_frame_simple(pageId,pageFrameId,pageParams){
    var pass_data_ajax = {'filter':"",'order':"",'page_num':1,'num_recs':1,'page_id':pageId,'frame_id':pageFrameId}
    jQuery.extend(pass_data_ajax,pageParams);
    var new_url = base_url+"Frame/load_content";
    $.ajax({
			type: "POST",
			data:  pass_data_ajax,
			url: new_url,
			success: function(returnData){
				$('#page_frame_'+pageFrameId+'_content').replaceWith(returnData);
      }
    });
  }

  // On button press to change period
  $(".elecChartPeriod").on("click", function(event) {
    var SelectedPeriod = $(this).val();
    $('#periodSelected').val(SelectedPeriod);
    $(".elecChartPeriod").css('color','#black');
    $(this).css('color','#blue');

    elec_chart_refresh();
  });

  var myElecChart=null;
  function elec_chart_refresh(){
    // load the farm and start date from the drop downs on the form
    var farmID = $('#farmSelector').val();
    var dateStart = $('#dateStartSelector').val();
    var groupPeriod = $('#periodSelected').val();

    // set the color of the button selected
    $(".elecChartPeriod").css('color','#000000');
    if(groupPeriod=="Hour"){
      $('#elec_btn_hour').css('color','#5800ff');
    }else if(groupPeriod=="Day"){
      $('#elec_btn_day').css('color','#5800ff');
    }else if(groupPeriod=="Week"){
      $('#elec_btn_week').css('color','#5800ff');
    }

    // get the parameters from the form
    var url = "<?php echo base_url();?>Farmdata/elecChart?farmID="+farmID+"&dateStart="+dateStart+"&groupPeriod="+groupPeriod;
    $.getJSON(url,function(data){
        console.log(arguments);

        // Get the results for display
        SensorData = data.Data;

        //  loop the data and put values into a structure we can use for display
        var labelsTimes =   [];
        var dataSolarValues =[];
        var dataGridValues =[];
        var dataPlantValues =[];
        for(i=0; i<SensorData.length; i++){
          var rowData = SensorData[i];
          labelsTimes.push(rowData.dateTimeOutput);
          dataSolarValues.push(rowData.avgSolarUse);
          dataGridValues.push(rowData.avgGridUse);
          dataPlantValues.push(rowData.avgPlantUse);
        }

    		var barChartData = {
    			labels: labelsTimes,
    			datasets: [{
    				label: 'Grid Consumption',
            stack:  'stack1',
    				//backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
    				borderColor: window.chartColors.red,
            backgroundColor: [ 'rgba(255, 99, 132, 0.2)'],
    				borderWidth: 1,
    				data: dataGridValues,
            fill: true

    			}, {
    				label: 'Solar Generation',
            stack:  'stack2',
    				//backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
    				borderColor: window.chartColors.blue,
            backgroundColor: [ 'rgba(54, 162, 235, 0.2)'],
    				borderWidth: 1,
    				data: dataSolarValues,
            fill: true
    			}, {
          	 label: 'Plant Use',
             stack:  'stack3',
             //backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
             borderColor: window.chartColors.green,
             backgroundColor: [ 'rgba(255, 206, 86, 0.6)'],
             borderWidth: 1,
             data: dataPlantValues,
             fill: true
         }]
    		};

        // clear the canvas of old charts and readload
        if(myElecChart != null){
          myElecChart.destroy();
        }
        myElecChart=null;
        // clear the HTML element and we will redraw to fix the height issue
        $("#elec_chart_container").html();
        $("#elec_chart_container").html('<canvas id="myElecChart" style="width:100%; height:100%;"></canvas>');

        // load the chart
        var ctx = document.getElementById("myElecChart").getContext('2d');
        myElecChart = new Chart(ctx, {
            type: 'line',
            data:barChartData,
            options: {
              maintainAspectRatio: false,
              responsive: true,
              scales: {
                xAxes: [{
                  type: 'time',
                  display: true,
                  ticks: {
                    major: {
                      fontStyle: 'bold',
                      fontColor: '#FF0000'
                    }
                  }
                }]
              }
        }
      });

        myElecChart.update();
    });
  };

  var myElecPieChart=null;
  function elec_location_pie_refresh(){
    // load the farm and start date from the drop downs on the form
    var farmID = $('#farmSelector').val();
    var dateStart = $('#dateStartSelector').val();

    // get the parameters from the form
    var url = "<?php echo base_url();?>Farmdata/elecUseByArea?farmID="+farmID+"&dateStart="+dateStart;
    $.getJSON(url,function(data){
        console.log('Elec Use By Area');
        console.log(arguments);

        // Get the results for display
        SensorData = data.Data;
        var rowData = SensorData[0];
        var PwrHeating = rowData.HeatingPower;
        var PwrCooling = rowData.CoolingPower;
        var PwrMilking = rowData.MilkingPower;
        var PwrOther = rowData.OtherPower;

    		var pieChartData = {
            datasets: [{
                data: [PwrHeating, PwrCooling, PwrMilking, PwrOther],
                borderColor: [ 'rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)'],
                backgroundColor: [ 'rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(187, 199, 185, 0.4)'],
                borderWidth: 1
            }],
            labels: ["Heating","Cooling","Milking","Other"]
        };

        // clear the canvas of old charts and readload
        if(myElecPieChart != null){
          myElecPieChart.destroy();
        }
        myElecPieChart=null;
        // clear the HTML element and we will redraw to fix the height issue
        $("#elec_pie_container").html();
        $("#elec_pie_container").html('<canvas id="myElecPieChart" style="width:100%; height:100%;"></canvas>');

        // load the chart
        var ctx = document.getElementById("myElecPieChart").getContext('2d');
        myElecPieChart = new Chart(ctx, {
            type: 'pie',
            data:pieChartData,
            options: {
              maintainAspectRatio: false,
              responsive: true,
              pieceLabel: {
                // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                render: 'percentage'
              }
            }
        });

        myElecPieChart.update();
    });
  };

  var myTempChart=null;
  function temp_chart_refresh(){
    // load the farm and start date from the drop downs on the form
    var farmID = $('#farmSelector').val();
    var dateStart = $('#dateStartSelector').val();

    // get the parameters from the form
    var url = "<?php echo base_url();?>Farmdata/temperature?farmID="+farmID;
    $.getJSON(url,function(data){
        console.log(arguments);

        // Get the results for display
        SensorData = data.Data;

        // Get the results for display
        SensorData = data.Data;
        var vatDates =[];
        var Milk_Vat_Temperature =[];
        var Chilled_Water_Temp =[];
        var Cool_Water_Temp =[];
        var Ambient_Temp =[];
        var Rinse_Drum_Temp =[];
        var Alkali_Drum_Temp =[];
        var Acid_Drum_Temp =[];

        for(i=0; i<SensorData.length; i++){
          var rowData = SensorData[i];
          vatDates.push(rowData.vatDates);
          Milk_Vat_Temperature.push(rowData.Milk_Vat_Temperature);
          Chilled_Water_Temp.push(rowData.Chilled_Water_Temp);
          Cool_Water_Temp.push(rowData.Cool_Water_Temp);
          Ambient_Temp.push(rowData.Ambient_Temp);
          Rinse_Drum_Temp.push(rowData.Rinse_Drum_Temp);
          Alkali_Drum_Temp.push(rowData.Alkali_Drum_Temp);
          Acid_Drum_Temp.push(rowData.Acid_Drum_Temp);

        }

    		var tempChartData = {
            datasets: [
              {
                label: 'Milk Vat',
                data: Milk_Vat_Temperature,
                borderColor: window.chartColors.blue,
                backgroundColor: window.chartColors.blue,
                fill: false
              },
              {
                label: 'Chilled Water',
                data: Chilled_Water_Temp,
                borderColor: window.chartColors.red,
                backgroundColor: window.chartColors.red,
                fill: false
              },
              {
                label: 'Cool Water',
                data: Cool_Water_Temp,
                borderColor: window.chartColors.green,
                backgroundColor: window.chartColors.green,
                fill: false
              },
              {
                label: 'Ambient',
                data: Ambient_Temp,
                borderColor: window.chartColors.yellow,
                backgroundColor: window.chartColors.yellow,
                fill: false
              },
              {
                label: 'Rinse Drum',
                data: Rinse_Drum_Temp,
                borderColor: window.chartColors.pink,
                backgroundColor: window.chartColors.pink,
                fill: false
              },
              {
                label: 'Alkali Drum',
                data: Alkali_Drum_Temp,
                borderColor: window.chartColors.purple,
                backgroundColor: window.chartColors.purple,
                fill: false
              },
              {
                label: 'Acid Drum',
                data: Acid_Drum_Temp,
                borderColor: window.chartColors.orange,
                backgroundColor: window.chartColors.orange,
                fill: false
              }
            ],
            labels: vatDates
        };

        // clear the canvas of old charts and readload
        if(myTempChart != null){
          myTempChart.destroy();
        }
        myTempChart=null;
        // clear the HTML element and we will redraw to fix the height issue
        $("#temp_chart_container").html();
        $("#temp_chart_container").html('<canvas id="myTempChart" style="width:100%; height:100%;"></canvas>');

        // load the chart
        var ctx = document.getElementById("myTempChart").getContext('2d');
        myTempChart = new Chart(ctx, {
          type: 'line',
          data:tempChartData,
          options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
              xAxes: [{
                type: 'time',
                display: true,
                ticks: {
                  major: {
                    fontStyle: 'bold',
                    fontColor: '#FF0000'
                  }
                }
              }]
            }
        }
      });

        myTempChart.update();
    });
  };

  // Farm details refresh
  function farm_details_refresh(){
    // load the farm and start date from the drop downs on the form
    var farmID = $('#farmSelector').val();
    var dateStart = $('#dateStartSelector').val();

    // get the parameters from the form
    var url = "<?php echo base_url();?>Farmdata/farmDetails?farmID="+farmID+"&dateStart="+dateStart;
    $.getJSON(url,function(data){
        console.log('Farm Details');
        console.log(arguments);

        // Get the results for display
        SensorData = data.Data;

        //  loop the data and put values into a structure we can use for display
        var rowData = null;
        rowData = SensorData[0];

        // map the sensor data to variables for display
        ltrPerDayMin        = rowData.ltrPerDayMin;
        ltrPerDayAve        = rowData.ltrPerDayAve;
        ltrPerDayMax        = rowData.ltrPerDayMax;
        tariffRatePeak      = rowData.tariffRatePeak;
        tariffRateOffPeak   = rowData.tariffRateOffPeak;
        dailySupplyCharge   = rowData.dailySupplyCharge;
        elecChargeDesc      = rowData.elecChargeDesc;
        farmDescription     = rowData.farmDescription;
        dairyNumberOfCows   = rowData.dairyNumberOfCows;
        dairyDescription    = rowData.dairyDescription;

        // update some variables with formatting
        dailySupplyCharge = "$"+dailySupplyCharge;

        // push the values into the page
        $('#ltrPerDayMin').html(ltrPerDayMin);
        $('#ltrPerDayAve').html(ltrPerDayAve);
        $('#ltrPerDayMax').html(ltrPerDayMax);
        $('#tariffRatePeak').html(tariffRatePeak);
        $('#tariffRateOffPeak').html(tariffRateOffPeak);
        $('#dailySupplyCharge').html(dailySupplyCharge);
        $('#elecChargeDesc').html(elecChargeDesc);
        $('#farmDescription').html(farmDescription);
        $('#dairyNumberOfCows').html(dairyNumberOfCows);
        $('#dairyDescription').html(dairyDescription);
    });
  };

  // elect stat refresh
  function farm_elecstat_refresh(){
    // load the farm and start date from the drop downs on the form
    var farmID = $('#farmSelector').val();
    var dateStart = $('#dateStartSelector').val();

    // get the parameters from the form
    var url = "<?php echo base_url();?>Farmdata/elecStats?farmID="+farmID+"&dateStart="+dateStart;
    $.getJSON(url,function(data){
        console.log('Elect Stat');
        console.log(arguments);

        // Get the results for display
        SensorData = data.Data;

        //  loop the data and put values into a structure we can use for display
        var rowData = null;
        rowData = SensorData[0];

        // map the sensor data to variables for display
        elecKwPerLt        = rowData.elecKwPerLt;
        elecDollarPerLt        = rowData.elecDollarPerLt;
        elecKwPerDay        = rowData.elecKwPerDay;
        elecDollarPerDay      = rowData.elecDollarPerDay;

        // push the values into the page
        $('#elecKwPerLt').html(elecKwPerLt);
        $('#elecDollarPerLt').html(elecDollarPerLt);
        $('#elecKwPerDay').html(elecKwPerDay);
        $('#elecDollarPerDay').html(elecDollarPerDay);

    });
  };

  // greenhouse gas refresh
  function farm_greenhouse_refresh(){
    // load the farm and start date from the drop downs on the form
    var farmID = $('#farmSelector').val();
    var dateStart = $('#dateStartSelector').val();

    // get the parameters from the form
    var url = "<?php echo base_url();?>Farmdata/emissions?farmID="+farmID+"&dateStart="+dateStart;
    $.getJSON(url,function(data){
        console.log('Elect Stat');
        console.log(arguments);

        // Get the results for display
        SensorData = data.Data;

        //  loop the data and put values into a structure we can use for display
        var rowData = null;
        rowData = SensorData[0];

        // map the sensor data to variables for display
        greenhouseGrid        = rowData.greenhouseGrid;
        greenhouseSolar       = rowData.greenhouseSolar;

        // push the values into the page
        $('#greenhouseGrid').html(greenhouseGrid);
        $('#greenhouseSolar').html(greenhouseSolar);
    });
  };

  // refresh the farm farm_list
  refresh_farms_list();
  // refresh all chart data on initial load
  refresh_dashboard_elements();
  // hide the side redraw
  $("#sidedrawer").parent().toggleClass('hide-sidedrawer');

});
</script>

<div class="dashboard-title secondary_bg_color">
  Individual Farm Dashboard
</div>
<div class="secondary_bg_color">
  <form class="dashboard-params-main" id="dashboardParams">
    <div class="dashboard-param">
      <label>Farm</label>
      <select id="farmSelector">
        <option value="1">
          Loading...
        </option>
      </select>
    </div>
    <div class="dashboard-param">
      <label>Date</label>
      <select id="dateStartSelector">
        <?php echo($dateOptions)?>
      </select>
    </div>
    <button type="submit" id="dashboardSubmit" class="dashboard-btn">Submit</button>
  </form>
</div>
