// jQuery document
$( document ).ready(function() {
	// --------------------------------
	// ROW COUNTER UPDATE
	// --------------------------------
	// If a new row count is selected, update the form hidden filed
  	$('#analysisDataContainer').on('change', '#rowCntSelector', function() {
		var $value = $(this).val();
		$('#analysisForm input[name="rows"]').val($value);
		$('#analysisForm input[name="page"]').val(0);
		
		// submit the form
		$('#analysisForm').submit();
	});
	
	// --------------------------------
	// PAGE NUMBER UPDATE
	// --------------------------------
	// If the users selected next, increment the page number
	$('#analysisDataContainer').on('click', '#nextPageSelector', function($e) {
		// stop the scrolling
		$e.preventDefault();
		
		// get the current page number
		var $value = +$('#analysisForm input[name="page"]').val() + 1;
		$('#analysisForm input[name="page"]').val($value);
		
		// submit the form
		$('#analysisForm').submit();
	});
	// If the users selected next, increment the page number
	$('#analysisDataContainer').on('click', '#prevPageSelector', function($e) {
		// stop the scrolling
		$e.preventDefault();
		
		// get the current page number
		var $value = +$('#analysisForm input[name="page"]').val() - 1;
		$('#analysisForm input[name="page"]').val($value);
		
		// submit the form
		$('#analysisForm').submit();
	});
	
	
	// --------------------------------
	// SORT COLUMNS
	// --------------------------------
	$('#analysisDataContainer').on('click', 'a.sortColSelector', function($e) {
		// stop the scrolling
		$e.preventDefault();
		
		// get the current page number
		var $value = $(this).attr('data-sort');
		$('#analysisForm input[name="sort"]').val($value);
		// reset page number to zero
		$('#analysisForm input[name="page"]').val(0);
		
		// submit the form
		$('#analysisForm').submit();
	});
	
	// --------------------------------
	// CHANGE GROUP BY
	// --------------------------------
	$('#analysisDataContainer').on('change', '#groupBySelector', function() {
		// reset the sort to empty
		$('#analysisForm input[name="sort"]').val('');
		
		// reset page number to zero
		$('#analysisForm input[name="page"]').val(0);
		
		// submit the form
		$('#analysisForm').submit();
	});
	
	// --------------------------------
	// MEASURE SELECT
	// --------------------------------
	// If the users selected a measure, add to list of selected measures
	$(".measureSelector").click(function() {
		// Get the ID of the selected measure
		var $value 	= $(this).attr('data-measureid');
		var $name 	= $(this).attr('data-measurename');
		var addMea	= true
		
		// loop all the measures, if this one already exists make it dance!
		var curMeasureID		= '';
		$("#measuresSelectedList ul li.measItem").each(function() {
			// get the data needed; ID, value and opp
			curMeasureID		= $(this).attr('data-measureid');
			
			// check if this (in loop) is the same measure we are about to add
			if(curMeasureID === $value){
				$(this).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
				addMea = false;
			}
		});
		
		// should we add the measure?
		if(addMea===true){
			// add another item to the list of options
			$("#measSelectedUL").append('<li class="measItem" data-measureid="'+$value+'">'+$name+'<a class="measureRemover" href="javascript:void(0)">x</a></li>');			
		}
		
		// update the matrix option
		matrixEnableDisable();
	});
	
	
	// --------------------------------
	// MEASURE REMOVE
	// --------------------------------
	// If the users selected a measure, add to list of selected measures
	$('#measuresSelectedList ul').on('click', 'li a.measureRemover', function() {
		// remove the li from the list
		$(this).parent().remove();		
		
		// update the matrix option
		matrixEnableDisable();
	});
	
	// --------------------------------
	// GROUP BY SELECT
	// --------------------------------
	// If the users selected a group by, set it as the selected group by
	$(".groupBySelector").click(function() {
		// Get the ID and name of the selected group by
		var $value 	= $(this).attr('data-dimid');
		var $name 	= $(this).attr('data-dimname');
		
		// firstly, we must check how many entries we have in the group by list
		// 1, move to the add, elseif 2, we must remove last before adding new entry
		var numberGroupBys			= $('#GroupBySelectedUL li.groupBySelected').length;
		var numberGroupBysAllowed	=	2;
		if(numberGroupBys >= numberGroupBysAllowed){
			$('#GroupBySelectedUL li').last().remove();
		}
		
		// append a new entry to the list
		$("#GroupBySelectedUL").append('<li class="groupBySelected" data-groupbyid="'+$value+'">'+$name+'<a class="groupRemover" href="javascript:void(0)">x</a></li>');

		// reset the sort to empty
		$('#analysisForm input[name="sort"]').val('');
		
		// reset page number to zero
		$('#analysisForm input[name="page"]').val(0);
		
		// update the matrix option
		matrixEnableDisable();
	});
	
	// --------------------------------
	// GROUP BY REMOVE
	// --------------------------------
	$('#GroupBySelectedUL').on('click', 'li.groupBySelected a.groupRemover', function() {
		// get parent values (the one clicked on)
		$(this).parent().remove();
		
		// update the matrix option
		matrixEnableDisable();
	});	
	
	// --------------------------------
	// MATRIX ENABLE/DISABLE
	// --------------------------------
	function matrixEnableDisable(){
		// count the number of measures selected
		var meaCount = $("#measuresSelectedList ul li.measItem").length;
		// count the number of group bys selected
		var grpCount = $('#GroupBySelectedUL li.groupBySelected').length;
		
		// if we have 2 group bys and at least 1 measure, enable the matrix
		if(meaCount > 0 && grpCount === 2){
			// enable matrix	
			$('#matrixCheckbox').removeAttr('disabled');
		}else{
			// disable matrix option
			$('#matrixCheckbox').attr('checked', false);
			$('#matrixCheckbox').attr('disabled', true);
		}
			
	}
	
	// --------------------------------
	// SUBMIT - Called when form is submitted
	// --------------------------------
	$('#analysisForm').submit(function() {
		// put the filters into a input field for submit
		setupFiltersJson();
		// put the group bys into the form for submit
		var grpCnt = setupGroupBys();
		// put the measures into the form for submit
		var measCnt = setupMeasures();
		// check we have some measures or group bys
		if(measCnt > 0 || grpCnt > 0){
			// check if the output should be CSV (1), or to the display (0)
			if($('#outputCsvFlag').val() == 0){
				// show the overlay
				$('#analysisDataContOverlay').fadeIn();
			
				// now we need to send the submit request to a PHP page
				// then load the generated HTML into the display section.
				$.ajax({ // create an AJAX call...
					data: $(this).serialize(), // get the form data
					type: $(this).attr('GET'), // GET or POST
					url: $(this).attr('action'), // the file to call
					success: function(response) { // on success..
						$('#analysisDataContainer').html(response); // update the DIV
						// remove the overlay
						$('#analysisDataContOverlay').fadeOut();
					}
				});
				
				// cancel original event to prevent form submitting
				return false; 
			}
		}else{
			// cancel original event to prevent form submitting
			return false; 	
		}
		
		// submit the form, this will produce CSV!!!
		
	});	
	// function used to setup the list of measures
	function setupMeasures(){
		var measureId = 0;
		var measCount = 0;
		// empty the list of selected lis
		$('#measureHiddenSelector').empty();
		// loop each of the selected li's
		$('#measSelectedUL li.measItem').each( function(){
			measCount++;
			measureId 	= $(this).attr('data-measureid');
			// append a new entry to the form list
			$("#measureHiddenSelector").append('<option value="'+measureId+'" selected="selected"></option>');
		});
		return measCount;
	}
	// function used to setup the list of group bys
	function setupGroupBys(){
		var groupById = 0;
		var grpCount = 0;
		// empty the list of selected lis
		$('#groupBySelector').empty();
		// loop each of the selected li's
		$('#GroupBySelectedUL li.groupBySelected').each( function(){
			grpCount++;
			groupById 	= $(this).attr('data-groupbyid');
			// append a new entry to the form list
			$("#groupBySelector").append('<option value="'+groupById+'" selected="selected"></option>');
		});
		return grpCount;
	}
	// function used to get the current filtes and return for performing analysis
	function setupFiltersJson(){
		// setup needed vars
		var $dimID			='';
		var $dimValue		='';
		var $dimName 		='';
		var $dimOpp			='';
		var $dimPerm		='';
		var filterObject 	= {};
		var $valArray;
		var $dimInFilterObject = false;
		// here we need to loop the list of filters and update the filters input before submit
		// add each filter to the array one to an array
		$("#dimSelectedUL li").each(function() {
			// get the data needed; ID, value and name
			$dimID		= $(this).data('dimid');
			$dimValue	= $(this).data('dimvalue');
			$dimOpp		= $(this).data('dimopp');
			$dimPerm	= $(this).data('dimperm');
			$dimName	= $(this).data('dimname');
			
			// check if this dim is already in filter object
			$dimInFilterObject = $dimID in filterObject;
			if ($dimInFilterObject == false){
				filterObject[$dimID] = [];
			}
			
			// add the value to the objects array
			$valArray = [$dimValue,$dimOpp,$dimPerm];
			filterObject[$dimID].push($valArray);
			
		});
		
		var $jsonString = JSON.stringify(filterObject);
		$('#filtersHiddenSelector').val($jsonString);
		
		return true;
	}

	// --------------------------------
	// DRILL THROUGH
	// --------------------------------
	$('#analysisDataContainer').on('click', '#analysisTableCont table a.drillInSel', function() {
		// get all the needed values from the form
		var value 			= $(this).attr('data-value');
		var dimId 			= $(this).attr('data-dimid');
		var dimName			= $(this).attr('data-dimname');
		var grpColNum 		= $(this).attr('data-grpcolnum'); // 1st or 2nd group by
		var grpChildId		= $(this).attr('data-grpchildid');
		var grpChildName	= $(this).attr('data-grpchildname');
		var lookupValue		= $(this).attr('data-valLookup');
		var filterCreated	= 0;
		
		// set the background of the parent to selected
		$(this).parent().fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
		//$(this).parent().css( "background-color","#92CDA7")
		
		// get the value for the filter - (allowing space for ajax call here, not yet known if required)
		if(lookupValue === '1'){
			$.ajax({
			  url: '/filters/getDimValue',
			  type: 'GET',
			  data: {'searchTerm': value,'dimID':dimId},
			  cache: false,
			  dataType : 'json',
			  success: function(data) {
				// -------------------------------
				// create the filter with new value
				// -------------------------------
				value = data.value;
				filterCreated = addFilter(dimId,value,dimName,'=');
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr + "\n" + err);
			  }
			});
			
		}else{
			// -------------------------------
			// create the new filter
			// -------------------------------
			filterCreated = addFilter(dimId,value,dimName,'=');
		};
		
		// -------------------------------
		// update group by column
		// -------------------------------
		var createNewGroupBy 	= true;
		var curGroupById		= '';
		// check we have a valid drill into, otherwise we wont change
		if(grpChildId != ''){
			// check if child already exists
			$("#GroupBySelectedUL li.groupBySelected").each(function() {
				curGroupById	= $(this).attr("data-groupbyid");
				
				// if the group by already exists, we dont need to create
				if(curGroupById == grpChildId){
					createNewGroupBy = false;
				}
			});
			
			// if needed create the new group by
			if(createNewGroupBy == true){
				// before the update of anything, connect to the correct div
				var grpSelector = "ul#GroupBySelectedUL li.groupBySelected:eq("+(grpColNum-1)+")";
				$(grpSelector).attr("data-groupbyid",grpChildId);
				$(grpSelector).html(grpChildName+'<a class="groupRemover" href="javascript:void(0)">x</a>');
			}
		}
		
		return false; // cancel original event to prevent url redirecting
		
	});	

	// --------------------------------
	// CSV DOWNLOAD
	// --------------------------------
	$('#analysisDataContainer').on('click', '#downloadCSV', function() {
		// set the csv flag to selected
		$('#outputCsvFlag').val(1);
		
		// set the form to open in new window
		$('#analysisForm').attr("target", "_blank");
		
		// submit the form
		$('#analysisForm').submit();
		
		// set the csv flag to selected
		$('#outputCsvFlag').val(0);
		
		return false; // cancel original event to prevent url redirecting
		
	});	
	
	// --------------------------------
	// DRAG AND DROP THE MEASURES & GROUP BY
	// --------------------------------
	$( "#measSelectedUL" ).sortable({
		items: "li.measItem",
		revert: true,
		cursor: "move"
    });
	$( "#GroupBySelectedUL" ).sortable({
		items: "li.groupBySelected",
		revert: true,
		cursor: "move"
    });
});
