<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|-----------------------------------------------------------------------------
| Set BASEURL for Calling the APIs & IMAGES
|-----------------------------------------------------------------------------
*/


const API_BASEURL = "http://tt.brainstormit.com.au/bsit_api/api/";
const API_METHOD_GET = "methodcollection/get";
const API_EXTERANAL_GET = "externalapi/getdata";


const API_COLLECTION_GET = "tableCollection/Get";
const API_COLLECTION_POST= "TableCollection/Post";
const API_COLLECTION_DELETE = "TableCollection/Delete";
const BASEURL_IMAGE 	= "http://localhost/bsit_sch/assets/core/images";
const SITEURL 			= "http://localhost/bsit_sch/";

const APP_KEY 			= "TTSTG_API";
const APP_PASSPHRASE 	= "TT@BSIT_API";
const APP_ROOT 			= "http://23.97.53.84";

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
defined('DELETE_FAIL')      OR define('DELETE_FAIL', "Error occurred while deleting the record."); // generic message of delete
defined('UPDATE_FAIL')      OR define('UPDATE_FAIL', "Error occurred while updating the record."); // generic message of update
defined('CREATE_FAIL')      OR define('CREATE_FAIL', "Error occurred while saving the record."); // generic message of create
defined('SELECT_FAIL')      OR define('SELECT_FAIL', "Error occurred while fetching the record."); // generic message of create
defined('DETAIL_ERROR_MESSAGE')      OR define('DETAIL_ERROR_MESSAGE', 1); // set 1 for display detail message from api otherwise display generic message

defined('PROPERTY_COLLECTION')      OR define('PROPERTY_COLLECTION','data_properties');
defined('TABLE_COLLECTION')      OR define('TABLE_COLLECTION','data_tables');
defined('COLL_COLLECTION')      OR define('COLL_COLLECTION','data_collections');
defined('PROPERTY_FRAMEID')      OR define('PROPERTY_FRAMEID','properties');
defined('TABLE_FRAMEID')      OR define('TABLE_FRAMEID','tables');
defined('COLL_FRAMEID')      OR define('COLL_FRAMEID','data_collections');


defined('PROPERTY_OTHER_COLLECTION')      OR define('PROPERTY_OTHER_COLLECTION','frame_properties');
defined('TABLE_OTHER_COLLECTION')      OR define('TABLE_OTHER_COLLECTION','frames');
defined('COLL_OTHER_COLLECTION')      OR define('COLL_OTHER_COLLECTION','page_frame');
defined('PROPERTY_OTHER_FRAMEID')      OR define('PROPERTY_OTHER_FRAMEID','frame_properties');
defined('TABLE_OTHER_FRAMEID')      OR define('TABLE_OTHER_FRAMEID','frames');
defined('COLL_OTHER_FRAMEID')      OR define('COLL_OTHER_FRAMEID','page_frames');
defined('DEFAULT_COLLECTION_PAGEID')      OR define('DEFAULT_COLLECTION_PAGEID','pages');

defined('SYS_APPLICATION_TITLE')      OR define('SYS_APPLICATION_TITLE','application_title');
defined('SYS_APPLICATION_ICON')      OR define('SYS_APPLICATION_ICON','application_icon');
defined('SYS_LOGIN_ICON')      OR define('SYS_LOGIN_ICON','login_icon');
defined('SYS_CSS_OVERRIDE')      OR define('SYS_CSS_OVERRIDE','css_override');

defined('SYS_MAX_LOGIN_ATTEMPTS')      OR define('SYS_MAX_LOGIN_ATTEMPTS','max_login_attempts');
defined('SYS_TIME_EXPIRE_RESET_PASSWORD_LINK')      OR define('SYS_TIME_EXPIRE_RESET_PASSWORD_LINK','time_expire_reset_password_link');
defined('SITE_REF')      OR define('SITE_REF','APG_WMS');
defined('SYS_SESSION_EXPIRATION_TIME')      OR define('SYS_SESSION_EXPIRATION_TIME',7200); //2 hours
