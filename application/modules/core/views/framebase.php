<?php
$addprop = '';
$framestart = '';

if ($enableTabbedFrames == 1) {
    if ($count_frame == $totalFrame) {
        $addprop = '</div></div><div class="propertyData"></div>';
    }
} else {
    if ($count_frame == $totalFrame) {
        $addprop = '</div><div class="propertyData"></div></div>';
    }
}
if ($count_frame == 1) {
    $framestart = '<div id="all_frame_data"><div class="frame_start">';


    if ($enableTabbedFrames == 1) {
        $query = $_SERVER['QUERY_STRING'];

        if ($frames[$frameID]['PFID'] == $frameID) {
            $selecteds = 'active';
        }

        // echo $actual_link; 
        // echo $query;
		echo $mainTitle='<div class="FrameMainTitle"></div>'; 

        $framestart .= '<div class="bsit_cola_tab" style="padding: 0 15px 0 15px; border:0;" ><ul class="nav nav-tabs nav-tabs-dropdown" id="menu-nav">';
        $has_data_loaded = 0;
        foreach ($frames as $frame) {

            if ($query == '') {

                if ($frames[$frameID]['PFID'] == $frame['PFID']) {
                    $active = 'active';
                } else {
                    $active = '';
                }
            } else {

                if ($query == $frame['PFID']) {
                    $active = 'active';
                } else {
                    $active = '';
                }
            }

            $active = ($has_data_loaded == 0) ? 'active' : $active;
            
            // $framestart .= '<li class="'.$active.'" style=" margin-bottom: 0;"><a data-toggle="tab" id="?'.$frame['PFID'].'" href="#main_content_part'.$frame['PFID'].'">'.$frame['FrameTitle'].'</a></li>';

            $framestart .= '<li class="' . $active . ' bsit-page-frame" data-pfid="' . $frame['PFID'] . '" style=" min-height: auto;margin-bottom: 0;"><a  has_data_loaded="' . $has_data_loaded . '" class="alltab" data-toggle="tab" id="?' . $frame['PFID'] . '" PFID="' . $frame['PFID'] . '" pageid = "' . $pageId . '" href="#main_content_part' . $frame['PFID'] . '">' . $frame['FrameTitle'] . '</a></li>';
            $has_data_loaded++;
        }

        $framestart .= '</ul></div><div class="tab-content" id="opprtunityViewTab">';
    } else {
    }
}


if ($_GET['headerPage'] == 1) {
    $addcss = "padding: 0px!important;margin-bottom: 20px!important;box-shadow: none!important;";
}


echo ($framestart . '
 <div id="main_content_part' . $frameID . '" class="main_content_part_class mui-col-xs-' . $frame_width . ' mui-col-xs-offset-' . $frame_offset . ' tab-pane fade in ' . $selecteds . '"
    style="position:relative; height:' . $frame_height . 'px; margin-bottom:20px;"  data-frameTitle="' . $FrameTitle . '" data-frameWidth="' . $frame_width . '"
    data-frameHeight="' . $frame_height . '" data-frameOffset="' . $frame_offset . '" data-frameOrder="' . $order . '" data-frameID="' . $frameID . '" pageId="' . $pageId . '" collection_id="' . $collection_id . '" frameID="' . $pageFrameId . '" frameName="' . $frameName . '"
    data-frameHideAdd="' . $hide_add . '" data-frameHideFilter="' . $hide_filter . '" data-frameHidePagination="' . $hide_pagination . '"
    data-frameHideDownload="' . $hide_allow_download . '" data-frameHideMultiEdit="' . $hide_multiple_edit . '" data-frameHideMultiDelete="' . $hide_multiple_delete . '" ">

    <div class="bsit-page-frame edit-pointer-cancel table-responsive mui-panel" id="page_frame_' .
    $frameID . '" style="height:' . $frame_height . 'px;' . $addcss . '" data-PFID="' . $frameID . '" >
        <input type="hidden" class="filter_history_api_class" id="filter_history_api_' . $frameID . '" value="" >
        <input type="hidden" class="page_type" id="page_type_' . $frameID . '" value="' . $page_type . '" >
         <input type="hidden" class="" id="default_page_size_' . $frameID . '" value="' . $default_page_size . '" >
        <input type="hidden" class="page_category_used_for_url" value="' . $page_category_used_for_url . '" >
        <input type="hidden" class="editParms" value="' . $editParms . '" >
        <input type="hidden" class="filter_history_api_class_state" id="filter_history_api_state_' . $frameID . '" value="" >
        <input type="hidden" class="history_api_data" id="history_api_data_' . $frameID . '" value="" >
        <input type="hidden" id="fram_prop_filter_' . $frameID . '" value="' . $filter . '">

        ' . $frame_content . '

    </div>
    <div id="' . $frameID . '" class="edit-div-overlay">
    </div>
</div>' . $addprop);

if ($enableTabbedFrames == 1) {
    if ($count_frame == $totalFrame) {
        $addprop = '</div>';
    }
}
