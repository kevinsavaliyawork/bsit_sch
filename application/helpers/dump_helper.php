<?php
/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author
 * @version 1.0
 */
if (!function_exists('dump')) {
    function dump($var, $label = 'Dump', $echo = true)
    {
        // Store dump in variable
        ob_start();
        print_r($var);
        $output = ob_get_clean();

        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre>' . $label . ' => ' . $output . '</pre>';

        // Output
        if ($echo == true) {
            echo $output;
        } else {
            return $output;
        }
    }

    function ausdate_to_datetime($ausdate)
    {
        $time = strtotime($ausdate);
        $datetime = date('Y-m-d H:i:s', $time);
        return $datetime;
    }

    function debug($var, $usevardump = false)
    {
        echo '<pre>';
        if ($usevardump) {
            var_dump($var);
        } else {
            print_r($var);
        }
        echo '</pre>';
    }

    function date_converter($date)
    {
        $exploded_date = explode('/', $date);

        $imploded_date = $exploded_date[2]."-".$exploded_date[1]."-".$exploded_date[0];

        return $imploded_date;
    }

    function au_date_to_mysql($date)
    {
        $exploded_date = explode('/', $date);

        $imploded_date = $exploded_date[2]."-".$exploded_date[0]."-".$exploded_date[1];

        return $imploded_date;
    }

    function au_date_to_unix($date_str)
    {
        $date_bits = explode('/', $date_str);
        if (count($date_bits) != 3) {
            return 0;
        } else {
            return mktime(0, 0, 0, $date_bits[1], $date_bits[0], $date_bits[2]);
        }
    }

    /**
     * Converts a unix timstampto an australian formatted date string.
     *
     * @param integer $unix_date
     * @return string Australian formatted date.
     */
    function unix_date_to_au($unix_date)
    {
        return date('d/m/Y', $unix_date);
    }

    function atom_to_aus_date($date)
    {
        $dates = current(explode('T', $date));
        $dat = explode('-', $dates);
        return $dat[2].'/'.$dat[1].'/'.$dat['0'];
    }

    /**
     * Converts an Australian formatted date/time string (d/m/y h:m:s) to a unix timestamp.
     *
     * @param string $datetime_str
     * @return integer A unix timestamp.
     */
    function au_datetime_to_unix($datetime_str)
    {
        $datetime_bits = explode(' ', $datetime_str);
        if (count($datetime_bits) != 2) {
            return 0;
        } else {
            $date_bits = explode('/', $datetime_bits[0]);
            if (count($date_bits) != 3) {
                return 0;
            } else {
                $time_bits = explode(':', $datetime_bits[1]);
                if (count($time_bits) != 2 && count($time_bits) != 3) {
                    return 0;
                } else {
                    if (count($time_bits) == 2) {
                        $time_bits[2] = 0;
                    }
                    return mktime($time_bits[0], $time_bits[1], $time_bits[2], $date_bits[1], $date_bits[0], $date_bits[2]);
                }
            }
        }
    }

    /**
     * Takes a unix timestamp and converts it to an australian formatted string including time.
     *
     * @param integer $unix_datetime
     * @return string An australian formatted string.
     */
    function unix_datetime_to_au($unix_datetime)
    {
        return date('d/m/Y H:i:s', $unix_datetime);
    }

    /**
     * Accepts a short date (m/y) and returns a Unix timestamp representing midnight on the 1st of that month.
     *
     * @param string $date_str
     * @return integer A unix timestamp
     */
    function au_short_date_to_unix($date_str)
    {
        $date_bits = explode('/', $date_str);

        if (count($date_bits) != 2) {
            return 0;
        } else {
            return mktime(0, 0, 0, $date_bits[0], 1, $date_bits[1]);
        }
    }

    /**
     * Accepts a unix timestamp and returns an australian short (m/y) date string.
     *
     * @param integer $unix_date
     * @return string A short date string.
     */
    function unix_date_to_au_short($unix_date)
    {
        return date('m/Y', $unix_date);
    }

    /**
     * Accepts a Unix timestamp and returns a string formatted ready for MySQL databases
     * containing only the date portion.
     *
     * @param integer $unix_date
     * @return string A string ready for MySQL
     */
    function unix_date_to_mysql($unix_date)
    {
        return date('Y-m-d', $unix_date);
    }

    /**
     * Accepts a Unix timestamp and returns a string formatted ready for MySQL databases
     * containing both date and time portions.
     *
     * @param integer $unix_date
     * @return string A string ready for MySQL
     */
    function unix_datetime_to_mysql($unix_date)
    {
        return date('Y-m-d H:i:s', $unix_date);
    }

    function start_of_day($unix_time)
    {
        $bits = getdate($unix_time);
        return mktime(0, 0, 0, $bits['mon'], $bits['mday'], $bits['year']);
    }

    function end_of_day($unix_time)
    {
        $bits = getdate($unix_time);
        return mktime(23, 59, 59, $bits['mon'], $bits['mday'], $bits['year']);
    }
}
