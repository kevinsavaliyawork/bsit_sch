<canvas id="pieChart" width="800" height="800"></canvas>
<script>
var ctx = document.getElementById("pieChart").getContext('2d');
var myPieChart = new Chart(ctx,{
    type: 'pie',
    data: {
        datasets: [{
            data: [10, 20, 30],
            backgroundColor:[
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }],
        labels: ["Red", "Yellow", "Blue"]
    },
    options:  {
      title: {
        display: true,
        text: 'A really cool pie chart'
      }
    }
});
</script>
