﻿<head>

    <?php

/* @start unset search session for timesheet added by HD*/
$uriseg = $this->uri->segment_array();

if($pageInfo->Title == ''){

    if ($uriseg[1] == 'collectionmanager') {
      $title = "Collection Manager";
    } elseif ($uriseg[1] == 'analysis') {
      $title = "Analysis";
    } else {
      $title = "Home";
    }

} else {
  $title = $pageInfo->Title;
}


?>

    <title><?php echo $this->session->userdata('appTitle')?> - <?php echo($title)?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <script>
	  var BASEURL_IMAGE = '<?php echo(BASEURL_IMAGE); ?>';
        var base_url = '<?php echo base_url();?>';
        var page_id = "<?php echo $pageId;?>";
        var page_title = "<?php echo $pageTitle;?>";
        var userID = "<?php echo $this->session->userdata('UserID'); ?>";
        var ENVIRONMENT = "<?php echo ENVIRONMENT; ?>";
      </script>



    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link href='https://fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,100,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'> -->
    <!-- FIXME: Changes made to mui.min.css (Should be no changes) -->
    <link href="<?php echo base_url();?>assets/mui-0.2.10/css/mui.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>core/assets/css/style.css" rel="stylesheet" type="text/css" />
     <!-- added by kevin 16-03-2020 -->
    <!-- <link href="<?php echo base_url();?>core/assets/css/bsit_core.css" rel="stylesheet" type="text/css" /> -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/materialize.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-2.2.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/mui-0.2.10/js/mui.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui-1.11.4.js"></script>
    <script src="<?php echo base_url();?>assets/js/json2.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src='<?php echo base_url();?>assets/js/linkify.min.js'></script>
    <script src='<?php echo base_url();?>assets/js/moment.js'></script>
    <script src='<?php echo base_url();?>assets/js/linkify-html.min.js'></script>
    <script src="<?php echo base_url();?>core/assets/js/main.js"></script>
    <script src="<?php echo base_url();?>core/assets/js/Page.js" type="module"></script>
    <script src="<?php echo base_url(); ?>core/assets/js/aes.js"></script>
    <script src="<?php echo base_url(); ?>core/assets/js/jquery-password-validator.js"></script>
    <!-- added by kevin 16-03-2020 -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">   
    <script src="<?php echo base_url(); ?>core/assets/js/bsit_core.js"></script>

    <link rel="icon shortcut" type="image/x-icon" href="<?php echo base_url();?>assets/core/images/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assets/core/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>assets/core/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/core/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/core/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/core/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>assets/core/images/favicon/manifest.json">

	<!-- input mask css And Jquery  -->
    <script src= "https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script> 
	
    <!-- [START]SCRIPT & CSS FOR MATERIAL DESIGN -->
    <script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/dataTables.material.min.js"></script>
    <link href="<?php echo base_url();?>assets/css/dataTables.material.min.css">
    <link href="<?php echo base_url();?>assets/css/material.min.css">

    <!-- Date and time pickers JS -->
    <script src='<?php echo base_url();?>assets/pickadate.js-3.5.6/lib/compressed/picker.js'></script>
    <script src='<?php echo base_url();?>assets/pickadate.js-3.5.6/lib/compressed/picker.date.js'></script>
    <script src='<?php echo base_url();?>assets/pickadate.js-3.5.6/lib/compressed/picker.time.js'></script>

    <!-- new added  by hd -->
    <script src="<?php echo base_url();?>assets/js/bootstrap-timepicker.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-timepicker.min.css">

    <script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">

    <!-- Date and time pickers CSS -->
    <link href='<?php echo base_url();?>assets/pickadate.js-3.5.6/lib/compressed/themes/classic.css' rel="stylesheet" type="text/css" />
    <link href='<?php echo base_url();?>assets/pickadate.js-3.5.6/lib/compressed/themes/classic.date.css' rel="stylesheet" type="text/css" />
    <link href='<?php echo base_url();?>assets/pickadate.js-3.5.6/lib/compressed/themes/classic.time.css' rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/material.min.css">
    <script src="<?php echo base_url();?>assets/js/material.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.blockUI.js"></script>

    <!-- [START] CKEDITOR -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>core/assets/css/sample.css">
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>core/assets/js/ckeditor.js"></script>-->
    <!-- [END] CKEDITOR -->
<!-- Documents popup -->
	<script src="<?php echo base_url();?>core/assets/js/utils/Documents/Documents.js" type="module"></script>
<!-- <script src="<?php echo base_url();?>core/assets/js/utils/Notes/ckeditor.js" type="script"></script> -->
    <!-- [END]SCRIPT & CSS FOR MATERIAL DESIGN -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage"
      content="<?php echo base_url();?>assets/core/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#2196F3">

<style type="text/css">
<?php echo $this->session->userdata('cssOverride');?>
</style>

  </head>



<div id="allcollpopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Collection</h4>
      </div>
      <div class="modal-body">
        <form id="allcoll">

          <div class="select-collection">
            <div class="mui-textfield mui-select" >
              <select name="collection_id" class="" id="collId"  required>
                <option value=""> SELECT COLLECTION </option>
              </select>
            </div>

            <button id="saveframe" class="mui-btn mui-btn--flat mui-btn--primary">Save</button>
            </div>

            <div class="or-div">
              <span>OR</span>
            </div>

            <div class="text-center">
              <button type="button" class="mui-btn mui-btn--primary"  onClick="document.location.href='<?php echo base_url();?>collectionmanager?pageId=<?php echo $pageInfo->pageId;?>&'">Add New Collection</button>
            </div>
        </form>
      </div>

    </div>

  </div>
</div>


 <div id="bsit-notes-modal-container" style="color: black;">
      <?php require_once APPPATH.'modules/core/views/NotesView.html' ?>
    </div>