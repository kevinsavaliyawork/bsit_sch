<!--
.io_numReps
#id_CV_Rank
#id_CV_NumVisits
#id_CV_NumVisitsToNewCust
#id_CV_Actual
#id_CV_Target
#id_CV_Img
-->

<div class="chart_main" id="repCustVisitsMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Customer Visits</div>
  </div>

  <!-- body -->
  <div class="chart_body" id="repCustomerVisits_container">
      <!-- LEFT -->
      <div class="chart_metric">
        <div class="chart_primary">
          <span class="chart_primary_title">Rank:</span>
          <span class="chart_primary_value" id="id_CV_Rank">#</span>/<span class="io_numReps">#</span></br>
          <span id="id_CV_NumVisits"># </span> <span id="id_CV_NumVisitsToNewCust"></span>
        </div>
        <div class="chart_scnd">
          <span class="chart_scnd_title">Target:</span>
          <span class="chart_scnd_value"><strong id="id_CV_Actual">#</strong></span>/<span id="id_CV_Target">#</span>
        </div>
      </div>

      <!-- RIGHT -->
      <div class="chart_hero">
        <img id="id_CV_Img" src="<?php echo base_url();?>assets/images/dashboard_gifs/AAD_thumbs_up.gif" />
      </div>
  </div>

</div>
