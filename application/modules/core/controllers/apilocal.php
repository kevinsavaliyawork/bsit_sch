<?php
defined('BASEPATH') or exit('No direct script access allowed');
//require_once(APPPATH.'/libraries/API_caller.php');
require_once(APPPATH . '/modules/core/libraries/API_caller.php');
class apilocal extends API_caller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

        $this->load->library('session');
    }

    // Adding a new record
    public function create()
    {
        $this->create_exec($_POST['data']);
    }

    // Updating and existing record
    public function update()
    {
        $this->update_exec($_POST['data']);
    }

    // Deleteing a single record
    public function delete()
    {
        $this->delete_exec($_POST['data']);
    }

    // loading a record
    public function loadsingle()
    {
        $this->loadsingle_exec($_POST['data']);
    }

    // loading a set of records filtered
    public function loadfiltered()
    {
        $this->loadfiltered_exec($_POST['data']);
    }

    // loading a drop list
    public function dropdownlist()
    {
        $this->dropdownlist_exec($_POST['data']);
    }

    // loading Generate Invoice Data
    public function call_sp()
    {
        $this->call_sp_exec($_POST['data']);
    }

    public function call_method()
    {
        // print_r($_POST['parameters']); exit;
        $this->call_method_exec($_POST['data']);
    }

    public function call_sp_login()
    {
        // TODO: Remove error log?
        error_log("logging in");

        $authorizeApiStatus = $this->validateUser($_POST);

        if ($authorizeApiStatus['status'] == 1) {
            $this->Call_SP_exec_Login($_POST['data']);
        } else {
            $dataMessage->Data = array();
            $dataMessage->HasError = true;
            $dataMessage->ErrorCode = 401;
            $dataMessage->ErrorMessage = $authorizeApiStatus['msg'];
            $returnData = json_encode($dataMessage);
            echo $returnData;
            exit;
        }
    }

    public function validateUser($data)
    {
        $username = $data['parameters'][0]['value'];
        $password = base64_decode($data['parameters'][1]['value']);
        $device = $data['parameters'][2]['value'];
        $response = array();


        $CURLDATA = array('collections' => json_encode(
            array('colls' => array(array(
                'coll_name' => 'users',
                'filter' => "usr.username = '" . $username . "'"
            )))
        ));

        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_GET;

        $page_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
        $page_result = json_decode($page_result);

        if (isset($page_result->Data)) {
            $page_result = $page_result->Data;
        }

        if (isset($page_result[0]->Results) && !empty($page_result[0]->Results)) {
            $user = (array)$page_result[0]->Results[0];

            $password_correct_db = $user['Password'];
            $params = array(8, false);
            $check =  $this->CheckPassword($password, $password_correct_db);
            if ($check) {
                if ($user) {
                    $response['status'] = 1;
                } else {
                    $response['status'] = 0;
                    $response['msg'] = 'Invalid Access';
                }
            } else {
                $response['status'] = 0;
                $response['msg'] = 'Invalid Username/Password';
            }
        } else {
            $response['status'] = 0;
            $response['msg'] = 'Invalid Authentication';
        }
        return $response;
    }

    public function CheckPassword($password_input, $password_db)
    {
        if (crypt($password_input, $password_db) == $password_db) {
            return true;
        } else {
            return false;
        }
    }
	
	// update notes for aver all project
	public function updateNotes()
    {
        $param = $this->input->post();
        if (empty($param)) {
            echo json_encode(["message"=>"Invalid Request."]);
            exit;
        }
        
         $_DATAPOST['parameters'] = [
            'key' => 'update_notes',
            'filters' => [
                [
                    'key' => 'note_id',
                    'value' => $param['note_id'],
                ],
                [
                    'key' => 'note',
                    'value' => $param['note'],
                ],
                [
                    'key' => 'note_type',
                    'value' => $param['note_type'],
                ],
                [
                    'key' => 'note_header',
                    'value' => $param['note_header'],
                ],
                [
                    'key' => 'notify_users',
                    'value' => $param['notify_users'],
                ]
            ]
        ];
        $data_parameters = json_encode($_DATAPOST['parameters']);
        $this->BASEURLMETHOD = API_BASEURL . API_METHOD_GET;
        $json_page_result = $this->API->CallAPI("GET", $this->BASEURLMETHOD, $data_parameters, false);
        $page_result = json_decode($json_page_result);

        if (isset($page_result->HasError) && $page_result->HasError == 1) {
            $load_error = true;
            $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : UPDATE_FAIL;
            echo $error_message;
            return;
        }
        echo  json_encode($page_result->Data);
        exit;
        
    }
}
