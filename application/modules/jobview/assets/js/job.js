// JavaScript Document
/**
 * Global var modalID stores the id of the add modal, this is important.
 * @type {string}
 */
var modalID = '';
var ModalIDHashtag = '';
var dontClearFlag = 0;
var moduleName = window.location.pathname;
$(document).ready(function () {

  drag_drop_job();
  job_scrolling_design();
  createModalDuplicateTasks();
  //When the user leaves the title input.
  $(document).on('blur', 'input[name="title"]', function () {
	  $('.loader').show();
    //This will ensure we will just check if the user is adding a new task, not updating a task.
    if (!$('.bsit_form input[name="_id"]').is(':visible')) {
      //Grab the adding modal ID
      modalID = $(this).parents('.modal').attr('id');
      ModalIDHashtag = '#'+modalID;
      //Perform the lookup, if the user is in the development tasks module. This JS is shared between projects
      //Support tasks and dev tasks.
      if(moduleName.includes("developmenttasks")) {
        lookupDuplicatedTasks($(this).val());
      }
	  $('.loader').hide();
    }
	$('.loader').hide();
  });

  //The user wants to create a new task regardless.
  $(document).on('click','#createNewTask',function () {
    continueCreatingNewTask(modalID);
  });
  //The user will cancel the new task.
  $(document).on('click','#cancelTask',function () {
    cancelTask(modalID);
  });
  //Just to ensure the duplicates modal will close, as this is created on the fly, on .on() will make that event work.
  $(document).on('click','.close-duplicated-modal',function () {
	// on close click open existing popup
	$(ModalIDHashtag).modal('show');
	
    //That flag indicates if the user is ignoring the existing tasks.
    $('#duplicatedVerified').val(0);
    //Cleaning and hiding things.
    $('#duplicatedModalBody').html('');
    $('#showDuplicatedJobs').hide();
    $('#duplicatedModalBody').modal('hide');
  });
  //When the add modal is closed we need to reset a few things.
  $(document).on('hidden.bs.modal',ModalIDHashtag, function () {
    console.log('here')
    if(dontClearFlag == 0) {
      $('#duplicatedVerified').val(0);
    }
  })
  $(document).on('click','.bsit-add-new-save', function () {
      $('#duplicatedVerified').val(0);
  })
});
/**
 * So this is going to create a modal in the body to display when a duplicated task is found
 * and append it to the body of the page.
 */
var createModalDuplicateTasks = function () {
  //This is used when the user ignores the duplicated tasks --> button "Continue creating new task"
  $('.bsit-page-frame').append('<input type="hidden" id="duplicatedVerified" value="0" name="duplicatedVerified">');
  //Creating the modal to show duplicated tasks.
  var divModal = '<div id="showDuplicatedJobs" class="modal  fade in" style="width: 80%; min-width: 200px; margin: 20px auto; overflow: auto; z-index: 99999; display: none;" aria-hidden="false">' +
    '<div class="mui-panel" style="margin-bottom:0px;">' +
    '<button class="mui-btn mui-btn--small mui-btn--fab mui-btn--accent bsit_form_close_btn close-duplicated-modal" style="fill: white; float:right; padding: 0.6rem;" data-dismiss="modal">\n' +
    ' <svg class="" viewBox="0 0 24 24"><path fill="#000000" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>\n' +
    '                </button>' +
    '<h2 class="bsit_form_titel">Possible duplicated Tasks</h2><hr>';
  divModal +='<p>The tasks below contain one or more terms from the task you were creating. Please ensure no task is created twice.</p>';
  divModal +='<div id="duplicatedModalBody"></div>';
  divModal +='<div class="text-right">';
  divModal +='<button class="mui-btn mui-btn--flat mui-btn--primary" id="cancelTask">Cancel task, I will use an existing task.</button>';
  divModal +='<button id="createNewTask" class="mui-btn mui-btn--flat mui-btn--dark">Continue creating new task</button>';
  divModal +='</div>';
  divModal +='</div> <!-- close mui-panel>';
  divModal +='</div> <!-- close showDuplicatedJobs>';
  $('body').append(divModal);
}
/**
 * This will perform a lookup to see if we can find more tasks with the words of the title.
 * If tasks are found, then we display a modal created in the function createModalDuplicateTasks()
 * @param title
 * @returns {boolean}
 */
var lookupDuplicatedTasks = function (title) {
  if (title.length > 0 && $('#duplicatedVerified').val() == 0) {
    $('.loader').show();
    var requestData = {
      parameters: [
        {key: "@title", value: title},
      ],
      sp_name: [{key: "BSIT_checkDuplicateTasks"}],
    };
    //Ajax call to check the tasks
    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      success: function (data) {
        if(data.length > 0) {
          dontClearFlag = 1;
          //Displaying the content in a table.
          var text = '<table class="table mui-table">';
          text += '<table class="table mui-table">';
          text += '<thead>';
          text += '<th>ID</th>';
          text += '<th>Title</th>';
          text += '</thead>';
          text += '</tbody>';
          $(data).each(function (k,v){
            text+='<tr>';
            text+='<td>'+v._id+'</td>';
            text+='<td>'+v.title+'</td>';
            text+='</tr>';
          });
          text += '</ul></div>';
          text += '</tbody>';
          text += '</table>';

          $('#duplicatedModalBody').html('').html(text);
          $('#showDuplicatedJobs').show();
          $('#'+modalID).modal('hide');
          $('#duplicatedModalBody').modal();

        }
        $('.loader').hide();
      },
      error: function (e) {
        alert("Error: " + e);
        $('.loader').hide();
      }
    });

  }
  return false;
}
/**
 * When the user decides to continue creating a new task.
 * @returns {boolean}
 */
var continueCreatingNewTask = function () {
  $('#duplicatedVerified').val(1);
  $('#duplicatedModalBody').modal('hide');
  $('#'+modalID).modal();
  $('#duplicatedModalBody').html('');
  $('#showDuplicatedJobs').hide();
  dontClearFlag = 1;
  return false;
}
/**
 * When the users decides to cancel the task.
 * @returns {boolean}
 */
var cancelTask = function () {
  $('#duplicatedVerified').val(0);
  $('#duplicatedModalBody').html('');
  $('#showDuplicatedJobs').hide();
  $('#duplicatedModalBody').modal('hide');
  $('#'+modalID).find('input,select').val('');
  $('#'+modalID).modal('hide');
  dontClearFlag = 0;
  return false;
}
// --------------------------------------------------------------
// -- CODE FOR JQUERY SCROLL DESIGN FOR JOB
// --------------------------------------------------------------
function job_scrolling_design() {
  $(".content").mCustomScrollbar({
    theme: "minimal"
  });
}


function drag_drop_job() {
  var $container = $(".task-container");
  var $task = $('.todo-task');
  $task.draggable({
    addClasses: false,
    connectToSortable: ".task-container",
  });
  $container.droppable({
    accept: ".todo-task"
  });
  $(".ui-droppable").droppable().sortable({
    placeholder: "ui-state-highlight",
    opacity: .6,
    helper: 'original',
    receive: function (event, ui) {
      console.log($(ui.item[0]));
      var job_id = $(ui.item[0]).attr("id");

      var ststus = $(this).attr('id');
      var json_str = '{"colls": [{"coll_id": "1", "values": {"key_id":"' + job_id + '", "Status":"' + ststus + '"}}]}';
      $.ajax({
        type: "POST",
        url: base_url + "core/Apilocal/update",
        data: {data: json_str},
        dataType: 'json',
        beforeSend: function () {
          $(".loader").hide();
        },
        success: function (response) {
          //  ajax_all(0,PFID,0,2);
          $(".loader").hide();
        }
      });
      Materialize.toast('Status Updated Successfully!', 3000);
    }
  });
}
