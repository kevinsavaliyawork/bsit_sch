<div class="<?php echo $class_name?>">
  <div style="padding: 8px;" class="bsit-page-frame" id="page_frame_<?php echo $dataframeid ?>" data-pfid="<?php echo $dataframeid ?>">
    <h6>
      PROPERTIES
      <span class="table_name_prop"></span>
      <span class="property_name_prop"></span>
    </h6>
    <form dynamic_data="1" id="editForm<?php echo $dataframeid ?>"
      data-collID="<?php echo $static_coll_id?>"
      user_id="<?php echo $this->session->userdata('UserID'); ?>"
    >
      <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead></thead>
        <tbody id="property_id">
         <input type="hidden" id="key_id" name="key_id" value="<?php echo $id?>">
         <input type="hidden" name="key_update_date" value="<?php date('Y-m-d')?>">
         <input type="hidden" name="key_update_by" value="<?php echo $this->session->userdata('UserID'); ?>">

<?php

foreach ($property_data as $key => $prop) {

    // build a HTML string for display
    $propTitle = $prop->title;
    $name = $prop->PropName;
    $disabledMsg = ($prop->readonly == 1 ? "disabled" : "");
    $requiredMsg = ($prop->required == 1 ? "required" : "");
    $SelVal = $showpropertyData->$name;

    switch ($prop->edit_type) {
    case 'TXT':
        $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
            <input type="text" name="'.$prop->PropName.'"
              value="'.htmlentities($SelVal).'" '.$disabledMsg.' '.$requiredMsg.'>
            </div>';
        break;

    case 'HID':

    $defaultValue = $prop->default_value;
            
            if (substr($prop->default_value, 0, 1) == 'S') {

                $session_variable = substr($prop->default_value, 2, -1);

                $defaultValue = $this->session->userdata($session_variable);
            } elseif (substr($prop->default_value, 0, 1) == 'Q') {
                $query_variable = substr($prop->default_value, 2, -1);
                $defaultValue = $_GET[$query_variable];
            }

            
        $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
            <input type="hidden" name="'.$prop->PropName.'"
              value="'.$defaultValue.'" '.$disabledMsg.' '.$requiredMsg.'>
            </div>';
        break;

    case 'PWD':
        $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
            <input type="password" name="'.$prop->PropName.'"
              value="'.$SelVal.'" '.$disabledMsg.'  '.$requiredMsg.'>
            </div>';
        break;

    case 'MLE':
        $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
            <textarea name="'.$prop->PropName.'" '.$disabledMsg.'
             '.$requiredMsg.'>'.$SelVal.'</textarea>
            </div>';
        break;

    case 'YN':
        $checked = '';

        if ($SelVal == 1) {
            $checked = 'checked';
        }

        $editHTML = '<div class="mui-checkbox '.$prop->style_class.'">
            <label class="bsit_check"><input type="checkbox" '.$checked.' class="propcheckbox"
              name="'.$prop->PropName.'" '.$disabledMsg.'  '.$requiredMsg.'><span class="bsit_checkmark bsit_checkmark_design_mode_sidebar"></span></label>
            </div>';
        break;

    case 'SEL':
        // get the json string of values - if an available
        $selectOptions = json_decode($prop->edit_value);

        $editHTML = '<div class="mui-textfield '.$prop->style_class.'"
            style="margin-bottom: 0px;"><label></label>
            <div class="mui-select" style="padding-top: 0px !important;">
            <select name="'.$prop->PropName.'" '.$disabledMsg.' '.$requiredMsg.'>';

        // if none in array, simply show the NA option only
        if (count($selectOptions) < 1) {
            $editHTML .= '<option value="NA">No values</option>';
        } else {
            foreach ($selectOptions as $optVal => $opt) {
                if ($SelVal == $optVal) {
                    $selected = 'selected="selected"';
                } else {
                    $selected = '';
                }

                if ($optVal == '_empty_') {
                    $optVal = '';
                }

                $editHTML .=  '<option value="'.htmlspecialchars($optVal).'"
                    '.$selected.'>'.$opt.'</option>';
            }
        }

        $editHTML .= '</select></div></div>';
        break;

    case 'COLL':
            // get the json string of values - if an available
            $editValue  = str_replace('"', "'", $prop->edit_value);
            $editSource = $prop->edit_source;

            $editHTML = '<div class="mui-textfield ui-front '.$prop->style_class.'">
                <input name="'.$prop->PropName.'" class="autoCompColl"
                  type="text" value="'.$SelVal.'" data-returnpropname="'.$prop->PropName.'"
                  data-coll="'.$editSource.'" data-columns="'.$editValue.'"
                  data-modalid="editModel'.$frame['PFID'].'" '.$disabledMsg.' '.$requiredMsg.'>
                </div>';
        break;

    case 'ADVC':
        // get the json string of values - if an available
        $editValue  = str_replace('"', "'", $prop->EditValue);
        $editSource = $prop->EditSource;

        $editHTML = '<div class="mui-textfield ui-front '.$prop->style_class.'">
            <input name="'.$prop->PropName.'" class="autoCompCollAdv"
              type="text" value="'.$SelVal.'" data-returnpropname="'.$prop->PropName.'"
              data-coll="'.$editSource.'" data-columns="'.$editValue.'"
              data-modalid="editModel'.$frame['PFID'].'" '.$disabledMsg.' '.$requiredMsg.'>
            </div>';
        break;

    case 'DATE':
            $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
                <input type="text" name="'.$prop->PropName.'"
                  class="datepicker" value="'.$SelVal.'" '.$disabledMsg.' '.$requiredMsg.'>
                </div>';
        break;

    case 'TIME':
        // TODO: Remove perigon
        $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
            <input type="text" name="'.$prop->PropName.'"
              class="timeperigon" value="'.$SelVal.'" '.$disabledMsg.' '.$requiredMsg.'>
            </div>';
        break;

    case 'FILE':
        $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
            <img name="'.$prop->PropName.'" src="'.$disabledMsg.'" id="image_file" '.$requiredMsg.'>
            </div>';
        break;

    // case 'GHDR':
    //     $editHTML = '<div class="mui-textfield '.$prop->style_class.'">
    //         <img name="'.$prop->PropName.'" src="'.$disabledMsg.'" id="image_file" '.$requiredMsg.'>
    //         </div>';
    //     break;    
    }

?>

<tr>
  <td class="mdl-data-table__cell--non-numeric"><?php echo $propTitle; ?></td>
  <td><?php echo $editHTML; ?></td>
</tr>

<?php
}
?>

  </tbody>
</table>

 </form>

 <div>
    <button id="commit-edit-update-<?php echo $dataframeid?>" dynamic_data="1"
      class="mui-btn mui-btn--primary bsit-commit-edit" coll_id=<?php echo $static_coll_id?>
    >Save</button>

    <img id="correctImg" src="<?php echo base_url();?>assets/core/images/correct.png"
      style="display:none; vertical-align:middle;">

    <img id="errorImg" src="<?php echo base_url();?>assets/core/images/error.png"
      style="display:none; vertical-align:middle;">

  </div>
</div>
</div>
