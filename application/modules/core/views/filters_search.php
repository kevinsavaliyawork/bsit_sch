<div id="filtersContainer">
    <?PHP
    $filtersFormList 	= '';
    $dimDropList 		= '';
    $dimSelectedList	= '';
    if(count($filters)>0){
        foreach($filters as $dimKey=>$dimfilter){			
            // loop each of the values
            foreach($dimfilter as $filter){
                // setup the li element
				$dimSelectedList = $dimSelectedList . '<li  data-dimid="'.$dimKey.'" ';
				$dimSelectedList = $dimSelectedList . 'data-dimvalue="'.$filter['value'].'" ';
				$dimSelectedList = $dimSelectedList . 'data-dimopp="'.$filter['opperator'].'" ';
				$dimSelectedList = $dimSelectedList . 'data-dimperm="'.$filter['perm_filter'].'" ';
				$dimSelectedList = $dimSelectedList . ($filter['perm_filter']==1 ? 'class="permfilter" >' : '>');
				// set the value to be show
                $dimSelectedList = $dimSelectedList . $filter['value'] . '<br/>';
                $dimSelectedList = $dimSelectedList . '<span class="secondaryTxt">';
                $dimSelectedList = $dimSelectedList . $filter['dim_name'].' '.$filter['opperator'];  
                $dimSelectedList = $dimSelectedList . '</span>';
				// is this a filter the user can change
				if($filter['perm_filter']<>'1'){
					// add the cross or not
					$dimSelectedList = $dimSelectedList . '<a class="dimRemover" href="#">x</a>';
				}
				$dimSelectedList = $dimSelectedList . '</li>';
            }	
        }
    }
    ?>
    <div id="filtersSearch">
        <div id="filtersClearSearch" style="display: none;">
            <img src="<?php echo BASEURL_IMAGE; ?>/ic_close_24px.svg"/>
        </div>
        <div id="advancedSearchDropArrow">
            <img id="advancedSearchDownImg" src="<?php echo BASEURL_IMAGE; ?>/ic_arrow_down_24px.svg"/>
            <img id="advancedSearchUpImg" style="display: none;" src="<?php echo BASEURL_IMAGE; ?>/ic_arrow_up_24px.svg"/>
        </div>

        <div id="filtersDimList">
            <img src="<?php echo BASEURL_IMAGE; ?>/ic_search_black_24dp.png" />
        </div>

        <input id="filterSearchBox" name="search" type="search" placeholder="Explorer Dimensions" autocomplete="off"/>
                  
    </div>
    
    <div id="filterSearchResults">
        <ul id="filterSearchResultsList">
        	
        </ul>
    </div> 
    
    <div id="advancedSearch">
        <div class="mui--text-title">Date Range Search</div>
    	<div id="advancedSerachDateSelCont">
            <div class="mui-textfield mui-textfield--float-label bsit_advDateCont">
                <input type="text" id="fromDate" disabled placeholder="From"/>
            </div>
            <div class="mui-textfield mui-textfield--float-label bsit_advDateCont">
                <input type="text" id="toDate" disabled placeholder="To"/> 
            </div>
            <button id="advancedSearchFilterDate" class="mui-btn mui-btn--raised" style="float:right;">Filter Date</button>  
        </div>
               
        <?php 
		if(isset($user_links) && count($user_links) > 0){         
		?>
            <div class="mui--text-title">Saved Links</div>
            <ul id="savedLinks">
                <?php
                foreach($user_links as $link){
					$URLLink 		= $link['url'];
					$URLDisplayName = $link['name'];
					$URLLinktype	= $link['extra_link_tags'];

					// append the users full name if found in the link
					$URLLink = str_replace('[userfullname]',$users_name_full,$URLLink);
					
					echo('<li><a href="'.$URLLink.'" '.$URLLinktype.'>'.$URLDisplayName.'</a></li>');
				}
				?>
            </ul>
         <?php }; ?>
    </div> 
    
    <div id="dimSelectedList">
        <ul id="dimSelectedUL">
            <?php
            echo($dimSelectedList);
            ?>
        </ul>
    </div>
    
</div>