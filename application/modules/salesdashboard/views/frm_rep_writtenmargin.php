<!--
.io_numReps
#id_WM_Rank
#id_WM_FooterVal1
#repWrittenMargin_container
#repWrittenMarginPercChart
-->

<div class="chart_main" id="repWrittenMarginMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Written Margin</div>
  </div>

  <!-- body -->
  <div class="chart_body" id="repWrittenMarginBody_container">
      <!-- LEFT -->
      <div class="chart_metric">
        <div class="chart_primary">
          <span class="chart_primary_title">Rank:</span>
          <span class="chart_primary_value" id="id_WM_Rank">#</span>/<span class="io_numReps">#</span>
        </div>
      </div>

      <!-- RIGHT -->
      <div class="chart_hero" id="repWrittenMargin_container" style="height:400px; max-height:150px">
        <canvas id="repWrittenMarginPercChart" style="width:100%; height:100%;"></canvas>
      </div>
  </div>

  <!-- Foooter -->
  <div class="chart_footer">
    <!-- LEFT -->
    <div class="chart_foot_1st">
      <span class="chart_foot_1st_title" >LY:</span>
      <span class="chart_foot_1st_value" id="id_WM_FooterVal1">#%</span>
    </div>
  </div>

</div>
