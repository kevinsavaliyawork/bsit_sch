	<script src="<?php echo base_url();?>customer/assets/js/customer_details.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>customer/assets/css/customer_style.css">
	<?php $_id= isset($_GET['_id']) ? $_GET['_id'] : $_POST['_id'];?>
<div class="bsit_opp_overview" >

	<div class="mui-col-lg-8 mui-col-md-12 mui-col-sm-12 mui-col-xs-12" id="bsit_opp_overview_data">
		<input type="hidden" name="_id" id="_id" value="<?php echo $_id?>">
		<?php $this->load->view('Overview', ['customer' => $customer , '_id' => $_id]);?>
	</div>
	 
	<div class="mui-col-lg-4 mui-col-md-12 mui-col-sm-12 mui-col-xs-12">
		<div class="bsit_opp_overview_activity">
			<div class="bsit_opp_overview_activity_title">
				<h3  style="float: left;width: 50%;">Activity (<span id="activityCount"><?php echo count($activity_history->Data) ?></span>)</h3>
				<div class="mui-textfield bsit_form bsit_statusdiv">				
				</div>
				<?php	
				$html_action_note='';
				$html_action_note .= '<div style="float: left;">';
					
				$html_action_note .= '<a class="bsit-notes-btn notes-btn-34" data-collid="12" data-recid="'.$_GET["_id"].'" data-modal="notes">
									<div class="BSIT-TableActions hasTooltip bsit_opp_delete_edit" style="margin-right:10px; opacity: 1" data-toggle="tooltip" data-placement="bottom" data-original-title="Notes">
										<label style="display: block; float: left; padding: 05px 15px; border-radius: 50px; color: #fff; background-color: #2196f3; min-width: 130px; text-align: center; font-size: 14px;">Add/View Note</label>
										<span class="icon_tooltip">Notes</span>
									</div>
								</a>';
					$html_action_note .= '</div>';
				?>

				<div><span class="bsit_cola_overview_edit_notes"><?php echo $html_action_note ?></span></div>
			</div>
			
			<input type="hidden" name="_id" id="_id" value="<?php echo $_id?>">
			<div class="cola_activity_list_scroll" id="customer_notes_list">
				<?php $this->load->view('customer_notes_list', ['activity_history' => $activity_history , '_id' => $_id]);?>
			</div>
		</div>
	</div>

	</div>
<?php

function RemoveSpecialChar($str)
{
    $res = preg_replace('/[\[\]\" "]+/', '', $str);
    return $res;
}
$str = $customer[0]->def_event_color;
$str1 = RemoveSpecialChar($str);
$arry = explode(",", $str1);
$color= $arry[0];
?>
<script type="text/javascript">
$(document).ready(function()
{ 
	var htm ='';
	var color= '<?php echo $color; ?>';
	var address= '<?php echo $customer[0]->address; ?>';
	var name= '<?php echo $customer[0]->name; ?>';
	 htm= '<a id="customerEditButton"><div class="BSIT-TableActions hasTooltip bsit_opp_delete_edit" style="margin-right:10px;opacity:1;"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-weight:bold;font-size:18px;color: #2196f3;"></i><span class="icon_tooltip">Edit </span></div></a>';
    change_color(color,address,name,htm);
}); 
</script>
