<?php

class documentmodel extends brain_Model {
	function __construct(){
		parent::__construct();	
		$this->load->model('core/Bsit_io','API');
		$this->load->library('session');
	}
	
	
	 // documents get data
    public function getDocumentDetails($getDocumentDetails){
          
           $CURLDATA = array('collections' => json_encode(
               array('colls' => array(array(
                   'coll_name' => 'attached_documents',
                   'filter' => "bsd._id = ".$getDocumentDetails.""
               )))
           ));

           $BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
           $getDocumentDetails_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $CURLDATA);
           return $getDocumentDetails_result;
    }
	
}
