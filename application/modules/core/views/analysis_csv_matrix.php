<?PHP

// clean a string
 function cleanData(&$str)
 {
		if($str == 't') $str = 'TRUE';
		if($str == 'f') $str = 'FALSE';
		if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
		  $str = "'$str";
		}
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
 }	

// filename for download
$filename = "bsit_io_" . date('Ymd') . ".csv";

header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-Type: text/csv");

$out = fopen("php://output", 'w');

// first we loop headers row only, 
if(!empty($analysis_data)){

	// if the measures count is 1, only one measure is in use
	// so show variances
	$show_variance = false;
	$matrix_col_cnt = count($analysis_dim2);
	if($analysis_meas_cnt==1){
		$show_variance = true;
	}
	$show_variance=false;
	// include a spacer for first column
	$row_array[] = '';
	
	// here we output the matrix header, which includes first two rows.
	// loop dim 2, the matrix dimension running horizontally.
	$col_headers_output = 1;
	foreach($analysis_dim2 as $matrixDim){
		// output the dim 1 empty cell
		
		$matrixDim_value 	= $matrixDim['dimVal'];
							
		// setup number of columns for header to span
		if($show_variance==true && $col_headers_output<$matrix_col_cnt){
			$colespan_cnt = $analysis_meas_cnt + 1;
		}else{
			$colespan_cnt = $analysis_meas_cnt;
		}
		
		// add this header to the output
		$row_array[] = $matrixDim_value;
		
		// create an artificial colspan
		for($k = 1; $k<$colespan_cnt; $k++){
			$row_array[] = '';
		}
		
		$col_headers_output++;
	}
				
	// output the first header row
	fputcsv($out, array_values($row_array), ',', '"');
	$row_array = array();
	
	// we output headers -- build array of keys for column names
	if(!empty($analysis_data['BSIT']['BSIT'])){
		$kcnt = 0;
		
		// get a list of measures
		$dim_1_name = $analysis_data['BSIT']['BSIT'][0];
		$dim_2_name = $analysis_data['BSIT']['BSIT'][1];
		$measure_list = $analysis_data['BSIT']['BSIT'][2];
		
		// output the name of the first dimension, running vertically
		$row_array[] = $dim_1_name;
		
		// loop each of the horizontal matrix dimensions
		$col_headers_output = 1;
		foreach($analysis_dim2 as $matrixDim){
			// loop each of the measures
			$mea_cnt = 0;
			foreach($measure_list as $row){
				if($mea_cnt == 0){
					// setup the CSS for right align and left border
					$columnClasses = 'class="mui--text-right matrixTableLeftBorder"';
				}else{
					$columnClasses = 'class="mui--text-right"';
				}
				// output the row header columns start
				$row_array[] = $row;
				$mea_cnt++;
			}
			// show variance header for previous matrix column
			if($show_variance==true && $col_headers_output<$matrix_col_cnt){
				$row_array[] = 'VAR %';
			}
			$col_headers_output++;
		}
	}
	
	// output the second header row
	fputcsv($out, array_values($row_array), ',', '"');
				
	// setup rowcount variable
	$rowCounter = 1;
	
	// loop each vertical dimension
	foreach($analysis_dim1 as $dim1){
		$dim1_value = $dim1['dimVal'];
		
		// setup a new array to hold the row
		$row_array 		= array();
		$row_array[] 	= $dim1_value;
				
		// loop each horizontal dimension
		$dim2Cnt = 0;
		$var_prev_val = 0;
		foreach($analysis_dim2 as $dim2){
			$dim2_name 	= $dim2['dimName'];
			$dim2_value = $dim2['dimVal'];
			$dim2_id 	= $dim2['dimId'];
			// loop each measure, output the line
			$mea_cnt = 0;
			foreach($measure_list as $measure){
				// setup this line with link
				if(!empty($analysis_data[$dim1_value][$dim2_value][$measure])){
					$rowValue = $analysis_data[$dim1_value][$dim2_value][$measure];
				}else{
					$rowValue = '-';
				}
								
				// show the variance column
				if($show_variance == true){
					if($rowValue != '-'){
						$var_curr_val = floatval(str_replace(array('$',','),'',$rowValue));
					}else{
						$var_curr_val = 0;
					}
					
					// if we are on the second column or more
					if($dim2Cnt > 0){
						// check if the value has increased or decreased since last itteration
						// --------------------------------------------------------------
						// VARIANCE IS FOR PREVIOUS COLUMN - SO OUTPUT BEFORE CURRENT
						// --------------------------------------------------------------
						if($var_curr_val > 0){
							$var_diff		= $var_prev_val-$var_curr_val;
							$var_percent 	= (($var_diff/$var_curr_val)*100);
							$var_percent 	= number_format($var_percent, 1, '.', '');
						}else{
							$var_percent 	= 	100;
						}
						if($var_prev_val > $var_curr_val){
							// gone up
							$row_array[] = $var_percent.'%';
						}elseif($var_prev_val < $var_curr_val){
							// gone down
							$row_array[] = $var_percent.'%';
						}else{
							// no change
							$row_array[] = $var_percent.'%';
						}
					}
					$var_prev_val = $var_curr_val;
				}
				$row_array[] = $rowValue;
				$mea_cnt++;
			}
			$dim2Cnt++;
		}

		// output the row
		fputcsv($out, array_values($row_array), ',', '"');
		$rowCounter++;
	}
}

fclose($out);
exit;

?>