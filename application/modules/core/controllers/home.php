<?php
defined('BASEPATH') or exit('No direct script access allowed');

class home extends brain_controller
{
    public function __construct()
    {

        parent::__construct(0);

        $this->load->model('module');
        $this->load->model('pagemodel');
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function index()
    {

        $page_name = $this->uri->segment(1);


        $page_result = $this->pagemodel->getPageHeaderInfo($page_name);


        // load the infomation
        $userID = $this->session->userdata('UserID');
        $tmp_data['menu_data'] = $menu  = $this->module->getClientActiveModules($userID);


        $pageMenu = $page_result[0]->Results[0]->is_display_menu;
        $pageHeader = $page_result[0]->Results[0]->is_display_header;
        $pageFooter = $page_result[0]->Results[0]->is_display_footer;

        if ($pageMenu == 1) {
            $data['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);
        }
        $data['header_view'] = 0;
        if ($pageHeader == 1) {
            $data['header_view'] = 1;
        }

        $data['pageMenu'] = $pageMenu;
        $data['footer_view'] = 0;
        if ($pageFooter == 1) {
            $data['footer_view'] = 1;
        }
        $notifications_Data = $this->pagemodel->getnotificationsData();
        $notifications = json_decode($notifications_Data);
        $data['notifications'] = $notifications->Data[0]->Results;
        // load the page
        $this->load->view('header', $data);
        $this->load->view('content', $data);
        $this->load->view('home');
        $this->load->view('footer', $data);
    }
}
