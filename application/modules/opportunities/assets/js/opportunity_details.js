//import {getPageFrameIDByName,getPageByName, getCollectionIDByName, advMultiCollectionSourceNote, getFrameData } from '../../frames/TableView/TableView.js';
//import { getFrameFromElement, updatePageFrameArray } from '../../Page.js';

$(document).ready(function()
{ 
 if($('ul.nav-tabs li').hasClass('bsit-page-frame')){
	$("ul.nav-tabs li.bsit-page-frame:first").addClass("active");
	  //edit opprtunity
	$("#commit-edit-update-144").removeClass("bsit-commit-edit").addClass("bsit-commit-editcustom");
	var editPOPUP = $('#editModel144').get(0).outerHTML;
	
	$( "#editModel144").remove();
	$( "#opprtunityViewTab").append(editPOPUP);	
 }

 
  showMoreAndLess();
    
	// add button of attahment documents
  var get_url = window.location.href; //window.location.href
  var url = new URL(get_url);
  var getParameter = url.searchParams.get("job_id");

  $('#add-btn-147').addClass('bsit-upload-btn').removeClass('bsit-btn-add');
  $("#add-btn-147").attr("data-collid","73");
  $("#add-btn-147").attr("data-recid",getParameter);
  $("#add-btn-147").attr("data-modal",'upload');
  
  
   
 }); 
 $('#editBtn').click(function() {
  $("#editModel144").remove();
});

 function refresh_documents(){
   $(".loader").show(); 
  var pageFrameId= 147;
  var pageId = 96;
  var pageParams;
    pageParams =  $('#fram_prop_filter_147').val();
    var pass_data_ajax = {'filter':"",'order':"",'page_num':1,'num_recs':10,'page_id':pageId,'frame_id':pageFrameId,'prop_frame_filter_parameter':pageParams}
    //jQuery.extend(pass_data_ajax,pageParams);
    var new_url = base_url+"core/Frame/load_content";
    $.ajax({
      type: "POST",
      data:  pass_data_ajax,
      url: new_url,
      success: function(returnData){
        $(".loader").hide(); 
        $('#page_frame_'+pageFrameId+'_content').replaceWith(returnData);
      }
    });
}
$( document ).ajaxComplete(function( event, xhr, settings )
{ 
  $('#page_frame_147_content table .bsit-edit-btn').addClass('bsit-edit-documents-btn-list').removeClass('bsit-edit-btn');
  showMoreAndLess();
});  

function showMoreAndLess(){
  
  $(".readMoreNOte").addClass("readlessactive")
  $(".readLessNOte").addClass("readlessinactive")
  $(".cola_more").removeClass("intro");

 $(document).on("click",".readMoreNOte",function(){
    $(this).prev().addClass("intro");
    $(this).removeClass("readlessactive");
    $(this).addClass("readlessinactive");
    $(this).next().removeClass("readlessinactive");
    $(this).next().addClass("readlessactive");
 });

  $(document).on("click",".readLessNOte",function(){
    $(this).prev().prev().removeClass("intro");
    $(this).removeClass("readlessactive");
    $(this).addClass("readlessinactive");
    $(this).prev().removeClass("readlessinactive");
    $(this).prev().addClass("readlessactive");
 });

  
}
$(document)
  .button()
  .on('click', '.bsit_form_close_btn', function() {
	  if(!$('li.bsit-page-frame:first').hasClass('active')){
        var get_url = window.location.href; //window.location.href
        var url = new URL(get_url);
        var getParameter = url.searchParams.get("job_id");
		
        if (getParameter) {  refresh_documents();}
       
      
    }
	
  });

$( document ).ajaxComplete(function( event, xhr, settings ) {
  // after add   
  if ( settings.url == base_url+"core/apilocal/call_method" )
  {
	if(xhr.responseText){
      var total = $('span#activityCount').text();
      total=parseInt(total)+1;
      $('span#activityCount').text(total);
      if($('li.bsit-page-frame:first').hasClass('active')){
        updateNoteList();
      }else{
		refresh_notes();
	  }
	}
   
  }
  // after Edit  
 if ( settings.url == base_url+"core/apilocal/updateNotes" )
  {
    if(xhr.responseText){
     if($('li.bsit-page-frame:first').hasClass('active')){
        updateNoteList();
     }else{
		refresh_notes();
	  }
    
    }
  }
   /*if ( settings.url == base_url+"core/Apilocal/update" )
  {
    if(xhr.responseText){
        updateOpportunityOverviewList();
     
    }
  }*/
  if ( settings.url == base_url+"core/Frame/load_content" )
  {
    if(xhr.responseText){
      setNotes();
    }
  }
  
 /* if ( settings.url === base_url+"core/document/documentfileuploadDoc" )
  {
    if(xhr.responseText != null){
      refresh_documents();
    }
  }*/
 
});

// notes details
function updateNoteList() {
  let url = base_url + 'OpportunityView/fetchLatestHistoryList';
  let job_id = $("#job_id").val();
  if (job_id) {
    $.ajax({
      type: 'POST',
      url: url,
      data: { 'job_id': job_id },
      success: function (res) {
        $("#activity_notes_list").html(res);
		//refresh_notes();
      }
    });
  }

}

// customer details 
function updateOpportunityOverviewList() {
  let url = base_url + 'OpportunityView/fetchOpportunityCustomerAllData';
  let job_id = $("#job_id").val();
  if (job_id) {
    $.ajax({
      type: 'POST',
      url: url,
      data: { 'job_id': job_id },
      success: function (res) {
     //   console.log(res);
        $("#bsit_opp_overview_data").html(res);
      }
    });
  }

}



function change_color(color,address,name,htm){
	var htmldata  = '<div class="framecolorBox" style="background:'+color+'">'+name.charAt(0)+'</div><div class="frameNameDetails"><h5 id="custname">'+name+'<span class="editButtontop">'+htm+'</span></h5><p id="custaddress">'+address+'</p></div>';
	$(".FrameMainTitle").html(htmldata);
}

$(document).on('click', '#customerEditButton', function () {  
  $("#customerEditView").trigger("click");
});


$(document).ready(function(){
   setNotes();
});
 function refresh_notes(){
   $(".loader").show(); 
  var pageFrameId= 150;
  var pageId = 96;
  var pageParams;
  /*$('#fram_prop_filter_193').val();

*/pageParams =  $('#fram_prop_filter_150').val();
    var pass_data_ajax = {'filter':"",'order':"",'page_num':1,'num_recs':10,'page_id':pageId,'frame_id':pageFrameId,'prop_frame_filter_parameter':pageParams}
    //jQuery.extend(pass_data_ajax,pageParams);
    var new_url = base_url+"core/Frame/load_content";
    $.ajax({
      type: "POST",
      data:  pass_data_ajax,
      url: new_url,
      success: function(returnData){
        $(".loader").hide(); 
        $('#page_frame_'+pageFrameId+'_content').replaceWith(returnData);
      }
    });
}
//page_frame_193
function setNotes(){
  var get_url = window.location.href; //window.location.href
  var url = new URL(get_url);
  var getParameter = url.searchParams.get("job_id");

//add Buttons
$('#add-btn-150').addClass('bsit-notes-btn').removeClass('bsit-btn-add');
$("#add-btn-150").attr("data-collid","73");
$("#add-btn-150").attr("data-recid",getParameter);
$("#add-btn-150").attr("data-modal",'notes');

//edit buttons
$('a.edit-del-btn-150.bsit-edit-btn').addClass('bsit-edit-notes-btn').removeClass('bsit-edit-btn');
$("a.edit-del-btn-150.bsit-edit-notes-btn").attr("data-collid","73");
$("a.edit-del-btn-150.bsit-edit-notes-btn").attr("data-noteeditid",getParameter);
$(".bsit-edit-notes-btn").css("float","left");


}
