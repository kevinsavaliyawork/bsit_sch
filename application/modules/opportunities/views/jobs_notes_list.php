<?php

error_reporting(0);
function dayCounts($date)
{
	$date1 = $date;
	$date2 = date("Y/m/d");
	$ts1 = strtotime($date1);
	$ts2 = strtotime($date2);
	$year1 = date('Y', $ts1);
	$year2 = date('Y', $ts2);
	$month1 = date('m', $ts1);
	$month2 = date('m', $ts2);
	$diff = $ts2 - $ts1;
	// $diff = strtotime($date2) - strtotime($date1);
	$months = (($year2 - $year1) * 12) + ($month2 - $month1);
	$years   = round($diff / (365 * 60 * 60 * 24));
	$weeks = intval(date("W", $date1)) - intval(date("W", strtotime(date("Y-m-01", $date1)))) + 1;
	$days = round($diff / (60 * 60 * 24));


	if ($days >= 364) {
		$dayCount = $years . ' Years ago';
	} elseif ($days > 60) {
		$dayCount = $months . ' Months ago';
	} elseif ($days >= 30) {
		$dayCount = $months . ' Month ago';
	} elseif ($days >= 14) {
		$dayCount = $weeks . ' Weeks ago';
	} elseif ($days >= 7) {
		$dayCount = $weeks . ' Week ago';
	} elseif ($days >= 2) {
		$dayCount = $days . ' Days ago';
	} elseif ($days == 1) {
		$dayCount = $days . ' Day ago';
	} elseif ($days == 0) {
		$dayCount = 'Today';
	}
	return $dayCount;
}

$arr = array();
//echo "<pre/>";print_R($activity_history->Data);exit;

foreach ($activity_history->Data as $activity_list) {
	$arr[] = $activity_list->date_only;
}
$activity_unique_date = array_unique($arr);
foreach ($activity_unique_date as $date) { 
	$today = date("Y-m-d", strtotime($date));
	$id='';
if(strtotime($today) == strtotime(date("Y-m-d"))){
	$id='id="today"';
}
?>
	<div class="bsit_opp_overview_activity_list" <?php echo $id;?> >
		<label> <?php echo date_format(date_create($date), "l, d M") ?></label>
		<ul id="opprtunityListNotes">
			<?php foreach ($activity_history->Data as $activity) { ?>
				<?php if ($date == $activity->date_only) { ?>
					<?php 
					if ($activity->type == 'Document') {
						$fileIcone = "bsit_activity_list_documents";
					}
					else if ($activity->type == 'note') {
						$fileIcone = "bsit_activity_list_comment";

					}
					else if ($activity->type == 'phone') {
						$fileIcone = "bsit_activity_list_called";
					}
					else if ($activity->type == 'status') {
						$fileIcone = "bsit_status_update";
					}
					else{
						$fileIcone = "bsit_activity_list_comment";
					}
					?>

					<?php
					$activityTitle = trim($activity->title);
					$type = '';
					$atitle = '';
					if (strpos($activityTitle, '*****') !== false) {
						$arr = explode('*****', $activityTitle);
						$type = trim($arr[0]);
						$atitle = trim($arr[1]);
					} else {
						$atitle = $activityTitle;
					}

					if ($atitle == '' || $atitle == null) {
						//$atitle .= '--';
						$atitle .= '';
					}
					$activity_highlights = "-";
					if ($type && $type == 2) {
						// $activityTitle = "<b style='color:#FD7F00;'>".$activityTitle."</b>";
						$atitle = "<b style=''>" . $atitle . "</b>";
						$activity_highlights = "'background: #F7E4DB'";
						$fileIcone = "cola_exclamation_mark";
					}
					$activityDescription = strip_tags(trim($activity->description),"<img>,<figure>,<table>");
					//$activityDescription = strip_tags(trim($activity->description));
					if ($activityDescription == '' || $activityDescription == null || empty($activityDescription) || $activityDescription == "NULL") {
						$activityDescription = '--';
					}
					if(isset($activity->filename)){
						$activityDescription = $activity->filename.' - '.$activityDescription;	
					}
					$duration = null;
					if($activity->type != 'note' && $activity->type != 'Document'){
						$internal_notes = strip_tags(trim($activity->internal_notes));
						$duration = "<p><span>".date_format(date_create($activity->startdate), "Y/m/d")."  To  ".date_format(date_create($activity->enddate), "Y/m/d") . " by".ucwords($activity->username)."</span> </p>";
					}
					$job_id= isset($_GET['job_id']) ? $_GET['job_id'] : $_POST['job_id'];
					?>

					<li class=<?php echo $fileIcone; ?> style=<?php echo $activity_highlights; ?>>
						<label> <?php echo $activity->event_name ?> - 
						<?php echo $atitle ?>
							<?php if ($activity->type == 'note') { ?>
								<a href="javascript:void(0);" class="bsit-edit-notes-btn beditnote_<?php echo $activity->id; ?> noteEditbtn" data-noteid="<?php echo $activity->id; ?>" data-collid="73" data-recid="<?php echo $job_id; ?>" title="Edit">
									<span><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:14px;color: #2196f3;"></i></span>
								</a>
							<?php } ?>
							<?php if ($activity->type == 'Document') { ?>
								<a href="javascript:void(0);" class="bsit-edit-documents-btn" data-collid="73"  data-docid="<?php echo $activity->id; ?>" data-recid="<?php echo $job_id; ?>" title="Edit">
									<span><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:14px;color: #2196f3;"></i></span>
								</a>
							<?php } ?>
						</label>
						<?php echo $duration; ?>
						<?php echo strip_tags(trim($activity->internal_notes)); ?>
					
							<p class="cola_more intro" id="cola_more"><?php echo $activityDescription ?></p>
							<?php if (strlen($activityDescription) >= 350) { ?>
								<span class="readMoreNOte" id="">Read More..</span>
								<span class="readLessNOte" id="">Read Less..</span>
							<?php  } ?>
						
						<?php if ($activity->date_only == '' || $activity->date_only == null) { ?>
							<span> -- </span>
						<?php } else { ?>
							<p><span><?php echo dayCounts(date_format(date_create($activity->date_only), "Y/m/d")) ?> by <?php echo ucwords($activity->author); ?></span> </p>  
						<?php } ?>
					</li>
				<?php } ?>
			<?php } ?>
		</ul>
	</div>
<?php } ?>