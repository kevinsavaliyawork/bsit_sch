<div class="chart_main">
  <div class="chart_head">
    <div class="chart_title">Summary</div>
  </div>
  <div class="chart_body" id="mySumChart_container" style="height:400px; max-height:400px">
    <canvas id="mySumChart" style="width:100%; height:100%;"></canvas>
  </div>
</div>
