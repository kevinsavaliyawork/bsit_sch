<?php
if(!empty($analysis_data[0])){
?>
	<div class="mui-row">
	  <div class="mui-col-md-4">
			Rows: <select id="rowCntSelector">
				<?php
				// create options for number of rows
				for($r=10; $r<=50; $r=$r+10){
					// if the row is the same as selected, highlight it.
					if($r == $rows){
						echo('<option selected=selected value="'.$r.'">'.$r.' Rows</option>');
					}else{
						echo('<option value="'.$r.'">'.$r.' Rows</option>');
					}
				}
				?>
			</select>
	  </div>
	  <div class="mui-col-md-4 mui-col-md-offset-4 mui--text-right">
		<?php
		// previous button for paging
		if($page>0){
			echo('<a href="#" id="prevPageSelector">Prev</a> ');
		};

		// add a next button for paging
		if(count($analysis_data)>$rows){
			echo(' <a href="#" id="nextPageSelector">Next</a>');
		};
		?>
	  </div>
	</div>
	<div id="analysisTableCont">
		<table class="mui-table">
			<thead>
				<tr>
					<?php 
					// here we loop the first row only, 
					// we output headers -- build array of keys for column names
					if(!empty($analysis_data[0])){
						$kcnt = 0;
						foreach($analysis_data[0] as $key=>$row){ 
							// ability to add difference css classes for different columns
							if($kcnt <= count($selectedGroups)-1){
								$columnClasses = 'class="mui--text-left"';
							}else{
								$columnClasses = 'class="mui--text-right"';
							}

							// output the row header columns start
							echo('<th nowrap '.$columnClasses.'>');

							// is this the column sorting the data, or is the sort empty
							if($sort_col == $key){ //|| $sort == '' & $kcnt == 0){
								// this is the column sorting, so is it A=ASC or D=DESC, or is it blank
								if($sort_order == "A" || $sort_order == ""){
									?>
									<a href="#" class="sortColSelector" data-sort="<?php echo('D'.$key)?>">
										<?php echo($key.' ▲'); ?>
									</a>
									<?php
								}else{
									?>
									<a href="#" class="sortColSelector" data-sort="<?php echo('A'.$key)?>">
										<?php echo($key.' ▼'); ?>
									</a>
									<?php
								}
							}else{	
								// none sorted column
								?>
								<a href="#" class="sortColSelector" data-sort="<?php echo('D'.$key)?>">
								<?php echo($key); ?>
								</a>
								<?php
							}

							// add to the list of keys
							$keys[] = $key;
							$kcnt++;

							// end the row
							echo('</th>');
						}
					}
					?>
				</tr>
			</thead>
		<tbody>
			<?php 
			// if there are any rows
			if(!empty($analysis_data)){
				// setup rowcount variable
				$rowCounter = 1;
				// loop each row and output table
				foreach($analysis_data as $row){
					// we always pull an extra row, don't show it
					if($rowCounter<=$rows){
						echo('<tr>');
							$columnCounter = 1;
							// loop each header, output the line
							foreach($keys as $key){
								// dimensions are treated differently
								if($columnCounter <= count($selectedGroups)){
									$columnClasses 	= 'class="mui--text-left"';

									// get dimensions and child dimension details
									$grpByDimID		=	$selectedGroups[$columnCounter-1];
									$grpByChildId	=	$dimensions[$grpByDimID]['child_id'];
									$grpByChildName	=	$dimensions[$grpByDimID]['child_name'];	
									$valLookup		=	$dimensions[$grpByDimID]['lookup_value'];								
									// setup the a tag for the dimension
									$colLink		= '<a href="#" class="drillInSel" ';
									$colLink		.='data-value="'.$row[$key].'"'; 
									$colLink		.='data-dimid="'.$grpByDimID.'" '; 
									$colLink		.='data-dimname="'.$key.'" '; 
									$colLink		.='data-grpcolnum="'.$columnCounter.'" '; 
									$colLink		.='data-grpchildid="'.$grpByChildId.'" ';
									$colLink		.='data-grpchildname="'.$grpByChildName.'" '; 
									$colLink		.='data-valLookup="'.$valLookup.'" '; 
									$colLink		.='>'; 
									$colLinkEnd		= '</a>';


									// setup this line with link
									$rowValue = $colLink.'<div style="height:100%; width:100%;">'.$row[$key].'</div>'.$colLinkEnd;
								}else{
									// setup the CSS for right align
									$columnClasses = 'class="mui--text-right"';

									// setup this line with link
									$rowValue = $row[$key];
								}

								?>
								<td <?php echo($columnClasses);?> >
									<?php echo($rowValue); ?>
								</td>
								<?php

								// increment the column counter
								$columnCounter++;
							}
						echo('</tr>');
					}
					$rowCounter++;
				}
			}
			?>
		</tbody>
	</table>
	<?php

	// should we show the download link?
	if($can_user_download==1){
	?>
		<div><a id="downloadCSV" href="#"><img style="opacity:.4" src="assets/core/images/ic_get_app_black_24dp_1x.png"/></a></div>
	<?php
	}
}else{
	?>
	<div class="main_menu_item">
		<div class="menu_item_icon">
			<img src="assets/core/images/methods.png" class="image">
		</div>
		<div class="menu_item_text">
			<p>No results have been found, <span style="font-weight:bold; font-size:16px">sorry!</span> </p>
			<p>Try using a different filter selection to expand your search.</p>
			<!--<p>Remember, filters of the same dimension work like an 'OR' statement, however, different dimensions are combined with an 'AND' clause.<br/> If two filters are entered, 1 for day and one for year, they will be combined with an 'AND'</p>-->
		</div>
	</div>
	<?php
}
?>


