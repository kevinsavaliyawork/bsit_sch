<?php
defined('BASEPATH') or exit('No direct script access allowed');
//require_once(APPPATH.'/libraries/API_caller.php');
require_once(APPPATH.'/modules/core/libraries/API_caller.php');
class document extends API_caller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        // define("UPLOAD_PATH",'application/modules/core/assets/uploads/');
		define("UPLOAD_PATH",'E:/BrainStorm IT/Websites/bsit_sch/core/uploads/');
        $this->load->library('session');
        $this->load->model('documentmodel');

       
    }

 
   public function documentfileuploadDoc()
    {
    
    $doc_url = $_POST['doc_url'];

    // $fileUploadVlidate = false;
    $number_of_files_uploaded = count($_FILES['files']['name']);
    if($number_of_files_uploaded > 0 )
    {
     
       //upload files
        for ($i = 0; $i <  $number_of_files_uploaded; $i++) 
        {

            if (!empty($_FILES['files']['name'][$i]))
            {
                if(!is_dir(UPLOAD_PATH . $_POST['collection_id'])){
                   $res = mkdir(UPLOAD_PATH . $_POST['collection_id'], 0777);
                   // create jobs folder
                   if (!is_dir(UPLOAD_PATH .$_POST['collection_id']."/".  $_POST['row_id'])) {
                        mkdir(UPLOAD_PATH . $_POST['collection_id']."/". $_POST['row_id'], 0777);
                    }
                }
                else{
                    if (!is_dir(UPLOAD_PATH .$_POST['collection_id']."/".  $_POST['row_id'])) {
                        mkdir(UPLOAD_PATH . $_POST['collection_id']."/". $_POST['row_id'], 0777);
                    }
                }

                $file = str_replace(' ', '_', $_FILES["files"]['name']);
                $file = preg_replace("/[^a-z0-9\.]/", "", strtolower($file));

                $filename = pathinfo($file, PATHINFO_FILENAME);
                $extension = pathinfo($file, PATHINFO_EXTENSION);
                $file_name = $filename . '_' . date("dmYhis") . '.' . $extension;

                $config = array(
                    //'upload_path' => './'.IMG_PATH.$query['job_id'],
                    'upload_path' => UPLOAD_PATH . $_POST['collection_id']."/".  $_POST['row_id'],
                    'allowed_types' => '*',
                    'overwrite' => TRUE,
                    'file_name' => $file_name
                );

                $this->load->library('upload', $config);

                // print_r($config); exit; 

                // Upload file to server
                if ($this->upload->do_upload('files')) {
                    // Uploaded file data
                    $img_name = $this->upload->data();

                    $uploadImgData[$i]['files'] = $img_name['file_name'];
                    $file_type = strtoupper($extension); // strtoupper(pathinfo($file_name[$i], PATHINFO_EXTENSION));
                    $file_size = intdiv($_FILES['files']['size'][$i], 1024);
                    $keywords = $_POST['keywords'];
                    $description = $_POST['description'];
                    $file_location =  $_POST['collection_id'].'/' . $_POST['row_id'] . '/' . $file_name;
                    $rowId  = $_POST['row_id'];
                    $collectionId  = $_POST['collection_id'];
                    $_id=$_POST['_id'];
                    $document_type_code = $_POST['document_type_code'];
                    $doc_url = null;//$_POST['doc_url'];
                    $param = [
                        'description' => $description,
                        'file_location' => $file_location,
                        'file' => $file,
                        'file_size' => $file_size,
                        'file_type' => $file_type,
                        'keywords' => $keywords,
                        'rowId' => $rowId,
                        'collection_id' => $collectionId,
                        'UploadCollectionID' => $_POST['UploadCollectionID'],
                        'document_type_code' => $document_type_code,
                        'doc_url' => $doc_url,
                        '_id' => $_id,
                       
                    ];
                    
                    $page_result = $this->upload_docfile($param);

                    if (isset($page_result->HasError) && $page_result->HasError == 1) {
                        $load_error = true;
                        $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : CREATE_FAIL;
                        $data_pass['error'] = $error_message;
                    }
                }
              
            }
        }
		//print_R($_POST);
            $data_pass['success'] = "File Uploaded Successfully";
            $data_pass['id'] = $_POST['row_id'];
            $data_pass['collid'] = $_POST['collection_id'];
           
            echo json_encode($data_pass);
    } 
    else if (strtolower($_POST['document_type_code']) == 'url' && !empty($doc_url))
     {
    
        $f_name = basename($doc_url); // to get file name

        $file = str_replace(' ', '_', $f_name);
        $file = preg_replace("/[^a-z0-9\.]/", "", strtolower($file));

        $filename = pathinfo($file, PATHINFO_FILENAME);
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $file_name = $filename . '_' . date("dmYhis") . '.' . $extension;


        $file_with_path = IMG_PATH . $_POST['row_id'] . '/' . $file_name;

        //$this->downloadUrlToFile($_POST['doc_url'], $file_with_path);

        //if (file_exists($file_with_path)) {
            $keywords = $_POST['keywords'];
            // $description= $this->input->post('description');
            $description = $_POST['description'];
           // $file_location = '/' . $query['job_id'] . '/' . $file_name;
          
            // $document_type_code =$this->input->post('document_type_code');
            $document_type_code = $_POST['document_type_code'];
            $file_type = strtoupper(pathinfo($file_name, PATHINFO_EXTENSION));
            $file_size = intdiv(filesize($file_with_path), 1024);
            $rowId  = $_POST['row_id'];
            $collectionId  = $_POST['collection_id'];
           // $doc_folder = $_POST['doc_folder'];
            $document_type_code = $_POST['document_type_code'];
            if($_POST['_id'] > 0){
                $url = $_POST['file_name'];    
            } else {
                $url = $_POST['doc_url'];
            }
            
            
            $_id=$_POST['_id'];

            $acceptedFormats = array('gif', 'png', 'jpg', 'jpeg', 'pdf', 'xlsx', 'xls', 'txt', 'docx', 'zip', 'csv');

            if(!in_array(strtolower($extension), $acceptedFormats)) {
                $file_type = "";
            }
            $param = [
                'description' => $description,
                'file_location' =>null,
                'file' =>  $_POST['doc_url'],
                'file_size' => $file_size,
                'file_type' => $file_type,
                'keywords' => $keywords,
                'rowId' => $rowId,
                'collection_id' => $collectionId,
                'UploadCollectionID' => $_POST['UploadCollectionID'],
                'document_type_code' => $document_type_code,
                'doc_url' => $_POST['doc_url'],
                '_id' => $_id,
               
            ];
          
            $page_result = $this->upload_docfile($param);

            if (isset($page_result->HasError) && $page_result->HasError == 1) {
                $load_error = true;
                $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : CREATE_FAIL;
                $data_pass['error'] = $error_message;
            }
			//print_R($_POST);
            $data_pass['success'] = "File Uploaded Successfully";
            $data_pass['id'] = $_POST['row_id'];
            $data_pass['collid'] = $_POST['collection_id'];
           
            echo json_encode($data_pass);
        
        }
     
    else if($_POST['_id'] > 0  && strtolower($_POST['document_type_code']) != 'url' ) //&& ($_POST['keywords'] != '' || $_POST['description'] != '')
        {
			$checkfileextenstion =  pathinfo($_POST['file_name'], PATHINFO_EXTENSION);     
            if($checkfileextenstion == ''){
               $_POST['file_name']=$_POST['file_name'].".".strtolower($_POST['file_type']);
            }
           // if($_POST['_id'] > 0 && ($_POST['keywords'] != '' || $_POST['description'] != ''))        
            $keywords = $_POST['keywords'];
            $description = $_POST['description'];
            $rowId  = $_POST['row_id'];
            $collectionId  = $_POST['collection_id'];
            $file_location = $_POST['file_location'];
            $file_name = $_POST['file_name'];
            $file_size_kb  = $_POST['file_size_kb'];
            $file_type  = $_POST['file_type'];
            $document_type_code = $_POST['document_type_code'];
            $doc_url = $_POST['doc_url'];
            $_id=$_POST['_id'];
            
            $param = [
                'file_location' => $file_location,
                'file' => $file_name,
                'file_size' => $file_size_kb,
                'file_type' => $file_type,
                'description' => $description,
                'keywords' => $keywords,
                'rowId' => $rowId,
                'document_type_code' => $document_type_code,
                'doc_url' => $doc_url,
                'collection_id' => $collectionId,
                'UploadCollectionID' => $_POST['UploadCollectionID'],
                '_id' => $_id,
            ];
            
            $page_result = $this->upload_docfile($param);
            
            if (isset($page_result->HasError) && $page_result->HasError == 1) {
                $load_error = true;
                $error_message = (DETAIL_ERROR_MESSAGE == 1) ? $page_result->ErrorMessage : CREATE_FAIL;
                $data_pass['error'] = $error_message;
            }
			//print_R($_POST);
            $data_pass['success'] = "File Uploaded Successfully";
            $data_pass['id'] = $_POST['row_id'];
            $data_pass['collid'] = $_POST['collection_id'];
           
            echo json_encode($data_pass);
    }
	else{
		return false;
	}
      
      
       
    }

    function upload_docfile($param)
    {
        $userId = $this->session->userdata('UserID');
        $key_id = '';
        if($param['_id'] > 0) {
            $key_id = '"key_id": "' .  $param['_id'] . '",';
        }

        $documentsDetails = '{
                                "colls": [{
                                    "coll_id": "' .  $param['UploadCollectionID'] . '",
                                    "values": {
                                        '.$key_id.'
                                        "collection_id": "' .  $param['collection_id'] . '",
                                        "key_create_by":"' . $userId . '",
                                        "description": "' . $param['description'] . '",
                                        "file_location": "' . $param['file_location'] . '",
                                        "file_name":"' . $param['file'] . '",
                                        "file_size_kb":"' . $param['file_size'] . '",
                                        "file_type":"' . $param['file_type'] . '",
                                        "key_words":"' . $param['keywords'] . '",
                                        "document_type_code":"' . $param['document_type_code'] . '",
                                        "doc_url":"' . $param['doc_url'] . '",
                                        "row_id":"' . $param['rowId'] . '",
                                        "key_update_by":"' . $userId . '"
                                    }
                                }]
                            }'; 
        


        $CURLDATA = array('collections' => $documentsDetails);
        //print_R($CURLDATA);
        $BASEURLMETHOD = API_BASEURL . API_COLLECTION_POST;
        $json_page_result = $this->API->CallAPI("POST", $BASEURLMETHOD, $CURLDATA);
        //print_R($json_page_result);exit;
        return json_decode($json_page_result);
    }
	    
     public function downloadDocument()
    {
        

        $documentData = $this->documentmodel->getDocumentDetails($_GET["docid"]);
        $documentData_detail = json_decode($documentData)->Data[0]->Results[0];
		
        //if url then it will redirect to url
        if (strtolower($documentData_detail->document_type_code) == "url" && $documentData_detail->doc_url != '') {
            echo  $url = $documentData_detail->doc_url;
            if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
                $url = "http://" . $url;
            }
            header('Location: ' . $url);
            exit;
        }
        
     
        $file = UPLOAD_PATH . $documentData_detail->file_location;
        
        $fileType = strtolower($documentData_detail->file_type);
        $imageExternstionArr = ['jpeg', 'jpg', 'png', 'gif', 'webp', 'bmp', 'svg'];

        if (!file_exists($file)) {
            echo "File not exist.";
            exit;
        }
       
         else if ($fileType == 'pdf') {
            //PDF files

            header('Content-type: application/pdf');
            header('Content-disposition: inline; filename="' . basename($documentData_detail->file_name) . '"');
            header('content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            flush(); // Flush system output buffer
            readfile($file);
            die();
        } else if (in_array($fileType, $imageExternstionArr)) {
            $type = $this->get_mime_content_type($fileType);
            header('Content-Type:' . $type);
            header('Content-disposition: inline; filename="' . basename($documentData_detail->file_name) . '"');
            header('Content-Length: ' . filesize($file));
            flush(); // Flush system output buffer
            readfile($file);
            die();
        } else {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($documentData_detail->file_name) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            flush(); // Flush system output buffer
            readfile($file);
            die();
        }
    }
 function get_mime_content_type($extension)
    {
        $extension = strtolower($extension);
        $mime_type = "";

        switch ($extension) {

            case "png":
                $mime_type = "image/png";
                break;
            case "gif":
                $mime_type = "image/gif";
                break;
            case "bmp":
                $mime_type = "image/bmp";
                break;
            case "svg";
                $mime_type = "image/svg+xml";
                break;
            case "webp":
                $mime_type = "image/webp";
                break;
            case ("jpeg" || "jpe" || "jpg"):
                $mime_type = "image/jpeg";
                break;
            default:
                $mime_type = "image/jpeg";
                break;
        }

        return $mime_type;
    }
}
