//define variables



let seconds = 0;
let minutes = 0;
let hours = 0;


let displaySeconds = 0;
let displayMinutes = 0;
let displayHours = 0;


let interval = null;

let status = "stopped";


//function to when increment each value
function stopwatch(){

    

    seconds++;

    if(seconds / 60 === 1){
        seconds = 0; 
        minutes ++;

        if(minutes /60 ===1){
            minutes = 0;
            hours++;
        }
    }


   

    //adding 0 before one digit charachters
    if (seconds <10) {
        

        

        if (String(seconds).charAt(0) == 0) {
   
            displaySeconds = seconds.toString();
        } else {
            displaySeconds = "0" +seconds.toString();
        }


    }
    else{
        displaySeconds = seconds;
    }

    if(minutes <10) {
        
        if (String(minutes).charAt(0) == 0) {
            displayMinutes = minutes.toString();
        } else {
            displayMinutes = "0" +minutes.toString();
        }


    }
    else{
        displayMinutes = minutes;
    }

    if(hours <10){

        if (String(hours).charAt(0) == 0) {
            displayHours = hours.toString();
        } else {
            displayHours = "0" +hours.toString();
        }
        
    }
    else{
        displayHours = hours;
    }

    //display
    document.getElementById("display").innerHTML = displayHours + ":" + displayMinutes + ":" + displaySeconds;
}

function startStop() {

    var currenttime = $("#display").html();

    var result = currenttime.split(":");

    
    if(result[0] > 0){
        hours = result[0];
        displaySeconds = result[0];
    }

    if(result[1] > 0){
        minutes = result[1];
        displayMinutes = result[1];
    }

    if(result[2] > 0){
        seconds = result[2];
        displayHours = result[2];
    }



    if (status === "stopped"){
        interval = window.setInterval(stopwatch, 1000);
        document.getElementById("startStop").innerHTML = "Stop";
        status = "started";
    }
    else{
        window.clearInterval(interval);
        document.getElementById("startStop").innerHTML = "Start";
        status = "stopped";


        let event_id = $("#startStop").attr("event_id");
        let end_date = $("#startStop").attr("date");


        var hrs = new Date().getHours();
        var mins = new Date().getMinutes();
        var sec = new Date().getSeconds();


        var allTime = hrs+':'+mins+':'+sec;


        if(mins >= '00' && mins < "30")
        {

            mins = "30";
        }
        else
       {
        mins = "00";
        hrs = parseInt(hrs) + parseInt(1);
       }


       var allTime = hrs+':'+mins+':'+sec;


        //let time = $("#display").html();

        let finalDateTime = end_date+' '+allTime;
        let currentUser = $("#jobOwnerSelectDropdown").val();

        let requestData = {
        parameters: [
          { key: "@event_id", value: event_id },
          { key: "@end_date", value: finalDateTime },
          { key: "@owner_id", value: currentUser },
        ],
        sp_name: [{ key: "BSIT_UpdateEvents" }],
      };


    $.ajax({
      type: "POST",
      url: base_url + "core/Apilocal/Call_SP",
      data: requestData,
      dataType: "json",
      success: function(res) {
        location.reload(true);
      },
      error: function(e) {
        alert("Error: " + e);
      },
    });


    }




}


//function fr reset button
function reset(){
    window.clearInterval(interval);
    seconds = 0;
    minutes = 0;
    hours = 0;
    document.getElementById("display").innerHTML = "00:00:00";
    document.getElementById("startStop").innerHTML = "Start";
    status="stopped";
}
