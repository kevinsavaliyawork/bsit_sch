<div id="block-user-session" style="width:80%; min-width:90%; margin:auto; margin-top:20px; margin-bottom:20px; overflow:auto;">
  <div class="block-user-message">
  <h2>Session Expired</h2>
   <p class="mui--text-justify">Looks like you've been inactive for a while. Please, unlock the screen to continue using the system.</p>
  <button class="mui-btn mui-btn--raised mui-btn--primary" id="unlock-user-screen"><i class="fa fa-unlock-alt"></i> Unlock screen</button>
  </div>
  <div id="loginFormAjax" style="display: none">
    <h2>Login</h2>
    <form id="form-signin-ajax" name="form_signin_ajax" method="POST" action="<?php echo base_url().'login'; ?>">
      <div class="mui-textfield">
        <input type="text" name="username" id="username-login-ajax" required/>
        <label>Username</label>
      </div>
      <div class="mui-textfield">
        <input type="password" name="password" id="password-login-ajax"  required/>
        <label>Password</label>
      </div>
      <div class="mui-row">
        <div class="mui-col-lg-6 mui-col-md-6 mui-col-sm-6 mui-col-xs-6 reset-password mui--text-left">
          <a class="mui-float-left mui--text-left login-reset-password" id="btn-login-forgot-password">Forgot Password?</a>
        </div>
        <div class="mui-col-lg-6 mui-col-md-6 mui-col-sm-6 mui-col-xs-6  mui--text-right">
          <button class="mui-btn mui-btn--primary mui-float-right mui--text-right" id="btn-login-ajax" type="button">Login</button>
        </div>
      </div>
      <div id="preloader-login-info" style="float: right"></div>
      <div id="login-ajax-error-report" ></div>
    </form>
  </div>
</div>

<script type="text/javascript">
    $(function () {
        //On click in the button, do validations and submit the form
        $(document).on('click', '#btn-login-ajax', function (e) {
            if (!$('#form-signin-ajax')[0].checkValidity()) {
                $('#form-signin-ajax')[0].reportValidity();
                return false;
            }
            $('#form-signin-ajax').find('input').attr('readonly',true);
            setLoginPasswordEncryption();
            var btnText = $(this).html();
            var btn = $(this);
            btn.hide();
            $('#preloader-login-info').html('<h6>Please, wait...</h6>');
            $('#login-ajax-error-report').html('');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#form-signin-ajax').attr('action'),
                data: $('#form-signin-ajax').serializeArray(),
                success: function (data) {
                    if (data.success) {
                        //unlock page
                        $.unblockUI();
                        $('#loginFormAjax').hide();
                        $('.block-user-message').show();
                        //Run session verification again.
                        var verifySession = setInterval(checkSession, sessionInterval);
                    } else {
                        var message = data.msg;
                        $(message).each(function (k,v) {
                            $('#login-ajax-error-report').append(v.message+'<br>');
                        });
                        $('#form-signin-ajax').find('input').val('').attr('readonly',false);
                    }
                    btn.show();
                    $('#preloader-login-info').html('');
                },
                error: function (jqXHR, exception) {
                    //If we cannot do it with ajax, then just redirect the user to the login page.
                    location.href = $('#form-signin-ajax').attr('action');
                },
            });
        });

        $(document).on('submit', '#form-signin-ajax', function (e) {
            if ($('#form-signin-ajax')[0].checkValidity()) {
                e.preventDefault();
                return false;
            }
            $('#form-signin-ajax')[0].reportValidity();
        });
        //Forgot password.
        $(document).on('click','#btn-login-forgot-password',function () {
          location.href=base_url+'reset-request'
        });
    });
    /**
     * Set password encryption
     */
    var setLoginPasswordEncryption = function () {
        var password = $('#password-login-ajax').val();
        var key = CryptoJS.enc.Hex.parse("0123456789abcdef0123456789abcdef");
        var iv = CryptoJS.enc.Hex.parse("abcdef9876543210abcdef9876543210");
        var hash = CryptoJS.AES.encrypt(password, key, {iv: iv});
        $("#password-login-ajax").val(hash);
    }
</script>
