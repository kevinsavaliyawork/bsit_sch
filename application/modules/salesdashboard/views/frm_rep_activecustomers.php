<!--
.io_numReps
#id_AC_Rank
#id_AC_Percent
#id_AC_Actuals
#id_AC_img
#id_AC_FooterVal1
#id_AC_FooterVal2
-->

<div class="chart_main" id="repActiveCustsMain">

  <!-- header -->
  <div class="chart_head">
    <div class="chart_title">Active Customers</div>
  </div>

  <!-- body -->
  <div class="chart_body" id="repActiveCustomers_container">
      <!-- LEFT -->
      <div class="chart_metric">
        <div class="chart_primary">
          <span class="chart_primary_title">Rank:</span>
          <span class="chart_primary_value" id="id_AC_Rank">#</span>/<span class="io_numReps">#</span>
        </div>
        <div class="chart_scnd">
          <span class="chart_scnd_title">Actual:</span>
          <span class="chart_scnd_value"></span><span id="id_AC_Actuals">#</span>
        </div>
      </div>

      <!-- RIGHT -->
      <div class="chart_hero">
        <img id="id_AC_Img" src="<?php echo base_url();?>assets/images/dashboard_gifs/AAD_thumbs_down.gif"/>
        <!--<img src="<?php echo base_url();?>assets/images/dashboard_gifs/AAD_thumbs_up.gif" />-->
        <!--<img src="<?php echo base_url();?>assets/images/dashboard_gifs/AAD_head_scratch.gif" />-->
      </div>
  </div>
  
</div>
