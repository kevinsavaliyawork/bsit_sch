// JavaScript Document

$(document).ready(function() {

drag_drop_job();
job_scrolling_design();
griedViewRidirectTojobsDetails();


$( function() {
    $( ".datepicker1" ).datepicker({
      showOn: "button",
      buttonImage: base_url+"/assets/core/images/calendar.png",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: 'dd/mm/yy',
      onSelect : function(dateText, event){
          var userId = $('#followupDate').val();
          // debugger
         
         var arr_dateText = dateText.split("/");
         startday = arr_dateText[0];
         startmonth = arr_dateText[1];
         startyear = arr_dateText[2];
         
         var DateFormateChange = startyear+'/'+startmonth+'/'+startday

          var keyId = event.input.context.attributes["data-recid"].value;
          var collId = this.parentElement.parentElement.parentNode.parentNode.firstChild.attributes[0].nodeValue;
          datechandes(DateFormateChange,keyId,userId,collId);
      }
    });
        
  } );
  
});

async function datechandes(DateFormateChange, keyId, userId, collId) {

  let collectionID = collId;
  let dataRecid = keyId;
  let followup_date = DateFormateChange;
  let UserId = userId;

  
  let jsonData = "{'colls': [{'coll_id':'"+collectionID+"','values': {'key_id':'"+dataRecid+"','key_update_by':'"+userId+"','followup_date':'"+followup_date+"'} } ] }";
  console.log(jsonData);
 
    $.ajax({
      type: 'POST',
      url: base_url + 'core/Apilocal/update',
      data: {
        data: jsonData,
      },
      dataType: 'json',
      success: function(res) {
        // console.log(res);
        // alert('done');
        Materialize.toast('Follow Up Date Updated Successfully!', 3000);
        
      },
      error: function(err) {
        // alert('error');
        
      },
    });
  
  }

// --------------------------------------------------------------
// -- CODE FOR JQUERY SCROLL DESIGN FOR JOB
// --------------------------------------------------------------
function job_scrolling_design(){
  $(".content").mCustomScrollbar({
    theme:"minimal",
	/*ID-85 Scrolling down on opportunities */
    scrollInertia: 5,
    mouseWheelPixels: 30,
    autoDraggerLength:false 
  });
}


function drag_drop_job(){
  var $container = $(".task-container");
  var $task = $('.todo-task');
  $task.draggable({
    addClasses: false,
    connectToSortable: ".task-container",
  });
  $container.droppable({
    accept: ".todo-task"
  });
  $(".ui-droppable").droppable().sortable({
    placeholder: "ui-state-highlight",
    opacity: .6,
    helper: 'original',
    receive: function(event, ui) {
    var job_id = $(ui.item[0]).attr("id");
    var ststus = $(this).attr('id');
    var json_str = '{"colls": [{"coll_id": "1", "values": {"key_id":"'+job_id+'", "Status":"'+ststus+'"}}]}'; 
    $.ajax({
      type: "POST",
      url: base_url+"core/Apilocal/update",
      data:  {data: json_str},
      dataType: 'json',
       beforeSend: function(){
        $(".loader").hide();
      },
      success: function(response){
      //  ajax_all(0,PFID,0,2);
        $(".loader").hide();        
      }
    });
    Materialize.toast('Status Updated Successfully!', 3000);
    }
  }); 
}


function cmp($a,$b)
{
	$datatime1 = strtotime($a['followup_data']);
	$datatime2 = strtotime($b['followup_data']);
	$datatime = $datatime1 - $datatime2;
	console.log ($datatime);
}

function griedViewRidirectTojobsDetails() {
  $(".opp_title_edit").on('click', function (e) { 
    var recid = $(this).attr('data-recid');
   //alert('hi');
    // var customerid = $(this).attr('data-customerid');
    // return;
    // var moduleUrl = base_url + 'page/job_details?job_id=' + recid + '&customer_id=' + customerid;
    var moduleUrl = base_url + 'page/opportunity_details?job_id=' + recid ;
    window.open(moduleUrl, '_blank');
    //window.location = moduleUrl;  
    e.stopImmediatePropagation();
  });
  
}