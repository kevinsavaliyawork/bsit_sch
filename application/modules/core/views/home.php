<div class="mui-col-sm-10 mui-col-sm-offset-1">
	<div class="mui-panel">
		<?php 
		// ------------------------------
		// -- INVOICE REPORT VIEW - Shows the invoice report from the SSRS server 
		// ------------------------------
		//$fullPath	= $frame['fullPath_content'];	
		//$viewPath	= $frame['viewPath_content'];
		
		$html = '<div id="page_frame_'.$frame['PFID'].'_content" class="call_content_div">';
		
		// report URL
		$user = $_SESSION['username'];
		$show_params = "true";
		if($user =="tosborne" || $user =="cosborne"){
		//	$show_params = "true";
		}
		$date = new DateTime();
		$date_week = $date->format('Y-m-d');
		$html .= '<h3>BrainStorm IT</h3>BrainStorm IT delivers enterprise software to help businesses achieve their most ambitious goals. We do this by implementing ERP and bespoke custom software solutions.<BR /><BR />

Our customers are people who have dreamed of something better than their current software or what typical software packages can provide, they are all searching for something unique to help give their businesses that competitive edge.<BR /><BR />

We believe in the positive impact that well written, designed and implemented software can have on an organisation. Our passionate team thrives off seeing the great impact our software can have on our customers, their businesses and their lives.</div>';
		$html .= '</div>';
		
		echo($html);
		?>	
	</div>
</div>