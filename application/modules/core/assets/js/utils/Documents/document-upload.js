import {
    getDocuments,
    getDocumentsData,
    resetAllFields,
    uploadFileFromUrl
} from '../../utils/Documents/Documents.js';

import {
    getPageFrameIDByName,
    getPageByName,
    getCollectionIDByName,
    advMultiCollectionSourceNote,
    getFrameData
} from '../../frames/TableView/TableView.js';
import {
    getFrameFromElement,
    updatePageFrameArray
} from '../../Page.js';

var page_id = await getPageByName('Documents');
var pageFrameId = await getPageFrameIDByName(page_id);

let UploadCollectionID = await getCollectionIDByName('attached_documents');

let collectionID;
let recordID;

var file_upload_url = base_url + "core/document/documentfileuploadDoc";

$('#fileuploadoc').bind('fileuploadsubmit', function(e, data) {

    var document_type_code = $('#document_type_code');
    var keywords = $('#doc_keywords');
    var description = $('#doc_description');
    var doc_url = $("#doc_url");

    var collID = $('#bsit-upload-modal-container').attr('data-collectionID');
    var rowID = $('#bsit-upload-modal-container').attr('data-rowID');

    var collection_id = $("#old_collection_id").val();
    var row_id = $("#old_row_id").val();

    var _id = $('#_id').val();
    var colid = '';
    var rowid = '';
    if (_id != '') {
        rowid = row_id;
        colid = collection_id;
    } else {
        rowid = rowID;
        colid = collID;

    }


    data.formData = {
        collection_id: colid,
        row_id: rowid,
        UploadCollectionID: UploadCollectionID,
        document_type_code: document_type_code.val(),
        keywords: keywords.val(),
        description: description.val(),
        doc_url: doc_url.val(),
        //doc_folder: doc_folder.val(),
    };

    //doc type validation
    if (!data.formData.document_type_code) {
        data.context.find('button').prop('disabled', false);
        document_type_code.focus();
        //alert("Please insert type.")
        $("#document_type_code").addClass("required_border_class");
        return false;
    }

    //url validation
    if (document_type_code.val() && document_type_code.val().toLowerCase() == 'url' && !data.formData.doc_url) {
        data.context.find('button').prop('disabled', false);
        doc_url.focus();
        //  alert("Please insert type.")
        $("#doc_url").addClass("required_border_class");
        return false;
    }

});

$(document).on("click", "#checkValidation", function() {
    var fileCount = $('#uploadFileCount tr').length;
    var documentcheckType = $("#document_type_code").val();
    let doc_url = $("#doc_url").val();
    let collID = $('#bsit-upload-modal-container').attr('data-collectionID');
    let rowID = $('#bsit-upload-modal-container').attr('data-rowID');
    var count = 0;

    var id = $("#_id").val();


    //doc type validation
    if (!documentcheckType) {
        $("#document_type_code").focus();
        $("#document_type_code").addClass("required_border_class");
        return false;
    }

    if (documentcheckType && documentcheckType.toLowerCase() == 'url') {
        //url validation
        if (doc_url == '') {
            $("#doc_url").focus();
            $("#doc_url").addClass("required_border_class");
            return false;
        } else {

            if (id > 0) {
                uploadFileFromUrl();
            } else if (doc_url != '') {
                uploadFileFromUrl();
            }


        }

    } else {
        if (fileCount == 0 && id == '') {
            $("#fileuploadmultiple").closest("div").addClass("required_border_class");
            return false;
        } else {
            if (fileCount == 0 && id > 0) {
                uploadFileFromUrl();
            }

        }
    }
    count = $("#uploadFileCount").find("tr").length;

    //max file limit validation
    if (count > 20) {
        $("#maxFileError").show();
        return false;
    }

    uploadFiles();
});



// show/ hide on change
$("#document_type_code").change(function() {
    var val = $("#document_type_code option").filter(":selected").val();
    showUrlField(val);
});

function showUrlField(val) {
    if (val && val.toLowerCase() == "url") {
        let el = $("#uploadFileCount").find("tr");
        $(el).each(function() {
            $(this).find(".cancel").trigger("click");
        });
        $("#uploadFileCount").html('');
        $("#doc_url_div").show();
        $("#imgUploadBox").hide();

    } else {
        $("#doc_url_div").hide();
        $("#doc_url").val('');
        $("#imgUploadBox").show();
    }
}



function uploadFiles() {
    $("#maxFileError").hide();
    $("#fileuploadmultiple").closest("div").removeClass("required_border_class");
    $("#startuploading").click();
}



$(function() {
    'use strict';
    var fileCount = $('#uploadFileCount tr').length;

    // Initialize the jQuery File Upload widget:
    $('#fileuploadDoc').fileupload({
        url: file_upload_url,
        limitMultiFileUploads: 3,
        limitConcurrentUploads: 3,
        // url: 'server/php/'
    }).on('fileuploadprogress', function(e, data) {
        $('.uploadmsg').hide();
        $('.progress').show();
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if (progress == 100) {
            let td = data.context.find("td:last-child");
            td.find("button.cancel").hide();
            td.find("button.success").show();
        }

    }).on('fileuploadprogressall', function(e, data) {
        var progress_all = parseInt(data.loaded / data.total * 100, 10);
        if (progress_all == 100) {}

    }).on('fileuploaddone', function(e, data) {
        let _id = $('#_id').val();
        if ($('#uploadFileCount tr').length == 1) {
            if (_id != '') {
                var collection_id = $("#old_collection_id").val();
                var row_id = $("#old_row_id").val();

                $('#page_frame_' + pageFrameId).css("display", "none");
                // $('#page_frame_'+pageFrameId).html('');

                Materialize.toast('All file Data Updated successfully!', 5000);
                if (fileCount == 0) {
                    getDocuments(collection_id, row_id);
                }
            } else {
                var collection_id1 = $("#collection_id").val();
                var row_id1 = $("#row_id").val();

                var collID = $('#bsit-upload-modal-container').attr('data-collectionID');
                var rowID = $('#bsit-upload-modal-container').attr('data-rowID');

                //$('#page_frame_'+pageFrameId).html('');
                Materialize.toast('File uploaded successfully!', 5000);
               
                if (fileCount == 0) {

                    getDocuments(collection_id1, row_id1);
                }
            }
        }
    });


    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:

    } else {
        // Load existing files:
        $('#fileuploadDoc').addClass('fileupload-processing');

        $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileuploadDoc').fileupload('option', 'url'),
                dataType: 'json',
                context: $('#fileuploadDoc')[0]
            })
            .always(function() {
                $(this).removeClass('fileupload-processing');
            })
            .done(function(result) {
                $(this)
                    .fileupload('option', 'done')
                    // eslint-disable-next-line new-cap
                    .call(this, $.Event('done'), {
                        result: result
                    });
            });
    }

});