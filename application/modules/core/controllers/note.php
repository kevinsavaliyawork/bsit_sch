<?php
defined('BASEPATH') or exit('No direct script access allowed');

class note extends brain_controller
{
    public function __construct()
    {
        parent::__construct(0);
        $this->load->model('core/Bsit_io', 'API');
        $this->load->library('session');
        $this->load->model('notemodel');

        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function updateNotes()
    {
        $param = $this->input->post();
        if (empty($param)) {
            echo json_encode(["message" => "Invalid Request."]);
            exit;
        }
        echo $this->notemodel->updateNotes($param);
    }
}
