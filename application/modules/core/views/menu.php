<?php error_reporting(0);
    $currentFile = $_SERVER["REQUEST_URI"];
    $parts = Explode('/', $currentFile);

    $currnet_menu = $parts[count($parts) - 1];

    $child_parent = array();
    $parent = array();
    $new_array = array();
    $increment = 0;

    foreach ($menu_data as $key => $resultData) {
        if ($resultData->display_in_menu == 1) {
            array_push($parent, $resultData->parent_id);
            if (in_array($resultData->parent_id, $parent)) {
                if ($resultData->parent_id == 0) {
                    $child_parent['parent'][$resultData->ModuleID] = $resultData;
                    $child_parent['parent'][$resultData->ModuleID]->child = 'No';
                } else {
                    $child_parent['child'][$resultData->parent_id][$increment] = $resultData;
                    $child_parent['parent'][$resultData->parent_id]->child = 'Yes';

                    $parts1 = Explode('/', $resultData->ModuleName);
                    $currnet_menu_check = $parts1[count($parts1) - 1];

                    if ($currnet_menu_check == $currnet_menu) {
                        $new_array[$resultData->parent_id]->open_child = 'Yes';
                    }
                }
                $increment++;
            }
        }
    }

    $menu_cnt = 0;
?>
<div id="sidedrawer-brand" class="mui--appbar-line-height mui--text-title">
    <a href="" target="_self">
      <img src="<?php echo base_url(); ?>assets/core/images/left_side_menu/<?php echo $this->session->userdata('appIcon')?>" class="logo-nav-menu"/>
    </a>
	<?php echo($this->session->userdata('appTitle'))?>
  </div>
  <div class="mui-divider"></div>
  
<ul class="content mCustomScrollbar">
<?php
    foreach ($child_parent['parent'] as $key => $menu_item) {
        if ($menu_item->child == 'Yes') {
            $menu_cnt++; ?>
        <li>
          <?php
            if ($menu_item->child == 'No') {
                ?>
                <a href="<?php echo(base_url());
                echo($menu_item->ModuleName); ?>"
                  class="<?php echo $class_pa; ?>" style="padding:0"
                >
          <?php
            } ?>

            <div class="main_menu_item bsit-menu-header">
            <div class="menu_item_icon">
                  <?php
                    if (file_exists("assets/core/images/left_side_menu/".$menu_item->ModuleName.".png")) {
                        ?>
                       
  
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/<?php echo($menu_item->ModuleName); ?>.png" class="image">
                    <?php
                    } elseif (file_exists("assets/core/images/left_side_menu/".str_replace("page/", "", $menu_item->ModuleName).".png")) {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/<?php echo(str_replace("page/", "", $menu_item->ModuleName)); ?>.png" class="image">
                    <?php
                    } else {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/methods.png" class="image">
                    <?php
                    } ?>
                </div>

                <div class="menu_item_text">
                    <p class="hasTooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo($menu_item->display_name); ?>">
                        <?php echo($menu_item->display_name); ?>
                    </p>
                </div>
                </div>
                <?php
                  $addsub = 'sub-menu-data';
            if ($menu_item->child == 'No') {
                $addsub = 'no-sub-menu-data'; ?>
                </a>
                <?php
            } ?>
                </strong>

                <?php
                if (isset($new_array[$key])) {
                    if ($new_array[$key]->open_child == 'Yes') {
                        $style_data = 'display: block;';
                    }
                } else {
                    $style_data = 'display: none;';
                } ?>
          <ul class="<?php echo $addsub; ?> " style="<?php echo $style_data; ?>">

            <?php foreach ($child_parent['child'][$menu_item->ModuleID] as $sub_menu_item) {
                    $parts1 = Explode('/', $sub_menu_item->ModuleName);
                    $currnet_menu_check = $parts1[count($parts1) - 1];

                    $class = "mdl-navigation__link";
                    if ($currnet_menu_check == $currnet_menu) {
                        $class = "mdl-navigation__link active";
                    } ?>

            <li>


            <a href="<?php echo(base_url());
                    echo($sub_menu_item->ModuleName); ?>" class="<?php echo $class; ?>" style="padding:0" >
            <div class="main_menu_item">
                <div class="menu_item_icon">
                  <?php
                    if (file_exists("assets/core/images/left_side_menu/".$sub_menu_item->ModuleName.".png")) {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/<?php echo($sub_menu_item->ModuleName); ?>.png" class="image">
                    <?php
                    } elseif (file_exists("assets/core/images/left_side_menu/".str_replace("page/", "", $sub_menu_item->ModuleName).".png")) {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/<?php echo(str_replace("page/", "", $sub_menu_item->ModuleName)); ?>.png" class="image">
                    <?php
                    } else {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/methods.png" class="image">
                    <?php
                    } ?>
                </div>
                <div class="menu_item_text">
                    <p class="hasTooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo($menu_item->display_name); ?>">
                        <?php echo($sub_menu_item->display_name); ?>
              <?php if ($sub_menu_item->description != '') {
                        ?>
              <?php
                        if ($menu_cnt == $sub_menu_item->total_count || $menu_cnt == ($sub_menu_item->total_count-1)) {
                            ?><span class="last_menu_item"><?php echo $sub_menu_item->description; ?></span><?php
                        } else {
                            ?><span><?php echo $sub_menu_item->description; ?></span><?php
                        } ?>
              <?php
                    } ?>
                    </p>
                </div>
            </div>
        </a>

            </li>
            <?php
                } ?>

          </ul>
        </li>

        <?php
        } else {
            ?>

        <li>
          <a href="<?php echo(base_url());
            echo($menu_item->ModuleName); ?>" class="mdl-navigation__link" style="padding:0" >
            <div class="main_menu_item">
            <div class="menu_item_icon">
                  <?php
                    if (file_exists("assets/core/images/".$menu_item->ModuleName.".png")) {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/<?php echo($menu_item->ModuleName); ?>.png" class="image">
                    <?php
                    } elseif (file_exists("assets/core/images/left_side_menu/".str_replace("page/", "", $menu_item->ModuleName).".png")) {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/<?php echo(str_replace("page/", "", $menu_item->ModuleName)); ?>.png" class="image">
                    <?php
                    } else {
                        ?>
                      <img src="<?php echo(base_url()); ?>assets/core/images/left_side_menu/methods.png" class="image">
                    <?php
                    } ?>
                </div>


                <div class="menu_item_text">
                    <p class="hasTooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo($menu_item->display_name); ?>">
                        <?php echo($menu_item->display_name); ?>
                    </p>
                </div>

                </div>
                <?php $addsub = 'sub-menu-data';
            if ($menu_item->child == 'No') {
                $addsub = 'no-sub-menu-data'; ?>
                </a>
                <?php
            } ?>

        </li>

        <?php
        } ?>

        <?php
    }
?>
    <li>
        
    </li>
</ul>

<footer id="footer">
          <div class="mui-container-fluid">
            Copyright © BrainStorm IT <!-- <?php echo(date("Y"));?> -->
          </div>
        </footer>


