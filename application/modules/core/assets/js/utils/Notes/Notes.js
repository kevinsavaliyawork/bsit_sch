import { getCollectionIDByName, advMultiCollectionSourceNote } from '../../frames/TableView/TableView.js';
import { getFrameFromElement } from '../../Page.js';

let notesCollectionID = await getCollectionIDByName('notes');

async function showNotesModal(event,type) {


  type = (type && type == "edit") ? 'edit' : 'add';

  let frame = getFrameFromElement(event.target);
  let collectionID = $(event.currentTarget).data('collid');
  let recordID = $(event.currentTarget).data('recid');
  let noteID = '';
  let noteList = '';
  if (type == 'edit') {
	   if($(event.currentTarget).data('noteeditid')){
    noteID = $(event.currentTarget).data('recid');  
    recordID = $(event.currentTarget).data('noteeditid');
    }else{
    noteID = $(event.currentTarget).data('noteid');  
    }
    //noteID = $(event.currentTarget).data('noteid');
  }
  
   //reset field
  resetNoteFields();
  
  // Assign variables to the modal
  $('#bsit-notes-modal-container').attr('data-collectionID', collectionID);
  $('#bsit-notes-modal-container').attr('data-rowID', recordID);

  // Clear notes that may have been in there previously.
  $('#bsit-notes-container').html('');
  $('#bsit-notes-container').css('display', 'none');
  
  // Show the modal
  $('#bsit-notes-modal').modal();
  onclickClose();
   if (type == 'add') {
    noteList = await getNotes(collectionID, recordID).catch(() => undefined);
  }
	
	 var urlCheck = window.location.pathname;
      if(urlCheck.indexOf('page/Schedules') > -1){
        $('.loader').hide();
      }
  /*let frameTitle = $(`#page_frame_${frame.PFID}_header h3`).text();

  if (frameTitle != '') {
    $('#bsit-notes-modal-header').html(`Notes - ${frameTitle}`);
  } else {
    $('#bsit-notes-modal-header').html(`Notes - ${page_title}`);
  }*/

  if (frame && frame.PFID) {
    let frameTitle = $(`#page_frame_${frame.PFID}_header h3`).text();

    if (frameTitle != '') {
      $('#bsit-notes-modal-header').html(`Notes - ${frameTitle}`);
    } else {
      $('#bsit-notes-modal-header').html(`Notes - ${page_title}`);
    }
  }

  // Check we got notes to display
  if (typeof noteList != 'undefined' && type == 'add') {
    // Get HTML for, and display each note.
    noteList.notes.forEach((note) => {
       let noteHTML = buildNoteHtml(note, frame);
      //let noteHTML = buildNoteHtml(note.createBy, note.createDate, note.note, note.note_header,note.notify_users);

      $('#bsit-notes-container').css('display', 'block');

      // Add html to the note container
      $('#bsit-notes-container').prepend(noteHTML);
      //$('#bsit-notes-container').append(noteHTML);
    });
  }

  //display note for edit
  if (type == 'edit') {
    getNoteData(collectionID, recordID, noteID);
  }

}

//edit note

function getNoteData(collectionID, recordID, noteID) {
  let requestData = {
    data: JSON.stringify([{
      collID: notesCollectionID,
      filter: `nt._id = ${noteID} AND nt.row_id = ${recordID} AND nt.coll_id = ${collectionID}`,
    }]),
  };

  // load the single records details from the server
  $.ajax({
    type: 'POST',
    url: base_url + 'core/Apilocal/loadfiltered',
    data: requestData,
    dataType: 'json',
    success: function (res) {
      if (res && typeof res[0]['Results'][0] != 'undefined') {
        let data = res[0]['Results'][0];
        $("#note_id").val(data.NoteId);
        $("#note_header").val(data.note_header);
        $("#bsit-notes-select").val(data.NType);
        var editor = CKEDITOR.instances.editor;
        CKEDITOR.instances.editor.setData(data.Note);
        var notify_users = data.notify_users;

        var uid = [];
        var uname = [];
        if (notify_users) {
          $.each(notify_users.split(","), function (index, val) {
            var user = val.split(':');
            uid.push(user[0]);
            uname.push(user[1]);
            $(".notemultiSelect-ul li:eq(0)").before('<li><span class="del-user-multi-select-val" data-lebal="' + user[1] + '">X</span>' + user[1] + '</li>');
          });
        }

        if (uname && uid) {
          $("#hf_advanceMultiSelectNote").val(uname.join(','));
          $("#hf_advanceMultiSelectNote_value").val(uid.join(','));
        }

      }
    }
  });
}

async function getNotes(collectionID, recordID) {
  let notesCollectionID = await getCollectionIDByName('notes');

  let requestData = {
    data: JSON.stringify([
      {
        collID: notesCollectionID,
        filter: `nt.row_id = ${recordID} AND nt.coll_id = ${collectionID}`,
      },
    ]),
  };
  let url = base_url + 'core/apilocal/loadfiltered';

  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'POST',
      url: url,
      async: false,
      data: requestData,
      dataType: 'json',
      success: function (res) {
        if (typeof res[0]['Results'][0] != 'undefined') {
          let results = res[0]['Results'];

          let notes = results.map((val) => {
            return {
              createBy: val.username,
              createDate: val.create_date,
              note: val.Note,
              note_header: val.note_header,
              notify_users: val.notify_users
            };
          });

          let notesToReturn = {
            collectionName: results[0].collectionName,
            notes: results,
          };

          resolve(notesToReturn);
        } else {
          reject(undefined);
        }
      },
      error: function (err) {
        alert(err.responseText);
        reject(err);
      },
    });
  });
}


// add note
function saveNote() {
  //validation for Note Heading
  let note_header = $("#note_header").val().trim();
  /*if (note_header == "") {
    $("#note_header").addClass("required_border_class");
    return false;
  } else {
    $("#note_header").removeClass("required_border_class");
  }*/

  // Get what was typed to save as the note
  let note = $('#editor').val();
  if (note.length == 0) {
     $("div#cke_editor").addClass("required_border_class");
   // $(".ck-editor__editable_inline").addClass("required_border_class");
    return false;
  }

  let notify_user_val = $('#hf_advanceMultiSelectNote_value').val().split(",");
  let notify_user_label = $('#hf_advanceMultiSelectNote').val().split(",");
  let collID = $('#bsit-notes-modal-container').attr('data-collectionID');
  let rowID = $('#bsit-notes-modal-container').attr('data-rowID');
  let noteType = $('#bsit-notes-select').val();
  // Set up the SP call with the new values to update the db
  let userNotifyValue = "";

  if ($("#hf_advanceMultiSelectNote").val().length != 0) {
    $.each($("#hf_advanceMultiSelectNote").val().split(","), function (i, v) {
      if (userNotifyValue.length == 0) {
        userNotifyValue = notify_user_val[i] + ":" + notify_user_label[i];
      } else {
        userNotifyValue = userNotifyValue + "," + notify_user_val[i] + ":" + notify_user_label[i];
      }
    });
  }

  let notify_users = $.trim(userNotifyValue);
  // console.log(notify_users);
  var regex = /(?:^<p[^>]*>)|(?:<\/p>$)/g;
      var htmlStr = note;  
      var notdata= htmlStr.replace(regex, "");

  let noteSaveRequest = {
    parameters: [
      {
        key: 'collectionID',
        value: collID,
      },
      {
        key: 'rowID',
        value: rowID,
      },
      {
        key: 'note_header',
        value: note_header,
      },
      {
        key: 'note',
        value:note,
      },
      {
        key: 'noteType',
        value: noteType,
      },
      {
        key: 'notify_users',
        value: notify_users,
      },
      {
        key: 'user_id',
        value: userID,
      },
    ],
    sp_name: [
      {
        key: 'bsit_addnewnote',
      },
    ],
  };

   //console.log(noteSaveRequest);
  //return false;
  let url = base_url + 'core/apilocal/call_method';

  // Save the note
  $.ajax({
    type: 'POST',
    url: url,
    data: noteSaveRequest,
    dataType: 'json',
    success: function (res) {
      console.log(res);
      let data = res[0];

      resetNoteFields();

      //reset note header
      //$("#note_header").val('');
      //reset textrea
      //$('iframe').contents().find('body').empty();
      // $('.notemultiSelect-ul li:not(:last)').remove();
      var obj_data = $(".notemultiSelect-ul").find("li");
      $('#hf_advanceMultiSelectNote').val('');
      $('#hf_advanceMultiSelectNote_value').val('');

      $.each(obj_data, function (i, e) {
        var classname = $(this).attr("class");
        if (classname != "li-inputnote") {
          $(this).remove();
        }
        else {
          $(this).find("input").val("");
        }
      });
      // debugger
      // Insert the note into the DOM
    /*  let newNoteHTML = buildNoteHtml(
        data.username,
        data.create_date,
        data.note,
        data.note_header,
        data.notify_users
      );*/

      let newNoteHTML = buildNoteHtml(data);

      $('#bsit-notes-container').prepend(newNoteHTML);
      //$('#bsit-notes-container').append(newNoteHTML);

      if ($('#bsit-notes-container').css('display') == 'none') {
        // Show notes container if it was hidden
        $('#bsit-notes-container').css('display', 'block');
      }

      // Clear and refocus the input box;
     // window.editor.setData('');
      
      


     // updateNoteList();
      // window.editor.setdata('');
      // $('#bsit-note-input')
      //   .val('')
      //   .focus();
    },
  });
}
function resetNoteFields() {
  //reset ckeditor
  $('iframe').contents().find('body').empty();
  
 //on load destroy 
  if(CKEDITOR.instances.editor)
  CKEDITOR.instances.editor.destroy();

// reinistial ckediter
CKEDITOR.replace( 'editor', {
  height: 300,
  extraPlugins: 'emoji,justify,font,colorbutton',
    toolbar: [
        ['ajaxsave'],
        ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'EmojiPanel'],//'-', 'Link', 'Unlink',
        [ 'Copy', 'Paste', 'PasteText'],//'Cut',
        ['Undo', 'Redo', '-', 'RemoveFormat'],
        ['TextColor', 'BGColor'],
       // ['Maximize']//, 'Image'
    ],
  
  //filebrowserImageBrowseUrl : "<?php echo base_url(); ?>core/apilocal/file_browser",
  filebrowserUploadUrl: '<?php echo base_url(); ?>core/apilocal/notesUpload',
  filebrowserUploadMethod: "form"

 });

  $("#note_id").val('');

  //reset note header
  $("#note_header").val('');

  

  $("#bsit-notes-select").val('1');

  // $('.notemultiSelect-ul li:not(:last)').remove();
  var obj_data = $(".notemultiSelect-ul").find("li");
  $('#hf_advanceMultiSelectNote').val('');
  $('#hf_advanceMultiSelectNote_value').val('');

  $.each(obj_data, function (i, e) {
    var classname = $(this).attr("class");
    if (classname != "li-inputnote") {
      $(this).remove();
    } else {
      $(this).find("input").val("");
    }
  });
}


function convertTZ(date, tzString) {
    return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));   
}

function buildNoteHtml(noteData, frame) {
  let noteID = (noteData.NoteId) ? noteData.NoteId : noteData._id;
  let coll_id = (noteData.CollectionId) ? noteData.CollectionId : noteData.coll_id;
  let note = (noteData.Note) ? noteData.Note : noteData.note;
  let notify_users = (noteData.notify_users) ? noteData.notify_users : noteData.notify_users;
  let recid = (noteData.row_id) ? noteData.row_id : noteData.RowId;
  let update_date = (noteData.key_update_date) ? noteData.key_update_date : noteData.update_date;
  let frameData = (frame) ? frame : '';
  let pfid = (frameData && frameData.PFID) ? frameData.PFID : '';

  // Format urls so they can be highlighted and clicked
  note = linkifyHtml(note, {
    attributes: {
      rel: 'nofollow',
    },
    defaultProtocol: 'https',
  });

  //var currentDateTime = new Date(noteData.create_date + "+00:00");
  var currentDateTime = new Date(update_date + "+00:00");
  
  // converdatetime zone based on australia
  var convertedDate = convertTZ(currentDateTime, "Australia/NSW");
  
  let noteHeader = noteData.note_header;
  if(noteData.note_header == undefined){
	noteHeader='';  
  }
  
  if (noteHeader) {
    noteHeader = `<br/><strong>${noteHeader}</strong>`;
  }

  let noteEditBtn = '';

  //if (pfid && pfid == 34) {
    noteEditBtn = `<a href="javascript:void(0);" class="bsit-edit-notes-btn noteEditbtn" data-noteid="${noteID}" data-collid="${coll_id}" data-recid="${recid}" title="Edit"><span><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:20px;color: #2196f3;"></i></span></a>`
  //}
//<img src="${BASEURL_IMAGE}/edit.png" >
  let newNote =
    `<div class="mui-panel mui-col-md-12">` +
    `<span style="color: #999999;" class="mui--divider-right cola_word_barck">` +
    `<strong>${moment(convertedDate).format('LLL')}` +
    ` | ${noteData.username}:</strong>` +
    noteEditBtn +
    `</span>` +
    noteHeader +
    `<br/>${note} <span style="color: #999999;font-size: 14px;"><br/>Notify to - ${notify_users} </span> </div>`;

  return newNote;
}

$(function () {
  $(".advanceMultiSelectNote")
    .bind("keydown", function (event) {
      if (event.keyCode === $.ui.keyCode.TAB &&
        $(this).data("ui-autocomplete").menu.active) {
        event.preventDefault();
      }
    })
    .autocomplete({
      minLength: 1,
      source: function (request, response, event) {
        advMultiCollectionSourceNote(request, response, $(this));
      },
      focus: function () {
        // prevent value inserted on focus
        return false;
      },
      select: function (event, ui) {
        // debugger
        var selectlab = ui.item.label;
        var selectval = ui.item.value;
        var selectval_value = ui.item.value.toString();
        var select_advanceMultiValue = $("#hf_advanceMultiSelectNote_value").val().split(",");

        if (jQuery.inArray(selectval_value, select_advanceMultiValue) != -1) {
          $('.advanceMultiSelectNote').val();
          return false;
        }

        if ($('#hf_advanceMultiSelectNote').val().length == 0) {
          selectlab = $('#hf_advanceMultiSelectNote').val() + $.trim(selectlab);
          $("#hf_advanceMultiSelectNote").val($.trim(selectlab));
          selectval = $('#hf_advanceMultiSelectNote_value').val() + $.trim(selectval);
          $("#hf_advanceMultiSelectNote_value").val($.trim(selectval));
        } else {
          selectlab = $('#hf_advanceMultiSelectNote').val() + ',' + $.trim(selectlab);
          $("#hf_advanceMultiSelectNote").val($.trim(selectlab));
          selectval = $('#hf_advanceMultiSelectNote_value').val() + ',' + $.trim(selectval);
          $("#hf_advanceMultiSelectNote_value").val($.trim(selectval));
        }



        var obj_data = jQuery(".notemultiSelect-ul").find("li");
        // alert($("#hf_advanceMultiSelectNote_value").val());
        $.each(obj_data, function (i, e) {
          var classname = $(this).attr("class");
          if (classname != "li-inputnote") {
            $(this).remove();
          }
          else {
            $(this).find("input").val("");
          }
        });

        $.each($("#hf_advanceMultiSelectNote").val().split(","), function (i, v) {

          if (v != "") {

            jQuery(".notemultiSelect-ul li:eq(0)").before('<li><span class="del-user-multi-select-val" data-lebal="' + v + '">X</span>' + v + '</li>');
          }
        });


        return false;
      }
    }).autocomplete("instance")._renderItem = function (ul, item) {
      return $("<li>")
        .append("<div>" + item.label + ", " + item.value + "</div>")
        .appendTo(ul);
    };
  function split(val) {
    return val.split(/,\s*/);
  }
  function extractLast(term) {
    return split(term).pop();
  }
});
//MULTI SELECT ADD REMOVE CODE
jQuery(document).on("click", ".del-user-multi-select-val", function () {
  // alert($('#hf_advanceMultiSelectNote_value').val());
  var val = $(this).attr("data-lebal");

  var hf = "";
  var hf_val = "";
  var obj_list = $('#hf_advanceMultiSelectNote_value').val().split(',');
  var obj_data = jQuery(".notemultiSelect-ul").find("li");

  $.each(obj_data, function (i, e) {

    var classname = $(this).attr("class");
    if (classname != "li-inputnote") {
      $(this).remove();
    }
    else {
      $(this).find("input").val("");
    }
  });

  $.each($("#hf_advanceMultiSelectNote").val().split(","), function (i, v) {
    if (v != "") {
      if (v != val) {
        hf = hf + "," + v;
        hf_val = hf_val + "," + obj_list[i];
        jQuery(".notemultiSelect-ul li:eq(0)").before('<li><span class="del-user-multi-select-val" data-lebal="' + v + '">X</span>' + v + '</li>');
      }

    }
  });

  $("#hf_advanceMultiSelectNote").val(hf);
  $("#hf_advanceMultiSelectNote_value").val(hf_val);
});




// Notes save / enter
$(document).on('submit', '#bsit-notes-form', function (e) {
  e.preventDefault();
  let note_id = $("#note_id").val();
  if (note_id != '') {
    editNote();
  } else {
    saveNote();
  }
});

// Change note type
$(document).on('change', '#bsit-notes-select', function (e) {
  // Update the selected option.
  $('#bsit-notes-select')
    .val(e.target.value)
    .prop('selected', true);
});

// Showing notes modal
$(document)
  .button()
  .on('click', '.bsit-notes-btn', function (e) {
     e.stopPropagation();
    var urlCheck = window.location.pathname;
      if(urlCheck.indexOf('page/Schedules') > -1){
        $('.loader').show();
        showNotesModal(e,"add");
      } else{
        showNotesModal(e,"add");
      }
    // showNotesModal(e,"add");
  });

 // Showing edit notes modal
$(document)
  .on('click', '.bsit-edit-notes-btn', function (e) {
    showNotesModal(e, "edit");
  }); 

$(document)
  .button()
  .on('click', '.bsit_form_close_btn', function () {

    var obj_data = $(".notemultiSelect-ul").find("li");
    $('#hf_advanceMultiSelectNote').val('');
    $('#hf_advanceMultiSelectNote_value').val('');

    $.each(obj_data, function (i, e) {
      var classname = $(this).attr("class");
      if (classname != "li-inputnote") {
        $(this).remove();
      }
      else {
        $(this).find("input").val("");
      }
    });

  });
function onclickClose() {
  $(`.modal-backdrop`).on('click', function (e) {

    e.preventDefault();
    e.stopPropagation();
    return false;
  });
}

function getNoteParam() {
  //validation for Note Heading
  let note_id = $("#note_id").val();
  let note_header = $("#note_header").val().trim();

  // Get what was typed to save as the note
  let note = $('#editor').val();

  let notify_user_val = $('#hf_advanceMultiSelectNote_value').val().split(",");
  let notify_user_label = $('#hf_advanceMultiSelectNote').val().split(",");
  let collID = $('#bsit-notes-modal-container').attr('data-collectionID');
  let rowID = $('#bsit-notes-modal-container').attr('data-rowID');
  let noteType = $('#bsit-notes-select').val();
  // Set up the SP call with the new values to update the db
  let userNotifyValue = "";

  if ($("#hf_advanceMultiSelectNote").val().length != 0) {
    $.each($("#hf_advanceMultiSelectNote").val().split(","), function (i, v) {
      if (userNotifyValue.length == 0) {
        userNotifyValue = notify_user_val[i] + ":" + notify_user_label[i];
      } else {
        userNotifyValue = userNotifyValue + "," + notify_user_val[i] + ":" + notify_user_label[i];
      }
    });
  }

  let notify_users = $.trim(userNotifyValue);

  var reqParam = {
    "note_header": note_header,
    "note": note,
    "note_id": note_id,
    "noteType": noteType,
    "notify_users": notify_users,
    "collID": collID,
    "rowID": rowID,
    "userID": userID,
  };

  return reqParam;
}

function validateNoteFormData() {
  let note_header = $("#note_header").val().trim();
  /*if (note_header == "") {
    $("#note_header").addClass("required_border_class");
    return false;
  } else {
    $("#note_header").removeClass("required_border_class");
  }*/
var messageLength = CKEDITOR.instances['editor'].getData().replace(/<[^>]*>/gi, '').length;
      if( !messageLength ) {
          alert( 'Please enter a message' );
          e.preventDefault();
       }
  let note = $('#editor').val();
  if (note.length == 0) {
    // $("#editor").addClass("required_border_class");
    $("div#cke_editor ").addClass("required_border_class");
    return false;
  }
}

async function editNote() {

  let validateNoteForm = validateNoteFormData();
  if (validateNoteForm == false) {
    return false;
  }

  let reqParam = getNoteParam();

  if (reqParam.note_id == '') {
    alert("Cannot save note! Please contact admin.");
    return false;
  }

 let param = {
    note_id: reqParam.note_id,
    note: reqParam.note,
    note_type: reqParam.noteType,
    note_header: reqParam.note_header,
    notify_users: reqParam.notify_users,
  };

  $.ajax({
    type: "POST",
    url: base_url + "core/apilocal/updateNotes",
    data: param,
    dataType: 'json',
    success: function (response) {
      $('#bsit-notes-modal').modal('hide');
      Materialize.toast('Note Updated Successfully!', 3000);
    },
      error: function(err) {
        console.log(err);
      }
  });
 }


