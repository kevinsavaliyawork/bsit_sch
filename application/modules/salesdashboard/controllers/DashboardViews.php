<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardViews extends brain_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('module');
		$this->load->model('Bsit_io','API');
	}

	function index($data_pass){
		// load the infomation
		$userID = $this->session->userdata('UserID');
		$name_first = $this->session->userdata('FirstName');
		$tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules($userID);

		$data_pass['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);

		// get the users ID so its available in the page
		$data_pass['userid'] = $this->session->userdata('UserID');

		// get the list of users in the system
		$USRCURLDATA = Array('collections' =>
								json_encode(
									Array(
										'colls'=> Array(
										    // user list
											Array(
												'coll_id'	=> '16',
												//'filter'	=> ""
												'filter'	=> "([usr].group_id <> 8 AND [sch]._id <> 3 AND [usr].is_active = 1)" // Ignore inactive users
											),
											// Customer list
											Array(
												'coll_id'	=> '12',
												'order'		=> 'o.name', // TaskId-38[Sort customer drop down]
												'filter'	=> ""
											),
											// job list
											Array(
												'coll_id'	=> '1',
												'cols'		=> Array('jobid', 'ParentJobID', 'name', 'desc', 'CustomerID'),
												'filter'	=> "jb.status <> 'Complete'"
											),
											// Work Type list
											Array(
												'coll_id'	=> '21',
												'cols'		=> Array('WorkTypeId', 'Name'),
												'filter'	=> ""
											),
											// SharedEvents list
											Array(
												'coll_id'	=> '31',
												'cols'		=> Array('SharedEventId', 'EventId', 'UserId'),
												'filter'	=> ""
											),
										)
									)
								)
							);
		// call the API
		$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
		$users_result = $this->API->CallAPI("GET", $BASEURLMETHOD, $USRCURLDATA);
		// convert JSON to array/object

		$users_result = json_decode($users_result);
		if(isset($users_result->Data)) {
			$users_result = $users_result->Data;
		}

		// Users
		$data_pass['curlDataUsr'] = $users_result[0]->Results;
		$this->load->view('salesdashboard/dashboard_params', $data_pass);
		//return $this->load->view($data_pass['module_name'].'/dashboard_params',$data_pass,true);
		
	}
	
	function pipeline_incomestream($data_pass){
		return $this->load->view('salesdashboard/pipeline_by_incomestream', $data_pass,true);
	}
	
	function pipeline_owner($data_pass){
		return $this->load->view('salesdashboard/pipeline_by_owner', $data_pass,true);
	}
	
	function pipeline_source($data_pass){
		
	
		return $this->load->view('salesdashboard/pipeline_by_source', $data_pass,true);
	}
	
	function pipeline_stage($data_pass){
		return $this->load->view('salesdashboard/pipeline_by_stage', $data_pass,true);
	}
	
	
	function weekly_status($data_pass){
		
		$loadData = $data_pass['load_data'];
				
				foreach($loadData as $key=>$data){
					
					if($key == 0){
						$firstArray = json_decode($data->data);
					} else {
						$secondArray = json_decode($data->data);
					}
					
				}
		// debug($firstArray[8]->status_title); 
		// 		exit;
				foreach($firstArray as $firstKey=> $firstArrayValue){
					
					$finalarray[$firstArrayValue->status_title]['opp_count'] = $firstArrayValue->opp_count - $secondArray[$firstKey]->opp_count;
					$finalarray[$firstArrayValue->status_title]['total_value'] = $firstArrayValue->total_value - $secondArray[$firstKey]->total_value;
				}
				
				
				$data_pass['Assessment'] = $firstArray[0]->status_title;
				$data_pass['Assessment_opp_count'] = $firstArray[0]->opp_count - $secondArray[0]->opp_count;
				$data_pass['Assessment_total_value'] =  $firstArray[0]->total_value - $secondArray[0]->total_value;

				$data_pass['Complete'] = $firstArray[1]->status_title;
				$data_pass['Complete_opp_count'] = $firstArray[1]->opp_count - $secondArray[1]->opp_count;
				$data_pass['Complete_total_value'] = $firstArray[1]->total_value - $secondArray[1]->total_value;
				
				$data_pass['Deal'] = $firstArray[2]->status_title;
				$data_pass['Deal_opp_count'] = $firstArray[2]->opp_count - $secondArray[2]->opp_count;
				$data_pass['Deal_total_value'] = $firstArray[2]->total_value - $secondArray[2]->total_value;

				$data_pass['Discovery'] = $firstArray[3]->status_title;
				$data_pass['Discovery_opp_count'] = $firstArray[3]->opp_count - $secondArray[3]->opp_count;
				$data_pass['Discovery_total_value'] = $firstArray[3]->total_value - $secondArray[3]->total_value;

				$data_pass['Lead_Idea'] = $firstArray[4]->status_title;
				$data_pass['Lead_Idea_opp_count'] = $firstArray[4]->opp_count - $secondArray[4]->opp_count;
				$data_pass['Lead_Idea_total_value'] = $firstArray[4]->total_value - $secondArray[4]->total_value;		

				$data_pass['Lost'] = $firstArray[5]->status_title;
				$data_pass['Lost_opp_count'] = $firstArray[5]->opp_count - $secondArray[5]->opp_count;
				$data_pass['Lost_total_value'] = $firstArray[5]->total_value - $secondArray[5]->total_value;
				
				$data_pass['Proposal'] = $firstArray[6]->status_title;
				$data_pass['Proposal_opp_count'] = $firstArray[6]->opp_count - $secondArray[6]->opp_count;
				$data_pass['Proposal_total_value'] = $firstArray[6]->total_value - $secondArray[6]->total_value;

				$data_pass['Qualified'] = $firstArray[7]->status_title;
				$data_pass['Qualified_opp_count'] = $firstArray[7]->opp_count - $secondArray[7]->opp_count;
				$data_pass['Qualified_total_value'] = $firstArray[7]->total_value - $secondArray[7]->total_value;

				
						
				$data_pass['final_array'] = $finalarray;
				
				return $this->load->view('salesdashboard/weekly_status', $data_pass,true);
	}

	
}