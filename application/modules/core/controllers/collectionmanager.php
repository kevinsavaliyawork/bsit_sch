<?php
defined('BASEPATH') or exit('No direct script access allowed');

class collectionmanager extends brain_controller
{
    public function __construct()
    {

        parent::__construct(0);
        $this->load->model('module');
        $this->load->model('collectionmodel');

        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

        // check if the user is 'logged in', if not, redirect to login page.
        $userID = $this->session->userdata('UserID');
        $loggedIn = $this->session->userdata('logged_in');
        if ($loggedIn == FALSE or $userID == "") {
            // redirect to the login page
            redirect('/login');
        }
    }


    public function index()
    {

        // load the infomation
        $userID = $this->session->userdata('UserID');
        $tmp_data['menu_data'] = $menu  = $this->module->getClientActiveModulesPermission($userID);

        $data['page_data_permission'] = $tmp_data['menu_data'];


        $data['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);


        $data['header_view'] = 1;

        $data['footer_view'] = 1;


        $data['pageMenu'] = 1;
        $this->load->view('header', $data);
        $this->load->view('content', $data);

        if ($_GET['CollID'] != '') {


            $json_page_result_COLL = $this->collectionmodel->getCollectionDataOfId($_GET['CollID']);

            $data_all['collection_name'] = $json_page_result_COLL[0]->Results[0]->Name;

            $page_result_data = $this->collectionmodel->getCollectiontablesAndProperties($_GET['CollID']);

            foreach ($page_result_data as $key => $value) {
                $data_array[$value->table_id . '$$' . $value->table_name][] = $value;
            }

            $data_all['default_property'] = $data_array;
        }


        $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(DEFAULT_COLLECTION_PAGEID);

        $data_all['dataframeid'] = $json_page_result_COLL[0]->Results[0]->CollID;

        $this->load->view('collectionmanager', $data_all);
        $this->load->view('footer');
    }

    public function tableAll()
    {

        $json_page_result = $this->collectionmodel->getCollectionTables();

        echo $json_page_result;
    }


    public function sqlAll()
    {
        $collectionId = $_POST['collection_id'];
        $pageId = $_POST['pageId'];
        if ($collectionId == '') {

            $page_result = $this->collectionmodel->addCollection($_POST['collection_name']);




            $collectionId = $page_result[0]->_id;

            if ($pageId > 0) {

                $json_page_resultAll = $this->collectionmodel->addPageFrame($pageId, $collectionId);
            }

            $collection_array = array(
                'Data' =>
                array(
                    0 => array(
                        '_id' => $collectionId,
                    ),
                ),
                'HasError' => false,
                'ErrorCode' => 0,
                'ErrorMessage' => null,
            );

            $json_page_resultCollection = json_encode($collection_array);
        } else {
            $collection_array = array(
                'Data' =>
                array(
                    0 => array(
                        '_id' => $collectionId,
                    ),
                ),
                'HasError' => false,
                'ErrorCode' => 0,
                'ErrorMessage' => null,
            );

            $json_page_resultCollection = json_encode($collection_array);
        }


        $json_page_resultAll = $this->collectionmodel->addTableCollection($collectionId, $_POST['tablevalue']);

        $json_page_result = $this->collectionmodel->getColumnsFromTablesColection();


        if ($_POST['collection_id'] == '') {
            foreach (json_decode($json_page_resultAll, true) as $key => $array) {
                $r[$key] = array_merge(json_decode($json_page_result, true)[$key], json_decode($json_page_resultCollection, true)[$key], $array);
            }
        } else {
            foreach (json_decode($json_page_resultAll, true) as $key => $array) {
                $r[$key] = array_merge(json_decode($json_page_result, true)[$key], json_decode($json_page_resultCollection, true)[$key], $array);
            }
        }

        echo json_encode($r);
    }


    public function addEditProperty()
    {
        $json_page_result = $this->collectionmodel->addEditPropertyToCollection($_POST);
        echo $json_page_result;
    }

    public function showPropertyData()
    {
        if ($_POST['type'] == 'property') {


            $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(PROPERTY_COLLECTION);

            $collectionId = $json_page_result_COLL[0]->Results[0]->CollID;

            $column_name = 'prop._id';

            $json_page_result_FRAME = $this->collectionmodel->getFrameDataByName(PROPERTY_FRAMEID);

            $frameId = $json_page_result_FRAME[0]->Results[0]->frameID;

            $page_result_prop = $this->collectionmodel->getFrameProperties($frameId);


            $data_all['property_data'] = $page_result_prop;
            $data_all['class_name'] = 'mdl-cell mdl-cell--4-col collec-properties coll-border';
            $data_all['static_coll_id'] = $collectionId;
            $data_all['id'] = $_POST['id'];
        } elseif ($_POST['type'] == 'table') {


            $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(TABLE_COLLECTION);


            $collectionId = $json_page_result_COLL[0]->Results[0]->CollID;

            $column_name = 'tbl._id';

            $json_page_result_FRAME = $this->collectionmodel->getFrameDataByName(TABLE_FRAMEID);


            $frameId = $json_page_result_FRAME[0]->Results[0]->frameID;

            $page_result_prop = $this->collectionmodel->getFrameProperties($frameId);

            $data_all['property_data'] = $page_result_prop;
            $data_all['class_name'] = 'mdl-cell mdl-cell--3-col collec-table coll-border';
            $data_all['static_coll_id'] = $collectionId;
            $data_all['id'] = $_POST['id'];
        } elseif ($_POST['type'] == 'collection') {

            $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(COLL_COLLECTION);

            $collectionId = $json_page_result_COLL[0]->Results[0]->CollID;

            $column_name = '_id';

            $json_page_result_FRAME = $this->collectionmodel->getFrameDataByName(COLL_FRAMEID);

            $frameId = $json_page_result_FRAME[0]->Results[0]->frameID;

            $page_result_prop = $this->collectionmodel->getFrameProperties($frameId);

            $data_all['property_data'] = $page_result_prop;
            $data_all['class_name'] = 'mdl-cell mdl-cell--3-col collec-coll coll-border';
            $data_all['static_coll_id'] = $collectionId;
            $data_all['id'] = $_POST['id'];
        }

        $json_page_result = $this->collectionmodel->getCollectionFilterByColumn($collectionId, $column_name, $_POST['id']);

        $data_all['showpropertyData'] = $json_page_result[0]->Results[0];

        $json_page_result_COLL = $this->collectionmodel->getCollectionDataByName(DEFAULT_COLLECTION_PAGEID);

        $data_all['dataframeid'] = $json_page_result_COLL[0]->Results[0]->CollID;

        echo $this->load->view('propertydata', $data_all, true);
    }


    public function deleteTable()
    {
        $json_page_result = $this->collectionmodel->deleteTable($_POST['table_id']);
        echo $json_page_result;
    }


    public function collectionData()
    {

        $collectionId = $_POST['collection_id'];
        $json_page_result = $this->collectionmodel->getCollectionData($collectionId);
        echo $json_page_result;
    }
}
