<?php

Class Bsit_io extends CI_Model{
	
	
	public $Appkey;
	public  $Apppassphere;
	function __construct(){
		parent::__construct();
		
		$this->Appkey = APP_KEY;
		$this->Apppassphere = APP_PASSPHRASE;
		
		
		
		
	}
	
	function setAppParameters($key,$passphere){
		$this->Appkey = $key;
		$this->Apppassphere = $passphere;
	}
		
	
	
	
	// this will post the query to the API interface
	function CallAPI($method, $url, $data = false,$dataPassAsQueryString = true){
		// echo "Total Req: " . $this->session->userdata('total_req');
		// echo "<br/>";
		// echo "Total Time: " .  $this->session->userdata('total_time');
		
		// $this->session->set_userdata('total_time', 0);
		// $this->session->set_userdata('total_req', 0);
		// exit;
				
		
		
		$identifier_data = $this->Appkey.':'.$this->session->userdata('Username');
		
		$Identifier = base64_encode($identifier_data);
		$timeStamp = gmdate('j F Y g:i a', time());
		$fragUrl = parse_url($url);
		$fragUrlPath = $fragUrl['path'];
		$curl = curl_init();
		$authKey = $this->getAuthorizationKey($method, $fragUrlPath, $timeStamp);
		
		
		/*debug('Authorization: Basic '.$authKey);
		debug('Identifier: '.$Identifier);
		debug('timeStamp: '.$timeStamp);exit;*/
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	         'Authorization: Basic '.$authKey
			,'Identifier: '.$Identifier
			,'Timestamp: '.$timeStamp
			,'Content-Type: application/json'
		));

		
		
		switch ($method)
		{
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
				if ($data['collections'] != ""){
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data['collections']);
				}
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				break;
			case "DELETE":
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
				if ($data['collections'] != ""){
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data['collections']);
				}
				break;
			default:
				if ($data)
					if($dataPassAsQueryString)
					$url = sprintf("%s?%s", $url, http_build_query($data));
					else{
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
						curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
					}
		}
		
		// Optional Authentication:
		//curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_NO);
		//curl_setopt($curl, CURLOPT_USERPWD, "username:password");
	
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($curl);

		// $execTime = $this->session->userdata('total_time');
		// $totalReq = $this->session->userdata('total_req');
		// if (!curl_errno($curl)) {
		// 	$info = curl_getinfo($curl);
		// 	$execTime += $info['total_time'];
		// 	$totalReq += 1;
		// 	$this->session->set_userdata('total_time', $execTime);
		// 	$this->session->set_userdata('total_req', $totalReq);
		// }
		  
		curl_close($curl);
	
		return $result;
	}
	
	
	function getAuthorizationKey($method, $url, $time){
		
		
		
		$app_key 		= $this->Appkey;
		
		$app_passphrase = $this->Apppassphere;
		$app_root 		= APP_ROOT;
		$app_url 		= $url;
		$app_type 		= $method;
		$app_timestamp 	= $time;// Must be in UTC timezone, test server is already
		
		//error_log($app_passphrase);
		$app_hashalgo	= 'sha256';
		// get the request details as a string
		$req_detail_string = strtolower($app_type.'|~|'.$app_timestamp.'|~|'.$app_url);
		
		//error_log($req_detail_string);
		// get the string token
		$token = hash_hmac($app_hashalgo , $req_detail_string, $app_passphrase,true);
		// base64 encode the string
		$token_encrypted = base64_encode($token);
		//error_log($token_encrypted);
		// setup the application key
		$security_key = $app_key.':'.$token_encrypted;
		//error_log($security_key);
		// base64 encode the security key
		$security_key_encrypted = base64_encode($security_key);
		//error_log($security_key_encrypted);
		return $security_key_encrypted;

	}
}
