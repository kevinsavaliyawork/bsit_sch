	<script src="<?php echo base_url();?>opportunities/assets/js/opportunity_details.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>opportunities/assets/css/opportunity_style.css">
	<?php $job_id= isset($_GET['job_id']) ? $_GET['job_id'] : $_POST['job_id'];?>
<div class="bsit_opp_overview" >

	<div class="mui-col-lg-8 mui-col-md-12 mui-col-sm-12 mui-col-xs-12" id="bsit_opp_overview_data">
		<input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id?>">
		<?php $this->load->view('Overview', ['customer' => $customer , 'job_id' => $job_id]);?>
	</div>
	 
	<div class="mui-col-lg-4 mui-col-md-12 mui-col-sm-12 mui-col-xs-12">
		<div class="bsit_opp_overview_activity">
			<div class="bsit_opp_overview_activity_title">
				<h3  style="float: left;width: 50%;">Activity (<span id="activityCount"><?php echo count($activity_history->Data) ?></span>)</h3>
				<div class="mui-textfield bsit_form bsit_statusdiv">				
				</div>
				<?php	
				$html_action_note='';
				$html_action_note .= '<div style="float: left;">';
					
				$html_action_note .= '<a class="bsit-notes-btn notes-btn-34" data-collid="73" data-recid="'.$_GET["job_id"].'" data-modal="notes">
									<div class="BSIT-TableActions hasTooltip bsit_opp_delete_edit" style="margin-right:10px; opacity: 1" data-toggle="tooltip" data-placement="bottom" data-original-title="Notes">
										<label style="display: block; float: left; padding: 05px 15px; border-radius: 50px; color: #fff; background-color: #2196f3; min-width: 130px; text-align: center; font-size: 14px;">Add/View Note</label>
										<span class="icon_tooltip">Notes</span>
									</div>
								</a>';
					$html_action_note .= '</div>';
				?>

				<div><span class="bsit_cola_overview_edit_notes"><?php echo $html_action_note ?></span></div>
			</div>
			
			<input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id?>">
			<div class="cola_activity_list_scroll" id="activity_notes_list">
				<?php $this->load->view('jobs_notes_list', ['activity_history' => $activity_history , 'job_id' => $job_id]);?>
			</div>
		</div>
	</div>

	</div>

	<?php
/*echo "<pre/>";
print_R($frame);
*/
function RemoveSpecialChar($str)
{
    $res = preg_replace('/[\[\]\" "]+/', '', $str);
    return $res;
}
$str = $customer[0]->def_event_color;
$str1 = RemoveSpecialChar($str);
$arry = explode(",", $str1);
$color= $arry[0];
?>
<script type="text/javascript">
	$(document).ready(function()
{ 
// upload documents add button
	var html='';
	var color= '<?php echo $color; ?>';
	var address= '<?php echo $customer[0]->customerName.' - '.$customer[0]->org_type; ?>';
	var name= '<?php echo $customer[0]->job_title; ?>';
	<?php if((isset($frame['allow_edit']) && $frame['allow_edit'] == '1') || $frame['Properties'][0]->AllowEdit == '1')	{ ?>
	 html = '<a id="customerEditButton"><div class="BSIT-TableActions hasTooltip bsit_opp_delete_edit" style="margin-right:10px;opacity:1;"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-weight:bold;font-size:18px;color: #2196f3;"></i><span class="icon_tooltip">Edit </span></div></a>';
	<?php } ?>
    change_color(color,address,name,html);
}); 
</script>