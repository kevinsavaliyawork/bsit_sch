    /*
 * jQuery File Upload Demo
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $ */

//var file_upload_url = base_url + "core/apilocal/documentfileuploadDoc";
$("#document_type_code").autocomplete({
  change: function (event) {
    showUrlField($(event.target).attr("data-selectedid"));
  }
});

var old_val = $("#document_type_code").attr("data-selectedid");
var new_val = '';
var count = 0;
setInterval(function () {
  new_val = $("#document_type_code").attr("data-selectedid");
  if (new_val && new_val != old_val) {
    showUrlField(new_val);
    old_val = new_val;
    new_val = '';
  }

  count = $("#uploadFileCount").find("tr").length;
  if (count > 20) {
    $("#maxFileError").show();
  } else {
    $("#maxFileError").hide();
  }
}, 500);

function showUrlField(val) {
  if (val && val.toLowerCase() == "url") {
    let el = $("#uploadFileCount").find("tr");
    $(el).each(function() {
      $( this ).find( ".cancel" ).trigger("click");
    });
    $("#uploadFileCount").html('');
    $("#doc_url_div").show();
    $("#imgUploadBox").hide();
    
  } else {
	$(".uploadbutton").show();
    $(".cola_table-responsive").show();
    $("#doc_url_div").hide();
    $("#doc_url").val('');
    $("#imgUploadBox").show();
  }
}

function resetUploadFields() {
  $("#document_type_code").removeClass("required_border_class");
  $("#doc_url").removeClass("required_border_class");
  $("#fileuploadmultiple").closest("div").removeClass("required_border_class");
  $('#doc_keywords').val('');
  $('#doc_description').val('');
  $('#document_type_code').val('');
  $('#doc_url').val('');
  $('#doc_url_div').hide('');
  $("#maxFileError").hide();
  $("#imgUploadBox").hide();
}

// function clearFilesQueue() {
//   let el = $("#uploadFileCount").find("tr");
//   $(el).each(function() {
//     $( this ).find( ".cancel" ).trigger("click");
//   });
// }



