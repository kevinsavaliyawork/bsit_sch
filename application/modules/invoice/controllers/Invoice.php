<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends brain_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('module');
		//$this->load->model('DB_IO');
		$this->load->model('Bsit_io','API');


		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

		
	}

	function index(){
		// load the infomation
		$userID = $this->session->userdata('UserID');
		$tmp_data['menu_data'] = $menu 	= $this->module->getClientActiveModules($userID);

		$data['menu_data'] = $this->load->view ('menu.php', $tmp_data, TRUE);

		// get the users ID so its available in the page
		$data['userid'] =$this->session->userdata('UserID');

		// get the list of users in the system
		$USRCURLDATA 	= 	Array('collections' =>
								json_encode(
									Array(
										'colls'=> Array(
										  /*  // user list
											Array(
												'coll_id'	=> '16',
												'filter'	=> ""
											),*/
											// Customer list
											Array(
												'coll_id'	=> '12',
												'order'		=> 'o.name', // TaskId-38[Sort customer drop down]
												'filter'	=> "",
												'numrec' => 5000
											)/*,
											// job list
											Array(
												'coll_id'	=> '1',
												'cols'		=> Array('jobid','ParentJobID','name','desc','CustomerID'),
												'filter'	=> "jb.status <> 'Complete'"
											),
											// Work Type list
											Array(
												'coll_id'	=> '22',
												'cols'		=> Array('WorkTypeId','Name'),
												'filter'	=> ""
											),
											// Invoice Header list
											Array(
												'coll_id'	=> '23',
												'cols'		=> Array('InvoiceId','InvNum','CustomerId','Status','StartDate','EndDate'),
												'order'		=> 'il.[_id] desc',
												'filter'	=> ""
											),*/
										)
									)
								)
							);

		// call the API
		$BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
		$users_result = $this->API->CallAPI("GET",$BASEURLMETHOD,$USRCURLDATA);

		// convert JSON to array/object
		$users_result = json_decode($users_result);

		if(isset($users_result->Data)){
			$users_result = $users_result->Data;
		}

		// Customers
		$data['curlDataCust'] = $users_result[0]->Results;
		// Invoice - (get last field of Invoice, used for Generate invoice-Number)
		// $data['curlDataInvoice'] = $users_result[4]->Results;

		// load the page


		return $this->load->view('Invoice/generate_invoice',$data,true);


	}


function report($data_pass){


		return $this->load->view('Invoice/InvReportView',$data,true);
		
	}

}
