<?php

/**
 * File Doc Comment
 */

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Doc Comment
 */
class Page extends brain_controller
{
    const ERRORVIEW = "bsiterror";

    /**
     * Constructor Doc Comment
     */
    public function __construct()
    {
        parent::__construct(0);

        $this->load->model('pagemodel');
        $this->load->model('module');
        $this->load->library('session');
    }

    public function index()
    {
        // cache dir variable
        $staticDir = APPPATH . "views/static/";
        $cacheDir = APPPATH . "views/cache/";

        // The frames variable will be looped when its time for display
        $frames = array();

        // load the menu infomation
        $userID = $this->session->userdata('UserID');
        $tmp_data['menu_data']  = $this->module->getClientActiveModulesPermission($userID);

        // get the page name that should be loaded from the url
        $page_name = $this->uri->segment(2, 0);

        $load_error = FALSE;

        if ($page_name == "0") {
            $load_error = true;
            $data['error_msg'] = "No page segment was defined, or could be found.";
        }



        // permission code start

        $curr_url = 'http://' . $_SERVER['HTTP_HOST'] . "" . $_SERVER['REQUEST_URI'];
        $page_flg = 0;


        foreach ($tmp_data['menu_data']  as $checkPermission) {


            if (($this->session->userdata('GroupID') == $checkPermission->GroupId) && (("page/" . $this->uri->segment(2, 0) == $checkPermission->ModuleName || strpos(urldecode($_SERVER['REQUEST_URI']), $checkPermission->ModuleName) > 0))) {

                $page_flg = 1;
                break;
            } else {
                $page_flg = 0;
            }
        }

        if ($page_flg == 0) {

            $load_error = true;
            $html = $this->load->view('Error_page', '', true);

            $data['page_content'] = $html;
        }



        // permission code end

        // -- REQUEST THE PAGE HEADER INFO
        $page_result = $this->pagemodel->getPageHeaderInfo($page_name);
        //      echo "<pre>";
        // print_r($page_result); exit;

        $data['curldata'] = $page_result;


        // check menu, header and footer is visible or not.

        $pageMenu = $page_result[0]->Results[0]->is_display_menu;
        $menu_file = $page_result[0]->Results[0]->Menu;

        $pageHeader = $page_result[0]->Results[0]->is_display_header;
        $header_file = $page_result[0]->Results[0]->Header;

        $pageFooter = $page_result[0]->Results[0]->is_display_footer;
        $footer_file = $page_result[0]->Results[0]->Footer;


        // if menu checkbox is enabled and menu file is not empty then then it will called database file otherwise default menu

        if ($pageMenu == 1) {

            if ($menu_file == '') {
                $data['menu_data'] = $this->load->view('menu.php', $tmp_data, TRUE);
            } else {



                $displayMenuFile = explode("/", $menu_file);

                $moduleName = $displayMenuFile[0];
                $controllerName = $displayMenuFile[1];
                $functionName = $displayMenuFile[2];

                require_once(APPPATH . 'modules/' . $moduleName . '/controllers/' . $controllerName . '.php');
                $oHome =  new $controllerName();
                $data['menu_data'] = $oHome->$functionName($tmp_data);
            }
        }
        $data['header_view'] = 0;
        $data['pageMenu'] = $pageMenu;
        if ($pageHeader == 1) {
            $data['header_view'] = 1;
        }

        $data['footer_view'] = 0;
        if ($pageFooter == 1) {
            $data['footer_view'] = 1;
        }

        $data['error_msg'] = "";


        // GET DATA RELATED SEARCH/SORT/PAGING
        if (isset($_REQUEST['order'])) {
            $sorting = $_REQUEST['order'];
        } else {
            $sorting = '';
        }

        if (isset($_REQUEST['filter'])) {
            $filtering = $_REQUEST['filter'];
        } else {
            $filtering = '';
        }

        if (isset($_REQUEST['page_num'])) {
            $page_number = $_REQUEST['page_num'];
        } else {
            $page_number = '';
        }

        if (isset($_REQUEST['num_recs'])) {
            $number_of_records = $_REQUEST['num_recs'];
        } else {
            $number_of_records = 10;
        }



        $other_func_data['sorting'] = $sorting;
        $other_func_data['filtering'] = $filtering;
        $other_func_data['page_number'] = $page_number;
        $other_func_data['number_of_records'] = $number_of_records;
        $other_func_data['download_data'] = 0;

        // FIXME: Spelling error

        if (isset($page_result->status) && $page_result->status <> "sucess") {
            $load_error = true;
            $error_message = (isset($page_result->error)) ? $page_result->error : $page_result->message;
            $error_msg[] = "CURL Error:" . $error_message;
        }


        // The page info should now be loaded and our page header info
        if (isset($page_result[0]->Results)) {
            $pageInfo = $page_result[0]->Results[0];
            $pageInfo->Title = $this->swapout_variables($pageInfo->Title);
            // TODO: Get PageType & MaxPageWidth?
            $data['pageInfo'] = $pageInfo;
            $data['pageTitle'] = $pageInfo->Title;
            $data['PageModuleName'] = $pageInfo->ModuleName;
            $data['pageId'] = $pageInfo->pageId;
            $data['page_req'] = $_GET;
        } else {
            $load_error = true;
            $error_message = "Page details couldn't be loaded";
            $error_msg[] = "API Error:" . $error_message;
        }



        // pass the error messages into the error view, if any
        $data['error_msg'] = $error_msg;

        // load the page header

        if ($sorting == '' && $filtering == '' && $page_number == '') {

            // if header checkbox is enabled and header file is not empty then then it will called database file otherwise default header

            if ($pageHeader == 1) {

                if ($header_file == '') {
                    $notifications_Data = $this->pagemodel->getnotificationsData();
                    $notifications = json_decode($notifications_Data);
                    $data['notifications'] = $notifications->Data[0]->Results;
                    $this->load->view('header', $data);
                } else {

                    $displayHeaderFile = explode("/", $header_file);

                    $moduleName = $displayHeaderFile[0];
                    $controllerName = $displayHeaderFile[1];
                    $functionName = $displayHeaderFile[2];

                    if (file_exists(APPPATH . 'modules/' . $moduleName . '/config/constants.php')) {
                        require_once(APPPATH . 'modules/' . $moduleName . '/config/constants.php');
                    }

                    require_once(APPPATH . 'modules/' . $moduleName . '/controllers/' . $controllerName . '.php');

                    $oHome =  new $controllerName();

                    $oHome->$functionName($data);
                }
            } else {
                $notifications_Data = $this->pagemodel->getnotificationsData();
                $notifications = json_decode($notifications_Data);
                // print_r($notifications); exit;
                $data['notifications'] = $notifications->Data[0]->Results;
                $this->load->view('header', $data);
            }
        } else {
            $this->load->view('header_sort', $data);
        }






        // check if a cached version of that page already exists
        // if not, we need to generate the page and store as a view for loading.
        // -- REQUEST THE PAGE BODY PARTS

        if ($load_error != true) {

            // we now know the user has access to this page and permission to open it.
            // now we can request the additional page info needed from the server
            // this will include the CSS, Javascript files and most importantly frames.

            // if this is a page built using the automated building code
            $record_limit = '';

            if ($pageInfo->Type == 'builder') {
                // Firstly we need to build the request package that will be sent to the server

                $page_frame_properties = $this->pagemodel->getPageFrameProperties($pageInfo->pageId, $record_limit);

                foreach ($page_frame_properties[0]->Results as $property) {
                    // get the page frame id - Key for further lookups
                    $PFID = $property->PageFrameId;

                    // get the frame order number
                    $FO = $property->FrameOrder;


                    // if the frame doesn't already exist add it to the frames array
                    if ($property->default_value != '') {
                        $property->default_value = $this->swapout_variables($property->default_value);
                    }

                    if (!array_key_exists($PFID, $frames)) {

                        // Getting data for Grourp Permission for Particular Frame
                        $frames = $this->GetFramePermission($PFID, $frames, $property, $cacheDir);
                    }

                    // add the property to the properties array
                    $frames[$PFID]['Properties'][] = $property;
                }

                // -- SETUP FRAME AND PROPERTY ARRAY - CALLING FROM THE FRAME PAGE
                // loop each of the frame properties to build an array of frame
                // and another array of properties.  These arrays will be used both
                // during the display and loading the data for each frame
                require_once(APPPATH . 'modules/core/controllers/frame.php');
                $frame_data = new Frame();
                $count_frame = 1;
                $totalFrame = count($frames);

                $page_name = $this->uri->segment(2, 0);
                $page_result = $this->pagemodel->getPageHeaderInfo($page_name);
                $enableTabbedFrames = $page_result[0]->Results[0]->enable_tabbed_frames;

                foreach ($frames as $frame) {
                    $pageFrameId = 0;

                    //SET DEFAULT PAGE SIZE IN PAGINATION
                    if ($frame['Properties'][0]->default_page_size != '') {
                        $frameName = $frame['Properties'][0]->FrameName;
                        $pageFrameId = $frame['Properties'][0]->FrameID;
                        $other_func_data['number_of_records'] = $frame['Properties'][0]->default_page_size;
                    } else {
                        $frameName = $frame['Properties'][0]->FrameName;
                        $pageFrameId = $frame['Properties'][0]->FrameID;
                        $other_func_data['number_of_records'] = 10;
                    }

                    // PASS PARTICULOR FRAME & SEARCH/SORT/PAGINATION ARRAY ON FRAME PAGE
                    // AND GET THE HTML-DATA FROM FRAME
                    //$content_html = $frame_data->load_frame($frame, $other_func_data);
                    $content_html = $frame_data->load_frame($frame, $other_func_data, $count_frame, $enableTabbedFrames);

                    $frameID = $frame['PFID'];
                    $frame['collection_id']  = $frame['collection'];
                    $frame['frameID'] = $frameID;
                    $frame['pageFrameId'] = $pageFrameId;
                    $frame['frameName'] = $frameName;
                    $frame['other_func_data'] = $other_func_data;
                    $frame['frame_content'] = $content_html;
                    $frame['default_page_size'] = $frame['Properties'][0]->default_page_size;
                    $frame['filter'] = $this->swapout_variables($frame['Properties'][0]->FrameFilter);
                    $frame['pageId'] = $pageInfo->pageId;
                    $frame['count_frame'] = $count_frame;
                    $frame['totalFrame'] = $totalFrame;
                    $frame['frames'] = $frames;
                    $frame['enableTabbedFrames'] = $enableTabbedFrames;

                    $html = $this->load->view('framebase', $frame, true);

                    $data['page_content'] .= $html;
                    $count_frame++;
                }
            }
        }



        $this->load->view('content', $data);


        // do we have any frames to load
        if (count($frames) > 0) {
            // -- JAVASCRIPT
            // loop each of the frames again and load the generic javascript
            // with only the data for this frame
            foreach ($frames as $key => $frame) {
                // Set the frame property ID so ID references will work
                $data['PFID'] = $frame['PFID'];
                // $data['FramePageView'] = $frame['FramePageView'];
                $data['frame_order'] = $frame['order'];
            }
        } else {
            // update the error message and set the view as the error view
            $error_msg[] = "No frames to load: " . $key;
            $page_name = self::ERRORVIEW;
        }

        // load the page footer
        if ($sorting == '' && $filtering == '' && $page_number == '') {

            // if footer checkbox is enabled and footer file is not empty then then it will called database file otherwise default footer

            if ($pageFooter == 1) {

                if ($footer_file == '') {
                    $this->load->view('footer');
                } else {

                    $displayFooterFile = explode("/", $footer_file);

                    $moduleName = $displayFooterFile[0];
                    $controllerName = $displayFooterFile[1];
                    $functionName = $displayFooterFile[2];

                    require_once(APPPATH . 'modules/' . $moduleName . '/controllers/' . $controllerName . '.php');

                    $oHome =  new $controllerName();

                    $oHome->$functionName();
                }
            }
        }
    }

    public function swapout_variables($origstr)
    {
        // find all the occurances of V(XXX)
        preg_match_all('#V\((.*?)\)#', $origstr, $matches);

        $updatestr = $origstr;

        foreach ($matches[1] as $match) {
            $updatestr = $this->swapout_multiple_fields($updatestr, $match);
        }

        return $updatestr;
    }

    // GET FILTER VALUES AS MULTIPLE FILTER
    // TODO: Can these swapout filter funcs be converted into 1 with an extra parameter?
    public function swapout_multiple_fields($origstr, $match)
    {
        $singleval = '';

        if ($_GET[$match] != '') {
            $singleval = str_replace("V(" . $match . ")", $_GET[$match], $origstr);
        }

        return $singleval;
    }


    // [end] GET QUERY STRINGS VARIABLE's VALUE FOR PERMANANT FILTER

    // -- USE THIS FOR LOADING THE FRAME THROUGH AJAX
    public function swapout_variables_ajax()
    {
        $origstr = $_GET['fram_filter_data'];
        $matches = $_GET['parameter'];

        // find all the occurances of V(XXX)
        preg_match('#V\((.*?)\)#', $origstr, $matches);
        $updatestr = $origstr;

        foreach ($matches as $match) {
            $updatestr = str_replace("V(" . $match . ")", $_GET[$match], $origstr);
        }

        echo $updatestr;
    }


    // Getting data for Group Permission for Particular Frame   
    public function GetFramePermission($PFID, $frames, $property, $cacheDir)
    {


        $fullPath_header = $cacheDir . $property->FrameName . '_' . $PFID . '_header.php';
        $viewPath_header = 'cache/' . $property->FrameName . '_' . $PFID . '_header';
        $fullPath_content = $cacheDir . $property->FrameName . '_' . $PFID . '_content.php';
        $viewPath_content = 'cache/' . $property->FrameName . '_' . $PFID . '_content';

        if (file_exists($fullPath_header)) {
            $frameCacheExists_header = true;
        } else {
            $frameCacheExists_header = false;
        }


        // fetch data from frame permission
        $page_result_group_permission_data = $this->pagemodel->getFramePermission($property->FrameID);



        // add the frame details to frames array
        if (!empty($page_result_group_permission_data[0]->Results)) {
            // add the frame details to frames array
            $frames[$PFID] = array(
                'PFID' => $PFID,
                //'FramePageView' => $FramePageView,
                'name' => $property->FrameName,
                'order' => $property->FrameOrder,
                'type' => $property->FrameType,
                'filter_original' => $property->FrameFilter,
                'inner_page'    => $property->edit_path,
                'filter' => $this->swapout_variables($property->FrameFilter),
                'editURL' => $property->EditURL,
                'editParms' => json_decode($property->EditParms),
                'FrameTitle' => $property->FrameTitle,
                'collection' => $property->CollId,
                'fullPath_header' => $fullPath_header,
                'viewPath_header' => $viewPath_header,
                'fullPath_content' => $fullPath_content,
                'viewPath_content' => $viewPath_content,
                'cacheExists_header' => $frameCacheExists_header,
                'allow_view' => $page_result_group_permission_data[0]->Results[0]->allow_view,
                'allow_add' => $page_result_group_permission_data[0]->Results[0]->allow_add,
                'allow_edit' => $page_result_group_permission_data[0]->Results[0]->allow_edit,
                'allow_delete' => $page_result_group_permission_data[0]->Results[0]->allow_delete,
                'allow_notes' => $page_result_group_permission_data[0]->Results[0]->allow_notes,
                'allow_links' => $page_result_group_permission_data[0]->Results[0]->allow_links,
                'allow_download' => $page_result_group_permission_data[0]->Results[0]->allow_download,
                'allow_multiple_edit' => $page_result_group_permission_data[0]->Results[0]->allow_multiple_edit,
                'allow_multiple_delete' => $page_result_group_permission_data[0]->Results[0]->allow_multiple_delete,
                'tot_page_count' => $property->total_count,
                'class_options' => $property->class_options,
                'frame_width' => $property->frame_width,
                'frame_height' => $property->frame_height,
                'frame_offset' => $property->frame_offset
            );
        } else {
            // add the frame details to frames array
            $frames[$PFID] = array(
                'PFID' => $PFID,
                //'FramePageView' => $FramePageView,
                'name' => $property->FrameName,
                'order' => $property->FrameOrder,
                'type' => $property->FrameType,
                'filter_original' => $property->FrameFilter,
                'inner_page' => $property->edit_path,
                'filter' => $this->swapout_variables($property->FrameFilter),
                'editURL' => $property->EditURL,
                'editParms' => json_decode($property->EditParms),
                'FrameTitle' => $property->FrameTitle,
                'collection' => $property->CollId,
                'fullPath_header' => $fullPath_header,
                'viewPath_header' => $viewPath_header,
                'fullPath_content' => $fullPath_content,
                'viewPath_content' => $viewPath_content,
                'cacheExists_header' => $frameCacheExists_header,
                'tot_page_count' => $property->total_count,
                'class_options' => $property->class_options,
                'frame_width' => $property->frame_width,
                'frame_height' => $property->frame_height,
                'frame_offset' => $property->frame_offset
            );
        }
        return $frames;
    }
}
