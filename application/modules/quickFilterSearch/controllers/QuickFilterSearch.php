<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class QuickFilterSearch extends brain_controller {

    function __construct() {
    parent::__construct(1);
    // Load the needed librarys
    $this->load->model('core/Bsit_io','API');
    $this->load->library('session');
    $this->load->model('quickFilterSearch/QuickFilterSearchmodel');
    
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }


	
   /**
    * Get Search over data
    * It will use when user edit then we will fetch 
    * fetch All Data based on serach text
    * Added by Niru [21-6-2021]
    */
    
    public function quickfilterView()
    {
        $userID = $this->session->userdata('UserID');
        $loggedIn = $this->session->userdata('logged_in');
        if ($loggedIn == false or $userID == "") {
            return;
        }

        // get the search term requested
        $searchTerm = '';
        if (isset($_GET['searchTerm'])) {
            $searchTerm = $_GET['searchTerm'];
        }

        $collection_id = null;
        if (isset($_GET['collection_id'])) {
            $collection_id = $_GET['collection_id'];
        }

       
        $quickfilterViewList = $this->QuickFilterSearchmodel->quickSearchTerm($searchTerm);//($_POST["_id"]);
        // output results as json type
        header('Content-Type: application/json');
        echo json_encode($quickfilterViewList);

        exit;
    }
 public function quickfiltersearch_json()
    {
        
            $searchData = $_GET['search'];
            $getgenericsearch = $this->QuickFilterSearchmodel->quickSearchTerm($searchData);
            
            $generic_all_search = json_decode($getgenericsearch)->Data;
            $result = [];
            $i = 0;
            foreach($generic_all_search as $record) {
                if ($i >= 5) {
                    break;
                } 
                $result[] = $record;
                $i++;
            }

            echo json_encode($result);
            exit;
    }
  
   public function quickfilterList()
    { 

        //print_R($_SESSION);
       /// echo $searchData = $this->session->userdata('searchData');
        if($this->session->userdata('searchData') == ""){
            return $this->load->view('quickFilterSearch/searchList',$data_pass,true); 
        }else{
            $searchData = $this->session->userdata('searchData');
			$numofrows = 1500;//$_GET['numofrows'];
            $getgenericsearch = $this->QuickFilterSearchmodel->quickSearchTerm($searchData,$numofrows);
            //$getgenericsearch = $this->QuickFilterSearchmodel->quickSearchTerm($searchData);
            //print_r($getgenericsearch);
            $generic_all_search = json_decode($getgenericsearch)->Data;
            $data_pass['generic_all_search'] =$generic_all_search;
            // $data_pass['search_disply_text'] = $this->session->userdata('searchData');
           // $this->session->set_userdata('searchData','');
            return $this->load->view('quickFilterSearch/searchList',$data_pass,true); 
        }
    }

    public function quickfilterallsearch()
    {
        $this->session->set_userdata('searchData', $_POST['searchData']);
        $data['generic_all_search'] = $_POST['searchData'];
        echo json_encode($data);
    }
      
}
