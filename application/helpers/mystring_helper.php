<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (! function_exists('mystring')) {
    /**
     * mystring converter
     * */
    function mystring()
    {
    }

    function array_to_string($array)
    {
        if (!empty($array)) {
            return implode(',', unserialize($array));
        } else {
            return $array;
        }
    }

    function xml2array($xmlObject, $out = array())
    {
        foreach ((array) $xmlObject as $index => $node) {
            $out[$index] = (is_object($node)) ? xml2array($node) : $node;
        }

        return $out;
    }

    function process_address($customer)
    {
        $address1 = isset($customer->addr1) ? $customer->addr1 : '';
        $address2 = isset($customer->addr2) ? $customer->addr2 : '';
        $address3 = isset($customer->addr3) ? $customer->addr3 : '';
        $address4 = isset($customer->addr4) ? $customer->addr4 : '';
        $city = isset($customer->city) ? $customer->city : '';
        $state = isset($customer->state) ? $customer->state : '';
        return $full_address = $address1. ' '.$address2.' '.$address3.' '.$address4.' '.$city.' '.$state;
    }
}
