<script src="<?php echo base_url();?>opportunities/assets/js/opportunity.js"></script>
<?php
	// ------------------------------
	// -- CREATE LISTVIEW CACHE FILE
	// ------------------------------
	// Its Inclue : Display the Content(Data) of Files

	$cacheDir	= APPPATH."views/cache/";
	$linkopen = "";
	$html = "";
	$html_pagi = "";
	$fullPath	= $frame['fullPath_content'];
	$viewPath	= $frame['viewPath_content'];

	$linkopen = "";
	$html .= '<div id="page_frame_'.$frame['PFID'].'_content" class="call_content_div">';
	$html .= '<input type="hidden" id="frame_order" value='.$frame['order'].'>';
	$html .= '<div id="job_view_status_container">';

	// echo "<pre>";
	// print_r($load_data);
	// echo "</pre>";

	$parent_workflow_id = '';
	
	// call to get a list of all the status options.
	$CURLDATA 	= 	Array('collections' =>
				json_encode(
					Array(
						'colls'=> Array(
							Array(
								'coll_name'	=> 'opportunity_workflow_steps',
								'cols'		=> Array("StatusTitle","WorkflowOrder", "TotalValue", "ExpectedValue"),
								'order'		=> "WorkflowOrder ASC",
								'numrec'	=> '9999')))));
								
     $BASEURLMETHOD = API_BASEURL.API_COLLECTION_GET;
	$json_page_result = $this->API->CallAPI("GET",$BASEURLMETHOD,$CURLDATA);
	$page_result = json_decode($json_page_result);
	
	// CREATE ARRAYS OF JOB STATUS & BASED ON THAT DISPLAY JOB STATS
	$status_results_data 	= $page_result->Data;
	$status_results_array 	= $status_results_data[0]->Results;

	foreach($status_results_array as $key=>$data)
	{
		$selectOptions_status[] = $data->StatusTitle;
		//$selectOptions_status[$data->status_title] = $data->_id;
	}

	// loop each job, if jobs has a stutus not in the array of stats, set to other!
	$other_needed = false;
	$other_status_text = 'InvalidStatus';
	foreach ($load_data as $key=>$frow)
	{
		// IS this status in the list of status options
		if(in_array($frow->Status,$selectOptions_status) != true){
			$load_data[$key]->Status = $other_status_text;
			$other_needed = true;
		}
		
		// Build a new array to hold the opportunities under each status
		$opps_in_status[$frow->Status][] = $load_data[$key];
	}
	// Add the option for other status
	if($other_needed){
		$selectOptions_status[] = $other_status_text;
		//$selectOptions_status[$other_status_text] = $other_status_text;
	}

	// echo "<pre>";
	// print_r($opps_in_status);
	// echo "</pre>";
	
	$html1 = '';
	// Loop each status and output the rows in each.
	foreach($selectOptions_status as $optVal=>$opt)
	{
		// Total Number of Job Count
		$count_job = $status_results_array[$optVal]->OppCount;
		$html .= '<div class="status_class">';
		$html .= '<div class="mui-panel">';
		$html .= '<div class="mui--text-subhead status_class_heading"><b>'.$opt.'</b> ('.$count_job.')</div>';
		
		// View value here - Check count of jobs is high enough
		if($count_job !=0){
			$html .= '<p>Total Value: $'.$status_results_array[$optVal]->TotalValue.'</p>';
			$html .= '<p>Est Value: $'.$status_results_array[$optVal]->ExpectedValue.'</p>';
		}
		$html .= '<div class="content mui-container-fluid status_class_content task-list task-container ui-droppable ui-sortable" id="'.$opt.'">';
		
		// Loop each opportunity and output here
		foreach ($opps_in_status[$opt] as $frow)
		{
			if($frow->Status == $opt)
			{
				// BUTTONS - Edit & Delete & Notes
				$html_action='';
				$html_action .= '<div style="float: right;">';


				if(isset($frame['allow_delete']))
				{
					if($frame['allow_delete'] == '1')
					{
						$html_action .= '<a class="bsit-delete-btn edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="delete">
										<div class="BSIT-TableActions hasTooltip" style="margin-right:3px;">
											<img src="'.BASEURL_IMAGE.'/delete.png"  id="tooltip2'.$frow->key_id.'">
											<span class="icon_tooltip">Delete</span>
										</div>
									</a>';
					}
				}else{
					if($frame['Properties'][0]->AllowDelete == '1')
					{
						$html_action .= '<a class="bsit-delete-btn edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="delete">
										<div class="BSIT-TableActions hasTooltip" style="margin-right:3px;">
											<img src="'.BASEURL_IMAGE.'/delete.png"  id="tooltip2'.$frow->key_id.'">
											<span class="icon_tooltip">Delete</span>
										</div>
									</a>';
					}
				}

				if(isset($frame['allow_notes']))
				{
					if($frame['allow_notes'] == '1')
					{
						$html_action .= '<a class="bsit-notes-btn notes-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="notes">
									<div class="BSIT-TableActions hasTooltip" style="margin-right:10px;">
										<img src="'.BASEURL_IMAGE.'/note-multiple.png" id="tooltip_notes'.$frow->key_id.'">
										<span class="icon_tooltip">Notes</span>
									</div>
								</a>';
					}
				}else{
					if($frame['Properties'][0]->AllowNotes == '1')
					{
						$html_action .= '<a class="bsit-notes-btn notes-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="notes">
									<div class="BSIT-TableActions hasTooltip" style="margin-right:10px;">
										<img src="'.BASEURL_IMAGE.'/note-multiple.png" id="tooltip_notes'.$frow->key_id.'">
										<span class="icon_tooltip">Notes</span>
									</div>
								</a>';
					}
				}

				$html_action .= '</div>';
				$html .= '<div class="mui-row status_class_each_content todo-task" id="'.$frow->jobId.'" >';
				
				// Title - Allow click to edit?
				if((isset($frame['allow_edit']) && $frame['allow_edit'] == '1') || $frame['Properties'][0]->AllowEdit == '1')	{
					$frow->title 	= '<span data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit"><span><a class="bsit-edit-btn opp_title_edit edit-del-btn-'.$frame['PFID'].'" data-collid="'.$frame['collection'].'" data-recid="'.$frow->key_id.'" data-modal="edit" style="cursor:pointer;">'.$frow->title.'</a></span></span>';
				}
				// Title - Output
				$html .= $frow->title;
				
				/*
				echo('<pre>');
				print_r($frow);
				echo('</pre>');
				*/
				
				// LOOP FOR DISPLAYING POPERTY CONTENT
				$cust_name = '';
				if($frow->CustColor == ''){
					$cust_colors[0] = '#000000';
					$cust_colors[1] = '#FFFFFF';
				}else{
					$cust_colors = explode(',',$frow->CustColor);
					$cust_colors = str_replace('[','',$cust_colors);
					$cust_colors = str_replace(']','',$cust_colors);
					$cust_colors = str_replace('"','',$cust_colors);
				}
				$cust_name .= '<div style="float: left;background-color:'.$cust_colors[0].';color:'.$cust_colors[1].';padding: 0 5px 0px;border-radius: 3px; border: thin solid '.$cust_colors[1].';">';
				$cust_name .= $frow->CustName;
				$cust_name .= '</div>';
				$html_job_terms='';
				
				// Job terms - Billable etc
				if($frow->TermsCode == 'Bill'){
					$html_job_terms .= '<img src="'.BASEURL_IMAGE.'/clock.png">';
				}else if($frow->TermsCode == 'QD'){
					$html_job_terms .= '<img src="'.BASEURL_IMAGE.'/currency-usd.png">';
				}
				
				// Assignee
				$assingee = '';
				if(isset($frow->NameFirst)){
						$assingee = '<div style="width: 24px; height: 24px;  border-radius: 50%;  font-size: 12px;  color: #fff;  line-height: 24px;  text-align: center; background: #000; float:right; margin-right: 10px">';
						$assingee .= substr($frow->NameFirst,0,1).'.'.substr($frow->NameLast,0,1);
						$assingee .= '</div>';
				}
				
				// Followup date
				$followup_date_html = '';
				if(isset($frow->followup_date)){
					$followup_sec 	= strtotime($frow->followup_date);
					$timenow_sec	= time();
					$diff_sec		= $followup_sec - $timenow_sec;
					$days			= floor($diff_sec / (60 * 60 * 24)); // 60 seconds per min, 60 mins per hour, 24 hours per day
					
					// if the followup date is overdue or expired show as red
					if($days <= 0){
						$opp_follup_div   = '<div class="opp_followup_date" style="color:#FF0000; font-weight:600; float:left;">';
					}elseif($days <= 3){
						$opp_follup_div   = '<div class="opp_followup_date" style="color:#FFA07A; font-weight:400; float:left;">';
					}else{
						$opp_follup_div   = '<div class="opp_followup_date" style="float:left;">';
					}
					
					// output the follup date
					$followup_date_html	.= $opp_follup_div;
					$followup_date_html	.= '<img src="'.BASEURL_IMAGE.'/clock.png" style="width:14px;"> ';
					$followup_date_html	.= date("d/m/Y" ,$followup_sec).'</div>';
				}
				
				$html .= '<div class="each_job_footer_part">';
					/*
					$html .= '<span class="icons_clock_doller">';
					$html .= $html_job_terms;
					$html .= '</span>';
					*/
						$html .= '<div style="clear:both; padding-top:2px; padding-bottom:2px;">';
						$html .= $cust_name;
						$html .= $assingee;
						$html .= '</div>';
						$html .= '<div style="clear:both; padding-top:9px; padding-bottom:2px;">';
						$html .= $followup_date_html;
						$html .= $html_action;
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
				
			}
			$html .= '<input type="hidden" class="tot_record_'.$frame['PFID'].'" value="'.$frow->total_count.'">';
		}
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
	}
		$html .= $html1;
		$html .= '</div>';
		//$html .= '<input type="hidden" class="tot_record_'.$frame['PFID'].'" value="'.$frow->total_count.'">';

	// ------------------------------
	// -- PAGINATION CODE
	// ------------------------------
	$html_pagi .= '<div class="pagination_tr" >';
	if(isset($frame['allow_download']))
	{
		if($frame['allow_download'] == 1){
			$html_pagi .= '<div class="download_button_div hasTooltip" id="download_data_'.$frame['PFID'].'">';
			$html_pagi .= '<img style="opacity:.4" src="'.BASEURL_IMAGE.'/ic_get_app_black_24dp_1x.png"/><span class="icon_tooltip_frame">Download</span>';
			$html_pagi .= '</div>';
		}
	}else{
		if($prop->hide_allow_download == '0'){
			$html_pagi .= '<div class="download_button_div hasTooltip" id="download_data_'.$frame['PFID'].'">';
			$html_pagi .= '<img style="opacity:.4" src="'.BASEURL_IMAGE.'/ic_get_app_black_24dp_1x.png"/><span class="icon_tooltip_frame">Download</span>';
			$html_pagi .= '</div>';
		}
	}
	if($prop->hide_pagination == '0')
	{
		$html_pagi .= '<div class="pagination dataTables_paginate paging_simple_numbers" id="example_paginate">';
		$html_pagi .= '<div class="pagination">';
		$html_pagi .= '<div id="pagination_output">';
		$html_pagi .= 'Rows Per Page : ';
		$html_pagi .= '<input type="hidden" id="total_page_'.$frame['PFID'].'" value="'.$frame['tot_page_count'].'">';
		$html_pagi .= '<input type="hidden" id="page_num_'.$frame['PFID'].'" class="pagination_class_'.$frame['PFID'].'" value="1">';
		$html_pagi .= '<input type="hidden" id="default_page_size_'.$frame['PFID'].'" value="'.$frame['Properties'][0]->default_page_size.'">';
		$html_pagi .= '<select id="num_recs_'.$frame['PFID'].'" class="number_of_rec" >';
		//$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
		//SET DEFAULT PAGE SIZE IN PAGINATION
		if($frame['Properties'][0]->default_page_size == ''){
			$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
		}else{
			if($frame['Properties'][0]->default_page_size == '10'){
				$html_pagi .= '<option value="10" selected="selected">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
			}else if($frame['Properties'][0]->default_page_size == '20'){
				$html_pagi .= '<option value="10">10</option><option value="20" selected="selected">20</option><option value="50">50</option><option value="100">100</option>';
			}else if($frame['Properties'][0]->default_page_size == '50'){
				$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50" selected="selected">50</option><option value="100">100</option>';
			}else if($frame['Properties'][0]->default_page_size == '100'){
				$html_pagi .= '<option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100"  selected="selected">100</option>';
			}else{
				$html_pagi .= '<option value="'.$frame['Properties'][0]->default_page_size.'">'.$frame['Properties'][0]->default_page_size.'</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option>';
			}
		}
		$html_pagi .= '</select>';
		$html_pagi .= '<div class="pagi_arrows">';
		$html_pagi .= '<input type="button" id="pagi_previous_'.$frame['PFID'].'" data-prevpage="prev" class="mdl-button previous pagination_class_'.$frame['PFID'].'" value="" style="background: url('.base_url().'assets/images/chevron-left.png);background-repeat: no-repeat;">';
		$html_pagi .= '<span id="pagination_number_div"></span>';
		$html_pagi .= '<input type="button" id="pagi_next_'.$frame['PFID'].'" data-nextpage="next" value="" class="mdl-button pagination_class_'.$frame['PFID'].'" style="background: url('.base_url().'assets/images/chevron-right.png);background-repeat: no-repeat;">';
		$html_pagi .= '</div>';
		$html_pagi .= '</div>';
		$html_pagi .= '</div>';
		$html_pagi .= '</div>';
		$html_pagi .= '<input type="hidden" class="PFID" value="'.$frame['PFID'].'">';
	}
	$html_pagi .= '</div>';
	$html .= $html_pagi;

	$html .= $html_sort;
	$html .= '</div>';

	// ------------------------------
	// -- CREATE LISTVIEW CACHE FILE
	// ------------------------------
	//echo $fullPath;


	if (file_exists($fullPath)) {
		$myfile1 = fopen($fullPath, "w") or die("Unable to open file!");
		fwrite($myfile1, $html);
		fclose($myfile1);
	   }
	echo $html;
?>
